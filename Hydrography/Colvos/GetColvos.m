% Extract subsets of Puget Sound CTD data from Colvos Passage Area
clear

load '/Users/ericdasaro/Documents/ADVfloat/Bathy/PScoast.mat'
plot(lon,lat,'r');hold on;

load ../PSAll/clean.mat
% P       SIG     T       readme  
% S       SIG_0   TH      sta     
% sta = 
%     lat: [1x1161 double]
%     lon: [1x1161 double]
%      yr: [1x1161 double]
%      mo: [1x1161 double]
%     day: [1x1161 double]

% Colvos to Blake I
ofile='Colvosclean.mat';
g=find(sta.lat>47.333 & sta.lat<47.58 & sta.lon>-122.58 & sta.lon<-122.46);

% ofile='SColvosclean.mat';  % South part
% g=find(sta.lat>47.333 & sta.lat<47.5 & sta.lon>-122.58 & sta.lon<-122.5);
% 
% ofile='NColvosclean.mat';  % North Part
% g=find(sta.lat>47.48 & sta.lat<47.58 & sta.lon>-122.58 & sta.lon<-122.46);
% 
% ofile='EPassclean.mat';  % E pass i.e. outlet of Blake Island
% g=find(sta.lat>47.4 & sta.lat<47.7 & sta.lon>-122.47 & sta.lon<-122.36);

%%%%%%%%%%%%%%%%%%%%%%%%%%%
plot(sta.lon(g),sta.lat(g),'o');
hold on
for i=1:length(g);
    gg=g(i);
    n=find(sta.lon==sta.lon(gg) & sta.lat==sta.lat(gg));
    text(sta.lon(gg),sta.lat(gg),num2str(length(n)));
end
set(gcf,'Color','w');

figure
% Get just selected data
sta.lon=sta.lon(g);
sta.lat=sta.lat(g);
sta.yr=sta.yr(g);
sta.mo=sta.mo(g);
sta.day=sta.day(g);
% P       SIG     T       readme  
% S       SIG_0   TH      sta     
%all[50(z) x 61(n)]
nn=size(P);
nP=ones(nn(1)+1,length(g))*NaN;
nS=nP;nTH=nP;
for i=1:length(g)
    gg=g(i);
    [nSIG_0(:,i), isort]=sort(SIG_0(:,gg));
    nP(1:end-1,i)=P(:,gg); % sort T,S not P   
    nS(1:end-1,i)=S(isort,gg);
    nTH(1:end-1,i)=TH(isort,gg);
    j=find(~isnan(nP(:,i)));
    j=max(j)+1;% first unfilled point
    nP(j,i)=300;  % add point at end
    nS(j,i)=nS(j-1,i)+0.03;
    nTH(j,i)=nTH(j-1,i);
 end
P=nP;S=nS;TH=nTH;S=nS;
T=sw_temp(S,TH,P,0.);
SIG=sw_dens(S,T,P);
SIG_0=sw_pden(S,T,P,0.);

plot(SIG_0,-P)

save(ofile,'readme','sta','P','S','T','TH','SIG_0','SIG');

















