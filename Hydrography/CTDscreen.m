clear all
load 'Hydrography/Hurricane/HistoricalClean'

badlist=[191 1444 1234 3204 2769 2175 3085 890 2924 697 93 1170 1735];
badlist=[2864];

rho=sw_dens(S(:,badlist),T(:,badlist),P(:,badlist));
sig0=sw_pden(S(:,badlist),T(:,badlist),P(:,badlist),150);

subplot(1,2,1)
plot(sig0-1000,-P(:,badlist),'k','LineWidth',4);
subplot(1,2,2)
plot(S(:,badlist),-P(:,badlist),'r');

return
% run HurrSim
ML=50
for Z=[1:50]
[rho, Tp, Sp,Tml,Sml]=HyP2(Z,Z,ML,Hy)
subplot(1,2,1);
plot(sw_pden(Sml,Tml,Z,ML),-Z,'o');
end

