clear all

root='/Users/eric/Documents/IceFloat 2013/GreenEdge/CTD@Camp/Vagabond2014/';
f{1}='Vagabond-2014a.mat';
f{2}='Vagabond-2014b.mat';
f{3}='Vagabond-2014c.mat';

Pgrid=[0:0.5:300]';  % depth grid for output

k=0;l=0;
for i=1:3
    load([root f{i}])
    for j=1:length(casts)
        unstruct(casts(j),'overwrite');
        S=real(SP);  % practical salinity
        S(S<0)=0.;
                
        l=l+1;
        if l==11 | l==5
            continue
        end
        k=k+1;
        P(1)=0;
        Nin=length(P);
        P(Nin+1)=max(300,P(Nin)+1);
        S(Nin+1)=S(Nin)+0.2;
        T(Nin+1)=T(Nin);
        
        N=length(Pgrid);
        Px(k,:)=Pgrid;
        Sx(k,:)=interp1(P,S,Pgrid);
        Tx(k,:)=interp1(P,T,Pgrid);
        mtime(k)=time;
        
        sta.lat(k)=lat;
        sta.lon(k)=lon;
        DV=datevec(time);
        sta.yr(k)=DV(1);
        sta.mo(k)=DV(2);
        sta.day(k)=DV(3);
        
    end
end
S=Sx';T=Tx';P=Px';
SIG_0=sw_pden(S,T,P,0) -1000;   % potential density
SIG = sw_dens(S,T,P)-1000;         % In-situ density
TH = sw_ptmp(S,T,P,0);                % potential temperature
plot(SIG_0,-P,'-')
readme='Vagabond 2014 CTD';
save 'cleanGreen.mat' sta S T P SIG_0 SIG TH readme

figure
pcolor(1:size(P,2),-P,SIG_0);shading flat;
hold on
contour(1:size(P,2),-Pgrid,SIG_0,[0:5:20 25:0.1:27],'g');
caxis([20 27]);
colorbar
set(gca,'yscale','log')
ylim([-300 -1]);
xlabel('number')
ylabel('P/dbar');

set(gcf,'Color','w');
print('-dpng','VagabondPcolor.png');






