CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901571 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20081001085619  20090520075741                                  2B  A   APEX-SBE 3470                                                   846 @��¿�1   @����u�@.Õ�$�@a��j~��1   ARGOS   A   A   A   @�33A33Ah  A�  A���A�33B  B33B0��BD��BXffBnffB�ffB���B�ffB�33B�ffB�  B�ffBƙ�B���B�  B�  B�33B���C��CL�CffCffCffC33C�3C$��C)ffC.� C3��C8�3C=�3CB��CGL�CP�fC[ffCe33CoffCy� C�� C���C���C�s3C��fC�� C��fC��3C��3C���C��fC�� C��fC³3CǦfC̀ Cљ�C֦fCۦfC�� C���C�fCC���C���D� D�3D�3D� D� D� D�fD$��D)�fD.�3D3� D8�3D=�3DB��DG��DL�fDQ�fDV�3D[ٚD`ٚDeٚDj� DoٚDt��Dy�3D�#3D�i�D��3D��fD�  D�\�D���D���D�#3D�l�D��3D��fD�)�D�ffDک�D���D�#3D�` D�fD��fD���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A  Ad��A�ffA�33A陙B33BffB0  BD  BW��Bm��B��B�34B�  B���B�  B���B�  B�34B�fgBڙ�B䙚B���B�34CfgC�C33C33C33C  C� C$fgC)33C.L�C3fgC8� C=� CBfgCG�CP�3C[33Ce  Co33CyL�C��fC�s3C�s3C�Y�C���C��fC���C���C���C��3C���C��fC���C�Cǌ�C�ffCр C֌�Cی�C�fC�3C��C� C� C�s3D�3D�fD�fD�3D�3D�3D��D$��D)��D.�fD3�3D8�fD=�fDB� DG� DL��DQ��DV�fD[��D`��De��Dj�3Do��Dt� Dy�fD��D�c4D���D�� D��D�VgD��gD��gD��D�fgD���D�� D�#4D�` Dڣ4D��gD��D�Y�D� D�� D��411111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��;A���A��^A�A��A�ȴA�;dA�1'A앁A�r�A�$�A�n�A�7A��#A�5?A���Aܟ�A�JA���AׁAִ9A�+A�n�A�?}A�\)A�(�A�K�A�"�A�
=A��+A��hA�1A��+A�A�hsA���A��mA�  A���A��PA�(�A���A�oA���A��^A�ȴAx�HAn��AfffA`�A`jAY��AS��AQAE?}A;��A6�\A2�A.��A,JA+&�A"�DA��A��A�HA?}A��A��A@���@�r�@�33@߾w@���@��@�J@�j@ļj@�@���@�l�@�{@���@���@���@���@�  @�bN@�v�@�7L@���@�Q�@�V@�@��
@��@��7@��D@u@l�D@b^5@["�@T�/@K�@G+@>��@7�@1�@*^5@'l�@#@��@j@�-@J@r�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��;A���A��^A�A��A�ȴA�;dA�1'A앁A�r�A�$�A�n�A�7A��#A�5?A���Aܟ�A�JA���AׁAִ9A�+A�n�A�?}A�\)A�(�A�K�A�"�A�
=A��+A��hA�1A��+A�A�hsA���A��mA�  A���A��PA�(�A���A�oA���A��^A�ȴAx�HAn��AfffA`�A`jAY��AS��AQAE?}A;��A6�\A2�A.��A,JA+&�A"�DA��A��A�HA?}A��A��A@���@�r�@�33@߾w@���@��@�J@�j@ļj@�@���@�l�@�{@���@���@���@���@�  @�bN@�v�@�7L@���@�Q�@�V@�@��
@��@��7@��D@u@l�D@b^5@["�@T�/@K�@G+@>��@7�@1�@*^5@'l�@#@��@j@�-@J@r�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
D�B
C�B
A�B
<jB
33B
33B
�7B
�!B
�TB
�mB�BP�Bl�BbNBYBffBu�B�JB��B�RB�jB��B�BB��B%B�B$�B1'BQ�BL�BN�BL�B=qB.B �BPB��B�BBƨB�oBm�B<jB�B
�B
�dB
��B
q�B
'�B
1B	�B	�B	ɺB	�!B	��B	q�B	R�B	H�B	C�B	N�B	ZB	ZB	L�B	�B	�B	DB��B��B��B	B��B	�B	49B	I�B	R�B	]/B	o�B	t�B	�JB	��B	��B	�3B	�!B	B	�)B	�fB	�`B	�yB	�B	�B	��B	��B
B
B

=B
JB
�B
 �B
(�B
-B
33B
:^B
=qB
A�B
G�B
J�B
R�B
VB
ZB
`BB
cTB
ffB
jB
m�B
r�B
u�B
w�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
D�B
C�B
A�B
<jB
33B
33B
�7B
�!B
�TB
�mB�BP�Bl�BbNBYBffBu�B�JB��B�RB�jB��B�BB��B%B�B$�B1'BQ�BL�BN�BL�B=qB.B �BPB��B�BBƨB�oBm�B<jB�B
�B
�dB
��B
q�B
'�B
1B	�B	�B	ɺB	�!B	��B	q�B	R�B	H�B	C�B	N�B	ZB	ZB	L�B	�B	�B	DB��B��B��B	B��B	�B	49B	I�B	R�B	]/B	o�B	t�B	�JB	��B	��B	�3B	�!B	B	�)B	�fB	�`B	�yB	�B	�B	��B	��B
B
B

=B
JB
�B
 �B
(�B
-B
33B
:^B
=qB
A�B
G�B
J�B
R�B
VB
ZB
`BB
cTB
ffB
jB
m�B
r�B
u�B
w�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081001085617  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081001085619  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081001085620  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081001085624  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081001085624  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081001085625  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081001090624                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081005164330  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081005164346  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081005164349  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081005164359  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081005164359  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081005164401  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081005191418                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081005164330  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090520075544  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090520075545  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090520075545  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090520075546  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090520075546  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090520075546  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090520075546  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090520075546  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090520075741                      G�O�G�O�G�O�                