CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901571 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080812125521  20090520075746                                  2B  A   APEX-SBE 3470                                                   846 @��*�1   @��]L;*@.[��S��@a�n��P1   ARGOS   A   A   A   @���A��A`  A���A�ffA�ffB	33BffB1��BF��BZffBm33B���B�ffB���B�  B���B���B���B�  BЙ�B�ffB�33BB���C��C��C� CffC33C� CL�C$� C)��C.� C3L�C8��C=L�CBffCGL�CQ��C[ffCeffCoffCy��C���C�� C��3C��fC���C��3C���C��3C��fC�� C��3C��3C���C�Cǳ3C̳3Cѳ3Cֳ3Cی�C�fC噚CꙚCC��C���DٚDٚD��D�fD�fD� D��D$� D)�fD.�fD3��D8��D=��DB��DG� DL�3DQ� DV�fD[� D`�fDe��DjٚDo� DtٚDy� D�#3D�c3D��fD��3D�  D�l�D���D�� D�)�D�p D�� D��D�&fD�l�Dک�D��3D�#3D�P D�fD�� D�` 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A  A^ffA���A���A陙B��B  B134BFfgBZ  Bl��B���B�33B�fgB���B�fgB�fgB���B���B�fgB�33B�  B�fgB���C� C� CffCL�C�CffC33C$ffC)� C.ffC333C8� C=33CBL�CG33CQ� C[L�CeL�CoL�Cy� C���C�s3C��fC���C���C��fC���C��fC���C��3C��fC��fC�� C�CǦfC̦fCѦfC֦fCۀ C���C��C��C��C� C���D�4D�4D�gD� D� DٚD�gD$ٚD)� D.� D3�4D8�gD=�gDB�gDG��DL��DQ��DV� D[��D`� De�gDj�4DoٚDt�4DyٚD�  D�` D��3D�� D��D�i�D���D���D�&gD�l�D���D��gD�#3D�i�DڦgD�� D�  D�L�D�3D���D�\�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�AA�uA�PA�A�~�A�  A�M�A�E�A�5?A�x�A��`A��;A�ZA���A�ȴA�{A�9XA�=qA�1A�1'Aם�A�v�AҴ9A�5?A�jA�l�Aʝ�A�\)A�\)A�`BA�E�A�bNA��A���A¥�A�x�A�+A�p�A�ȴA�JA��A�K�A�x�A��A���A��TA�dZA�p�A|�Ar��Aj{Ac%AS�hAN�DAL  AEO�A;p�A3`BA-�-A(E�A-A�`A�7A%A~�A	��A��A I�@���@�K�@�X@�j@���@�1'@�r�@��H@��@��9@�o@�J@��@�+@�@���@��@��+@��@��@�p�@��^@��!@�
=@�;d@�&�@�J@��@�Ĝ@w�@o��@e`B@]��@RJ@L(�@H  @?l�@9X@4I�@,1@%@ 1'@�H@Z@bN@V@ƨ@�
11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111AA�uA�PA�A�~�A�  A�M�A�E�A�5?A�x�A��`A��;A�ZA���A�ȴA�{A�9XA�=qA�1A�1'Aם�A�v�AҴ9A�5?A�jA�l�Aʝ�A�\)A�\)A�`BA�E�A�bNA��A���A¥�A�x�A�+A�p�A�ȴA�JA��A�K�A�x�A��A���A��TA�dZA�p�A|�Ar��Aj{Ac%AS�hAN�DAL  AEO�A;p�A3`BA-�-A(E�A-A�`A�7A%A~�A	��A��A I�@���@�K�@�X@�j@���@�1'@�r�@��H@��@��9@�o@�J@��@�+@�@���@��@��+@��@��@�p�@��^@��!@�
=@�;d@�&�@�J@��@�Ĝ@w�@o��@e`B@]��@RJ@L(�@H  @?l�@9X@4I�@,1@%@ 1'@�H@Z@bN@V@ƨ@�
11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�+B
�+B
�+B
�%B
�B
y�B
k�B
l�B
l�B
��B
��B
�)BB
��B
��BVB1'B?}BS�BQ�B`BBk�Bm�Bw�B�=B�VB��BȴB��B��B�#B�yB�B%BuB{B�B�BhB
=B��B�B}�B^5BB
�;B
�FB
�B
dZB
6FB
bB	�B	�B	��B	�=B	o�B	O�B	49B	�B	#�B�mB�B�TB��B��B��B��B�
B��B	B	+B	2-B	33B	8RB	@�B	p�B	�B	��B	��B	�-B	�FB	ĜB	��B	��B	�
B	�BB	�B	��B	��B
B
	7B
\B
{B
�B
�B
!�B
%�B
+B
/B
7LB
:^B
B�B
E�B
G�B
M�B
Q�B
T�B
ZB
_;B
cTB
jB
o�B
s�B
v�B
x�B
x�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�+B
�+B
�+B
�%B
�B
y�B
k�B
l�B
l�B
��B
��B
�)BB
��B
��BVB1'B?}BS�BQ�B`BBk�Bm�Bw�B�=B�VB��BȴB��B��B�#B�yB�B%BuB{B�B�BhB
=B��B�B}�B^5BB
�;B
�FB
�B
dZB
6FB
bB	�B	�B	��B	�=B	o�B	O�B	49B	�B	#�B�mB�B�TB��B��B��B��B�
B��B	B	+B	2-B	33B	8RB	@�B	p�B	�B	��B	��B	�-B	�FB	ĜB	��B	��B	�
B	�BB	�B	��B	��B
B
	7B
\B
{B
�B
�B
!�B
%�B
+B
/B
7LB
:^B
B�B
E�B
G�B
M�B
Q�B
T�B
ZB
_;B
cTB
jB
o�B
s�B
v�B
x�B
x�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080812125518  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080812125521  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080812125521  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080812125525  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080812125526  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080812125526  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080812130210                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080816165759  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080816165814  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080816165818  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080816165826  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080816165826  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080816165828  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080816204931                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080816165759  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090520075531  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090520075532  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090520075532  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090520075533  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090520075533  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090520075533  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090520075533  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090520075534  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090520075746                      G�O�G�O�G�O�                