CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2900726 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080914022435  20090422025052                                  2B  A   APEX-SBE 3464                                                   846 @��)A��1   @��-oz�@:Լj~��@b01&�y1   ARGOS   A   A   A   @���A��A`  A���A���A�  B
��B��B1��BE33BY��Bm��B���B�ffB���B���B�33B�33B���B�33BЙ�B���B�  B�  B���C  C�fC
�fC  CffC�C�fC$ffC)33C.  C2��C8L�C=ffCB� CGffCQ� C[33Ce� Co� Cy  C���C�� C��3C���C���C�� C�� C��fC�ffC�s3C�ffC�Y�C���C�Cǳ3C̳3Cѳ3C֙�C۳3C���C�s3C�3C�ffC��fC�Y�DٚD� DٚD�3DٚD��D�fD$�3D)� D.� D3� D8� D=�3DB��DG�3DL�3DQ�3DV��D[�3D`��DeٚDj� Do�fDtٚDy��D�)�D�\�D���D��fD�,�D�c3D���D��D�#3D�\�D��3D��3D�&fD�c3Dڰ D��fD�  D�Y�D�D��3D��311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�  A34AY��A�fgA���A���B	33B33B0  BC��BX  Bl  B�  B���B�  B�  B�ffB�ffB�  B�ffB���B�  B�33B�33B���C ��C� C
� C��C  C�4C� C$  C(��C-��C2fgC7�gC=  CB�CG  CQ�CZ��Ce�Co�Cx��C�fgC�L�C�� C�fgC�fgC�L�C�L�C�s3C�33C�@ C�33C�&gC�fgC�fgCǀ C̀ Cр C�fgCۀ C�Y�C�@ C� C�33C�s3C�&gD� D�fD� D��D� D�3D��D$��D)�fD.�fD3�fD8�fD=��DB�3DG��DL��DQ��DV�3D[��D`� De� Dj�fDo��Dt� Dy�3D��D�P D���D�ٙD�  D�VfD���D���D�fD�P D��fD��fD��D�VfDڣ3D�ٙD�3D�L�D��D��fD��f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��mA��A��A�RA�+A���A�{AȲ-A�K�A��uA��A�|�A�;dA�=qA�(�A��A�VA��A��A��RA�Q�A�`BA��A��wA��A��A��hA�~�A��A�=qA���A���A�G�A��A��\A�?}A��A��TA�^5A��9A��jA�7LA�oA�r�A��A���A�
=A��TA��#A���A�bNA��A��/A� �A�A�A�K�A|��Au/Ao�Ai/AfbAadZA\5?AV{ARffANffAJ�!AE��AC��A@A5�FA-�TA&bA�DA(�A	O�A��@���@�  @��@�"�@�@��H@�|�@�G�@��@���@��\@���@�=q@���@��!@}/@sC�@o��@e�h@Zn�@R��@L��@Dj@=�-@8�9@/�w@*�\@#S�@��@��@�m@�@�@|�@��@G�?��D?��P?��/11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��mA��A��A�RA�+A���A�{AȲ-A�K�A��uA��A�|�A�;dA�=qA�(�A��A�VA��A��A��RA�Q�A�`BA��A��wA��A��A��hA�~�A��A�=qA���A���A�G�A��A��\A�?}A��A��TA�^5A��9A��jA�7LA�oA�r�A��A���A�
=A��TA��#A���A�bNA��A��/A� �A�A�A�K�A|��Au/Ao�Ai/AfbAadZA\5?AV{ARffANffAJ�!AE��AC��A@A5�FA-�TA&bA�DA(�A	O�A��@���@�  @��@�"�@�@��H@�|�@�G�@��@���@��\@���@�=q@���@��!@}/@sC�@o��@e�h@Zn�@R��@L��@Dj@=�-@8�9@/�w@*�\@#S�@��@��@�m@�@�@|�@��@G�?��D?��P?��/11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B
�B
.B
R�B
�\B
��B
�3B1B2-BI�B�B0!Bn�B:^Be`BbNBm�BO�BcTBP�BO�B=qBA�BB�B=qB?}B=qB1'B5?B5?B/B/B0!B.B-B)�B(�B&�B �B�B�BuBVBJBBB
��B
�B
�NB
�/B
��B
ÖB
�jB
�?B
��B
�uB
m�B
Q�B
=qB
.B
�B
B	�B	�B	��B	�9B	��B	�uB	�%B	A�B	 �B	bB�BŢB�oB�JB�Bk�B_;BJ�B5?B@�BL�BbNBy�B��B�B�jB��B�yB	+B	�B	%�B	6FB	P�B	{�B	�\B	��B	�FB	ɺB	�;B	�B	��B
PB
�B
'�B
5?B
>wB
I�B
Q�B
ZB
`BB
hsB
l�B
p�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	��B	��B
�B
.B
R�B
�\B
��B
�3B1B2-BI�B�B0!Bn�B:^Be`BbNBm�BO�BcTBP�BO�B=qBA�BB�B=qB?}B=qB1'B5?B5?B/B/B0!B.B-B)�B(�B&�B �B�B�BuBVBJBBB
��B
�B
�NB
�/B
��B
ÖB
�jB
�?B
��B
�uB
m�B
Q�B
=qB
.B
�B
B	�B	�B	��B	�9B	��B	�uB	�%B	A�B	 �B	bB�BŢB�oB�JB�Bk�B_;BJ�B5?B@�BL�BbNBy�B��B�B�jB��B�yB	+B	�B	%�B	6FB	P�B	{�B	�\B	��B	�FB	ɺB	�;B	�B	��B
PB
�B
'�B
5?B
>wB
I�B
Q�B
ZB
`BB
hsB
l�B
p�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080914022414  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080914022435  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080914022440  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080914022451  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080914022452  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080914022453  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080914031451                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080918050841  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080918050846  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080918050848  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080918050852  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080918050852  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080918050853  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080918060251                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080918050841  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422024536  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422024537  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422024537  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422024538  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422024538  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422024538  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422024538  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422024538  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422025052                      G�O�G�O�G�O�                