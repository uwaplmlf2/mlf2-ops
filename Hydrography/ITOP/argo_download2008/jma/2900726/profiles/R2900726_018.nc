CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   p   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4\   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C<   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E,   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H,   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K,   CALIBRATION_DATE            	             
_FillValue                  ,  N,   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    NX   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N\   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N`   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Nd   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Nh   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  2900726 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080816050026  20090422025052                                  2B  A   APEX-SBE 3464                                                   846 @��So�1   @�詏+�e@;�~��"�@bP�\)1   ARGOS   B   B   B   @���A��Al��A���A���A�33B  B��B.ffBDffBV��Bm33B�33B�  B�  G�O�B�ffBڙ�B���B�ffB���CffCL�C33CL�CL�C�3C��C#�fC(��C.L�C3  C8� C=� CBL�CGffCQL�C[33CeffCoL�CyL�C��3C���C���C��3C�� C�s3C�� C���C���C�L�C�@ C�Y�C�ffC�s3Cǀ C̦fC���C�� C�� C�� C�s3C� C�L�C�s3C��fD�fD�3DٚD�3D��DٚD��D$�3D)ٚD.��D3��D8��D=��DB�fDG�3DLٚDQ�fDV�fD[�3D`�fDe��Dj� Do�fDtٚDy��D�)�D�i�D���D���D�&fD�i�D���D�� D��D�VfD��fD��D�&fD�Y�DڦfD���D�)�D�\�D�D�ٚD��f1111111111111114111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�34AfgAi��A�33A�33A陙B33B  B-��BC��BV  BlffB��B���B���G�O�B�  B�34B�fgB�  B�fgC33C�C  C�C�C� C��C#�3C(��C.�C2��C8L�C=L�CB�CG33CQ�C[  Ce33Co�Cy�C���C�� C�� C���C�ffC�Y�C�ffC�s3C�� C�33C�&fC�@ C�L�C�Y�C�ffČ�Cѳ3C֦fCۦfC�fC�Y�C�ffC�33C�Y�C���D��D�fD��D�fD� D��D� D$�fD)��D.� D3� D8� D=��DB��DG�fDL��DQ��DV��D[�fD`��De� Dj�3Do��Dt��Dy��D�#4D�c4D��gD��gD�  D�c4D��gD�ٚD�gD�P D�� D��4D�  D�S4Dڠ D��gD�#4D�VgD�4D��4D�� 1111111111111114111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�7A�A���A��PA��A�M�A���A�jA�5?A�\)A�G�A��7A��uA���G�O�A���A�bA�1A�33A�x�A��+A�p�A��A�hsA�&�A��-A�n�A�1'A�JA��TA��!A�dZA��A��-A�O�A��A�l�A���A�t�A��/A�Q�A��wA�VA���A���A�M�A��
A�%A�\)A���A���A�n�A�;dA���A��A�-A��mA���A~z�At �Ao�mAlĜAi�Afv�AfbAd�DAc\)AbȴAaXAV��AF�A9�wA-��A$�\A�jAp�@�$�@�1@Ձ@���@���@���@���@�|�@��@��@�7L@��@��+@�V@�33@{t�@ihs@Y��@PbN@I7L@@��@;o@4�@+dZ@%��@   @��@�h@n�@@	��@|�@j@��?��-?���1111111111111194111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�7A�A���A��PA��A�M�A���A�jA�5?A�\)A�G�A��7A��uA���G�O�A���A�bA�1A�33A�x�A��+A�p�A��A�hsA�&�A��-A�n�A�1'A�JA��TA��!A�dZA��A��-A�O�A��A�l�A���A�t�A��/A�Q�A��wA�VA���A���A�M�A��
A�%A�\)A���A���A�n�A�;dA���A��A�-A��mA���A~z�At �Ao�mAlĜAi�Afv�AfbAd�DAc\)AbȴAaXAV��AF�A9�wA-��A$�\A�jAp�@�$�@�1@Ձ@���@���@���@���@�|�@��@��@�7L@��@��+@�V@�33@{t�@ihs@Y��@PbN@I7L@@��@;o@4�@+dZ@%��@   @��@�h@n�@@	��@|�@j@��?��-?���1111111111111194111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�hB
�hB0!BE�Bt�Bl�BcTB[#BO�BI�BH�BD�BB�B@�G�O�B?}B=qB:^B7LB49B1'B-B+B(�B(�B(�B)�B+B+B+B)�B'�B%�B#�B!�B�B�B�B�B�B{BuBoBVBJB
=B+B  B
��B
�B
�B
�B
�B
�yB
�mB
�ZB
�BB
��B
��B
p�B
^5B
N�B
VB
[#B
[#B
aHB
gmB
e`B
YB	�B	��B	cTB	-B	B�B~�BM�B>wB0!B,B2-B?}BF�BffBy�B�bB�B�qB��B�yB�B	bB	A�B	o�B	��B	�9B	ɺB	�B	�yB	��B
B
�B
#�B
.B
49B
>wB
H�B
M�B
VB
\)B
bNB
hs1111111111111194111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�hB
�hB0!BE�Bt�Bl�BcTB[#BO�BI�BH�BD�BB�B@�G�O�B?}B=qB:^B7LB49B1'B-B+B(�B(�B(�B)�B+B+B+B)�B'�B%�B#�B!�B�B�B�B�B�B{BuBoBVBJB
=B+B  B
��B
�B
�B
�B
�B
�yB
�mB
�ZB
�BB
��B
��B
p�B
^5B
N�B
VB
[#B
[#B
aHB
gmB
e`B
YB	�B	��B	cTB	-B	B�B~�BM�B>wB0!B,B2-B?}BF�BffBy�B�bB�B�qB��B�yB�B	bB	A�B	o�B	��B	�9B	ɺB	�B	�yB	��B
B
�B
#�B
.B
49B
>wB
H�B
M�B
VB
\)B
bNB
hs1111111111111194111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080816050024  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080816050026  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080816050027  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080816050031  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080816050031  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080816050032  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080816050032  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080816051405                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080819052653  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080819052659  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080819052700  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080819052705  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080819052705  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080819052705  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080819052706  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080819054641                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080819052653  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422024529  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422024529  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422024529  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422024531  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090422024531  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422024531  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422024531  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422024531  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422024531  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422024531  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422025052                      G�O�G�O�G�O�                