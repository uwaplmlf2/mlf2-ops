CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900726 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080825051148  20090422025052                                  2B  A   APEX-SBE 3464                                                   846 @��2d��1   @��2��6�@;V�t�@bG��-V1   ARGOS   A   A   A   @���A  Ah  A�  A���A�ffB��BffB2��BF  BY��Bn��B�33B�33B���B�  B���B���B���Bř�BЙ�B�  B�ffB�  B�  C �fC� C
��C�3C�fC�C33C$� C)33C.ffC3ffC8�C=  CBL�CG33CQL�CZ�3Ce  CoffCy�C��3C���C�� C���C��fC���C���C�ffC�� C�� C��fC�ffC�� C�ffCǀ Č�C���C֦fC۳3C���C�3C�fC�ffC��C���D��D�fD�3D�fD��D��D�3D$�3D)��D.�3D3�3D8�fD=�3DBٚDGٚDLٚDQ� DV��D[� D`� De��Dj�fDo��Dt� Dy�3D�3D�Y�D�� D��D�)�D�ffD���D��fD�&fD�` D�� D���D�  D�` Dڬ�D��D��D�VfD�fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA��Ad��A�ffA�  A���B
��B��B2  BE33BX��Bn  B���B���B�34B���B�fgB�fgB�34B�34B�34Bؙ�B�  B홚B���C �3CL�C
��C� C�3C�gC  C$L�C)  C.33C333C7�gC<��CB�CG  CQ�CZ� Cd��Co33Cx�gC���C��3C��fC�� C���C�s3C�� C�L�C�ffC�ffC���C�L�C�ffC�L�C�ffC�s3Cѳ3C֌�Cۙ�C�� C噙C��C�L�C�s3C�s3D� D��D�fD��D��D��D�fD$�fD)� D.�fD3�fD8��D=�fDB��DG��DL��DQ�3DV� D[�3D`�3De� DjٙDo� Dt�3Dy�fD��D�S4D���D��4D�#4D�` D��gD�� D�  D�Y�D���D��gD��D�Y�DڦgD��4D�gD�P D� D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A���A��A�=qA�{A˲-A�n�A��-A���A���A���A�A�A���A�1A�A�A��A���A���A��HA�ZA�9XA���A��A�=qA�?}A��9A���A��jA� �A���A���A�`BA�1'A�%A��A��A�t�A�oA��\A��A�z�A���A�I�A��-A���A��DA���A�/A�+A�`BA��^A���A�S�A�  A���A�z�A�G�A��mA�?}A�7LA�bAƨAy�TAu��Aq�Amx�Ai;dAe&�AaAV(�AI�AA|�A6E�A-dZA!dZA5?A
�`@���@�9@�x�@�z�@�bN@��@��j@��9@��@�/@�bN@��!@��H@�M�@��w@�`B@yhs@jn�@_K�@R^5@J�H@A�@9hs@3o@,�/@&�R@"^5@�-@��@
=@^5@��@
��@5?@-?��?�7L1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A���A��A�=qA�{A˲-A�n�A��-A���A���A���A�A�A���A�1A�A�A��A���A���A��HA�ZA�9XA���A��A�=qA�?}A��9A���A��jA� �A���A���A�`BA�1'A�%A��A��A�t�A�oA��\A��A�z�A���A�I�A��-A���A��DA���A�/A�+A�`BA��^A���A�S�A�  A���A�z�A�G�A��mA�?}A�7LA�bAƨAy�TAu��Aq�Amx�Ai;dAe&�AaAV(�AI�AA|�A6E�A-dZA!dZA5?A
�`@���@�9@�x�@�z�@�bN@��@��j@��9@��@�/@�bN@��!@��H@�M�@��w@�`B@yhs@jn�@_K�@R^5@J�H@A�@9hs@3o@,�/@&�R@"^5@�-@��@
=@^5@��@
��@5?@-?��?�7L1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�1B	�DB	�B	�}B
D�B
��B
��B
�
B	7B�B,B/B1'B1'B2-B,B$�B'�B1'B9XB5?B0!B1'B1'B/B33B+B'�B(�B%�B$�B%�B&�B)�B-B,B+B(�B$�B!�B�B�B�B�BuBVBhBoBDB
��B
��B
�B
�B
�B
�B
�B
�mB
�fB
�NB
�/B
��B
ŢB
�B
�PB
x�B
cTB
T�B
>wB
(�B
{B	�BB	��B	~�B	L�B	&�B�B��B�VBe`BH�B;dB/B+B49BA�BK�BbNBp�B�+B��B�RB��B�B��B	�B	?}B	_;B	�B	��B	ĜB	�B	�ZB	��B
B
\B
�B
"�B
.B
;dB
D�B
K�B
VB
`BB
e`B
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�1B	�DB	�B	�}B
D�B
��B
��B
�
B	7B�B,B/B1'B1'B2-B,B$�B'�B1'B9XB5?B0!B1'B1'B/B33B+B'�B(�B%�B$�B%�B&�B)�B-B,B+B(�B$�B!�B�B�B�B�BuBVBhBoBDB
��B
��B
�B
�B
�B
�B
�B
�mB
�fB
�NB
�/B
��B
ŢB
�B
�PB
x�B
cTB
T�B
>wB
(�B
{B	�BB	��B	~�B	L�B	&�B�B��B�VBe`BH�B;dB/B+B49BA�BK�BbNBp�B�+B��B�RB��B�B��B	�B	?}B	_;B	�B	��B	ĜB	�B	�ZB	��B
B
\B
�B
"�B
.B
;dB
D�B
K�B
VB
`BB
e`B
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080825051144  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080825051148  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080825051149  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080825051152  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080825051153  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080825051153  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080825052635                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080829053413  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080829053419  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080829053421  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080829053429  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080829053429  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080829053430  QCP$                G�O�G�O�G�O�           10000JA  ARFMdecpA9_b                                                                20080829053413  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422024531  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422024532  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422024532  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422024533  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422024533  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422024533  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422024533  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422024533  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422025052                      G�O�G�O�G�O�                