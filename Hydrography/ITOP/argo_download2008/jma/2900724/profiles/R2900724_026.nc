CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2900724 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20081014130014  20090422023742                                  2B  A   APEX-SBE 3463                                                   846 @�����
=1   @�����j�@:�|�hs@`x��E�1   ARGOS   A   A   A   @�  AffAh  A���A���A�33B33BffB2  BD��BZffBnffB�ffB�  B�33B�ffB���B�33B�ffB�33B�33B�ffB�  B�ffB�33C� C� C��CffC  CffCL�C$33C)�C.L�C3L�C833C=L�CBffCG� CQ� C[�Ce  CoL�Cy33C�� C���C���C���C��fC���C���C��fC��3C���C�� C��fC�� C�s3Cǳ3C̳3C�� C֦fCی�C�3C��C�fC�3C�� C�� D�fD�fDٚD��D� D��D��D$�3D)�3D.�3D3ٚD8�fD=��DBٚDGٚDL� DQٚDVٚD[� D`� De�3Dj�fDo� Dt� Dy�3D�&fD�ffD���D�� D�#3D�Y�D���D���D�&fD�c3D��3D��fD��D�l�DڦfD��D�#3D�\�D��D��fD���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�  AffAh  A���A���A�33B33BffB2  BD��BZffBnffB�ffB�  B�33B�ffB���B�33B�ffB�33B�33B�ffB�  B�ffB�33C� C� C��CffC  CffCL�C$33C)�C.L�C3L�C833C=L�CBffCG� CQ� C[�Ce  CoL�Cy33C�� C���C���C���C��fC���C���C��fC��3C���C�� C��fC�� C�s3Cǳ3C̳3C�� C֦fCی�C�3C��C�fC�3C�� C�� D�fD�fDٚD��D� D��D��D$�3D)�3D.�3D3ٚD8�fD=��DBٚDGٚDL� DQٚDVٚD[� D`� De�3Dj�fDo� Dt� Dy�3D�&fD�ffD���D�� D�#3D�Y�D���D���D�&fD�c3D��3D��fD��D�l�DڦfD��D�#3D�\�D��D��fD���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�AᙚA��HA�=qA�(�A�$�A��A϶FA�bA�~�A���A���A���A���A�jA�ffA�VA�A��RA��uA���A��A�t�A�bA��A��A�`BA��^A��RA�XA��#A��A��A�A�A�%A�A�K�A���A���A��!A��A��FA���A�x�A��A�-A�$�A��A�A�A�l�A���A��/A�r�A���A��Az��Ax�yAvĜAs��Al�AjQ�Ac;dAa�A[�
AUdZAR��AO�ALn�AG�AC��A?�wA5G�A,I�A!oA��At�A
^5Ap�@��
@���@�V@�x�@�+@�33@�r�@�x�@���@���@�7L@�|�@���@�$�@�@��/@� �@��;@{�@h�`@\�@R^5@H �@=�@8r�@0b@*^5@$�@�y@~�@ff@�`@V@	��@��@j@7L?�V?�7L11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111AᙚA��HA�=qA�(�A�$�A��A϶FA�bA�~�A���A���A���A���A�jA�ffA�VA�A��RA��uA���A��A�t�A�bA��A��A�`BA��^A��RA�XA��#A��A��A�A�A�%A�A�K�A���A���A��!A��A��FA���A�x�A��A�-A�$�A��A�A�A�l�A���A��/A�r�A���A��Az��Ax�yAvĜAs��Al�AjQ�Ac;dAa�A[�
AUdZAR��AO�ALn�AG�AC��A?�wA5G�A,I�A!oA��At�A
^5Ap�@��
@���@�V@�x�@�+@�33@�r�@�x�@���@���@�7L@�|�@���@�$�@�@��/@� �@��;@{�@h�`@\�@R^5@H �@=�@8r�@0b@*^5@$�@�y@~�@ff@�`@V@	��@��@j@7L?�V?�7L11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	0!B	S�B	�uB	��B	��B	��B
VB
R�B
cTB
z�B
�hB
�^B
ƨB1B�BhB
�B
��B+BhB�B�B%�B%�B49B49B8RB;dB;dB;dB<jB=qB<jB;dB:^B9XB8RB6FB49B2-B-B(�B!�B�B�B\B1BB
��B
�B
�5B
��B
�LB
��B
�uB
�=B
|�B
m�B
M�B
C�B
 �B
�B	��B	�BB	�B	��B	�RB	��B	�hB	�7B	XB	7LB	\B��B�HB��BÖB�XB�?B�RBBŢB��B��B�HB��B	&�B	-B	D�B	N�B	ZB	ffB	hsB	w�B	q�B	��B	�B	�jB	ȴB	�B	�B	��B
B
hB
�B
%�B
0!B
9XB
E�B
M�B
VB
]/B
bNB
iyB
o�B
s�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	0!B	S�B	�uB	��B	��B	��B
VB
R�B
cTB
z�B
�hB
�^B
ƨB1B�BhB
�B
��B+BhB�B�B%�B%�B49B49B8RB;dB;dB;dB<jB=qB<jB;dB:^B9XB8RB6FB49B2-B-B(�B!�B�B�B\B1BB
��B
�B
�5B
��B
�LB
��B
�uB
�=B
|�B
m�B
M�B
C�B
 �B
�B	��B	�BB	�B	��B	�RB	��B	�hB	�7B	XB	7LB	\B��B�HB��BÖB�XB�?B�RBBŢB��B��B�HB��B	&�B	-B	D�B	N�B	ZB	ffB	hsB	w�B	q�B	��B	�B	�jB	ȴB	�B	�B	��B
B
hB
�B
%�B
0!B
9XB
E�B
M�B
VB
]/B
bNB
iyB
o�B
s�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081014130012  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081014130014  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081014130015  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081014130019  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081014130019  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081014130020  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081014131205                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081018044427  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081018044431  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081018044431  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081018044435  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081018044436  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081018044436  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081018062358                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081018044427  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422021749  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422021750  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422021750  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422021751  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422021751  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422021751  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422021751  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422021751  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422023742                      G�O�G�O�G�O�                