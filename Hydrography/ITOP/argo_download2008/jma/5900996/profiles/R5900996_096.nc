CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900996 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               `A   JA  20080820125304  20090518085341                                  2B  A   2296                                                            846 @�� ���1   @��D�[g@5���n�@`����+1   ARGOS   A   A   A   @�ffA��AfffA�  A�33A�  B
ffB��B2��BF  BY��Bm��B���B�ffB���B���B���B���B���B�  B�33Bڙ�B�  B�  B���C�C33C�C��CffC� CffC$33C)ffC.� C3L�C833C=��CB� CG33CQ  CZ��Cd�3Cn�fCy�C�� C���C��fC���C���C�s3C�� C��fC���C��3C��fC��3C��fC�CǙ�C̦fCь�C���Cۙ�C���C�fC��C�ffC� C���D��D�3DٚDٚD�3DٚDٚD$��D)�fD.�3D3�fD8� D=��DB� DG� DL��DQ�fDV��D[�3D`��De�fDjٚDo�fDt� Dy� D�#3D�ffD�� D�� D��D�l�D���D��D�#3D�\�D��3D���D��D�l�Dڬ�D�� D�  D�VfD�3D�#31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA	��A^ffA�  A�33A�  BffB��B0��BD  BW��Bk��B���B�ffB���B���B���B���B���B�  B�33Bٙ�B�  B�  B���C ��C�3C
��CL�C�fC  C�fC#�3C(�fC.  C2��C7�3C=�CB  CF�3CP� CZL�Cd33CnffCx��C�� C�Y�C�ffC�L�C�L�C�33C�@ C�ffC�L�C�s3C�ffC�s3C�ffC�L�C�Y�C�ffC�L�C֌�C�Y�C�L�C�ffC�L�C�&fC�@ C�Y�D��D�3D��D��D�3D��D��D$��D)�fD.�3D3�fD8� D=��DB� DG� DL��DQ�fDV��D[�3D`��De�fDj��Do�fDt� Dy� D�3D�VfD�� D�� D�	�D�\�D���D�ٚD�3D�L�D��3D���D��D�\�Dڜ�D�� D� D�FfD�3D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A��A�ȴA��yA�oA��A�~�A�A�t�AڋDAа!A˅A��
A���A��
A�  A�x�A���A�/A��TA��uA�  A��\A�z�A���A���A�n�A���A�+A�t�A���A��-A��+A��A�K�A��
A�l�A��
A���A�1'A�ZA���A��A�O�A���A�/A��uA���A��\A}S�Av�/At�Ao�mAj(�Ac��A^�/A\M�AX��AU|�AOG�AH�AA��A<��A7VA1O�A,ZA'�A#��A n�A�
A��A�A-@���@���@��#@ͩ�@���@�x�@���@���@��@�
=@�"�@���@�5?@�M�@���@�t�@�"�@�;d@�j@���@�X@�b@u?}@i��@`  @Xr�@IX@B��@9&�@1�^@,(�@%�h@ �u@�-@��@9X@�7@?}@
~�@�@V@o1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A��A�ȴA��yA�oA��A�~�A�A�t�AڋDAа!A˅A��
A���A��
A�  A�x�A���A�/A��TA��uA�  A��\A�z�A���A���A�n�A���A�+A�t�A���A��-A��+A��A�K�A��
A�l�A��
A���A�1'A�ZA���A��A�O�A���A�/A��uA���A��\A}S�Av�/At�Ao�mAj(�Ac��A^�/A\M�AX��AU|�AOG�AH�AA��A<��A7VA1O�A,ZA'�A#��A n�A�
A��A�A-@���@���@��#@ͩ�@���@�x�@���@���@��@�
=@�"�@���@�5?@�M�@���@�t�@�"�@�;d@�j@���@�X@�b@u?}@i��@`  @Xr�@IX@B��@9&�@1�^@,(�@%�h@ �u@�-@��@9X@�7@?}@
~�@�@V@o1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B��B��Bz�B�hB��B	A�B	B�B	W
B	��B	�fB
"�B
�{B
�}B
�)B
�B+BuB �B?}B^5Br�Bx�B��B��B�{B�oB��B��B��B�JB�Bu�Bk�BffB_;B[#BVBR�BO�BD�B@�B:^B0!B"�B+B
��B
�B
��B
�bB
p�B
aHB
H�B
1'B
%B	�B	�HB	�
B	��B	�jB	�bB	s�B	^5B	F�B	F�B	7LB	%�B	�B	JB��B�/BȴB�3B�B�{B��B��B��B�B�wB�B�ZB��B		7B	�B	"�B	/B	D�B	W
B	cTB	|�B	��B	�B	B	��B	�HB	�B
	7B
�B
{B
 �B
.B
;dB
F�B
P�B
YB
^5B
ffB
l�B
p�B
t�B
x�B
{�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B��B��Bz�B�hB��B	A�B	B�B	W
B	��B	�fB
"�B
�{B
�}B
�)B
�B+BuB �B?}B^5Br�Bx�B��B��B�{B�oB��B��B��B�JB�Bu�Bk�BffB_;B[#BVBR�BO�BD�B@�B:^B0!B"�B+B
��B
�B
��B
�bB
p�B
aHB
H�B
1'B
%B	�B	�HB	�
B	��B	�jB	�bB	s�B	^5B	F�B	F�B	7LB	%�B	�B	JB��B�/BȴB�3B�B�{B��B��B��B�B�wB�B�ZB��B		7B	�B	"�B	/B	D�B	W
B	cTB	|�B	��B	�B	B	��B	�HB	�B
	7B
�B
{B
 �B
.B
;dB
F�B
P�B
YB
^5B
ffB
l�B
p�B
t�B
x�B
{�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080820125302  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080820125304  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080820125304  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080820125305  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080820125309  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080820125309  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080820125309  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080820125309  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080820125309  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080820130405                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080824042500  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080824042512  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080824042514  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080824042515  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080824042522  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080824042522  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080824042522  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080824042522  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080824042523  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080824063836                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080824042500  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518085120  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518085120  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090518085120  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518085120  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518085122  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518085122  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518085122  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518085122  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518085122  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518085341                      G�O�G�O�G�O�                