CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900996 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               bA   JA  20080909185328  20090518085349                                  2B  A   2296                                                            846 @�� ����1   @�� ���@5�A�7K�@a����1   ARGOS   A   A   A   @���A33A`  A���A���A홚B
ffB��B2  BF��BY��BnffB�ffB���B�33B���B���B�ffB���B�ffB�33Bڙ�B�33B�  B�  CffC�fC  C��C33C�fC��C$�C)L�C.33C3ffC8L�C=ffCBffCG�CQL�C[  Ce�Co� CyffC��3C�� C���C��3C���C�Y�C���C�ffC�� C���C���C�� C�� C�Cǳ3Č�Cь�C֙�Cۀ C�� C�3C��CC�s3C��fD��D� D� D��D��D�3D�fD$��D)�3D.��D3� D8��D=�fDBٚDG� DL�fDQٚDV��D[�3D`� De��Dj� Do��Dt��Dy�3D�&fD�` D�� D��3D�&fD�i�D���D��fD�  D�Y�D���D���D�  D�c3DڦfD���D�&fD�ffD�3D�l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33AX  A���A���A陚BffB��B0  BD��BW��BlffB�ffB���B�33B���B���B�ffB���B�ffB�33Bٙ�B�33B�  B�  C �fCffC
� CL�C�3CffC�C#��C(��C-�3C2�fC7��C<�fCA�fCF��CP��CZ� Cd��Co  Cx�fC�s3C�� C�Y�C�s3C�Y�C��C�Y�C�&fC�@ C���C���C�� C�� C�Y�C�s3C�L�C�L�C�Y�C�@ C�@ C�s3C�L�C�Y�C�33C�ffD��D� D� D��D��D�3D�fD$��D)�3D.��D3� D8��D=�fDB��DG� DL�fDQ��DV��D[�3D`� De��Dj� Do��Dt��Dy�3D�fD�P D�� D��3D�fD�Y�D���D��fD� D�I�D���D���D� D�S3DږfD���D�fD�VfD�3D�\�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��#A���A�!A�A�7A��A�Q�A�S�A�G�A���A�7LA˕�A�7LA�%A�~�A�x�A���A�Q�A��A�bA�x�A��A�
=A�l�A�9XA�
=A���A�n�A�ffA���A���A�1'A���A��
A��A�  A���A�l�A��A�XA��7A���A�C�A�|�A��hA�A�
=A�ĜA���A��A��A~��A{VAx�yAp�Ai�PAc`BA\ȴAY%AS�mANZAM�AH9XAD�\A=�A7?}A2��A,��A'�-A!&�A(�AZA��A�@���@�K�@�dZ@�7L@�1'@���@�z�@�K�@���@��^@��h@���@�r�@��P@���@���@��@�/@}��@z�\@w��@j�H@^�y@U��@K"�@D1@A7L@<�@5p�@0�@.@(�9@!�#@z�@A�@S�@b@{@
��@��@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��#A���A�!A�A�7A��A�Q�A�S�A�G�A���A�7LA˕�A�7LA�%A�~�A�x�A���A�Q�A��A�bA�x�A��A�
=A�l�A�9XA�
=A���A�n�A�ffA���A���A�1'A���A��
A��A�  A���A�l�A��A�XA��7A���A�C�A�|�A��hA�A�
=A�ĜA���A��A��A~��A{VAx�yAp�Ai�PAc`BA\ȴAY%AS�mANZAM�AH9XAD�\A=�A7?}A2��A,��A'�-A!&�A(�AZA��A�@���@�K�@�dZ@�7L@�1'@���@�z�@�K�@���@��^@��h@���@�r�@��P@���@���@��@�/@}��@z�\@w��@j�H@^�y@U��@K"�@D1@A7L@<�@5p�@0�@.@(�9@!�#@z�@A�@S�@b@{@
��@��@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B��B��B��B�B�fB	�B	K�B	gmB	�fB
6FB
O�B
q�B
�=B
��B
�B
�B
�B
�BBB.BL�B�'BĜB�RB��B��B��B�B��B��B��B��B��B��B�+B� By�Bm�BaHBN�BB�B1'B$�B�B1B
��B
�sB
��B
��B
�B
��B
�JB
aHB
>wB
#�B
  B	�B	�
B	ŢB	�wB	��B	��B	}�B	^5B	L�B	,B	�B	+B��B�BB��B�LB��B��B��B�B��B��B�B��B	B	DB	�B	"�B	1'B	A�B	I�B	ZB	hsB	u�B	�B	�=B	��B	�!B	ƨB	�/B	�yB	��B
\B
�B
&�B
0!B
8RB
A�B
N�B
XB
_;B
gmB
m�B
p�B
u�B
x�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B��B��B��B�B�fB	�B	K�B	gmB	�fB
6FB
O�B
q�B
�=B
��B
�B
�B
�B
�BBB.BL�B�'BĜB�RB��B��B��B�B��B��B��B��B��B��B�+B� By�Bm�BaHBN�BB�B1'B$�B�B1B
��B
�sB
��B
��B
�B
��B
�JB
aHB
>wB
#�B
  B	�B	�
B	ŢB	�wB	��B	��B	}�B	^5B	L�B	,B	�B	+B��B�BB��B�LB��B��B��B�B��B��B�B��B	B	DB	�B	"�B	1'B	A�B	I�B	ZB	hsB	u�B	�B	�=B	��B	�!B	ƨB	�/B	�yB	��B
\B
�B
&�B
0!B
8RB
A�B
N�B
XB
_;B
gmB
m�B
p�B
u�B
x�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080909185326  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080909185328  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080909185329  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080909185329  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080909185333  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080909185333  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080909185333  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080909185333  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080909185334  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080909191044                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080913035846  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080913035851  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080913035852  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080913035852  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080913035856  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080913035856  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080913035857  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080913035857  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080913035857  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080913051034                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080913035846  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518085125  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518085125  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090518085125  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518085126  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518085127  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518085127  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518085127  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518085127  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518085127  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518085349                      G�O�G�O�G�O�                