CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900996 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               fA   JA  20081020185250  20090518085344                                  2B  A   2296                                                            846 @��m:�1   @����@5�I�^@a�j~��1   ARGOS   A   A   A   @�  A  Ac33A�ffA�  A�  B
  B33B2  BF��B[33BnffB���B���B�ffB�33B�33B�ffB���B�  B�ffB�ffB�  B�  B�33C  CL�C� C� C�C�C� C$ffC)33C.ffC3L�C8  C=  CB  CGL�CQ  C[�CeL�CoL�Cy� C���C�� C��3C�� C��fC���C�s3C��3C��3C��fC���C��3C�� C CǙ�C̙�Cљ�C֌�C�s3C���C噚C���C�� C�� C��fD�fD��D��D�3D�3D�3D� D$ٚD)�fD.��D3�fD8ٚD=� DB�fDG��DL�3DQٚDV��D[� D`��De�fDj�fDo� Dt� Dy� D�)�D�l�D��fD��3D�0 D�i�D���D�ٚD�)�D�i�D�� D���D��D�ffDڰ D�� D��D�c3D�D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A  A[33A�ffA�  A�  B  B33B0  BD��BY33BlffB34B���B�ffB�33B�33B�ffB���B�  B�ffB�ffB�  B�  B�33C � C��C  C  C��C��C  C#�fC(�3C-�fC2��C7� C<� CA� CF��CP� CZ��Cd��Cn��Cy  C�L�C�� C�s3C�� C�ffC�L�C�33C�s3C�s3C�ffC�L�C�s3C�@ C�@ C�Y�C�Y�C�Y�C�L�C�33C�L�C�Y�C��C� C� C�ffD�fD��D��D�3D�3D�3D� D$��D)�fD.��D3�fD8��D=� DB�fDG��DL�3DQ��DV��D[� D`��De�fDj�fDo� Dt� Dy� D��D�\�D��fD��3D�  D�Y�D���D�ɚD��D�Y�D�� D���D��D�VfDڠ D�� D�	�D�S3D�D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�A�^5A�p�A�A뗍A띲A럾A��A뛦A�uA�1A�z�A���A�l�A�Aǉ7AÇ+A��^A���A�jA�S�A�A�ĜA��yA�~�A�JA��A�{A�1'A���A�VA�$�A�O�A�ȴA��wA�jA�bNA�t�A�ƨA���A�A��jA��A��;A��7A�bNA�K�A�~�A��yA�=qA�G�A|�Ax��At�DAq�^Al��Af=qAa�7A^ffAVffARA�AMƨAGp�A=A7�hA4E�A.jA*1'A#�7A�A��A
��AK�@��!@�u@�&�@�ƨ@�hs@�Ĝ@� �@��#@���@��D@�G�@� �@�J@�~�@�$�@��@�1@�ƨ@��u@z�H@t�D@m�-@b-@T��@P�9@L��@E�-@=�h@4�/@/;d@+o@'�@"�\@�T@M�@K�@��@5?@
n�@\)@9X@"�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A�A�^5A�p�A�A뗍A띲A럾A��A뛦A�uA�1A�z�A���A�l�A�Aǉ7AÇ+A��^A���A�jA�S�A�A�ĜA��yA�~�A�JA��A�{A�1'A���A�VA�$�A�O�A�ȴA��wA�jA�bNA�t�A�ƨA���A�A��jA��A��;A��7A�bNA�K�A�~�A��yA�=qA�G�A|�Ax��At�DAq�^Al��Af=qAa�7A^ffAVffARA�AMƨAGp�A=A7�hA4E�A.jA*1'A#�7A�A��A
��AK�@��!@�u@�&�@�ƨ@�hs@�Ĝ@� �@��#@���@��D@�G�@� �@�J@�~�@�$�@��@�1@�ƨ@��u@z�H@t�D@m�-@b-@T��@P�9@L��@E�-@=�h@4�/@/;d@+o@'�@"�\@�T@M�@K�@��@5?@
n�@\)@9X@"�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B	+B	oB	�B	'�B	)�B	(�B	)�B	+B	)�B	s�B	��B	�B
bNB
q�B
��B
��B
�TB
��B9XBH�BQ�B8RBC�BK�BK�BYBW
BW
Bo�BgmBhsBjBdZBe`BbNBe`B`BB`BBZBT�BI�B;dB49B(�B�B
=B
��B
�B
�#B
�wB
��B
�=B
s�B
e`B
M�B
.B
�B
	7B	�
B	�qB	��B	�1B	_;B	R�B	J�B	=qB	,B	uB	  B�BÖB�'B�B��B��B��B��B�B�'B�^B��B�B�B	B	,B	ZB	gmB	q�B	x�B	}�B	�B	�bB	��B	�3B	��B	�)B	��B
bB
�B
 �B
,B
8RB
B�B
G�B
Q�B
W
B
]/B
cTB
jB
p�B
v�B
y�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B	+B	oB	�B	'�B	)�B	(�B	)�B	+B	)�B	s�B	��B	�B
bNB
q�B
��B
��B
�TB
��B9XBH�BQ�B8RBC�BK�BK�BYBW
BW
Bo�BgmBhsBjBdZBe`BbNBe`B`BB`BBZBT�BI�B;dB49B(�B�B
=B
��B
�B
�#B
�wB
��B
�=B
s�B
e`B
M�B
.B
�B
	7B	�
B	�qB	��B	�1B	_;B	R�B	J�B	=qB	,B	uB	  B�BÖB�'B�B��B��B��B��B�B�'B�^B��B�B�B	B	,B	ZB	gmB	q�B	x�B	}�B	�B	�bB	��B	�3B	��B	�)B	��B
bB
�B
 �B
,B
8RB
B�B
G�B
Q�B
W
B
]/B
cTB
jB
p�B
v�B
y�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081020185246  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081020185250  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081020185251  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020185251  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020185255  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081020185255  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020185256  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081020185256  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081020185256  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020191705                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081023154835  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081023154840  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081023154841  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081023154841  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081023154845  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081023154845  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081023154845  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081023154845  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081023154846  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081023191018                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081023154835  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518085135  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518085135  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090518085136  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518085136  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518085137  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518085137  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518085137  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518085137  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518085137  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518085344                      G�O�G�O�G�O�                