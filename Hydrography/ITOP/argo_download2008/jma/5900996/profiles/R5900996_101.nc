CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900996 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               eA   JA  20081009185324  20090518085340                                  2B  A   2296                                                            846 @���/�c1   @���Ib��@5�p��
=@a"��`B1   ARGOS   A   A   A   @�ffAffAc33A�ffA�33A�ffB
ffBffB2ffBF  BZffBnffB�  B�33B�ffB�ffB���B�  B�ffB�33BЙ�B�33B�  B���B���C� C� CL�C�fCffCffC��C#�fC)ffC.�3C3� C8ffC=�3CB33CG33CP��CZ�fCd��Cn��Cy33C��fC��3C�� C���C���C���C�ffC�� C�s3C���C���C�� C���C³3CǦfC̙�CѦfCֳ3Cۙ�C���C�fC�fC�fC��C���D�3D�3D��DٚD�fD�3D�fD$�3D)�3D.ٚD3ٚD8��D=��DB�fDG�fDL��DQ�3DV� D[�fD`��De��Dj� Do�3Dt�fDy�fD�,�D�i�D�� D��fD�&fD�` D��3D�ٚD�&fD�\�D���D���D�  D�ffDڦfD���D��D�l�D��D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��AY��A���A�ffA噙B  B  B0  BC��BX  Bl  B��B�  B�33B�33B�fgB���B�33B�  B�fgB�  B���B홚B���C �fC�fC
�3CL�C��C��C33C#L�C(��C.�C2�fC7��C=�CA��CF��CP33CZL�Cd  Cn  Cx��C�Y�C�ffC�s3C�@ C�L�C�L�C��C�33C�&fC�L�C�@ C�s3C�� C�ffC�Y�C�L�C�Y�C�ffC�L�C�L�C�Y�C�Y�C�Y�C�@ C�L�D��D��D�gD�4D� D��D� D$��D)��D.�4D3�4D8�gD=�gDB� DG� DL�gDQ��DV��D[� D`�4De�4Dj��Do��Dt� Dy� D��D�VgD���D��3D�3D�L�D�� D��gD�3D�I�D���D�ٚD��D�S3Dړ3D�ɚD�	�D�Y�D�D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A��
A��A��/A��HA��`A��mA��#A��yA��A�XA� �A�G�A�oA���A���A�?}A�1'A�v�A�-A���A�\)A��A�K�A��A�r�A��A�G�A�?}A�?}A���A�VA���A�jA�bNA�z�A�/A�;dA�9XA��A��`A�K�A��A�JA�9XA�x�A�l�A��mA�&�A�?}A��FA��wA�/AO�AyhsAt^5AoXAk33AdȴA_�;A[�AU�
ARr�AOS�AJ�yAE�AB{A<9XA8z�A4n�A&��AQ�A��A	O�A n�@��`@��@��
@��@Ѓ@�?}@��@��@���@�+@�Z@��@��H@�
=@�O�@���@�7L@��u@�n�@~ff@n�@k�@\z�@TI�@J��@C�@A��@<9X@4�j@.��@)7L@#�m@ff@7L@�@J@�y@
�\@�@-1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A��
A��A��/A��HA��`A��mA��#A��yA��A�XA� �A�G�A�oA���A���A�?}A�1'A�v�A�-A���A�\)A��A�K�A��A�r�A��A�G�A�?}A�?}A���A�VA���A�jA�bNA�z�A�/A�;dA�9XA��A��`A�K�A��A�JA�9XA�x�A�l�A��mA�&�A�?}A��FA��wA�/AO�AyhsAt^5AoXAk33AdȴA_�;A[�AU�
ARr�AOS�AJ�yAE�AB{A<9XA8z�A4n�A&��AQ�A��A	O�A n�@��`@��@��
@��@Ѓ@�?}@��@��@���@�+@�Z@��@��H@�
=@�O�@���@�7L@��u@�n�@~ff@n�@k�@\z�@TI�@J��@C�@A��@<9X@4�j@.��@)7L@#�m@ff@7L@�@J@�y@
�\@�@-1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	p�B	p�B	o�B	p�B	p�B	p�B	o�B	gmB	|�B	�{B
'�B
cTB
~�B
�oB
��B
B
�HBVB.B49BhsB�B�%Bu�B{�B�bB�hBx�BjBiyBffBgmBdZBhsB�1B��B� Bx�Bl�Bo�B`BBS�BI�BE�B?}B5?B&�B�B
��B
�B
�fB
��B
B
��B
�DB
r�B
ZB
F�B
'�B
PB	�B	��B	�B	�wB	�!B	�{B	� B	`BB	Q�B	@�B	 �B�ZB��B�?B��B��B��B��B��B��B��B�)B��B�sB	bB	"�B	�B	%�B	5?B	A�B	N�B	[#B	k�B	� B	�1B	�B	�BB	�B
B
DB
\B
%�B
,B
1'B
<jB
F�B
O�B
XB
`BB
e`B
k�B
o�B
t�B
{�B
� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	p�B	p�B	o�B	p�B	p�B	p�B	o�B	gmB	|�B	�{B
'�B
cTB
~�B
�oB
��B
B
�HBVB.B49BhsB�B�%Bu�B{�B�bB�hBx�BjBiyBffBgmBdZBhsB�1B��B� Bx�Bl�Bo�B`BBS�BI�BE�B?}B5?B&�B�B
��B
�B
�fB
��B
B
��B
�DB
r�B
ZB
F�B
'�B
PB	�B	��B	�B	�wB	�!B	�{B	� B	`BB	Q�B	@�B	 �B�ZB��B�?B��B��B��B��B��B��B��B�)B��B�sB	bB	"�B	�B	%�B	5?B	A�B	N�B	[#B	k�B	� B	�1B	�B	�BB	�B
B
DB
\B
%�B
,B
1'B
<jB
F�B
O�B
XB
`BB
e`B
k�B
o�B
t�B
{�B
� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081009185321  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081009185324  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081009185324  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081009185324  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081009185329  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081009185329  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081009185329  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081009185329  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081009185329  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081009190808                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081013034747  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081013034752  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081013034752  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081013034753  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081013034757  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081013034757  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081013034757  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081013034757  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081013034757  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081013060854                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081013034747  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518085132  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518085133  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090518085133  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518085133  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518085134  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518085134  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518085135  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518085135  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518085135  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518085340                      G�O�G�O�G�O�                