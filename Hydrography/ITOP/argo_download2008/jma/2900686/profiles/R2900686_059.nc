CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   n   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4T   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6|   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :\   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J�   CALIBRATION_DATE            	             
_FillValue                  ,  M�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N    HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N`   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Np   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Nt   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  2900686 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ;A   JA  20080930185626  20091208062807                                  2B  A   APEX-SBE 2971                                                   846 @��P6�1   @��dۗ@9\j~��#@atj~��#1   ARGOS   A   A   A   @���A��Ai��A���A���A홚B
ffB��B2ffBE��BX��Bn��B�ffB���B���B���B�33B�  B���B�ffBЙ�B���B䙚B�33B���CL�CffCL�CffC�C�C� C$ffC(��C.  C333C7��C=ffCBffCGL�CQ  C[�Ce33Co��Cy33C��fC��fC���C���C��3C���C��fC�s3C���C�ffC�s3C�s3C��fC�� Cǳ3C̳3CѦfCֳ3C۳3C�fC�fC�3C�s3C��fC���D�3D�fD��D�3D�fDٚD� D$ٚD)ٚD.��D3��D8�fD=�fDB��DG�fDL�3Dj�3Do�3Dt� Dy�fD�,�D�i�D�� D��fD�&fD�p D��fD��fD�  D�VfD���D��D�&fD�l�Dڣ3D��3D�  D�c3D��D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���@� A8  A���A�  A���A���BfgB&  B934BLfgBbfgBvffB���B���B�fgB�  B���B���B�33B�fgBә�B�fgB�  B�B�fgCL�C33CL�C  C  CffC!L�C%�3C*�fC0�C4�3C:L�C?L�CD33CM�fCX  Cb�Cl� Cv�C��C��C�  C��C�&fC�  C��C��fC�  C�ٙC��fC��fC��C�33C�&fC�&fC��C�&fC�&fC��C��C�&fC��fC��C�  D�D  D
�4D�D  D4D�D$4D)4D.gD3gD8  D=  DBgDG  DL�Dj�Do�Dt�Dy  D�ɚD�gD�<�D��3D��3D��D�C3D��3D���D��3D�I�DǆgD��3D�	�D�@ D�� D��D�  D�I�D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�S�A�A�A�$�A�-A�=qA�`BA��#A�~�A��A�ȴAП�A��HA�O�A�oA�JA�K�A���A���A��A�=qA�  A��FA�S�A�n�A�33A���A��A�ffA���A��\A�-A��;A��jA���A��A�S�A�l�A��A���A�5?A�^5A�7LA�$�A�`BA�ZA��TA���A���A���A�ZA�=qA�XA��hA��hA�ffAdZAy�Au�PArAj(�AfA�A^VAX  AR��AM/AG\)AA�#A< �A6��A4�+A/33A)hsA�A  A&�A�-@�33@�-@ݡ�@��/@���@�l�@��7@��m@���@�@�ff@��m@��R@
=@l��@`�`@R�@J^5@@Q�@:J@4�@,Z@(Q�@"�H@9X@/@
=@�
@	hs@5?@��?���?�^5?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�S�A�A�A�$�A�-A�=qA�`BA��#A�~�A��A�ȴAП�A��HA�O�A�oA�JA�K�A���A���A��A�=qA�  A��FA�S�A�n�A�33A���A��A�ffA���A��\A�-A��;A��jA���A��A�S�A�l�A��A���A�5?A�^5A�7LA�$�A�`BA�ZA��TA���A���A���A�ZA�=qA�XA��hA��hA�ffAdZAy�Au�PArAj(�AfA�A^VAX  AR��AM/AG\)AA�#A< �A6��A4�+A/33A)hsA�A  A&�A�-@�33@�-@ݡ�@��/@���@�l�@��7@��m@���@�@�ff@��m@��R@
=@l��@`�`@R�@J^5@@Q�@:J@4�@,Z@(Q�@"�H@9X@/@
=@�
@	hs@5?@��?���?�^5?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�B�B�B	~�B	�TB	{B	7LB	9XB	� B
%�B
ȴBXBF�B[#B
e`B
��B
�'B
��B
�B
��BbB"�B,BH�BgmBW
B-BG�BF�B=qBJ�BVBYBVBP�BM�BH�BB�B>wB;dB7LB1'B,B'�B"�B�BuB	7B
��B
�B
�mB
�
B
ŢB
�jB
�B
��B
�B
n�B
\)B
7LB
 �B	��B	�;B	ǮB	�B	��B	|�B	cTB	O�B	E�B	0!B	�B�B�
B�B�DB�B{�Bu�Br�Bw�B~�B�=B��B�B�?B	\B	�B	!�B	,B	S�B	p�B	��B	�B	ŢB	��B	�ZB	��B
B
PB
�B
0!B
>wB
F�B
M�B
T�B
^5B
ffB
k�B
o�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B�B�B�B	~�B	�TB	{B	7LB	9XB	� B
%�B
ȴBXBF�B[#B
e`B
��B
�'B
��B
�B
��BbB"�B,BH�BgmBW
B-BG�BF�B=qBJ�BVBYBVBP�BM�BH�BB�B>wB;dB7LB1'B,B'�B"�B�BuB	7B
��B
�B
�mB
�
B
ŢB
�jB
�B
��B
�B
n�B
\)B
7LB
 �B	��B	�;B	ǮB	�B	��B	|�B	cTB	O�B	E�B	0!B	�B�B�
B�B�DB�B{�Bu�Br�Bw�B~�B�=B��B�B�?B	\B	�B	!�B	,B	S�B	p�B	��B	�B	ŢB	��B	�ZB	��B
B
PB
�B
0!B
>wB
F�B
M�B
T�B
^5B
ffB
k�B
o�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080930185622  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080930185626  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080930185627  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080930185631  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080930185631  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080930185632  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080930185632  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080930191459                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081003162104  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081003162108  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081003162109  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081003162113  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081003162113  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081003162113  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081003162114  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081003191038                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081003162104  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416070759  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416070759  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416070759  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416070800  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090416070800  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416070800  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416070800  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416070801  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416070801  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416070801  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416071336                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208050152  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208050335  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208050335  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208050336  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208050337  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208050337  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208050337  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208050337  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208050337  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208062807                      G�O�G�O�G�O�                