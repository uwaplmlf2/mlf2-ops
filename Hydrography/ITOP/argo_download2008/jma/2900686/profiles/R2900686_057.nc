CDF   *   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   j   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D<   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G<   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J<   CALIBRATION_DATE            	             
_FillValue                  ,  M<   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Mh   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Ml   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Mp   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Mt   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Mx   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  2900686 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               9A   JA  20080910185604  20091208062817                                  2B  A   APEX-SBE 2971                                                   846 @����1   @���'q@9��t�j@au/��w1   ARGOS   B   B   B   @�  A��Ak33A�ffA���A�ffB	��B��B2ffBE33BZ  Bn��B���B�  B���B���B���B�  B�33B�  B�33B���B���C33C  C� CL�C$L�G�O�CB��CG� CQ33C[33Ce�Co  Cy� C��3C�� C�s3C�s3C��3C��3C��fC��3C��fC�� C�� C�� C���C Cǀ C̀ Cь�C�� Cۙ�C�fC�3C�s3C�� C��C���DٚD��D��D��D��D�fD�3D$� D)�3D.��D3��D8��D=��DB��DG�fDL��DQ��DV�3D[� D`�3De�3Dj�3Do��Dt�fDy��D�&fD�l�D��3D�� D�,�D�i�D�� D���D��D�c3D���D��fD�33D�c3DڦfD��D�&fD�ffD��D��1111111111111111111111111111411111111111111111111111111111111111111111111111111111111111111111111111111111  @�  @ə�A;33A�ffA���A�ffA�34B��B&ffB933BN  Bb��Bu��B�  B���B���B���B�  B�33B�  B�33B���B���C33C  C� CL�C!L�G�O�C?��CD� CN33CX33Cb�Cl  Cv� C�33C�  C��3C��3C�33C�33C�&fC�33C�&fC�  C�@ C�@ C��C�  C�  C�  C��C�@ C��C�&fC�33C��3C�@ C��C��D�D�D�D�D�D&fD3D$  D)3D-��D2��D7��D<��DA��DG&fDL�DQ�DV3D[  D`3Dd�3Di�3Do�DtfDx��D��fD��D�C3D�� D���D�	�D�@ D�|�D���D�3D�L�DǆfD��3D�3D�FfD���D��fD�fD�<�D���1111111111111111111111111111411111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�FA�A蝲A�XA��A��A�?}A�G�A��7A���Aҧ�Aƺ^A��DA���A��A��A���A��A��#A�l�A�=qA�z�A��uA�
=A�ĜA�7LA��G�O�A�Q�A�jA�$�A��HA���A��A�"�A�^5A�r�A��A��A��HA�M�A��A���A�^5A��/A}�TAy�mAs�-Ar�+AoG�Al�+AjZAfffA^�A[K�AVĜAQl�AO?}AIl�AE33AA
=A=�hA5A)
=A�;A�AE�A�\@�bN@ް!@� �@Ͼw@�1@�-@��@�ƨ@��9@�  @�r�@��#@��@���@���@��D@���@�1@n�@eO�@\�j@P�9@D��@;33@5?}@-/@'�;@!��@�h@�@o@E�@�F@	7L@�@M�@ bN?�/1111111111111111111111111119411111111111111111111111111111111111111111111111111111111111111111111111111111  A�FA�A蝲A�XA��A��A�?}A�G�A��7A���Aҧ�Aƺ^A��DA���A��A��A���A��A��#A�l�A�=qA�z�A��uA�
=A�ĜA�7LA��G�O�A�Q�A�jA�$�A��HA���A��A�"�A�^5A�r�A��A��A��HA�M�A��A���A�^5A��/A}�TAy�mAs�-Ar�+AoG�Al�+AjZAfffA^�A[K�AVĜAQl�AO?}AIl�AE33AA
=A=�hA5A)
=A�;A�AE�A�\@�bN@ް!@� �@Ͼw@�1@�-@��@�ƨ@��9@�  @�r�@��#@��@���@���@��D@���@�1@n�@eO�@\�j@P�9@D��@;33@5?}@-/@'�;@!��@�h@�@o@E�@�F@	7L@�@M�@ bN?�/1111111111111111111111111119411111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B,B)�B,B6FBQ�BR�BcTBo�BjB�%B�%B��B��B��B��B��B��B��B�=B~�Bx�Bv�BhsBm�BjB[#BXBW
G�O�B7LB1'B-B'�B#�B�B�BoB	7B
��B
�B
�BB
��B
ǮB
�^B
�B
��B
�B
ffB
_;B
O�B
@�B
7LB
"�B
  B	�B	��B	B	�RB	��B	�DB	x�B	k�B	H�B	oB�NBB�3B�uB�Bv�Bx�B{�B|�B|�B�+B�VB��B��B�!B�LB�dB��B�B	B	�B	&�B	M�B	gmB	�B	��B	�^B	��B	�5B	�B	��B
VB
�B
%�B
2-B
>wB
F�B
M�B
T�B
]/B
cTB
gm1111111111111111111111111111411111111111111111111111111111111111111111111111111111111111111111111111111111  B,B)�B,B6FBQ�BR�BcTBo�BjB�%B�%B��B��B��B��B��B��B��B�=B~�Bx�Bv�BhsBm�BjB[#BXBW
G�O�B7LB1'B-B'�B#�B�B�BoB	7B
��B
�B
�BB
��B
ǮB
�^B
�B
��B
�B
ffB
_;B
O�B
@�B
7LB
"�B
  B	�B	��B	B	�RB	��B	�DB	x�B	k�B	H�B	oB�NBB�3B�uB�Bv�Bx�B{�B|�B|�B�+B�VB��B��B�!B�LB�dB��B�B	B	�B	&�B	M�B	gmB	�B	��B	�^B	��B	�5B	�B	��B
VB
�B
%�B
2-B
>wB
F�B
M�B
T�B
]/B
cTB
gm1111111111111111111111111111411111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080910185601  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080910185604  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080910185605  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080910185608  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080910185608  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080910185609  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080910185609  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20080910185609  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080910191051                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080913042951  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080913042955  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080913042956  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080913043001  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080913043001  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080913043001  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080913043001  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20080913043002  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080913051541                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080913042951  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416070754  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416070755  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416070755  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416070756  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090416070756  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416070756  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416070756  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416070756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416070756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416070756  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416071344                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208050152  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208050332  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208050333  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208050333  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208050334  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091208050334  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208050334  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208050334  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208050334  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208050334  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208050334  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208062817                      G�O�G�O�G�O�                