CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   o   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K   CALIBRATION_DATE            	             
_FillValue                  ,  N   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N4   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N8   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N<   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N@   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  ND   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  2900686 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               <A   JA  20081010185520  20091208062808                                  2B  A   APEX-SBE 2971                                                   846 @���8�w1   @���W�k�@9-V�@av-V1   ARGOS   A   A   A   @�ffA��Ah  A���A���A�ffB
ffB��B1��BF  BY��BlffB�  B�  B���B���B�  B���B���Bƙ�B�33Bڙ�B�  B�  B���C� CffC33C�C�C�C  C$�C)33C.  C3�C8� C=ffCBL�CGL�CQffC[33CeffCo� Cy33C���C���C��3C�s3C�ffC�ffC�Y�C��3C���C��3C��3C���C��3C�C�s3C�ffC�s3C�s3C�s3C�fC���C�� C���C��fC���DٚDٚD��D�fD�3D��D�fD$�3D)ٚD.�fD3��D8ٚD=�3DBٚDG��DL�fDQ�3DV�3D[� D`ٚDe� Dj��Do��Dt��Dy�3D��D�ffD��fD�� D�#3D�ffD�� D���D�#3D�i�D���D�� D��D�Y�D�D�� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33@� A6ffA���A�  Aՙ�A���BfgB%34B9��BM34B`  Bs��B���B�fgB���B���B���B���B�fgB�  B�fgB���B���B�B���CL�C�C  C  C  C�fC!  C&�C*�fC0  C5ffC:L�C?33CD33CNL�CX�CbL�ClffCv�C��C��C�&fC��fC�ٙC�ٙC���C�&fC�@ C�&fC�&fC�  C�&fC��C��fC�ٙC��fC��fC��fC��C�@ C�33C�@ C��C��D4D4D
�4D  D�D�4D  D$�D)4D.  D3gD84D=�DB4DF�4DL  DQ�DV�D[�D`4De�DjgDogDtgDy�D���D�3D�C3D�|�D�� D�3D�<�D���D�� D�gD�FgD�\�D�gD��gD�FgD�L�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A�(�A��A��A��`A��A�^5A�S�Aش9A�oA�C�A�(�A�K�A��A�%A��-A�v�A�n�A���A�1A�`BA�ȴA�M�A���A��+A�?}A�E�A���A�ZA���A���A�E�A�?}A�%A��DA�33A�/A�?}A�`BA���A�E�A���A��A��mA��A��A��A�
=A�A�A��yA��\A���A��`A~��A|�jAw�;Ar�Anv�Ak�AhAd��A_��AY�AV��AR  AM%AG�PA>M�A8��A4�uA*n�A��A��A	|�A~�@�\)@땁@��H@֧�@��;@�Q�@�1'@�Q�@�&�@�+@��;@��D@�C�@��+@��@�v�@�9X@��#@�ƨ@~ff@o+@a�@S�
@Ix�@B��@:�@2�!@+�
@&5?@!��G�O�@l�@dZ@G�?��D?�(�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111911111 A��A�(�A��A��A��`A��A�^5A�S�Aش9A�oA�C�A�(�A�K�A��A�%A��-A�v�A�n�A���A�1A�`BA�ȴA�M�A���A��+A�?}A�E�A���A�ZA���A���A�E�A�?}A�%A��DA�33A�/A�?}A�`BA���A�E�A���A��A��mA��A��A��A�
=A�A�A��yA��\A���A��`A~��A|�jAw�;Ar�Anv�Ak�AhAd��A_��AY�AV��AR  AM%AG�PA>M�A8��A4�uA*n�A��A��A	|�A~�@�\)@땁@��H@֧�@��;@�Q�@�1'@�Q�@�&�@�+@��;@��D@�C�@��+@��@�v�@�9X@��#@�ƨ@~ff@o+@a�@S�
@Ix�@B��@:�@2�!@+�
@&5?@!��G�O�@l�@dZ@G�?��D?�(�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111911111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B  B  B
��B
��B
��BA�BYBhsBdZBl�BM�BA�B{�B�=B��B��B��B�JB|�B�B{�B� B� B|�B� B{�Bv�Bs�Bn�BgmBdZBaHB]/BW
BN�BJ�BF�BB�B?}B;dB5?B/B'�B!�B�BJB%B
��B
�mB
�#B
��B
�}B
�B
��B
�bB
w�B
^5B
L�B
?}B
-B
�B
	7B	�yB	�NB	��B	�jB	��B	x�B	`BB	J�B	#�B�B�B��B�bB�Bz�Bt�Bt�Bv�B}�B�=B��B��B�B�LB�wB��B�#B�B��B	1B	uB	�B	49B	VB	o�B	�oB	�B	��B	�B	�B	��B
JB
�G�O�B
T�B
aHB
gmB
n�B
o�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111911111 B  B  B
��B
��B
��BA�BYBhsBdZBl�BM�BA�B{�B�=B��B��B��B�JB|�B�B{�B� B� B|�B� B{�Bv�Bs�Bn�BgmBdZBaHB]/BW
BN�BJ�BF�BB�B?}B;dB5?B/B'�B!�B�BJB%B
��B
�mB
�#B
��B
�}B
�B
��B
�bB
w�B
^5B
L�B
?}B
-B
�B
	7B	�yB	�NB	��B	�jB	��B	x�B	`BB	J�B	#�B�B�B��B�bB�Bz�Bt�Bt�Bv�B}�B�=B��B��B�B�LB�wB��B�#B�B��B	1B	uB	�B	49B	VB	o�B	�oB	�B	��B	�B	�B	��B
JB
�G�O�B
T�B
aHB
gmB
n�B
o�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111911111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081010185518  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081010185520  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081010185521  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081010185525  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081010185525  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081010185526  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081010191423                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081013041754  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081013041758  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081013041759  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081013041803  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081013041803  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081013041803  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081013061510                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081013041754  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416070801  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416070801  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416070802  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416070803  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416070803  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416070803  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416070803  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416070803  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416071338                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208050153  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208050337  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208050338  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208050338  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208050339  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208050339  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208050339  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208050339  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208050339  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208062808                      G�O�G�O�G�O�                