CDF   *   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   k   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4H   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6`   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8x   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :$   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <<   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >T   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  @    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @l   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  B   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D0   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D`   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G`   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J`   CALIBRATION_DATE            	             
_FillValue                  ,  M`   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    M�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    M�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    M�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    M�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  M�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N    HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    NArgo profile    2.2 1.2 19500101000000  2900686 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               =A   JA  20081020125413  20091208062806                                  2B  A   APEX-SBE 2971                                                   846 @���B�\)1   @���p4V@9)�^5?}@avȴ9X1   ARGOS   B   B   B   @�  A  Ah  A���A�33A���B	33B��B1��BFffBZffBm33B�ffB���B�33B�ffB�  B�33B�  B���B�ffB���B�  B���B���CffC�fC� C33C� C� CffC$L�C)33C.L�C3� C8� C=� CBL�CG� CQffC[33Ce��CoffCyL�C���C��3C���C���C�� C���C��fC��fC���C�� C�� C��3C��fC CǙ�C̳3Cѳ3C֌�C۳3C���C���C���C�� C��C��3D� DٚD�3D��D�3D� D�fD$� D)��D.ٚD3�3D8��D=� DB�3DGٚDL�fDQ�3DV��D[�fD`ٚDe� G�O�D�i�D�� D���D�&fD�\�D��fD��fD�,�D�ffD�� D�� D�#3D�Y�D� D�	�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114111111111111111 @���@���A6ffA�  A�ffA�  A���B34B%34B:  BN  B`��BtffB�fgB�  B�33B���B�  B���B���B�33Bԙ�B���B癚B�fgB���C��CffC�CffCffCL�C!33C&�C+33C0ffC5ffC:ffC?33CDffCNL�CX�Cb� ClL�Cv33C��C�&fC�  C��C��3C��C��C��C��C�33C�33C�&fC��C��3C��C�&fC�&fC�  C�&fC�@ C�@ C�@ C�33C�  C�&fD�D4D�D�4D��D�D  D$�D(�4D.4D3�D8gD<��DB�DG4DL  DQ�DU�4D[  D`4De�G�O�D�gD�<�D�y�D��3D���D�C3D��3D�ɚD�3D�<�D�\�D�� D��gD�<�D��g11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�JA�JA�1A�1A�A�I�AᙚAݶFA� �A�I�A��A���A�{A��#A� �A�%A�+A���A�dZA���A���A��A���A���A��A���A���A��7A�p�A�bA�=qA�33A�{A�E�A���A���A�C�A��RA���A�G�A�oA��jA��`A��jA��#A�/A��A��/A��A�C�A�&�A��A}hsAw33AqXAl�Ai�7Ag�Ac�;A`ȴA\��AX��AVn�AOVAK%AD�9A>�+A7�A2��A.ffA'�-A �A�A��@�`B@�n�@ܴ9@�
=@��@�~�@�V@�X@�O�@��@��w@�K�@��T@�
=@��@��G�O�@i&�@^��@T��@K��@=�@8�9@3o@-/@&E�@!XG�O�@bN@�h@��@ �?�j11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111194111111111911111 A�JA�JA�1A�1A�A�I�AᙚAݶFA� �A�I�A��A���A�{A��#A� �A�%A�+A���A�dZA���A���A��A���A���A��A���A���A��7A�p�A�bA�=qA�33A�{A�E�A���A���A�C�A��RA���A�G�A�oA��jA��`A��jA��#A�/A��A��/A��A�C�A�&�A��A}hsAw33AqXAl�Ai�7Ag�Ac�;A`ȴA\��AX��AVn�AOVAK%AD�9A>�+A7�A2��A.ffA'�-A �A�A��@�`B@�n�@ܴ9@�
=@��@�~�@�V@�X@�O�@��@��w@�K�@��T@�
=@��@��G�O�@i&�@^��@T��@K��@=�@8�9@3o@-/@&E�@!XG�O�@bN@�h@��@ �?�j11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111194111111111911111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�B	��B	��B	�B	�NB
%B
A�B
XB
�%B
��B
��B
�bB
�oB
��B
��B
�B
��BB
=B\B�B.B1'B,B-B,B.B.B7LB33B33B5?B33B/B.B+B&�B$�B!�B�B�B�BoB1B
��B
��B
�B
�#B
��B
ÖB
�RB
��B
��B
|�B
]/B
I�B
;dB
0!B
#�B
{B
B	�B	�ZB	ƨB	�9B	��B	z�B	aHB	L�B	;dB	�B��B��B�'B�oB�+B� Bz�Bq�Bn�Bs�B�+B�VB��B�9BB��B�#B�B��B	DG�O�B	~�B	�hB	�B	��B	�;B	�B	��B
PB
�G�O�B
S�B
\)B
cTB
hsB
o�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114111111111911111 B	�B	��B	��B	�B	�NB
%B
A�B
XB
�%B
��B
��B
�bB
�oB
��B
��B
�B
��BB
=B\B�B.B1'B,B-B,B.B.B7LB33B33B5?B33B/B.B+B&�B$�B!�B�B�B�BoB1B
��B
��B
�B
�#B
��B
ÖB
�RB
��B
��B
|�B
]/B
I�B
;dB
0!B
#�B
{B
B	�B	�ZB	ƨB	�9B	��B	z�B	aHB	L�B	;dB	�B��B��B�'B�oB�+B� Bz�Bq�Bn�Bs�B�+B�VB��B�9BB��B�#B�B��B	DG�O�B	~�B	�hB	�B	��B	�;B	�B	��B
PB
�G�O�B
S�B
\)B
cTB
hsB
o�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114111111111911111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081020125411  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081020125413  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020125414  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020125418  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081020125418  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020125418  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20081020125418  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20081020125418  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020130417                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081023041818  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081023041822  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081023041823  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081023041827  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081023041827  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081023041827  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20081023041827  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20081023041827  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081023064636                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081023041818  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416070803  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416070804  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416070804  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416070805  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090416070805  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416070805  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416070805  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416070805  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416070805  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416070805  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416071335                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208050153  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208050340  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208050340  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208050341  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208050342  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091208050342  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208050342  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208050342  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208050342  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208050342  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208050342  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208062806                      G�O�G�O�G�O�                