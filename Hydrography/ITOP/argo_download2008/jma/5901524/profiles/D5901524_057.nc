CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901524 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               9A   JA  20080916050518  20090908021006  A9_66109_057                    2C  D   APEX-SBE 2818                                                   846 @��hN�1   @��:�vW@)�j~��#@a��/��1   ARGOS   A   A   A   @�33A  Aa��A�33A�  A���B
ffB33B1��BD��BY��Bm33B���B���B�33B�33B���B�  B���B�  B�33B���B�  B�ffB���CL�C� CL�C��C��CL�C�C$ffC)� C.��C3� C8�C=ffCB�3CGffCP�fC[�3Ce�3CoffCyL�C��3C�� C��3C��fC��3C�� C���C�� C��fC���C��fC�s3C���C�� CǙ�Č�Cѳ3C֦fCۙ�C�s3C�s3C�� C�� C���C���D�fDٚD�fD�3D�3D� D��D$�3D)��D.��D3� D8�fD=��DB��DG��DL�fDQٚDV� D[�fD`� De��Dj�fDo�fDt��Dy�3D�  D�c3D���D��fD�  D�c3D���D��D�,�D�` D��fD�ٚD�#3D�p Dک�D�� D�&fD�i�D�3D��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33AL��A���A���A�ffB33B  B,ffB?��BTffBh  B|  B�  B���B���B�33B�ffB�33B�ffB͙�B�33B�ffB���B�  C   C33C
  CL�CL�C  C��C#�C(33C-L�C233C6��C<�CAffCF�CO��CZffCdffCn�Cx  C��C��C��C�  C��C�ٚC��3C�ٚC�  C��fC�  C���C�&fC��C��3C��fC��C�  C��3C���C���C��C��C��3C��fDs3D�fDs3D� D� Dl�Dy�D$� D)y�D.y�D3��D8s3D=y�DBy�DGy�DLs3DQ�fDV��D[s3D`��Dey�Djs3Dos3Dty�Dy� D��fD�9�D�� D���D��fD�9�D�s3D�� D�3D�6fD�|�Dǰ D���D�FfDڀ D�fD���D�@ D�i�D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�XA�O�A���A�!A�|�A�v�A�r�A�\)A���A��A�^A�jA�hA���A��A雦A�-A�|�A�VA�\A��A���A�C�A�`BA܁A�-A� �Aմ9A�jA��A���A̬A��/A���A�&�A�JA���A�ȴA���A��A��DA�A�C�A~�Ax$�Apv�A^��AQ�AH(�AB��A@��A5dZA. �A'�A!VA�TA�
A��A�mA�RA1A��A
(�A=qA�A�FA��A =q@�b@�v�@�^5@��@��@ܼj@�Z@� �@��@�+@�A�@�j@��h@���@�hs@�;d@�1'@�(�@��/@��@�33@��F@� �@�~�@�?}@�bN@��@���@�o@� �@{"�@p��@fff@\9X@R~�@J�@C"�@;S�@6��@/�@(�`@!��@v�@�!@+@��@K�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�XA�O�A���A�!A�|�A�v�A�r�A�\)A���A��A�^A�jA�hA���A��A雦A�-A�|�A�VA�\A��A���A�C�A�`BA܁A�-A� �Aմ9A�jA��A���A̬A��/A���A�&�A�JA���A�ȴA���A��A��DA�A�C�A~�Ax$�Apv�A^��AQ�AH(�AB��A@��A5dZA. �A'�A!VA�TA�
A��A�mA�RA1A��A
(�A=qA�A�FA��A =q@�b@�v�@�^5@��@��@ܼj@�Z@� �@��@�+@�A�@�j@��h@���@�hs@�;d@�1'@�(�@��/@��@�33@��F@� �@�~�@�?}@�bN@��@���@�o@� �@{"�@p��@fff@\9X@R~�@J�@C"�@;S�@6��@/�@(�`@!��@v�@�!@+@��@K�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�=B�7B�B�1B�=B�bB��B�'B		7B	�B
2-B
S�B
�DB
�JB
p�B
H�B
�uBr�B�JB�oB�PB��B��B�XB�qB��B�^B�}BŢB�;B�BB�B-B9XB=qB0!BDB��B��B;dB+B
ȴB
^5B
D�B
!�B	�)B	��B	�%B	w�B	l�B	]/B	ZB	YB	L�B	;dB	9XB	F�B	u�B	�=B	��B	��B	��B	�FB	��B	�B	�TB	�`B	�yB	�TB	�TB	�fB	�sB	�B	�B	��B	��B	��B
B
B
%B
%B
%B
DB
JB
VB
bB
PB
VB
�B
�B
�B
�B
�B
�B
'�B
,B
33B
8RB
<jB
A�B
E�B
I�B
M�B
Q�B
XB
ZB
`BB
e`B
k�B
n�B
r�B
x�B
|�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�=B�7B�B�1B�=B�bB��B�'B	
=B	�B
2-B
S�B
�JB
�PB
r�B
I�B
�oBs�B�PB�uB�bB��B��B�^B�}B��B�^B��BȴB�HB�BB �B/B<jBC�B6FBVB��B�B>wB
=B
��B
aHB
G�B
&�B	�BB	��B	�1B	y�B	p�B	`BB	]/B	\)B	O�B	=qB	;dB	H�B	v�B	�JB	��B	��B	�B	�LB	��B	�B	�ZB	�mB	�B	�`B	�ZB	�mB	�B	�B	�B	��B	��B	��B
B
B
+B
+B
+B
JB
PB
\B
hB
VB
\B
�B
�B
�B
�B
�B
�B
(�B
-B
49B
9XB
=qB
B�B
F�B
J�B
N�B
R�B
YB
[#B
aHB
ffB
l�B
o�B
s�B
y�B
}�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=1.3(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809290141562008092901415620080929014156200809290153532008092901535320080929015353200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080916050516  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080916050518  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080916050519  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080916050523  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080916050523  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080916050523  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080916052955                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080920041807  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080920041811  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080920041812  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080920041815  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080920041816  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080920041816  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080920051105                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080920041807  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090430062749  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090430062750  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090430062750  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090430062751  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090430062751  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090430062751  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090430062751  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090430062751  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090430063905                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080919104459  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20080929014156  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080929014156  CV  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080929015353  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020936  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021006                      G�O�G�O�G�O�                