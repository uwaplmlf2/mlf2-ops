CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901524 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               8A   JA  20080906025728  20090908021004  A9_66109_056                    2C  D   APEX-SBE 2818                                                   846 @��)��%1   @��,�tn�@)ۥ�S��@axr� Ĝ1   ARGOS   A   A   A   @�  A��Ai��A���A���A���B
ffB33B1��BD��BY��Bm��B���B�  B�  B���B���B�33B�33B�ffB�ffB�ffB�33B�  B�  C �fC33C
�fCL�C��C� C33C$L�C)L�C.L�C3L�C8��C=��CB��CG�3CQL�C[ffCe� Co� CyffC���C��3C���C���C��3C��3C���C�� C���C�� C�� C�s3C��3C�CǙ�C̀ Cь�C֙�Cی�C�3C噚CꙚC� C��3C��3D�fD�fDٚD�fD� D��DٚD$��D)�3D.ٚD3�fD8��D=�3DB��DG� DL� DQ�3DV�fD[��D`� De� Dj� Do��Dt� Dy� D��D�` D�� D���D�  D�i�D��fD�� D��D�Y�D��3D���D�fD�l�Dڣ3D��D�  D�c3D��D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  @�33AQ��A���A���A���BffB33B+��B>��BS��Bg��B{33B�  B�  B���B���B�33B�33B�ffB�ffB�ffB�33B�  B�  B���C�3C	ffC��C�C  C�3C"��C'��C,��C1��C7�C<�CA�CF33CO��CY�fCd  Cn  Cw�fC�ٚC��3C���C���C��3C��3C���C�  C�ٚC�  C�  C��3C��3C�ٚC�ٚC�� C���C�ٚC���C��3C�ٚC�ٚC�� C��3C��3DffDffDy�DffD` DY�Dy�D$Y�D)s3D.y�D3ffD8l�D=s3DBl�DG` DL` DQs3DVffD[l�D`� De� Dj` Dol�Dt` Dy� D���D�0 D�p D���D�� D�9�D�vfD�� D���D�)�D�s3DǬ�D��fD�<�D�s3D๚D�� D�33D�l�D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A� �A�33A�(�A�5?A�;dA��A�C�A�9A�A�9A��A�ĜA�+A�jA�p�A�t�A�/A�E�A���A�=qA�7LA�ƨA�JA�A�XA�ƨA�~�Aӗ�A���A��Aɩ�A��yA�l�A�A�;dA��yA��
A�-A��yA�
=A��jA��A�$�A���A���A�VA}hsAyAs�A`JA\=qAM�hAD1AA�A5�PA0�A�;AdZA��A�^A�HA�FA��A=qA	
=A�A�HAO�@� �@��w@�bN@�j@���@�E�@��@��@�C�@�p�@�9X@��m@ļj@�Z@�Z@�n�@�1@���@��R@��@���@���@���@��@�;d@�/@�o@���@���@���@t�D@l(�@bn�@U�@P��@H1'@>��@9��@5/@(Ĝ@!G�@�+@��@p�@Ĝ@��@9X1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A� �A�33A�(�A�5?A�;dA��A�C�A�9A�A�9A��A�ĜA�+A�jA�p�A�t�A�/A�E�A���A�=qA�7LA�ƨA�JA�A�XA�ƨA�~�Aӗ�A���A��Aɩ�A��yA�l�A�A�;dA��yA��
A�-A��yA�
=A��jA��A�$�A���A���A�VA}hsAyAs�A`JA\=qAM�hAD1AA�A5�PA0�A�;AdZA��A�^A�HA�FA��A=qA	
=A�A�HAO�@� �@��w@�bN@�j@���@�E�@��@��@�C�@�p�@�9X@��m@ļj@�Z@�Z@�n�@�1@���@��R@��@���@���@���@��@�;d@�/@�o@���@���@���@t�D@l(�@bn�@U�@P��@H1'@>��@9��@5/@(Ĝ@!G�@�+@��@p�@Ĝ@��@9X1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�B	��B	�B	��B	��B
2-B
H�B
R�B
�B
��B
�ZB  BDB
��BDB:^BDB\B�B,B<jBL�Bw�B��B��B�?BƨB��B�B��B�B8RBF�BN�BC�B;dB2-B�B��B�TB�PB.BB
�sB
��B
��B
~�B
gmB
A�B	�B	��B	��B	�B	p�B	G�B	M�B	W
B	>wB	B�B	M�B	�B	��B	��B	��B	�B	�^B	ɺB	��B	�#B	�/B	�ZB	�TB	�TB	�B	�B	��B	��B	��B	��B
B
B	��B
%B
	7B
DB
JB
PB
bB
PB
hB
oB
{B
�B
�B
!�B
,B
/B
49B
;dB
>wB
B�B
H�B
K�B
P�B
T�B
YB
]/B
ffB
o�B
q�B
t�B
z�B
~�B
� B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�B	��B	�B	��B	��B
33B
H�B
R�B
�B
��B
�`BBPB
��BPB<jBJBbB�B.B=qBN�By�B��B��B�FBǮB��B�B��B �B:^BH�BR�BF�B<jB7LB%�BB�sB�{B2-B%B
�B
��B
��B
�B
iyB
G�B	�B	�B	��B	�+B	t�B	I�B	S�B	ZB	@�B	C�B	O�B	�B	��B	��B	��B	�B	�jB	��B	��B	�/B	�;B	�fB	�ZB	�`B	�B	�B	��B	��B	��B	��B
B
B	��B
+B

=B
JB
PB
VB
hB
VB
oB
uB
�B
�B
�B
"�B
-B
0!B
5?B
<jB
?}B
C�B
I�B
L�B
Q�B
VB
ZB
^5B
gmB
p�B
r�B
u�B
{�B
� B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=1.5(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809190145012008091901450120080919014501200809190157412008091901574120080919015741200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080906025726  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080906025728  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080906025729  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080906025733  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080906025733  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080906025733  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080906030815                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080910050157  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080910050204  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080910050205  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080910050210  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080910050211  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080910050211  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080910055522                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080910050157  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090430062747  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090430062747  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090430062748  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090430062749  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090430062749  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090430062749  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090430062749  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090430062749  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090430063903                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080909112738  CV  DAT$            G�O�G�O�F�qf                JM  ARCAJMQC1.0                                                                 20080919014501  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080919014501  CV  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080919015741  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020933  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021004                      G�O�G�O�G�O�                