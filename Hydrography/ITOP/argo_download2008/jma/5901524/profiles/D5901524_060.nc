CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901524 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               <A   JA  20081016013140  20090908021013  A9_66109_060                    2C  D   APEX-SBE 2818                                                   846 @��)z�0}1   @��,$�O�@*����+@at�`A�71   ARGOS   A   A   A   @�33AffA^ffA�  A�33A�33B
  B��B133BF  BY33Bn  B�ffB�  B���B���B���B�  B���B�33BЙ�B�33B䙚B�ffB�ffC��CffC� C�C�C��C� C$� C)� C.33C3��C8�3C=� CBffCG� CQ33C[�CeL�Co� Cy� C���C��3C���C�� C��fC��fC���C�� C�ffC���C�� C���C�� C���CǙ�C̦fCѦfC֌�CۦfC�� C��C�fC��C�� C���D�fD�fD�3D�fDٚD��D��D$�fD)��D.�3D3�fD8ٚD=��DB� DG��DL� DQ�3DV��D[ٚD`�fDe�fDj�fDo� Dt�3Dy��D�)�D�l�D��fD��fD�#3D�l�D��fD��fD�,�D�ffD��fD��fD�&fD�\�Dک�D�� D�&fD�Y�D�D�p 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @y��A33AK33A�ffA���AᙚB33B��B,ffBA33BTffBi33B|  B���B�33B�ffB�33B���B�ffB���B�33B���B�33B�  B�  C ffC33C
L�C�fC�fCffCL�C#L�C(L�C-  C2ffC7� C<L�CA33CFL�CP  CY�fCd�CnL�CxL�C��3C��C�  C�&fC��C��C��3C��fC���C�  C��fC�  C�&fC�33C�  C��C��C��3C��C��fC��3C��C��3C�&fC��3Dy�D��D�fDy�D��D� D� D$y�D)l�D.ffD3y�D8��D=� DB�3DG� DLs3DQ�fDVl�D[��D`y�Dey�Djy�Dos3DtffDy� D�3D�FfD�� D�� D���D�FfD�� D�� D�fD�@ D�� D�� D�  D�6fDڃ3D�ɚD�  D�33D�3D�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�^A�^A�ƨA�ȴA���A���A���A���A���A���A���A��#A�/A�;dA�G�A��;A�  A�-A���A�jA�z�Aޟ�A�A�Aӧ�AиRA�bA��#AƮA�XA��9A���A���A�A�A�O�A��;A�G�A���A�K�A�|�A�(�A�x�A�r�A�-Az�uAo��A^VAXz�AE��AAXA?&�A;"�A6��A0�A*��A$-A"bA �A{A�-AO�A	�A�A�7A �@��@��y@�Q�@��#@�o@���@��#@�+@�Q�@�?}@���@�X@�33@�  @�/@�&�@��/@�v�@��@��#@�dZ@��u@�p�@���@�A�@�Q�@�@���@���@�(�@�M�@� �@�9X@wK�@p�`@ix�@]`B@UV@L(�@E?}@?�P@9�@6$�@/�P@( �@!G�@t�@��@�7@�y1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�^A�^A�ƨA�ȴA���A���A���A���A���A���A���A��#A�/A�;dA�G�A��;A�  A�-A���A�jA�z�Aޟ�A�A�Aӧ�AиRA�bA��#AƮA�XA��9A���A���A�A�A�O�A��;A�G�A���A�K�A�|�A�(�A�x�A�r�A�-Az�uAo��A^VAXz�AE��AAXA?&�A;"�A6��A0�A*��A$-A"bA �A{A�-AO�A	�A�A�7A �@��@��y@�Q�@��#@�o@���@��#@�+@�Q�@�?}@���@�X@�33@�  @�/@�&�@��/@�v�@��@��#@�dZ@��u@�p�@���@�A�@�Q�@�@���@���@�(�@�M�@� �@�9X@wK�@p�`@ix�@]`B@UV@L(�@E?}@?�P@9�@6$�@/�P@( �@!G�@t�@��@�7@�y1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�;B	�;B	�;B	�5B	�5B	�BB	�;B	�HB	�HB	�TB	�`B	�B
0!B\B�B>wBJ�BR�B_;BYBk�Bn�B�'B�}B��B��B��BVB$�BP�B9XBC�B:^B6FB"�BB�NB��B�^B��Be`B�B
ÖB
��B
:^B
VB	�/B	B	��B	�bB	�VB	�\B	�\B	�1B	�+B	t�B	�VB	|�B	x�B	�B	}�B	�DB	�{B	��B	�?B	��B	ĜB	��B	ȴB	��B	��B	�B	�#B	�sB	�B	�B	��B	��B	��B
B
  B
B
B
%B
JB
PB
bB
oB
�B
�B
�B
�B
�B
#�B
$�B
)�B
0!B
2-B
:^B
=qB
@�B
D�B
H�B
N�B
Q�B
VB
ZB
\)B
aHB
hsB
n�B
t�B
w�B
{�B
� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�;B	�;B	�;B	�5B	�5B	�BB	�;B	�HB	�HB	�TB	�`B	�B
1'B\B�B?}BK�BS�B`BB\)Bm�Bs�B�3B��B��B�
B��BbB(�BR�B>wBF�B;dB9XB&�BB�mB��B�qB��BhsB!�B
ŢB
��B
=qB
oB	�BB	ȴB	��B	�oB	�bB	�hB	�oB	�=B	�=B	v�B	�hB	� B	z�B	�%B	�B	�PB	��B	��B	�LB	��B	ƨB	��B	ɺB	��B	�B	�#B	�/B	�yB	�B	�B	��B
  B
  B
B
B
B
%B
+B
PB
VB
hB
uB
�B
�B
�B
�B
 �B
$�B
%�B
+B
1'B
33B
;dB
>wB
A�B
E�B
I�B
O�B
R�B
W
B
[#B
]/B
bNB
iyB
o�B
u�B
x�B
|�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=1.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810290140362008102901403620081029014036200810290154142008102901541420081029015414200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20081016013138  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081016013140  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081016013141  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081016013145  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081016013145  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081016013145  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081016031023                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081020042052  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081020042056  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020042057  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020042101  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020042101  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081020042102  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020061641                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081020042052  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090430062756  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090430062757  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090430062757  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090430062758  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090430062758  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090430062758  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090430062758  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090430062758  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090430063852                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081019104212  CV  DAT$            G�O�G�O�F��a                JM  ARCAJMQC1.0                                                                 20081029014036  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081029014036  CV  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081029015414  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020944  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021013                      G�O�G�O�G�O�                