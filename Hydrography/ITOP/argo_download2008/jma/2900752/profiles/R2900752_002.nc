CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900752 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080918002139  20090520041242                                  2B  A   APEX-SBE 3452                                                   846 @��n�F1   @��nܺ�v@:��n��@a���R1   ARGOS   A   A   A   @�  AffAnffA���Ař�A���B��B  B/��BE33BZ  Bm��B�33B���B���B�ffB���B���B�  B�ffB�  Bڙ�B�33B�ffB�  CffC�3C�3CffC33C�C�C$33C)ffC.�C3L�C8L�C=33CBffCG� CQ33C[33Ce  CoL�Cy�C��3C�� C�� C���C��3C��fC��3C��3C���C�� C�� C���C�� C�C�� C�� Cљ�C�� Cۙ�C���C��C�fC���C��fC���D��D��D�3D�3D�3D�3DٚD$�fD)��D.�fD3�3D8��D=ٚDBٚDG�fDL��DQ��DV� D[��D`� De�3Dj� Do��Dt��Dy�3D�)�D�` D���D��fD�,�D�ffD���D���D�  D�c3D�� D���D�&fD�c3Dڬ�D���D�#3D�` D�3D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33Ak33A�  A�  A�33B  B33B.��BDffBY33Bl��B���B�34B�34B�  B�fgB�34B���B�  Bϙ�B�34B���B�  B���C33C� C� C33C  C�gC�gC$  C)33C-�gC3�C8�C=  CB33CGL�CQ  C[  Cd��Co�Cx�gC���C�ffC�ffC��3C���C���C���C���C�s3C��fC�ffC�s3C�ffC�s3CǦfC̦fCр C֦fCۀ C�3C�s3C��C�3C��C�� D� D��D�fD�fD�fD�fD��D$��D)� D.ٙD3�fD8� D=��DB��DG��DL� DQ� DV�3D[� D`�3De�fDj�3Do��Dt� Dy�fD�#4D�Y�D��4D�� D�&gD�` D��4D��gD��D�\�D���D��gD�  D�\�DڦgD��gD��D�Y�D��D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ĜA�A�t�A���AדuA�ƨA��
A�z�A���A�A���A��A�oA��+A�z�A�5?A��A��A��A�33A�S�A�A��!A�ffA�%A�33A�`BA��7A��`A� �A��;A�M�A���A�/A�~�A�/A���A���A��A�O�A�$�A�33A�VA�G�A��jA�ĜA��A�t�A~v�Az��Ax��Aw"�At��AqXAn�Ak��Ae��A_�^A]�PAXbAR��AP��AF��AB��A<$�A9��A8bA0��A.ȴA,�A?}A1'A�A =q@�%@�7L@�M�@ǥ�@Õ�@�A�@��R@���@��@���@�1@�@�
=@�@�"�@�9X@��H@��j@x�u@tI�@n�@`bN@XQ�@L�@E/@:~�@5�@,z�@)�@#�@�+@�u@�-@^5@�@
��@�@/@M�?��w?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�ĜA�A�t�A���AדuA�ƨA��
A�z�A���A�A���A��A�oA��+A�z�A�5?A��A��A��A�33A�S�A�A��!A�ffA�%A�33A�`BA��7A��`A� �A��;A�M�A���A�/A�~�A�/A���A���A��A�O�A�$�A�33A�VA�G�A��jA�ĜA��A�t�A~v�Az��Ax��Aw"�At��AqXAn�Ak��Ae��A_�^A]�PAXbAR��AP��AF��AB��A<$�A9��A8bA0��A.ȴA,�A?}A1'A�A =q@�%@�7L@�M�@ǥ�@Õ�@�A�@��R@���@��@���@�1@�@�
=@�@�"�@�9X@��H@��j@x�u@tI�@n�@`bN@XQ�@L�@E/@:~�@5�@,z�@)�@#�@�+@�u@�-@^5@�@
��@�@/@M�?��w?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B~�B� B�=B	�B	33B	�-B	��B	�BB
B
+B
H�B
J�B
W
B
jB
~�B
�B
��BhB%�B(�B-B-B&�B1'B1'B1'B�B�B�B�B�B{B�B{BuB\B
=B
��B
��B
�B
�B
�B
�BB
�B
��B
ŢB
��B
�'B
��B
�7B
� B
w�B
l�B
^5B
N�B
@�B
&�B
\B
B	�B	��B	ŢB	��B	�DB	u�B	k�B	dZB	L�B	B�B	33B��B�XB��B�%Bk�Bn�BffBo�Br�B�B�{B�{B��B�RB�qBɺB�B�5B�mB��B	+B	JB	!�B	1'B	=qB	dZB	y�B	��B	��B	B	��B	�mB	�B	��B

=B
�B
 �B
)�B
33B
=qB
F�B
L�B
S�B
\)B
]/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B~�B� B�=B	�B	33B	�-B	��B	�BB
B
+B
H�B
J�B
W
B
jB
~�B
�B
��BhB%�B(�B-B-B&�B1'B1'B1'B�B�B�B�B�B{B�B{BuB\B
=B
��B
��B
�B
�B
�B
�BB
�B
��B
ŢB
��B
�'B
��B
�7B
� B
w�B
l�B
^5B
N�B
@�B
&�B
\B
B	�B	��B	ŢB	��B	�DB	u�B	k�B	dZB	L�B	B�B	33B��B�XB��B�%Bk�Bn�BffBo�Br�B�B�{B�{B��B�RB�qBɺB�B�5B�mB��B	+B	JB	!�B	1'B	=qB	dZB	y�B	��B	��B	B	��B	�mB	�B	��B

=B
�B
 �B
)�B
33B
=qB
F�B
L�B
S�B
\)B
]/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080918002001  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080918002139  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080918002140  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080918002144  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080918002144  QCP$                G�O�G�O�G�O�            FB40JA  ARUP                                                                        20080918013508                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080915041706  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090520040959  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090520040959  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090520040959  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090520041001  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090520041001  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090520041001  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090520041001  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090520041001  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090520041242                      G�O�G�O�G�O�                