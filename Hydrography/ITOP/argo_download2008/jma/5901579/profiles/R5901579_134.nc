CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   B   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  4�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       54   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6<   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  7D   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  :�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;(   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  <0   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       <t   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  =|   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C�   CALIBRATION_DATE            	             
_FillValue                  ,  F�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    F�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    F�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    F�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    F�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  F�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G(   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G8   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G<   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         GL   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         GP   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        GT   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    GXArgo profile    2.2 1.2 19500101000000  5901579 PALAU2008                                                       JAMSTEC Sato Naoki                                              PRES            TEMP            PSAL               �A   JA  20081016045244  20091207062840                                  2B  A   APEX-SBE 3605                                                   846 @��8\�1   @��9���@2t�j~�@_�     1   ARGOS   A   A   A   @�ffA��AfffA�  A���A陚B	��BffB0  BE��BZ��BnffB�ffB�ffB�33B���B�ffB�33B�ffB�33B���B�33B���B���B���C� CL�C� C��CffC� CL�C$��C)�3C.� C3ffC8L�C=�3CBL�CG� CQ� C[�3C��3C��fC���C���C��fC���C���C���C�� C�� C�� C�CǦfC�� CѦfCֳ3C۳3C�fC�Y�C�s3C� C��C�ٚC�@ 111111111111111111111111111111111111111111111111111111111111111111  @�33A  Ad��A�33A�  A���B	34B  B/��BE34BZfgBn  B�33B�33B�  B�fgB�33B�  B�33B�  BЙ�B�  B䙚BB�fgCffC33CffC� CL�CffC33C$�3C)��C.ffC3L�C833C=��CB33CGffCQffC[��C��fC���C�� C���C���C�� C���C���C�s3C��3C��3C�CǙ�C̳3Cљ�C֦fCۦfC���C�L�C�ffC�s3C� C���C�33111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�hA�uA�x�A�r�A�p�A�ffA�  A�~�A�/A�`BA䝲A��A�9XA�z�A�G�Aװ!A�hsA�7LA��TA�33AЬAϮA�z�A�ZA�7LA��AìA�9XA��-A��A�  A��A�ƨA��FA��A�ffA�33A��DA�ƨA���A��
A��A��A�Q�A���A�p�A�\)A~�jAw��Ar��Aj^5Ab��AV�AO�PAI��AA�;A:�yA4��A.-A,  A(M�A%��A 1AoA��111111111111111111111111111111111111111111111111111111111111111111  A�A�hA�uA�x�A�r�A�p�A�ffA�  A�~�A�/A�`BA䝲A��A�9XA�z�A�G�Aװ!A�hsA�7LA��TA�33AЬAϮA�z�A�ZA�7LA��AìA�9XA��-A��A�  A��A�ƨA��FA��A�ffA�33A��DA�ƨA���A��
A��A��A�Q�A���A�p�A�\)A~�jAw��Ar��Aj^5Ab��AV�AO�PAI��AA�;A:�yA4��A.-A,  A(M�A%��A 1AoA��111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B%B%B%BhBoB{B�B1'B]/BffBjBiyB�B�=B	ȴB
DB
<jB
o�B
�PB
ŢB
�sBB+B0!B<jBhsBgmB� B�VB�uB��B�B�!B�?B�B��B�}B�)B��B��B�!B�B1'B�B
��B
�5B
��B
�'B
��B
w�B
]/B
2-B
DB	�
B	�dB	��B	�+B	o�B	aHB	Q�B	L�B	>wB	7LB	)�B	1'B	2-111111111111111111111111111111111111111111111111111111111111111111  B%B%B%BhBoB{B�B1'B]/BffBjBiyB�B�=B	ȴB
DB
<jB
o�B
�PB
ŢB
�sBB+B0!B<jBhsBgmB� B�VB�uB��B�B�!B�?B�B��B�}B�)B��B��B�!B�B1'B�B
��B
�5B
��B
�'B
��B
w�B
]/B
2-B
DB	�
B	�dB	��B	�+B	o�B	aHB	Q�B	L�B	>wB	7LB	)�B	1'B	2-111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081016045237  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081016045244  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081016045245  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081016045249  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081016045249  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081016045249  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081016045249  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20081016062611                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081016045237  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090522082002  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090522082003  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090522082003  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090522082004  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090522082004  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090522082004  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090522082004  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090522082004  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090522082004  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090522082004  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090522082819                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207052713  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207053430  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207053430  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207053431  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207053432  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207053432  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207053432  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207053432  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207053432  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091207062840                      G�O�G�O�G�O�                