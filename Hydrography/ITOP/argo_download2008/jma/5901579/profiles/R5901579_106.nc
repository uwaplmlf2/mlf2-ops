CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   ?   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  4�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  5   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  6   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  7   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  7L   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  8H   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  9�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  :�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  :�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  ;�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  <�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =(   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @(   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C(   CALIBRATION_DATE            	             
_FillValue                  ,  F(   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    FT   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    FX   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    F\   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    F`   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Fd   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    F�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    F�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    F�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         F�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         F�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        F�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    F�Argo profile    2.2 1.2 19500101000000  5901579 PALAU2008                                                       JAMSTEC Sato Naoki                                              PRES            TEMP            PSAL               jA   JA  20080918051641  20091207062749                                  2B  A   APEX-SBE 3605                                                   846 @��8ʆB1   @��@�i�@1�5?|�@_�-V1   ARGOS   B   B   B   @�ffA  Ah  A�33A���A�ffB
ffB��B2  BFffBZ��Bo��B���B�33B���B�ffB�ffB���B�  BǙ�Bљ�B�ffB�ffG�O�C��C�3C��C� C$� CQ33CB� CG�3CQ� C[��Ce��CoffCy33C�� C�ٚC���C�� C��fC��3C��3C��3C�� C��3C���C���C��3C³3Cǳ3C�� CѦfC���CۦfC�� C�fC�fC�s3C��3C���C�@ 111111111111111111111114111114111111111111111111111111111111111 @�33AffAfffA�ffA�  A陙B
  BfgB1��BF  BZfgBo34B���B�  B�fgB�33B�33B�fgB���B�fgB�fgB�33B�33G�O�C� C��C� CffC$ffCQ�CBffCG��CQffC[� Ce� CoL�Cy�C��3C���C�� C��3C���C��fC��fC��fC�s3C��fC�� C�� C��fC¦fCǦfC̳3Cљ�C�� Cۙ�C�s3C噙CꙙC�ffC��fC���C�33111111111111111111111114111114111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�RA���A���A���A���A���A���A��;A��;A���A�A���A�K�A���A�`BA�t�Aۡ�Aڇ+A�1A��
A�?}Aԟ�G�O�A�  A�VA�/A��A�G�O�A��wA��\A��mA���A��A�|�A�%A��A��/A�M�A�A�A�  A�p�A���A��A{�Aw�mAt�!AqXAf�Ae
=A`��A\{AR�AJA@�DA:=qA7��A0��A(�A%A ��A��A�111111111111111111111194111193111111111111111111111111111111111 A�RA���A���A���A���A���A���A��;A��;A���A�A���A�K�A���A�`BA�t�Aۡ�Aڇ+A�1A��
A�?}Aԟ�G�O�A�  A�VA�/A��A�G�O�A��wA��\A��mA���A��A�|�A�%A��A��/A�M�A�A�A�  A�p�A���A��A{�Aw�mAt�!AqXAf�Ae
=A`��A\{AR�AJA@�DA:=qA7��A0��A(�A%A ��A��A�111111111111111111111194111193111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�\B�bB�bB�\B�\B�VB�VB�bB�hB��B�B;dBk�BǮB	�!B
>wB
^5B
�=B
�oB
�RB
�#B$�B�G�O�BQ�BdZBdZBe`G�O�B�-B�B�sBB��B�B��B�uBe`B;dB�B	7B
�B
ŢB
��B
�7B
u�B
e`B
S�B
%�B
�B
%B	�B	ƨB	��B	�%B	s�B	l�B	YB	;dB	D�B	9XB	(�B	o111111111111111111111114111193111111111111111111111111111111111 B�\B�bB�bB�\B�\B�VB�VB�bB�hB��B�B;dBk�BǮB	�!B
>wB
^5B
�=B
�oB
�RB
�#B$�B�G�O�BQ�BdZBdZBe`G�O�B�-B�B�sBB��B�B��B�uBe`B;dB�B	7B
�B
ŢB
��B
�7B
u�B
e`B
S�B
%�B
�B
%B	�B	ƨB	��B	�%B	s�B	l�B	YB	;dB	D�B	9XB	(�B	o111111111111111111111114111193111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080918051631  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080918051641  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080918051642  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080918051647  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080918051647  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080918051647  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080918051647  QCF$                G�O�G�O�G�O�            4100JA  ARUP                                                                        20080918060511                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080918051631  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090522081844  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090522081844  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090522081845  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090522081846  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090522081846  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090522081846  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090522081846  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090522081846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090522081846  QCF$                G�O�G�O�G�O�             100JA  ARGQaqcp2.8b                                                                20090522081846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090522081846  QCF$                G�O�G�O�G�O�             100JA  ARGQrqcpt16b                                                                20090522081846  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090522082722                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207052708  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207053359  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207053359  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207053400  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207053401  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207053401  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207053401  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207053401  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207053401  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8c                                                                20091207053401  QCF$                G�O�G�O�G�O�             100JA  ARGQaqcp2.8c                                                                20091207053401  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207053401  QCF$                G�O�G�O�G�O�             100JA  ARGQrqcpt16b                                                                20091207053401  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091207062749                      G�O�G�O�G�O�                