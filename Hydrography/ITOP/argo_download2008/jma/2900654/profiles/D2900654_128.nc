CDF   .   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900654 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               �A   JA  20080808025650  20090907082845  A9_67024_128                    2C  D   APEX-SBE 2972                                                   846 @�����1   @��캘vW@;�bM��@`��1'1   ARGOS   A   A   A   @���A��A�33A�33BffBE��Bl  B���B�ffB�33Bƙ�B�ffB���CL�CL�CL�CffC)33C3L�C=� CGL�C[ffCo� C�s3C�s3C��3C��fC�� C���C���Cǳ3Cр C۳3C噚C�� C��3D� D��D��D� D� D�3D�3D$� D)�3D.�3D3�fD8��D=�fDB��DGٚDN�DT9�DZ��D`�fDg�DmFfDs��Dy� D��D�l�D���D��fD�  D�c3D�� D�l�D��fD�ffD��fD�i�D�� D�31111111111111111111111111111111111111111111111111111111111111111111111111   @�ffA33A�ffA�ffB  BE33Bk��B���B�33B�  B�ffB�33B홚C33C33C33CL�C)�C333C=ffCG33C[L�CoffC�ffC�ffC��fC���C��3C�� C���CǦfC�s3CۦfC��C�3C��fDٚD�fD�fDٚD��D��D��D$ٚD)��D.��D3� D8�3D=� DB�fDG�3DNfDT33DZ�fD`� DgfDm@ Ds�3Dy��D��D�i�D���D��3D��D�` D���D�i�D��3D�c3D��3D�ffD���D� 1111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A훦A�A���Aŗ�A�bA��TA�p�A�5?A�S�A���A��jA��A�=qA�ĜA���A�v�A��`A��A��9A�1'A�ZA�7LA�bA�M�A��mA���A�dZA�jA�9XA���A��A|�yAwp�An��Af�`A^{AU|�ALVAAƨA6~�A*��A A1'A~�Ap�@�"�@��/@���@ش9@�-@���@�&�@�1@��@��@�b@��m@��@�@xA�@i��@XĜ@P1'@IG�@?�;@3��@&��@�@�@"�@@G�@7L1111111111111111111111111111111111111111111111111111111111111111111111111   A훦A�A���Aŗ�A�bA��TA�p�A�5?A�S�A���A��jA��A�=qA�ĜA���A�v�A��`A��A��9A�1'A�ZA�7LA�bA�M�A��mA���A�dZA�jA�9XA���A��A|�yAwp�An��Af�`A^{AU|�ALVAAƨA6~�A*��A A1'A~�Ap�@�"�@��/@���@ش9@�-@���@�&�@�1@��@��@�b@��m@��@�@xA�@i��@XĜ@P1'@IG�@?�;@3��@&��@�@�@"�@@G�@7L1111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�B
��B
�7B
e`B
49B
y�B
��BB+B33B=qBD�BB�BC�BF�BE�BC�B?}B=qB:^B5?B.B)�B&�B �B�B1B
��B
�TB
��B
�B
��B
�B
]/B
9XB
hB	�yB	ÖB	�PB	]/B	#�B�B�}B��B�B�B��B��B�\B�DB�uB�B��B��B�B	B	oB	,B	C�B	bNB	�%B	��B	�B	�dB	��B	�B
hB
,B
A�B
P�B
]/B
iyB
iy1111111111111111111111111111111111111111111111111111111111111111111111111   B
�B
�
B
�uB
k�B
8RB
|�B
��BB-B49B=qBD�BB�BC�BF�BE�BC�B?}B=qB:^B5?B.B)�B&�B �B�B1B
��B
�ZB
��B
�B
��B
�B
^5B
:^B
oB	�B	ĜB	�VB	_;B	$�B��B��B��B�B�!B��B��B�bB�JB�{B�B��B��B�B	B	oB	,B	C�B	bNB	�%B	��B	�B	�dB	��B	�B
hB
,B
A�B
P�B
]/B
iyB
iy1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808160248382008081602483820080816024838200808160306062008081603060620080816030606200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080808025647  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080808025650  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080808025651  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080808025651  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080808025655  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080808025655  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080808025655  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080808025655  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080808025656  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080808030728                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080812042000  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080812042005  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080812042005  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080812042006  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080812042010  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080812042010  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080812042010  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080812042010  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080812042010  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080812051210                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828003203  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828003203  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828003207  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828003207  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828003208  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828003208  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828003208  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828021034                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080811185543  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416051539  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416051539  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416051539  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416051540  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416051541  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416051541  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416051541  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416051541  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416051541  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416052334                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080811113011  CV  DAT$            G�O�G�O�F�7C                JM  ARCAJMQC1.0                                                                 20080816024838  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080816024838  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080816030606  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090907082724  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090907082845                      G�O�G�O�G�O�                