CDF   .   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900654 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               �A   JA  20080818012542  20090907082832  A9_67024_130                    2C  D   APEX-SBE 2972                                                   846 @��fE�,�1   @��l����@<����@`���n�1   ARGOS   A   A   A   @���A33A���A�ffB��BD��Bn  B���B�  B�  B�ffB���B���C �fC
��C�C  C)33C3L�C=� CG33C[� CoffC��3C���C�� C�� C��3C���C��fCǳ3Cь�C�s3C� C���C�� D�3D��D�fD�3DٚDٚD��D$�3D)��D.�3D3��D8�fD=ٚDB�3DG�3DN�DTY�DZy�D`�fDg  DmFfDs� Dy�3D�&fD�c3D��3D�ٚD�&fD�ffD���D�c3D���D�ffD��3D�c3D��3D�31111111111111111111111111111111111111111111111111111111111111111111111111   @�33AffA�33A�  B��BC��Bl��B�33B�ffB�ffB���B�33B�33C ��C
� C��C�3C(�fC3  C=33CF�fC[33Co�C���C�s3C�Y�C�Y�C���C��fC�� Cǌ�C�ffC�L�C�Y�C�fC���D� D��D�3D� D�fD�fD��D$� D)��D.� D3��D8�3D=�fDB� DG� DM��DTFfDZffD`�3Dg�Dm33Ds��Dy� D��D�Y�D���D�� D��D�\�D��3D�Y�D��3D�\�D�ٚD�Y�D��D�	�1111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A�l�A��TA���A��^A��A��HA�
=A�bNA�K�A�hsA�9XA��mA�G�A�G�A�  A�?}A���A�
=A��A�bA��DA�9XA��-A�1A��A��;A�M�A��A��RA}��Ax�Ap(�AdA�A[G�AOXAD��A=K�A5��A&5?A�TAA
�`A M�@��@��;@䛦@��
@�z�@�M�@�Ĝ@�V@���@�9X@��@��`@��h@�%@��F@s��@a�^@T��@Pb@Hb@@bN@0�9@#C�@&�@-@@�-@�^@��1111111111111111111111111111111111111111111111111111111111111111111111111   A���A�l�A��TA���A��^A��A��HA�
=A�bNA�K�A�hsA�9XA��mA�G�A�G�A�  A�?}A���A�
=A��A�bA��DA�9XA��-A�1A��A��;A�M�A��A��RA}��Ax�Ap(�AdA�A[G�AOXAD��A=K�A5��A&5?A�TAA
�`A M�@��@��;@䛦@��
@�z�@�M�@�Ĝ@�V@���@�9X@��@��`@��h@�%@��F@s��@a�^@T��@Pb@Hb@@bN@0�9@#C�@&�@-@@�-@�^@��1111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�B��B	#�B	�1B
<jB
��B
�FB
��B
�yBBbB�B#�B=qB=qB@�BC�BE�BC�BB�B>wB5?B.B(�B�B�B+B
�B
��B
�jB
��B
�7B
`BB
-B
  B	ɺB	��B	}�B	]/B	�B	B�B��B��B��B�B��B�PB�hB��B��B�BŢB�ZB��B		7B	�B	<jB	S�B	r�B	�\B	��B	�B	�qB	��B	��B
{B
.B
>wB
Q�B
^5B
iyB
iy1111111111111111111111111111111111111111111111111111111111111111111111111   B��B��B	&�B	�hB
?}B
��B
�LB
��B
�B%BhB�B$�B>wB>wB@�BC�BE�BD�BB�B>wB5?B.B(�B�B�B1B
�B
��B
�qB
��B
�=B
aHB
.B
B	��B	��B	~�B	_;B	�B	B�B��B��B��B�B��B�VB�oB��B��B�BƨB�ZB��B		7B	�B	<jB	S�B	r�B	�\B	��B	�B	�qB	��B	��B
{B
.B
>wB
Q�B
^5B
iyB
iy1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.3(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808260244442008082602444420080826024444200808260301042008082603010420080826030104200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080818012539  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080818012542  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080818012542  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080818012543  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080818012547  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080818012547  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080818012547  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080818012547  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080818012548  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080818014008                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080822041846  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080822041850  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080822041851  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080822041851  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080822041855  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080822041855  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080822041855  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080822041855  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080822041856  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080822050831                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828003214  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828003214  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828003218  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828003218  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828003218  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828003218  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828003218  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828021119                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080821185604  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416051544  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416051544  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416051544  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416051544  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416051546  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416051546  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416051546  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416051546  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416051546  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416052348                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080821111814  CV  DAT$            G�O�G�O�F�KC                JM  ARCAJMQC1.0                                                                 20080826024444  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080826024444  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080826030104  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090907082704  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090907082832                      G�O�G�O�G�O�                