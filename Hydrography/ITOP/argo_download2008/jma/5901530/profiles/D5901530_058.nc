CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901530 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               :A   JA  20081004200102  20090908021442  A9_66111_058                    2C  D   APEX-SBE 2820                                                   846 @��I/Ɉ1   @��R8Q�@1��-V@a`Q��1   ARGOS   A   A   A   @���A��Ac33A���A�ffA�33B	��B��B2  BD��BZ  Bn  B�ffB�ffB���B�33B���B���B�33B�  B�33B�  B�  B�  B�ffC33CL�CL�C33CL�C��C�3C$��C)� C.L�C3�C8  C=�CBL�CGffCQ� C[ffCeffCoL�CyL�C�� C�� C��3C��fC��3C�� C�� C�� C��fC��fC�� C���C���C�Cǳ3C̙�Cѳ3C֌�Cۙ�C�� C��CꙚC�fC��fC��fD�3D�fD�fD�3D��D� DٚD$��D)�3D.�3D3�3D8�3D=�fDB�fDG�fDLٚDQ��DV��D[� D`ٚDe� Dj�fDo� DtٚDy�fD�&fD�c3D��fD���D��D�ffD��3D���D�  D�` D���D���D��D�ffDک�D���D�#3D�i�D�3D�P 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA��A`  A�  A���A陚B��B��B133BD  BY33Bm33B�  B�  B�33B���B�ffB�33B���Bƙ�B���Bڙ�B䙚BB�  C  C�C�C  C�CffC� C$ffC)L�C.�C2�fC7��C<�fCB�CG33CQL�C[33Ce33Co�Cy�C��fC��fC���C���C���C�ffC��fC��fC���C���C��fC�� C�s3C CǙ�C̀ Cљ�C�s3Cۀ C�fC�s3C� C��C��C���D�fD��DٚD�fD� D�3D��D$��D)�fD.�fD3�fD8�fD=��DB��DG��DL��DQ� DV��D[�3D`��De�3Dj��Do�3Dt��Dy��D�  D�\�D�� D��fD�fD�` D���D��fD��D�Y�D��fD��fD�3D�` Dڣ3D��fD��D�c3D��D�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A�hA�~�A�t�A�dZA�bNA�bNA�;dA�-A�oA���A�jA�-A�ƨA��HA��A�9XA�bNAک�A�jA���AӁAѲ-Aϰ!A�ffA̅A��A��A��A�dZAŬAÃA��A� �A�/A��A��9A��^A�jA��^A��A�jA�E�A��wA�  A�A��A���A�dZA���A�S�A~Q�As"�Ah�uAa��AWC�ANA�AF�/A?7LA5|�A*�A#G�A|�A5?A9XAJA�-A	+AE�A�P@�hs@�?}@�%@�ff@Ͳ-@�l�@�V@�l�@��F@��D@�l�@���@���@��\@��R@�r�@�Q�@�"�@�E�@��-@���@���@�E�@�b@�z�@�@t(�@fV@[�
@R-@J��@C�@=?}@3��@-p�@&��@"M�@I�@ �@(�@�
@�@
��@�;@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A�hA�~�A�t�A�dZA�bNA�bNA�;dA�-A�oA���A�jA�-A�ƨA��HA��A�9XA�bNAک�A�jA���AӁAѲ-Aϰ!A�ffA̅A��A��A��A�dZAŬAÃA��A� �A�/A��A��9A��^A�jA��^A��A�jA�E�A��wA�  A�A��A���A�dZA���A�S�A~Q�As"�Ah�uAa��AWC�ANA�AF�/A?7LA5|�A*�A#G�A|�A5?A9XAJA�-A	+AE�A�P@�hs@�?}@�%@�ff@Ͳ-@�l�@�V@�l�@��F@��D@�l�@���@���@��\@��R@�r�@�Q�@�"�@�E�@��-@���@���@�E�@�b@�z�@�@t(�@fV@[�
@R-@J��@C�@=?}@3��@-p�@&��@"M�@I�@ �@(�@�
@�@
��@�;@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
\B
VB
\B
\B
VB
VB
VB
DB
PB

=B
JB
	7B
�B
ǮB
�/B
��B�BF�BQ�B~�B�PB��B�B�^B�^BɺB��B�B�)B�mB��BB�B"�B!�B'�B'�B%�B!�B�BB�TB��B�Bp�B:^B&�B
�B
��B
��B
�uB
q�B
9XB
+B	�ZB	�LB	��B	~�B	`BB	?}B	�B��B�sB�mB�#B�)B�B�
B�B�B�HB��B��B	B	DB	"�B	H�B	|�B	�VB	��B	��B	��B	�FB	ÖB	��B	��B	�BB	�TB	�`B	�B	�B	��B
B

=B

=B
�B
�B
0!B
9XB
@�B
E�B
J�B
M�B
R�B
ZB
`BB
dZB
jB
n�B
r�B
r�B
u�B
{�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
\B
VB
\B
\B
VB
VB
VB
DB
PB

=B
JB
	7B
�B
��B
�BBB"�BG�BS�B� B�bB��B�B�^B�jBɺB��B�
B�/B�yB��BB!�B#�B$�B)�B)�B%�B"�B�BB�ZB��B�!Bs�B;dB(�B
��B
��B
��B
��B
t�B
<jB
	7B	�mB	�XB	��B	�B	bNB	B�B	�B	B�B�sB�)B�/B�
B�B�B�
B�NB��B��B	B	JB	"�B	H�B	|�B	�VB	��B	��B	��B	�FB	ÖB	��B	��B	�BB	�TB	�`B	�B	�B	��B
B

=B

=B
�B
�B
0!B
9XB
@�B
E�B
J�B
M�B
R�B
ZB
`BB
dZB
jB
n�B
r�B
r�B
u�B
{�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810171626392008101716263920081017162639200810171638452008101716384520081017163845200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20081004200044  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081004200102  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081004200106  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081004200114  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081004200114  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081004200116  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081004211911                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081008163120  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081008163124  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081008163125  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081008163129  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081008163129  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081008163129  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20081008163129  CV  TIME            G�O�G�O�                    JA  ARGQrelo2.1                                                                 20081008163129  CV  LAT$            G�O�G�O�A��                JA  ARGQrelo2.1                                                                 20081008163129  CV  LON$            G�O�G�O�CN                JA  ARUP                                                                        20081008191028                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081008163120  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422064347  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422064348  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422064348  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422064349  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422064349  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422064349  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422064349  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422064349  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422064642                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081008004050  CV  DAT$            G�O�G�O�F��W                JM  ARCAJMQC1.0                                                                 20081017162639  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081017162639  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081017163845  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021412  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021442                      G�O�G�O�G�O�                