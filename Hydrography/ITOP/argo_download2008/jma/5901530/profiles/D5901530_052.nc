CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901530 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               4A   JA  20080805185824  20090908021431  A9_66111_052                    2C  D   APEX-SBE 2820                                                   846 @��M�0�x1   @��Or(3�@0z�G�@a}7KƧ�1   ARGOS   A   A   A   @���AffA`  A���A�33A���B	��B��B133BFffBXffBm��B�  B�  B���B�  B���B�33B���B�  B�ffBڙ�B䙚B���B�33C��CffC� C� C� C� C� C$  C)  C.L�C3  C8L�C=�CBL�CGffCQ�3C[� Ce� Co�Cy� C���C��fC��fC���C�� C��3C�� C�s3C�� C���C�� C���C���C³3CǦfC̳3Cѳ3C֙�CۦfC�fC��C��C�s3C��fC���D��D��D� D� D�fD�3DٚD$��D)��D.ٚD3ٚD8ٚD=�3DB� DG�3DL�3DQ�fDV� D[��D`ٚDeٚDjٚDo� Dt� Dy�3D�#3D�p D���D�� D�  D�` D��fD��3D�)�D�i�D��fD���D��D�\�DڦfD��3D�  D�ffD�D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��A^ffA���A�ffA�  B	33BffB0��BF  BX  Bm33B��B���B���B���B�ffB�  B���B���B�33B�ffB�ffBB�  C� CL�CffCffCffCffCffC#�fC(�fC.33C2�fC833C=  CB33CGL�CQ��C[ffCeffCo  CyffC�� C���C���C���C��3C��fC�s3C�ffC��3C�� C��3C���C�� C¦fCǙ�C̦fCѦfC֌�Cۙ�C���C� C� C�ffC���C���D�fD�fD��DٚD� D��D�3D$�3D)�fD.�3D3�3D8�3D=��DB��DG��DL��DQ� DV��D[�fD`�3De�3Dj�3DoٚDt��Dy��D�  D�l�D��fD���D��D�\�D��3D�� D�&fD�ffD��3D�ٚD��D�Y�Dڣ3D�� D��D�c3D�fD��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�DA�jA�O�A�;dA�t�AꟾA��A�ffA�l�A�\A�DA���A�A�;dA��A�VA�?}A�bA�hsA�{A�{Aִ9A�ffAӥ�A�x�A�$�Ả7A�\)AˑhA�1'A�x�A�$�Aƴ9A�|�A�bA��;A�S�A�I�A���A�E�A�bNA��DA���A���A��yA��mA�oA�I�A�A�A��TA�ffA�z�A��Av^5Al�!Aa��A[�AU�AQp�AJVAD�!A;�mA3&�A0�HA'ƨA$E�A�^A��A�RA�;A��@��m@�u@�@�5?@��/@д9@�bN@�p�@��@�K�@��@��u@�M�@�l�@�1'@��j@���@�{@��R@�1'@���@�J@�n�@��T@��H@+@w\)@l1@`�@X1'@Ol�@G�@CdZ@8Q�@/�@*-@%/@!��@Z@5?@�@j@	hs@�+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�DA�jA�O�A�;dA�t�AꟾA��A�ffA�l�A�\A�DA���A�A�;dA��A�VA�?}A�bA�hsA�{A�{Aִ9A�ffAӥ�A�x�A�$�Ả7A�\)AˑhA�1'A�x�A�$�Aƴ9A�|�A�bA��;A�S�A�I�A���A�E�A�bNA��DA���A���A��yA��mA�oA�I�A�A�A��TA�ffA�z�A��Av^5Al�!Aa��A[�AU�AQp�AJVAD�!A;�mA3&�A0�HA'ƨA$E�A�^A��A�RA�;A��@��m@�u@�@�5?@��/@д9@�bN@�p�@��@�K�@��@��u@�M�@�l�@�1'@��j@���@�{@��R@�1'@���@�J@�n�@��T@��H@+@w\)@l1@`�@X1'@Ol�@G�@CdZ@8Q�@/�@*-@%/@!��@Z@5?@�@j@	hs@�+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��B��B��B��B�?B��B��B��B�)B��B	+B	/B	}�B	��B	�'B
1'B
q�B
��B
��B
�RB
�sBBJBPB\BcTBe`BjB�B�hB�/B�BB�B��BVBPB+BbB�B��B�)BǮB�?B��B~�BdZBR�B49B�BB
�ZB
�jB
��B
hsB
2-B	��B	�5B	ǮB	�B	�JB	n�B	H�B	&�B	�B��B�B�fB�#B��BɺB��B�`B��B	VB	�B	(�B	B�B	o�B	� B	�B	��B	��B	�B	�?B	�jB	ÖB	��B	��B	�BB	�`B	�B	�B	��B
  B
	7B
�B
!�B
+B
1'B
8RB
=qB
B�B
E�B
I�B
O�B
W
B
\)B
`BB
cTB
hsB
n�B
s�B
w�B
z�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B��B��B��B�?B��B��B��B�)B��B	+B	0!B	}�B	��B	�FB
49B
t�B
��B
��B
�^B
�yBBPBbBoBcTBe`Bk�B�%B�hB�5B�NB�B��B\BVB	7BuB"�BB�;BɺB�LB��B�Be`BT�B5?B�BB
�fB
�wB
��B
jB
5?B
B	�;B	ȴB	�B	�PB	p�B	J�B	'�B	�B��B�B�mB�)B��B��B��B�fB��B	VB	�B	)�B	B�B	o�B	� B	�B	��B	��B	�B	�?B	�jB	ÖB	��B	��B	�BB	�`B	�B	�B	��B
  B
	7B
�B
!�B
+B
1'B
8RB
=qB
B�B
E�B
I�B
O�B
W
B
\)B
`BB
cTB
hsB
n�B
s�B
w�B
z�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808181432442008081814324420080818143244200808181448062008081814480620080818144806200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080805185821  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080805185824  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080805185825  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080805185829  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080805185829  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080805185829  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080805191239                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080809163836  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080809163851  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080809163854  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080809163902  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080809163903  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080809163904  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080809203554                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080809163836  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422064334  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422064334  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422064335  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422064336  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422064336  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422064336  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422064336  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422064336  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422064659                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080808225216  CV  DAT$            G�O�G�O�F�2v                JM  ARCAJMQC1.0                                                                 20080818143244  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080818143244  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080818144806  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021356  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021431                      G�O�G�O�G�O�                