CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900684 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ;A   JA  20080906051027  20090424020457                                  2B  A   APEX-SBE 2810                                                   846 @��=z�H1   @��=P6�@:��+@bk��$�1   ARGOS   A   A   A   @���A��Al��A���A�  A�  B
ffB  B0ffBDffBY33Bl��B�33B�  B���B���B���B�33B���B�  B���B�33B�  B�  B�33CL�C  C33CffC�CffC  C$L�C)�C.� C3ffC8ffC=ffCBL�CG33CQL�C[ffCeffCo� CyL�C��3C���C���C��3C���C�s3C���C�ffC�s3C���C��fC��fC�� C³3CǙ�C̦fC�s3C�s3CۦfC���C� C��C�3C��fC�� D�3D� D� D� D��D��D��D$� D)�fD.ٚD3ٚD8�3D=�fDB�fDG�3DL�fDQ�fDV��D[ٚD`� De� Dj�3Do�3Dt�3Dy�fD��D�i�D���D��fD��D�ffD���D��3D�fD�p D��3D��D�  D�c3Dڣ3D��fD��D�VfD�fD�Y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA��Ai��A�33A�ffA�ffB	��B33B/��BC��BXffBl  B��B���B�34B�fgB�34B���B�fgBƙ�B�fgB���B䙚BB���C�C��C  C33C�gC33C��C$�C(�gC.L�C333C833C=33CB�CG  CQ�C[33Ce33CoL�Cy�C���C�s3C�� C���C�s3C�Y�C�� C�L�C�Y�C�� C���C���C��fC�Cǀ Č�C�Y�C�Y�Cی�C�� C�ffC�s3CC��C��fD�fD�3D�3D�3D��D��D� D$�3D)��D.��D3��D8�fD=��DBٙDG�fDL��DQ��DV��D[��D`�3De�3Dj�fDo�fDt�fDy��D�gD�c4D��gD�� D�gD�` D��gD���D� D�i�D���D��4D��D�\�Dڜ�D�� D�4D�P D� D�S41111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�!A��/A֑hA�7LAŰ!A��9A�$�A�?}A��A�=qA�=qA��RA��A�33A�|�A��`A�I�A�ZA�t�A�1'A� �A���A���A���A�+A�K�A���A�oA�jA��A��DA�oA��^A�I�A�5?A��TA��+A�K�A��
A�S�A�ĜA�l�A���A��A��A��hA�l�A���A��RA� �A�5?A�ƨA���A}�wAz��At��Ap�+Ak+Af�!Ab �A[7LAY33AUXAP �AL�DAEl�A?�TA5A-O�A ��A9XA�-A"�@���@��@��@Гu@�~�@��+@��@�hs@�"�@�o@�X@�A�@�J@��F@��@���@�hs@~ff@wl�@tZ@o|�@d��@U�h@Lz�@Ct�@?��@7|�@5�@/l�@( �@#S�@��@�y@��@`B@Q�@�D@�^?���?�C�?��+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A�!A��/A֑hA�7LAŰ!A��9A�$�A�?}A��A�=qA�=qA��RA��A�33A�|�A��`A�I�A�ZA�t�A�1'A� �A���A���A���A�+A�K�A���A�oA�jA��A��DA�oA��^A�I�A�5?A��TA��+A�K�A��
A�S�A�ĜA�l�A���A��A��A��hA�l�A���A��RA� �A�5?A�ƨA���A}�wAz��At��Ap�+Ak+Af�!Ab �A[7LAY33AUXAP �AL�DAEl�A?�TA5A-O�A ��A9XA�-A"�@���@��@��@Гu@�~�@��+@��@�hs@�"�@�o@�X@�A�@�J@��F@��@���@�hs@~ff@wl�@tZ@o|�@d��@U�h@Lz�@Ct�@?��@7|�@5�@/l�@( �@#S�@��@�y@��@`B@Q�@�D@�^?���?�C�?��+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B
C�B	��B
oB
+B
]/B
hsB
}�B
�B
��B
��B
�FB
ȴB
��BBhB�B%�B)�B1'B49B33B2-B0!B/B-B+B(�B%�B%�B$�B#�B!�B!�B!�B �B�B�B�BoBbBbB
=B%B  B
��B
�B
�B
�NB
��B
ɺB
�jB
�B
��B
�DB
o�B
ZB
B�B
.B
�B	��B	�B	�HB	ǮB	�-B	�uB	� B	L�B	&�B�BȴB��B}�BcTBM�B@�B5?B:^B=qBC�BJ�B_;BjB� B��B�FB��B�
B�B��B	JB	�B	(�B	;dB	iyB	�B	��B	�'B	ƨB	�B	�`B	�B	��B
PB
�B
(�B
5?B
>wB
J�B
Q�B
ZB
aHB
e`B
l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	��B
C�B	��B
oB
+B
]/B
hsB
}�B
�B
��B
��B
�FB
ȴB
��BBhB�B%�B)�B1'B49B33B2-B0!B/B-B+B(�B%�B%�B$�B#�B!�B!�B!�B �B�B�B�BoBbBbB
=B%B  B
��B
�B
�B
�NB
��B
ɺB
�jB
�B
��B
�DB
o�B
ZB
B�B
.B
�B	��B	�B	�HB	ǮB	�-B	�uB	� B	L�B	&�B�BȴB��B}�BcTBM�B@�B5?B:^B=qBC�BJ�B_;BjB� B��B�FB��B�
B�B��B	JB	�B	(�B	;dB	iyB	�B	��B	�'B	ƨB	�B	�`B	�B	��B
PB
�B
(�B
5?B
>wB
J�B
Q�B
ZB
aHB
e`B
l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080906051020  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080906051027  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080906051029  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080906051036  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080906051037  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080906051037  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080906054219                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080910045805  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080910045811  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080910045812  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080910045817  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080910045817  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080910045818  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080910055441                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081014002156  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081014002157  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081014002201  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081014002201  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081014002201  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081014002201  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081014002201  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081014021612                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080910045805  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090424014530  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090424014531  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090424014531  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090424014531  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090424014532  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090424014532  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090424014532  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090424014532  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090424014532  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090424020457                      G�O�G�O�G�O�                