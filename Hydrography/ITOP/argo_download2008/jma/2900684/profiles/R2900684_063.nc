CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900684 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ?A   JA  20081016085707  20090424020459                                  2B  A   APEX-SBE 2810                                                   846 @��7=ѻ1   @��<F)�@;H1&�x�@be�����1   ARGOS   A   A   A   @�  A33AfffA���A�ffA���B
ffB��B2  BFffBZ  BlffB�33B�  B�33B�  B�33B�  B�  B�33BЙ�Bڙ�B�  B���B���C ��C�C
�fCffC33C�fC  C$�C)L�C.ffC3ffC8  C=L�CB  CG� CQffC[33CeffCo��Cy33C�� C�� C���C��3C��3C���C�� C��3C��fC���C��fC���C�� C³3CǦfC�� C�� C�� Cی�C�fC�3C� C�Y�C��C�� D� DٚD��D�3D�3D��D� D$�3D)ٚD.�fD3ٚD8ٚD=� DB� DGٚDL� DQٚDV�fD[��D`��De�3Dj� Do�fDt�3Dy�fD�&fD�i�D���D���D�&fD�l�D��fD��D��D�i�D�� D��fD�#3D�\�DڦfD��D�&fD�i�D�fD�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A  Ac33A�33A���A�33B	��B��B133BE��BY33Bk��B��B���B���B���B���B���B���B���B�34B�34B㙚B�fgB�34C ��C�gC
�3C33C  C�3C��C#�gC)�C.33C333C7��C=�CA��CGL�CQ33C[  Ce33CofgCy  C��fC��fC�s3C���C���C�s3C��fC���C���C��3C���C�s3C��fC�Cǌ�C̦fCѦfC֦fC�s3C���C噙C�ffC�@ C�s3C�ffD�3D��D��D�fD�fD��D�3D$�fD)��D.��D3��D8��D=�3DB�3DG��DL�3DQ��DV��D[� D`��De�fDj�3Do��Dt�fDy��D�  D�c4D��4D��gD�  D�fgD�� D��4D�gD�c4D���D�� D��D�VgDڠ D��4D�  D�c4D� D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A���A��A��A��hA�$�A��\A���A�~�A��A��A�VA�~�A���A��yA�Q�A���A�l�A��A��`A��A�oA���A���A�ȴA��HA��HA�p�A���A��wA�A�A�oA�A��!A�hsA��;A��\A�VA�I�A���A��9A��wA�ƨA��hA��DA�A�G�A���A���A��7A}�wAy�PAt��Ap�uAn��Al �Af�9AaAY�wAVbAP��AIABZA;��A9VA5%A0�A-�-A(��A�A�wA
jA��@�hs@�&�@�-@�1@���@��@���@�9X@��@�dZ@�/@��T@�E�@�;d@�"�@��7@�S�@�Ĝ@{S�@v$�@n�y@fff@W�P@M�-@Gl�@>ȴ@6�R@-?}@&ȴ@!7L@C�@��@~�@?}@	7L@@S�@ b?��?�+?�?}1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A���A��A��A��hA�$�A��\A���A�~�A��A��A�VA�~�A���A��yA�Q�A���A�l�A��A��`A��A�oA���A���A�ȴA��HA��HA�p�A���A��wA�A�A�oA�A��!A�hsA��;A��\A�VA�I�A���A��9A��wA�ƨA��hA��DA�A�G�A���A���A��7A}�wAy�PAt��Ap�uAn��Al �Af�9AaAY�wAVbAP��AIABZA;��A9VA5%A0�A-�-A(��A�A�wA
jA��@�hs@�&�@�-@�1@���@��@���@�9X@��@�dZ@�/@��T@�E�@�;d@�"�@��7@�S�@�Ĝ@{S�@v$�@n�y@fff@W�P@M�-@Gl�@>ȴ@6�R@-?}@&ȴ@!7L@C�@��@~�@?}@	7L@@S�@ b?��?�+?�?}1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�5B	�5B	�5B	�/B	�B
&�B
x�B
��B
�/B
�B�B/B�B�B�BH�BC�B>wB5?B8RBB�BE�B?}B8RB1'B?}B9XB+B&�B&�B$�B$�B$�B$�B#�B#�B"�B!�B�B�B\B	7BB
��B
�B
�B
�mB
�HB
��B
ƨB
�-B
��B
�7B
r�B
`BB
XB
J�B
2-B
{B	��B	�`B	��B	��B	�1B	k�B	\)B	F�B	1'B	$�B	bB�;B��B�DBr�B_;BM�B@�B;dB49B0!BA�BH�BiyB�B�VB��B�LB��B�sB	B	VB	�B	)�B	1'B	>wB	aHB	�JB	�B	�qB	��B	�BB	��B
B
bB
�B
)�B
5?B
@�B
I�B
S�B
ZB
aHB
gmB
l�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�5B	�5B	�5B	�/B	�B
&�B
x�B
��B
�/B
�B�B/B�B�B�BH�BC�B>wB5?B8RBB�BE�B?}B8RB1'B?}B9XB+B&�B&�B$�B$�B$�B$�B#�B#�B"�B!�B�B�B\B	7BB
��B
�B
�B
�mB
�HB
��B
ƨB
�-B
��B
�7B
r�B
`BB
XB
J�B
2-B
{B	��B	�`B	��B	��B	�1B	k�B	\)B	F�B	1'B	$�B	bB�;B��B�DBr�B_;BM�B@�B;dB49B0!BA�BH�BiyB�B�VB��B�LB��B�sB	B	VB	�B	)�B	1'B	>wB	aHB	�JB	�B	�qB	��B	�BB	��B
B
bB
�B
)�B
5?B
@�B
I�B
S�B
ZB
aHB
gmB
l�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081016085705  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081016085707  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081016085708  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081016085708  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081016085712  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081016085712  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081016085712  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081016085712  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081016085713  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081016090904                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081020041845  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081020041849  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081020041850  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020041850  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020041854  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081020041854  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020041854  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081020041854  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081020041855  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020061604                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081020041845  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090424014539  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090424014540  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090424014540  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090424014540  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090424014541  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090424014541  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090424014541  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090424014541  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090424014541  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090424020459                      G�O�G�O�G�O�                