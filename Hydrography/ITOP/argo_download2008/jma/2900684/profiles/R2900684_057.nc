CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900684 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               9A   JA  20080817092714  20090424020505                                  2B  A   APEX-SBE 2810                                                   846 @��2o���1   @��33�JV@;l�C��@bt1&�y1   ARGOS   A   A   A   @���A��Ak33A���A�33A�ffBffBffB2ffBF��BZ  Bn��B�33B�33B�33B�ffB�33B�33B�  B�  BЙ�B�33B�33B�ffB���CffC� C33C�fCffC��C�C#��C)L�C.L�C3L�C8ffC<��CB�CF��CP��CZ�3Cd�fCo  Cx�3C�Y�C���C���C��fC��fC�� C�� C�s3C���C��fC�s3C��fC�L�C�s3Cǌ�C̙�CѦfCֳ3Cۀ C�ffC�s3C�ffC�s3C�s3C�Y�D��D��D��D�3D�fDٚD��D$ٚD)ٚD.ٚD3ٚD8�fD=ٚDB��DGٚDL��DQٚDV�3D[ٚD`�3De� Dj�3Do� Dt��Dy�3D��D�l�D��fD��D�fD�i�D�� D���D��D�l�D���D�� D�,�D�c3Dڣ3D��3D�&fD�ffD�fD�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgAfgAh  A�33A���A���B��B��B1��BF  BY33Bn  B���B���B���B�  B���B���B���Bƙ�B�34B���B���B�  B�34C33CL�C  C�3C33C��C�gC#��C)�C.�C3�C833C<��CA�gCF��CPfgCZ� Cd�3Cn��Cx� C�@ C�� C��3C���C���C�ffC�ffC�Y�C�� C���C�Y�C���C�33C�Y�C�s3C̀ Cь�C֙�C�ffC�L�C�Y�C�L�C�Y�C�Y�C�@ D� D� D� D�fD��D��D��D$��D)��D.��D3��D8��D=��DB� DG��DL� DQ��DV�fD[��D`�fDe�3Dj�fDo�3Dt� Dy�fD�gD�fgD�� D��4D� D�c4D���D��gD�gD�fgD��4D�ٚD�&gD�\�Dڜ�D���D�  D�` D� D�C41111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�-A�A�Aޛ�A�bNA�oA�C�A��mA�K�A�ZA��^A�ĜA�%A�G�A�|�A���A��RA�5?A���A�S�A�r�A�?}A���A���A�Q�A���A���A�"�A�ƨA�Q�A�33A��yA�jA�I�A��A��A��9A��DA�M�A�bA��7A��A��A���A���A�A���A�XA��uA�$�A��A��yAp�A{�hAx��Au?}Ao�Am�Ah$�Ad{A]O�AW|�AO��ALr�AG�AEC�AA�TA<�A9hsA4��A)��AA�
A
ZA�u@��#@���@�1'@�V@Ĭ@��`@��@�1@���@��/@��9@���@�j@���@�@��@�|�@��@{33@s�F@h�9@Y��@P�9@Dz�@?�;@7�;@3�
@-�@(Ĝ@#o@�j@�;@@O�@	�#@v�@�H@ bN?�1?��/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�-A�A�Aޛ�A�bNA�oA�C�A��mA�K�A�ZA��^A�ĜA�%A�G�A�|�A���A��RA�5?A���A�S�A�r�A�?}A���A���A�Q�A���A���A�"�A�ƨA�Q�A�33A��yA�jA�I�A��A��A��9A��DA�M�A�bA��7A��A��A���A���A�A���A�XA��uA�$�A��A��yAp�A{�hAx��Au?}Ao�Am�Ah$�Ad{A]O�AW|�AO��ALr�AG�AEC�AA�TA<�A9hsA4��A)��AA�
A
ZA�u@��#@���@�1'@�V@Ĭ@��`@��@�1@���@��/@��9@���@�j@���@�@��@�|�@��@{33@s�F@h�9@Y��@P�9@Dz�@?�;@7�;@3�
@-�@(Ĝ@#o@�j@�;@@O�@	�#@v�@�H@ bN?�1?��/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
VB
T�B
J�B
uB	�3B
�B
#�B
Q�B
L�B
hsB
�)BB+B�B49B8RB6FB8RBH�B;dB;dB2-B2-B33B+B,B)�B(�B&�B$�B$�B$�B$�B%�B%�B%�B#�B"�B!�B �B�B�B�BhBJBB
��B
�B
�B
�/B
��B
�jB
��B
�uB
�B
t�B
YB
O�B
7LB
$�B
1B	�B	ɺB	�RB	��B	�uB	�B	q�B	_;B	I�B	PB�B�!B�=BjBQ�BB�B49B)�B(�B/B7LBI�BbNBs�B� B��B��B�LB��B�#B�B��B	PB	!�B	A�B	jB	�=B	�B	�qB	��B	�B	�yB	��B

=B
�B
&�B
33B
>wB
G�B
N�B
YB
^5B
dZB
m�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
VB
T�B
J�B
uB	�3B
�B
#�B
Q�B
L�B
hsB
�)BB+B�B49B8RB6FB8RBH�B;dB;dB2-B2-B33B+B,B)�B(�B&�B$�B$�B$�B$�B%�B%�B%�B#�B"�B!�B �B�B�B�BhBJBB
��B
�B
�B
�/B
��B
�jB
��B
�uB
�B
t�B
YB
O�B
7LB
$�B
1B	�B	ɺB	�RB	��B	�uB	�B	q�B	_;B	I�B	PB�B�!B�=BjBQ�BB�B49B)�B(�B/B7LBI�BbNBs�B� B��B��B�LB��B�#B�B��B	PB	!�B	A�B	jB	�=B	�B	�qB	��B	�B	�yB	��B

=B
�B
&�B
33B
>wB
G�B
N�B
YB
^5B
dZB
m�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080817092701  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080817092714  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080817092717  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080817092725  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080817092725  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080817092726  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080817101736                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080821044041  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080821044045  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080821044046  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080821044050  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080821044050  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080821044051  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080821052836                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081014002146  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081014002146  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081014002150  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081014002150  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081014002151  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081014002151  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081014002151  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081014021424                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080821044041  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090424014526  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090424014526  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090424014526  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090424014526  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090424014527  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090424014527  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090424014527  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090424014527  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090424014528  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090424020505                      G�O�G�O�G�O�                