CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900684 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               >A   JA  20081006055810  20090424020458                                  2B  A   APEX-SBE 2810                                                   846 @���*���1   @���n�c^@:O�;@bk�E���1   ARGOS   A   A   A   @�ffA33Ah  A���A�ffA陚B
��B��B3��BF  BZ  Bm��B�33B�  B�  B�  B�33B�  B���Bƙ�B�  B�33B䙚B�ffB���CffC�C
�fC�fC�CffC33C$  C)L�C-�fC3�C7��C=33CB  CGffCQL�C[�Ce�Co33Cy33C��fC���C���C��3C�� C��3C��fC���C�� C�� C��3C��3C���C�� Cǌ�C̳3Cь�C֙�C�s3C�3C�� C�� C�3C��3C���D� D� D��D�fDٚD��D��D$�fD)��D.�fD3� D8ٚD=�3DBٚDG�3DL��DQ� DV�fD[�fD`��De��DjٚDoٚDt� DyٚD�  D�\�D���D��3D�)�D�` D���D��D�0 D�l�D�� D��fD�&fD�\�Dک�D��fD��D�Y�D�fD�0 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A  Ad��A�33A���A�  B
  B��B2��BE33BY33Bl��B���B���B���B���B���B���B�fgB�34BЙ�B���B�34B�  B�fgC33C�gC
�3C�3C�gC33C  C#��C)�C-�3C2�gC7��C=  CA��CG33CQ�CZ�gCd�gCo  Cy  C���C�s3C�� C���C�ffC���C���C�� C�ffC�ffC���C���C��3C¦fC�s3C̙�C�s3Cր C�Y�C���C�fC�fCC���C��3D�3D�3D��D��D��D��D� D$��D)� D.ٙD3�3D8��D=�fDB��DG�fDL� DQ�3DV��D[��D`� De��Dj��Do��Dt�3Dy��D��D�VgD��4D���D�#4D�Y�D��4D��4D�)�D�fgD���D�� D�  D�VgDڣ4D�� D�gD�S4D� D�)�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A��A���A�l�A�G�Aգ�A��A�r�A��A�dZA�
=A�~�A��A��A�  A�=qA��7A�M�A��A�ȴA�1A��uA�+A��^A�1'A���A�ƨA�1A�&�A�x�A�1'A�(�A���A���A�(�A��#A�ZA���A�bNA��A���A��A���A�?}A�t�A��
A�-A��A��mA��uA�K�A�1A�;dA~��A|VAu/AnĜAi�Ab�DA\^5AZ�yAZAV��AR�AK�AC��A>�RA5oA.=qA&5?A��A-AƨA5?@�K�@�+@��#@���@�M�@�33@�^5@�hs@��@��D@�r�@�b@�G�@�hs@��@��@��!@|I�@y�@s�m@g;d@\�j@R��@E/@=p�@7K�@1��@)��@%/@;d@�!@|�@�
@�R@
M�@K�@`B@hs?��R?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A��A���A�l�A�G�Aգ�A��A�r�A��A�dZA�
=A�~�A��A��A�  A�=qA��7A�M�A��A�ȴA�1A��uA�+A��^A�1'A���A�ƨA�1A�&�A�x�A�1'A�(�A���A���A�(�A��#A�ZA���A�bNA��A���A��A���A�?}A�t�A��
A�-A��A��mA��uA�K�A�1A�;dA~��A|VAu/AnĜAi�Ab�DA\^5AZ�yAZAV��AR�AK�AC��A>�RA5oA.=qA&5?A��A-AƨA5?@�K�@�+@��#@���@�M�@�33@�^5@�hs@��@��D@�r�@�b@�G�@�hs@��@��@��!@|I�@y�@s�m@g;d@\�j@R��@E/@=p�@7K�@1��@)��@%/@;d@�!@|�@�
@�R@
M�@K�@`B@hs?��R?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	r�B	r�B	q�B	�B
-B
7LB
>wB
G�B
{�B
�BhB�B�B �B!�B$�B5?B>wB;dB6FB0!B.B/B1'B-B-B,B+B)�B%�B%�B%�B$�B$�B#�B!�B �B�B�B�BhBDBB
��B
��B
�B
�B
�mB
�TB
�;B
�/B
�B
��B
��B
��B
��B
t�B
W
B
@�B
�B
%B	��B	��B	�yB	��B	�?B	�hB	w�B	L�B	,B	B�5BĜB�'B�hBffBM�BD�BC�BD�BG�BL�B]/Bp�B�B�uB�BB��B�ZB��B	hB	�B	0!B	@�B	bNB	� B	��B	�B	ƨB	�B	�yB	��B
	7B
�B
!�B
)�B
33B
>wB
I�B
N�B
T�B
[#B
dZB
hs1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	r�B	r�B	q�B	�B
-B
7LB
>wB
G�B
{�B
�BhB�B�B �B!�B$�B5?B>wB;dB6FB0!B.B/B1'B-B-B,B+B)�B%�B%�B%�B$�B$�B#�B!�B �B�B�B�BhBDBB
��B
��B
�B
�B
�mB
�TB
�;B
�/B
�B
��B
��B
��B
��B
t�B
W
B
@�B
�B
%B	��B	��B	�yB	��B	�?B	�hB	w�B	L�B	,B	B�5BĜB�'B�hBffBM�BD�BC�BD�BG�BL�B]/Bp�B�B�uB�BB��B�ZB��B	hB	�B	0!B	@�B	bNB	� B	��B	�B	ƨB	�B	�yB	��B
	7B
�B
!�B
)�B
33B
>wB
I�B
N�B
T�B
[#B
dZB
hs1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081006055807  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081006055810  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081006055811  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081006055815  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081006055815  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081006055816  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081006063647                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081010043651  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081010043655  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081010043656  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081010043700  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081010043700  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081010043700  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081010061857                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081014002212  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081014002212  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081014002216  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081014002216  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081014002217  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081014002217  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081014002217  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081014021859                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081010043651  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090424014537  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090424014537  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090424014537  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090424014537  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090424014539  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090424014539  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090424014539  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090424014539  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090424014539  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090424020458                      G�O�G�O�G�O�                