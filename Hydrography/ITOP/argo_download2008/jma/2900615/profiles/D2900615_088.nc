CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               XA   JA  20080808025415  20090916024447  A9_60107_088                    2C  D   APEX-SBE 2341                                                   846 @���&N1   @������@9��1&�@bXbM�1   ARGOS   A   A   A   @�  AffAi��A���A�33A�ffBffB  B0  BDffBY33BnffB���B�  B���B�33B�  B�33B�  B�  B�ffB���B���B�ffB�  C33CffC� CL�C33C�CffC$L�C(�fC.33C2��C8�C=L�CA�fCGffCP��CZ��CeL�Co� Cy� C��fC�� C�s3C��3C���C�ffC�Y�C�ffC��3C�� C��fC��fC��fC¦fCǙ�Č�CѦfC�ffC�s3C�s3C噚C� C��C� C��3D��DٚD��D�3D��D� D��D$��D)� D.� D3�fD8�fD=� DB�fDGٚDL��DQ��DV�fD[� D`��De��Dj�fDo� Dt� DyٚD�#3D�l�D��3D�� D�)�D�ffD��fD�ٚD��D�` D��fD���D�0 D�p DڦfD�� D�&fD�i�D�3D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  AffAa��A���A�33A�ffBffB  B.  BBffBW33BlffB33B�  B���B�33B�  B�33B�  B�  B�ffB���B���B�ffB�  C �3C�fC  C��C�3C��C�fC#��C(ffC-�3C2L�C7��C<��CAffCF�fCPL�CZL�Cd��Co  Cy  C�ffC�� C�33C�s3C�L�C�&fC��C�&fC�s3C�@ C�ffC�ffC�ffC�ffC�Y�C�L�C�ffC�&fC�33C�33C�Y�C�@ C�L�C�@ C�s3D��D��D��D�3D��D� D��D$��D)� D.� D3�fD8�fD=� DB�fDG��DL��DQ��DV�fD[� D`��De��Dj�fDo� Dt� Dy��D�3D�\�D��3D�� D��D�VfD��fD�ɚD��D�P D��fD���D�  D�` DږfD�� D�fD�Y�D�3D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A�ƨA���A�z�A�M�A��A��FA���A��TA�A�/A�7LA��
A��A�1'A�33A��hA�C�A�\)A���A��wA�(�A�1'A�ffA�Q�A�dZA�-A��DA���A��A���A��A�/A�A���A�+A��A�;dA��A���A�VA��mA�A�A���A���A�9XA�r�A���A�M�A�-A|9XAz��Aw�Au�Atv�ApZAh-Ad~�A]�A\��AZ��AX�uAVjAT�RAS��AN�RAK\)AF�HA?�#A=�
A8�A*{A"1A�FA�AA�@��@�u@�hs@��#@�J@�|�@���@��w@���@�V@���@�7L@���@��\@���@�hs@|�j@y�@r~�@g�@[�F@T�D@LZ@Dj@;�F@4(�@/
=@+t�@'�@�@�P@5?@o@��@��@��@&�?�~�?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A�ƨA���A�z�A�M�A��A��FA���A��TA�A�/A�7LA��
A��A�1'A�33A��hA�C�A�\)A���A��wA�(�A�1'A�ffA�Q�A�dZA�-A��DA���A��A���A��A�/A�A���A�+A��A�;dA��A���A�VA��mA�A�A���A���A�9XA�r�A���A�M�A�-A|9XAz��Aw�Au�Atv�ApZAh-Ad~�A]�A\��AZ��AX�uAVjAT�RAS��AN�RAK\)AF�HA?�#A=�
A8�A*{A"1A�FA�AA�@��@�u@�hs@��#@�J@�|�@���@��w@���@�V@���@�7L@���@��\@���@�hs@|�j@y�@r~�@g�@[�F@T�D@LZ@Dj@;�F@4(�@/
=@+t�@'�@�@�P@5?@o@��@��@��@&�?�~�?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB1'B1'B)�Bp�B�JB�\B��B��B��B�9B�9B�9B�^B�LB�FB�!B��B��B�{B�bB�VB�DB�%B}�Bw�Bn�BdZB[#BW
BT�BO�BI�BD�BB�BA�B?}B<jB7LB49B/B&�B�B�BoB%B
��B
�B
�TB
��B
�3B
��B
�bB
|�B
r�B
l�B
XB
1'B
�B
B	��B	�B	�fB	�#B	��B	��B	�9B	��B	�DB	l�B	cTB	C�B	B�mB��B��B}�Bk�BYBK�BE�BK�BP�BdZBn�B�B��B�3B��B�NB	  B	B	�B	)�B	33B	G�B	cTB	�B	��B	�B	��B	�
B	�mB	�B	��B
B
�B
+B
?}B
E�B
O�B
T�B
YB
aHB
jB
l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B1'B2-BA�Bu�B�PB�hB��B��B��B�?B�?B�LB�dB�XB�RB�-B�B��B��B�hB�\B�JB�7B~�By�Bp�Be`B\)BW
BVBQ�BJ�BD�BB�BB�B@�B=qB8RB5?B0!B'�B�B�BuB+B
��B
�B
�ZB
��B
�?B
��B
�hB
|�B
r�B
m�B
ZB
2-B
 �B
B	��B	�B	�mB	�#B	��B	��B	�?B	��B	�PB	m�B	dZB	E�B	B�yB��B��B~�Bl�BZBL�BF�BL�BQ�BdZBo�B�B��B�9B��B�TB	  B	B	�B	)�B	33B	G�B	cTB	�B	��B	�B	��B	�
B	�mB	�B	��B
B
�B
+B
?}B
E�B
O�B
T�B
YB
aHB
jB
l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<�j<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.5(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808210320302008082103203020080821032030200808210342412008082103424120080821034241200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20080808025412  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080808025415  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080808025415  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080808025416  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080808025419  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080808025419  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080808025420  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080808025420  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080808025420  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080808030655                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080812040140  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080812040144  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080812040145  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080812040145  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080812040149  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080812040149  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080812040149  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080812040149  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080812040150  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080812051032                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080812040140  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090501065458  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090501065458  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090501065458  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090501065459  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090501065500  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090501065500  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090501065500  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090501065500  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090501065500  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090501065708                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080811112919  CV  DAT$            G�O�G�O�F�7�                JM  ARCAJMQC1.0                                                                 20080821032030  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080821032030  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080821034241  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916024231  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916024447                      G�O�G�O�G�O�                