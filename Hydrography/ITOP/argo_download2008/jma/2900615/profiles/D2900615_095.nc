CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               _A   JA  20081017055414  20090916024443  A9_60107_095                    2C  D   APEX-SBE 2341                                                   846 @��r���1   @��r�_1�@8��+J@a���E�1   ARGOS   A   A   A   @�33A  Ai��A���A�ffA홚B33B��B2  BFffBY33Bn  B�  B���B�ffB�ffB�  B���B���B�33B���B�33B�  B�ffB���C33C33CffCL�C�C�fCL�C#�fC)33C-�fC2��C8L�C=�CB33CGffCQffC[��Ce�CoffCx�fC�� C���C��3C�� C�� C��3C�� C��3C�� C��fC���C�� C���C�CǙ�Č�Cљ�Cր Cی�C�s3C�fC�3C�3C��3C�� D� DٚDٚD�3D��D�3D�3D$�fD)ٚD.�3D3�3D8�fD=�3DB�fDG�3DL��DQ�fDV��D[�fD`�fDe� DjٚDo��Dt�fDy�fD�)�D�c3D���D�� D�)�D�` D�� D��3D�#3D�l�D��3D�ٚD�&fD�p Dڬ�D���D�#3D�` D� D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��A^ffA�33A���A�  BffB��B/33BC��BVffBk33B33B�ffB�  B�  B���B�ffB�33B���B�ffB���B♚B�  B�ffC � C� C
�3C��CffC33C��C#33C(� C-33C2�C7��C<ffCA� CF�3CP�3CZ�fCdffCn�3Cx33C�ffC�@ C�Y�C�ffC�ffC�Y�C�ffC�Y�C�ffC�L�C�33C�ffC�33C�33C�@ C�33C�@ C�&fC�33C��C�L�C�Y�C�Y�C�Y�C�ffD�3D��D��D�fD� D�fD�fD$��D)��D.�fD3�fD8��D=�fDB��DG�fDL� DQ��DV� D[��D`��De�3Dj��Do� Dt��Dy��D�3D�L�D��3D�ɚD�3D�I�D���D���D��D�VfD���D��3D� D�Y�DږfD��fD��D�I�D�D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�I�A�XA�^5A�7LA�oA��AΡ�A�~�A�oA�
=A�+A���A���A��yA��HA�x�A�{A��A�9XA�JA��A��A� �A�JA��FA��PA�Q�A��A���A��A���A�^5A��7A���A�(�A��A���A��!A�O�A��-A���A���A��PA��
A��RA�C�A���A���A���A��TA|��Ay7LAt9XAn�+Ai�Ae��Ad�A`Q�A]K�AW�ASVAO�hAJĜABȴA:��A1�-A-��A&��A$  A�PA^5Av�@�z�@�h@ץ�@��`@��^@��F@�+@���@�M�@�+@��@�1@�%@��@��P@���@���@|j@u�-@p�u@kS�@e��@a��@Y�@R�@K�
@?�;@9X@2�H@,9X@&ȴ@!%@��@�/@��@Z@r�@�+@�!?���?�7L?��y?�bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�I�A�XA�^5A�7LA�oA��AΡ�A�~�A�oA�
=A�+A���A���A��yA��HA�x�A�{A��A�9XA�JA��A��A� �A�JA��FA��PA�Q�A��A���A��A���A�^5A��7A���A�(�A��A���A��!A�O�A��-A���A���A��PA��
A��RA�C�A���A���A���A��TA|��Ay7LAt9XAn�+Ai�Ae��Ad�A`Q�A]K�AW�ASVAO�hAJĜABȴA:��A1�-A-��A&��A$  A�PA^5Av�@�z�@�h@ץ�@��`@��^@��F@�+@���@�M�@�+@��@�1@�%@��@��P@���@���@|j@u�-@p�u@kS�@e��@a��@Y�@R�@K�
@?�;@9X@2�H@,9X@&ȴ@!%@��@�/@��@Z@r�@�+@�!?���?�7L?��y?�bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�B
�B
��B
�B
��B
�B+B�B�B$�B#�B9XB>wB@�B;dBP�BR�BQ�BM�BM�BG�BH�BG�BF�BB�B:^B6FB2-B/B-B+B(�B%�B"�B �B�B�B�B�B�BuBPB%B  B
��B
�B
�sB
�/B
��B
�?B
��B
�=B
q�B
S�B
?}B
-B
#�B
uB
B	�B	�B	ƨB	�!B	�PB	l�B	G�B	5?B	�B	1B�BɺB��Bw�Bu�BgmB_;B^5BffBr�B}�B�+B��B�!B�LB��B��B�yB	PB	�B	49B	I�B	XB	gmB	w�B	~�B	��B	�B	�qB	��B	�BB	�B	��B

=B
�B
#�B
2-B
>wB
G�B
O�B
T�B
aHB
hsB
o�B
r�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�B
�B
�B
�B
��B
��BoB�B�B'�B+B<jB@�BB�BA�BP�BR�BR�BP�BN�BI�BI�BG�BF�BD�B;dB7LB33B0!B-B+B)�B&�B"�B �B�B�B�B�B�B{BVB%BB
��B
�B
�yB
�5B
��B
�FB
��B
�DB
r�B
T�B
@�B
-B
$�B
{B
%B	�B	�B	ǮB	�-B	�\B	n�B	H�B	7LB	�B	
=B�B��B��Bx�Bv�BhsB`BB_;BgmBr�B}�B�1B��B�!B�LB��B��B�yB	PB	�B	49B	I�B	XB	gmB	w�B	~�B	��B	�B	�qB	��B	�BB	�B	��B

=B
�B
#�B
2-B
>wB
G�B
O�B
T�B
aHB
hsB
o�B
r�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.7(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810300207512008103002075120081030020751200810300213262008103002132620081030021326200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20081017055412  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081017055414  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081017055415  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081017055415  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081017055419  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081017055419  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081017055420  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081017055420  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081017055420  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081017062622                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081021035401  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081021035406  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081021035406  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081021035407  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081021035411  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081021035411  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081021035411  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081021035411  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081021035411  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081021070035                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081021035401  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090501065515  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090501065516  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090501065516  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090501065516  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090501065517  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090501065517  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090501065517  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090501065517  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090501065517  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090501065717                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081020105815  CV  DAT$            G�O�G�O�F�Ô                JM  ARCAJMQC1.0                                                                 20081030020751  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081030020751  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081030021326  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916024227  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916024443                      G�O�G�O�G�O�                