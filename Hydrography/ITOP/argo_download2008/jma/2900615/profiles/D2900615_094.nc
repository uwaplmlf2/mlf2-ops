CDF   
   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ^A   JA  20081008085252  20090916024442  A9_60107_094                    2C  D   APEX-SBE 2341                                                   846 @���d��1   @���u��@8t��E�@aˮz�H1   ARGOS   A   A   A   @�ffA33Ak33A���A���A�33B	��B33B2  BF  BZffBlffB33B���B�ffB�  B�ffB�  B���B���B�ffB�  B�ffB���B���C� CL�C
�fC��C��C��C�fC$33C)L�C.ffC2�3C7�fC=  CB33CG33CQL�C[L�Ce� Cn�fCyL�C��fC��3C���C�� C�� C���C�� C���C���C���C���C��3C��3C�� Cǳ3Č�C�� C�� C۳3C�3C噚C�� C�3C�� C�� D� D�3D�3DٚDٚD�fDٚD$� D)� D.��D3� D8�3D=� DB�3DG� DL�fDQ�3DVٚD[�fD`ٚDe�fDj�fDo�3Dt��Dy��D�0 D�i�D���D��fD�&fD�i�D��fD��D�#3D�c3D���D��3D�  D�ffDڦfD��fD�&fD�c3D�3D��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A	��Aa��A���A�  A�ffB33B��B/��BC��BX  Bj  B|��B�ffB�33B���B�33B���B���Bř�B�33B���B�33B홚B�ffC �fC�3C
L�C33C33C33CL�C#��C(�3C-��C2�C7L�C<ffCA��CF��CP�3CZ�3Cd�fCnL�Cx�3C�Y�C�ffC�@ C�33C�33C�L�C�33C�� C�L�C�� C�@ C�ffC�ffC�s3C�ffC�@ C�s3C�s3C�ffC�ffC�L�C�s3C�ffC�s3C�s3D��D��D��D�3D�3D� D�3D$��D)��D.�fD3��D8��D=��DB��DG��DL� DQ��DV�3D[� D`�3De� Dj� Do��Dt�3Dy�fD��D�VfD���D��3D�3D�VfD��3D��fD� D�P D��fD�� D��D�S3Dړ3D��3D�3D�P D� D�	�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�z�A��A�dZA�|�A�5?A���A֗�A�Aϝ�AǕ�A�Q�A�/A���A�VA�?}A���A���A��9A���A�JA�~�A�v�A��A�K�A�bNA�+A�C�A�?}A��`A�XA��A���A�VA���A�XA�5?A���A�+A��A�$�A�  A��\A�ȴA���A��A��A� �A��jA�I�A�+AVA{t�Ay�Ar �Al{Ac��Ab{A[`BAZVAP��AAdZA<v�A7/A2ȴA-�TA'ƨA&�yA$1AjAA�A
=Az�A ��@�w@�+@��T@�@���@��y@��+@���@�@��j@�5?@�O�@��H@��;@�x�@���@�$�@� �@z��@pA�@l��@g�@\�D@SC�@H�u@@�9@6�+@-@(�@ �`@�/@�@�-@7L@��@
~�@�+@ �u?�;d?���?�1'?�!1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�z�A��A�dZA�|�A�5?A���A֗�A�Aϝ�AǕ�A�Q�A�/A���A�VA�?}A���A���A��9A���A�JA�~�A�v�A��A�K�A�bNA�+A�C�A�?}A��`A�XA��A���A�VA���A�XA�5?A���A�+A��A�$�A�  A��\A�ȴA���A��A��A� �A��jA�I�A�+AVA{t�Ay�Ar �Al{Ac��Ab{A[`BAZVAP��AAdZA<v�A7/A2ȴA-�TA'ƨA&�yA$1AjAA�A
=Az�A ��@�w@�+@��T@�@���@��y@��+@���@�@��j@�5?@�O�@��H@��;@�x�@���@�$�@� �@z��@pA�@l��@g�@\�D@SC�@H�u@@�9@6�+@-@(�@ �`@�/@�@�-@7L@��@
~�@�+@ �u?�;d?���?�1'?�!1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
O�B
J�B
G�B
P�B
o�B
�NB
�TB
��B
ǮB
�9B
�B1B
=B'�B-B0!B49BC�BT�BO�BJ�BG�BB�B<jB9XB9XB7LB6FB5?B33B1'B/B-B,B+B)�B'�B&�B"�B �B�BhB1BB
��B
�B
�B
�;B
��B
��B
��B
��B
�1B
ffB
H�B
$�B
�B
  B	��B	ĜB	}�B	dZB	J�B	8RB	#�B	DB	+B��B�/BȴB��B�VBx�BgmB[#BYBVB_;Be`Bs�B� B�{B��B�jB��B�#B�yB��B	VB	�B	(�B	=qB	W
B	cTB	u�B	�+B	��B	�}B	��B	�sB	��B
1B
�B
$�B
-B
9XB
C�B
M�B
R�B
ZB
e`B
hsB
m�B
p�B
v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
P�B
K�B
H�B
Q�B
r�B
�B
�fB
��B
��B
ĜB
��BDBVB(�B.B2-B8RBF�BW
BP�BK�BG�BD�B=qB9XB:^B7LB6FB6FB33B1'B0!B-B,B+B+B'�B'�B"�B!�B�BoB1BB
��B
�B
�B
�BB
��B
B
��B
��B
�=B
gmB
J�B
$�B
�B
  B	��B	ȴB	~�B	e`B	K�B	9XB	%�B	DB	1B��B�5BɺB��B�\By�BhsB\)BZBW
B`BBffBs�B�B�{B��B�jB��B�#B�yB��B	VB	�B	(�B	=qB	W
B	cTB	u�B	�+B	��B	�}B	��B	�sB	��B
1B
�B
$�B
-B
9XB
C�B
M�B
R�B
ZB
e`B
hsB
m�B
p�B
v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<�o<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.6(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909071304012009090713040120090907130401200909071830462009090718304620090907183046200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20081008085250  IP                  G�O�G�O�G�O�                JM  ARFMDECPA9                                                                  20090907130356  IP                  G�O�G�O�G�O�                JM  ARFMBITP1                                                                   20081008085250  SV                  D�3DٚG�O�                JM  ARGQRQCP1                                                                   20090907130356  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20090907130401  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090907130401  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090907183046  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916024224  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916024442                      G�O�G�O�G�O�                