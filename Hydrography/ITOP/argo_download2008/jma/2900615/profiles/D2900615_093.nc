CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ]A   JA  20080927011747  20090916024437  A9_60107_093                    2C  D   APEX-SBE 2341                                                   846 @��l�l�1   @��s+l��@8Y�"��`@a˥�S��1   ARGOS   A   A   A   @�  AffAfffA���A���A�33B	33B��B2  BF��BZffBn  B���B�  B�33B�  B�ffB�ffB�ffB���B���B�33B���B�ffB�ffC�CL�C  CffC  C��C33C#��C)�C.ffC3� C8L�C=�3CBffCG��CQ�C[� CeffCo  Cx�fC���C��fC�s3C���C��3C��3C���C�� C���C��fC��fC��fC��3C¦fCǌ�C̦fC�� C�s3C۳3C�� C�Y�C�Y�CC�ffC�� D��D�3D��D�fD�3D��D� D$�fD)� D.��D3��D8�3D=�3DB�3DG��DLٚDQ�fDV��D[�3D`ٚDeٚDjٚDo� Dt� DyٚD�#3D�l�D���D���D�)�D�l�D���D���D�#3D�c3D�� D��3D�#3D�ffDک�D��fD�#3D�c3D��D�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��A\��A���A�  A�ffB��B33B/��BDffBX  Bk��B33B���B�  B���B�33B�33B�33Bř�Bϙ�B�  B㙚B�33B�33C � C�3C
ffC��CffC33C��C#33C(� C-��C2�fC7�3C=�CA��CG  CP� CZ�fCd��CnffCxL�C�@ C�Y�C�&fC�@ C�ffC�ffC�L�C�s3C�� C�Y�C�Y�C�Y�C�ffC�Y�C�@ C�Y�C�s3C�&fC�ffC�33C��C��C�L�C��C�33D�3D��D�fD� D��D�fD��D$� D)��D.�3D3�fD8��D=��DB��DG�fDL�3DQ� DV�fD[��D`�3De�3Dj�3Do��Dt��Dy�3D� D�Y�D��fD�ٚD�fD�Y�D���D�ɚD� D�P D���D�� D� D�S3DږfD��3D� D�P D�D�0 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��`A��A��A��A���A�VAº^A�5?A�r�A�+A��\A��A��A���A��mA���A���A��mA�jA���A��^A��A��A���A���A�bNA��A�x�A� �A��uA��A�ZA�JA�7LA���A�Q�A�ȴA�XA��A��A�t�A�A��A��FA���A�\)A�O�A�\)A�l�A�bNA~�uAy��ApȴAl-A_C�A\VAYƨAV��AP��AH�AB�jA=��A8��A2�A-��A)hsA$�+AS�AhsAĜAZAA�@��#@�9@��T@�9X@¸R@�K�@��F@�S�@�`B@��P@���@��!@�-@�v�@�j@�@�-@{S�@w�@uV@q�^@i�7@e/@Y7L@Jn�@B�H@:��@17L@,9X@%V@!�7@@%@�@1'@9X@�u@j@�#?�v�?�C�?��P?��`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��`A��A��A��A���A�VAº^A�5?A�r�A�+A��\A��A��A���A��mA���A���A��mA�jA���A��^A��A��A���A���A�bNA��A�x�A� �A��uA��A�ZA�JA�7LA���A�Q�A�ȴA�XA��A��A�t�A�A��A��FA���A�\)A�O�A�\)A�l�A�bNA~�uAy��ApȴAl-A_C�A\VAYƨAV��AP��AH�AB�jA=��A8��A2�A-��A)hsA$�+AS�AhsAĜAZAA�@��#@�9@��T@�9X@¸R@�K�@��F@�S�@�`B@��P@���@��!@�-@�v�@�j@�@�-@{S�@w�@uV@q�^@i�7@e/@Y7L@Jn�@B�H@:��@17L@,9X@%V@!�7@@%@�@1'@9X@�u@j@�#?�v�?�C�?��P?��`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�5B
�5B
�5B
�5B
ƨB
��B
�9B
�#B
�BB
�yB
�BB�B,B7LBC�B?}BF�B?}BC�B>wB8RB6FB49B49B49B0!B/B,B,B+B)�B&�B$�B$�B"�B!�B�B�B�B{BDB%B
��B
�B
�B
�mB
�5B
ɺB
�jB
��B
�=B
dZB
H�B
�B
	7B	��B	�B	��B	��B	�B	hsB	O�B	49B	�B	
=B��B�BB�B��B��B�Be`B\)BR�BVBYB_;BjBt�B�B�uB��B�B�jBɺB�ZB	B	�B	.B	8RB	@�B	I�B	`BB	n�B	��B	�XB	��B	�;B	�B	��B
\B
�B
$�B
1'B
>wB
D�B
N�B
VB
[#B
bNB
gmB
l�B
q�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�5B
�5B
�5B
�;B
��B
�}B
�wB
�)B
�NB
�B
�BDB!�B.B8RBC�B@�BG�B@�BE�B?}B:^B7LB5?B49B5?B0!B/B-B-B,B)�B'�B$�B$�B#�B!�B�B�B�B�BJB+B
��B
�B
�B
�mB
�;B
��B
�qB
��B
�JB
e`B
K�B
�B

=B	��B	�B	��B	��B	�B	iyB	Q�B	6FB	�B	DB��B�HB�B��B��B�BffB]/BS�BW
BZB`BBjBu�B�B�uB��B�B�jB��B�`B	B	�B	.B	8RB	@�B	I�B	`BB	n�B	��B	�XB	��B	�;B	�B	��B
\B
�B
$�B
1'B
>wB
D�B
N�B
VB
[#B
bNB
gmB
l�B
q�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<ě�<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.6(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810100215482008101002154820081010021548200810100236582008101002365820081010023658200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20080927011731  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080927011747  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080927011750  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080927011752  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080927011800  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080927011800  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080927011801  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080927011801  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080927011802  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080927014052                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081001040518  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081001040523  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081001040523  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081001040524  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081001040528  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081001040528  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081001040528  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081001040528  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081001040528  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081001064522                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081001040518  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090501065511  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090501065511  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090501065511  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090501065511  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090501065512  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090501065512  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090501065512  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090501065512  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090501065512  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090501065723                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081010021548  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081010021548  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081010023658  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916024218  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916024437                      G�O�G�O�G�O�                