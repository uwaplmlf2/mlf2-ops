CDF   /   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901499 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               SA   JA  20080803025358  20090901084512  A5_24108_083                    2C  D   APEX-SBE 1714                                                   846 @��YP�1   @��<�)@8/��-V@b������1   ARGOS   A   A   A   @���A��AfffA���A�33A���B
��B33B2  BE33BZ  Bo33B���B�ffB�  B�  B�ffB�ffB���B���B���B�  B���B���B�  C�3C�3CL�C��C�C� CL�C$33C(��C.33C3  C8ffC=33CA�fCGL�CQ� C[�CeL�CoL�CyL�C��fC��fC���C�� C���C���C���C�� C��3C��3C�� C��fC��3C³3CǙ�C̀ C�s3Cֳ3C�s3C���C� CꙚC�� C���C�� D� D� D��DٚD��D� D�3D$�fD)� D.� D3� D8�fD=�fDB� DG� DL�fDQ�3DV��D[��D`�3De��Dj��Do� Dt��Dy�3D�  D�p D���D��D�)�D�\�D�� D���D�,�D�c3D��fD���D��D�c3Dڰ D��fD��D�i�D�D�c31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��AfffA���A�33A���B
��B33B2  BE33BZ  Bo33B���B�ffB�  B�  B�ffB�ffB���B���B���B�  B���B���B�  C�3C�3CL�C��C�C� CL�C$33C(��C.33C3  C8ffC=33CA�fCGL�CQ� C[�CeL�CoL�CyL�C��fC��fC���C�� C���C���C���C�� C��3C��3C�� C��fC��3C³3CǙ�C̀ C�s3Cֳ3C�s3C���C� CꙚC�� C���C�� D� D� D��DٚD��D� D�3D$�fD)� D.� D3� D8�fD=�fDB� DG� DL�fDQ�3DV��D[��D`�3De��Dj��Do� Dt��Dy�3D�  D�p D���D��D�)�D�\�D�� D���D�,�D�c3D��fD���D��D�c3Dڰ D��fD��D�i�D�D�c31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A�JA�bA�VA�oA�%A�%A��PA�G�A�z�A�\)A��TAɰ!A�JA�{A�%A���A���A��yA���A�$�A�"�A�bNA��^A�;dA��FA�hsA���A��A�I�A���A�v�A�A�?}A�|�A�jA�A��!A�bA�G�A��A�{A���A�^5A��A��A��PA�ffA�1'A�7LA�^5A�E�A�&�A��A�^Av�DAu|�AqS�Ak&�Aa��A_��AW��ATI�AO/AK"�AE�hA>�A<�uA6A�A2jA&��A��A�Ar�@�x�@���@�K�@�\)@��@ũ�@��+@���@�`B@�z�@�1'@�33@���@��R@�Q�@���@��@��@��7@~��@v�y@h1'@` �@So@H��@>�y@9�^@41@+�@#ƨ@l�@��@�P@dZ@\)@S�@�y@j@ ��?�{?�Q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A�JA�bA�VA�oA�%A�%A��PA�G�A�z�A�\)A��TAɰ!A�JA�{A�%A���A���A��yA���A�$�A�"�A�bNA��^A�;dA��FA�hsA���A��A�I�A���A�v�A�A�?}A�|�A�jA�A��!A�bA�G�A��A�{A���A�^5A��A��A��PA�ffA�1'A�7LA�^5A�E�A�&�A��A�^Av�DAu|�AqS�Ak&�Aa��A_��AW��ATI�AO/AK"�AE�hA>�A<�uA6A�A2jA&��A��A�Ar�@�x�@���@�K�@�\)@��@ũ�@��+@���@�`B@�z�@�1'@�33@���@��R@�Q�@���@��@��@��7@~��@v�y@h1'@` �@So@H��@>�y@9�^@41@+�@#ƨ@l�@��@�P@dZ@\)@S�@�y@j@ ��?�{?�Q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�B�B�B�B�B �B�BgmBcTBȴBB��BƨB�qB��B��B�;B�BB�ZB�mB�sB�`B�;B�
BȴB�RB�bB�Bp�Bm�B`BB_;B[#BVBS�BS�BR�BP�BO�BM�BI�BA�B=qB8RB2-B(�B�BbB+B
��B
�B
�ZB
�
B
�}B
�B
|�B
v�B
cTB
D�B
 �B
hB	�B	��B	��B	��B	~�B	]/B	R�B	6FB	&�B	B��B�B��Bl�B]/BN�BP�BP�BP�BcTBu�B�B�1B��B�!B��B��B�B	B	�B	%�B	,B	7LB	A�B	l�B	�7B	�B	ŢB	�;B	��B
B
uB
#�B
,B
5?B
>wB
J�B
T�B
\)B
dZB
iyB
q�B
x�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�B�B�B�B�B �B!�BjBl�B��BB��B��B��B��B�
B�NB�TB�mB�sB�yB�mB�BB�B��B�qB�oB�Bq�Bn�B`BB_;B\)BW
BS�BS�BR�BQ�BP�BN�BJ�BB�B>wB9XB33B)�B�BhB+B
��B
�B
�`B
�B
��B
�B
|�B
w�B
e`B
F�B
!�B
uB	�B	��B	��B	��B	�B	]/B	T�B	7LB	(�B	B��B�!B��Bm�B^5BO�BP�BQ�BQ�BdZBv�B�B�1B��B�!B��B�B�B	B	�B	%�B	,B	7LB	A�B	l�B	�7B	�B	ŢB	�;B	��B
B
uB
#�B
,B
5?B
>wB
J�B
T�B
\)B
dZB
iyB
q�B
x�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808160749072008081607490720080816074907200808160806152008081608061520080816080615200908190000002009081900000020090819000000  JA  ARFMdecpA5_a                                                                20080803025348  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080803025358  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080803025400  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080803025401  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080803025408  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080803025408  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080803025409  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080803025409  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080803025409  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080803035626                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080807034153  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080807034157  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080807034158  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080807034159  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080807034203  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080807034203  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080807034203  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080807034203  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080807034203  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080807051537                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080901002027  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080901002028  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080901002032  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080901002032  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080901002032  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080901002032  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080901002032  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080901015123                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080807034153  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090522013427  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090522013428  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090522013428  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090522013428  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090522013429  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090522013429  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090522013429  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090522013429  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090522013430  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090522014217                      G�O�G�O�G�O�                JA  ARUP                                                                        20090522015654                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080806170648  CV  DAT$            G�O�G�O�F�-�                JM  ARCAJMQC1.0                                                                 20080816074907  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080816074907  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080816080615  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090819000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090901084445  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090901084512                      G�O�G�O�G�O�                