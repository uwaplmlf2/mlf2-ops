CDF   '   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901499 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ZA   JA  20081012055550  20090901084520  A5_24108_090                    2C  D   APEX-SBE 1714                                                   846 @��4���1   @��4��|�@72���m@b��x���1   ARGOS   A   A   A   @�33A��Ac33A�  A�ffA�  B
��B  B1��BE��BY��BlffB�ffB�33B���B���B�ffB�ffB�  B�  B�  B�33B���B�ffB���C� CL�CffC33C� CffC33C$�C)ffC.� C3ffC7�fC=33CB�CF��CQ�C[� Ce� Co� Cy� C���C��3C��fC���C��3C�s3C��fC�� C��fC��3C���C���C���C�Cǳ3C̦fCѳ3C֦fC۳3C�3C噚C�fC� C���C��fD��DٚD��D��D�3D�fD�fD$� D)� D.� D3� D8ٚD=��DB�fDG��DL� DQ��DV�fD[�3D`��DeٚDj�fDo�fDt�fDyٚD�  D�l�D��fD�� D��D�ffD�� D���D��D�i�D���D���D�)�D�c3Dڣ3D��3D�)�D�ffD�3D�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��Ac33A�  A�ffA�  B
��B  B1��BE��BY��BlffB�ffB�33B���B���B�ffB�ffB�  B�  B�  B�33B���B�ffB���C� CL�CffC33C� CffC33C$�C)ffC.� C3ffC7�fC=33CB�CF��CQ�C[� Ce� Co� Cy� C���C��3C��fC���C��3C�s3C��fC�� C��fC��3C���C���C���C�Cǳ3C̦fCѳ3C֦fC۳3C�3C噚C�fC� C���C��fD��DٚD��D��D�3D�fD�fD$� D)� D.� D3� D8ٚD=��DB�fDG��DL� DQ��DV�fD[�3D`��DeٚDj�fDo�fDt�fDyٚD�  D�l�D��fD�� D��D�ffD�� D���D��D�i�D���D���D�)�D�c3Dڣ3D��3D�)�D�ffD�3D�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A��A��;A��TA��mA��yA��A��HA�
=A�^5A��yA̶FA��mA���Aŧ�A��DA��uA�  A���A�"�A�  A�l�A�O�A�O�A�A�A�p�A�G�A��A��hA�I�A�ffA�VA�t�A�jA���A�G�A�A�$�A�A�r�A�=qA���A�p�A���A���A��hA��!A�t�A��;A��A��A�?}A��AzQ�At�jAo��Ak�-Ai�Ae��A`v�AY�AU7LAOO�AH1'AE\)AAl�A;|�A8bA5`BA/��A%�At�AoA�@�"�@�5?@ۮ@Ϯ@��H@�33@�C�@���@�5?@��@��@�7L@�b@�x�@�b@�=q@���@��#@}O�@yhs@rM�@fV@]�@O��@FE�@97L@2n�@-p�@'�w@"�\@S�@  @n�@�-@
M�@Q�@Z?��R?���?���?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A��A��;A��TA��mA��yA��A��HA�
=A�^5A��yA̶FA��mA���Aŧ�A��DA��uA�  A���A�"�A�  A�l�A�O�A�O�A�A�A�p�A�G�A��A��hA�I�A�ffA�VA�t�A�jA���A�G�A�A�$�A�A�r�A�=qA���A�p�A���A���A��hA��!A�t�A��;A��A��A�?}A��AzQ�At�jAo��Ak�-Ai�Ae��A`v�AY�AU7LAOO�AH1'AE\)AAl�A;|�A8bA5`BA/��A%�At�AoA�@�"�@�5?@ۮ@Ϯ@��H@�33@�C�@���@�5?@��@��@�7L@�b@�x�@�b@�=q@���@��#@}O�@yhs@rM�@fV@]�@O��@FE�@97L@2n�@-p�@'�w@"�\@S�@  @n�@�-@
M�@Q�@Z?��R?���?���?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
ŢB
ȴB
��B
��B
��B
��B
��BQ�B�BŢB��B�TB�`B�B�BB�fB�/B��BB�XB�FB�XB�B��B��B��B��B�uB�BffBn�BdZB\)BW
BQ�BM�BK�BI�BB�B;dB2-B)�B �B�B+B
��B
�B
�mB
�;B
��B
��B
�B
�VB
o�B
ZB
I�B
>wB
1'B
�B	�B	�#B	�jB	��B	�bB	{�B	cTB	R�B	F�B	.B	B�B�B�1Bo�BZBS�BP�BVB`BBm�B�%B��B�3B��B��B�B��B	  B	$�B	5?B	?}B	P�B	YB	hsB	��B	�3B	��B	�ZB	��B
1B
\B
�B
)�B
8RB
>wB
G�B
P�B
YB
^5B
ffB
r�B
w�B
� B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
ŢB
ȴB
��B
��B
��B
��B
��B[#B�=BǮB��B�`B�mB�B��B%B�mB�BB��BĜB�dB�LB�dB�B��B�B��B��B��B�+BgmBp�Be`B]/BXBR�BN�BK�BJ�BC�B<jB33B+B!�B�B+B  B
��B
�mB
�BB
�B
B
�B
�\B
p�B
[#B
J�B
?}B
2-B
�B	�B	�/B	�wB	��B	�hB	}�B	dZB	S�B	H�B	/B	B�B�'B�7Bp�B[#BT�BQ�BW
BaHBn�B�+B��B�3B��B��B�B��B	B	$�B	5?B	?}B	P�B	YB	hsB	��B	�3B	��B	�ZB	��B
1B
\B
�B
)�B
8RB
>wB
G�B
P�B
YB
^5B
ffB
r�B
w�B
� B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810250834452008102508344520081025083445200810250836322008102508363220081025083632200908190000002009081900000020090819000000  JA  ARFMdecpA5_a                                                                20081012055535  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081012055550  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081012055553  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081012055555  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081012055604  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081012055604  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081012055604  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081012055604  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081012055606  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081012073108                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20081016034230  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081016034235  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081016034235  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081016034236  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081016034240  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081016034240  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081016034240  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081016034240  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081016034241  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081016060924                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20081016034230  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090522013447  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090522013448  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090522013448  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090522013448  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090522013449  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090522013449  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090522013450  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090522013450  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090522013450  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090522014214                      G�O�G�O�G�O�                JA  ARUP                                                                        20090522015649                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081015170600  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20081025083445  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081025083445  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081025083632  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090819000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090901084446  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090901084520                      G�O�G�O�G�O�                