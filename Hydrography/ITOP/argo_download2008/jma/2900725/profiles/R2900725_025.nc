CDF   *   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   g   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  48   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  6<   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8@   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  9�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :D   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ;�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <H   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    C�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    F�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    I�   CALIBRATION_DATE            	             
_FillValue                  ,  L�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    L�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    L�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    L�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    L�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  L�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M4   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    MD   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    MH   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         MX   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M\   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M`   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    MdArgo profile    2.2 1.2 19500101000000  2900725 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20081005190128  20091208012325                                  2B  A   APEX-SBE 3465                                                   846 @��R3�ax1   @��Rx9��@:}p��
=@a$�n��1   ARGOS   B   B   B   @���A��AfffA�ffA���A陚B33B33B2��BG33BZffBm33B���B�33B�33B�ffB���B���B�33Bƙ�BЙ�B�ffB���B�33B�  C� CffCL�CffC  C33C  C$ffC)ffC.L�C333C8L�C=ffCA�fG�O�Cy33C��fC��fC��3C���C��fC�� C�� C�s3C���C��fC��3C��fC��3C�CǙ�Č�CѦfC�� Cۙ�C�3C��C�ٚC�� G�O�D��D�fD�3D�3D$�3D)�fD.�3D3�fD8� DV� D[�3D`�fDeٚDj� Do�3Dt�fDy�fD�#3D�l�D�� D���D�#3D�p D���D��D�)�D�l�D��3D�� D�#3D�l�Dڣ3D���D��D�ffD��D�ٚD�y�1111111111111111111111111111111111111114111111111111111111111111411111111111111111111111111111111111111 @���A��AfffA�ffA���A陚B33B33B2��BG33BZffBm33B���B�33B�33B�ffB���B���B�33Bƙ�BЙ�B�ffB���B�33B�  C� CffCL�CffC  C33C  C$ffC)ffC.L�C333C8L�C=ffCA�fG�O�Cy33C��fC��fC��3C���C��fC�� C�� C�s3C���C��fC��3C��fC��3C�CǙ�Č�CѦfC�� Cۙ�C�3C��C�ٚC�� G�O�D��D�fD�3D�3D$�3D)�fD.�3D3�fD8� DV� D[�3D`�fDeٚDj� Do�3Dt�fDy�fD�#3D�l�D�� D���D�#3D�p D���D��D�)�D�l�D��3D�� D�#3D�l�Dڣ3D���D��D�ffD��D�ٚD�y�1111111111111111111111111111111111111114111111111111111111111111411111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ĜA�9A��A�t�A�?}A�9A�x�A���A�bA�  A�~�A��A��A���A�ƨA�K�A��9A�A���A���A�l�A�t�A�XA�JA���A�33A�ffA��A�(�A��A�^5A�`BA��^A�I�A�/A�C�A��uA�v�G�O�A�;dA��A�9XA�A�S�A�dZA�~�A�?}A��^A�`BA}p�A{�AvjAt~�Anz�Am/Ah$�Af1Ad{Aa�7A]�AY�ASS�AN�HG�O�A&JAA�
A-@�dZ@�M�@���@�v�@�p�@��@�b@��;@��`@���@�ƨ@�Z@���@~V@m@b�@Xr�@N{@Dz�@;�F@2^5@,�j@'
=@#�m@ b@M�@/@hs@�@I�@��@?}@=q@ Ĝ@ r�1111111111111111111111111111111111111194111111111111111111111119411111111111111111111111111111111111111 A�ĜA�9A��A�t�A�?}A�9A�x�A���A�bA�  A�~�A��A��A���A�ƨA�K�A��9A�A���A���A�l�A�t�A�XA�JA���A�33A�ffA��A�(�A��A�^5A�`BA��^A�I�A�/A�C�A��uA�v�G�O�A�;dA��A�9XA�A�S�A�dZA�~�A�?}A��^A�`BA}p�A{�AvjAt~�Anz�Am/Ah$�Af1Ad{Aa�7A]�AY�ASS�AN�HG�O�A&JAA�
A-@�dZ@�M�@���@�v�@�p�@��@�b@��;@��`@���@�ƨ@�Z@���@~V@m@b�@Xr�@N{@Dz�@;�F@2^5@,�j@'
=@#�m@ b@M�@/@hs@�@I�@��@?}@=q@ Ĝ@ r�1111111111111111111111111111111111111194111111111111111111111119411111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
(�B
)�B
+B
+B
.B
,B
&�B
?}B
v�B
��B{B�B{B;dBF�B\)BhsBr�Bz�B~�B�B�+B�VB�JBw�Bu�Bt�Bp�Bl�BffB_;B[#BXBR�BN�BI�BH�B?}B;dG�O�B�BDB1B
��B
�B
�)B
��B
��B
��B
��B
�JB
u�B
hsB
L�B
C�B
.B
#�B
�B
VB	��B	�;B	��B	�^B	�oG�O�B�TB��B��B��B�hB�=B�B�B�B�B�B	+B	
=B	$�B	0!B	;dB	F�B	jB	�B	��B	�-B	ǮB	�)B	�B	��B
1B
VB
�B
%�B
33B
=qB
D�B
K�B
XB
^5B
e`B
jB
k�1111111111111111111111111111111111111114111111111111111111111111411111111111111111111111111111111111111 B
(�B
)�B
+B
+B
.B
,B
&�B
?}B
v�B
��B{B�B{B;dBF�B\)BhsBr�Bz�B~�B�B�+B�VB�JBw�Bu�Bt�Bp�Bl�BffB_;B[#BXBR�BN�BI�BH�B?}B;dG�O�B�BDB1B
��B
�B
�)B
��B
��B
��B
��B
�JB
u�B
hsB
L�B
C�B
.B
#�B
�B
VB	��B	�;B	��B	�^B	�oG�O�B�TB��B��B��B�hB�=B�B�B�B�B�B	+B	
=B	$�B	0!B	;dB	F�B	jB	�B	��B	�-B	ǮB	�)B	�B	��B
1B
VB
�B
%�B
33B
=qB
D�B
K�B
XB
^5B
e`B
jB
k�1111111111111111111111111111111111111114111111111111111111111111411111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081005190126  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081005190128  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081005190129  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081005190133  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081005190133  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081005190133  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20081005190133  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20081005190134  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081005191806                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081008164622  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081008164626  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081008164627  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081008164631  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081008164631  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081008164631  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20081008164631  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20081008164631  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081008191425                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081008164622  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422024039  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422024039  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422024040  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422024041  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090422024041  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422024041  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422024041  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422024041  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422024041  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422024041  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422024217                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207082858  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207083037  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207083037  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207083038  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207083040  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207083040  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207083040  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207083040  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207083040  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207083040  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207083040  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208012325                      G�O�G�O�G�O�                