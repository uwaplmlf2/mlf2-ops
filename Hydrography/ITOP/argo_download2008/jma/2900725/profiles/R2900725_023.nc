CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   f   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  44   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  64   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  84   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  9�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :4   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ;�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <4   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ?d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  Ad   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  Cd   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    C�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    F�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    I�   CALIBRATION_DATE            	             
_FillValue                  ,  L�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    L�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    L�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    L�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    L�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  L�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M    HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M$   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M4   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M8   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M<   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M@Argo profile    2.2 1.2 19500101000000  2900725 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080916012925  20091208012308                                  2B  A   APEX-SBE 3465                                                   846 @��Nβ@y1   @��U�K�@:aG�z�@a&M���1   ARGOS   A   A   A   @�ffAffAfffA�33A���A���B
��B33B2��BFffBY��Bm��B�33B���B���B���B���B�ffB�ffB���B�ffB�33B�  B�  B���C�CL�CL�C�C  C�CL�C#�fC)L�C.  C3��C8L�C=L�CBL�CG� CQL�C[  Ce  CoffCy  C���C�� C���C�� C�s3C��3C���C�ffC��fC��fC�� C���C���Cۙ�C�� C�fC��C�fC���C��fDٚD�3D� D�fD��DٚD� D$��D)ٚD.��D3�3D8�fD=��DBٚDG��DL� DQ�fDo�3Dt�3Dy�fD�)�D�ffD���D��3D�  D�l�D�� D��D��D�ffD���D���D�&fD�c3D��D��fD��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�ffAffAfffA�33A���A���B
��B33B2��BFffBY��Bm��B�33B���B���B���B���B�ffB�ffB���B�ffB�33B�  B�  B���C�CL�CL�C�C  C�CL�C#�fC)L�C.  C3��C8L�C=L�CBL�CG� CQL�C[  Ce  CoffCy  C���C�� C���C�� C�s3C��3C���C�ffC��fC��fC�� C���C���Cۙ�C�� C�fC��C�fC���C��fDٚD�3D� D�fD��DٚD� D$��D)ٚD.��D3�3D8�fD=��DBٚDG��DL� DQ�fDo�3Dt�3Dy�fD�)�D�ffD���D��3D�  D�l�D�� D��D��D�ffD���D���D�&fD�c3D��D��fD��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ƨA�`BA�9A�A�bA�33A��mA�bNA�r�A��A��FA���A�C�A���A��A�K�A���A��DA�Q�A�n�A�v�A���A��hA���A�1'A�t�A��#A�
=A�bA�G�A�bA�~�A��wA��wA�C�A�hsA��A�+A�+A�^5A�5?A���A��A�
=A�I�A��A�A�A�ZA���A��uA�7LA�G�A�1A���A���A|��Awp�Ar�`AT�AN=qAI��AC;dA>Q�A8�HA6�+A-�7A%"�A��A��A��A��@�+@�ƨ@�n�@ݙ�@�o@��;@�^5@�bN@��@�t�@���@�dZ@�9X@�P@pr�@`A�@T(�@Kƨ@A�7@:��@1�#@)&�@"��@�@��G�O�@ff@j@�\@ 1'?���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111911111  A�ƨA�`BA�9A�A�bA�33A��mA�bNA�r�A��A��FA���A�C�A���A��A�K�A���A��DA�Q�A�n�A�v�A���A��hA���A�1'A�t�A��#A�
=A�bA�G�A�bA�~�A��wA��wA�C�A�hsA��A�+A�+A�^5A�5?A���A��A�
=A�I�A��A�A�A�ZA���A��uA�7LA�G�A�1A���A���A|��Awp�Ar�`AT�AN=qAI��AC;dA>Q�A8�HA6�+A-�7A%"�A��A��A��A��@�+@�ƨ@�n�@ݙ�@�o@��;@�^5@�bN@��@�t�@���@�dZ@�9X@�P@pr�@`A�@T(�@Kƨ@A�7@:��@1�#@)&�@"��@�@��G�O�@ff@j@�\@ 1'?���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111911111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
+B
%B
\B
�B
&�B
_;B
r�B
_;B
��B
��B
��B�B8RB.B0!BQ�BD�BK�BhsBT�BhsBp�B� B��B��B�JB�1B� Bx�Bm�BdZB_;BXBI�BG�BB�B?}B;dB6FB33B+B&�B"�B�B�BbBB
��B
��B
�B
�HB
��B
ĜB
�XB
�B
�uB
z�B
hsB	�)B	��B	�B	�\B	x�B	dZB	ZB	49B	uB��B�/BŢB�B��B�\B�7B�B�B�DB��B��B��B�RB�wB	#�B	-B	E�B	e`B	�=B	��B	�LB	��B	�/B	�B
B
oB
�B
'�G�O�B
\)B
aHB
cTB
jB
m�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111911111  B
+B
%B
\B
�B
&�B
_;B
r�B
_;B
��B
��B
��B�B8RB.B0!BQ�BD�BK�BhsBT�BhsBp�B� B��B��B�JB�1B� Bx�Bm�BdZB_;BXBI�BG�BB�B?}B;dB6FB33B+B&�B"�B�B�BbBB
��B
��B
�B
�HB
��B
ĜB
�XB
�B
�uB
z�B
hsB	�)B	��B	�B	�\B	x�B	dZB	ZB	49B	uB��B�/BŢB�B��B�\B�7B�B�B�DB��B��B��B�RB�wB	#�B	-B	E�B	e`B	�=B	��B	�LB	��B	�/B	�B
B
oB
�B
'�G�O�B
\)B
aHB
cTB
jB
m�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111911111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080916012923  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080916012925  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080916012926  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080916012930  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080916012930  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080916012930  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080916012931  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080916015221                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080918161935  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080918161938  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080918161939  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080918161943  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080918161943  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080918161943  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080918161944  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080918191324                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080918161935  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422024035  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422024035  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422024035  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422024036  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090422024036  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422024036  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422024036  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422024036  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422024036  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422024037  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422024214                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207082858  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207083034  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207083034  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207083035  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207083036  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207083036  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207083036  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207083036  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207083036  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208012308                      G�O�G�O�G�O�                