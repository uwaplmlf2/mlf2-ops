CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2900725 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080825190153  20090422024215                                  2B  A   APEX-SBE 3465                                                   846 @��K�RL�1   @��K뵪J@:\(�@a&5?|�1   ARGOS   A   A   A   @���AffAfffA�  A�  A�33B
��B��B2ffBD��BY��Bm��B�33B�  B�  B���B�ffB���B�33B�33B���B�  B�  B�ffB�33C� CffC  C  CL�C�C33C$33C)ffC-�fC3L�C8  C=L�CB  CGffCQ33CZ��Cd��Co33CyffC�� C��fC��3C��fC���C��fC�� C�s3C��3C��3C�Y�C���C�� C³3CǦfC̀ CѦfC�Y�C�ffC���C�s3C�3C�s3C� C���D�fD�fD�3D�3D��D�fD�3D$�3D)ٚD.��D3��D8��D=� DBٚDG�fDL�fDQ� DV�3D[��D`��De��DjٚDo�3Dt��Dy� D�,�D�i�D�� D��3D�  D�c3D��3D�� D�  D�i�D���D��3D�&fD�i�Dڣ3D���D�  D�i�D�3D���D�Vf11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���AffAfffA�  A�  A�33B
��B��B2ffBD��BY��Bm��B�33B�  B�  B���B�ffB���B�33B�33B���B�  B�  B�ffB�33C� CffC  C  CL�C�C33C$33C)ffC-�fC3L�C8  C=L�CB  CGffCQ33CZ��Cd��Co33CyffC�� C��fC��3C��fC���C��fC�� C�s3C��3C��3C�Y�C���C�� C³3CǦfC̀ CѦfC�Y�C�ffC���C�s3C�3C�s3C� C���D�fD�fD�3D�3D��D�fD�3D$�3D)ٚD.��D3��D8��D=� DBٚDG�fDL�fDQ� DV�3D[��D`��De��DjٚDo�3Dt��Dy� D�,�D�i�D�� D��3D�  D�c3D��3D�� D�  D�i�D���D��3D�&fD�i�Dڣ3D���D�  D�i�D�3D���D�Vf11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A�XA�{A��mA���A���AđhA�I�A�ZA�ƨA�-A��hA�C�A�x�A�x�A�I�A���A�VA��HA��RA�p�A�t�A��A���A�-A���A�jA��!A��A�K�A�1'A��yA���A��A���A��A�7LA��A���A�G�A�I�A��A���A�(�A���A��A� �A�A�A�%A��/A��DA�p�A�AC�A{��Av$�AqVAl��Ai��Ac��A`M�A\ZAXĜAUG�AT  AP��AJ~�AE��A@��A7?}A(n�A#ƨA33A�PAffA�@�bN@��H@�33@Л�@��T@�(�@�z�@��\@�A�@���@��@�K�@��/@��F@��#@�$�@��@;d@u�@g��@Z��@M�T@E�-@@1'@9��@3��@)��@$�/@��@�#@��@��@A�@@
�H@�@O�@^5?��;?�O�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A�XA�{A��mA���A���AđhA�I�A�ZA�ƨA�-A��hA�C�A�x�A�x�A�I�A���A�VA��HA��RA�p�A�t�A��A���A�-A���A�jA��!A��A�K�A�1'A��yA���A��A���A��A�7LA��A���A�G�A�I�A��A���A�(�A���A��A� �A�A�A�%A��/A��DA�p�A�AC�A{��Av$�AqVAl��Ai��Ac��A`M�A\ZAXĜAUG�AT  AP��AJ~�AE��A@��A7?}A(n�A#ƨA33A�PAffA�@�bN@��H@�33@Л�@��T@�(�@�z�@��\@�A�@���@��@�K�@��/@��F@��#@�$�@��@;d@u�@g��@Z��@M�T@E�-@@1'@9��@3��@)��@$�/@��@�#@��@��@A�@@
�H@�@O�@^5?��;?�O�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
T�B
Q�B
N�B
K�B
G�B
�B
.B
|�B
�?B
ŢB
�#B
=B�B)�B8RB5?B8RB7LB7LB7LB7LB=qB=qB;dB:^B:^B8RB8RB6FB5?B5?B33B2-B1'B0!B.B-B,B)�B'�B$�B!�B�B�B�BJB+B  B
��B
�B
�B
ĜB
�!B
��B
�oB
x�B
bNB
N�B
@�B
$�B
{B
B	�B	�`B	�/B	��B	�3B	��B	�1B	bNB	1'B	�B	%B��B�TB�wB�B��B��B��B��B��B�B�3B�dB�jBĜB��B�B	
=B	�B	$�B	9XB	H�B	[#B	v�B	��B	�'B	ƨB	��B	�BB	�B
B
bB
�B
+B
2-B
7LB
C�B
I�B
R�B
ZB
aHB
hsB
m�B
o�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
T�B
Q�B
N�B
K�B
G�B
�B
.B
|�B
�?B
ŢB
�#B
=B�B)�B8RB5?B8RB7LB7LB7LB7LB=qB=qB;dB:^B:^B8RB8RB6FB5?B5?B33B2-B1'B0!B.B-B,B)�B'�B$�B!�B�B�B�BJB+B  B
��B
�B
�B
ĜB
�!B
��B
�oB
x�B
bNB
N�B
@�B
$�B
{B
B	�B	�`B	�/B	��B	�3B	��B	�1B	bNB	1'B	�B	%B��B�TB�wB�B��B��B��B��B��B�B�3B�dB�jBĜB��B�B	
=B	�B	$�B	9XB	H�B	[#B	v�B	��B	�'B	ƨB	��B	�BB	�B
B
bB
�B
+B
2-B
7LB
C�B
I�B
R�B
ZB
aHB
hsB
m�B
o�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080825190150  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080825190153  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080825190154  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080825190158  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080825190158  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080825190158  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080825191606                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080829163502  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080829163506  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080829163507  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080829163511  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080829163511  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080829163511  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080829191045                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080829163502  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422024030  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422024031  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422024031  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422024032  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422024032  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422024032  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422024032  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422024032  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422024215                      G�O�G�O�G�O�                