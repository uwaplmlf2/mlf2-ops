CDF   B   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   F   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5\   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6t   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       94   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       :L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       =   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >$   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >T   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    AT   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    DT   CALIBRATION_DATE            	             
_FillValue                  ,  GT   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         G�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         G�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    H Argo profile    2.2 1.2 19500101000000  2900666 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               mA   JA  20080820012345  20090515053346                                  2B  A   APEX-SBE 2984                                                   846 @���U���1   @���A;�@=�x���@b�1&�x�1   ARGOS   F   F   F   A$��A���A͙�B��B,��BP  Bu��B�  B���B�ffB�  B�  B���C� CffCL�C �C)� C4  CG��C[33CoffC���C�33C���C��3C���C�L�C�L�C�ٚC��3C�ٚC�fC�ffC�ffD ��D�fD
� D��Dy�D� DY�D#Y�D(@ D-,�D2S3D7` D<Y�DAFfDG� DM�fDT  DZ9�D`l�Df�fDl� Ds&fD�fD�L�D�6fD�vfD��3D�� D�i�D���D�L�D�fD�0 D�3D�9�4444444444444444444444444444444444444444444444444444444444444444444444  ��32@���A��A� A�fgB
ffB0  BVffB}��B���B�33B�33B�  B�33B���C�gC�4C�C"��C6fgCI��C^  Cq�4C�� C��C�  C��gC���C���C�&gC�@ C�&gC��3C�3C�3C��gDL�DFfD33D  D&fD  D  D#�fD(�3D-��D3fD8  D<��DC&fDIl�DO�fDU� D\3DbL�Dh�fDn��D{,�D�  D�	�D�I�D��fD��3D�<�D�� D�  D�ٙD�3D�fD��4444444444444444444444444444444444444444444444444444444444444444444444  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�7LA��;Aϙ�A�\)A��;A�|�A��^A�oA�hsA�ȴA�bNA�1'A�C�A�G�A�|�A��/A�K�A���A�VA�z�A�`BA�$�A|�`As�Am+Afz�A`�HAW�;ALn�AA7LA8��A/t�A&(�A�+A��A	O�A��@�C�@�~�@�A�@�r�@���@���@���@�S�@��^@��w@�?}@��@��/@��@�(�@���@�(�@y7L@o��@kt�@[ƨ@Sƨ@KdZ@E�h@<Z@8A�@(1'@��@j@��@�@��?���4444444444444444444444444444444444444444444444444444444444444444444444  A�7LA��;Aϙ�A�\)A��;A�|�A��^A�oA�hsA�ȴA�bNA�1'A�C�A�G�A�|�A��/A�K�A���A�VA�z�A�`BA�$�A|�`As�Am+Afz�A`�HAW�;ALn�AA7LA8��A/t�A&(�A�+A��A	O�A��@�C�@�~�@�A�@�r�@���@���@���@�S�@��^@��w@�?}@��@��/@��@�(�@���@�(�@y7L@o��@kt�@[ƨ@Sƨ@KdZ@E�h@<Z@8A�@(1'@��@j@��@�@��?���4444444444444444444444444444444444444444444444444444444444444444444444  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
2-B
+B
�B
ǮB
ȴB
��B
�BB	7B%B+B
��B
��B
��B
�B
�B
�B
�fB
�TB
�/B
��B
�XB
��B
p�B
O�B
5?B
�B	�B	�jB	�B	]/B	2-B	+B�ZB�?B�Bl�BA�B)�B!�B�B�B!�B(�B9XBF�BS�By�B�\B��B��B�5B�B	+B	�B	/B	@�B	W
B	{�B	�\B	�B	�dB	ɺB	�`B
B
�B
0!B
D�B
R�B
_;4444444444444444444444444444444444444444444444444444444444444444444444  B
33B
,B
�B
ȴB
ɺB
��B
�BB
=B+B1B
��B
��B
��B
�B
�B
�B
�lB
�ZB
�5B
��B
�^B
��B
q�B
P�B
6EB
�B	��B	�pB	�B	^5B	33B	1B�`B�EB�Bm�BB�B+B"�B�B�B"�B)�B:^BG�BT�Bz�B�bB�B��B�;B�B	1B	�B	0!B	A�B	XB	|�B	�bB	�B	�jB	��B	�fB
B
�B
1'B
E�B
S�B
`A4444444444444444444444444444444444444444444444444444444444444444444444  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080820012342  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080820012345  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080820012346  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080820012346  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080820012350  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080820012350  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080820012350  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080820012350  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080820012351  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080820013511                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080824052554  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080824052611  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080824052613  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080824052614  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080824052623  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080824052623  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080824052624  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080824052624  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080824052625  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080824064839                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080829023819  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080829023819  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080829023823  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080829023823  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080829023823  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080829023823  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080829023823  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080829060316                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080824052554  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515034456  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515034456  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515034456  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515034456  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515034457  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515034457  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515034458  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515034458  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515034458  QCF$                G�O�G�O�G�O�            4040JA  ARGQrqcpt16b                                                                20090515034458  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515034849                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080824052554  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515050559  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515050559  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515050559  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515050600  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515050601  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515050601  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515050601  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090515050601  QCF$                G�O�G�O�G�O�            8000JA  ARGQaqcp2.8b                                                                20090515050601  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515050601  QCF$                G�O�G�O�G�O�            C040JA  ARGQrqcpt16b                                                                20090515050601  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090515051004                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080824052554  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515052914  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515052915  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515052915  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515052915  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515052916  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515052916  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515052916  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090515052916  QCF$                G�O�G�O�G�O�            8000JA  ARGQaqcp2.8b                                                                20090515052916  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515052916  QCF$                G�O�G�O�G�O�            C040JA  ARGQrqcpt16b                                                                20090515052916  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090515053346                      G�O�G�O�G�O�                