CDF   :   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   F   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5\   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6t   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       94   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       :L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       =   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >$   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >T   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    AT   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    DT   CALIBRATION_DATE            	             
_FillValue                  ,  GT   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         G�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         G�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    H Argo profile    2.2 1.2 19500101000000  2900666 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               sA   JA  20080919012503  20090515053412                                  2B  A   APEX-SBE 2984                                                   846 @��j� ��1   @��k8�@=�ȴ9X@b�9XbN1   ARGOS   F   F   F   AI��A�  A���B(ffB-��BS33B{33B�33B���B�33B���Bޙ�B�C�fC��C�3C ffC*L�C4�CI�3C[� Cn�fC��C�ٚC��fC��3C��fC�Y�C�33C��3C���C��fC�fC�L�C��D ��D� D
��D��Dy�D` Ds3D#` D(ffD-S3D2L�D79�D<S3DB  DG�fDM�3DS�fDZ33D`� Df�fDl�3Ds�Dy�D�fD�<�D�s3D���D��D�c3D��3D�P D��3D�S3D�� D�p 4444444444444444444444444444444444444444444444444444444444444444444444  ��34�33@AX  A�32Aə�B
ffB2ffBW��B~��B���B�ffB�34B�34B�ffB�34C� C33C�C!�gC7� CIL�C\�3Co�gC�� C���C�ٙC���C�@ C��C�ٙCƳ3C���Cڌ�C�33C��3C��D �3D  D  D��D�3D�fD�3D#ٙD(�fD-� D2��D7�fD=�3DB��DI&fDOY�DU�fD\33Db�DhFfDn��Dz��D�� D��gD�,�D�fgD��4D��D���D�	�DΌ�D��D牚D�)�4444444444444444444444444444444444444444444444444444444444444444444444  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�A�ZA���A�1'A���A���A��#A�p�A�jA��A� �A� �A�|�A� �A�XA� �A�
=A���A�S�A�`BA�%A��uA�jA�;dAz�jApjAh�A`�`AY�wAR�`AM�AE�
A;��A4  A-C�A%oA��Al�A�;@�J@��@���@��#@�`B@�?}@��F@��
@���@�@��9@��^@�%@�r�@�|�@~�R@u�@nv�@c��@Z-@O�P@E��@=�-@6ȴ@(bN@
=@
=@��@
=q@�?��-4444444444444444444444444444444444444444444444444444444444444444444444  A�A�A�ZA���A�1'A���A���A��#A�p�A�jA��A� �A� �A�|�A� �A�XA� �A�
=A���A�S�A�`BA�%A��uA�jA�;dAz�jApjAh�A`�`AY�wAR�`AM�AE�
A;��A4  A-C�A%oA��Al�A�;@�J@��@���@��#@�`B@�?}@��F@��
@���@�@��9@��^@�%@�r�@�|�@~�R@u�@nv�@c��@Z-@O�P@E��@=�-@6ȴ@(bN@
=@
=@��@
=q@�?��-4444444444444444444444444444444444444444444444444444444444444444444444  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	Q�B	O�B	K�B
r�B(�B49B2-B.B,B)�B#�B�B�B�BDBBB
��B
��B
�ZB
�B
�
B
�qB
��B
�+B
Q�B
-B
1B	�B	��B	�9B	�hB	]/B	9XB	�B��BƨB�bB{�BcTBK�BC�B-B�B#�B33B@�BJ�BJ�BgmB�1B��B�XB��B�B	B	�B	=qB	W
B	q�B	�7B	��B	�9B	�
B	�B
JB
!�B
33B
D�B
R�4444444444444444444444444444444444444444444444444444444444444444444444  B	R�B	P�B	L�B
s�B)�B5?B33B/B-B+B$�B �B �B�BJB%BB  B
��B
�`B
�#B
�B
�wB
��B
�1B
R�B
.B
	7B	�B	��B	�?B	�nB	^5B	:^B	�B��BǮB�hB|�BdZBL�BD�B.B�B$�B49BA�BK�BK�BhsB�7B��B�^B��B�B	B	�B	>wB	XB	r�B	�=B	��B	�?B	�B	�B
PB
"�B
49B
E�B
S�4444444444444444444444444444444444444444444444444444444444444444444444  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080919012500  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080919012503  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080919012504  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080919012504  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080919012508  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080919012508  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080919012509  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080919012509  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080919012509  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080919025852                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923040745  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080923040751  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080923040751  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080923040751  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080923040755  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080923040755  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080923040756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080923040756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080923040756  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080923051012                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923040745  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515034510  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515034511  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515034511  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515034511  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515034512  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515034512  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515034512  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515034512  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515034512  QCF$                G�O�G�O�G�O�              40JA  ARGQrqcpt16b                                                                20090515034512  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515034917                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923040745  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515050614  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515050614  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515050615  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515050615  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515050616  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515050616  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515050616  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090515050616  QCF$                G�O�G�O�G�O�            8000JA  ARGQaqcp2.8b                                                                20090515050616  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515050616  QCF$                G�O�G�O�G�O�            8040JA  ARGQrqcpt16b                                                                20090515050616  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090515051033                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923040745  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515052929  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515052930  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515052930  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515052930  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515052931  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515052931  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515052931  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090515052931  QCF$                G�O�G�O�G�O�            8000JA  ARGQaqcp2.8b                                                                20090515052931  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515052931  QCF$                G�O�G�O�G�O�            8040JA  ARGQrqcpt16b                                                                20090515052931  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090515053412                      G�O�G�O�G�O�                