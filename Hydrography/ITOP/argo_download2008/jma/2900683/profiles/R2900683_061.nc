CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900683 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               =A   JA  20080920045507  20090518064413                                  2B  A   APEX-SBE 2821                                                   846 @��j���1   @��k"R��@:��hr�!@aZ� ě�1   ARGOS   A   A   A   @���A  AfffA���Ař�A�33B
��B��B133BFffBZ  Bm33B���B�  B�33B���B�33B�33B���Bř�B���B�ffB�  B�ffB���C33C��CffC� CL�C� CL�C$L�C)  C-�3C2�3C7��C<��CA�fCG  CQ33C[L�CeffCo33Cy�C���C�� C��fC�� C���C��fC��fC���C��3C���C���C�� C��fC�Cǌ�C̦fCѦfC֙�C�� C�3C�fC�fC�3C�� C��fD��D��D��D��D� D� DٚD$��D)��D.��D3ٚD8�fD=ٚDB� DGٚDL�fDQ��DV�3D[ٚD`�fDe�3Dj� Do�3Dt� Dy� D�#3D�l�D���D�� D��D�c3D��fD��fD��D�ffD���D�ٚD�  D�i�Dڣ3D���D�  D�i�D� D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA��Ac33A�  A�  A陙B
  B��B0ffBE��BY33BlffB�fgB���B���B�fgB���B���B�fgB�34B�fgB�  B㙚B�  B�fgC  CfgC33CL�C�CL�C�C$�C(��C-� C2� C7��C<��CA�3CF��CQ  C[�Ce33Co  Cx�gC�s3C��fC���C��fC�� C���C���C�� C���C��3C�� C��fC���C C�s3Č�Cь�Cր CۦfC���C��C��CC��fC���D� D��D� D� D�3D�3D��D$� D)� D.� D3��D8��D=��DB�3DG��DL��DQ� DV�fD[��D`��De�fDj�3Do�fDt�3Dy�3D��D�fgD��4D��D�4D�\�D�� D�� D�4D�` D��gD��4D��D�c4Dڜ�D��gD��D�c4D�D� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A�+A�A�7A�t�A�1'AօA�=qA��yA�=qA���A��A���A��!A�"�A���A��RA�K�A��yA�"�A���A��`A��A���A��^A�1A�|�A���A��^A�x�A��uA���A�;dA���A��/A�oA��#A�z�A�&�A��`A���A�+A�v�A��/A�hsA�O�A�z�A���A��A�|�A�"�A��7A���A~��A{O�Aw�At1'Ao��Al�Ak%Af�9A`��A[AYp�AY7LAU�wAQ��AM�AH  AB�`A8ZA1��A*ȴA��Av�A�;A�!@��H@��@�j@�  @�$�@��
@�n�@��F@���@��`@�&�@��w@�@���@��^@��T@�
=@~E�@o��@a��@Z��@TZ@I�7@@��@9��@3t�@)hs@%/@!7L@?}@�P@z�@�;@�F@	%@5?@"�@&�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A�+A�A�7A�t�A�1'AօA�=qA��yA�=qA���A��A���A��!A�"�A���A��RA�K�A��yA�"�A���A��`A��A���A��^A�1A�|�A���A��^A�x�A��uA���A�;dA���A��/A�oA��#A�z�A�&�A��`A���A�+A�v�A��/A�hsA�O�A�z�A���A��A�|�A�"�A��7A���A~��A{O�Aw�At1'Ao��Al�Ak%Af�9A`��A[AYp�AY7LAU�wAQ��AM�AH  AB�`A8ZA1��A*ȴA��Av�A�;A�!@��H@��@�j@�  @�$�@��
@�n�@��F@���@��`@�&�@��w@�@���@��^@��T@�
=@~E�@o��@a��@Z��@TZ@I�7@@��@9��@3t�@)hs@%/@!7L@?}@�P@z�@�;@�F@	%@5?@"�@&�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�B�B	W
B
�B

=B
uB
v�B
=BQ�B_;Bt�B�B�B�BcTB�PB�Bt�Bt�Bn�Bk�BdZBXB_;BZB�B�B%�B(�B.B49B6FB5?B0!B,B+B)�B)�B&�B%�B"�B �B�B�B�B�BDB  B
�B
�fB
�;B
��B
�FB
��B
�PB
}�B
m�B
[#B
J�B
D�B
/B
{B	��B	��B	��B	�ZB	��B	�}B	��B	�\B	^5B	@�B	�B�`B��B��B�%Bp�BdZBiyBaHBaHBm�B|�B�VB�{B�BŢB�B�B��B	+B	{B	 �B	2-B	O�B	p�B	�B	�hB	�B	B	��B	�`B	��B
%B
\B
�B
+B
2-B
>wB
H�B
O�B
W
B
_;B
e`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�B�B	W
B
�B

=B
uB
v�B
=BQ�B_;Bt�B�B�B�BcTB�PB�Bt�Bt�Bn�Bk�BdZBXB_;BZB�B�B%�B(�B.B49B6FB5?B0!B,B+B)�B)�B&�B%�B"�B �B�B�B�B�BDB  B
�B
�fB
�;B
��B
�FB
��B
�PB
}�B
m�B
[#B
J�B
D�B
/B
{B	��B	��B	��B	�ZB	��B	�}B	��B	�\B	^5B	@�B	�B�`B��B��B�%Bp�BdZBiyBaHBaHBm�B|�B�VB�{B�BŢB�B�B��B	+B	{B	 �B	2-B	O�B	p�B	�B	�hB	�B	B	��B	�`B	��B
%B
\B
�B
+B
2-B
>wB
H�B
O�B
W
B
_;B
e`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080920045505  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080920045507  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080920045508  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080920045512  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080920045512  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080920045513  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080920051753                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923035823  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080923035827  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080923035828  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080923035832  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080923035832  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080923035832  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080923050826                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923035823  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518064036  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518064037  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518064037  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518064038  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518064038  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518064038  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518064038  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518064038  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518064413                      G�O�G�O�G�O�                