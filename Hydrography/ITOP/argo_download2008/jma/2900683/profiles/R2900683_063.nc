CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   n   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4T   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6|   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :\   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J�   CALIBRATION_DATE            	             
_FillValue                  ,  M�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N    HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N`   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Np   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Nt   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  2900683 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ?A   JA  20081010055441  20091209003003                                  2B  A   APEX-SBE 2821                                                   846 @��jͿ��1   @��o�Sp@:�Z�1@aTbM��1   ARGOS   A   A   A   @���A  AfffA�  A���A���B
��B��B1��BFffBZffBn  B�33B�ffB���B�  B���B�  B�33B�  B�33B���B�33CL�CffC�CffC$� C(�fC.  C333C833C=33CB� CG�CQ�C[��CeffCn�fCy  C���C���C���C��3C��fC���C��3C���C���C��fC���C���C��fC�� Cǌ�C�� Cѳ3C֙�C۳3C���C�L�C�fC�fC���C�� D� D��D��D�fDٚD� D�3D$ٚD)� D.�fD3��D8� D=�3DB�fDG��DL�fDQ� DV� D[�3D`� De�fDj��Do�3Dt�3DyٚD�&fD�i�D���D��D�,�D�c3D��fD��fD�,�D�l�D���D�ٚD��D�c3Dڣ3D�� D�,�D�` D� D��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�34A��Ac33A�ffA�33A�33B
  B  B0��BE��BY��Bm33B���B�  B�fgB���B�34B���B���Bř�B���B�fgB���C�C33C�gC33C$L�C(�3C-��C3  C8  C=  CBL�CF�gCP�gC[fgCe33Cn�3Cx��C�s3C�� C�� C���C���C�s3C���C��3C�� C���C�� C�� C���C¦fC�s3C̦fCљ�Cր Cۙ�C�� C�33C��C��C� C�ffD�3D� D� D��D��D�3D�fD$��D)�3D.��D3� D8�3D=�fDBٙDG� DL��DQ�3DV�3D[�fD`�3De��Dj� Do�fDt�fDy��D�  D�c4D��gD��4D�&gD�\�D�� D�� D�&gD�fgD��gD��4D�4D�\�Dڜ�D�ٚD�&gD�Y�D�D�g11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�7A�\A�A�S�A�\)A��A◍Aԡ�A�=qA�AēuA���A��7A���A�?}A���A���A��mA�A�?}A�ZA�-A�-A���A�%A�?}A�O�A�bA�t�A���A���A��A�XA�"�A� �A�-A�1A��A�VA�$�A�%A�-A�&�A��A�1'A�p�A�A�^5A%A}��A}XAv��Au��As�AnZAfbNAa�A\{AU��AR~�AL��AH�AEl�AE33A?x�A4�HA+VA$��AA�AhsAp�@���@ꗍ@�(�@�1@���@�bN@�5?@�I�@� �@��#@���@�Ĝ@�$�@��@�ƨ@��@�Ĝ@�$�@�K�@m@d�D@ZJ@P  @E�@>��@7�@.ȴ@+"�@"��@��@&�@Z@�w@�-@
�@��@S�@ ��?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�7A�\A�A�S�A�\)A��A◍Aԡ�A�=qA�AēuA���A��7A���A�?}A���A���A��mA�A�?}A�ZA�-A�-A���A�%A�?}A�O�A�bA�t�A���A���A��A�XA�"�A� �A�-A�1A��A�VA�$�A�%A�-A�&�A��A�1'A�p�A�A�^5A%A}��A}XAv��Au��As�AnZAfbNAa�A\{AU��AR~�AL��AH�AEl�AE33A?x�A4�HA+VA$��AA�AhsAp�@���@ꗍ@�(�@�1@���@�bN@�5?@�I�@� �@��#@���@�Ĝ@�$�@��@�ƨ@��@�Ĝ@�$�@�K�@m@d�D@ZJ@P  @E�@>��@7�@.ȴ@+"�@"��@��@&�@Z@�w@�-@
�@��@S�@ ��?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
�B
�B
�B
+B
2-B
1'B
�7B
�^B
��B
�NB
��B%�B'�BL�BZB_;B`BBcTBaHB]/BaHBffBaHB^5B\)BVBQ�BO�BL�BJ�BG�BD�B@�B>wB49B(�B�B�B
=BB
��B
�B
�)B
��B
�jB
�B
��B
��B
��B
�{B
x�B
r�B
ffB
L�B
+B
�B	��B	�HB	��B	�RB	��B	��B	�{B	}�B	M�B	"�B	B�`BȴB�!B{�Bk�BffB\)B`BBhsBr�B� B�hB��B�BB��B�/B�HB	B	DB	�B	!�B	S�B	jB	�B	��B	�RB	��B	�/B	�B	��B
VB
�B
&�B
2-B
?}B
D�B
M�B
YB
_;B
e`B
iy11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B
�B
�B
�B
�B
+B
2-B
1'B
�7B
�^B
��B
�NB
��B%�B'�BL�BZB_;B`BBcTBaHB]/BaHBffBaHB^5B\)BVBQ�BO�BL�BJ�BG�BD�B@�B>wB49B(�B�B�B
=BB
��B
�B
�)B
��B
�jB
�B
��B
��B
��B
�{B
x�B
r�B
ffB
L�B
+B
�B	��B	�HB	��B	�RB	��B	��B	�{B	}�B	M�B	"�B	B�`BȴB�!B{�Bk�BffB\)B`BBhsBr�B� B�hB��B�BB��B�/B�HB	B	DB	�B	!�B	S�B	jB	�B	��B	�RB	��B	�/B	�B	��B
VB
�B
&�B
2-B
?}B
D�B
M�B
YB
_;B
e`B
iy11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081010055438  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081010055441  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081010055442  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081010055446  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081010055446  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081010055446  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081010063128                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081013035908  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081013035912  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081013035913  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081013035917  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081013035917  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081013035918  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081013061144                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081013035908  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518064041  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518064042  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518064042  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518064043  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518064043  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518064043  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518064043  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518064043  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518064410                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208074741  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208075142  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208075142  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208075143  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208075144  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208075144  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208075144  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208075144  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208075144  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091209003003                      G�O�G�O�G�O�                