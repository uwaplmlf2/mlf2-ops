CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   ?   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  3�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  4�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  5   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  6   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  7   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  7L   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  8H   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  8�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  9�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  :�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  :�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  ;�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  <�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   =�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   F�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   O�   CALIBRATION_DATE      	   
                
_FillValue                  �  X�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Y   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Y   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Y   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Y   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Y   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    YX   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Yh   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Yl   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Y|   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Y�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Y�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Y�Argo profile    2.2 1.2 19500101000000  2900582 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               eA   JA  20080925045851  20090918073735  A9_67038_101                    2C  D   APEX-SBE 2986                                                   846 @���z�1   @���)&N(@:�     @`��Q�1   ARGOS   A   A   A   @���A  A�ffA�ffB��BD��Bl  B�  B�  B���B�33B���BC ��C
��C�C�C)ffC333C=� CGffC[�CoffC���C��fC���C��fC�ffC��3C�� CǦfCѦfCی�C�fC�s3C�ٚDٚDٚD�fD�3DٚD�3D� D$�3D)�fD.��D3�fD8� D=��DB��DG� DN,�DTY�DZ�3D`�3DgfDm@ Ds��Dy�3D�  D�i�D��3D��3111111111111111111111111111111111111111111111111111111111111111 @�ffAffA���A陚B33BDffBk��B���B���B�ffB�  Bڙ�B�ffC �3C
�3C  C  C)L�C3�C=ffCGL�C[  CoL�C���C���C�� C���C�Y�C��fC��3CǙ�Cљ�Cۀ C噚C�ffC���D�3D�3D� D��D�3D��DٚD$��D)� D.�fD3� D8��D=�3DB�fDG��DN&fDTS3DZ��D`��Dg  Dm9�Ds�3Dy��D��D�ffD�� D�� 111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�A��A�?}A�K�Aɗ�A�S�A��A���A�t�A�?}A��HA��RA�5?A�C�A�"�A���A�t�A�bNA��A���A���A��A���A�A�VA��A�r�A�/Ax��ApȴAY�TAXQ�AVM�AT-AMƨA9+A,�9A�DA�uAO�AK�A��@�5?@Ý�@��R@�=q@���@�X@�O�@���@�z�@�z�@��w@��
@�Ĝ@��7@�`B@y�7@kt�@^ff@T�@L9X@I�7111111111111111111111111111111111111111111111111111111111111111 A�A��A�?}A�K�Aɗ�A�S�A��A���A�t�A�?}A��HA��RA�5?A�C�A�"�A���A�t�A�bNA��A���A���A��A���A�A�VA��A�r�A�/Ax��ApȴAY�TAXQ�AVM�AT-AMƨA9+A,�9A�DA�uAO�AK�A��@�5?@Ý�@��R@�=q@���@�X@�O�@���@�z�@�z�@��w@��
@�Ĝ@��7@�`B@y�7@kt�@^ff@T�@L9X@I�7111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�B	��B
.B
H�B
�+BB\B�B(�B0!B6FBB�BB�BB�BB�BB�BA�B?}B9XB33B.B(�B�BPB
�B
��B
ĜB
�!B
�VB
gmB
B	��B	�B	�sB	ɺB	n�B	G�B	uB��B�B�B��B��B��B�RB��B�B�;B�;B�BB�HB�mB	B	\B	"�B	1'B	M�B	`BB	{�B	�{B	��B	�dB	��111111111111111111111111111111111111111111111111111111111111111 B	�B	��B
.B
T�B
�{B%BhB�B)�B1'B8RBB�BB�BC�BC�BB�BA�B?}B:^B49B.B)�B�BVB
�B
��B
ŢB
�'B
�\B
jB
B	��B	�B	�yB	��B	p�B	I�B	{B��B�B�#B��B��B��B�XB��B�B�;B�;B�BB�HB�sB	B	\B	"�B	2-B	M�B	`BB	{�B	�{B	��B	�dB	��111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<D��<T��<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810030805402008100308054020081003080540200810030824302008100308243020081003082430200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080925045848  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080925045851  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080925045852  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080925045856  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080925045856  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080925045856  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080925052022                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080929041935  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080929041940  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080929041941  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080929041944  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080929041945  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080929041945  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080929064403                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080929041935  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519015200  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519015200  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519015200  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519015202  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519015202  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519015202  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519015202  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519015202  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090519015526                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080928173325  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20081003080540  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081003080540  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081003082430  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090918073642  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090918073735                      G�O�G�O�G�O�                