CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900582 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               _A   JA  20080827105733  20090918073737  A9_67038_095                    2C  D   APEX-SBE 2986                                                   846 @��x��|�1   @��zd۔@:����F@`�O�;dZ1   ARGOS   A   A   A   @�33A��A�33A홚B��BF  Bl  B�33B�33B���B�  B�ffBC33C  C  C�fC(�fC3  C=ffCG�C[��CoL�C��3C��fC���C���C�� C��fC���Cǀ C�� C�� C�3CC���D� D�3D� D�3D�fD� DٚD$��D)�fD.� D3ٚD8��D=��DB��DG� DN3DTFfDZy�D`�3Dg�DmL�Dsy�Dy�3D�)�D�l�D���D��D�,�D�l�D���D�l�D��fD�ffD���D�l�D�,�111111111111111111111111111111111111111111111111111111111111111111111111@�  A  A�ffA���BffBE��Bk��B�  B�  B�ffB���B�33B�ffC�C
�fC�fC��C(��C2�fC=L�CG  C[� Co33C��fC���C���C���C�s3C���C���C�s3Cѳ3C۳3C�fC��C�� DٚD��DٚD��D� D��D�3D$�fD)� D.ٚD3�3D8�3D=�fDB�3DG��DN�DT@ DZs3D`��DgfDmFfDss3Dy��D�&fD�i�D��fD��fD�)�D�i�D�ٚD�i�D��3D�c3D�ٚD�i�D�)�111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�Q�A�XA�^5A�?}AͮA�VA�v�A�bNA�1'A�S�A�G�A���A�=qA��FA�C�A��7A�\)A��A�1'A�ZA��FA��/A���A��7A�jA�bA�l�A��yA|��Aul�Al��Af��A_�^AW�AOK�AH$�A@$�A:��A2bA(��A ��A�A�Av�@��@���@֗�@�=q@�b@���@�Z@���@�1'@���@�M�@���@�5?@��@~V@r~�@a��@Z^5@R^5@IX@@bN@+ƨ@!G�@�`@�^@��@?���111111111111111111111111111111111111111111111111111111111111111111111111A�Q�A�XA�^5A�?}AͮA�VA�v�A�bNA�1'A�S�A�G�A���A�=qA��FA�C�A��7A�\)A��A�1'A�ZA��FA��/A���A��7A�jA�bA�l�A��yA|��Aul�Al��Af��A_�^AW�AOK�AH$�A@$�A:��A2bA(��A ��A�A�Av�@��@���@֗�@�=q@�b@���@�Z@���@�1'@���@�M�@���@�5?@��@~V@r~�@a��@Z^5@R^5@IX@@bN@+ƨ@!G�@�`@�^@��@?���111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�yB	�yB	�B
1B
��B
�BB�B$�B0!BK�BS�BS�BR�BP�BM�BJ�BH�BE�BB�B?}B9XB0!B,B�B	7B
�B
�/B
��B
{�B
VB
49B
�B	�B	ɺB	��B	�+B	q�B	S�B	33B	�B	B�mB�}B�B��B��B��B��B�B�jB�B�B��B��B	bB	"�B	6FB	R�B	jB	�7B	�{B	�B	��B	��B
B
�B
2-B
D�B
Q�B
bNB
o�111111111111111111111111111111111111111111111111111111111111111111111111B	�yB	�yB	�B
uB
ĜB
�B+B�B&�B2-BN�BVBT�BS�BP�BN�BJ�BI�BE�BB�B?}B:^B0!B-B�B
=B
��B
�;B
��B
|�B
W
B
5?B
�B	�B	��B	��B	�1B	r�B	T�B	49B	�B	B�yB��B�B��B��B��B��B�B�qB�B�B��B��B	bB	"�B	6FB	R�B	jB	�7B	�{B	�B	��B	��B
B
�B
2-B
D�B
Q�B
bNB
o�111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809030757142008090307571420080903075714200809030819242008090308192420080903081924200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080827105730  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080827105733  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080827105734  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080827105738  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080827105738  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080827105738  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080827110742                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080830042904  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080830042909  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080830042909  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080830042913  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080830042913  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080830042914  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080830051056                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080830042904  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519015144  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519015145  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519015145  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519015146  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519015146  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519015146  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519015146  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519015147  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519015529                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080829173856  CV  DAT$            G�O�G�O�F�[�                JM  ARCAJMQC1.0                                                                 20080903075714  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080903075714  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080903081924  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090918073646  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090918073737                      G�O�G�O�G�O�                