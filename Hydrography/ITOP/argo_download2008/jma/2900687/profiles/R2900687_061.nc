CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900687 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               =A   JA  20081020055552  20090416072436                                  2B  A   APEX-SBE 2968                                                   846 @��5�%�1   @��5Gޠ@8.V�u@b]O�;dZ1   ARGOS   A   A   A   @���A33Ac33A�ffA���A�  B��B��B2ffBF��BZffBm33B�ffB�ffB�ffB�  B���B�33B�33Bƙ�B���Bڙ�B�33B�  B���CffC33C��CffC33C  C�C$33C(��C.33C333C8�C=ffCB�CF�fCQ33C[�Cd�fCoffCx�fC��3C��fC���C��3C��3C�s3C�ffC�s3C�� C�s3C���C��fC��fC�Cǌ�C̦fC�s3Cֳ3Cۙ�C�3C�ffC� C� C���C���D�3D�fD�3D�fD��D�3D�3D$�fD)ٚD.��D3�fD8� D=�fDB� DG� DLٚDQ��DV�3D[�3D`�3De�3Dj��Do�fDt�fDy� D�&fD�ffD��3D�� D�#3D�Y�D�� D�� D�&fD�i�D�� D��D�#3D�ffDڜ�D��D��D�i�D�D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA  A`  A���A�33A�ffB
��B  B1��BF  BY��BlffB��B�  B�  B���B�34B���B���B�34B�fgB�34B���BB�34C33C  CfgC33C  C��C�gC$  C(��C.  C3  C7�gC=33CA�gCF�3CQ  CZ�gCd�3Co33Cx�3C���C���C�� C���C���C�Y�C�L�C�Y�C�ffC�Y�C�s3C���C���C�s3C�s3Č�C�Y�C֙�Cۀ C���C�L�C�ffC�ffC� C�s3D�fDٙD�fD��D� D�fD�fD$ٙD)��D.� D3��D8�3D=��DB�3DG�3DL��DQ��DV�fD[�fD`�fDe�fDj� Do��Dt��Dy�3D�  D�` D���D�ٚD��D�S4D���D�ٚD�  D�c4D���D��4D��D�` DږgD��4D�gD�c4D�4D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�bNA�r�A�t�A�r�A�t�A�Q�A�;dAݡ�AڼjA���A�S�A�7LA�(�A�ƨA�jA�G�A�5?A��!A���A���A��A���A���A�;dA�A�E�A�dZA��TA�1'A�A�A�C�A�/A���A���A�&�A��9A��hA�\)A���A�bA�9XA��wA��A��PA��^A�A���A�S�A�S�A�I�A}33AwS�AtȴAp��Am��Aj$�AgAap�A\��AZ^5AS��AO��AJ�HAD�`A>bNA8��A3A/�;A-?}A(M�A�/A��AAoA�@�hs@�V@���@ŉ7@��@���@��u@�+@�x�@�S�@� �@�@�5?@�Q�@�t�@�Q�@yX@t��@n�y@h  @_�@Zn�@Qhs@G��@=V@3�
@)��@#��@ A�@p�@Q�@��@`B@
�@�P@��@X?�j?�r�?�Z1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�bNA�r�A�t�A�r�A�t�A�Q�A�;dAݡ�AڼjA���A�S�A�7LA�(�A�ƨA�jA�G�A�5?A��!A���A���A��A���A���A�;dA�A�E�A�dZA��TA�1'A�A�A�C�A�/A���A���A�&�A��9A��hA�\)A���A�bA�9XA��wA��A��PA��^A�A���A�S�A�S�A�I�A}33AwS�AtȴAp��Am��Aj$�AgAap�A\��AZ^5AS��AO��AJ�HAD�`A>bNA8��A3A/�;A-?}A(M�A�/A��AAoA�@�hs@�V@���@ŉ7@��@���@��u@�+@�x�@�S�@� �@�@�5?@�Q�@�t�@�Q�@yX@t��@n�y@h  @_�@Zn�@Qhs@G��@=V@3�
@)��@#��@ A�@p�@Q�@��@`B@
�@�P@��@X?�j?�r�?�Z1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
�B
�B
�B
�B
�B
�dB�B%�B33BF�BbNBn�BjBjBffBcTBJ�B�B,B7LB=qBB�B[#B\)B[#BYBW
BR�BN�BM�BI�BG�BE�BD�BA�B:^B33B-B&�B�BbBB
��B
�B
�TB
��B
ÖB
�FB
��B
�\B
u�B
jB
VB
C�B
5?B
 �B
B	�B	�/B	�qB	�-B	��B	� B	ffB	M�B	5?B	(�B	�B	B�ZBȴB��B�oB�Bm�BcTB_;B]/Be`Bp�B|�B�bB��B�XBĜB�B�sB�B	PB	�B	2-B	>wB	L�B	bNB	�hB	��B	�^B	��B	�B	��B
uB
�B
&�B
,B
7LB
D�B
L�B
Q�B
W
B
^5B
e`B
l�B
p�B
u�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�B
�B
�B
�B
�B
�B
�dB�B%�B33BF�BbNBn�BjBjBffBcTBJ�B�B,B7LB=qBB�B[#B\)B[#BYBW
BR�BN�BM�BI�BG�BE�BD�BA�B:^B33B-B&�B�BbBB
��B
�B
�TB
��B
ÖB
�FB
��B
�\B
u�B
jB
VB
C�B
5?B
 �B
B	�B	�/B	�qB	�-B	��B	� B	ffB	M�B	5?B	(�B	�B	B�ZBȴB��B�oB�Bm�BcTB_;B]/Be`Bp�B|�B�bB��B�XBĜB�B�sB�B	PB	�B	2-B	>wB	L�B	bNB	�hB	��B	�^B	��B	�B	��B
uB
�B
&�B
,B
7LB
D�B
L�B
Q�B
W
B
^5B
e`B
l�B
p�B
u�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081020055549  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081020055552  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081020055553  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020055553  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020055557  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081020055557  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020055558  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081020055558  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081020055558  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020062746                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081024041828  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081024041832  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081024041832  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081024041833  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081024041837  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081024041837  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081024041837  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081024041837  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081024041837  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081024063936                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081024041828  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416072038  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416072039  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416072039  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416072039  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416072040  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416072040  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416072040  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416072040  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416072040  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416072436                      G�O�G�O�G�O�                