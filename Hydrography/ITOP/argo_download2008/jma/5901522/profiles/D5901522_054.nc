CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901522 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               6A   JA  20080811105335  20090908020758  A9_66094_054                    2C  D   APEX-SBE 2803                                                   846 @���$�1   @��:��@2��1&�@a��E���1   ARGOS   A   A   A   @���A��Ah  A�ffA�33A�ffB
ffBffB333BE��BY��Bm33B���B�  B�  B���B���B���B���B�33B�  B�  B�ffB�ffB�ffC�C� C� C� C��CL�CL�C$L�C)�C-�fC3ffC8�C=L�CB33CG�3CQ� C[L�Cd�fCo  Cy33C��3C���C�� C�s3C�� C�s3C���C���C�� C��fC���C��3C���C¦fCǀ C̦fCљ�C֙�Cۙ�C�� C�fC� C�3C��3C��3D��D� D� D� D�3D��DٚD$�3D)�3D.�fD3ٚD8�3D=ٚDB�3DG�fDL� DQ� DV� D[�3D`�fDe� Dj�3Do� Dt�3Dy��D�,�D�` D���D��3D�&fD�i�D�� D���D��D�i�D���D��fD�0 D�l�Dک�D��fD��D�Y�D�D�p 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA33AfffA���A�ffA홚B
  B  B2��BE33BY33Bl��B�ffB���B���B�ffB�ffB�ffB���B�  B���B���B�33B�33B�33C  CffCffCffC� C33C33C$33C)  C-��C3L�C8  C=33CB�CG��CQffC[33Cd��Cn�fCy�C��fC���C�s3C�ffC�s3C�ffC�� C�� C��3C���C���C��fC�� C�C�s3C̙�Cь�C֌�Cی�C�3C噚C�s3C�fC��fC��fD�fD��D��DٚD��D�fD�3D$��D)��D.� D3�3D8��D=�3DB��DG� DLٚDQ��DV��D[��D`� DeٚDj��Do��Dt��Dy�fD�)�D�\�D��fD�� D�#3D�ffD���D�ٚD��D�ffD��fD��3D�,�D�i�DڦfD��3D�fD�VfD�fD�l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�1A��;A�hA�PA�-A��A�&�A�9A�1'A��A���A��
A�n�A�G�A��A��A��yA��Ả7A���A�`BA�JA�$�A���A�&�A�A��A���A�?}A�=qA���A��A�`BA�ĜA�`BA�{A�A�dZA�|�A��7A���A�M�A�1A���A���A�7LA�-A�ffA�G�A�`BA�|�A��\A��A�jA���A}oAw��Aot�An��AdA�A^��AX��AP5?AJ�RAA�hA;p�A4VA.�/A,��A(��A!�A�FAI�A�@��u@�@�;d@�@�+@��@��/@�{@��7@���@��T@�j@�v�@��@���@��@���@��`@��\@�;d@�@rn�@hbN@_�P@T(�@NE�@G�w@>�R@6�+@/��@*-@%��@!7L@��@��@�@S�@A�@�@	7L@+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�1A��;A�hA�PA�-A��A�&�A�9A�1'A��A���A��
A�n�A�G�A��A��A��yA��Ả7A���A�`BA�JA�$�A���A�&�A�A��A���A�?}A�=qA���A��A�`BA�ĜA�`BA�{A�A�dZA�|�A��7A���A�M�A�1A���A���A�7LA�-A�ffA�G�A�`BA�|�A��\A��A�jA���A}oAw��Aot�An��AdA�A^��AX��AP5?AJ�RAA�hA;p�A4VA.�/A,��A(��A!�A�FAI�A�@��u@�@�;d@�@�+@��@��/@�{@��7@���@��T@�j@�v�@��@���@��@���@��`@��\@�;d@�@rn�@hbN@_�P@T(�@NE�@G�w@>�R@6�+@/��@*-@%��@!7L@��@��@�@S�@A�@�@	7L@+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�B�B��B��B	DB	�/B
bNB
��B
��B\B;dB`BBe`B_;B`BBiyBcTBe`BdZBhsBp�Bo�Bq�B{�B~�B�B�7B�bB��B�oB�{B��B��B��B��B��B��B��B��B�B�B�B��B�bB~�BiyBT�BA�B.B �BPB
��B
�ZB
ɺB
�FB
��B
}�B
T�B
K�B
#�B	��B	�5B	�^B	��B	z�B	aHB	?}B	)�B	�B	JB�B��B�FB��B��B��B��B��B�-B�mB�B	bB	�B	7LB	^5B	q�B	�B	��B	��B	�B	�XB	ȴB	��B	�fB	�B	��B
1B
�B
 �B
0!B
8RB
>wB
B�B
L�B
O�B
W
B
`BB
`BB
e`B
hsB
k�B
n�B
s�B
w�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�B�B��B��B	JB	�5B
e`B
��B
��BbB<jBcTBffB`BBbNBk�Be`BffBe`BiyBp�Bp�Bs�B|�B� B�B�DB�hB��B�uB��B��B��B��B��B��B��B��B��B�B�'B�B��B�hB�BjBVBB�B/B!�BVB
��B
�`B
��B
�RB
��B
� B
T�B
N�B
$�B
B	�BB	�dB	��B	|�B	cTB	@�B	+B	�B	PB�B�B�RB��B��B��B��B��B�3B�mB�B	bB	�B	7LB	^5B	q�B	�B	��B	��B	�B	�XB	ȴB	��B	�fB	�B	��B
1B
�B
 �B
0!B
8RB
>wB
B�B
L�B
O�B
W
B
`BB
`BB
e`B
hsB
k�B
n�B
s�B
w�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808240808392008082408083920080824080839200808240825052008082408250520080824082505200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080811105332  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080811105335  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080811105335  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080811105339  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080811105340  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080811105340  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080811110345                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080815040441  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080815040445  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080815040446  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080815040450  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080815040450  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080815040451  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080815051051                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080815040441  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519071710  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519071710  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519071710  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519071712  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519071712  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519071712  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519071712  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519071712  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071924                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080814171619  CV  DAT$            G�O�G�O�F�=�                JM  ARCAJMQC1.0                                                                 20080824080839  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080824080839  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080824082505  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020717  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908020758                      G�O�G�O�G�O�                