CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901522 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               <A   JA  20081010055414  20090908020805  A9_66094_060                    2C  D   APEX-SBE 2803                                                   846 @���6`|1   @����   @3Լj~��@a�n��O�1   ARGOS   A   A   A   @���AffAc33A�  A�  A�33B	��B  B2ffBF��BZ  BnffB�33B�ffB�  B���B�ffB���B���Bƙ�B�ffB�  B�33B�ffB���CffCL�C��CL�CffC  C��C$�C)ffC.�C3L�C7��C<�fCB33CGffCQ  C[L�CeL�Co��CyffC��3C���C��3C���C���C��fC��fC���C���C�� C�� C�� C���C�Cǳ3C�� C�s3C֙�Cۀ C�ffC�fC�s3C�3C���C��3D��D� D�fD��D�3D� D� D$ٚD)��D.��D3��D8�3D=�fDB� DG�3DL��DQ�fDV��D[ٚD`��De� Dj� Do�3Dt��Dy�fD�  D�l�D��3D��D�#3D�ffD�� D��3D��D�ffD���D�� D�,�D�c3DڦfD���D�#3D�l�D�D�p 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA��Aa��A�33A�33A�ffB	33B��B2  BFffBY��Bn  B�  B�33B���B���B�33B�ffB�ffB�ffB�33B���B�  B�33B�ffCL�C33C� C33CL�C�fC�3C$  C)L�C.  C333C7�3C<��CB�CGL�CP�fC[33Ce33Co� CyL�C��fC���C��fC�� C���C���C���C�� C���C��3C�s3C�s3C�� C CǦfC̳3C�ffC֌�C�s3C�Y�C噚C�ffC�fC�� C��fD�fD��D� D�fD��DٚDٚD$�3D)�3D.�fD3�fD8��D=� DB��DG��DL�3DQ� DV�fD[�3D`�3DeٚDjٚDo��Dt�fDy� D��D�i�D�� D��fD�  D�c3D���D�� D��D�c3D��fD���D�)�D�` Dڣ3D��D�  D�i�D�fD�l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�7LA�/A�1'A�5?A�=qA�O�A�Q�A�S�A�Q�A�O�A��`A�Q�A�$�A�K�A�=qAϲ-AȁAŰ!AhA���A�n�A�bNA���A���A���A��;A���A�jA���A�n�A�dZA�x�A�33A��DA��#A��A���A�~�A�/A�A���A���A�oA��A�ȴA�7LA�A~�Ay?}ArQ�AlAe"�A^�AU��AR��AM�AEdZA=�FA<=qA8��A6jA1hsA/G�A*��A%G�A$(�AA\)AƨA�A
��Ax�@��@ߕ�@ҧ�@��@���@�{@�ff@��`@���@�^5@�x�@���@�bN@�o@���@�|�@���@�{@��;@�G�@�b@t��@q��@j�!@]�@W�;@R��@K��@?��@9�7@2n�@*�!@"n�@V@��@��@  @@33@��@/@��@   1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�7LA�/A�1'A�5?A�=qA�O�A�Q�A�S�A�Q�A�O�A��`A�Q�A�$�A�K�A�=qAϲ-AȁAŰ!AhA���A�n�A�bNA���A���A���A��;A���A�jA���A�n�A�dZA�x�A�33A��DA��#A��A���A�~�A�/A�A���A���A�oA��A�ȴA�7LA�A~�Ay?}ArQ�AlAe"�A^�AU��AR��AM�AEdZA=�FA<=qA8��A6jA1hsA/G�A*��A%G�A$(�AA\)AƨA�A
��Ax�@��@ߕ�@ҧ�@��@���@�{@�ff@��`@���@�^5@�x�@���@�bN@�o@���@�|�@���@�{@��;@�G�@�b@t��@q��@j�!@]�@W�;@R��@K��@?��@9�7@2n�@*�!@"n�@V@��@��@  @@33@��@/@��@   1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	C�B	C�B	C�B	B�B	C�B	E�B	F�B	F�B	H�B	J�B	��B
��B
�B
��B
��B
�B
��BA�BffBo�Bv�B}�B�bB��B��B��B��B��B�jB��B�}B�dB��B��B��B�JB}�Bm�BgmBT�BB�B&�B�BB
�BB
�B
��B
�=B
o�B
L�B
+B
B	�BB	�^B	�B	�oB	t�B	YB	N�B	A�B	9XB	+B	"�B	�B	B��B�B�5B�#B�#BŢB�jB�}B�^B�B�#B�
B�yB��B	B	%�B	A�B	YB	m�B	}�B	�=B	��B	�9B	�}B	��B	�B	�/B	�;B	�B	��B
B
oB
�B
#�B
,B
9XB
=qB
B�B
B�B
H�B
Q�B
YB
`BB
e`B
iyB
m�B
r�B
w�B
|�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	C�B	C�B	C�B	B�B	C�B	E�B	F�B	F�B	H�B	L�B	�B
��B
��B
��B
��B
�B  BD�BhsBq�By�B�B�uB��B�B��B��B�B�wBB��B�jB��B��B��B�VB~�Bm�BjBVBD�B'�B�BB
�NB
�!B
��B
�DB
q�B
N�B
-B
B	�NB	�dB	�B	�{B	v�B	YB	O�B	B�B	:^B	,B	#�B	�B	B��B�B�BB�)B�)BƨB�qB��B�dB�!B�)B�B�yB��B	B	%�B	A�B	YB	m�B	}�B	�=B	��B	�9B	�}B	��B	�B	�/B	�;B	�B	��B
B
oB
�B
#�B
,B
9XB
=qB
B�B
B�B
H�B
Q�B
YB
`BB
e`B
iyB
m�B
r�B
w�B
|�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810230756352008102307563520081023075635200810230814522008102308145220081023081452200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20081010055412  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081010055414  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081010055415  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081010055419  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081010055419  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081010055420  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081010063056                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081014040612  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081014040616  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081014040617  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081014040626  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081014040626  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081014040627  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081014061547                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081014040612  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519071725  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519071725  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519071725  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519071727  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519071727  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519071727  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519071727  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519071727  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071912                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081013171653  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20081023075635  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081023075635  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081023081452  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020727  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908020805                      G�O�G�O�G�O�                