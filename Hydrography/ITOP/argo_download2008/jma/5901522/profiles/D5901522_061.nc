CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901522 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               =A   JA  20081020055423  20090908020808  A9_66094_061                    2C  D   APEX-SBE 2803                                                   846 @��;6lw�1   @��>���@4=p��
=@a�����1   ARGOS   A   A   A   @�33A  Ac33A�33A���A�ffB
ffB��B2  BG33B[33Bl��B���B���B�33B���B�ffB���B���B���B�ffB���B�  B�33B���C33CffC�C��C��CffCffC$� C)ffC.ffC3  C833C=�CB�CF�fCQffC[L�CeffCoffCy33C���C��3C�� C��fC��3C���C�ffC�ffC��3C�s3C�� C���C��fC³3CǦfC̳3Cр Cֳ3C�s3C���C�3CꙚC�ffC��fC��fD� D�fD�fD��DٚD�3D�3D$� D)ٚD.��D3�3D8�3D=��DBٚDG�3DL�fDQ�3DV�3D[� D`��De�3Dj�3Do� DtٚDy��D�0 D�p D��fD��D�&fD�i�D��fD��fD�fD�ffD��fD���D�0 D�ffDڦfD���D�&fD�\�D�fD�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  AffAa��A�ffA���A陚B
  BffB1��BF��BZ��BlffB�ffB���B�  B�ffB�33B�ffB���Bƙ�B�33Bڙ�B���B�  B�ffC�CL�C  C� C� CL�CL�C$ffC)L�C.L�C2�fC8�C=  CB  CF��CQL�C[33CeL�CoL�Cy�C���C��fC��3C���C��fC�� C�Y�C�Y�C��fC�ffC�s3C���C���C¦fCǙ�C̦fC�s3C֦fC�ffC�� C�fC��C�Y�C���C���DٚD� D� D�3D�3D��D��D$ٚD)�3D.�3D3��D8��D=�3DB�3DG��DL� DQ��DV��D[��D`�3De��Dj��DoٚDt�3Dy�3D�,�D�l�D��3D��fD�#3D�ffD��3D��3D�3D�c3D��3D��D�,�D�c3Dڣ3D�ٚD�#3D�Y�D�3D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�?}A�?}A�A�A�A�A�E�A�G�A�G�A�x�A� �A��A�jAҼjA�{A��
A��ȂhA�-A��AöFA�XA�C�A�K�A��DA�I�A�I�A��TA��hA��/A��^A���A���A�+A��9A�v�A��A�Q�A��A��A��!A���A�-A�p�A��DA��-A���A��#A��A��A��-A��+A�A��A���AhsAy�;As�Aq��AnI�Ak�Ae��A[��AW�AN�+AK+AG��AB�A??}A7�;A-�hA(�9A�-A�A�/A�@�S�@�  @�A�@�+@�ȴ@���@��@�x�@���@���@�dZ@�@�\)@��9@��y@��@�(�@�\)@�z�@|�/@u�h@nv�@a�@R��@F5?@A&�@>ff@97L@/��@,I�@(�@$��@ Ĝ@��@�m@ �@"�@��@1@G�@ Q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�?}A�?}A�A�A�A�A�E�A�G�A�G�A�x�A� �A��A�jAҼjA�{A��
A��ȂhA�-A��AöFA�XA�C�A�K�A��DA�I�A�I�A��TA��hA��/A��^A���A���A�+A��9A�v�A��A�Q�A��A��A��!A���A�-A�p�A��DA��-A���A��#A��A��A��-A��+A�A��A���AhsAy�;As�Aq��AnI�Ak�Ae��A[��AW�AN�+AK+AG��AB�A??}A7�;A-�hA(�9A�-A�A�/A�@�S�@�  @�A�@�+@�ȴ@���@��@�x�@���@���@�dZ@�@�\)@��9@��y@��@�(�@�\)@�z�@|�/@u�h@nv�@a�@R��@F5?@A&�@>ff@97L@/��@,I�@(�@$��@ Ĝ@��@�m@ �@"�@��@1@G�@ Q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	1'B	2-B	1'B	2-B	49B	5?B	7LB	VB	^5B	� B
L�B
�B
��B
�qB
��B
�TB%B�BI�By�B� B�=B�oB��B�?B�XB�?B�LB��B��BÖB�wB�^B�B��B�{Bt�BiyBdZBcTBQ�B@�B8RB0!B#�B�B\B1B
��B
�B
�B
��B
ƨB
��B
�DB
p�B
cTB
P�B
C�B
#�B	��B	�5B	�wB	��B	��B	� B	n�B	M�B	�B	1B�B�FB��B��B��B��B��B��B�!BɺB�B��B	B	"�B	A�B	[#B	q�B	� B	�hB	��B	�B	ÖB	��B	��B	��B	�ZB
B
JB
�B
!�B
%�B
1'B
6FB
@�B
F�B
K�B
R�B
ZB
cTB
iyB
o�B
u�B
y�B
}�B
� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	1'B	2-B	1'B	2-B	49B	5?B	7LB	VB	aHB	�DB
S�B
�B
��B
�wB
��B
�ZB1B �BM�B{�B�B�JB�uB��B�LB�^B�FB�XBB��BĜB��B�qB�B��B��Bu�BiyBe`BdZBR�BA�B8RB1'B$�B�B\B	7B
��B
�B
�B
��B
ǮB
�B
�JB
q�B
dZB
Q�B
E�B
%�B	��B	�BB	�}B	��B	��B	�B	p�B	P�B	�B	
=B�B�LB�B��B��B��B��B��B�'B��B�B��B	B	#�B	A�B	[#B	q�B	� B	�hB	��B	�B	ÖB	��B	��B	��B	�ZB
B
JB
�B
!�B
%�B
1'B
6FB
@�B
F�B
K�B
R�B
ZB
cTB
iyB
o�B
u�B
y�B
}�B
� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200811020804182008110208041820081102080418200811020810232008110208102320081102081023200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20081020055420  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081020055423  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020055424  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020055428  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020055428  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081020055429  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020062621                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081024040920  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081024040925  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081024040926  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081024040930  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081024040930  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081024040931  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081024061425                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081024040920  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519071727  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519071728  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519071728  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519071729  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519071729  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519071729  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519071729  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519071729  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071915                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081023165632  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20081102080418  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081102080418  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081102081023  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020731  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908020808                      G�O�G�O�G�O�                