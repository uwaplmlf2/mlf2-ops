CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901522 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               9A   JA  20080910105255  20090908020757  A9_66094_057                    2C  D   APEX-SBE 2803                                                   846 @��4��1   @��7�N�@2K��Q�@a�hr� �1   ARGOS   A   A   A   @���AffA^ffA�  A�ffA���B	��B��B2ffBF  BZffBm��B���B�ffB�33B�  B�  B�  B���Bƙ�B�ffB�33B�ffB�ffB�ffC� CffC
��C33C33CL�C��C$� C)33C.�C3L�C7�fC=L�CB  CG33CQffC[L�Ce��Co� Cy�C��fC�� C���C���C��fC�s3C��3C�s3C��3C���C���C��fC���C³3Cǌ�C̙�Cѳ3C�� C۳3C���C�fCꙚCC��C���D� D� D� D�3D��D� D�3D$�3D)�fD.�fD3ٚD8�fD=�fDB��DG� DLٚDQ�3DV� D[ٚD`�fDeٚDj�3Do�fDt��Dy�3D�#3D�` D���D��fD��D�l�D���D���D�#3D�ffD���D�� D�,�D�c3Dڜ�D���D�  D�Y�D�fD�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffA^ffA�  A�ffA���B	��B��B2ffBF  BZffBm��B���B�ffB�33B�  B�  B�  B���Bƙ�B�ffB�33B�ffB�ffB�ffC� CffC
��C33C33CL�C��C$� C)33C.�C3L�C7�fC=L�CB  CG33CQffC[L�Ce��Co� Cy�C��fC�� C���C���C��fC�s3C��3C�s3C��3C���C���C��fC���C³3Cǌ�C̙�Cѳ3C�� C۳3C���C�fCꙚCC��C���D� D� D� D�3D��D� D�3D$�3D)�fD.�fD3ٚD8�fD=�fDB��DG� DLٚDQ�3DV� D[ٚD`�fDeٚDj�3Do�fDt��Dy�3D�#3D�` D���D��fD��D�l�D���D���D�#3D�ffD���D�� D�,�D�c3Dڜ�D���D�  D�Y�D�fD�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�1'A�?}A���A�^A��A���A��A�A�l�A���Aڴ9A�A�-A�Q�A�I�A��A�ƨA�7LA�S�A��yA�n�A�  A��A�r�A�1A�`BA��jA�9XA��A��A��PA��HA�9XA���A��A�oA���A�n�A�S�A�S�A�/A�  A�ȴA�/A���A���A��\A�~�A�ZA��hA���A~r�Ax�AvbNAn~�A_�mAV��ARJAIAB�`A<1'A6ȴA.�\A* �A#�^AQ�A-A��A��A�jA(�@�l�@�+@���@Гu@�ȴ@�Ĝ@�$�@�7L@�Ĝ@��+@�-@���@��9@�?}@�ȴ@�r�@��@�Z@�=q@���@��@���@|�D@x�@o�w@`Q�@XĜ@Rn�@I�@@1'@6��@1��@*��@%/@!&�@?}@-@��@��@E�@Z@	��@�@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�1'A�?}A���A�^A��A���A��A�A�l�A���Aڴ9A�A�-A�Q�A�I�A��A�ƨA�7LA�S�A��yA�n�A�  A��A�r�A�1A�`BA��jA�9XA��A��A��PA��HA�9XA���A��A�oA���A�n�A�S�A�S�A�/A�  A�ȴA�/A���A���A��\A�~�A�ZA��hA���A~r�Ax�AvbNAn~�A_�mAV��ARJAIAB�`A<1'A6ȴA.�\A* �A#�^AQ�A-A��A��A�jA(�@�l�@�+@���@Гu@�ȴ@�Ĝ@�$�@�7L@�Ĝ@��+@�-@���@��9@�?}@�ȴ@�r�@��@�Z@�=q@���@��@���@|�D@x�@o�w@`Q�@XĜ@Rn�@I�@@1'@6��@1��@*��@%/@!&�@?}@-@��@��@E�@Z@	��@�@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	1'B	>wB	>wB	=qB	C�B	E�B	G�B	bNB	�LB
L�B
n�B
�+B
ǮB
��B=qBE�Bk�B�B�uB�BȴB�NB�B��B��B��BbB{B��BÖB�^B�?B�B��B��B�+By�Bn�BbNBQ�BN�BR�BQ�B.B$�B�BJBB
��B
�5B
ƨB
��B
�B
u�B
M�B
+B	�B	�qB	��B	}�B	_;B	D�B	�B	VB��B�ZB�B��BƨB�dB��B��B��B��B��B�9B��B�B�yB��B	�B	<jB	M�B	\)B	iyB	z�B	�=B	��B	�LB	ŢB	��B	�)B	�fB	�B	��B
B
�B
�B
$�B
.B
8RB
C�B
G�B
P�B
W
B
ZB
_;B
cTB
ffB
k�B
q�B
t�B
w�B
z�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	1'B	>wB	>wB	=qB	D�B	H�B	I�B	gmB	�qB
M�B
p�B
�JB
��B
��B=qBE�Bk�B�B�{B�BɺB�ZB��B��B��BBoB�BÖBŢB�jB�LB�!B��B��B�1Bz�Bo�BdZBR�BO�BS�BT�B/B$�B�BPBB
��B
�;B
ȴB
��B
�B
w�B
Q�B
	7B	�B	�}B	��B	� B	`BB	F�B	�B	bB��B�`B�B��BǮB�jB��B��B��B��B��B�?B��B�B�yB	  B	�B	<jB	M�B	\)B	iyB	z�B	�=B	��B	�LB	ŢB	��B	�)B	�fB	�B	��B
B
�B
�B
$�B
.B
8RB
C�B
G�B
P�B
W
B
ZB
_;B
cTB
ffB
k�B
q�B
t�B
w�B
z�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809230805332008092308053320080923080533200809230829472008092308294720080923082947200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080910105253  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080910105255  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080910105256  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080910105300  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080910105300  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080910105300  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080910110159                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080914050357  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080914050417  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080914050422  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080914050433  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080914050434  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080914050435  QCP$                G�O�G�O�G�O�           10000JA  ARFMdecpA9_b                                                                20080914050357  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519071717  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519071718  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519071718  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519071719  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519071719  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519071719  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519071719  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519071719  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071922                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080913165453  CV  DAT$            G�O�G�O�F�y�                JM  ARCAJMQC1.0                                                                 20080923080533  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080923080533  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080923082947  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020714  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908020757                      G�O�G�O�G�O�                