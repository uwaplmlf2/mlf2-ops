CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   1   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  9`   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  9�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  :X   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  :�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;P   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  <   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  <H   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  =@   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  >   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  >�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  >�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  ?�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ?�   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  @�   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  A|   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  A�   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  Bt   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  Cl   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    C�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  8  O�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    P4   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    PD   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    PH   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         PX   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         P\   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        P`   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    PdArgo profile    2.2 1.2 19500101000000  2900728 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL            DOXY               A   JA  20080824020028  20090707054717                                  2B  A   APEX-SBE 3382                                                   846 @���n�c^1   @��鸂w@=O\(�@bQ����1   ARGOS   A   A   A       @���A1��A���A�33A�  B��B8��B]33B�33B�33B���B�  B�  B�  B���CffCffC��C$�fC.�fC8�3CBL�CJ��CW�fCaL�CkffCt�fCffC�s3C�L�C�L�C�s3C�ffC�Y�C�Y�C�33C�ٚC��fC�� C�L�C�33C�s3C�ffC��C�Y�C��C��C�  C�331111111111111111111111111111111111111111111111111   @�fgA6fgA�  A���A�ffB��B:  B^ffB���B���B�fgB���Bә�B晚B�fgC�3C�3C�gC%33C/33C9  CB��CK�CX33Ca��Ck�3Cu33C�3C���C�s3C�s3C���C���C�� C�� C�Y�C�  C���C��fC�s3C�Y�Cř�Cʌ�C�33CԀ C�@ C�@ C�&fC�Y�1111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A޺^A�JAݏ\A��yA�|�A�(�A��A��
A���A��RA�`BA�&�A�t�A�\)A�+A��A�;dA�dZA��`A�S�A���A�?}A���A��A�=qA�A� �A�"�A�oA�;dA��jA�v�A��A�9XA�n�A|�+Ay\)At^5AnȴAj��AhZAdAa�A]�wAX�AT��AQ��AM�AHr�1111111111111111111111111111111111111111111111111   A޺^A�JAݏ\A��yA�|�A�(�A��A��
A���A��RA�`BA�&�A�t�A�\)A�+A��A�;dA�dZA��`A�S�A���A�?}A���A��A�=qA�A� �A�"�A�oA�;dA��jA�v�A��A�9XA�n�A|�+Ay\)At^5AnȴAj��AhZAdAa�A]�wAX�AT��AQ��AM�AHr�1111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	�JB	� B	�mB
�B
L�B
m�B
�B%B!�B1'B49B8RB49B1'B+B+B$�B"�B �B#�B�B�BbBoB1B
��B
��B
�B
�B
�B
�B
��B
ǮB
�-B
��B
�=B
z�B
[#B
J�B
?}B
-B
�B
oB	��B	�B	�5B	��B	�d1111111111111111111111111111111111111111111111111   B	��B	�JB	� B	�mB
�B
L�B
m�B
�B%B!�B1'B49B8RB49B1'B+B+B$�B"�B �B#�B�B�BbBoB1B
��B
��B
�B
�B
�B
�B
��B
ǮB
�-B
��B
�=B
z�B
[#B
J�B
?}B
-B
�B
oB	��B	�B	�5B	��B	�d1111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C;mC;bC:}/CKZ^CXl�C_�C\��C[!CW�JCJ�jCMU?CI�CJ�JCG��CG�CF�CF��CF�;CE��CF�fCIMPCFՁCG�^CA��CG/CC)yCC)7CA�JCA�CC�'CD(1C<�C@�C;�wC7�C0�sC0�C2�C1��C16�C0�C,��C,W�C,e�C*ffC(�LC'�qC&��C$a�0000000000000000000000000000000000000000000000000   C;mC;bC:}/CKZ^CXl�C_�C\��C[!CW�JCJ�jCMU?CI�CJ�JCG��CG�CF�CF��CF�;CE��CF�fCIMPCFՁCG�^CA��CG/CC)yCC)7CA�JCA�CC�'CD(1C<�C@�C;�wC7�C0�sC0�C2�C1��C16�C0�C,��C,W�C,e�C*ffC(�LC'�qC&��C$a�0000000000000000000000000000000000000000000000000   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA12                                                                 20080824020016  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080824020028  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080824020030  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080824020037  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080824020037  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080824020038  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080824024054                      G�O�G�O�G�O�                JA  ARFMdecpA12                                                                 20080828054548  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080828054555  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828054556  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828054602  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828054603  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828054603  QCP$                G�O�G�O�G�O�               0JA  ARFMdecpA12                                                                 20090601021101  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090601021421  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090601021421  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090601021422  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090601021423  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090601021423  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090601021423  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090601021423  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090601021423  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090601021801                      G�O�G�O�G�O�                JA  ARFMdecpA12a                                                                20090707053233  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090707053722  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090707053722  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090707053723  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090707053724  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090707053724  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090707053724  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090707053724  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090707053724  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090707054717                      G�O�G�O�G�O�                