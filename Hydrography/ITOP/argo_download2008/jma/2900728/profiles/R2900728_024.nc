CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   2   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  9d   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  9�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  :`   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  :�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;\   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  <$   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  <X   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  =    TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  =T   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  >   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  >�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ?   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  ?�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  @   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  @�   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  A�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  A�   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  B�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    C�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  8  O�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    P   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    P   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    P   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    P    HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  P$   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Pd   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Pt   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Px   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         P�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         P�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        P�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    P�Argo profile    2.2 1.2 19500101000000  2900728 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL            DOXY               A   JA  20080829012616  20090707054722                                  2B  A   APEX-SBE 3382                                                   846 @��'n�c^1   @��'����@<�^5?|�@bW��$�1   ARGOS   A   A   A       @�33A4��A~ffA�ffAՙ�B  B8ffB]��B�33B�33B�33B�33B�ffB㙚B�ffC  CL�C��C%  C.�fC8��CB�3CLL�CVffC^ffCk��Cu�3CL�C�s3C�&fC��fC�&fC�33C�L�C�@ C��3C��fC�� C�ٚC�� C���C�s3C�Y�Cό�C�Y�C�33Cތ�C�Y�C� C�Y�11111111111111111111111111111111111111111111111111  @陙A8  A���A�  A�34B��B933B^fgB���B���B���B���B���B�  B���C33C� C��C%33C/�C8��CB�fCL� CV��C^��Cl  Cu�fC� C���C�@ C�� C�@ C�L�C�fgC�Y�C���C�� C���C��4C�ٚC��gCŌ�C�s4CϦgC�s4C�L�CަgC�s4C虚C�s411111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A���A�%A��A�A�K�A��-A�|�A��RA���A�-A��A���A��A��mA��#A�ffA�(�A�  A�bA��PA���A�XA�XA�
=A��`A���A�ƨA��TA�\)A�K�A��PA���A���A���A�M�A~�A{7LAx��Au�hAqO�AnAix�AeAa|�A]"�AX�AT��AM�#AG`B11111111111111111111111111111111111111111111111111  A���A���A�%A��A�A�K�A��-A�|�A��RA���A�-A��A���A��A��mA��#A�ffA�(�A�  A�bA��PA���A�XA�XA�
=A��`A���A�ƨA��TA�\)A�K�A��PA���A���A���A�M�A~�A{7LAx��Au�hAqO�AnAix�AeAa|�A]"�AX�AT��AM�#AG`B11111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	ĜB	ŢB	ÖB	��B	��B
#�B
t�B
��B
�'B
�
BuB%�B5?BC�B@�B;dB33B,B'�B"�B!�B �B�BuBBB
��B
��B
�B
�B
�ZB
�;B
�B
��B
�}B
�!B
��B
��B
�JB
{�B
iyB
XB
C�B
49B
%�B
�B	��B	�B	ȴB	�311111111111111111111111111111111111111111111111111  B	ĜB	ŢB	ÖB	��B	��B
#�B
t�B
��B
�'B
�
BuB%�B5?BC�B@�B;dB33B,B'�B"�B!�B �B�BuBBB
��B
��B
�B
�B
�ZB
�;B
�B
��B
�}B
�!B
��B
��B
�JB
{�B
iyB
XB
C�B
49B
%�B
�B	��B	�B	ȴB	�311111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C9�}C8xRC7�FC:�NCF߾C\�C]��C]�C`8C]	yCOOCK�;CF�PCDݲCC�RCB��CC"�CCw�CC��CC�`CD��CHyCHL�CF�qCF|�CF��CF��CF
CG�#CIm�C@l�C>.�C=ԼC;:�C8F%C7��C4��C4��C9�C7�FC5�TC6Z�C6R�C7ѪC3��C8�mC6�\C3}�C0PbC%��00000000000000000000000000000000000000000000000000  C9�}C8xRC7�FC:�NCF߾C\�C]��C]�C`8C]	yCOOCK�;CF�PCDݲCC�RCB��CC"�CCw�CC��CC�`CD��CHyCHL�CF�qCF|�CF��CF��CF
CG�#CIm�C@l�C>.�C=ԼC;:�C8F%C7��C4��C4��C9�C7�FC5�TC6Z�C6R�C7ѪC3��C8�mC6�\C3}�C0PbC%��00000000000000000000000000000000000000000000000000  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA12                                                                 20080829012611  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080829012616  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080829012617  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080829012621  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080829012621  QCP$                G�O�G�O�G�O�            FB40JA  ARUP                                                                        20080829022006                      G�O�G�O�G�O�                JA  ARFMdecpA12                                                                 20080902042744  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080902042748  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080902042749  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080902042752  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080902042752  QCP$                G�O�G�O�G�O�            FB40JA  ARUP                                                                        20080902051156                      G�O�G�O�G�O�                JA  ARFMdecpA12                                                                 20090601021102  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090601021424  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090601021424  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090601021425  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090601021426  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090601021426  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090601021426  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090601021426  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090601021426  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090601021806                      G�O�G�O�G�O�                JA  ARFMdecpA12a                                                                20090707053234  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090707053724  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090707053725  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090707053726  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090707053727  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090707053727  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090707053727  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090707053727  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090707053727  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090707054722                      G�O�G�O�G�O�                