CDF   (   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   o   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Eh   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Nh   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Wh   CALIBRATION_DATE      	   
                
_FillValue                  �  `h   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a8   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    aH   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    aL   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a\   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a`   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        ad   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    ahArgo profile    2.2 1.2 19500101000000  2900614 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               \A   JA  20080909065317  20090831075417  A9_60138_092                    2C  D   APEX-SBE 2408                                                   846 @����-1   @��􊞇�@7�;dZ�@_+I�1   ARGOS   A   A   A   @�33A��Ah  A���A�  A���B
��B  B1��BFffBZffBm33B�33B���B�  B�  B�33B�33B���B�ffB�ffBڙ�B�ffB�33B�  C��C��C� C�CL�C�C� C$�C)�C.ffC3ffC8�3C=� CBffCGffCQL�C[  Ce� Cn�fCy�C��fC���C���C�� C��fC��3C��fC���C��fC��fC�� C���C��fC¦fC�� C�� Cѳ3C�ٚC�� C�fC�s3CꙚC�fC��3C�� D� D��D��D��D��D� D� D$� D)�3D.��D3�3D8ٚD=ٚDB��DG�fDL�fDQ�3DV� D[ٚD`�3De� DjٚDo�3Dt�fDy�3D�0 D�ffD��fD���D�)�D�l�D���D��3D�&fD�ffD��fD�� D�  D�l�Dڰ D߳3111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA33Aa��A�ffA���A陚B	33BffB0  BD��BX��Bk��B~��B���B�33B�33B�ffB�ffB���Bř�Bϙ�B���B㙚B�ffB�33C33C33C�C�3C�fC�3C�C#�3C(�3C.  C3  C8L�C=�CB  CG  CP�fCZ��Ce�Cn� Cx�3C�s3C�ffC�Y�C�L�C�s3C�� C�s3C�ffC�s3C�s3C���C�Y�C�s3C�s3Cǌ�Č�Cр C֦fCی�C�s3C�@ C�ffC�s3C� C���D�fD�3D� D�3D�3D�fD�fD$�fD)��D.�3D3��D8� D=� DB�3DG��DL��DQ��DV�fD[� D`��De�fDj� Do��Dt��Dy��D�#3D�Y�D���D�� D��D�` D�� D��fD��D�Y�D���D��3D�3D�` Dڣ3Dߦf111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�RA�ZA�uA�wA�A�$�A� �A܇+A�`BA�bA�9XA��A�"�A��Aư!A�JA��9A�A� �A�ȴA��#A��FA��A��9A�p�A��7A� �A��7A�dZA�JA��A���A�`BA�
=A���A�l�A�VA��A�I�A�7LA�%A��A�S�A�l�A��A�%A�1A�33A�v�A��PA��-A}VAx1'Ar��Alr�Ae��Ab��A[x�AU��APbNAJĜADȴA>v�A:��A5�^A1��A-l�A+�-A#O�A��A\)A
��A ��@�Ĝ@�(�@υ@���@��@�bN@���@�{@��@��`@�-@�=q@�+@�^5@�ȴ@�9X@��\@��u@�5?@��@�~�@�@h �@U�T@D��@@A�@9%@1&�@';d@#dZ@��@t�@�/@��@=q@l�@+@
�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�RA�ZA�uA�wA�A�$�A� �A܇+A�`BA�bA�9XA��A�"�A��Aư!A�JA��9A�A� �A�ȴA��#A��FA��A��9A�p�A��7A� �A��7A�dZA�JA��A���A�`BA�
=A���A�l�A�VA��A�I�A�7LA�%A��A�S�A�l�A��A�%A�1A�33A�v�A��PA��-A}VAx1'Ar��Alr�Ae��Ab��A[x�AU��APbNAJĜADȴA>v�A:��A5�^A1��A-l�A+�-A#O�A��A\)A
��A ��@�Ĝ@�(�@υ@���@��@�bN@���@�{@��@��`@�-@�=q@�+@�^5@�ȴ@�9X@��\@��u@�5?@��@�~�@�@h �@U�T@D��@@A�@9%@1&�@';d@#dZ@��@t�@�/@��@=q@l�@+@
�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBp�B��B�RB��BɺB�dB	cTB	�PB	��B	��B	�NB	�B
 �B
VB
\)B
~�B
�B
�#B
�;B{B �B(�B+B1'B8RB[#By�B~�B�B�%B�1B�B� B{�Bw�BjB`BB]/BZBT�BH�B:^B.B%�BbBB
��B
�B
�ZB
��B
�?B
��B
� B
dZB
E�B
&�B
oB	�B	�B	�qB	��B	�+B	gmB	VB	D�B	5?B	&�B	�B	B��B�
B�FB��B��B�=B��B��BƨB�;B��B	B	uB	B	�B	 �B	#�B	/B	=qB	G�B	M�B	jB	�B	�7B	��B	��B	�dB	��B	�B	��B
B
oB
"�B
)�B
7LB
;dB
K�B
N�B
Q�B
XB
ZB
cT111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 Bp�B��B�RB��B��BÖB	gmB	�bB	��B	��B	�TB	�B
#�B
ZB
^5B
�B
�!B
�)B
�ZB�B!�B)�B-B33B;dB]/Bz�B� B�B�+B�7B�B�B}�Bx�Bl�BaHB^5B[#BVBI�B;dB.B&�BhBB
��B
�B
�`B
��B
�LB
��B
�B
ffB
G�B
'�B
{B	�B	�B	�wB	��B	�7B	hsB	W
B	E�B	6FB	'�B	�B	B��B�B�LB��B��B�DB��B��BƨB�;B��B	B	{B	B	�B	 �B	#�B	/B	=qB	G�B	M�B	jB	�B	�7B	��B	��B	�dB	��B	�B	��B
B
oB
"�B
)�B
7LB
;dB
K�B
N�B
Q�B
XB
ZB
cT111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.4(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810171300092008101713000920081017130009200908260158562009082601585620090826015856200908260250002009082602500020090826025000  JA  ARFMdecpA9_b                                                                20080909065315  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080909065317  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080909065318  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080909065318  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080909065322  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080909065322  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080909065322  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080909065322  QCF$                G�O�G�O�G�O�             300JA  ARGQaqcp2.8a                                                                20080909065322  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080909065322  QCF$                G�O�G�O�G�O�             340JA  ARGQrqcpt16b                                                                20080909065323  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080909070539                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080913040317  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080913040322  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080913040323  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080913040323  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080913040327  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080913040327  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080913040327  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080913040327  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080913040328  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080913051121                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080913040317  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416015817  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416015817  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416015817  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416015817  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416015818  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416015818  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416015818  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416015818  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416015818  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416020024                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081017130005  CV  DAT$            G�O�G�O�F�w�                JM  ARCAJMQC1.0                                                                 20081017130009  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081017130009  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090826015856  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090826025000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075311  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075417                      G�O�G�O�G�O�                