CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900614 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               [A   JA  20080830011757  20090831075420  A9_60138_091                    2C  D   APEX-SBE 2408                                                   846 @��lP}1   @��p韰@7�-V@_�Q��1   ARGOS   A   A   A   @�ffA��Ai��A���A�33A�ffB
ffBffB2��BFffBZ  Bn  B�ffB�ffB���B���B�  B�33B���B���B�  B���B䙚B�33B�33CffC��CffC� CffC�C33C$� C(�fC.�C3ffC833C=  CA��CGL�CQ  C[33CeL�CoffCyffC�� C�� C��3C��3C���C��fC�� C��3C��fC�� C�� C��3C��3C�CǦfC̀ C�s3C֙�Cی�C���C噚C�� C�� C��3C��3D� D� D� D�fD��D�3D� D$��D)��D.�fD3ٚD8�3D=� DB�3DG��DL�fDQ�fDV��D[� D`�fDeٚDjٚDo�3Dt��Dy�fD��D�ffD���D��3D�  D�ffD��fD��D�)�D�c3D��fD�ٚD��D�l�Dڬ�D��D�,�D�` D�3D�0 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A33Ah  A�  A�ffA陚B
  B  B2ffBF  BY��Bm��B�33B�33B���B���B���B�  B���Bƙ�B���Bڙ�B�ffB�  B�  CL�C� CL�CffCL�C  C�C$ffC(��C.  C3L�C8�C<�fCA�3CG33CP�fC[�Ce33CoL�CyL�C��3C��3C��fC��fC�� C���C��3C��fC���C��3C��3C��fC��fC CǙ�C�s3C�ffC֌�Cۀ C�� C��C�3C�3C��fC��fDٚD��D��D� D�3D��DٚD$�fD)�fD.� D3�3D8��D=ٚDB��DG�fDL� DQ� DV�fD[��D`� De�3Dj�3Do��Dt�fDy� D�fD�c3D���D�� D��D�c3D��3D��fD�&fD�` D��3D��fD�fD�i�Dک�D��fD�)�D�\�D� D�,�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A�|�A��A�PA�hA�wA�jA�33A�ƨA��yA��A�/Aǲ-A�
=Aħ�A�VA���A�oA��7A��hA�K�A�$�A�"�A���A��A�l�A��!A���A�  A� �A�-A��A�Q�A��A�1A��uA��A�~�A��HA�ȴA�C�A�K�A���A�ȴA�E�A�M�A���A�G�A���A�?}A~�yAs��An=qAi7LAeC�A_�#A\�yAY��AUdZAP�yAL1'AH��ABbA?�A;x�A7A09XA)�A$�A�A%AbA
ȴAV@�%@�@�+@��@�@ě�@��9@���@��;@�j@��@�J@��m@���@�I�@���@�9X@�7L@|�@y��@w|�@k��@m�h@[�@D�@8�u@2J@,�@'K�@ �@1@��@`B@�!@�@��@	�^@�@ƨ@ ��?�^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A�|�A��A�PA�hA�wA�jA�33A�ƨA��yA��A�/Aǲ-A�
=Aħ�A�VA���A�oA��7A��hA�K�A�$�A�"�A���A��A�l�A��!A���A�  A� �A�-A��A�Q�A��A�1A��uA��A�~�A��HA�ȴA�C�A�K�A���A�ȴA�E�A�M�A���A�G�A���A�?}A~�yAs��An=qAi7LAeC�A_�#A\�yAY��AUdZAP�yAL1'AH��ABbA?�A;x�A7A09XA)�A$�A�A%AbA
ȴAV@�%@�@�+@��@�@ě�@��9@���@��;@�j@��@�J@��m@���@�I�@���@�9X@�7L@|�@y��@w|�@k��@m�h@[�@D�@8�u@2J@,�@'K�@ �@1@��@`B@�!@�@��@	�^@�@ƨ@ ��?�^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�jB	�XB	��B	��B	��B	��B	��B	��B	�jB
�B
n�B
�PB
�bB
��B
��B
��B
��B
��BA�BW
BjB�hB��B~�B[#Bm�B`BBl�BiyBm�Bq�Bq�Bl�BffBffBdZBcTBcTBbNB]/BS�BJ�B:^B2-B�BoBB
�B
��B
�dB
��B
k�B
O�B
6FB
#�B
DB	��B	�sB	�B	��B	�B	��B	z�B	m�B	ZB	G�B	-B	{B	  B�B�#B��BÖB�'B��B�B�B�dB��B��B�B	  B	hB	�B	#�B	+B	7LB	8RB	N�B	e`B	t�B	�B	�DB	�oB	��B	�9B	��B	��B	��B
B
\B
�B
!�B
0!B
;dB
F�B
P�B
W
B
]/B
e`B
l�B
q�B
x�B
� B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�jB	�^B	��B	��B	��B	��B	��B	�B	ƨB
�B
p�B
�PB
�hB
��B
��B
��B
��B
��BB�BXBl�B�oB��B�B\)Bp�BcTBm�Bl�Bo�Br�Br�Bn�BgmBffBe`BdZBdZBcTB^5BT�BL�B;dB49B�BuBB
�B
��B
�jB
��B
l�B
P�B
7LB
$�B
JB	��B	�yB	�B	��B	�B	��B	{�B	n�B	[#B	I�B	/B	�B	B�B�)B��BĜB�-B��B�!B�B�jB��B��B�B	B	hB	�B	#�B	,B	7LB	9XB	N�B	e`B	t�B	�B	�DB	�oB	��B	�9B	��B	��B	��B
B
\B
�B
!�B
0!B
;dB
F�B
P�B
W
B
]/B
e`B
l�B
q�B
x�B
� B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809120246542008091202465420080912024654200809120257562008091202575620080912025756200908240000002009082400000020090824000000  JA  ARFMdecpA9_b                                                                20080830011740  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080830011757  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080830011759  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080830011801  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080830011809  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080830011809  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080830011809  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080830011809  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080830011811  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080830013441                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080903040311  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080903040316  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080903040316  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080903040317  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080903040321  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080903040321  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080903040321  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080903040321  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080903040321  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080903050705                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080903040311  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416015814  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416015815  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416015815  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416015815  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416015816  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416015816  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416015816  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416015816  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416015816  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416020027                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080902103737  CV  DAT$            G�O�G�O�F�c�                JM  ARCAJMQC1.0                                                                 20080912024654  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080912024654  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080912025756  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090824000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075308  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075420                      G�O�G�O�G�O�                