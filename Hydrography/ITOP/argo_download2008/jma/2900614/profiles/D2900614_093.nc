CDF   (   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   Q   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     D  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  T  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     D  44   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  T  5x   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     D  5�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     D  7   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  T  8T   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     D  8�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  T  9�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     D  :@   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     D  ;�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  T  <�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     D  =   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  T  >`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     D  >�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  ?�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   @�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   I�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   R�   CALIBRATION_DATE      	   
                
_FillValue                  �  [�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    \   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    \   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    \   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    \   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  \   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    \X   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    \h   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    \l   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         \|   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         \�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        \�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    \�Argo profile    2.2 1.2 19500101000000  2900614 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ]A   JA  20080919045350  20090831075416  A9_60138_093                    2C  D   APEX-SBE 2408                                                   846 @��i���{1   @��k*�61@7��vȴ9@_rn��O�1   ARGOS   A   B   B   @���AffAd��A���A�ffA���B
��BffB2ffBE��BY��BlffB�ffB�ffB�  B�ffB�  B���B�ffBƙ�B�ffB�  B�  B�ffB�33C33C� CL�C� C� C�C��C$�C)  C.�C3  C7�fC=�CBffCG33CQffCZ�fCe33Cn�fCyffC��3C���C���C��fC��3C��3C�� C�� C�� C���C�s3C���C�� C¦fC�� C̳3Cљ�C֌�C�s3C�ffC� CꙚC�� C���C�� DٚD�fD� DٚD� D�fD� D$� D)�fD.��D/f111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�ffA33Aa��A�33A���A�33B
  B��B1��BD��BX��Bk��B�  B�  B���B�  B���B�ffB�  B�33B�  Bڙ�B䙚B�  B���C  CL�C�CL�CL�C�fCffC#�fC(��C-�fC2��C7�3C<�fCB33CG  CQ33CZ�3Ce  Cn�3Cy33C���C��3C�� C���C���C���C�ffC�ffC�ffC�s3C�Y�C�s3C��fC�CǦfC̙�Cр C�s3C�Y�C�L�C�ffC� C�fC� C��fD��D��D�3D��D�3D��D�3D$�3D)��D.� D.��111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�hsA�`BA��/Aٕ�A���A՝�A���A���A�`BAξwA̶FA�A�v�AʼjA�C�A�ffA�S�A���A��A�`BA�^5A�VA�p�A�VA�ZA��jA��A���A��PA�^5A��`A��A�1A�1'A�bA�ƨA�G�A���A��A�;dA��hA��A�l�A���A��A�K�A��A�/A�t�A��wA��-A�n�A�v�A�Q�A�E�A}��Av�ArM�Al�HAgK�Ae�A_�A[hsAW�AS�7AM
=AI�TAAXA7A0E�A!�;A�wA��@�?}@��@�ƨ@�j@�C�@���@�l�@���111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�hsA�`BA��/Aٕ�A���A՝�A���A���A�`BAξwA̶FA�A�v�AʼjA�C�A�ffA�S�A���A��A�`BA�^5A�VA�p�A�VA�ZA��jA��A���A��PA�^5A��`A��A�1A�1'A�bA�ƨA�G�A���A��A�;dA��hA��A�l�A���A��A�K�A��A�/A�t�A��wA��-A�n�A�v�A�Q�A�E�A}��Av�ArM�Al�HAgK�Ae�A_�A[hsAW�AS�7AM
=AI�TAAXA7A0E�A!�;A�wA��@�?}@��@�ƨ@�j@�C�@���@�l�G�O�111111111111111111111111111111111111111111111111111111111111111111111111111111114   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oG�O�B	:^B	:^B	<jB	>wB	t�B	��B	�B	��B	�3B	�}B	�NB	�B	��B	��B
M�B
��B=qBI�BW
Bz�B�7B_;BaHBp�BZBS�BVBS�BQ�BR�BYBYB[#B[#BXBYBXBVBT�BS�BQ�BK�B>wB2-B/B&�B�B	7B
��B
�yB
�B
�B
��B
�}B
��B
��B
w�B
_;B
B�B
'�B
�B
B	�B	�)B	ƨB	��B	��B	w�B	O�B	.B��B�B�RB��B��B��B��B�B��B��B��111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	:^B	:^B	<jB	@�B	v�B	��B	�%B	��B	�9B	��B	�TB	�B	��B	��B
S�B
��B=qBJ�BYB}�B�JBcTBbNBr�B[#BT�BVBT�BR�BS�BYB[#B\)B\)B[#BYBXBW
BVBS�BR�BL�B?}B33B0!B'�B�B
=B
��B
�B
�B
�B
��B
��B
�B
��B
x�B
`BB
C�B
'�B
�B
B	�B	�/B	ȴB	�B	��B	z�B	Q�B	0!B��B�B�^B��B��B��B��B�B��B��G�O�111111111111111111111111111111111111111111111111111111111111111111111111111111114   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
G�O�PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200908240815412009082408154120090824081541200908240946182009082409461820090824094618200908240000002009082400000020090824000000  JA  ARFMdecpA9_b                                                                20080919045348  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080919045350  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080919045350  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080919045351  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080919045355  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080919045355  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080919045355  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080919045355  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080919045355  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080919052729                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923035101  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080923035106  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080923035106  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080923035107  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080923035110  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080923035110  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080923035111  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080923035111  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080923035111  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080923050707                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923035101  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416015819  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416015819  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416015819  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416015819  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416015821  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416015821  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416015821  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416015821  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416015821  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090416020023                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080922104010  CV  DAT$            G�O�G�O�F��W                JM  ARSQJMQC1.0                                                                 20080922104010  CF  TEMP            D/fD/fG�O�                JM  ARSQJMQC1.0                                                                 20080922104010  CF  PSAL            D/fD/fG�O�                JM  ARCAJMQC1.0                                                                 20090824081541  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090824081541  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090824094618  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090824000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075308  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075416                      G�O�G�O�G�O�                