CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900614 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               YA   JA  20080810030929  20090831075407  A9_60138_089                    2C  D   APEX-SBE 2408                                                   846 @��k܊�1   @��l4��@6�I�^5@_��\(��1   ARGOS   A   A   A   @�ffA33Al��A�33A�ffA陚BffB��B1��BE��BX��Bn��B�  B���B���B�ffB�  B�ffB�  Bƙ�B�33B���B�  B���B�  CffC� CffCffCL�C33CffC$� C)L�C-��C2�3C7��C=�CA�3CG�CP�fC[ffCeffCo��CyffC��3C���C��fC�� C���C�� C��3C�� C���C���C���C���C�� C�CǙ�C�� Cљ�C֌�C�� C�� C�3C�fC�� C��C��3D�3D� D�3D�3D� D� D��D$�fD)�3D.�3D3�fD8� D=�fDB��DG� DL��DQ�fDV�3D[ٚD`� DeٚDjٚDo� Dt� Dy�3D�  D�p D���D�ٚD��D�p D���D�� D�,�D�l�D��fD��3D�,�D�p Dڜ�D��D�)�D�c3D� D� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A  Ai��A���A���A�  B��B��B0��BD��BX  Bn  B���B�33B�33B�  B���B�  B���B�33B���B�ffB㙚B�ffB���C33CL�C33C33C�C  C33C$L�C)�C-��C2� C7��C<�fCA� CF�fCP�3C[33Ce33CoffCy33C���C�s3C���C��fC�s3C��fC���C��fC�s3C�s3C�� C�� C��fC Cǀ C̦fCр C�s3CۦfC�fC噚C��C�fC�s3C���D�fD�3D�fD�fD�3D�3D��D$��D)�fD.�fD3ٚD8�3D=��DB��DG�3DL��DQ��DV�fD[��D`�3De��Dj��Do�3Dt�3Dy�fD��D�i�D��3D��3D�3D�i�D��fD�ٚD�&fD�ffD�� D���D�&fD�i�DږfD��3D�#3D�\�D�D�	�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�;dA�7LA�/A� �A�/A��A�oA�-A�M�A�bA�
=A��HA��A�bA��uA�
=A�"�A�5?A��/A�1A� �A�C�A��DA��A�K�A�ƨA�\)A���A��/A��A���A�hsA�
=A��A��A��A���A�1A���A��A�  A���A�M�A�t�A���A~{Av��As�Ao/Aj�Ad�yA^E�AW��AS+AM\)AC�7A>��A:-A6E�A.=qA&M�A#�FA�
A�A��A=qAA�A`B@��@���@�G�@��u@��H@�`B@��@�(�@���@�(�@�ȴ@�o@�|�@�?}@��T@��T@��@���@�Z@���@~�y@zM�@so@k33@h �@f��@c33@WK�@L9X@A��@;S�@4�D@,z�@(b@!�^@dZ@+@C�@�@`B@
�@1'@�+@�F@ Q�?���?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�;dA�7LA�/A� �A�/A��A�oA�-A�M�A�bA�
=A��HA��A�bA��uA�
=A�"�A�5?A��/A�1A� �A�C�A��DA��A�K�A�ƨA�\)A���A��/A��A���A�hsA�
=A��A��A��A���A�1A���A��A�  A���A�M�A�t�A���A~{Av��As�Ao/Aj�Ad�yA^E�AW��AS+AM\)AC�7A>��A:-A6E�A.=qA&M�A#�FA�
A�A��A=qAA�A`B@��@���@�G�@��u@��H@�`B@��@�(�@���@�(�@�ȴ@�o@�|�@�?}@��T@��T@��@���@�Z@���@~�y@zM�@so@k33@h �@f��@c33@WK�@L9X@A��@;S�@4�D@,z�@(b@!�^@dZ@+@C�@�@`B@
�@1'@�+@�F@ Q�?���?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	��B	��B	�B	��B
B
	7B
DB
oB
B�B
��B
�5B
�TB�B�B33B=qB<jBJ�BW
B^5BaHB^5BdZB_;BdZBcTBbNB`BBYBS�BP�BO�BK�BL�BB�B49B/B(�B"�B�BVB
��B
�B
��B
�LB
��B
{�B
k�B
XB
@�B
%�B
%B	�yB	��B	�LB	�uB	}�B	jB	[#B	,B	uB	hB��B�fB��BB�`B�B��B��B��B��B�B��B��B�TB�B�B��B��B	uB	1'B	8RB	C�B	[#B	]/B	p�B	w�B	�B	�7B	��B	�B	�^B	�qB	ǮB	�B	�B	�B	��B

=B
�B
�B
/B
<jB
F�B
P�B
XB
_;B
dZB
k�B
n�B
t�B
{�B
�B
�71111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	��B	��B
  B
B

=B
PB
�B
K�B
��B
�BB
�yB�B!�B49B>wB>wBL�BXB_;BcTB`BBe`BbNBe`BdZBcTBbNB\)BS�BQ�BO�BL�BN�BD�B5?B0!B)�B#�B�BbB
��B
�B
��B
�RB
��B
|�B
l�B
YB
A�B
'�B
1B	�B	�B	�XB	�{B	~�B	k�B	]/B	.B	{B	oB��B�mB��BÖB�mB�!B��B��B��B��B�B��B��B�ZB��B�B��B��B	{B	1'B	8RB	C�B	[#B	]/B	p�B	w�B	�B	�7B	��B	�B	�^B	�qB	ǮB	�B	�B	�B	��B

=B
�B
�B
/B
<jB
F�B
P�B
XB
_;B
dZB
k�B
n�B
t�B
{�B
�B
�71111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808230153162008082301531620080823015316200808230205002008082302050020080823020500200908240000002009082400000020090824000000  JA  ARFMdecpA9_b                                                                20080810030918  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080810030929  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080810030931  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080810030932  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080810030939  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080810030939  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080810030940  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080810030940  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080810030940  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080810040917                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080814034704  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080814034709  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080814034710  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080814034710  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080814034714  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080814034714  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080814034714  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080814034714  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080814034715  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080814050513                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080814034704  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416015810  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416015810  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416015810  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416015810  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416015811  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416015811  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416015811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416015811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416015811  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416020035                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080823015316  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080823015316  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080823020500  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090824000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075317  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075407                      G�O�G�O�G�O�                