CDF   
   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   m   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4P   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6t   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :L   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <p   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @H   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  Bl   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   E    SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   N    SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   W    CALIBRATION_DATE      	   
                
_FillValue                  �  `    HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    `�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a    HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        a   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    a Argo profile    2.2 1.2 19500101000000  2900614 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ^A   JA  20080930085211  20090831075412  A9_60138_094                    2C  D   APEX-SBE 2408                                                   846 @���
1   @���@y\�@8Ix���@_u�-V1   ARGOS   A   A   A   @�ffAffAc33A�ffA�33A���B��B  B2  BE��B[33BnffB�33B�  B�33B�ffB�ffB�ffB�ffBǙ�B���Bڙ�B���B�33B���C��C�CffC� C� C�CffC$33C)�3C.�3C333C8L�C=ffCB33CG33CQ� C[L�CeL�Cn�fCy�C���C���C���C��3C���C��3C��fC���C���C��fC��3C�� C��3C�� Cǀ Č�Cь�C֌�Cی�C���C�ffCꙚC� C��C��fD��DٚD�3D��D�3D�fDٚD$� D)� D.��D3�fD8��D=��DB�3DG� DL��DQ�3DVٚD[ٚD`� De�3Dj� DoٚDt� DyٚD�&fD�i�D��fD��fD�#3D�s3D��fD�� D�  D�Y�D��3D���D�  D��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�33A��Aa��A���A�ffA�  B33B��B1��BE33BZ��Bn  B�  B���B�  B�33B�33B�33B�33B�ffBЙ�B�ffB䙚B�  B�ffC� C  CL�CffCffC  CL�C$�C)��C.��C3�C833C=L�CB�CG�CQffC[33Ce33Cn��Cy  C�� C���C�� C��fC���C��fC���C�� C���C���C��fC��3C��fC³3C�s3C̀ Cр Cր Cۀ C���C�Y�C��C�s3C� C���D�fD�3D��D�3D��D� D�3D$ٚD)��D.�3D3� D8�fD=�fDB��DGٚDL�fDQ��DV�3D[�3D`ٚDe��DjٚDo�3DtٚDy�3D�#3D�ffD��3D��3D�  D�p D��3D���D��D�VfD�� D��D��D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��
A��#A��#A��HA��TA��mA��yA��A��TA��mA���A�+A��A��A΍PA�Q�A�x�AǶFA��A�K�A���A��DA���A�G�A�A�l�A��A�VA��
A���A�|�A�l�A�z�A���A��RA���A�~�A��A�%A�7LA��PA��A�ĜA���A�{A�S�A��mA�bNA��wA�bA�t�A�VA}�^AyO�Av�+Aq�Ag�wAb��AZv�AY��AX��AP��AH��AE�A?��A:  A6bA/��A'O�A$jA   A�A��A
��Al�@�&�@��@�7@�V@�?}@�p�@��H@�dZ@��!@�/@���@��h@�o@�7L@��u@��@��@�I�@�w@xA�@r^5@Zn�@Nȴ@@  @9%@0Ĝ@,(�@(A�@�@p�@�!@ �@��@(�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A��
A��#A��#A��HA��TA��mA��yA��A��TA��mA���A�+A��A��A΍PA�Q�A�x�AǶFA��A�K�A���A��DA���A�G�A�A�l�A��A�VA��
A���A�|�A�l�A�z�A���A��RA���A�~�A��A�%A�7LA��PA��A�ĜA���A�{A�S�A��mA�bNA��wA�bA�t�A�VA}�^AyO�Av�+Aq�Ag�wAb��AZv�AY��AX��AP��AH��AE�A?��A:  A6bA/��A'O�A$jA   A�A��A
��Al�@�&�@��@�7@�V@�?}@�p�@��H@�dZ@��!@�/@���@��h@�o@�7L@��u@��@��@�I�@�w@yhs@r^5@Zn�@Nȴ@@  @9%@0Ĝ@,(�@(A�@�@p�@�!@ �@��@(�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��B��B��B��B��B��B��B��B��B��B��B�#B	B	K�B	�{B	�HB	�BB	�B
B
<jB9XBS�BVBo�Br�B{�B|�B�+B�%B�B�%B�oB�uB�bB�%B�B�DB�7B�hB�VBr�BaHBI�B?}B6FB)�B"�B�BB
�HB
ǮB
�RB
��B
�B
s�B
XB
+B
uB	�B	�sB	�HB	�qB	��B	�=B	r�B	ZB	I�B	.B	{B		7B��B�#B��B��B�?B�9B�FB�'B�?B�#B�B��B	�B	!�B	�B	=qB	K�B	S�B	YB	n�B	�\B	��B	��B	��B	��B	�qB	��B	�NB	��B
	7B
�B
 �B
+B
<jB
A�B
D�B
I�B
J�B
S�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B��B��B��B��B��B��B��B��B��B��B�B�/B	B	P�B	��B	�HB	�HB	��B
1B
A�B<jBT�BVBr�Bs�B|�B~�B�+B�+B�B�+B�uB�uB�hB�7B�B�JB�DB�uB�bBs�BbNBJ�B@�B7LB+B#�B�BB
�NB
ȴB
�^B
��B
�B
t�B
ZB
,B
�B	�B	�sB	�TB	�}B	��B	�DB	t�B	[#B	K�B	0!B	�B	
=B��B�#B��B��B�FB�?B�FB�3B�FB�)B�B��B	�B	"�B	�B	=qB	K�B	S�B	ZB	n�B	�\B	��B	��B	��B	��B	�qB	��B	�NB	��B
	7B
�B
 �B
+B
<jB
A�B
D�B
I�B
J�B
S�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810271300332008102713003320081027130033200811241117542008112411175420081124111754200908240000002009082400000020090824000000  JA  ARFMdecpA9_b                                                                20080930085208  IP                  G�O�G�O�G�O�                JM  ARFMDECPA9                                                                  20081027130031  IP                  G�O�G�O�G�O�                JM  ARFMBITP1                                                                   20080930085208  SV                  D�fD�i�G�O�                JM  ARGQRQCP1                                                                   20081027130031  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20081027130033  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081027130033  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081124111754  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090824000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075304  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075412                      G�O�G�O�G�O�                