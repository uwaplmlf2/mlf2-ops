CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901560 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20081007060202  20090514011012                                  2B  A   APEX-SBE 3468                                                   846 @���R��1   @����8�@.��n��@`J$�/�1   ARGOS   B   B   B   @�  A  A^ffA���A�  A�ffB
  B��B133BE��BY33Bo33B�  B�  B���B�ffB�  B�33B�33B�33B�ffB�33B���B�  B�33C��C33C�C��CL�CffC33C$� C)L�C.�3C3� C8� C=��CB�CGffCQffC[�CeffCoL�Cy33C���C��fC�� C��3C��fC���C���C��3C��3C���C��3C���C��3C�s3CǙ�Č�C�s3Cր C۳3C�3C噚C�� C��C��C���DٚD�fD�fD��DٚD�3DٚD$� D)ٚD.� D3��D8��D=� DBٚDG�fDLٚDQ�3E4�3E4x D��3E1�3E/��Do� Dt� Dy�fD�)�D�p D�� D���D�  D�c3D���D���D��D�l�D��3D�� D�#3D�c3Dڰ D��3D��D�c3D�3D�� D��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111144444111111111111111111111111@���AffA\��A���A�33A噙B	��B34B0��BE34BX��Bn��B���B���B���B�33B���B�  B�  B�  B�33B�  B䙚B���B�  C� C�C  C� C33CL�C�C$ffC)33C.��C3ffC8ffC=� CB  CGL�CQL�C[  CeL�Co33Cy�C�� C���C��3C��fC���C�� C�� C��fC��fC���C��fC���C��fC�ffCǌ�C̀ C�ffC�s3CۦfC�fC��C�3C� C� C���D�4D� D� D�gD�4D��D�4D$��D)�4D.��D3�gD8�4D=ٚDB�4DG� DL�4DQ��E4ٙE4vfD�� E1��E/� Do��DtٚDy� D�&gD�l�D���D�ٚD��D�` D���D��D�gD�i�D�� D���D�  D�` Dڬ�D�� D��D�` D� D���D��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111144444111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�x�A�~�A�x�A�r�A�\)A��mA�ĜA��A��A�~�A�  A���A�K�A��TA�1A�/A�G�A١�A�ƨAִ9A�G�A�S�A��;A�A�A�A��mA��AÙ�A��A��A��`A�A�A�1A�;dA��A���A�E�A���A���A���A�  A�+A��`A�dZA�JA��A�G�A��!A���A���A{�hAt�Al~�Ae%A[dZAS�PAM/AE�-AB �A<jA6ffA.��A(=qA"��A��AK�A�A��A	��A`B@�Ĝ@��@�l�@�dZ@�J@� �@ȼj@Ĭ@���@�C�@�ff@�`B@���@�Z@�S�@�bN@���BI�G�O�B	�B	�N@���@�Ĝ@�ȴ@���@{�F@n��@dZ@\�/@St�@I�^@@Ĝ@97L@2��@-/@&��@!hs@��@�7@O�@�^@p�@	��@��@"�@X11111111111111111111111111111111111111111111111111111111111111111111111111111111111111144144111111111111111111111111A�x�A�~�A�x�A�r�A�\)A��mA�ĜA��A��A�~�A�  A���A�K�A��TA�1A�/A�G�A١�A�ƨAִ9A�G�A�S�A��;A�A�A�A��mA��AÙ�A��A��A��`A�A�A�1A�;dA��A���A�E�A���A���A���A�  A�+A��`A�dZA�JA��A�G�A��!A���A���A{�hAt�Al~�Ae%A[dZAS�PAM/AE�-AB �A<jA6ffA.��A(=qA"��A��AK�A�A��A	��A`B@�Ĝ@��@�l�@�dZ@�J@� �@ȼj@Ĭ@���@�C�@�ff@�`B@���@�Z@�S�@�bN@���BI�G�O�B	�B	�N@���@�Ĝ@�ȴ@���@{�F@n��@dZ@\�/@St�@I�^@@Ĝ@97L@2��@-/@&��@!hs@��@�7@O�@�^@p�@	��@��@"�@X11111111111111111111111111111111111111111111111111111111111111111111111111111111111111144144111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B]/B]/B]/B_;BgmB�jBŢB�HB��BbBI�BȴB	�B	�NB	�yB
�VB
�HB�B!�B?}B2-B;dBx�B��B�;B�B%B#�B �BoB�TB��BȴBÖB�LB��B��B��B��B��B�{B�oBw�B`BB6FB�B%B
�ZB
��B
��B
w�B
T�B
33B
%B	��B	�'B	�\B	v�B	k�B	\)B	K�B	%�B	B��B�B�TB�`B�BB�5B�fB�B�B	B	uB	'�B	?}B	^5B	k�B	}�B	��B	��B	�^B	ƨB	�B	�BB	�`B	�??�?)7L?1�?>v�B
JB
bB
oB
�B
#�B
+B
1'B
6FB
<jB
C�B
J�B
N�B
S�B
ZB
_;B
cTB
ffB
jB
n�B
r�B
v�B
{�B
~�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111144444111111111111111111111111B]/B]/B]/B_;BgmB�jBŢB�HB��BbBI�BȴB	�B	�NB	�yB
�VB
�HB�B!�B?}B2-B;dBx�B��B�;B�B%B#�B �BoB�TB��BȴBÖB�LB��B��B��B��B��B�{B�oBw�B`BB6FB�B%B
�ZB
��B
��B
w�B
T�B
33B
%B	��B	�'B	�\B	v�B	k�B	\)B	K�B	%�B	B��B�B�TB�`B�BB�5B�fB�B�B	B	uB	'�B	?}B	^5B	k�B	}�B	��B	��B	�^B	ƨB	�B	�BB	�`B	�??�?)7L?1�?>v�B
JB
bB
oB
�B
#�B
+B
1'B
6FB
<jB
C�B
J�B
N�B
S�B
ZB
_;B
cTB
ffB
jB
n�B
r�B
v�B
{�B
~�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111144444111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081007060155  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081007060202  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081007060203  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081007060207  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081007060207  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081007060207  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20081007060207  QCF$                G�O�G�O�G�O�             140JA  ARGQrqcpt16b                                                                20081007060208  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081007064702                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081011042741  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081011042745  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081011042746  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081011042750  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081011042750  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081011042750  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20081011042750  QCF$                G�O�G�O�G�O�             140JA  ARGQrqcpt16b                                                                20081011042750  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081011061444                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081011042741  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090514010442  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090514010443  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090514010443  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090514010444  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090514010444  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090514010444  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090514010444  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090514010444  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090514010444  QCF$                G�O�G�O�G�O�             140JA  ARGQaqcp2.8b                                                                20090514010444  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090514010444  QCF$                G�O�G�O�G�O�             140JA  ARGQrqcpt16b                                                                20090514010444  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090514011012                      G�O�G�O�G�O�                