CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901560 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080907022611  20090514011010                                  2B  A   APEX-SBE 3468                                                   846 @��c���1   @��c���@/�z�H@`lbM��1   ARGOS   A   A   A   @���A  Ac33A���A�33A�33B��B  B1��BE��BZ  Bn��B���B�  B�  B�  B�33B���B�33B�ffB�ffB�33B�33B�33B���C33CL�C��CffC��C� CffC$�C)L�C.� C3ffC8� C=��CB��CGL�CQ  C[� Ce  CoffCy��C���C��3C�� C��3C��fC��fC�� C�� C��fC��fC���C��fC�� C�s3Cǌ�C�� Cь�C֌�Cی�C�� C� C�3C��C���C���D� D� D�3D�fD� DٚD��D$�fD)�fD.� D3��D8��D=��DB�3DG��DL��DQٚDVٚD[� D`�fDe��Dj� Do� Dt�3Dy��D��D�c3D���D�� D�&fD�ffD��3D��3D�fD�p D��fD���D�,�D�l�Dڙ�D�ٚD�)�D�ffD�D�� D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A  Ac33A���A�33A�33B��B  B1��BE��BZ  Bn��B���B�  B�  B�  B�33B���B�33B�ffB�ffB�33B�33B�33B���C33CL�C��CffC��C� CffC$�C)L�C.� C3ffC8� C=��CB��CGL�CQ  C[� Ce  CoffCy��C���C��3C�� C��3C��fC��fC�� C�� C��fC��fC���C��fC�� C�s3Cǌ�C�� Cь�C֌�Cی�C�� C� C�3C��C���C���D� D� D�3D�fD� DٚD��D$�fD)�fD.� D3��D8��D=��DB�3DG��DL��DQٚDVٚD[� D`�fDe��Dj� Do� Dt�3Dy��D��D�c3D���D�� D�&fD�ffD��3D��3D�fD�p D��fD���D�,�D�l�Dڙ�D�ٚD�)�D�ffD�D�� D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�VA��A�t�A�-A�A�uA�!A�jA��A�  A�PA�^A��#A�jA�Q�A��#Aۣ�A���AٸRA� �A�9XAљ�A�|�AΧ�A˸RA�Aħ�Að!A���A���A��A��A�%A�33A�p�A� �A�;dA�A�A���A��A�oA��;A�O�A�?}A��hA�O�A�x�A�Av�+Aql�Akx�Ad��A_VAZ�DAVffAG�AD1'A?
=A7��A5+A1\)A*�A%�A"-A��A�;A&�AVAVA
M�A��@���@�@�
=@��@�;d@Ͳ-@�@ċD@�o@���@�p�@��`@�p�@��@��@��y@���@�j@�J@�A�@�&�@��H@�r�@�M�@�E�@��u@s�m@jJ@^��@Tz�@M��@E/@>5?@7|�@2�@,�j@&$�@�y@=q@C�@�h@;d@�m@ ��?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�VA��A�t�A�-A�A�uA�!A�jA��A�  A�PA�^A��#A�jA�Q�A��#Aۣ�A���AٸRA� �A�9XAљ�A�|�AΧ�A˸RA�Aħ�Að!A���A���A��A��A�%A�33A�p�A� �A�;dA�A�A���A��A�oA��;A�O�A�?}A��hA�O�A�x�A�Av�+Aql�Akx�Ad��A_VAZ�DAVffAG�AD1'A?
=A7��A5+A1\)A*�A%�A"-A��A�;A&�AVAVA
M�A��@���@�@�
=@��@�;d@Ͳ-@�@ċD@�o@���@�p�@��`@�p�@��@��@��y@���@�j@�J@�A�@�&�@��H@�r�@�M�@�E�@��u@s�m@jJ@^��@Tz�@M��@E/@>5?@7|�@2�@,�j@&$�@�y@=q@C�@�h@;d@�m@ ��?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B�ZB�B�B��B	�B	#�B	,B	<jB	^5B	��B
JB
9XB
n�B
t�B
��B
��B
�B
�TB�B<jBp�B��B��B��B�yB�B�ZB�BB��B9XB49B&�B��B�B��B�/B��BĜB�1BdZBZBS�BI�B�B
�5B
��B
�B
YB
;dB
�B	��B	�NB	��B	�3B	r�B	cTB	L�B	9XB	2-B	1'B	5?B	(�B	!�B	�B	%B��B��B�B��B�B�mB�B	B	{B	#�B	;dB	E�B	hsB	n�B	|�B	�\B	��B	�-B	ŢB	��B	�)B	�TB	�B	�B	��B	��B
  B
%B

=B
bB
�B
�B
&�B
.B
5?B
9XB
@�B
D�B
I�B
M�B
Q�B
XB
_;B
dZB
k�B
p�B
w�B
|�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B��B�ZB�B�B��B	�B	#�B	,B	<jB	^5B	��B
JB
9XB
n�B
t�B
��B
��B
�B
�TB�B<jBp�B��B��B��B�yB�B�ZB�BB��B9XB49B&�B��B�B��B�/B��BĜB�1BdZBZBS�BI�B�B
�5B
��B
�B
YB
;dB
�B	��B	�NB	��B	�3B	r�B	cTB	L�B	9XB	2-B	1'B	5?B	(�B	!�B	�B	%B��B��B�B��B�B�mB�B	B	{B	#�B	;dB	E�B	hsB	n�B	|�B	�\B	��B	�-B	ŢB	��B	�)B	�TB	�B	�B	��B	��B
  B
%B

=B
bB
�B
�B
&�B
.B
5?B
9XB
@�B
D�B
I�B
M�B
Q�B
XB
_;B
dZB
k�B
p�B
w�B
|�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080907022550  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080907022611  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080907022615  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080907022625  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080907022625  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080907022626  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080907030008                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080911044943  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080911044954  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080911044956  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080911045003  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080911045003  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080911045004  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080911051847                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080911044943  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090514010435  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090514010435  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090514010436  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090514010437  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090514010437  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090514010437  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090514010437  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090514010437  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090514011010                      G�O�G�O�G�O�                