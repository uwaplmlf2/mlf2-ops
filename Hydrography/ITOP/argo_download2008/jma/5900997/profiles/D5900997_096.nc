CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900997 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               `A   JA  20080901011421  20090916045612  A9_60126_096                    2C  D   APEX-SBE 2360                                                   846 @��֬/	1   @���g�7Z@9�1&�x�@ahbM��1   ARGOS   A   A   A   @���A33Ad��A�ffAř�A���B	��BffB2ffBE��BX��Bl��B���B���B���B���B���B�  B�  Bƙ�B�  B�  B�ffB���B�33C33C33CffCL�C� C33C�C$�C)�C.33C3L�C8ffC=��CBffCGL�CQ� C[� CeffCoffCy�C�� C�s3C�� C���C�s3C���C���C���C���C�s3C���C���C��fC�� Cǳ3C̙�Cь�C֦fCۦfC�s3C� C�ffC�fC��fC�� D��D�fD�3D� DٚD�3D�3D$ٚD)�3D.��D3ٚD8��D=ٚDB� DG�3DL�3DQ�fDVٚD[��D`� De��Dj� Do� Dt�3Dy�3D�#3D�VfD��3D���D�fD�Y�D��3D��3D��D�ffD��3D��fD��D�c3Dڬ�D�� D�)�D�` D�D�L�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33Ad��A�ffAř�A���B	��BffB2ffBE��BX��Bl��B���B���B���B���B���B�  B�  Bƙ�B�  B�  B�ffB���B�33C33C33CffCL�C� C33C�C$�C)�C.33C3L�C8ffC=��CBffCGL�CQ� C[� CeffCoffCy�C�� C�s3C�� C���C�s3C���C���C���C���C�s3C���C���C��fC�� Cǳ3C̙�Cь�C֦fCۦfC�s3C� C�ffC�fC��fC�� D��D�fD�3D� DٚD�3D�3D$ٚD)�3D.��D3ٚD8��D=ٚDB� DG�3DL�3DQ�fDVٚD[��D`� De��Dj� Do� Dt�3Dy�3D�#3D�VfD��3D���D�fD�Y�D��3D��3D��D�ffD��3D��fD��D�c3Dڬ�D�� D�)�D�` D�D�L�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�!A�-A�^A�ƨA�+A�hAպ^A���A͏\A�E�A��#Aĩ�A��9A�\)A�ƨA���A�`BA��jA���A��uA�1'A�ffA��jA���A���A��A�+A���A�|�A�hsA�I�A��A��A�-A�E�A��+A�oA�x�A�+A��yA�;dA��+A�jA��A�O�A���A�r�A��-A���A��RA�{A��\A��A�  A}Av^5Arn�Aot�Akt�AhĜAc�
A`�yAZ�AT^5AQ��AK?}AIS�AD��A=ƨA:�\A0bA(^5A ^5A�A�DA
^5AC�@��@�F@�@�&�@�%@�E�@�?}@�A�@�~�@�o@�%@��D@��9@��`@��@��!@�-@{�@m�T@_��@V��@Kt�@A�@9�7@3o@+dZ@&E�@!��@��@��@�h@bN@S�@	%@�-@��@&�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�!A�-A�^A�ƨA�+A�hAպ^A���A͏\A�E�A��#Aĩ�A��9A�\)A�ƨA���A�`BA��jA���A��uA�1'A�ffA��jA���A���A��A�+A���A�|�A�hsA�I�A��A��A�-A�E�A��+A�oA�x�A�+A��yA�;dA��+A�jA��A�O�A���A�r�A��-A���A��RA�{A��\A��A�  A}Av^5Arn�Aot�Akt�AhĜAc�
A`�yAZ�AT^5AQ��AK?}AIS�AD��A=ƨA:�\A0bA(^5A ^5A�A�DA
^5AC�@��@�F@�@�&�@�%@�E�@�?}@�A�@�~�@�o@�%@��D@��9@��`@��@��!@�-@{�@m�T@_��@V��@Kt�@A�@9�7@3o@+dZ@&E�@!��@��@��@�h@bN@S�@	%@�-@��@&�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	��B	��B	��B	��B
��B
�#B�B�1B�oB�uB��B�VB��B��B��B��B��B��B��B��B�uB�PB�1B|�Bv�Bs�Bs�Br�Bq�BhsBcTB`BB[#BQ�BL�BI�BF�BC�B?}B>wB7LB.B(�B �B�B\B%B  B
��B
�B
�TB
��B
��B
�B
��B
~�B
k�B
]/B
I�B
<jB
#�B
�B	��B	�BB	��B	�^B	�B	��B	q�B	ffB	@�B	�B��B�/B�}B�B��B�{B�+B�B� B}�B�B�VB��B�B�qB��B�HB��B	B	VB	�B	33B	D�B	dZB	�B	��B	�^B	��B	�BB	�B
+B
�B
!�B
/B
7LB
A�B
L�B
ZB
_;B
gmB
l�B
r�B
w�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	��B	��B	�B
��B
�mB�B�=B�uB��B��B��B��B��B��B��B��B��B��B��B��B�VB�JB~�Bw�Bt�Bs�Bs�Br�BjBdZBaHB\)BR�BM�BI�BG�BD�B?}B?}B8RB/B)�B �B�BbB%B  B
��B
�B
�ZB
��B
B
�B
��B
� B
l�B
^5B
J�B
=qB
$�B
�B	��B	�HB	��B	�dB	�B	��B	r�B	gmB	A�B	 �B��B�5B��B�B��B��B�1B�B�B~�B�B�\B��B�B�qB��B�HB��B	B	VB	�B	33B	D�B	dZB	�B	��B	�^B	��B	�BB	�B
+B
�B
!�B
/B
7LB
A�B
L�B
ZB
_;B
gmB
l�B
r�B
w�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<D��<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809131959332008091319593320080913195933200809132020572008091320205720080913202057200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20080901011406  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080901011421  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080901011424  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080901011426  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080901011433  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080901011433  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080901011433  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080901011433  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080901011435  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080901015614                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080904161747  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080904161751  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080904161752  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080904161752  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080904161756  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080904161756  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080904161756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080904161756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080904161757  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080904191229                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080904161747  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519065540  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519065541  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519065541  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519065541  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519065543  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519065543  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519065543  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519065543  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519065543  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071001                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080904050006  CV  DAT$            G�O�G�O�F�f�                JM  ARCAJMQC1.0                                                                 20080913195933  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080913195933  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080913202057  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045533  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045612                      G�O�G�O�G�O�                