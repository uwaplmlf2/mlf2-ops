CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900997 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               dA   JA  20081011010754  20090916045608  A9_60126_100                    2C  D   APEX-SBE 2360                                                   846 @��݌"�|1   @���%�}�@9mO�;dZ@amhr� �1   ARGOS   A   A   A   @�ffA33Aa��A���Ař�A�ffB	��B  B2ffBE��B[33Bn  B�33B�  B�33B���B�33B�ffB���B�  B�  Bڙ�B䙚BB���CffCL�CL�CffC33CL�C  C$ffC)33C-�fC2��C8L�C=  CBffCGffCQ��C[L�Ce33CoL�Cy33C�s3C���C���C�s3C�Y�C��fC���C���C��3C�� C��3C���C��3C�CǙ�Č�Cѳ3Cֳ3C۳3C�fC�3C�� C�� C���C���D��D��D�fD�3DٚD�3D��D$�3D)�3D.�fD3ٚD8�fD=��DBٚDG� DL� DQ�3DVٚD[�fD`��DeٚDj��Do�3DtٚDy� D��D�i�D�� D���D��D�i�D���D�ٚD�33D�p D�� D�ٚD��D�ffDڰ D�� D�&fD�` D�fD��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA33Aa��A���Ař�A�ffB	��B  B2ffBE��B[33Bn  B�33B�  B�33B���B�33B�ffB���B�  B�  Bڙ�B䙚BB���CffCL�CL�CffC33CL�C  C$ffC)33C-�fC2��C8L�C=  CBffCGffCQ��C[L�Ce33CoL�Cy33C�s3C���C���C�s3C�Y�C��fC���C���C��3C�� C��3C���C��3C�CǙ�Č�Cѳ3Cֳ3C۳3C�fC�3C�� C�� C���C���D��D��D�fD�3DٚD�3D��D$�3D)�3D.�fD3ٚD8�fD=��DBٚDG� DL� DQ�3DVٚD[�fD`��DeٚDj��Do�3DtٚDy� D��D�i�D�� D���D��D�i�D���D�ٚD�33D�p D�� D�ٚD��D�ffDڰ D�� D�&fD�` D�fD��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A��A��mA���A�l�A�A�JAպ^A�hsA�M�A�  A���A���A�VA��A���A�-A���A���A�VA��A���A��DA�A�A��+A�7LA��jA�n�A��+A�A�ĜA�S�A�(�A�A�=qA��A���A�A��DA�ZA��A���A��A�VA� �A��A�$�A��HA�A�"�A��jA�+A�bNA�hsA�XA{"�AvĜAr��Aq&�AnȴAhĜAbZA]33AT��AN �AI��AG�^AD�uA>�A9ƨA-l�A I�AbA��A9XA�@�9X@ߍP@�$�@�|�@�%@�V@�G�@�@��m@�M�@���@���@���@�t�@�9X@�  @�`B@|��@uO�@l��@a��@S�m@F�y@?|�@6v�@/
=@)%@#S�@�@~�@�@��@�@	�^@��@j@ �u?��H?�X1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A��A��mA���A�l�A�A�JAպ^A�hsA�M�A�  A���A���A�VA��A���A�-A���A���A�VA��A���A��DA�A�A��+A�7LA��jA�n�A��+A�A�ĜA�S�A�(�A�A�=qA��A���A�A��DA�ZA��A���A��A�VA� �A��A�$�A��HA�A�"�A��jA�+A�bNA�hsA�XA{"�AvĜAr��Aq&�AnȴAhĜAbZA]33AT��AN �AI��AG�^AD�uA>�A9ƨA-l�A I�AbA��A9XA�@�9X@ߍP@�$�@�|�@�%@�V@�G�@�@��m@�M�@���@���@���@�t�@�9X@�  @�`B@|��@uO�@l��@a��@S�m@F�y@?|�@6v�@/
=@)%@#S�@�@~�@�@��@�@	�^@��@j@ �u?��H?�X1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	ɺB	�ZB	�B	��B	�fB	�B	�;B
L�B
I�B
ZB
��B
ÖBDBBhB7LB-BaHBdZBp�Bl�Bk�BffB^5BYBT�BH�BH�BN�BJ�BD�BB�BB�B=qB:^B6FB6FB2-B0!B.B.B'�B%�B#�B�BuBPB
��B
��B
�B
�ZB
�B
ȴB
�wB
�B
��B
�B
o�B
gmB
ZB
=qB
!�B
1B	�TB	ɺB	�-B	�3B	��B	�7B	v�B	B�B	B�B��B�B��B�JB|�B�B�B�JB�=B�7B��B�B�jB��B�B�B	  B	PB	!�B	+B	6FB	H�B	^5B	z�B	��B	�qB	��B	�ZB	��B
B
{B
�B
+B
9XB
B�B
L�B
VB
_;B
ffB
p�B
w�B
w�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	ɺB	�`B	�B	��B	�yB	�!B	�ZB
P�B
VB
_;B
��B
ƨBJB+BoB9XB1'BcTBgmBq�Bl�Bl�BhsB`BBYBW
BI�BI�BO�BK�BD�BB�BB�B>wB:^B7LB7LB2-B0!B.B.B'�B%�B$�B�BuBVB  B
��B
�B
�`B
�B
ȴB
�}B
�B
��B
�B
o�B
hsB
\)B
?}B
"�B

=B	�`B	��B	�3B	�9B	��B	�=B	x�B	D�B	%B�B��B�B��B�PB}�B�B�B�PB�DB�7B��B�B�jB��B�B�B	  B	PB	!�B	+B	6FB	H�B	^5B	z�B	��B	�qB	��B	�ZB	��B
B
{B
�B
+B
9XB
B�B
L�B
VB
_;B
ffB
p�B
w�B
w�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<D��<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810232003402008102320034020081023200340200810232016122008102320161220081023201612200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20081011010738  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081011010754  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081011010757  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081011010759  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081011010807  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081011010807  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081011010807  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081011010807  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081011010809  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081011014405                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081014155414  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081014155418  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081014155419  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081014155419  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081014155423  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081014155423  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081014155423  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081014155423  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081014155424  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081014191234                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081014155414  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519065551  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519065552  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519065552  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519065553  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519065554  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519065554  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519065554  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519065554  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519065554  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519070957                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081014052947  CV  DAT$            G�O�G�O�F��                 JM  ARCAJMQC1.0                                                                 20081023200340  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081023200340  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081023201612  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045529  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045608                      G�O�G�O�G�O�                