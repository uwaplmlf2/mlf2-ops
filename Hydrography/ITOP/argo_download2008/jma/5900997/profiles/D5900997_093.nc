CDF   
   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900997 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ]A   JA  20080803012607  20090916045602  A9_60126_093                    2C  D   APEX-SBE 2360                                                   846 @��]eC!1   @��^�2�@9��-@ab~��"�1   ARGOS   A   A   A   @���A  Ak33A���A���A�33B
  B33B1��BFffBZ��Bm33B�33B�ffB�33B�33B�33B���B�ffB�33BЙ�B�ffB�ffB�  B�  C� CL�CL�C��C33C�C33C$ffC)ffC.�C3  C7�fC=ffCB��CG��CQL�C[33CeffCo33Cy� C��3C���C���C���C�s3C���C���C��3C�� C��fC���C�� C���C Cǳ3C̙�C�s3C֦fCۦfC���C�� C�� CC���C��fD� D�fD�fD�3D� D��D�fD$�fD)� D.��D3��D8�3D=��DB�3DG� DLٚDQ� DV� D[� D`�3De��DjٚDo��Dt� Dy� D�  D�ffD��3D��fD��D�l�D���D��fD�fD�ffD��3D��fD�)�D�s3Dڰ D���D�)�D�S3D��D�)�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A  Ak33A���A���A�33B
  B33B1��BFffBZ��Bm33B�33B�ffB�33B�33B�33B���B�ffB�33BЙ�B�ffB�ffB�  B�  C� CL�CL�C��C33C�C33C$ffC)ffC.�C3  C7�fC=ffCB��CG��CQL�C[33CeffCo33Cy� C��3C���C���C���C�s3C���C���C��3C�� C��fC���C�� C���C Cǳ3C̙�C�s3C֦fCۦfC���C�� C�� CC���C��fD� D�fD�fD�3D� D��D�fD$�fD)� D.��D3��D8�3D=��DB�3DG� DLٚDQ� DV� D[� D`�3De��DjٚDo��Dt� Dy� D�  D�ffD��3D��fD��D�l�D���D��fD�fD�ffD��3D��fD�)�D�s3Dڰ D���D�)�D�S3D��D�)�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�=qA��yA���A�
=A�~�A�`BA��#A�x�A��A�(�A�1A�(�A���A�A� �A�G�A��wA���A��uA���A�K�A�&�A���A��wA��A�A���A��uA�oA�(�A��;A��/A��A���A�
=A�+A���A�C�A��
A���A�+A��A��A���A�7LA���A���A�VA�?}A��A�M�A�v�A�TA{
=Ax1'Asx�ApAk|�Ai�AbȴA]�;AX�uATr�AO�7AJ�AF  A?�hA;C�A6M�A2��A+�A$(�A��AJA5?@��9@�X@��@�t�@�/@��@��7@�;d@���@��@�hs@�5?@��@�Ĝ@�@��@��@��7@|�@t(�@e?}@Z�@M@B�!@:n�@2�@.�R@+S�@%�-@ ��@��@ƨ@  @�h@
M�@
=@�@�7?��R?��m1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�=qA��yA���A�
=A�~�A�`BA��#A�x�A��A�(�A�1A�(�A���A�A� �A�G�A��wA���A��uA���A�K�A�&�A���A��wA��A�A���A��uA�oA�(�A��;A��/A��A���A�
=A�+A���A�C�A��
A���A�+A��A��A���A�7LA���A���A�VA�?}A��A�M�A�v�A�TA{
=Ax1'Asx�ApAk|�Ai�AbȴA]�;AX�uATr�AO�7AJ�AF  A?�hA;C�A6M�A2��A+�A$(�A��AJA5?@��9@�X@��@�t�@�/@��@��7@�;d@���@��@�hs@�5?@��@�Ĝ@�@��@��@��7@|�@t(�@e?}@Z�@M@B�!@:n�@2�@.�R@+S�@%�-@ ��@��@ƨ@  @�h@
M�@
=@�@�7?��R?��m1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�%B
�VB
��B
��B
��B
��B
�qB
��B
��B
��B
�B
�BBJB �Bm�Bk�B�B~�Bn�B`BBZBt�B�JB�7B�B~�B}�Bz�Bx�Bq�BffBbNBe`BcTB_;B[#BQ�BN�BE�BD�B=qB6FB0!B$�B�BuBB
��B
�sB
�#B
��B
�qB
�B
��B
�\B
t�B
aHB
L�B
@�B
!�B
PB	��B	�NB	��B	�LB	��B	�7B	v�B	bNB	R�B	49B	�B��BǮB�B��B�{B�\B�B�B�1B�7B��B��B��B�!BÖB�
B�mB��B	oB	!�B	6FB	C�B	T�B	s�B	�VB	�B	ǮB	�/B	��B
B
PB
�B
&�B
6FB
D�B
N�B
T�B
]/B
e`B
l�B
o�B
u�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�+B
�\B
��B
�B
��B
��B
B
ǮB
ĜB
��B
�)B
�NBhB'�Bp�Bo�B�%B�Bp�BbNBZBu�B�PB�DB�B� B}�B{�Bz�Br�BgmBcTBffBdZB`BB\)BQ�BO�BF�BE�B>wB6FB1'B%�B�B{B%B
��B
�yB
�)B
��B
�wB
�B
��B
�bB
u�B
bNB
M�B
B�B
"�B
VB	��B	�TB	��B	�RB	��B	�=B	w�B	cTB	S�B	5?B	�B��BȴB�B��B��B�bB�%B�B�1B�=B��B��B��B�'BĜB�
B�mB��B	oB	!�B	6FB	C�B	T�B	s�B	�VB	�B	ǮB	�/B	��B
B
PB
�B
&�B
6FB
D�B
N�B
T�B
]/B
e`B
l�B
o�B
u�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909071306022009090713060220090907130602200909071842542009090718425420090907184254200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20080803012547  IP                  G�O�G�O�G�O�                JM  ARFMDECPA9                                                                  20090907130558  IP                  G�O�G�O�G�O�                JM  ARFMBITP1                                                                   20080803012547  SV                  A���B1��G�O�                JM  ARGQRQCP1                                                                   20090907130558  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20090907130602  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090907130602  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090907184254  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045520  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045602                      G�O�G�O�G�O�                