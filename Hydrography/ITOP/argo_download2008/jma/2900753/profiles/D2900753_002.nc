CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900753 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080918003405  20100219001253  A9_76277_002                    2C  D   APEX-SBE 3522                                                   846 @�ﮞW:�1   @��Tb��@6ؓt�j@a��1'1   ARGOS   A   A   A   @���AffAd��A�33A���A홚B
ffB33B1��BF  BZ  Bm��B���B�33B�  B�ffB�33B�33B���B���B�  B���B�33B���B�33CL�C� CL�C��C� C  C33C$  C)ffC.L�C3ffC8ffC=L�CB33CGffCQ� C[L�Cd�fCn��Cx�fC��fC��3C��fC���C���C�� C��fC�Y�C���C�� C���C��3C��3C³3Cǌ�Č�Cр C�� C�s3C���C�s3C�3C�fC�� C�� D� D� D� D��D� D�fD�3D$�3D)� D.�3D3��D8�3D=��DB�3DG�3DLٚDQ�fDV� D[��D`�fDe�fDjٚDo�3Dt� Dy��D�&fD�i�D��fD��fD��D�i�D���D��D�&fD�` D���D��fD�&fD�p Dڣ3D���D��D�VfD�3D��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffAd��A�33A���A홚B
ffB33B1��BF  BZ  Bm��B���B�33B�  B�ffB�33B�33B���B���B�  B���B�33B���B�33CL�C� CL�C��C� C  C33C$  C)ffC.L�C3ffC8ffC=L�CB33CGffCQ� C[L�Cd�fCn��Cx�fC��fC��3C��fC���C���C�� C��fC�Y�C���C�� C���C��3C��3C³3Cǌ�Č�Cр C�� C�s3C���C�s3C�3C�fC�� C�� D� D� D� D��D� D�fD�3D$�3D)� D.�3D3��D8�3D=��DB�3DG�3DLٚDQ�fDV� D[��D`�fDe�fDjٚDo�3Dt� Dy��D�&fD�i�D��fD��fD��D�i�D���D��D�&fD�` D���D��fD�&fD�p Dڣ3D���D��D�VfD�3D��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A� �A�A�A�ƨA�A�ĜA��TA�=qA�33A�
=AэPA��HA�A�A�=qA��A�E�A�=qA�VA��PA���A��A�33A�l�A�S�A��A�ZA��A�I�A��DA���A�dZA���A���A�A���A�M�A��A��9A���A��hA�?}A���A��A�E�A� �A��;A�ƨA��A�  A��
A}&�A{t�At�\ArQ�Am��Aj1AhbNAb�A]K�AV�`AU33AQ/AM�AGVA>(�A:��A4ȴA.-A'�^A!�AAbA
E�A�@�@��@�33@���@�hs@�@��+@�1'@�z�@��@��H@���@���@��@�5?@��@�O�@
=@{"�@r�@nv�@lI�@c@U`B@I�^@A�7@:M�@1��@+��@&v�@!x�@�^@�@^5@�@Ĝ@��@G�?��m?�l�?��?�71111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A� �A�A�A�ƨA�A�ĜA��TA�=qA�33A�
=AэPA��HA�A�A�=qA��A�E�A�=qA�VA��PA���A��A�33A�l�A�S�A��A�ZA��A�I�A��DA���A�dZA���A���A�A���A�M�A��A��9A���A��hA�?}A���A��A�E�A� �A��;A�ƨA��A�  A��
A}&�A{t�At�\ArQ�Am��Aj1AhbNAb�A]K�AV�`AU33AQ/AM�AGVA>(�A:��A4ȴA.-A'�^A!�AAbA
E�A�@�@��@�33@���@�hs@�@��+@�1'@�z�@��@��H@���@���@��@�5?@��@�O�@
=@{"�@r�@nv�@lI�@c@U`B@I�^@A�7@:M�@1��@+��@&v�@!x�@�^@�@^5@�@Ĝ@��@G�?��m?�l�?��?�71111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
B
�9B
��B
��B)�B.B-B7LB?}BVBW
BXB`BBbNBbNBr�B~�B�B�B~�B~�B|�Bv�Bp�BjBgmBe`B^5B[#BZBT�BO�BI�BE�BC�B>wB9XB6FB33B-B$�B�B\BB
��B
�sB
�/B
ÖB
�dB
�!B
��B
�VB
l�B
^5B
H�B
7LB
,B
oB	�B	��B	��B	�LB	��B	�DB	hsB	XB	?}B	&�B	\B��B�BBĜB�'B��B�{BjBe`B�1B�PB��B��B��B�^B�qB�)B�B��B��B	�B	5?B	D�B	W
B	^5B	m�B	u�B	�B	�{B	�B	ƨB	�B	�TB	��B	��B
PB
�B
'�B
6FB
=qB
J�B
S�B
]/B
bNB
iyB
n�B
s�B
u�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�9B
��B
��B)�B/B2-B=qBG�BW
B]/B[#BdZBgmBe`Bu�B�B�B�B�B�B}�Bx�Bs�Bk�BgmBffB`BB\)B[#BVBQ�BJ�BE�BD�B?}B9XB7LB49B.B%�B�BbB%B
��B
�yB
�5B
ĜB
�jB
�'B
��B
�bB
m�B
_;B
I�B
7LB
-B
uB	��B	��B	��B	�RB	��B	�PB	iyB	ZB	A�B	(�B	hB��B�HBŢB�-B��B��Bk�Be`B�1B�VB��B��B��B�dB�qB�/B�B��B	  B	�B	5?B	D�B	W
B	^5B	m�B	u�B	�B	�{B	�B	ƨB	�B	�TB	��B	��B
PB
�B
'�B
6FB
=qB
J�B
S�B
]/B
bNB
iyB
n�B
s�B
u�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809250237282008092502372820080925023728200809250251252008092502512520080925025125201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20080918003214  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080918003405  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080918003406  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080918003409  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080918003410  QCP$                G�O�G�O�G�O�            FB40JA  ARUP                                                                        20080918013607                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080916044330  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090520091321  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090520091321  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090520091322  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090520091323  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090520091323  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090520091323  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090520091323  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090520091323  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090520091442                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080915111934  CV  DAT$            G�O�G�O�F�}�                JM  ARCAJMQC1.0                                                                 20080925023728  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080925023728  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080925025125  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219001220  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219001253                      G�O�G�O�G�O�                