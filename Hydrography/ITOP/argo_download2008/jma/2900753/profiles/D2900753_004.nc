CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  2900753 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20081002012631  20100219001256  A9_76277_004                    2C  D   APEX-SBE 3522                                                   846 @����&�1   @�����7�@6��"��`@a�7KƧ�1   ARGOS   A   A   A   @�  A��Ad��A���A�  A���B	33BffB133BD��BZ  Bn  B���B�ffB���B���B�ffB���B���B�  B�33Bڙ�B�  B�  B�33C  C� C
��C�C33C� C��C$ffC)� C.L�C3L�C833C<�fCBffCG�CQ� C[�CeffCo��Cy� C��3C���C���C�� C�� C�� C�s3C�� C���C�� C�� C��fC�ffC�s3C�ffC̳3C���Cֳ3CۦfC�fC�fC� C�s3C�s3C�ffD��D��D� D� D�3D��D� D$ٚD)�fD.� D3�fD8� D=��DBٚDG� DLٚDQ� DV��D[��D`�fDe� Dj� Do�3DtٚDy�3D��D�ffD��fD��3D��D�c3D�� D���D�,�D�ffD��3D���D�0 D�` Dڬ�D�� D�&fD�i�D�D��D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���AffAa��A�33A�ffA�33BffB��B0ffBD  BY33Bm33B�ffB�  B�33B�ffB�  B�ffB�33Bƙ�B���B�33B䙚BB���C ��CL�C
��C�fC  CL�CffC$33C)L�C.�C3�C8  C<�3CB33CF�fCQL�CZ�fCe33CoffCyL�C���C�� C�s3C��fC��fC�ffC�Y�C�ffC�s3C�ffC�ffC���C�L�C�Y�C�L�C̙�Cѳ3C֙�Cی�C���C��C�ffC�Y�C�Y�C�L�D��D� D�3D�3D�fD��D�3D$��D)��D.�3D3��D8�3D=� DB��DG�3DL��DQ�3DV� D[��D`��De�3Dj�3Do�fDt��Dy�fD�fD�` D�� D���D�fD�\�D���D��fD�&fD�` D���D��fD�)�D�Y�DڦfD��D�  D�c3D�3D��3D��f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�C�A�1A�r�A�n�A�dZA�XA�O�A���A蝲A��A�I�A�"�A�v�A��;A�
=A�hsA�I�A�ƨA��A�ƨA�7LA��jA�-A��\A�A�jA�VA�E�A��FA�bNA�"�A�XA�&�A�A�  A��A���A��hA�n�A�VA���A��TA���A���A�1A���A���A��hA�-A~��Ax��AsC�Aq�7Ao&�Am�wAk�TAf�yA_�A]ƨAXbAT(�AM��AF�ABQ�A:�A5��A2ZA.1A+A( �A!��A�A��AdZ@�1@��;@��@�ȴ@���@�@�Q�@��w@�~�@��!@��@��@�n�@�$�@��/@���@�J@|��@rJ@l�@e�T@_�@T��@I�^@@�9@;t�@6v�@2=q@,��@ r�@��@ �@S�@K�@S�@�@ ��?�O�?�7L?�?}??�v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�C�A�1A�r�A�n�A�dZA�XA�O�A���A蝲A��A�I�A�"�A�v�A��;A�
=A�hsA�I�A�ƨA��A�ƨA�7LA��jA�-A��\A�A�jA�VA�E�A��FA�bNA�"�A�XA�&�A�A�  A��A���A��hA�n�A�VA���A��TA���A���A�1A���A���A��hA�-A~��Ax��AsC�Aq�7Ao&�Am�wAk�TAf�yA_�A]ƨAXbAT(�AM��AF�ABQ�A:�A5��A2ZA.1A+A( �A!��A�A��AdZ@�1@��;@��@�ȴ@���@�@�Q�@��w@�~�@��!@��@��@�n�@�$�@��/@���@�J@|��@rJ@l�@e�T@_�@T��@I�^@@�9@;t�@6v�@2=q@,��@ r�@��@ �@S�@K�@S�@�@ ��?�O�?�7L?�?}??�v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
B
�JB
�DB
�hB
�{B
��B
��B
��B
�-B
��B>wBZB��B��B�/B�B�sB��B��B��B�B�B�B�`B��B��B��B��B��B��B�JB~�Bt�BZBS�BO�BG�B@�B:^B:^B8RB0!B+B"�BJB
�B
��B
��B
�?B
��B
��B
� B
ffB
^5B
R�B
K�B
?}B
'�B
+B	��B	�/B	ǮB	��B	�1B	u�B	W
B	D�B	7LB	)�B	�B	bB��B�fB��B��B�hB�B�%B�B�7B�%B�VB��B�!BB�#B�fB	DB	!�B	'�B	6FB	=qB	S�B	ffB	q�B	w�B	��B	�-B	��B	�sB	��B	��B
B
bB
�B
-B
7LB
9XB
E�B
M�B
]/B
ffB
jB
n�B
p�B
z�B
z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�JB
�JB
�hB
�{B
��B
��B
��B
�9B
�#B@�BaHB��B��B�;B�)B�B1B��B��B�B�B�B�yB��B��BĜB��B��B��B�VB� Bw�B[#BVBP�BI�BA�B:^B:^B9XB0!B,B#�BVB
�B
��B
��B
�FB
��B
��B
�B
ffB
_;B
R�B
K�B
@�B
)�B
1B	��B	�5B	ɺB	�B	�7B	w�B	XB	E�B	8RB	+B	�B	hB��B�sBÖB��B�oB�%B�+B�B�=B�+B�\B��B�!BÖB�#B�mB	DB	!�B	'�B	6FB	=qB	S�B	ffB	q�B	w�B	��B	�-B	��B	�sB	��B	��B
B
bB
�B
-B
7LB
9XB
E�B
M�B
]/B
ffB
jB
n�B
p�B
z�B
z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810150201222008101502012220081015020122200810150226252008101502262520081015022625201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20081002012629  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081002012631  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081002012632  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081002012636  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081002012636  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081002012637  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081002023133                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081006044736  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081006044739  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081006044740  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081006044744  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081006044744  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081006044744  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081006063222                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081006044736  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090520091326  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090520091326  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090520091326  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090520091327  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090520091327  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090520091327  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090520091327  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090520091327  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090520091440                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081005113319  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20081015020122  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081015020122  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081015022625  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219001226  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219001256                      G�O�G�O�G�O�                