CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   r   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4d   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =T   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  AX   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C    PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E\   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   E�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   N�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   W�   CALIBRATION_DATE      	   
                
_FillValue                  �  `�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    al   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    ap   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    at   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    ax   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a|   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        a�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    a�Argo profile    2.2 1.2 19500101000000  2900753 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080918003358  20100219001252  A9_76277_001                    2C  D   APEX-SBE 3522                                                   846 @��.C �1   @��3�&�@7��O�;@a��t�j1   ARGOS   A   A   A   @���A��Ai��A�33Ař�A홚B
  B��B133BE33BY��BlffB���B���B���B�ffB���B�  B�ffB�ffB���B���B�33B�  B�  C �fCL�C
�3C  C�3C��C� C#��C)L�C-��C2�fC833C=�CB�CG  CQ��C[L�Ce� CoL�Cy� C��3C��3C�� C���C�� C��3C�� C�s3C���C��3C�� C���C�� C�s3C�s3C̳3C�Y�C�s3Cی�C�ffC��C�ffC��C���C��fD�fD��D��D� D�3DٚD�fD$� D)� D.��D3�3D8�fD=��DB�fDG��DL�3DQ�3DV��D[��D`� DeٚDj�3Do��Dt��Dy�3D�)�D�p D���D���D��D�ffD���D��D�)�D�` D���D�� D�#3D�ffDک�D�ٚD�)�D�VfD�3111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���A  Ah  A�ffA���A���B	��B33B0��BD��BY33Bl  B���B���B���B�33B�ffB���B�33B�33BЙ�Bڙ�B�  B���B���C ��C33C
��C�fC��C� CffC#� C)33C-�3C2��C8�C=  CB  CF�fCQ� C[33CeffCo33CyffC��fC��fC��3C�� C�s3C��fC�s3C�ffC���C��fC��3C���C��3C�ffC�ffC̦fC�L�C�ffCۀ C�Y�C� C�Y�C� C�� C���D� D�fD�fDٚD��D�3D� D$ٚD)��D.�3D3��D8� D=�fDB� DG�3DL��DQ��DV�3D[�fD`��De�3Dj��Do�fDt�fDy��D�&fD�l�D���D�ٚD�fD�c3D���D��fD�&fD�\�D��fD���D�  D�c3DڦfD��fD�&fD�S3D� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A��TA�^A�"�A�hsA���A�ĜA�C�A���A��;AĶFA���A�/A��PA��A��FA�x�A�XA�-A�l�A�|�A�A��\A�ffA���A�1A�ƨA���A�C�A�ƨA�K�A��7A��^A���A��DA�hsA�33A�|�A�bA�|�A���A�M�A�M�A�A���A��HA��A�ZA~�Ax�!At1'Aot�Ak��Ahr�AfM�A^��AT�AM�#AM�AJv�AG�;AF~�AC�AA?}A9��A.��A.-A'�A&��A!�;A�A��A=qA7L@�;d@�j@ݑh@�9X@˾w@���@�@�@��@��#@�@��w@�^5@�S�@��@���@�bN@z�!@q��@q�@c��@X  @PQ�@D��@>ff@3ƨ@1�7@&�+@"�@z�@�^@��@�;@
=@
�@�u@��@E�@��@ A�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A��A��TA�^A�"�A�hsA���A�ĜA�C�A���A��;AĶFA���A�/A��PA��A��FA�x�A�XA�-A�l�A�|�A�A��\A�ffA���A�1A�ƨA���A�C�A�ƨA�K�A��7A��^A���A��DA�hsA�33A�|�A�bA�|�A���A�M�A�M�A�A���A��HA��A�ZA~�Ax�!At1'Aot�Ak��Ahr�AfM�A^��AT�AM�#AM�AJv�AG�;AF~�AC�AA?}A9��A.��A.-A'�A&��A!�;A�A��A=qA7L@�;d@�j@ݑh@�9X@˾w@���@�@�@��@��#@�@��w@�^5@�S�@��@���@�bN@z�!@q��@q�@c��@X  @PQ�@D��@>ff@3ƨ@1�7@&�+@"�@z�@�^@��@�;@
=@
�@�u@��@E�@��@ A�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
B+B+B)�B#�B�BDB$�B1'B;dBE�B_;Bw�BjBn�Bl�Bm�Bp�Bo�B}�Bv�B^5BW
BS�BO�BL�BG�B@�B>wB<jB;dB9XB7LB5?B5?B5?B49B33B1'B0!B-B(�B �BoBB
�mB
�B
ƨB
��B
��B
� B
iyB
P�B
>wB
.B
!�B	��B	��B	�-B	�B	��B	�uB	�VB	�B	r�B	Q�B	.B	&�B	\B	JB��B��BǮB�LB�-B��B�PB�PB�DB�{B��B��BƨB��B�;B�fB��B	1B	,B	33B	6FB	A�B	N�B	e`B	jB	� B	��B	��B	��B	��B	�B	��B

=B
uB
"�B
)�B
5?B
A�B
E�B
P�B
T�B
XB
\)B
dZB
k�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B+B+B)�B$�B"�BhB(�B49B?}BL�BdZBy�Bl�Bo�Bn�Bp�Bt�Bt�B�B{�B_;BXBT�BP�BM�BI�BA�B?}B<jB;dB:^B8RB5?B5?B5?B49B49B1'B1'B-B)�B!�BuBB
�sB
�
B
ǮB
��B
��B
�B
jB
Q�B
?}B
/B
#�B	��B	��B	�-B	�B	��B	�uB	�\B	�B	t�B	T�B	.B	(�B	\B	PB��B��BȴB�LB�9B��B�\B�PB�JB��B��B��BƨB��B�;B�mB��B		7B	,B	33B	6FB	A�B	N�B	e`B	k�B	� B	��B	��B	��B	��B	�B	��B

=B
uB
"�B
)�B
5?B
A�B
E�B
P�B
T�B
XB
\)B
dZB
k�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809150219362008091502193620080915021936200809150240432008091502404320080915024043201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20080918003213  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080918003358  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080918003358  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080918003402  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080918003403  QCP$                G�O�G�O�G�O�            FB40JA  ARUP                                                                        20080918013541                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080906051355  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090520091319  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090520091319  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090520091319  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090520091320  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090520091320  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090520091321  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090520091321  QCP$                G�O�G�O�G�O�            FB40JA  ARUP                                                                        20090520091441                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080905111840  CV  DAT$            G�O�G�O�F�iv                JM  ARCAJMQC1.0                                                                 20080915021936  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080915021936  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080915024043  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219001222  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219001252                      G�O�G�O�G�O�                