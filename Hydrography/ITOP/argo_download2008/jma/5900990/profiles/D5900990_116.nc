CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900990 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               tA   JA  20080819004959  20090916045440  A5_06497_116                    2C  D   APEX-SBE 223                                                    846 @��
�1   @�駺g�@2�O�;dZ@b���"��1   ARGOS   A   A   A   @���AffAc33A���A���A陚B	��BffB0��BF  BY33Bl��B���B�33B�  B���B�  B���B�33B�  B�33B�33B�  B���B�  C33CL�C��CffC�CffC� C$�3C)��C.L�C3��C8ffC=L�CBL�CG�CQL�C[  CeffCo��Cy33C��fC�� C��fC���C��3C��fC��3C�� C�� C���C���C��3C��3C¦fCǳ3Č�C�s3C֦fC۳3C�3C�s3CꙚC�fC��C��3D� DٚD� D� D�fD�fDٚD$� D)� D.�3D3ٚD8ٚD=��DB��DG��DL� DQٚDV� D[� D`��De�fDj� Do��Dt��Dy� D�,�D�l�D���D��fD�)�D�` D���D���D�)�D�i�D��fD�� D�0 D�i�Dڬ�D���D�)�D�ffD�3D�c31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffAS33A���A���AᙚB��BffB,��BB  BU33Bh��B}��B�33B�  B���B�  B���B�33B�  B�33B�33B�  B���B�  C 33CL�C
��CffC�CffC� C#�3C(��C-L�C2��C7ffC<L�CAL�CF�CPL�CZ  CdffCn��Cx33C�&fC�  C�&fC��C�33C�&fC�33C�@ C�@ C��C��C�33C�33C�&fC�33C��C��3C�&fC�33C�33C��3C��C�&fC��C�33D� D��D� D� D�fD�fD��D$� D)� D.�3D3��D8��D=��DBy�DGy�DL� DQ��DV� D[� D`��De�fDj� Do��Dty�Dy� D��D�L�D�|�D��fD�	�D�@ D���D���D�	�D�I�D��fD�� D� D�I�Dڌ�D���D�	�D�FfD�3D�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�dZA훦A���A���A��`A���A�A���A��yA���A�p�A�-A���A���Aۗ�AړuA��;A��A�33Aϟ�A��TA�
=A�5?A�A��Aã�A��A�%A��+A��uA��A�VA��9A���A��RA��A��A���A�VA�ƨA�S�A��A�x�A�A�dZA���A�&�A�%A��A��/A��A�v�A{�Awt�Au+ApI�Ah�+A^��AW�AN��ALA�AD�+A?p�A9?}A2��A.��A(�A"��At�AK�A�A	�T@���@�I�@��`@�
=@���@���@�&�@�/@��`@�Q�@��@�`B@�  @�-@�1@��F@�|�@�S�@�-@��@���@� �@��;@|�/@p �@e�T@W�;@NE�@Kƨ@?
=@7��@1�@.5?@(�9@"M�@;d@�
@�w@�@
=@1@|�@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�dZA훦A���A���A��`A���A�A���A��yA���A�p�A�-A���A���Aۗ�AړuA��;A��A�33Aϟ�A��TA�
=A�5?A�A��Aã�A��A�%A��+A��uA��A�VA��9A���A��RA��A��A���A�VA�ƨA�S�A��A�x�A�A�dZA���A�&�A�%A��A��/A��A�v�A{�Awt�Au+ApI�Ah�+A^��AW�AN��ALA�AD�+A?p�A9?}A2��A.��A(�A"��At�AK�A�A	�T@���@�I�@��`@�
=@���@���@�&�@�/@��`@�Q�@��@�`B@�  @�-@�1@��F@�|�@�S�@�-@��@���@� �@��;@|�/@p �@e�T@W�;@NE�@Kƨ@?
=@7��@1�@.5?@(�9@"M�@;d@�
@�w@�@
=@1@|�@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�B	��B	�BB	�NB	�yB	�B	�B	��B	��B	�ZB
	7B
-B
k�B
�wB
�BB
��B33BQ�B�B� B�\B��B��B�3B�LB�FB�dB�RB�dB�qB�}BɺB��B?}B?}B5?B�B�BB�)B�FB��By�B^5BL�B@�B6FB&�BPB
��B
�#B
�9B
��B
�B
q�B
P�B
$�B	�B	��B	��B	�7B	q�B	hsB	R�B	:^B	49B	�B�yB�)B��B�LB�B��B��B��B�BB��B��B�BB		7B	#�B	@�B	P�B	YB	r�B	�%B	��B	��B	�LB	�jB	ŢB	��B	��B	�BB	�B
  B
bB
�B
-B
6FB
D�B
L�B
Q�B
VB
[#B
aHB
e`B
hsB
m�B
r�B
w�B
{�B
� B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�B	��B	�BB	�NB	�yB	�B	�B	��B	��B	�yB
DB
0!B
n�B
�}B
�HB
��B5?BS�B�B�B�hB��B�B�9B�LB�LB�jB�XB�jB�wB��B��B�B@�B@�B9XB�B�B1B�;B�LB��B{�B_;BM�BA�B7LB(�BVB
��B
�/B
�FB
��B
�B
r�B
R�B
&�B	��B	��B	��B	�DB	r�B	jB	VB	<jB	6FB	�B�B�;B��B�RB�B��B��B��B�BĜBÖB��B�HB	
=B	$�B	A�B	Q�B	ZB	s�B	�+B	��B	��B	�RB	�qB	ƨB	��B	�B	�HB	�B
  B
hB
�B
-B
7LB
E�B
M�B
R�B
W
B
[#B
aHB
ffB
iyB
n�B
s�B
w�B
|�B
� B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=1.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809010227132008090102271320080901022713200809010240542008090102405420080901024054200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20080819004952  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080819004959  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080819004959  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080819005000  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080819005005  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080819005005  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080819005005  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080819005005  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080819005006  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080819013641                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080823033737  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080823033742  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080823033742  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080823033743  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080823033747  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080823033747  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080823033747  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080823033747  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080823033747  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080823050233                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080823033737  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515063830  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515063830  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515063830  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515063831  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515063832  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515063832  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515063832  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515063832  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515063832  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515064107                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080822114333  CV  DAT$            G�O�G�O�F�M=                JM  ARCAJMQC1.0                                                                 20080901022713  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080901022713  CV  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080901024054  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045323  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045440                      G�O�G�O�G�O�                