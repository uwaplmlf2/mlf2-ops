CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900990 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               wA   JA  20080918004934  20090916045427  A5_06497_119                    2C  D   APEX-SBE 223                                                    846 @��',�5�1   @��'Q�@2���$�@b�I�^5?1   ARGOS   A   A   A   @�33A33Ac33A���A�33A���B
��B  B333BFffBZ��Bm��B�  B�  B���B�  B�  B�ffB�33B�33BЙ�B�ffB�  B�  B�ffCL�C� C��C� C�C��C� C$� C)� C.L�C333C833C=� CB� CGL�CQ33C[� CeffCo�CyL�C��fC���C�� C���C���C���C�� C�� C��fC��fC���C���C��3C¦fCǀ C̳3C�� C֦fCی�C�� C�� CꙚC� C�� C��fD�fD��D�fD�fD�3DٚD��D$��D)��D.� D3�fD8�3D=ٚDBٚDG�3DL�3DQ� DV�3D[�3D`��De�fDjٚDo�3Dt� Dy�fD�&fD�p D��fD�� D�&fD�\�D���D���D�&fD�ffD��fD��3D�&fD�i�Dڬ�D���D�  D�l�D�3D�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A33AS33A���A�33A���B��B  B/33BBffBV��Bi��B~  B�  B���B�  B�  B�ffB�33B�33BΙ�B�ffB�  B�  B�ffC L�C� C
��C� C�C��C� C#� C(� C-L�C233C733C<� CA� CFL�CP33CZ� CdffCn�CxL�C�&fC��C�  C��C��C��C�@ C�@ C�&fC�&fC��C��C�33C�&fC�  C�33C�@ C�&fC��C�@ C�@ C��C�  C�@ C�&fD�fDl�D�fD�fD�3D��D��D$��D)��D.� D3�fD8�3D=��DB��DG�3DL�3DQ� DV�3D[�3D`��De�fDj��Do�3Dt� Dy�fD�fD�P D��fD�� D�fD�<�D�|�D���D�fD�FfD��fD��3D�fD�I�Dڌ�D��D�  D�L�D�3D�#31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�jA�n�A�$�A�VA�A�A�ȴA�`BA�t�A�ȴA�ffA��yAѣ�A�AʓuA�ƨAò-A�p�A��mA��/A��yA�A�A�r�A�dZA��A��A�K�A�jA��\A��A��hA��\A��A��hA�?}A�ȴA��mA�K�A���A��A��#A�&�A�G�A��A�I�A���A�G�A��A�JAl�Ax�HAt-An�yAi`BAd�9A`~�A[AV�`AT�`AM�wAI�AC�7A>��A6jA4A,A%�A�`A�DA�\A��A
�RA�T@��@��@��@۾w@�M�@�X@��@���@���@�bN@�?}@��@�9X@��@���@�t�@�V@���@��7@���@�E�@���@���@x�u@o�@f�y@\�D@R-@Ko@D1@<1@4��@.v�@)7L@$�j@�;@&�@�j@x�@9X@	�@Z@ bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�jA�n�A�$�A�VA�A�A�ȴA�`BA�t�A�ȴA�ffA��yAѣ�A�AʓuA�ƨAò-A�p�A��mA��/A��yA�A�A�r�A�dZA��A��A�K�A�jA��\A��A��hA��\A��A��hA�?}A�ȴA��mA�K�A���A��A��#A�&�A�G�A��A�I�A���A�G�A��A�JAl�Ax�HAt-An�yAi`BAd�9A`~�A[AV�`AT�`AM�wAI�AC�7A>��A6jA4A,A%�A�`A�DA�\A��A
�RA�T@��@��@��@۾w@�M�@�X@��@���@���@�bN@�?}@��@�9X@��@���@�t�@�V@���@��7@���@�E�@���@���@x�u@o�@f�y@\�D@R-@Ko@D1@<1@4��@.v�@)7L@$�j@�;@&�@�j@x�@9X@	�@Z@ bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	PB	PB	-B	-B	s�B	y�B	�ZB
_;B
��B
�BBM�Bt�B��B�{B��B��B�B�3B�LB�^B�RB�9B�3B�!B�B��B��B��B��B�1B� B}�By�Br�Bo�BcTB\)BR�BL�BL�BB�B5?B!�B�BB
��B
�BB
��B
�B
�PB
q�B
W
B
=qB
'�B
bB	��B	�ZB	�B	�LB	��B	�+B	jB	J�B	7LB	bB��B�;B��BŢB�wB�'B��B�B�-BǮB��B�ZB�sB�B	�B	&�B	=qB	YB	l�B	r�B	z�B	�DB	��B	�9B	�qB	ɺB	��B	�
B	�TB	�B
%B
oB
�B
)�B
5?B
<jB
C�B
J�B
Q�B
W
B
]/B
aHB
e`B
l�B
r�B
v�B
{�B
� B
�B
�D1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	PB	PB	-B	-B	s�B	{�B	�fB
aHB
��B
�yBP�Bx�B��B��B��B�B�'B�LB�XB�dB�XB�LB�?B�-B�B��B��B��B��B�=B�B}�Bz�Bt�Bq�BdZB^5BS�BM�BM�BC�B7LB"�B�BB
��B
�HB
��B
�B
�VB
r�B
XB
>wB
(�B
hB	��B	�`B	�)B	�RB	��B	�1B	l�B	K�B	:^B	uB��B�BB��BƨB��B�-B�B�B�9BɺB��B�fB�B�B	�B	(�B	?}B	ZB	m�B	s�B	{�B	�JB	��B	�?B	�wB	ɺB	��B	�B	�ZB	�B
+B
oB
�B
+B
5?B
=qB
D�B
J�B
R�B
XB
^5B
bNB
ffB
m�B
s�B
w�B
|�B
� B
�%B
�D1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=1.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810010230022008100102300220081001023002200810010246462008100102464620081001024646200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20080918004929  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080918004934  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080918004934  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080918004934  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080918004938  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080918004938  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080918004939  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080918004939  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080918004939  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080918013753                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080922033902  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080922033907  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080922033907  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080922033908  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080922033912  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080922033912  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080922033912  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080922033912  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080922033913  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080922051216                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080922033902  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515063837  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515063837  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515063838  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515063838  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515063839  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515063839  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515063839  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515063839  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515063839  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515064116                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081001023002  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081001023002  CV  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081001024646  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045301  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045427                      G�O�G�O�G�O�                