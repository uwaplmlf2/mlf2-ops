CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900990 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               zA   JA  20081018004938  20090916045429  A5_06497_122                    2C  D   APEX-SBE 223                                                    846 @����!�1   @����h��@2"M���@b�\(�1   ARGOS   A   A   A   @�ffA33Ac33A���A�  A陚B
  B��B2  BFffBZffBm��B�  B�33B�  B�ffB�ffB�  B�ffB���B�  Bڙ�B�33B�  B�33C33C�CffC��C��C��CL�C$ffC)�C.��C3��C8L�C=� CBL�CGL�CQffC[�Ce�3CoL�Cy� C��3C��3C��fC��3C���C���C��fC���C�� C��3C���C��fC��3C³3Cǌ�C̦fCѦfCր CۦfC�fC�3C�3C���C��fC��3D��D��DٚD�fD�fD� D�3D$��D)�3D.��D3� D8� D=� DB�fDG� DL��DQ� DV�fD[��D`�fDe��Dj��DoٚDt��DyٚD�&fD�ffD��fD���D�)�D�i�D�� D��D�0 D�` D��fD���D��D�ffDڜ�D��fD�#3D�ffD��D�L�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA33AS33A���A�  AᙚB  B��B.  BBffBVffBi��B~  B�33B�  B�ffB�ffB�  B�ffB���B�  Bؙ�B�33B�  B�33C 33C�C
ffC��C��C��CL�C#ffC(�C-��C2��C7L�C<� CAL�CFL�CPffCZ�Cd�3CnL�Cx� C�33C�33C�&fC�33C��C��C�&fC��C�  C�33C�L�C�&fC�33C�33C��C�&fC�&fC�  C�&fC�&fC�33C�33C�L�C�&fC�33D��D��D��D�fD�fD� D�3D$��D)�3D.��D3� D8� D=� DB�fDG� DLy�DQ� DV�fD[��D`�fDe��Dj��Do��Dty�Dy��D�fD�FfD��fD���D�	�D�I�D�� D�ɚD� D�@ D��fD���D���D�FfD�|�D��fD�3D�FfD�|�D�,�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�!A�-A�RA���A�^A��;A��A��A�A�"�A�oA�VA�VA敁A�I�A��yA�jA��;A���A�dZA���Aҡ�A�?}A�"�A�v�A��A���AŴ9A�33A�{A��A�v�A��wA��
A�/A��^A��DA��mA�9XA�~�A��A�33A�XA�I�A���A���A���A��9A���A���A���A}��Awx�ApĜAj(�Ab�/A\n�AT�AN~�AE7LAD�9A@^5A:-A4I�A,ZA#��A VAr�A�A{A/@�  @�I�@���@׾w@Гu@ɉ7@���@�Q�@��R@�bN@��@�I�@�G�@���@��+@�33@��@��H@��@�=q@���@�Q�@|�@xQ�@g+@]�@S�m@L��@Ep�@=��@:��@2�!@,(�@%�@ 1'@dZ@
=@��@�@(�@	&�@�-@��@ �`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�!A�-A�RA���A�^A��;A��A��A�A�"�A�oA�VA�VA敁A�I�A��yA�jA��;A���A�dZA���Aҡ�A�?}A�"�A�v�A��A���AŴ9A�33A�{A��A�v�A��wA��
A�/A��^A��DA��mA�9XA�~�A��A�33A�XA�I�A���A���A���A��9A���A���A���A}��Awx�ApĜAj(�Ab�/A\n�AT�AN~�AE7LAD�9A@^5A:-A4I�A,ZA#��A VAr�A�A{A/@�  @�I�@���@׾w@Гu@ɉ7@���@�Q�@��R@�bN@��@�I�@�G�@���@��+@�33@��@��H@��@�=q@���@�Q�@|�@xQ�@g+@]�@S�m@L��@Ep�@=��@:��@2�!@,(�@%�@ 1'@dZ@
=@��@�@(�@	&�@�-@��@ �`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�B
�B
�B
�%B
�B
�hB
��B
�uB
��B
�^B
�XB
�^B
�9BJ�Bl�B�B�B��B��B��B�9B�XB�
B�B�BoB+B1'B9XB:^BK�BI�B@�B7LB'�BVB�B�/B��B��B��Bv�B]/BG�B.B�BDB
��B
�B
��B
�B
�uB
hsB
L�B
%�B
DB	�B	��B	�B	�B	}�B	ffB	K�B	,B	VB�B�HB��B�^B�9B��B�B�qBǮB�B�B	  B		7B	�B	"�B	$�B	8RB	L�B	_;B	u�B	�DB	��B	�?B	ĜB	��B	�B	�TB	�B	��B
+B
�B
&�B
33B
:^B
@�B
G�B
K�B
S�B
ZB
`BB
e`B
iyB
n�B
p�B
u�B
z�B
~�B
�B
�+B
�=1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�B
�B
�B
�%B
�B
�hB
��B
�uB
��B
�^B
�XB
�dB
�RBO�Bn�B�B�1B��B��B��B�9B�dB�B�B�B�B,B2-B:^B=qBO�BK�BE�B9XB)�BhB��B�;B��BB��Bx�B_;BI�B/B�BJB
��B
�B
��B
�B
��B
jB
N�B
'�B
PB	�B	��B	�'B	�B	� B	hsB	M�B	/B	bB�B�ZB��B�dB�FB�B�B�}BɺB�B�B	B	DB	�B	#�B	%�B	9XB	M�B	`BB	u�B	�JB	��B	�?B	ŢB	��B	�B	�ZB	�B	��B
1B
�B
&�B
49B
;dB
A�B
G�B
L�B
T�B
ZB
aHB
ffB
jB
n�B
q�B
v�B
z�B
~�B
�B
�+B
�D1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=1.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810310244342008103102443420081031024434200810310248342008103102483420081031024834200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20081018004931  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081018004938  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081018004938  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081018004939  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081018004943  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081018004943  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081018004943  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081018004943  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081018004943  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081018014359                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20081022033948  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081022033953  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081022033954  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081022033954  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081022033958  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081022033958  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081022033959  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081022033959  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081022033959  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081022060517                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20081022033948  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515063844  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515063845  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515063845  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515063845  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515063846  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515063846  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515063846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515063846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515063846  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515064118                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081021112854  CV  DAT$            G�O�G�O�F��3                JM  ARCAJMQC1.0                                                                 20081031024434  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081031024434  CV  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081031024834  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045303  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045429                      G�O�G�O�G�O�                