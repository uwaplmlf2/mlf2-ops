CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900990 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               sA   JA  20080809004959  20090916045437  A5_06497_115                    2C  D   APEX-SBE 223                                                    846 @��(-�c�1   @��,n���@2���R@b�=p��
1   ARGOS   A   A   A   @���A33Ah  A�33A�33A�  B  B��B2  BE33BY��Bm��B�33B���B�  B�  B���B���B�ffB�  B�33B�33B�33B���B���CL�C33CffC� C��CffCL�C$33C)33C.ffC2�fC8L�C=33CBffCGL�CQ��C[ffCe33CoL�CyL�C���C��3C�� C��fC�� C���C���C���C�� C��3C���C��3C��fC�C���C̙�Cљ�C֙�Cۙ�C�3C� CꙚC�ffC� C�ffD��D� D��D�3D� DٚD��D$��D)��D.��D3�3D8��D=� DB��DG��DL� DQ�3DV� D[�3D`� De��Dj�fDo��Dt�3Dy�3D�0 D�l�D�� D��fD�&fD�l�D���D��D�,�D�c3D��fD���D�)�D�ffDک�D��fD�#3D�i�D�fD�p 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��AY��A�  A�  A���BffB  B.ffBA��BV  Bj  B|��B���B�33B�33B���B���B���B�33B�ffB�ffB�ffB�  B�  C ffCL�C
� C��C�3C� CffC#L�C(L�C-� C2  C7ffC<L�CA� CFffCP�3CZ� CdL�CnffCxffC�&fC�@ C�L�C�33C��C�&fC�&fC�&fC�L�C�@ C�&fC�@ C�33C��C�Y�C�&fC�&fC�&fC�&fC�@ C��C�&fC��3C��C��3D�3D�fD�3D��D�fD� D�3D$�3D)�3D.�3D3��D8� D=�fDB�3DG�3DL�fDQ��DV�fD[��D`�fDe�3Dj��Do�3Dt��Dy��D�3D�P D��3D�ɚD�	�D�P D���D���D� D�FfD���D�� D��D�I�Dڌ�D�ɚD�fD�L�D�D�S31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�5?A�?}A�K�A�hA�%A���A���A�A띲A�Q�A���A�ƨA��HA�{A���A�A��A��A�|�A�A�Q�Aд9A��A̼jA��Aɕ�A�dZA��;A�K�AÓuA�1'A���A�  A�A��RA��DA��wA�1A�5?A���A�;dA���A�$�A�ZA�|�A�JA��HA���A��A�
=A���A��/A��RA���A�M�AsS�Al �AhȴA_�A_l�AW��APZAN{AE%A@I�A:��A6r�A1VA+�TA&n�A+A�7A�-@�J@�!@�/@���@Ѳ-@ř�@��@�E�@��T@��7@�z�@���@�@�l�@��@��-@�~�@��@�/@�ƨ@���@�S�@|�D@s@f�@W|�@T(�@E�-@;ƨ@8�u@3t�@/��@*��@$�@$�@~�@��@��@ff@Ĝ@�@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�5?A�?}A�K�A�hA�%A���A���A�A띲A�Q�A���A�ƨA��HA�{A���A�A��A��A�|�A�A�Q�Aд9A��A̼jA��Aɕ�A�dZA��;A�K�AÓuA�1'A���A�  A�A��RA��DA��wA�1A�5?A���A�;dA���A�$�A�ZA�|�A�JA��HA���A��A�
=A���A��/A��RA���A�M�AsS�Al �AhȴA_�A_l�AW��APZAN{AE%A@I�A:��A6r�A1VA+�TA&n�A+A�7A�-@�J@�!@�/@���@Ѳ-@ř�@��@�E�@��T@��7@�z�@���@�@�l�@��@��-@�~�@��@�/@�ƨ@���@�S�@|�D@s@f�@W|�@T(�@E�-@;ƨ@8�u@3t�@/��@*��@$�@$�@~�@��@��@ff@Ĝ@�@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�jB	�jB	�}B	�B
O�B
e`B
VB
�
B
�B
��B1B\B�B6FB:^B=qB6FB9XBT�BjB��B%B49BI�BG�BI�BJ�BM�BZBq�B?}B(�BhB{BVB��BBDB�mB�HB��B�JBm�BgmBS�BH�B<jB-B�BoBB
�B
B
�B
��B
_;B
<jB
%�B	��B	��B	ȴB	��B	��B	k�B	ZB	B�B	0!B	�B	%B�B�B�B�B��B��B�-B�qB�dBȴB�B��B��B	{B	B�B	D�B	`BB	|�B	�{B	��B	�LB	ŢB	��B	��B	��B	�B	�yB
B
DB
�B
-B
2-B
B�B
H�B
N�B
R�B
YB
_;B
ffB
k�B
n�B
u�B
x�B
~�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�jB	�jB	�}B	�B
O�B
ffB
VB
�B
�B
��B
=BhB�B7LB<jB?}B8RB<jBW
Bk�B��B+B6FBK�BG�BI�BJ�BN�B]/Bu�BB�B,BoB{BoB��BBVB�yB�NB��B�\Bn�BhsBVBI�B=qB/B�BuBB
�B
ÖB
�B
��B
aHB
=qB
'�B	��B	��B	��B	��B	��B	l�B	\)B	C�B	2-B	�B	1B��B�B�B�B��B��B�3B�wB�jB��B�B��B��B	�B	B�B	D�B	aHB	}�B	��B	��B	�RB	ƨB	��B	��B	��B	�B	�B
B
JB
�B
-B
2-B
B�B
H�B
N�B
S�B
YB
`BB
ffB
l�B
n�B
u�B
y�B
~�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.9(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808220243362008082202433620080822024336200808220301372008082203013720080822030137200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20080809004952  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080809004959  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080809004959  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080809005000  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080809005005  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080809005005  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080809005005  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080809005005  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080809005005  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080809013306                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080813033841  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080813033846  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080813033847  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080813033847  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080813033851  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080813033851  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080813033851  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080813033851  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080813033852  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080813051945                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080813033841  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515063828  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515063828  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515063828  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515063828  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515063829  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515063829  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515063829  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515063829  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515063829  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515064103                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080822024336  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080822024336  CV  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080822030137  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045319  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045437                      G�O�G�O�G�O�                