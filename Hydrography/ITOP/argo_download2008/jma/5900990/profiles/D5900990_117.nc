CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900990 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               uA   JA  20080829004955  20090916045438  A5_06497_117                    2C  D   APEX-SBE 223                                                    846 @��'�4x1   @��,��@31&�x�@b�vȴ9X1   ARGOS   A   A   A   @�  A��Ad��A�  A���A�  B
��BffB2  BE��BZ  Bm33B�ffB�  B�  B�33B�ffB�ffB�  B�ffB���B�33B䙚B�  B���C�3CL�C� C�C33C�CL�C$  C)� C.33C3  C8��C=ffCB� CG� CQ33CZ�fCe� Co33Cx�fC�� C��fC�� C���C���C��3C��3C�� C�� C�� C���C��3C���C�Cǌ�C̦fC�� C֦fC۳3C�fC���C�� CC��3C�� D�fD��D�3D��D� D�fD�fD$ٚD)ٚD.ٚD3�3D8��D=� DBٚDG�fDL�3DQ�fDV��D[�fD`�3De��Dj�fDo��Dt��Dy�3D�)�D�ffD��3D��3D�)�D�` D���D���D�,�D�p D�� D�� D�&fD�` Dک�D���D�,�D�l�D��D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A��AT��A�  A���A�  B��BffB.  BA��BV  Bi33B|��B�  B�  B�33B�ffB�ffB�  B�ffB���B�33B♚B�  B���C �3CL�C
� C�C33C�CL�C#  C(� C-33C2  C7��C<ffCA� CF� CP33CY�fCd� Cn33Cw�fC�@ C�&fC�@ C��C��C�33C�33C�@ C�@ C�  C�L�C�33C��C��C��C�&fC�@ C�&fC�33C�&fC�L�C�@ C��C�33C�  D�fD��D�3D��D� D�fD�fD$��D)��D.��D3�3D8��D=� DB��DG�fDL�3DQ�fDV��D[�fD`�3De��Dj�fDoy�Dty�Dys3D�	�D�FfD��3D��3D�	�D�@ D�|�D���D��D�P D�� D�� D�fD�@ Dډ�D��D��D�L�D��D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�`BA�A�-A��A�  A�&�A�9XA�FA�/A��AլA�Q�A�VA���A���Aŝ�AÅA��A�n�A���A�(�A�ZA�Q�A�
=A�hsA���A��A�t�A�l�A��TA�A���A��A�G�A�S�A���A���A�  A��\A�G�A��A���A�=qA�|�A��PA�9XA���A��#A��A�=qA�S�A� �Az�AtJAc�FAaO�A\{AS��AQ��ALbAHjA@-A=�A4�RA/|�A+�wA%�7A Q�A�DA�+At�AVA��@���@�\@��@�
=@�@Ĵ9@�G�@�~�@��@��R@�\)@�9X@��#@��/@��u@��m@�l�@�~�@��j@�@���@��+@~ȴ@oK�@b��@\j@V��@LZ@F$�@@  @6�R@,�D@&�R@!hs@/@��@�T@M�@��@
��@1'@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�`BA�A�-A��A�  A�&�A�9XA�FA�/A��AլA�Q�A�VA���A���Aŝ�AÅA��A�n�A���A�(�A�ZA�Q�A�
=A�hsA���A��A�t�A�l�A��TA�A���A��A�G�A�S�A���A���A�  A��\A�G�A��A���A�=qA�|�A��PA�9XA���A��#A��A�=qA�S�A� �Az�AtJAc�FAaO�A\{AS��AQ��ALbAHjA@-A=�A4�RA/|�A+�wA%�7A Q�A�DA�+At�AVA��@���@�\@��@�
=@�@Ĵ9@�G�@�~�@��@��R@�\)@�9X@��#@��/@��u@��m@�l�@�~�@��j@�@���@��+@~ȴ@oK�@b��@\j@V��@LZ@F$�@@  @6�R@,�D@&�R@!hs@/@��@�T@M�@��@
��@1'@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBjBcTBaHBffBffB�B	%B	[#B
$�B
t�B
��B
�LB
�sB>wBdZB�+B��B��B��B��B��B�3B�B��B��B��B��B��B��B�PB�B�Bz�Bs�Bm�Be`BbNB\)BXBO�BA�B<jB6FB,B!�B�BB
�B
�yB
�
B
ĜB
�B
�oB
l�B
{B
B	�HB	�XB	�B	�\B	{�B	[#B	H�B	(�B	�B	%B��B�NB�B��B�FB�B�B�B�?B�B��B	
=B	�B	�B	�B	�B	�B	6FB	L�B	hsB	q�B	�+B	�DB	�uB	�B	�B	B	��B	�B	�B
+B
�B
"�B
.B
9XB
@�B
D�B
N�B
YB
_;B
cTB
gmB
jB
n�B
s�B
y�B
}�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 Bk�BcTBaHBffBffB�%B	1B	aHB
(�B
x�B
��B
�^B
�BA�BgmB�7B��B��B��B��B��B�?B�!B��B��B��B�B��B��B�\B�%B�B{�Bt�Bo�BffBdZB\)BYBQ�BB�B<jB7LB-B"�B�BB
�B
�B
�B
ŢB
�B
�{B
p�B
�B
B	�TB	�^B	�B	�hB	}�B	\)B	J�B	)�B	�B	1B��B�ZB�B��B�RB�B�!B�B�LB�B��B	DB	�B	�B	�B	�B	 �B	7LB	M�B	iyB	q�B	�+B	�JB	�{B	�B	�B	ÖB	��B	�
B	�B
1B
�B
#�B
/B
:^B
A�B
D�B
N�B
ZB
_;B
cTB
gmB
k�B
o�B
t�B
z�B
}�B
�B
�%1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=1.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809110214372008091102143720080911021437200809110223302008091102233020080911022330200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20080829004948  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080829004955  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080829004955  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080829004956  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080829005000  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080829005000  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080829005001  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080829005001  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080829005001  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080829021410                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080902033913  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080902033917  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080902033918  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080902033918  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080902033922  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080902033922  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080902033922  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080902033922  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080902033923  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080902050434                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080902033913  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515063832  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515063833  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090515063833  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515063833  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515063834  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515063834  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515063834  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515063834  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515063834  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515064104                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080901112709  CV  DAT$            G�O�G�O�F�a\                JM  ARCAJMQC1.0                                                                 20080911021437  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080911021437  CV  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080911022330  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045320  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045438                      G�O�G�O�G�O�                