CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901521 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               :A   JA  20080920012158  20090519070956                                  2B  A   APEX-SBE 2807                                                   846 @����|�1   @��$�8�@2e�S���@`؛��S�1   ARGOS   A   A   A   @���A  Ad��A���A�33A���B��B��B0��BFffBZ  BnffB�  B�33B�33B�  B�ffB�  B�  B�33B�33B�ffB���BB���CL�C��C� C��CffC� CL�C$�C)ffC-�fC3L�C833C=  CBL�CG  CQ� C[ffCe33CoL�CyL�C�Y�C�s3C���C���C���C���C�� C��fC���C���C��fC��fC��fC¦fCǦfC̦fC�� C֙�CۦfC���C噚C��C�fC� C��3D�3D� D�fDٚD��D� D� D$� D)�fD.ٚD3��D8�fD=��DBٚDG��DL� DQٚDV� D[� D`� De��Dj�fDo��DtٚDy��D�0 D�ffD��3D���D�#3D�i�D��3D���D�  D�i�D��fD�� D�)�D�l�Dڰ D��fD�)�D�` D� D�P 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�34A��Aa��A�  A���A�33B  B��B0  BE��BY33Bm��B���B���B���B���B�  B���B���B���B���B�  B�fgB�34B�34C�CfgCL�CfgC33CL�C�C#�gC)33C-�3C3�C8  C<��CB�CF��CQL�C[33Ce  Co�Cy�C�@ C�Y�C��3C��3C��3C�s3C�ffC���C�s3C�s3C���C���C���C�Cǌ�Č�CѦfCր Cی�C�s3C� C�s3C��C�ffC���D�fD�3D��D��D��D�3D�3D$�3D)��D.��D3��D8��D=� DB��DG� DL�3DQ��DV�3D[�3D`�3De� DjٙDo� Dt��Dy� D�)�D�` D���D��gD��D�c4D���D��gD��D�c4D�� D��D�#4D�fgDک�D�� D�#4D�Y�D�D�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�|�A�\A�A�A�A��DA�t�A�t�A��;A��A�ȴA�G�A�  A��A�9A�bAڸRA�dZAדuA�\)A�|�A�t�A�ȴA�~�A�1A�?}A�l�A´9A��HA��A���A��-A�Q�A���A�|�A���A��A��jA��7A���A�O�A���A�n�A��yA���A���A���A�A�33A��DA�^5A��
A���A�;dA�I�A���A}C�AzjAs�wAj�DAiC�Aa��A[C�AU�wAL��AEdZA<E�A4��A/\)A*E�A%XAhsA��A��A-@���@�$�@�(�@��@�dZ@��w@�V@��\@�@��F@�r�@���@�E�@��y@��+@��+@�+@�|�@��-@�O�@}�@n�R@f@[�@R�@K33@EO�@=�@7+@0bN@+��@$�@!%@o@5?@��@+@�D@	7L@E�@^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�|�A�\A�A�A�A��DA�t�A�t�A��;A��A�ȴA�G�A�  A��A�9A�bAڸRA�dZAדuA�\)A�|�A�t�A�ȴA�~�A�1A�?}A�l�A´9A��HA��A���A��-A�Q�A���A�|�A���A��A��jA��7A���A�O�A���A�n�A��yA���A���A���A�A�33A��DA�^5A��
A���A�;dA�I�A���A}C�AzjAs�wAj�DAiC�Aa��A[C�AU�wAL��AEdZA<E�A4��A/\)A*E�A%XAhsA��A��A-@���@�$�@�(�@��@�dZ@��w@�V@��\@�@��F@�r�@���@�E�@��y@��+@��+@�+@�|�@��-@�O�@}�@n�R@f@[�@R�@K33@EO�@=�@7+@0bN@+��@$�@!%@o@5?@��@+@�D@	7L@E�@^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�%B�B�%Bz�Bk�B[#BS�B]/B�BĜB��B		7B	M�B	ÖB
VB
`BB
u�B
�=B
��B
��B
��B
�mB  B�B6FBC�BE�B�B�B�3B��B�#B�#B�BĜB�qB�^B�jB�9B�hB�Bq�Bu�B|�Bn�B`BBO�B?}B49B#�BB
�B
�B
ŢB
�?B
��B
�%B
bNB
5?B
+B
B	�;B	��B	��B	q�B	N�B	0!B	�B	PB��B�`B��BŢB��B�oB�hB��B��B�FB�
B��B	oB	+B	;dB	F�B	cTB	jB	�bB	��B	��B	�9B	ǮB	��B	�HB	�fB	��B
JB
uB
'�B
2-B
9XB
?}B
G�B
M�B
T�B
ZB
^5B
ffB
k�B
o�B
s�B
u�B
z�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�%B�B�%Bz�Bk�B[#BS�B]/B�BĜB��B		7B	M�B	ÖB
VB
`BB
u�B
�=B
��B
��B
��B
�mB  B�B6FBC�BE�B�B�B�3B��B�#B�#B�BĜB�qB�^B�jB�9B�hB�Bq�Bu�B|�Bn�B`BBO�B?}B49B#�BB
�B
�B
ŢB
�?B
��B
�%B
bNB
5?B
+B
B	�;B	��B	��B	q�B	N�B	0!B	�B	PB��B�`B��BŢB��B�oB�hB��B��B�FB�
B��B	oB	+B	;dB	F�B	cTB	jB	�bB	��B	��B	�9B	ǮB	��B	�HB	�fB	��B
JB
uB
'�B
2-B
9XB
?}B
G�B
M�B
T�B
ZB
^5B
ffB
k�B
o�B
s�B
u�B
z�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080920012140  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080920012158  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080920012202  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080920012210  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080920012210  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080920012212  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080920013604                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923162035  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080923162039  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080923162040  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080923162044  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080923162044  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080923162044  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080923191056                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081020002027  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020002027  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020002031  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081020002031  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020002031  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081020002031  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081020002032  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020021109                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080923162035  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519070434  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519070435  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519070435  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519070435  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519070436  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519070436  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519070436  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519070436  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519070437  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519070956                      G�O�G�O�G�O�                