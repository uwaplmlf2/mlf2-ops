CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901521 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               7A   JA  20080821012127  20090519070943                                  2B  A   APEX-SBE 2807                                                   846 @��+���1   @��+8�9@31���l�@`����F1   ARGOS   A   A   A   @�ffA��AfffA�ffA���A���B
  B  B2ffBF  B[��Bn��B�33B���B���B�ffB�33B�33B���B�ffB�33B���B�ffB�  B�33CL�C� C� CL�C33C33C� C$� C)� C.ffC3��C8  C=ffCB�CGL�CQ��C[ffCe� Co� Cx�fC�s3C��3C�� C���C���C���C�� C�� C��fC���C��fC���C��fC¦fCǳ3C���Cѳ3C֌�C�s3C�3C�fC�s3C�3C���C�� D�fD�fD�fD��D� D�fD�3D$�fD)�fD.��D3� D8ٚD=� DBٚDG� DL�fDQ�fDV�fD[��D`��De��Dj� Do��Dt��Dy� D��D�ffD��fD���D��D�ffD�� D��D��D�p D���D�ٚD�)�D�p Dک�D��3D��D�\�D�fD��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A  Ad��A���A���A�  B	��B��B2  BE��B[34BnfgB�  B�fgB���B�33B�  B�  B�fgB�33B�  Bڙ�B�33B���B�  C33CffCffC33C�C�CffC$ffC)ffC.L�C3� C7�fC=L�CB  CG33CQ� C[L�CeffCoffCx��C�ffC��fC��3C�� C�� C���C��3C��3C���C�� C���C���C���C�CǦfC�� CѦfCր C�ffC�fC噙C�ffC�fC�� C��3D� D� D� D�gDٚD� D��D$� D)� D.�gD3��D8�4D=ٚDB�4DGٚDL� DQ� DV� D[�gD`�gDe�gDj��Do�gDt�gDy��D��D�c3D��3D��D��D�c3D���D��gD�gD�l�D���D��gD�&gD�l�DڦgD�� D�gD�Y�D�3D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�JA�t�A�9XA�{A��A�z�A�v�A�dZA�A�A�O�A�A���A��A�A��/Aߕ�A�
=A�C�A�E�A�A׬A�C�A��
A�p�A���A�$�AˬA�VA�G�A�S�Aò-A�|�A�$�A��A�/A��A�O�A���A�r�A���A��A��^A��`A��/A��
A�9XA��A���A���A�VA���A�r�A��A�r�A��hA�
=A���A���A���At�9Ar�An��AbI�A_x�AW/AP�9AM��AG��A9�A4�yA*{A"9XA7LAVAbNA��@�I�@�bN@�b@�v�@��/@�p�@��H@�?}@��R@���@���@�\)@�+@�hs@��9@�p�@�~�@�E�@�|�@�Z@o�w@a��@V5?@Q��@I�7@CS�@<�/@8�u@2^5@)x�@%@"=q@��@t�@�u@�j@7L@�@bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�JA�t�A�9XA�{A��A�z�A�v�A�dZA�A�A�O�A�A���A��A�A��/Aߕ�A�
=A�C�A�E�A�A׬A�C�A��
A�p�A���A�$�AˬA�VA�G�A�S�Aò-A�|�A�$�A��A�/A��A�O�A���A�r�A���A��A��^A��`A��/A��
A�9XA��A���A���A�VA���A�r�A��A�r�A��hA�
=A���A���A���At�9Ar�An��AbI�A_x�AW/AP�9AM��AG��A9�A4�yA*{A"9XA7LAVAbNA��@�I�@�bN@�b@�v�@��/@�p�@��H@�?}@��R@���@���@�\)@�+@�hs@��9@�p�@�~�@�E�@�|�@�Z@o�w@a��@V5?@Q��@I�7@CS�@<�/@8�u@2^5@)x�@%@"=q@��@t�@�u@�j@7L@�@bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B�B��B��B�B�B�B�B��B	@�B	�B
-B
F�B
aHB
�DB
��B
�dB
ǮB
�ZB
��B�B2-BE�Be`Bq�Bz�Bz�B{�Bt�Bw�Bu�Bx�Bz�B{�B~�B{�B�B�B|�B�B|�BdZBhsBiyBiyBw�By�Bx�Bk�BbNB\)BQ�BD�B=qB0!B �BDB
�BB
��B
XB
J�B
K�B	��B	�ZB	�}B	��B	��B	�%B	B�B	0!B	oB��B�)B��B�NB�ZB�RB�BB��B�XBɺB�B��B	B	�B	#�B	<jB	N�B	k�B	}�B	�PB	��B	�'B	ÖB	�;B	��B
B
hB
 �B
1'B
6FB
A�B
G�B
M�B
R�B
XB
^5B
bNB
ffB
jB
n�B
r�B
v�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B�B��B��B�B�B�B�B��B	@�B	�B
-B
F�B
aHB
�DB
��B
�dB
ǮB
�ZB
��B�B2-BE�Be`Bq�Bz�Bz�B{�Bt�Bw�Bu�Bx�Bz�B{�B~�B{�B�B�B|�B�B|�BdZBhsBiyBiyBw�By�Bx�Bk�BbNB\)BQ�BD�B=qB0!B �BDB
�BB
��B
XB
J�B
K�B	��B	�ZB	�}B	��B	��B	�%B	B�B	0!B	oB��B�)B��B�NB�ZB�RB�BB��B�XBɺB�B��B	B	�B	#�B	<jB	N�B	k�B	}�B	�PB	��B	�'B	ÖB	�;B	��B
B
hB
 �B
1'B
6FB
A�B
G�B
M�B
R�B
XB
^5B
bNB
ffB
jB
n�B
r�B
v�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080821012109  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080821012127  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080821012131  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080821012138  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080821012139  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080821012139  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080821013422                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080825042529  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080825042533  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080825042534  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080825042538  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080825042539  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080825042539  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080825052143                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081020002011  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020002011  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020002015  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081020002015  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020002016  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081020002016  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081020002016  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020020918                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080825042529  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519070426  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519070427  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519070427  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519070427  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519070428  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519070428  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519070428  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519070428  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519070428  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519070943                      G�O�G�O�G�O�                