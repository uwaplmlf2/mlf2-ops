CDF   0   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   p   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4\   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C<   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E,   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H,   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K,   CALIBRATION_DATE            	             
_FillValue                  ,  N,   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    NX   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N\   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N`   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Nd   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Nh   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5901521 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               9A   JA  20080911025257  20090519070945                                  2B  A   APEX-SBE 2807                                                   846 @��%9D�[1   @��%a"�@2���Q�@`�7KƧ�1   ARGOS   B   B   B   G�O�A�  A홚B
  B��B2  BF��BZffBm��B���B�33B�  B�33B���B�  B�ffB�ffB���Bڙ�B�ffB���B�33C  C��CL�CL�C�3C� C33C$�C)ffC.33C333C8� C=33CB33CG33CQ� C[� Ce�Co� Cy� C�� C��3C��3C��fC��fC���C�� C���C�s3C�ffC��fC��fC�� C���C�� C�� CѦfC�s3Cی�C���C� C�� C���C��3C��fD� D�fD� D� DٚD�3D� D$ٚD)��D.��D3ٚD8ٚD=�fDBٚDGٚDL�3DQ�fDV� D[��D`��DeٚDj� Do��Dt��Dy�3D�,�D�l�D��3D��fD�,�D�c3D��3D��fD�)�D�c3D�� D�� D��D�c3Dک�D��D�  D�` D� D�Ff4111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�A�ffA�  B	33B��B133BF  BY��Bl��B�fgB���B���B���B�34B���B�  B�  B�fgB�34B�  B�fgB���C ��C��C�C�C� CL�C  C#�gC)33C.  C3  C8L�C=  CB  CG  CQL�C[L�Cd�gCoL�CyL�C��fC���C���C���C���C�� C��fC�s3C�Y�C�L�C���C���C�ffC³3CǦfC̦fCь�C�Y�C�s3C�� C�ffC�fC�3C���C���D�3DٙD�3D�3D��D�fD�3D$��D)��D.� D3��D8��D=ٙDB��DG��DL�fDQ��DV�3D[� D`� De��Dj�3Do� Dt��Dy�fD�&gD�fgD���D�� D�&gD�\�D���D�� D�#4D�\�D���D�ٚD�gD�\�Dڣ4D��4D��D�Y�D�D�@ 4111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A�ƨA�x�A뗍A�l�A�jA䟾A�  A�^5A��A�+A�A��A�/Aݙ�A۲-A�ƨA���A��/AС�Aϲ-A�\)Aͺ^A��yA���A���A�A�^5A�1'A���A�E�A��A���A�~�A���A���A�5?A���A���A�  A���A��jA�=qA���A�C�A��yA��A���A�JA�;dA�z�A�
=A�;dA��A~�uAy?}Au�Ah�DA`ZAWx�AQG�AI��AF��A>�A9VA1�mA.�9A!�AA�A�A{@���@�`B@�?}@�p�@�9X@�t�@���@�I�@��`@�A�@�l�@��-@��/@��@��^@��\@��y@��@�?}@���@��+@u�-@ihs@_;d@U�@M�@E��@@ �@7
=@.��@(�9@#"�@ ��@��@��@��@��@�@C�@ �@�-4111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A�ƨA�x�A뗍A�l�A�jA䟾A�  A�^5A��A�+A�A��A�/Aݙ�A۲-A�ƨA���A��/AС�Aϲ-A�\)Aͺ^A��yA���A���A�A�^5A�1'A���A�E�A��A���A�~�A���A���A�5?A���A���A�  A���A��jA�=qA���A�C�A��yA��A���A�JA�;dA�z�A�
=A�;dA��A~�uAy?}Au�Ah�DA`ZAWx�AQG�AI��AF��A>�A9VA1�mA.�9A!�AA�A�A{@���@�`B@�?}@�p�@�9X@�t�@���@�I�@��`@�A�@�l�@��-@��/@��@��^@��\@��y@��@�?}@���@��+@u�-@ihs@_;d@U�@M�@E��@@ �@7
=@.��@(�9@#"�@ ��@��@��@��@��@�@C�@ �@�-4111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�BdZBP�Br�B��B	hB	[#B
%B
7LB
K�B
��B
�LB
ĜB
�}B
�XB
�\B
O�B
}�BC�B
��B
�B
�B+BA�BK�BM�BO�B��B��B��B��B�}BǮBB�'B�uB�B�B��B�Bq�BiyB]/BVBM�B;dB0!B�B%B
�B
�NB
��B
ȴB
�wB
�B
�DB
o�B
�B	��B	ĜB	�B	�7B	y�B	T�B	<jB	&�B	�B�B�HB�B�XB��B�oB�{B��B��B�RB��B��B	�B	49B	J�B	W
B	aHB	r�B	� B	�=B	��B	�RB	�dB	��B	��B	�B
B
{B
#�B
.B
5?B
;dB
F�B
P�B
VB
]/B
`BB
cTB
gmB
m�B
q�B
s�B
x�B
{�B
~�4111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�BdZBP�Br�B��B	hB	[#B
%B
7LB
K�B
��B
�LB
ĜB
�}B
�XB
�\B
O�B
}�BC�B
��B
�B
�B+BA�BK�BM�BO�B��B��B��B��B�}BǮBB�'B�uB�B�B��B�Bq�BiyB]/BVBM�B;dB0!B�B%B
�B
�NB
��B
ȴB
�wB
�B
�DB
o�B
�B	��B	ĜB	�B	�7B	y�B	T�B	<jB	&�B	�B�B�HB�B�XB��B�oB�{B��B��B�RB��B��B	�B	49B	J�B	W
B	aHB	r�B	� B	�=B	��B	�RB	�dB	��B	��B	�B
B
{B
#�B
.B
5?B
;dB
F�B
P�B
VB
]/B
`BB
cTB
gmB
m�B
q�B
s�B
x�B
{�B
~�4111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080911025254  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080911025257  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080911025258  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080911025302  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080911025302  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080911025302  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080911025302  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20080911025302  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080911030313                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080914051019  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080914051035  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080914051040  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080914051049  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080914051049  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080914051049  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080914051049  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20080914051051  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20080914051051  CV  TIME            G�O�G�O�F�y+                JA  ARGQrelo2.1                                                                 20080914051051  CV  LAT$            G�O�G�O�A�\)                JA  ARGQrelo2.1                                                                 20080914051051  CV  LON$            G�O�G�O�C	�                JA  ARUP                                                                        20080914064715                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081020002021  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020002022  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020002026  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081020002026  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081020002026  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081020002026  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020002026  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20081020002026  QCF$                G�O�G�O�G�O�            4100JA  ARGQaqcp2.8a                                                                20081020002026  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081020002026  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20081020002026  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020021034                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080914051019  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519070432  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519070432  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519070432  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519070433  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519070434  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090519070434  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519070434  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519070434  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519070434  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090519070434  QCF$                G�O�G�O�G�O�            4100JA  ARGQaqcp2.8b                                                                20090519070434  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519070434  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20090519070434  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519070945                      G�O�G�O�G�O�                