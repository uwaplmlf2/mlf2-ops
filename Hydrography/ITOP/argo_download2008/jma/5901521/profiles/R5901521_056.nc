CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901521 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               8A   JA  20080831014546  20090519070946                                  2B  A   APEX-SBE 2807                                                   846 @�읰��1   @���6;�@2�I�^@`�/��w1   ARGOS   A   A   A   @�33A33Ad��A�  A�33A�ffB	��B33B133BE33BZffBlffB�ffB�  B�ffB�33B���B�  B���B�ffB�  Bڙ�B�33B�  B���CffCL�C� C��CffCffCffC$33C)�C.�C333C833C=� CB��CG33CQ�C[� Ce� CoffCyL�C��3C�ٚC���C���C��3C��fC���C�� C�� C�� C�ffC���C���C�Cǌ�C̳3Cѳ3C֦fCۦfC�� C� C� C��C���C�� DٚDٚD� D�3D� D� D��D$ٚD)��D.�3D3� D8�3D=��DB�3DG��DLٚDQ��DVٚD[��D`��De��Dj�3Do�fDt��DyٚD�#3D�Y�D��fD��fD�)�D�ffD�� D���D�)�D�c3D�� D��3D��D�` Dڬ�D��D�&fD�\�D��D�L�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A��Ac33A�33A�ffA陙B	34B��B0��BD��BZ  Bl  B�33B���B�33B�  B���B���B�fgB�33B���B�fgB�  B���B�fgCL�C33CffC� CL�CL�CL�C$�C)  C.  C3�C8�C=ffCB� CG�CQ  C[ffCeffCoL�Cy33C��fC���C�� C�� C��fC���C�� C��3C��3C�s3C�Y�C���C�� C Cǀ C̦fCѦfC֙�Cۙ�C�s3C�s3C�s3C� C��C�s3D�4D�4D��D��D��DٚD�gD$�4D)�4D.��D3��D8��D=�gDB��DG�gDL�4DQ�gDV�4D[�4D`�gDe�gDj��Do� Dt�4Dy�4D�  D�VgD��3D��3D�&gD�c3D���D�ٚD�&gD�` D���D�� D��D�\�Dک�D��gD�#3D�Y�D�D�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�n�A�n�A�Q�A�VA��#A�"�A�ZA�"�A�ffA�A�wA�ȴA�^5A��`A���A�VA�9XA�/A��mA��yA���A���A�"�A�dZAԙ�A� �A���AҲ-A��A�XA���A�bA�1A�/A��A��A�x�A�G�A�-A���A���A�^5A�  A���A���A�1'A�t�A��uA�t�A��A��uA�VA�`BA�VA��A�`BA�Q�A��!A��A{7LAt�Ap9XAa�AX{AU�;AO��AF�AAG�A;��A5ƨA*{A�7A��A
^5A�@�&�@�/@��m@�z�@Ѓ@�9X@�p�@��@�p�@�ȴ@���@�|�@�(�@���@���@�n�@�|�@�hs@��;@�@�9X@u��@i�7@]��@T�D@P  @H �@@�u@8b@49X@.@'K�@"�H@{@��@��@��@E�@dZ@1'1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�n�A�n�A�Q�A�VA��#A�"�A�ZA�"�A�ffA�A�wA�ȴA�^5A��`A���A�VA�9XA�/A��mA��yA���A���A�"�A�dZAԙ�A� �A���AҲ-A��A�XA���A�bA�1A�/A��A��A�x�A�G�A�-A���A���A�^5A�  A���A���A�1'A�t�A��uA�t�A��A��uA�VA�`BA�VA��A�`BA�Q�A��!A��A{7LAt�Ap9XAa�AX{AU�;AO��AF�AAG�A;��A5ƨA*{A�7A��A
^5A�@�&�@�/@��m@�z�@Ѓ@�9X@�p�@��@�p�@�ȴ@���@�|�@�(�@���@���@�n�@�|�@�hs@��;@�@�9X@u��@i�7@]��@T�D@P  @H �@@�u@8b@49X@.@'K�@"�H@{@��@��@��@E�@dZ@1'1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�B�1B�VB�\B{�Bt�Br�Bu�B�DB�qB�B	&�B	Q�B	z�B	��B	�/B	��B
�B
Q�B
u�B
��B
�?B
ŢB
�B5?B`BBjB�%B�bB��B�XB�)B�mB�B0!B0!B;dBBŢB��B�TB�B��B��B�JBt�BR�BS�BP�BC�B?}B6FB#�BDB
�B
�/B
��B
�}B
��B
�B
bNB
G�B	��B	��B	�}B	��B	p�B	YB	E�B	2-B	
=B�B�BB�)BŢB��B�dB��BĜB�B�B�RB��B	B	,B	=qB	H�B	_;B	q�B	�+B	��B	��B	�-B	�qB	ĜB	�BB	�B
B
bB
�B
!�B
/B
8RB
A�B
J�B
Q�B
XB
]/B
bNB
hsB
m�B
q�B
t�B
w�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�B�1B�VB�\B{�Bt�Br�Bu�B�DB�qB�B	&�B	Q�B	z�B	��B	�/B	��B
�B
Q�B
u�B
��B
�?B
ŢB
�B5?B`BBjB�%B�bB��B�XB�)B�mB�B0!B0!B;dBBŢB��B�TB�B��B��B�JBt�BR�BS�BP�BC�B?}B6FB#�BDB
�B
�/B
��B
�}B
��B
�B
bNB
G�B	��B	��B	�}B	��B	p�B	YB	E�B	2-B	
=B�B�BB�)BŢB��B�dB��BĜB�B�B�RB��B	B	,B	=qB	H�B	_;B	q�B	�+B	��B	��B	�-B	�qB	ĜB	�BB	�B
B
bB
�B
!�B
/B
8RB
A�B
J�B
Q�B
XB
]/B
bNB
hsB
m�B
q�B
t�B
w�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080831014526  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080831014546  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080831014550  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080831014601  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080831014602  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080831014603  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080831024919                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080903161621  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080903161637  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080903161641  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080903161649  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080903161650  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080903161651  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080903190942                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081020002016  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020002017  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020002021  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081020002021  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020002021  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081020002021  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081020002021  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020020953                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080903161621  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519070429  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519070429  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519070430  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519070430  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519070431  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519070431  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519070431  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519070431  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519070431  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519070946                      G�O�G�O�G�O�                