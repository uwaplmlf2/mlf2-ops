CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901533 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               7A   JA  20080909050509  20090519011437                                  2B  A   APEX-SBE 2882                                                   846 @���-��P1   @�����R�@:��/��@b|�hs1   ARGOS   A   A   A   @���A��Ah  A�33A���A�ffB	��B��B1��BFffBY��Bm33B���B���B�ffB���B�33B�33B�ffB�33B�33B�  B�33BB���CL�C33C  CL�C�CffC  C#��C(�3C-�3C2�fC8�C=ffCA��CF�fCQ  C[  CeL�Cn�fCy33C��fC��3C���C�� C��3C�ffC���C�L�C���C�Y�C�� C�� C�� C���CǦfC̙�CѦfCֳ3C�s3C�� C��C�fC��C��3C���D� D� D�3D�3D�3D�fD��D$� D)� D.��D3� D8��D=��DB��DG�fDL� DQ��DV�fD[��D`� DeٚDj�fDo�3DtٚDy�fD�,�D�` D��fD�� D�33D�ffD�� D�� D�,�D�i�D��fD��D�0 D�c3Dک�D��D�&fD�l�D� D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  AfgAa��A�  A���A�33B  B33B0  BD��BX  Bk��B��B���B���B���B�ffB�ffB���B�ffB�ffB�33B�ffB���B�  C �gC��C
��C�gC�4C  C��C#fgC(L�C-L�C2� C7�4C=  CAfgCF� CP��CZ��Cd�gCn� Cx��C�s3C�� C���C���C�� C�33C�Y�C��C�fgC�&gC�L�C�L�C���C�C�s3C�fgC�s3Cր C�@ C�L�C�Y�C�s3C�Y�C� C�fgD�fD�fD��D��D��D��D�3D$�fD)�fD.�3D3�fD8�3D=� DB� DG��DL�fDQ�3DV��D[�3D`�fDe� Dj��Do��Dt� Dy��D�  D�S3D���D��3D�&fD�Y�D��3D��3D�  D�\�D���D���D�#3D�VfDڜ�D���D��D�` D�3D�	�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�wA�9A�A闍A�hA陚AЇ+A��mA�ƨA�hsA�v�A���A� �A�+A�jA���A�(�A�p�A��TA���A�JA��9A���A���A�ffA���A��A���A��A�z�A��A�A�A��A���A��yA�G�A���A��A���A��A��A���A��jA�hsA�S�A�{A�ȴA�A��A�1A��#A�|�A~�RA~A�A|9XAw�As\)Af5?Aa��A]33AY��AU��ATbAQhsAN�yAJ��AFjAD$�A@E�A6(�A-�mA#|�A�A�A��@�I�@�Z@���@�~�@�@�b@�Z@��@��@�;d@� �@�hs@��
@��-@�|�@��@|�@tZ@r=q@g|�@]?}@R��@I�7@A&�@;�F@65?@0A�@+o@$(�@\)@�@�H@  @1@b@��@ �9?��^?��j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A�wA�9A�A闍A�hA陚AЇ+A��mA�ƨA�hsA�v�A���A� �A�+A�jA���A�(�A�p�A��TA���A�JA��9A���A���A�ffA���A��A���A��A�z�A��A�A�A��A���A��yA�G�A���A��A���A��A��A���A��jA�hsA�S�A�{A�ȴA�A��A�1A��#A�|�A~�RA~A�A|9XAw�As\)Af5?Aa��A]33AY��AU��ATbAQhsAN�yAJ��AFjAD$�A@E�A6(�A-�mA#|�A�A�A��@�I�@�Z@���@�~�@�@�b@�Z@��@��@�;d@� �@�hs@��
@��-@�|�@��@|�@tZ@r=q@g|�@]?}@R��@I�7@A&�@;�F@65?@0A�@+o@$(�@\)@�@�H@  @1@b@��@ �9?��^?��j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�BhBuB�B�B�B�B�B
�mBG�Bq�B��B��B��B��B��B �B�B"�B �B!�B$�B#�B%�B$�B"�B"�B+B-B/B0!B33B2-B0!B/B-B(�B$�B$�B)�B'�B$�B�B�B{BPB%B
��B
�B
�TB
��B
ƨB
ÖB
�9B
��B
��B
��B
�B
jB
(�B
�B
B	�B	�;B	�B	��B	B	�-B	��B	�uB	�B	VB	0!B��B��B��B�bB_;B=qB6FB6FB>wBG�BP�B_;Bl�B�B�{B��BŢB�fB�B��B	\B	%�B	,B	M�B	p�B	��B	�RB	��B	�/B	�B	��B
B
uB
�B
0!B
8RB
?}B
G�B
N�B
S�B
_;B
gmB
m�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 BhBuB�B�B�B�B�B
�mBG�Bq�B��B��B��B��B��B �B�B"�B �B!�B$�B#�B%�B$�B"�B"�B+B-B/B0!B33B2-B0!B/B-B(�B$�B$�B)�B'�B$�B�B�B{BPB%B
��B
�B
�TB
��B
ƨB
ÖB
�9B
��B
��B
��B
�B
jB
(�B
�B
B	�B	�;B	�B	��B	B	�-B	��B	�uB	�B	VB	0!B��B��B��B�bB_;B=qB6FB6FB>wBG�BP�B_;Bl�B�B�{B��BŢB�fB�B��B	\B	%�B	,B	M�B	p�B	��B	�RB	��B	�/B	�B	��B
B
uB
�B
0!B
8RB
?}B
G�B
N�B
S�B
_;B
gmB
m�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080909050502  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080909050509  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080909050512  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080909050517  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080909050518  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080909050518  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080909052458                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080913041825  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080913041829  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080913041830  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080913041834  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080913041834  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080913041834  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080913051340                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081017001911  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081017001912  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081017001916  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081017001916  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081017001916  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081017001916  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081017001916  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081017015823                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080913041825  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519011037  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519011037  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519011038  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519011038  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519011039  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519011039  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519011039  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519011039  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519011039  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519011437                      G�O�G�O�G�O�                