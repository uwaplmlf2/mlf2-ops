CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901533 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               4A   JA  20080810014521  20090519011432                                  2B  A   APEX-SBE 2882                                                   846 @��p���1   @��p�ޠ@;+I�^@b_�;dZ1   ARGOS   A   A   A   @���A��Ak33A���A�  A���B	��B��B1��BFffBZ  Bn  B�33B�ffB���B�ffB�ffB�33B���B�  B���B���B䙚B���B���CffC��C
�fC�C33C�C�fC$L�C(�fC.  C3�C7�fC=L�CBffCGL�CQ  C[L�Ce�Cn�fCy�C���C�L�C�&fC�s3C���C�� C���C��fC�� C���C�ffC�s3C�s3C�C�33C�ffCѦfC֦fC�� C���C��C��C�3C�Y�C�ffD��D�fDٚDٚDٚD��D�fD$�3D)�3D.��D3ٚD8��D=��DB��DG�fDL��DQ��DV� D[ٚD`�fDe��Dj�fDo� Dt�3Dy�fD�fD�Y�D���D��3D�  D�i�D���D��fD�&fD�VfD���D���D��D�c3Dک�D��D�fD�c3D��D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA33Aa��A�  A�33A�  B34BfgB/34BD  BW��Bk��B�  B�33B���B�33B�33B�  B�fgB���Bϙ�Bٙ�B�fgB왚B���C ��C33C
L�C� C��C� CL�C#�3C(L�C-ffC2� C7L�C<�3CA��CF�3CPffCZ�3Cd� CnL�Cx� C�@ C�  C�ٙC�&fC�@ C�33C�� C�Y�C�s3C�L�C��C�&fC�&fC�@ C��fC��C�Y�C�Y�C�s3C�� C�@ C�@ C�ffC��C��D�gD� D�4D�4D�4D�gD� D$��D)��D.�4D3�4D8�4D=�gDB�gDG� DL�gDQ�4DV��D[�4D`� De�gDj� Do��Dt��Dy� D�3D�FgD��gD�� D��D�VgD��gD��3D�3D�C3D��gD�ٚD�	�D�P DږgD��gD�3D�P D�D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�z�A�t�A�v�A���A�S�A�  A�oA���A���A�"�A�n�A��^A�~�A��+A���A�;dA��hA�9XA���A��/A�l�A�7LA���A�E�A�%A��^A�K�A��yA�-A��;A���A�oA��A�~�A��wA�K�A���A�&�A���A��A��A��A���A�1'A�-A��A��
A��A�{A��A�K�A��A�A�t�A�A}��Az�AyK�AwVAs�PApE�AmXAk�Ah$�A`1'AX��AUO�AQƨAN�AIXAB�A<1'A3�A%�;Ax�AXA��A��@�@�dZ@��@�~�@�r�@�x�@�t�@���@�
=@��h@���@��@�~�@��;@�ƨ@}�-@wK�@l��@c�@V��@KS�@F5?@>E�@8 �@1��@*n�@$�j@�;@C�@�@b@ƨ@	hs@��@S�@7L?�O�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�z�A�t�A�v�A���A�S�A�  A�oA���A���A�"�A�n�A��^A�~�A��+A���A�;dA��hA�9XA���A��/A�l�A�7LA���A�E�A�%A��^A�K�A��yA�-A��;A���A�oA��A�~�A��wA�K�A���A�&�A���A��A��A��A���A�1'A�-A��A��
A��A�{A��A�K�A��A�A�t�A�A}��Az�AyK�AwVAs�PApE�AmXAk�Ah$�A`1'AX��AUO�AQƨAN�AIXAB�A<1'A3�A%�;Ax�AXA��A��@�@�dZ@��@�~�@�r�@�x�@�t�@���@�
=@��h@���@��@�~�@��;@�ƨ@}�-@wK�@l��@c�@V��@KS�@F5?@>E�@8 �@1��@*n�@$�j@�;@C�@�@b@ƨ@	hs@��@S�@7L?�O�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�B�BoB
�hB
[#B
iyB
9XB
oB
!�B
;dB
t�B
��B`BBZB]/B]/BN�B,B=qBI�BO�BF�B>wB:^B:^B:^B<jBH�BG�BC�B>wB9XB7LB>wB6FB6FB.B,B(�B+B,B+B$�B�BuBJB
=BB  B
��B
�B
�HB
��B
�dB
�B
��B
�uB
�=B
}�B
m�B
_;B
Q�B
F�B
7LB
hB	�B	�NB	��B	ÖB	�B	�B	ffB	A�B	{B�B��B�B�BcTBD�B33B+B.B5?B@�BK�B^5Bt�B��B�9B��B�/B�B	1B	�B	7LB	Q�B	y�B	��B	��B	ĜB	�B	�B	��B

=B
�B
&�B
6FB
@�B
J�B
O�B
YB
_;B
dZB
k�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�B�BoB
�hB
[#B
iyB
9XB
oB
!�B
;dB
t�B
��B`BBZB]/B]/BN�B,B=qBI�BO�BF�B>wB:^B:^B:^B<jBH�BG�BC�B>wB9XB7LB>wB6FB6FB.B,B(�B+B,B+B$�B�BuBJB
=BB  B
��B
�B
�HB
��B
�dB
�B
��B
�uB
�=B
}�B
m�B
_;B
Q�B
F�B
7LB
hB	�B	�NB	��B	ÖB	�B	�B	ffB	A�B	{B�B��B�B�BcTBD�B33B+B.B5?B@�BK�B^5Bt�B��B�9B��B�/B�B	1B	�B	7LB	Q�B	y�B	��B	��B	ĜB	�B	�B	��B

=B
�B
&�B
6FB
@�B
J�B
O�B
YB
_;B
dZB
k�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080810014454  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080810014521  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080810014526  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080810014539  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080810014540  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080810014542  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080810024413                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080814040051  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080814040055  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080814040055  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080814040059  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080814040059  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080814040100  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080814050643                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081017001856  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081017001856  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081017001900  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081017001900  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081017001901  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081017001901  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081017001901  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081017015610                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080814040051  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519011029  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519011030  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519011030  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519011030  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519011031  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519011031  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519011031  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519011031  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519011031  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519011432                      G�O�G�O�G�O�                