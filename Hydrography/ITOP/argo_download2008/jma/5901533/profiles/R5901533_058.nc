CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901533 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               :A   JA  20081009055440  20090519011443                                  2B  A   APEX-SBE 2882                                                   846 @��r{��1   @��s��$@:��$�@a�V�u1   ARGOS   A   A   A   @�ffA  Ah  A�ffA�33A�ffB  B  B2  BF  BZffBnffB�  B�  B�ffB�33B�  B���B�ffB�  B�ffB���B�  B�33B�ffC��C33C� C  CffC33C  C$L�C)L�C.33C3L�C8  C=33CBL�CG� CQffC[� Ce  Co�CyffC��fC��fC��3C���C���C�� C���C�s3C�s3C�Y�C��fC���C���C¦fCǦfČ�Cь�C֦fC�s3C�s3C��C�� CC��fC��fD��D� D�fD� D� D� D� D$�3D)�3D.�3D3� D8�fD=��DBٚDG� DL�fDQ�3DV� D[� D`� De��Dj� Do�3Dt�3Dy�fD�)�D�i�D�� D�� D�fD�ffD��fD��3D�,�D�c3D�� D��D�)�D�l�Dک�D�ٚD��D�Y�D�fD�,�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33AffA^ffA���A�ffA噙B��B��B/��BC��BX  Bl  B��B���B�33B�  B���B���B�33B���B�33Bؙ�B���B�  B�33C  C��C
�fCffC��C��CffC#�3C(�3C-��C2�3C7ffC<��CA�3CF�fCP��CZ�fCdffCn� Cx��C�Y�C�Y�C�ffC�� C�@ C�33C�@ C�&fC�&fC��C�Y�C�L�C�� C�Y�C�Y�C�@ C�@ C�Y�C�&fC�&fC�@ C�s3C�L�C�Y�C�Y�D�gD��D� D��D��D��D��D$��D)��D.��D3��D8� D=�gDB�4DG��DL� DQ��DV��D[��D`��De�4Dj��Do��Dt��Dy� D�gD�VgD���D���D�3D�S3D��3D�� D��D�P D���D��gD�gD�Y�DږgD��gD�gD�FgD�3D��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�Q�A�Q�A�bNA�K�AݬA���A���Aȉ7A���A�^5A�G�A�|�A�`BA�bNA���A���A��PA�t�A�t�A��mA��9A��A��A�z�A��FA��RA�$�A���A�VA���A�Q�A�|�A���A�C�A��#A��jA��A�z�A�bA���A��A�A�A�bNA���A��\A�l�A�t�A�G�A�ZA~v�A|��A{G�AxȴAt�`AoƨAk�wAhĜA^�RAYƨAT��AQS�ALjAH��AF�AD^5A?�A;XA5ƨA2��A(A�A n�A{A��A�@�|�@�bN@�x�@ʸR@�ƨ@�\)@���@�r�@���@��;@��F@���@�5?@��y@��D@���@���@�I�@{@t�D@o\)@d��@XQ�@Ix�@D�D@?�P@9�^@4j@.ff@&E�@!�#@Z@x�@�@�@	&�@�R@z�@%?��y?�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�Q�A�Q�A�bNA�K�AݬA���A���Aȉ7A���A�^5A�G�A�|�A�`BA�bNA���A���A��PA�t�A�t�A��mA��9A��A��A�z�A��FA��RA�$�A���A�VA���A�Q�A�|�A���A�C�A��#A��jA��A�z�A�bA���A��A�A�A�bNA���A��\A�l�A�t�A�G�A�ZA~v�A|��A{G�AxȴAt�`AoƨAk�wAhĜA^�RAYƨAT��AQS�ALjAH��AF�AD^5A?�A;XA5ƨA2��A(A�A n�A{A��A�@�|�@�bN@�x�@ʸR@�ƨ@�\)@���@�r�@���@��;@��F@���@�5?@��y@��D@���@���@�I�@{@t�D@o\)@d��@XQ�@Ix�@D�D@?�P@9�^@4j@.ff@&E�@!�#@Z@x�@�@�@	&�@�R@z�@%?��y?�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
�B
�B
�B
��B
��BJB�B�B�B�B/BC�BE�BS�B]/BZBXBW
BVBR�BR�BO�BL�BI�BF�BC�BA�B@�B<jB9XB9XB5?B33B2-B.B+B)�B'�B%�B �B�B{BbB+B
��B
��B
�yB
��B
��B
��B
��B
�=B
v�B
_;B
K�B
<jB
DB	��B	�5B	��B	�^B	�B	��B	��B	~�B	m�B	R�B	?}B	�B	B�`B��B��BhsBO�BA�B49B49B8RB?}BP�Bt�B~�B��B�9B�dB��B�B�B��B	�B	1'B	C�B	O�B	k�B	�7B	��B	�?B	��B	��B	�)B	�mB	��B
+B
�B
�B
,B
;dB
F�B
L�B
R�B
\)B
iyB
q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�B
�B
�B
�B
��B
��BJB�B�B�B�B/BC�BE�BS�B]/BZBXBW
BVBR�BR�BO�BL�BI�BF�BC�BA�B@�B<jB9XB9XB5?B33B2-B.B+B)�B'�B%�B �B�B{BbB+B
��B
��B
�yB
��B
��B
��B
��B
�=B
v�B
_;B
K�B
<jB
DB	��B	�5B	��B	�^B	�B	��B	��B	~�B	m�B	R�B	?}B	�B	B�`B��B��BhsBO�BA�B49B49B8RB?}BP�Bt�B~�B��B�9B�dB��B�B�B��B	�B	1'B	C�B	O�B	k�B	�7B	��B	�?B	��B	��B	�)B	�mB	��B
+B
�B
�B
,B
;dB
F�B
L�B
R�B
\)B
iyB
q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081009055438  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081009055440  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081009055441  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081009055445  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081009055445  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081009055446  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20081009065230                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081013040238  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081013040252  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081013040255  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081013040303  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081013040303  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081013040304  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20081013061217                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081017001927  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081017001928  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081017001932  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081017001932  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081017001932  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081017001932  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081017001932  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20081017020037                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081013040238  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519011044  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519011045  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519011045  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519011045  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519011046  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519011046  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519011046  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519011046  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519011047  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090519011443                      G�O�G�O�G�O�                