CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901533 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               5A   JA  20080820025518  20090519011433                                  2B  A   APEX-SBE 2882                                                   846 @����ZC�1   @���=�/h@:����F@bH��
=q1   ARGOS   A   A   A   @���AffAi��A�33A���A�33B	33B��B0��BG��BY��Bm��B���B�ffB���B���B�ffB���B�  Bř�BЙ�Bڙ�B䙚B�33B���C �fC� CL�C  C33C33CL�C$ffC)� C.� C3�3C8ffC=� CB33CG33CQ�C[33CeffCo� Cy  C��fC��fC�� C�� C�ffC�Y�C���C���C���C�s3C���C���C��3C³3C�� C���C�� C֙�Cۙ�C�fC��C� C�s3C��3C��3D��D��D��D��D�3DٚD� D$�fD)� D.� D3� D8ٚD=��DB��DGٚDL�fDQٚDV�3D[ٚD`�fDe�3Dj��DoٚDt� Dy��D�&fD�l�D�� D���D�  D�c3D��3D��fD�,�D�c3D���D��fD�#3D�` DڦfD��D��D�` D�D�L�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A  Ac34A�  A���A�  B��B  B/33BF  BX  Bl  B�  B���B�  B�  B���B�  B�33B���B���B���B���B�ffB�  C � C�C
�gC��C��C��C�gC$  C)�C.�C3L�C8  C=�CA��CF��CP�4CZ��Ce  Co�Cx��C�s3C�s3C���C�L�C�33C�&gC�fgC�fgC�fgC�@ C�fgC�fgC�� C Cǌ�C̙�Cь�C�fgC�fgC�s3C�Y�C�L�C�@ C� C�� D�3D� D�3D�3D��D� D�fD$��D)�fD.�fD3�fD8� D=� DB� DG� DL��DQ� DV��D[� D`��De��Dj�3Do� Dt�fDy�3D��D�` D��3D�� D�3D�VfD��fD�ٙD�  D�VfD���D�ٙD�fD�S3Dڙ�D���D� D�S3D��D�@ 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A��yA�n�A�+A�hsA��FA�\)A���A���A���A��mA�;dA���A�M�A�(�A���A��A�^5A�^5A�/A��A�t�A���A�A�x�A��7A�/A�dZA��A��^A�l�A�VA�7LA��A�jA���A��A�t�A�I�A���A�p�A��mA�S�A���A��A���A��-A��A�^5A��uA��yA�A�t�A��mA�?}A��A�ZA�{A|�jAy�As33Al��Ah�Ae�^AcC�A`5?A]�^A[�AY��AL��A@��A;hsA.jA�`A��A	"�@�A�@���@�=q@�33@�Ĝ@�p�@�&�@�
=@��P@��h@���@�`B@��@��m@��@�l�@{"�@v��@h  @Z^5@Q�@H��@@��@9�@0A�@+��@&�@"��@/@b@�@|�@dZ@��@�@�H?���?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A��yA�n�A�+A�hsA��FA�\)A���A���A���A��mA�;dA���A�M�A�(�A���A��A�^5A�^5A�/A��A�t�A���A�A�x�A��7A�/A�dZA��A��^A�l�A�VA�7LA��A�jA���A��A�t�A�I�A���A�p�A��mA�S�A���A��A���A��-A��A�^5A��uA��yA�A�t�A��mA�?}A��A�ZA�{A|�jAy�As33Al��Ah�Ae�^AcC�A`5?A]�^A[�AY��AL��A@��A;hsA.jA�`A��A	"�@�A�@���@�=q@�33@�Ĝ@�p�@�&�@�
=@��P@��h@���@�`B@��@��m@��@�l�@{"�@v��@h  @Z^5@Q�@H��@@��@9�@0A�@+��@&�@"��@/@b@�@|�@dZ@��@�@�H?���?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
��B
��B
��B
W
B
e`B
k�B
ffB
hsB
� B
�B
��BVB	7B-B:^B49B:^B9XB8RB<jB8RB8RB7LB5?B33B1'B/B.B-B.B.B,B-B,B(�B'�B%�B$�B#�B!�B �B�B�B�B�B{BoB1B
��B
��B
��B
�B
�B
�B
�`B
�;B
�B
ɺB
�dB
��B
�VB
n�B
Q�B
B�B
<jB
9XB
<jB
)�B
�B
	7B	��B	�DB	m�B	0!B�yBƨB��Bs�BbNBI�B;dB?}B>wBP�B]/BcTBo�B�JB��B�BÖB�B�B	bB	�B	G�B	q�B	�DB	��B	�wB	�
B	�B	��B
JB
�B
"�B
/B
:^B
B�B
J�B
P�B
XB
_;B
hsB
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
��B
��B
��B
W
B
e`B
k�B
ffB
hsB
� B
�B
��BVB	7B-B:^B49B:^B9XB8RB<jB8RB8RB7LB5?B33B1'B/B.B-B.B.B,B-B,B(�B'�B%�B$�B#�B!�B �B�B�B�B�B{BoB1B
��B
��B
��B
�B
�B
�B
�`B
�;B
�B
ɺB
�dB
��B
�VB
n�B
Q�B
B�B
<jB
9XB
<jB
)�B
�B
	7B	��B	�DB	m�B	0!B�yBƨB��Bs�BbNBI�B;dB?}B>wBP�B]/BcTBo�B�JB��B�BÖB�B�B	bB	�B	G�B	q�B	�DB	��B	�wB	�
B	�B	��B
JB
�B
"�B
/B
:^B
B�B
J�B
P�B
XB
_;B
hsB
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080820025514  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080820025518  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080820025519  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080820025523  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080820025523  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080820025523  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080820030457                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080824050955  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080824051008  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080824051011  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080824051019  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080824051019  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080824051021  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080824064459                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081017001901  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081017001901  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081017001906  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081017001906  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081017001906  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081017001906  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081017001906  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081017015654                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080824050955  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519011032  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519011032  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519011032  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519011033  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519011034  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519011034  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519011034  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519011034  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519011034  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519011433                      G�O�G�O�G�O�                