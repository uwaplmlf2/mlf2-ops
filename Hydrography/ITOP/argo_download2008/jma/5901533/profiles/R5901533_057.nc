CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   e   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  40   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  6,   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8(   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  9�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :$   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ;�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <    PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ?H   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  AD   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C@   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    Cp   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Fp   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Ip   CALIBRATION_DATE            	             
_FillValue                  ,  Lp   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    L�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    L�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    L�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    L�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  L�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    L�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    L�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M    HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    MArgo profile    2.2 1.2 19500101000000  5901533 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               9A   JA  20080929045532  20090519011438                                  2B  A   APEX-SBE 2882                                                   846 @���&�1   @���\(��@:l�����@a�+I�1   ARGOS   A   A   A   @�  A��Ad��A���A�33A�  B��B33B133BD  BX  Bj��B�33B�ffB�33B�ffB�33B�33B�33B���Bϙ�Bؙ�B�  B뙚B�33B�  C� C	ffCffCffCL�CL�C#�3C(��C-ffC2L�C7  C;��CA�3CFffCO� CZ33Cc��Cn�3Cx33C��3C���C��fC�L�C��C��fC�Y�C��3C�L�C���C�  C��C�&fC�@ C�Y�C���C�  C�Y�C�&fC��3C�� C�fC�fC�C���DL�DY�D�3DY�D�3D��D� D$s3D)Y�D.��D3y�D8ffD=� DB� DG��DLS3DQ` DV��D[� D`ffDe,�Djs3Do�fDt��Dy��D��fD� D�� D�� D��3D��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @���A  A[33A�  A�ffA�33BfgB��B.��BA��BU��BhfgB~  B�33B�  B�33B�  B�  B�  B�B�fgB�fgB���B�fgB�  B���C�fC��C��C��C�3C�3C#�C(  C,��C1�3C6ffC;  CA�CE��CN�fCY��Cc  Cn�Cw��C��fC�� C�Y�C�  C���C���C��C��fC�  C�� C��3C���C�ٙC��3C��Cˀ Cг3C��C�ٙCߦfC�s3C�Y�C�Y�C�L�C�@ D&gD34Dl�D34Dl�Ds4Dy�D$L�D)34D.s4D3S4D8@ D=y�DBY�DG�gDL,�DQ9�DVs4D[Y�D`@ DegDjL�Do` Dts4DyfgD��3D���D�l�D���D�� D��g11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A��A�{A���A��A�5?A��A�&�A�9XA��!A�oA���A�jA��7A� �A��!A��#A��;A�$�A��7A��A��;A��`A���A�+A�-A��RA��A�XA�A�%A���A�A�A���A�v�A�ZA��A��hA�jA���A��/A�+A�?}A�{A��A���A�r�A�ZA�A��+A��A��A�~�A�z�A|�\Au�
Ap=qAlQ�Ag�
Ae�hAb�A^n�AY�AV5?AOx�AL1AF(�AB��A:�/A2=qA*jA��A��A%A�@�bN@��H@�?}@�@�z�@�x�@��7@�E�@�bN@��
@���@���@�Ĝ@�E�@��;@��#@~v�@u��@o�w@YG�@E�-@;�@1%@-�h@+��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A��A��A��A�{A���A��A�5?A��A�&�A�9XA��!A�oA���A�jA��7A� �A��!A��#A��;A�$�A��7A��A��;A��`A���A�+A�-A��RA��A�XA�A�%A���A�A�A���A�v�A�ZA��A��hA�jA���A��/A�+A�?}A�{A��A���A�r�A�ZA�A��+A��A��A�~�A�z�A|�\Au�
Ap=qAlQ�Ag�
Ae�hAb�A^n�AY�AV5?AOx�AL1AF(�AB��A:�/A2=qA*jA��A��A%A�@�bN@��H@�?}@�@�z�@�x�@��7@�E�@�bN@��
@���@���@�Ĝ@�E�@��;@��#@~v�@u��@o�w@YG�@E�-@;�@1%@-�h@+��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	XB	YB	XB	XB	ffB	��B	�?B	��B	��B
�B
�hB�BE�B<jB�B'�B0!BA�BQ�BN�BI�BE�BB�B>wB<jB:^B9XB7LB6FB/B-B,B,B-B,B+B)�B)�B)�B)�B&�B!�B�B�BhB\BPBB
��B
�B
�sB
�BB
�B
�qB
�3B
��B
z�B
_;B
L�B
:^B
49B
%�B
\B	��B	�mB	ǮB	�FB	��B	�DB	jB	@�B	"�B�HB��B��B�7BZBM�BL�B;dB;dB>wBE�BK�B_;Bo�B~�B�PB�B�LB�#B	B	�B	>wB	W
B	�DB	�3B	��B	�BB	�B	�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	XB	YB	XB	XB	ffB	��B	�?B	��B	��B
�B
�hB�BE�B<jB�B'�B0!BA�BQ�BN�BI�BE�BB�B>wB<jB:^B9XB7LB6FB/B-B,B,B-B,B+B)�B)�B)�B)�B&�B!�B�B�BhB\BPBB
��B
�B
�sB
�BB
�B
�qB
�3B
��B
z�B
_;B
L�B
:^B
49B
%�B
\B	��B	�mB	ǮB	�FB	��B	�DB	jB	@�B	"�B�HB��B��B�7BZBM�BL�B;dB;dB>wBE�BK�B_;Bo�B~�B�PB�B�LB�#B	B	�B	>wB	W
B	�DB	�3B	��B	�BB	�B	�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080929045530  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080929045532  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080929045533  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080929045537  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080929045537  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080929045537  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080929064953                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081003040327  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081003040331  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081003040332  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081003040336  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081003040336  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081003040337  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20081003062452                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081017001922  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081017001922  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081017001926  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081017001926  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081017001927  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081017001927  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081017001927  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20081017015951                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081003040327  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519011042  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519011042  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519011043  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519011043  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519011044  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519011044  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519011044  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519011044  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519011044  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090519011438                      G�O�G�O�G�O�                