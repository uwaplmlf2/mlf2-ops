CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   j   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D<   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G<   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J<   CALIBRATION_DATE            	             
_FillValue                  ,  M<   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Mh   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Ml   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Mp   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Mt   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Mx   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               ^A   JA  20080914111812  20090422061736                                  2B  A   2404                                                            846 @��@\�$1   @��@���@+_;dZ�@aM���1   ARGOS   A   A   A   @�33A33Ad��A���A�ffA���B��B��B2ffBF��BZffBn  B���B�  B�  B���B���B�33B���B�33B�ffB�ffB�  B���B�  CL�C� C��C��C��CffC�3C$��C)� C.��C3L�C833C=�CBL�CG33CQ��C[� CeffCo33CyL�C���C�� C�� C���C��fC��3C��3C��fC�� C���C�� C���C���C�C�� Č�Cр C֌�Cی�C�s3C�fC�s3C�fC���C���D� D�fD� D�3D� D� D�fD$ٚD)� D.�3D3�3D8ٚD=�fDB� DG��DL�3DQٚDVٚD[��D`� De�fDj�fDo�3Dt� Dy�3D�0 D�ffD���D��fD�0 D�ffD�� D��fD�,�D�l�D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���AffA`  A�fgA�  A�fgB��BfgB133BE��BY33Bl��B�33B�ffB�ffB�  B�  B���B�  Bƙ�B���B���B�ffB�33B�ffC  C33CL�CL�CL�C�CffC$L�C)33C.L�C3  C7�fC<��CB  CF�fCQL�C[33Ce�Cn�fCy  C�fgC���C���C��gC�� C���C���C�� C���C�s4C���C�s4C�fgC�fgCǙ�C�fgC�Y�C�fgC�fgC�L�C� C�L�C� C�s4C��gD��D�3D��D� D��D��D�3D$�gD)��D.� D3� D8�gD=�3DB��DG��DL� DQ�gDV�gD[��D`��De�3Dj�3Do� Dt��Dy� D�&fD�\�D��3D���D�&fD�\�D��fD���D�#3D�c3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�!A�A�DA�+A�I�A�bNA�~�A�=qA�A�M�A�VA�S�A�^5A�%A�bA���A�
=A�"�A��#A�K�A�&�A�v�Aڥ�A��
A�VA־wA�;dAԏ\AғuA��A�|�A���AƍPA�K�A�-A�p�A���A��yA�9XA��FA��mA�/A��
A��A��mA�A�p�A�=qAsx�Ai�#Ab��A`�/AW�^AL�A<��A9S�A1�A-�hA$�A!C�A=qA�AC�AAbNAC�Ar�AZAA �!@���@��@�%@���@�  @�@ʟ�@�`B@�5?@�{@���@�{@�@�X@�5?@�S�@��9@�M�@��@��@�(�@���@�E�@��9@�v�@��^@}?}@sC�@f5?@Z�H@T�D@J~�@BJ@<z�@4�@4�j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�!A�A�DA�+A�I�A�bNA�~�A�=qA�A�M�A�VA�S�A�^5A�%A�bA���A�
=A�"�A��#A�K�A�&�A�v�Aڥ�A��
A�VA־wA�;dAԏ\AғuA��A�|�A���AƍPA�K�A�-A�p�A���A��yA�9XA��FA��mA�/A��
A��A��mA�A�p�A�=qAsx�Ai�#Ab��A`�/AW�^AL�A<��A9S�A1�A-�hA$�A!C�A=qA�AC�AAbNAC�Ar�AZAA �!@���@��@�%@���@�  @�@ʟ�@�`B@�5?@�{@���@�{@�@�X@�5?@�S�@��9@�M�@��@��@�(�@���@�E�@��9@�v�@��^@}?}@sC�@f5?@Z�H@T�D@J~�@BJ@<z�@4�@4�j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	{B	{B	{B	�B	0!B	`BB	�TB
8RB
� B
�VB
��B
�B
��B
��B1B{B�B$�B2-B5?B=qBgmBo�B�B��B�LB�;B�;B�B�B�B�B5?B<jB=qB6FB0!B"�B�BPB�B��Bo�BH�B�B
ŢB
�RB
��B
M�B
�B
B	��B	�5B	�B	�B	n�B	N�B	33B	�B	�B	�B	;dB	\)B	YB	YB	`BB	k�B	dZB	^5B	cTB	m�B	t�B	p�B	u�B	�%B	�bB	��B	�-B	�jB	ƨB	��B	�BB	�mB	�B	��B
B
DB
\B
{B
�B
�B
 �B
!�B
)�B
,B
2-B
5?B
:^B
C�B
G�B
J�B
P�B
W
B
[#B
aHB
aH1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B	{B	{B	{B	�B	0!B	`BB	�TB
8RB
� B
�VB
��B
�B
��B
��B1B{B�B$�B2-B5?B=qBgmBo�B�B��B�LB�;B�;B�B�B�B�B5?B<jB=qB6FB0!B"�B�BPB�B��Bo�BH�B�B
ŢB
�RB
��B
M�B
�B
B	��B	�5B	�B	�B	n�B	N�B	33B	�B	�B	�B	;dB	\)B	YB	YB	`BB	k�B	dZB	^5B	cTB	m�B	t�B	p�B	u�B	�%B	�bB	��B	�-B	�jB	ƨB	��B	�BB	�mB	�B	��B
B
DB
\B
{B
�B
�B
 �B
!�B
)�B
,B
2-B
5?B
:^B
C�B
G�B
J�B
P�B
W
B
[#B
aHB
aH1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080914111759  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080914111812  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080914111815  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080914111822  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080914111823  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080914111824  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080914122038                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080918041735  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080918041740  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080918041741  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080918041745  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080918041745  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080918041745  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080918055312                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081009015025  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081009015026  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081009015029  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081009015029  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081009015030  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081009015030  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081009015030  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081009061752                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080918041735  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422061629  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422061629  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090422061629  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422061629  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422061630  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422061630  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422061631  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422061631  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422061631  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422061736                      G�O�G�O�G�O�                