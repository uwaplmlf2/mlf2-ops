CDF   ;   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   e   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  40   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  6,   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8(   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  9�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :$   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ;�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <    PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ?H   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  AD   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C@   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    Cp   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Fp   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Ip   CALIBRATION_DATE            	             
_FillValue                  ,  Lp   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    L�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    L�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    L�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    L�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  L�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    L�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    L�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M    HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    MArgo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               _A   JA  20080925185629  20091211042204                                  2B  A   2404                                                            846 @��ǣEg�1   @���M���@+�dZ�@a�
=p�1   ARGOS   B   B   B   @�ffA��Ak33A���A�  A�33BffBffB0��BE33BY��Bm33B�33B�ffB�ffB�ffB���B�ffB���B���B���B���B���B�33B���C��CffC� G�O�C)�3C.� C3ffC833C=� CBffCG� CQL�C[�fCe33Co��Cy�C���C���C���C�ٚC�� C���C���C�� C�� C�� C���C���C���C¦fCǙ�C̦fC�� C�s3Cی�C�ffC��C�s3C�3C���C���D�fD�fD� D� D��DٚD��D$ٚD)� D.��D3��D8�fD=�3DB�3DG� DL�fDQ�3DV�3D[��D`� De� Dj��Do��DtٚDy��D�,�D�Y�D��fD��3D�&fD�ffD���D��3D�#3D�p 11111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111111111111   @���A  AfffA�fgA���A���B33B33B/��BD  BXfgBl  B���B���B���B���B�  B���B�33B�33B�33B�33B�33BB�33CL�C�C33G�O�C)ffC.33C3�C7�fC=33CB�CG33CQ  C[��Cd�fCoL�Cx��C�s4C�s4C�s4C��4C���C�fgC�s4C�Y�C���C���C�fgC��gC��gC C�s4C̀ Cљ�C�L�C�fgC�@ C�fgC�L�C��C�s4C��gD�3D�3D��D��D��D�gD��D$�gD)��D.��D3��D8�3D=� DB� DG��DL�3DQ� DV� D[�gD`��De��Dj�gDo��Dt�gDy��D�#3D�P D���D�ٙD��D�\�D�� D�əD��D�ff11111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A��HA���A���A��
A��A��A�1A��A��/A�"�A��A�O�A���A���A�+A�hsA�\A���A��A�&�A�1A�t�A�1'AܮAړuA��
G�O�A�dZA���AǮA�t�A�M�A���A�1'A��jA�"�A��
A�$�A�dZA�z�A�-A�C�A�I�A���Aq+Am�
Ad��A[�wAR�9AK�7A=S�A7��A0��A'��A#dZA 9XA-A-AbA�A`BA	��A�A�A ��@�F@��H@�@؃@���@���@��H@� �@�ff@�ff@�7L@��@��7@�7L@���@�{@���@��@�b@��
@���@�/@��-@�K�@�K�@��@xbN@p  @f{@\��@R�@KC�@E�-@=`B@:~�11111111111111111111111111194111111111111111111111111111111111111111111111111111111111111111111111111   A���A��HA���A���A��
A��A��A�1A��A��/A�"�A��A�O�A���A���A�+A�hsA�\A���A��A�&�A�1A�t�A�1'AܮAړuA��
G�O�A�dZA���AǮA�t�A�M�A���A�1'A��jA�"�A��
A�$�A�dZA�z�A�-A�C�A�I�A���Aq+Am�
Ad��A[�wAR�9AK�7A=S�A7��A0��A'��A#dZA 9XA-A-AbA�A`BA	��A�A�A ��@�F@��H@�@؃@���@���@��H@� �@�ff@�ff@�7L@��@��7@�7L@���@�{@���@��@�b@��
@���@�/@��-@�K�@�K�@��@xbN@p  @f{@\��@R�@KC�@E�-@=`B@:~�11111111111111111111111111194111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B�B��B��B��B�#B�B�HB	��B
�B
=qB
'�B
bNB
_;B
u�B
�JB
��B
��B
�`B\B�B �B49B=qBdZB�=B��B�FG�O�B�B9XB?}B?}B2-B�B+B�BȴBu�BL�B(�B�B
��B
ÖB
�+B
A�B
.B
+B	�B	��B	�B	z�B	e`B	A�B	,B	/B	49B	VB	R�B	YB	`BB	`BB	e`B	m�B	o�B	p�B	e`B	{�B	�%B	��B	��B	�B	�XB	�^B	ŢB	��B	�B	�ZB	�B	�B	��B
B
+B
JB
VB
�B
�B
 �B
$�B
)�B
/B
49B
8RB
>wB
A�B
F�B
K�B
P�B
S�B
YB
]/11111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111111111111   B��B�B��B��B��B�#B�B�HB	��B
�B
=qB
'�B
bNB
_;B
u�B
�JB
��B
��B
�`B\B�B �B49B=qBdZB�=B��B�FG�O�B�B9XB?}B?}B2-B�B+B�BȴBu�BL�B(�B�B
��B
ÖB
�+B
A�B
.B
+B	�B	��B	�B	z�B	e`B	A�B	,B	/B	49B	VB	R�B	YB	`BB	`BB	e`B	m�B	o�B	p�B	e`B	{�B	�%B	��B	��B	�B	�XB	�^B	ŢB	��B	�B	�ZB	�B	�B	��B
B
+B
JB
VB
�B
�B
 �B
$�B
)�B
/B
49B
8RB
>wB
A�B
F�B
K�B
P�B
S�B
YB
]/11111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080925185627  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080925185629  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080925185630  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080925185635  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080925185635  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080925185635  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080925185635  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20080925185635  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080925191540                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080928164456  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080928164512  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080928164516  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080928164527  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080928164527  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080928164527  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080928164527  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20080928164529  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20080928164529  CV  TIME            G�O�G�O�F��Z                JA  ARGQrelo2.1                                                                 20080928164529  CV  LAT$            G�O�G�O�A]�#                JA  ARGQrelo2.1                                                                 20080928164529  CV  LON$            G�O�G�O�C]�                JA  ARUP                                                                        20080928190956                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081009015030  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081009015031  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081009015035  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081009015035  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081009015035  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081009015035  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081009015035  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20081009015035  QCF$                G�O�G�O�G�O�            4000JA  ARGQaqcp2.8a                                                                20081009015035  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081009015035  QCF$                G�O�G�O�G�O�              40JA  ARGQrqcpt16b                                                                20081009015035  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081009061825                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080928164456  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422061631  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422061631  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090422061632  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422061632  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422061633  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090422061633  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422061633  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422061633  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422061633  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422061633  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422061633  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422061736                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091211000929  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091211000946  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091211000946  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091211000947  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091211000947  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091211000950  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091211000950  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091211000950  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091211000950  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091211000950  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091211000950  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091211000950  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091211042204                      G�O�G�O�G�O�                