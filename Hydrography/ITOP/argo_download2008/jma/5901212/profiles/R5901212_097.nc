CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   i   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J   CALIBRATION_DATE            	             
_FillValue                  ,  M   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    MD   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    MH   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    ML   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    MP   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  MT   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               aA   JA  20081014125539  20090422061737                                  2B  A   2404                                                            846 @��ʝd��1   @����	{B@,C��%@afffff1   ARGOS   A   A   A   @���AffA`  A���A���A�  B��B  B1��BDffBY33Bo33B�ffB���B�ffB���B�ffB���B�33B�33B�33B�33B�ffB�ffB�33CffCffC33C�C  C33C��C$L�C)33C.L�C3� C8��C=�3CB��CG33CQ33C[33Ce� CoffCy�C��3C��3C���C���C�s3C���C���C���C���C���C��3C�� C���C³3CǦfC�� Cљ�Cֳ3Cۙ�C�3C� CꙚC��C�� C��3D� D� DٚDٚD�fD��D��D$� D)�3D.� D3��D8�3D=� DB��DG�fDL�3DQ��DVٚD[� D`��De��Dj�3Do��Dt�3Dy��D�#3D�p D��3D�� D�&fD�ffD���D�ٚD��D��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @���A��A^ffA���A�  A�33BfgB��B134BD  BX��Bn��B�33B�fgB�33B���B�33B���B�  B�  B�  B�  B�33B�33B�  CL�CL�C�C  C�fC�C� C$33C)�C.33C3ffC8�3C=��CB� CG�CQ�C[�CeffCoL�Cy  C��fC��fC���C���C�ffC�� C�� C�� C�� C���C��fC�s3C�� C¦fCǙ�C̳3Cь�C֦fCی�C�fC�s3C��C� C��3C��fDٚD��D�4D�4D� D�gD�gD$ٚD)��D.ٚD3�gD8��D=ٚDB�gDG� DL��DQ�gDV�4D[��D`�gDe�4Dj��Do�gDt��Dy�gD�  D�l�D�� D���D�#3D�c3D���D��gD��D�	�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ĜA���A�hA�x�A�r�A�l�A�ffA�^5A�ffA�bNA�n�A�PA�+A뙚A�jA�v�A�33A��`A�"�A�C�A��TA�
=A�x�A�9XA�ƨA��`A���A��AʬAɩ�A�`BA�1A�Q�A��/A�Q�A��yA�(�A��hA�=qA�~�A���A���A�ĜA��A���A���A�%A�%A���A|I�AshsAn^5Ai?}Adr�AW�AJ$�AA�A:JA6M�A.v�A(=qA$M�A   AM�A%A%A  A��A	�FA�h@��@���@��m@�^5@ڇ+@��@ʸR@�=q@�{@���@�/@���@��\@�M�@��@�M�@�G�@��\@�Ĝ@��+@��@���@�O�@���@��@��@|�@r�\@i&�@\�j@S�m@K�F@B-@;o@8A�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�ĜA���A�hA�x�A�r�A�l�A�ffA�^5A�ffA�bNA�n�A�PA�+A뙚A�jA�v�A�33A��`A�"�A�C�A��TA�
=A�x�A�9XA�ƨA��`A���A��AʬAɩ�A�`BA�1A�Q�A��/A�Q�A��yA�(�A��hA�=qA�~�A���A���A�ĜA��A���A���A�%A�%A���A|I�AshsAn^5Ai?}Adr�AW�AJ$�AA�A:JA6M�A.v�A(=qA$M�A   AM�A%A%A  A��A	�FA�h@��@���@��m@�^5@ڇ+@��@ʸR@�=q@�{@���@�/@���@��\@�M�@��@�M�@�G�@��\@�Ĝ@��+@��@���@�O�@���@��@��@|�@r�\@i&�@\�j@S�m@K�F@B-@;o@8A�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�+B	�1B	�%B	�%B	�%B	�+B	�+B	�%B	�=B	�JB	�oB	ĜB
8RB
z�B
�wB
�/B
��B  B%B\B�B;dBE�B��B�qB�B�BBDBoB�B�B+B-B33B/B$�BoB+B��BɺB��Bo�BJ�B'�BPB
��B
��B
��B
� B
N�B
6FB
�B
B	��B	�B	��B	�hB	�+B	e`B	e`B	ffB	n�B	gmB	o�B	p�B	q�B	t�B	r�B	dZB	hsB	p�B	w�B	l�B	u�B	�1B	��B	�B	�wB	ÖB	��B	�;B	�`B	�B	��B
B
B
DB
VB
oB
�B
�B
�B
#�B
'�B
49B
7LB
9XB
>wB
D�B
H�B
L�B
R�B
XB
\)111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	�+B	�1B	�%B	�%B	�%B	�+B	�+B	�%B	�=B	�JB	�oB	ĜB
8RB
z�B
�wB
�/B
��B  B%B\B�B;dBE�B��B�qB�B�BBDBoB�B�B+B-B33B/B$�BoB+B��BɺB��Bo�BJ�B'�BPB
��B
��B
��B
� B
N�B
6FB
�B
B	��B	�B	��B	�hB	�+B	e`B	e`B	ffB	n�B	gmB	o�B	p�B	q�B	t�B	r�B	dZB	hsB	p�B	w�B	l�B	u�B	�1B	��B	�B	�wB	ÖB	��B	�;B	�`B	�B	��B
B
B
DB
VB
oB
�B
�B
�B
#�B
'�B
49B
7LB
9XB
>wB
D�B
H�B
L�B
R�B
XB
\)111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081014125536  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081014125539  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081014125540  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081014125540  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081014125544  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081014125544  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081014125544  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081014125544  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081014125545  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081014130941                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081018163236  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081018163253  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081018163255  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081018163257  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081018163306  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081018163306  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081018163306  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081018163306  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081018163307  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20081018163307  CV  TIME            G�O�G�O�F��W                JA  ARGQrelo2.1                                                                 20081018163307  CV  LAT$            G�O�G�O�A`Z                JA  ARGQrelo2.1                                                                 20081018163307  CV  LON$            G�O�G�O�C2�                JA  ARUP                                                                        20081018203638                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081018163236  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422061636  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422061636  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090422061636  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422061637  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422061638  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422061638  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422061638  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422061638  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422061638  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422061737                      G�O�G�O�G�O�                