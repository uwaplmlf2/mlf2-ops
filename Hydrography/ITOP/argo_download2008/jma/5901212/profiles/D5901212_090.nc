CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   i   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  C�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Dx   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Mx   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Vx   CALIBRATION_DATE      	   
                
_FillValue                  �  _x   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    _�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    _�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `    HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    `H   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    `X   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    `\   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         `l   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         `p   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        `t   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    `xArgo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ZA   JA  20080805125521  20081226021528  A9_60134_090                    2C  D   APEX-SBE 2404                                                   846 @��Hp�b�1   @��L5��@*s�E���@a���o1   ARGOS   A   A   A   @�33A��Aa��A���A���A���BffB��B2  BDffBX��BnffB�  B�33B���B�ffB�ffB�  B�  Bƙ�BЙ�B�  B䙚B�ffB�  C33C��CffC� C��CffC��C$33C)ffC.ffC3L�C8ffC=� CB33CG� CQ��C[33Ce� Co�Cy33C��fC�� C�� C��3C���C�� C��fC���C�� C�ٚC��fC��fC���C�� CǙ�C̳3Cѳ3C�ffCۦfC�ffC�3C�� C�3C�� C�� D�3D��D��D��D� D�3D��D$�fD)� D.�fD3ٚD8�3D=�fDB��DG��DL�fDQ��DV� D[ٚD`�fDe� Dj�fDo�fDt� Dy��D��D�ffD�� D���D�,�D�ffD��3D��fD�  D��3111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�  A  A`  A���A�  A�  B  B33B1��BD  BXffBn  B���B�  B���B�33B�33B���B���B�ffB�ffB���B�ffB�33B���C�C� CL�CffC� CL�C� C$�C)L�C.L�C333C8L�C=ffCB�CGffCQ� C[�CeffCo  Cy�C���C��3C��3C��fC���C��3C���C���C�s3C���C���C���C�� C³3Cǌ�C̦fCѦfC�Y�Cۙ�C�Y�C�fC�3C�fC��3C��3D��D�3D�fD�3D��D��D�fD$� D)��D.� D3�3D8��D=� DB�fDG�fDL� DQ�fDV��D[�3D`� DeٚDj� Do� Dt��Dy�fD�fD�c3D���D��D�)�D�c3D�� D��3D��D�� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�&�A��A�JA�`BA��A��A��A�A�+A쟾A�\A��HA���A�oA�t�A�\A���A�ȴA���A� �A�x�A݇+A܍PAۑhAڑhA���A�ȴA��;A���A��A�hsA���Aĺ^A��A�v�A�/A�ȴA��A���A�dZA�  A�oA���A�`BA�~�A��PA�1'A~�AyO�Ar��Agp�A[��AM�A@$�A9ƨA3�A-��A%�;A%A!�AS�AffA��A��A"�A�A
��AbNA��A�A�m@���@��T@ܬ@�"�@�V@˥�@�dZ@�t�@�+@�o@���@��;@�@�r�@�Z@�v�@���@�I�@���@�t�@�C�@��T@��j@�p�@�E�@��m@y��@nȴ@a�7@Vv�@L�j@G�@BJ@9�7111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�&�A��A�JA�`BA��A��A��A�A�+A쟾A�\A��HA���A�oA�t�A�\A���A�ȴA���A� �A�x�A݇+A܍PAۑhAڑhA���A�ȴA��;A���A��A�hsA���Aĺ^A��A�v�A�/A�ȴA��A���A�dZA�  A�oA���A�`BA�~�A��PA�1'A~�AyO�Ar��Agp�A[��AM�A@$�A9ƨA3�A-��A%�;A%A!�AS�AffA��A��A"�A�A
��AbNA��A�A�m@���@��T@ܬ@�"�@�V@˥�@�dZ@�t�@�+@�o@���@��;@�@�r�@�Z@�v�@���@�I�@���@�t�@�C�@��T@��j@�p�@�E�@��m@y��@nȴ@a�7@Vv�@L�j@G�@BJ@9�7111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBbNB�hB�FB�B	1B		7B	JB	bB	DB	�B	�B	�B	�HB	��B
�B
gmB
{�B
�uB
�wB
B
�BB\B�B1'BG�Bm�B�B��B�/B{B"�BE�BI�BP�BJ�BE�B.BJB��B�3B�BgmBC�B$�B
��B
�^B
�uB
p�B
K�B
PB	�;B	��B	�B	t�B	q�B	o�B	s�B	t�B	q�B	_;B	|�B	r�B	ffB	cTB	ffB	hsB	jB	�\B	��B	��B	��B	��B	�PB	�VB	��B	��B	�B	�dB	��B	��B	��B	�`B	�NB	�B	��B
  B	��B
%B
hB
�B
�B
�B
�B
&�B
(�B
1'B
9XB
<jB
E�B
J�B
K�B
S�B
W
B
Y111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   BbNB�hB�FB�B	1B		7B	JB	bB	DB	�B	�B	�%B	�NB	��B
�B
hsB
|�B
�{B
�}B
ÖB
�BBbB�B2-BI�Bn�B�%B��B�BB�B)�BG�BM�BP�BN�BI�B33BVB  B�LB�+BiyBE�B%�BB
�jB
�{B
r�B
N�B
bB	�TB	��B	�B	v�B	s�B	q�B	s�B	u�B	s�B	`BB	}�B	s�B	gmB	cTB	ffB	iyB	k�B	�\B	��B	��B	��B	��B	�PB	�VB	��B	��B	�B	�dB	��B	��B	��B	�`B	�NB	�B	��B
  B	��B
%B
hB
�B
�B
�B
�B
&�B
(�B
1'B
9XB
<jB
E�B
J�B
K�B
S�B
W
B
Y111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSP                                                                                                                                                                                                                                TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                  None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                           None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            NextCycleSP=0.1                                                                                                                                                                                                                                                 None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : max(CTM adjustment , 0.01(PSS-78))                                                                                                                                                                   None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808181432212008081814322120080818143221200808181447532008081814475320080818144753200812240000002008122400000020081224000000  JA  ARFMdecpA9_b                                                                20080805125519  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080805125521  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080805125522  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080805125526  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080805125526  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080805125527  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080805130731                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080809162828  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080809162844  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080809162847  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080809162856  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080809162857  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080809162858  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080809203446                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081009015005  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081009015005  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081009015009  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081009015009  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081009015009  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081009015009  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081009015010  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081009061538                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080818143221  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080818143221  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080818144753  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.0 SeHyD1.0                                                        20081224000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081226015317  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081226021528                      G�O�G�O�G�O�                