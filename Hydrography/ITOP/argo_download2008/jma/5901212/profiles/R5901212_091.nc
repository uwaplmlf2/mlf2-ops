CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   j   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D<   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G<   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J<   CALIBRATION_DATE            	             
_FillValue                  ,  M<   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Mh   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Ml   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Mp   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Mt   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Mx   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               [A   JA  20080815185709  20090422061735                                  2B  A   2404                                                            846 @���N ��1   @����d�
@*���S��@a-V1   ARGOS   A   A   A   @���A33AfffA�  A�  A陚B
ffB33B2  BD  BX  BlffB�  B�  B�33B���B�33B�ffB�ffBƙ�B���B�ffB�ffBB�33CffC33CL�CL�C��C� C� C$ffC)� C.ffC3�3C8�C=ffCB  CG��CQffC[L�Ce�Cn�fCyffC���C��3C��fC��3C�� C���C��3C���C��fC�� C���C���C��fC³3Cǳ3C̳3Cр C֌�Cی�C�s3C�s3C�fC�s3C��3C��3DٚD�fD�fD�fD� DٚD��D$��D)ٚD.ٚD3ٚD8��D=�fDB�3DG��DL� DQ��DVٚD[��D`�fDe��Dj�3DoٚDtٚDy�3D�)�D�l�D���D��3D�#3D�ffD��fD��3D�fD�l�D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�fgA��Ad��A�33A�33A���B
  B��B1��BC��BW��Bl  B���B���B�  B�fgB�  B�33B�33B�fgBϙ�B�33B�33B�fgB�  CL�C�C33C33C� CffCffC$L�C)ffC.L�C3��C8  C=L�CA�fCG� CQL�C[33Ce  Cn��CyL�C���C��fC���C��fC��3C�� C��fC���C���C�s3C�� C�� C���C¦fCǦfC̦fC�s3Cր Cۀ C�ffC�ffCꙙC�ffC��fC��fD�4D� D� D� D��D�4D�gD$�4D)�4D.�4D3�4D8�gD=� DB��DG�4DL��DQ�gDV�4D[�gD`� De�gDj��Do�4Dt�4Dy��D�&gD�i�D���D�� D�  D�c3D��3D�� D�3D�i�D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�"�A�&�A�bA�
=A�
=A�
=A�bA흲A�|�A�A��A��A�=qA���A�$�A�hA�n�A�oA�dZA���AދDA��A�?}A�I�A�^5A�hsA�VAֶFA�^5A�hsA�bA� �A��A��A�K�A�/A���A���A��A�1'A�\)A�r�A�A�A�ffA�dZA���A���A�VA{VAs��Ai�Aa33AW�
AP��AF�ACA7oA.ĜA*��A$ZA�Ar�A��A�#A	�A�
AM�A�A��A�+@�b@��@�%@��@Ͼw@ǅ@���@��^@�/@�j@�-@��@�+@�@��P@�=q@�?}@���@�(�@��-@�?}@���@�@��y@�bN@�(�@|1@s��@j=q@]�h@Sƨ@K�m@DZ@=@8�9@8Q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�"�A�&�A�bA�
=A�
=A�
=A�bA흲A�|�A�A��A��A�=qA���A�$�A�hA�n�A�oA�dZA���AދDA��A�?}A�I�A�^5A�hsA�VAֶFA�^5A�hsA�bA� �A��A��A�K�A�/A���A���A��A�1'A�\)A�r�A�A�A�ffA�dZA���A���A�VA{VAs��Ai�Aa33AW�
AP��AF�ACA7oA.ĜA*��A$ZA�Ar�A��A�#A	�A�
AM�A�A��A�+@�b@��@�%@��@Ͼw@ǅ@���@��^@�/@�j@�-@��@�+@�@��P@�=q@�?}@���@�(�@��-@�?}@���@�@��y@�bN@�(�@|1@s��@j=q@]�h@Sƨ@K�m@DZ@=@8�9@8Q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�
B�B�B�B�B�B�#B	Q�B	{�B	�HB	��B
6FB
l�B
�1B
ǮB
ǮB
�mBoB�B"�B.B1'B8RB<jBR�BW
Bn�B� B�B�dB��B�B/B7LB1'B&�B0!B/B6FB2-B�B��B�RB�bBH�B
�B
�XB
�PB
t�B
XB
%�B	��B	ŢB	��B	�B	q�B	ZB	M�B	aHB	e`B	N�B	P�B	YB	l�B	cTB	ffB	ffB	�1B	�oB	��B	��B	�DB	�7B	�1B	�hB	��B	�qB	��B	�/B	�B	��B
B	��B
+B
B
PB
hB
!�B
$�B
&�B
#�B
$�B
'�B
&�B
)�B
.B
8RB
=qB
A�B
G�B
K�B
P�B
T�B
ZB
^5B
aH1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B�
B�B�B�B�B�B�#B	Q�B	{�B	�HB	��B
6FB
l�B
�1B
ǮB
ǮB
�mBoB�B"�B.B1'B8RB<jBR�BW
Bn�B� B�B�dB��B�B/B7LB1'B&�B0!B/B6FB2-B�B��B�RB�bBH�B
�B
�XB
�PB
t�B
XB
%�B	��B	ŢB	��B	�B	q�B	ZB	M�B	aHB	e`B	N�B	P�B	YB	l�B	cTB	ffB	ffB	�1B	�oB	��B	��B	�DB	�7B	�1B	�hB	��B	�qB	��B	�/B	�B	��B
B	��B
+B
B
PB
hB
!�B
$�B
&�B
#�B
$�B
'�B
&�B
)�B
.B
8RB
=qB
A�B
G�B
K�B
P�B
T�B
ZB
^5B
aH1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080815185706  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080815185709  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080815185709  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080815185713  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080815185714  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080815185714  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080815191326                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080819042513  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080819042518  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080819042519  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080819042523  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080819042523  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080819042523  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080819054043                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081009015010  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081009015010  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081009015014  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081009015014  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081009015014  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081009015014  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081009015015  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081009061611                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080819042513  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422061622  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422061622  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090422061622  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422061623  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422061624  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422061624  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422061624  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422061624  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422061624  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422061735                      G�O�G�O�G�O�                