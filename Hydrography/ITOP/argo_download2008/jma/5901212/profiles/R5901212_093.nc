CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   i   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J   CALIBRATION_DATE            	             
_FillValue                  ,  M   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    MD   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    MH   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    ML   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    MP   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  MT   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               ]A   JA  20080904125541  20090422061734                                  2B  A   2404                                                            846 @���YP�@1   @����P�@@+>��"��@a�"��`1   ARGOS   A   A   A   @�ffA  A`  A���A�ffA�  B  B  B0ffBDffBX��Bn  B���B�ffB�ffB���B�  B���B�  B�  B�33B�  B�33B�  B�  CL�C��CffC� CL�C��C�3C$ffC)33C.L�C3  C8��C=ffCBL�CG33CQ33C[�Ce��CoL�Cy�C��3C��fC��fC��3C�� C���C���C��3C��3C���C���C���C���C�CǦfC�� Cр C֌�C�� C�� C��C�fC�� C�� C�� DٚD� D��D�fD�3D�3D��D$ٚD)ٚD.� D3�fD8�3D=� DBٚDG�fDLٚDQ� DV� D[�fD`��De�fDj��Do�3Dt�fDy�fD�&fD�` D��fD�� D�)�D�ffD���D��fD��D�� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�  A��A\��A�33A���A�ffB33B33B/��BC��BX  Bm33B�fgB�  B�  B�34B���B�34B���Bƙ�B���Bڙ�B���B홚B���C�CfgC33CL�C�CfgC� C$33C)  C.�C2��C8��C=33CB�CG  CQ  CZ�gCefgCo�Cx�gC���C���C���C���C��fC�� C�s3C���C���C�s3C�s3C��3C��3C Cǌ�C̦fC�ffC�s3CۦfC�ffC�s3C��C�fC��fC��fD��D�3D� D��D�fD�fD� D$��D)��D.�3D3��D8�fD=�3DB��DG��DL��DQ�3DV�3D[��D`� De��Dj��Do�fDt��Dy��D�  D�Y�D�� D�ٚD�#4D�` D��4D�� D�4D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�7LA���A�^A��A�7A���A�G�A�  A�ȴA�hA�1A�{A���A��A�DA��A�+A�`BA�jA�7LA�`BA�-A�+A�1'AۑhA�VA�+A�p�A��mA�O�A�Q�A�&�A��TA��#A��jA���A��A���A��mA�?}A���A�A��A��A���A�A��uAdZAsS�Ak�wA`��AY�hARȴAH�uAF�`A;7LA7x�A-�A(5?A#�AdZA9XA�A-A
1'A	S�AȴA(�AhsA J@�x�@�^@� �@�^5@���@υ@��H@�+@��H@�`B@���@�I�@�x�@�V@� �@�@��T@��!@�Z@�=q@��m@�G�@��@�ƨ@��^@���@}p�@t�@ix�@aX@Yhs@P�9@H��@@��@<�D111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�7LA���A�^A��A�7A���A�G�A�  A�ȴA�hA�1A�{A���A��A�DA��A�+A�`BA�jA�7LA�`BA�-A�+A�1'AۑhA�VA�+A�p�A��mA�O�A�Q�A�&�A��TA��#A��jA���A��A���A��mA�?}A���A�A��A��A���A�A��uAdZAsS�Ak�wA`��AY�hARȴAH�uAF�`A;7LA7x�A-�A(5?A#�AdZA9XA�A-A
1'A	S�AȴA(�AhsA J@�x�@�^@� �@�^5@���@υ@��H@�+@��H@�`B@���@�I�@�x�@�V@� �@�@��T@��!@�Z@�=q@��m@�G�@��@�ƨ@��^@���@}p�@t�@ix�@aX@Yhs@P�9@H��@@��@<�D111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
�B
�B
�B
�B
)�B
-B
,B
5?B
5?B
E�B
��B
��B
�)B�B�B2-B49BR�B^5B`BBiyBcTBp�Bm�B�B��B�\B�qB�}B��B�B�B.B2-B2-B&�B�BoB1B��B�qB�+B?}B �B
��B
�FB
�B
J�B
"�B	��B	�TB	��B	��B	��B	z�B	bNB	,B	{B	�B	VB	
=B	%B	'�B	#�B	YB	[#B	^5B	VB	k�B	y�B	}�B	�B	�%B	�hB	��B	��B	�XB	ɺB	�B	�NB	�yB	�B	��B	��B
B
	7B
hB
�B
�B
�B
!�B
'�B
)�B
(�B
/B
6FB
=qB
A�B
E�B
F�B
L�B
R�B
YB
\)111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B
�B
�B
�B
�B
�B
)�B
-B
,B
5?B
5?B
E�B
��B
��B
�)B�B�B2-B49BR�B^5B`BBiyBcTBp�Bm�B�B��B�\B�qB�}B��B�B�B.B2-B2-B&�B�BoB1B��B�qB�+B?}B �B
��B
�FB
�B
J�B
"�B	��B	�TB	��B	��B	��B	z�B	bNB	,B	{B	�B	VB	
=B	%B	'�B	#�B	YB	[#B	^5B	VB	k�B	y�B	}�B	�B	�%B	�hB	��B	��B	�XB	ɺB	�B	�NB	�yB	�B	��B	��B
B
	7B
hB
�B
�B
�B
!�B
'�B
)�B
(�B
/B
6FB
=qB
A�B
E�B
F�B
L�B
R�B
YB
\)111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080904125538  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080904125541  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080904125542  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080904125546  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080904125546  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080904125546  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080904130730                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080908160442  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080908160447  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080908160448  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080908160452  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080908160452  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080908160452  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080908190955                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081009015020  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081009015020  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081009015024  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081009015024  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081009015025  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081009015025  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081009015025  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081009061718                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080908160442  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422061627  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422061627  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090422061627  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422061627  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422061628  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422061628  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422061628  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422061628  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422061628  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422061734                      G�O�G�O�G�O�                