CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   j   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D<   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G<   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J<   CALIBRATION_DATE            	             
_FillValue                  ,  M<   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Mh   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Ml   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Mp   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Mt   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Mx   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               `A   JA  20081004085532  20090422061737                                  2B  A   2404                                                            846 @��?�7�I1   @��?�ٱ�@+ۥ�S��@a��R1   ARGOS   A   A   A   @�  A  Ad��A�ffA�ffA陚B	33B33B2ffBE��BY��Bm��B���B�ffB���B�33B�ffB�ffB�  B�  B�33B�  B�ffB�  B�ffC� CL�C��C  C33C�3C��C$ffC)33C.� C333C8�C=� CBffCG��CQ� C[ffCeffCo�Cx�fC�� C�� C�� C���C���C��fC�� C�s3C���C���C���C��fC�� C³3CǦfČ�Cь�C֌�Cۙ�C�� C�fC�fC��C���C�� D�3DٚDٚD�3DٚD��D��D$�3D)ٚD.�3D3�3D8�fD=��DBٚDG�3DL� DQ��DV�3D[� D`� De�fDj� Do��Dt�fDy�fD�,�D�\�D��fD���D�,�D�c3D���D���D�)�D�i�D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���A��Aa��A���A���A�  BffBffB1��BD��BX��Bl��B�fgB�  B�34B���B�  B�  B���Bƙ�B���Bڙ�B�  BB�  CL�C�CfgC��C  C� CfgC$33C)  C.L�C3  C7�gC=L�CB33CGfgCQL�C[33Ce33Cn�gCx�3C�ffC��fC��fC��3C��3C���C��fC�Y�C�� C�� C�s3C���C�ffC�Cǌ�C�s3C�s3C�s3Cۀ C�ffC��C��C�s3C� C�ffD�fD��D��D�fD��D��D� D$�fD)��D.�fD3�fD8��D=� DB��DG�fDL�3DQ� DV�fD[�3D`�3De��Dj�3Do��Dt��DyٙD�&gD�VgD�� D��gD�&gD�\�D��gD��gD�#4D�c4D��41111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�~�A�PA�+A�PA�+A�DA�DA�7A�C�A�1A�
=A���A�1A�;dA�7A�v�A�(�A�ffA���A�~�A�ȴA�G�A��
AٍPA׼jA��A��A�`BA̋DA��#A���A�bA�{A� �A���A��
A��hA�JA��hA� �A�^5A� �A��/A���A�1'A��A�p�Az�AnjAgt�A`�jAU�AP��AJ�AD�RA8�`A.ȴA++A"E�AĜA(�A=qA�A|�A	��A��A1AQ�@�+@�w@�@�dZ@���@���@��H@��@��/@��y@�C�@��@�ƨ@�n�@�(�@�Q�@�
=@��#@�"�@�r�@���@�K�@��u@�~�@�;d@�V@�{@�+@}�@p�9@_�;@Xb@S�@M`B@D��@?
=@9��@9�^1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�~�A�PA�+A�PA�+A�DA�DA�7A�C�A�1A�
=A���A�1A�;dA�7A�v�A�(�A�ffA���A�~�A�ȴA�G�A��
AٍPA׼jA��A��A�`BA̋DA��#A���A�bA�{A� �A���A��
A��hA�JA��hA� �A�^5A� �A��/A���A�1'A��A�p�Az�AnjAgt�A`�jAU�AP��AJ�AD�RA8�`A.ȴA++A"E�AĜA(�A=qA�A|�A	��A��A1AQ�@�+@�w@�@�dZ@���@���@��H@��@��/@��y@�C�@��@�ƨ@�n�@�(�@�Q�@�
=@��#@�"�@�r�@���@�K�@��u@�~�@�;d@�V@�{@�+@}�@p�9@_�;@Xb@S�@M`B@D��@?
=@9��@9�^1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�B	�B	�B	�B	�B	�B	�B	�B	{B	!�B	(�B	)�B	2-B	cTB	��B
�uB
B
�;B
�B�B�BB�BA�B@�BA�B��BBB�;B�`B)�B2-B+B'�B!�B�BuB	7B�B�NB�wB~�B<jB-B
��B
��B
��B
o�B
2-B
bB	��B	��B	ȴB	�XB	��B	m�B	I�B	F�B	H�B	]/B	ffB	ffB	e`B	v�B	l�B	dZB	ZB	O�B	5?B	-B	L�B	VB	z�B	�PB	��B	��B	�FB	��B	��B	�B	�BB	�B	��B
B
B
B

=B
\B
oB
�B
�B
�B
!�B
#�B
(�B
1'B
7LB
;dB
B�B
H�B
K�B
N�B
T�B
ZB
]/B
]/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B	�B	�B	�B	�B	�B	�B	�B	�B	{B	!�B	(�B	)�B	2-B	cTB	��B
�uB
B
�;B
�B�B�BB�BA�B@�BA�B��BBB�;B�`B)�B2-B+B'�B!�B�BuB	7B�B�NB�wB~�B<jB-B
��B
��B
��B
o�B
2-B
bB	��B	��B	ȴB	�XB	��B	m�B	I�B	F�B	H�B	]/B	ffB	ffB	e`B	v�B	l�B	dZB	ZB	O�B	5?B	-B	L�B	VB	z�B	�PB	��B	��B	�FB	��B	��B	�B	�BB	�B	��B
B
B
B

=B
\B
oB
�B
�B
�B
!�B
#�B
(�B
1'B
7LB
;dB
B�B
H�B
K�B
N�B
T�B
ZB
]/B
]/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081004085529  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081004085532  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081004085533  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081004085537  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081004085537  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081004085537  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081004090840                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081008044004  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081008044009  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081008044010  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081008044014  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081008044014  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081008044014  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081008061847                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081009015035  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081009015036  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081009015040  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081009015040  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081009015040  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081009015040  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081009015040  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081009061859                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081008044004  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422061634  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422061634  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090422061634  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422061634  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422061635  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422061635  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422061635  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422061635  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422061636  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422061737                      G�O�G�O�G�O�                