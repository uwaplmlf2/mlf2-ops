CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   C   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  4�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5<   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6H   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  7T   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  ;    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;D   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  <P   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       <�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  =�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C�   CALIBRATION_DATE            	             
_FillValue                  ,  F�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    F�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G    HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    GL   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G\   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G`   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Gp   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Gt   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Gx   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G|Argo profile    2.2 1.2 19500101000000  5900657 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               �A   JA  20081001084944  20090520062317                                  2B  A   937                                                             846 @���1   @���β@*����F@`���+1   ARGOS   A   A   A   @�ffA��A�ffA�ffB  BF��BnffB�33B���B���B�33Bڙ�B�  CffC��CL�CffC)� C3�3C=ffCG� C[��Co  C���C���C��3C�� C���C�ffC���Cǳ3CѦfC�s3C�fC��C���DٚDٚD�fD��D�fDٚD�fD$� D)� D.�fD3��D8��D=��DB��DG��DN3DTS3DZ�3D`� Df��DmS3Ds�3Dy�fD�#3D�i�D���D��fD�0 D�i�D��3D���1111111111111111111111111111111111111111111111111111111111111111111 @�33A33A���A噙B��BFfgBn  B�  B���B�fgB�  B�fgB���CL�C� C33CL�C)ffC3��C=L�CGffC[� Cn�fC�� C���C��fC�s3C���C�Y�C�� CǦfCљ�C�ffC噙C� C�� D�4D�4D� D�gD� D�4D� D$��D)ٚD.� D3�4D8�gD=�gDB�gDG�gDN�DTL�DZ��D`ٚDf�4DmL�Ds��Dy� D�  D�fgD���D��3D�,�D�fgD�� D��g1111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��`A�A�A�A�9XA�7LA�7LA���A�n�A�PA�9XA�A�A�oA�9XA�ȴAʲ-A���A�ffA��-A���A�dZA��A���A��;A��FAt�DAd�RAV�DAKO�A?�^A0VA%�TA-A�`A�A�A�h@���@�{@�bN@���@�^5@���@�S�@��F@�K�@��@���@�~�@���@���@�|�@�M�@��@�I�@��^@�O�@��@�"�@��@�ff@x��@m�h@a�@[�F@Q%@B��@=1111111111111111111111111111111111111111111111111111111111111111111 A��`A�A�A�A�9XA�7LA�7LA���A�n�A�PA�9XA�A�A�oA�9XA�ȴAʲ-A���A�ffA��-A���A�dZA��A���A��;A��FAt�DAd�RAV�DAKO�A?�^A0VA%�TA-A�`A�A�A�h@���@�{@�bN@���@�^5@���@�S�@��F@�K�@��@���@�~�@���@���@�|�@�M�@��@�I�@��^@�O�@��@�"�@��@�ff@x��@m�h@a�@[�F@Q%@B��@=1111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	ĜB	ÖB	ĜB	ŢB	ƨB	ȴB
��B2-BF�B49B"�B}�BƨB��B!�B(�B1'B1'B%�BB�NB~�B
��B
�jB
S�B
hB	�
B	ŢB	�!B	m�B	dZB	s�B	n�B	L�B	B	_;B	u�B	{�B	u�B	|�B	�bB	��B	�B	�'B	�qB	ÖB	�
B	�)B	�fB	��B
B
VB
PB
�B
�B
 �B
$�B
(�B
.B
5?B
>wB
B�B
F�B
H�B
N�B
W
B
\)1111111111111111111111111111111111111111111111111111111111111111111 B	ĜB	ÖB	ĜB	ŢB	ƨB	ȴB
��B2-BF�B49B"�B}�BƨB��B!�B(�B1'B1'B%�BB�NB~�B
��B
�jB
S�B
hB	�
B	ŢB	�!B	m�B	dZB	s�B	n�B	L�B	B	_;B	u�B	{�B	u�B	|�B	�bB	��B	�B	�'B	�qB	ÖB	�
B	�)B	�fB	��B
B
VB
PB
�B
�B
 �B
$�B
(�B
.B
5?B
>wB
B�B
F�B
H�B
N�B
W
B
\)1111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA4_a                                                                20081001084942  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081001084944  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081001084944  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081001084945  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081001084949  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081001084949  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081001084949  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081001084949  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081001084949  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081001090338                      G�O�G�O�G�O�                JA  ARFMdecpA4_a                                                                20081005160425  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081005160447  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081005160450  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081005160451  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081005160502  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081005160502  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081005160502  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081005160502  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081005160504  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081005191059                      G�O�G�O�G�O�                JA  ARFMdecpA4_a                                                                20081005160425  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090520062102  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090520062103  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090520062103  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090520062103  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090520062104  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090520062104  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090520062104  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090520062104  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090520062105  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090520062317                      G�O�G�O�G�O�                