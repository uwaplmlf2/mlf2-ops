CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900709 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               DA   JA  20081010055108  20090908004428  A9_49510_068                    2C  D   APEX-SBE 3411                                                   846 @�����1   @�����ޭ@=F$�/�@b���O�;1   ARGOS   A   A   A   @�33A33A���A홚B��BF��Bm��B�33B���B���B���B���B�  C33C�C33C33C)33C3L�C=  CGffC[L�CoffC��fC��3C���C��fC��fC�� C�� Cǌ�Cѳ3C۳3C��C�� C���D�fD� D�fD�fDٚD��D� D$� D)ٚD.�fD3�3D8��D=ٚDB�3DG�fDN  DTFfDZy�D`�fDg�Dm` Ds� Dy�3D�  D�l�D���D��fD�  D�l�D�� D�` D��D�l�D��D�ffD��fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111   @�33A33A���A홚B��BF��Bm��B�33B���B���B���B���B�  C33C�C33C33C)33C3L�C=  CGffC[L�CoffC��fC��3C���C��fC��fC�� C�� Cǌ�Cѳ3C۳3C��C�� C���D�fD� D�fD�fDٚD��D� D$� D)ٚD.�fD3�3D8��D=ٚDB�3DG�fDN  DTFfDZy�D`�fDg�Dm` Ds� Dy�3D�  D�l�D���D��fD�  D�l�D�� D�` D��D�l�D��D�ffD��fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�A�A��;A�M�A�ƨA�ƨA�+A��#A�~�A�-A�
=A���A�33A�JA�ȴA��DA�jA���A��A��DA�ȴA�"�A�XA�~�A�%Axv�Ao��Ah  AbM�AZ��AR^5AKG�AB�A8JA+A#�^A{A-A��A -@��T@� �@ӝ�@�I�@�33@�|�@��h@�n�@��-@��@��/@��@�"�@��@��`@�b@z��@v{@p1'@i��@^��@T��@M�-@E?}@>�+@6�@+S�@ Ĝ@l�@�;@��@�F?�(�?��#1111111111111111111111111111111111111111111111111111111111111111111111111   A�A�A��;A�M�A�ƨA�ƨA�+A��#A�~�A�-A�
=A���A�33A�JA�ȴA��DA�jA���A��A��DA�ȴA�"�A�XA�~�A�%Axv�Ao��Ah  AbM�AZ��AR^5AKG�AB�A8JA+A#�^A{A-A��A -@��T@� �@ӝ�@�I�@�33@�|�@��h@�n�@��-@��@��/@��@�"�@��@��`@�b@z��@v{@p1'@i��@^��@T��@M�-@E?}@>�+@6�@+S�@ Ĝ@l�@�;@��@�F?�(�?��#1111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�RB�XB�B%B)�B'�B33B5?B/B,B&�B"�B�B�BJBB
��B
��B
�B
�mB
�TB
�TB
�B
�jB
�VB
ffB
B�B
,B
VB	�TB	B	��B	ffB	1'B	B��B�B�PBm�BT�BH�B<jB:^B;dB?}BI�B\)BhsBx�B�bB��B�}B�B�B	
=B	�B	+B	;dB	M�B	q�B	�\B	��B	�jB	��B	�`B
%B
�B
 �B
F�B
VB
bNB
o�B
r�1111111111111111111111111111111111111111111111111111111111111111111111111   B�RB�dB�BPB,B)�B49B7LB0!B-B'�B#�B�B�BPBB  B
��B
�B
�mB
�TB
�TB
�
B
�qB
�\B
gmB
C�B
-B
\B	�ZB	ÖB	��B	hsB	2-B	B��B�B�VBn�BVBI�B=qB;dB<jB@�BJ�B\)BhsBx�B�bB��B�}B�B�B	
=B	�B	+B	;dB	M�B	q�B	�\B	��B	�jB	��B	�`B
%B
�B
 �B
F�B
VB
bNB
o�B
r�1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<�o<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810180833422008101808334220081018083342200810180856462008101808564620081018085646200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20081010055105  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081010055108  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081010055109  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081010055113  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081010055114  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081010055114  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081010062934                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081014035203  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081014035207  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081014035208  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081014035212  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081014035212  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081014035212  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081014061147                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081014035203  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519024645  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519024645  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519024646  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519024647  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519024647  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519024647  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519024647  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519024647  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519024926                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081013171802  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20081018083342  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081018083342  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081018085646  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908004230  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908004428                      G�O�G�O�G�O�                