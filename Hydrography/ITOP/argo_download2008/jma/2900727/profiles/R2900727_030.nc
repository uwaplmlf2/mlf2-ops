CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   3   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  9h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  9�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  :h   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  :�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;h   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  <4   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  <h   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  =4   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  >4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  ?    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ?4   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  @    PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  @4   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  A    DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  A�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  B    DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  B�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  C    	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    L   CALIBRATION_DATE            	             
_FillValue                  8  P   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    PD   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    PH   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    PL   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    PP   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  PT   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    P�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    P�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    P�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         P�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         P�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        P�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    P�Argo profile    2.2 1.2 19500101000000  2900727 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL            DOXY               A   JA  20080927050140  20090707052927                                  2B  A   APEX-SBE 3381                                                   846 @��k�W;1   @��k˩�e@;����m@b`�t�j1   ARGOS   A   A   A       @陚A)��A���A�33A�ffB  B8��Bc33B�  B�  B���B���Bԙ�B�  B���CL�C33C�C%33C/��C933CB�3CL�3CW  C_��Ck��Cu��CffC���C�ٚC�� C�L�C���C�ffC���C�s3C���C��fC�L�C��3C���C�s3C�ffCϙ�C�@ C٦fC�Y�C�33C虚C�@ C��111111111111111111111111111111111111111111111111111 @�fgA(  A���A�ffAљ�B��B8fgBb��B���B���B�fgB�fgB�fgB���B�fgC33C�C  C%�C/� C9�CB��CL��CV�fC_� Ck� Cu� CL�C�� C���C��3C�@ C���C�Y�C�� C�ffC�� C���C�@ C��fC�� C�ffC�Y�Cό�C�33Cٙ�C�L�C�&fC��C�33C�  111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A��A��AּjA�Q�A�7LA�VA��^A���A� �A��9A���A��`A��A��A�z�A�`BA���A��9A��^A�&�A���A��TA�v�A�(�A�ĜA�~�A��A�1A�M�A���A�33A�E�A�jAƨA{Av�As�Al�AiAf�\Aa�A^�DAY��AS�hAL��AI"�AD�A<�A9�T111111111111111111111111111111111111111111111111111 A��A��A��A��AּjA�Q�A�7LA�VA��^A���A� �A��9A���A��`A��A��A�z�A�`BA���A��9A��^A�&�A���A��TA�v�A�(�A�ĜA�~�A��A�1A�M�A���A�33A�E�A�jAƨA{Av�As�Al�AiAf�\Aa�A^�DAY��AS�hAL��AI"�AD�A<�A9�T111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B��B	(�B	�B	B
�B
B�B
m�B
�LB
�HB
�B
��B
��B
��B\B{BhBJB
��B
��B
��B
�B
�B
�B
�B
�mB
�#B
�B
��B
ȴB
ĜB
�?B
��B
��B
��B
�JB
w�B
aHB
S�B
33B
#�B
�B
%B	��B	�NB	ȴB	��B	��B	�B	\)B	R�111111111111111111111111111111111111111111111111111 B��B��B	(�B	�B	B
�B
B�B
m�B
�LB
�HB
�B
��B
��B
��B\B{BhBJB
��B
��B
��B
�B
�B
�B
�B
�mB
�#B
�B
��B
ȴB
ĜB
�?B
��B
��B
��B
�JB
w�B
aHB
S�B
33B
#�B
�B
%B	��B	�NB	ȴB	��B	��B	�B	\)B	R�111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�CK�CM��CH�CO��CR��CeX�Cj�fCHD�CcbCa!HC[�-CVJ=CHCF�CO	7CN �CM�DCOz^CM�LCM��CMRCM3�CMGmCL%CJa�CIԼCI-PCG[�CI��CS��CQ�CM
=CJ��CG
CBK�C=:�C:lJC8�9C6g�C4ɺC5XC4^�C2�/C3'mC2nC1?}C2�NC2CTC4K�C4wC4&�000000000000000000000000000000000000000000000000000 CK�CM��CH�CO��CR��CeX�Cj�fCHD�CcbCa!HC[�-CVJ=CHCF�CO	7CN �CM�DCOz^CM�LCM��CMRCM3�CMGmCL%CJa�CIԼCI-PCG[�CI��CS��CQ�CM
=CJ��CG
CBK�C=:�C:lJC8�9C6g�C4ɺC5XC4^�C2�/C3'mC2nC1?}C2�NC2CTC4K�C4wC4&�000000000000000000000000000000000000000000000000000 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA12                                                                 20080927050137  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080927050140  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080927050141  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080927050144  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080927050144  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080927050145  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080927052616                      G�O�G�O�G�O�                JA  ARFMdecpA12                                                                 20081001043320  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081001043323  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081001043324  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081001043328  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081001043328  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081001043328  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20081001065117                      G�O�G�O�G�O�                JA  ARFMdecpA12                                                                 20090601015413  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090601015943  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090601015943  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090601015944  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090601015945  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090601015945  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090601015945  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090601015945  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090601015945  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090601020356                      G�O�G�O�G�O�                JA  ARFMdecpA12a                                                                20090707051456  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090707052329  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090707052330  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090707052330  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090707052331  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090707052331  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090707052331  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090707052331  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090707052331  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090707052927                      G�O�G�O�G�O�                