CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   2   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  9d   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  9�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  :`   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  :�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;\   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  <$   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  <X   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  =    TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  =T   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  >   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  >�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ?   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  ?�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  @   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  @�   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  A�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  A�   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  B�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    C�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  8  O�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    P   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    P   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    P   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    P    HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  P$   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Pd   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Pt   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Px   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         P�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         P�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        P�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    P�Argo profile    2.2 1.2 19500101000000  2900727 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL            DOXY               A   JA  20080808051306  20090707052923                                  2B  A   APEX-SBE 3381                                                   846 @����F��1   @���>c]�@=��7Kƨ@bB��"��1   ARGOS   A   A   A       @陚A8  A�ffA�  A�33BffB733B_��B�  B���B�  B�ffB���B癚B�ffC��C��C��C$�3C/��C933CC�fCM�fCX�Cb�Ck��Cu� C� C���C�� C���C��3C�ٚC���C�� C�  C��fC���C���C�� C��fC�s3C�� C�� C�� Cٌ�Cތ�C�Y�C�s3C�3311111111111111111111111111111111111111111111111111  @陚A8  A�ffA�  A�33BffB733B_��B�  B���B�  B�ffB���B癚B�ffC��C��C��C$�3C/��C933CC�fCM�fCX�Cb�Ck��Cu� C� C���C�� C���C��3C�ٚC���C�� C�  C��fC���C���C�� C��fC�s3C�� C�� C�� Cٌ�Cތ�C�Y�C�s3C�3311111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�"�A�uA��;A�jA�M�A��
A�jA���A���A�hsA�|�A��DA�A�A�+A�t�A��A��A�?}A�|�A��-A�-A�M�A��#A�?}A�%A�S�A��jA�  A�z�A���A��^A�XA��+A�;dA}�-A{/AxI�Au"�ArĜAo;dAl{Ag%AaƨA^1'AX�ATz�AQ�hAJ��AF��AA�F11111111111111111111111111111111111111111111111111  A�"�A�uA��;A�jA�M�A��
A�jA���A���A�hsA�|�A��DA�A�A�+A�t�A��A��A�?}A�|�A��-A�-A�M�A��#A�?}A�%A�S�A��jA�  A�z�A���A��^A�XA��+A�;dA}�-A{/AxI�Au"�ArĜAo;dAl{Ag%AaƨA^1'AX�ATz�AQ�hAJ��AF��AA�F11111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
1B	�B	��B	�B
	7B
��B
�qB
�!B
�^B
�sBBPBuBoBoB�B�B�B\BDBBB
��B
�B
�BB
��B
ɺB
��B
�dB
�B
��B
��B
�\B
�B
t�B
ffB
YB
K�B
;dB
.B
�B
B	�B	�
B	��B	�?B	�{B	�%B	r�11111111111111111111111111111111111111111111111111  B
�B
1B	�B	��B	�B
	7B
��B
�qB
�!B
�^B
�sBBPBuBoBoB�B�B�B\BDBBB
��B
�B
�BB
��B
ɺB
��B
�dB
�B
��B
��B
�\B
�B
t�B
ffB
YB
K�B
;dB
.B
�B
B	�B	�
B	��B	�?B	�{B	�%B	r�11111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�CK$CN��CU�!Ch?�Cc��Ck��CmG+Cil�Ch�)Cc��CW�jCT�CT�CS/�CR��CR"CN�CL��CL	yCL�CN�hCUQhCT.VCP
=CG��CC�/CB�)C?�mC>��C=�LC<\�C<��C@'C=�+C<��C;�VC<��C:�C:��C9�?C9;�C8��C6�bC6̋C6+�C6�DC5cC4��C1�9C2��00000000000000000000000000000000000000000000000000  CK$CN��CU�!Ch?�Cc��Ck��CmG+Cil�Ch�)Cc��CW�jCT�CT�CS/�CR��CR"CN�CL��CL	yCL�CN�hCUQhCT.VCP
=CG��CC�/CB�)C?�mC>��C=�LC<\�C<��C@'C=�+C<��C;�VC<��C:�C:��C9�?C9;�C8��C6�bC6̋C6+�C6�DC5cC4��C1�9C2��00000000000000000000000000000000000000000000000000  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA12                                                                 20080808051258  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080808051306  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080808051308  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080808051314  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080808051315  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080808051315  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080808053448                      G�O�G�O�G�O�                JA  ARFMdecpA12                                                                 20080812043416  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080812043419  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080812043420  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080812043423  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080812043424  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080812043424  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080812051305                      G�O�G�O�G�O�                JA  ARFMdecpA12                                                                 20090601015407  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090601015914  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090601015915  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090601015915  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090601015916  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090601015916  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090601015916  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090601015916  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090601015917  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090601020353                      G�O�G�O�G�O�                JA  ARFMdecpA12a                                                                20090707051451  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090707052303  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090707052303  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090707052304  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090707052305  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090707052305  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090707052305  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090707052305  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090707052305  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090707052923                      G�O�G�O�G�O�                