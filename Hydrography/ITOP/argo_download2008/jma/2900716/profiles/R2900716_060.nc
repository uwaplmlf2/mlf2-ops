CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   I   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D�   CALIBRATION_DATE            	             
_FillValue                  ,  G�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    H   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    H   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    H   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    H   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  H   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    HT   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Hd   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Hh   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Hx   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         H|   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        H�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    H�Argo profile    2.2 1.2 19500101000000  2900716 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               <A   JA  20080811045812  20090519041740                                  2B  A   APEX-SBE 3418                                                   846 @�篝i�1   @���kT�@:m�hr�!@`y�S���1   ARGOS   A   A   A   @�  A  A�ffA�ffB  BE��Bm33B���B���B�ffBƙ�B�33B���C� CL�C� C� C)ffC3ffC=ffCGL�C[ffCn�3C�� C��3C��fC�� C��fC��3C��fCǀ Cр C�� C��C�fC��3D�fD�fD��D� DٚD��D� D$�3D)��D.��D3��D8�fD=�fDB�3DG� DN�DTFfDZ�3D`��Dg  Dm@ Ds� Dy��D��D�l�D���D��fD�,�D�l�D�� D�l�D��fD�l�D���D�i�D���D�S31111111111111111111111111111111111111111111111111111111111111111111111111   @�  A  A�ffA�ffB  BE��Bm33B���B���B�ffBƙ�B�33B���C� CL�C� C� C)ffC3ffC=ffCGL�C[ffCn�3C�� C��3C��fC�� C��fC��3C��fCǀ Cр C�� C��C�fC��3D�fD�fD��D� DٚD��D� D$�3D)��D.��D3��D8�fD=�fDB�3DG� DN�DTFfDZ�3D`��Dg  Dm@ Ds� Dy��D��D�l�D���D��fD�,�D�l�D�� D�l�D��fD�l�D���D�i�D���D�S31111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�7AꕁA�r�A��A��A�VA��-A��A��RA�33A�dZA���A�$�A���A���A���A���A���A�(�A�p�A�`BA��A�O�A�A�K�A�l�A�7LA+AshsAlZAat�AX�9AO�#AC��A7�A3�A*z�A$�A�mA��A�
A	A-@�o@旍@�t�@�K�@̛�@��;@�ff@��@�%@�v�@��w@�t�@���@�;d@�ƨ@��H@{��@p1'@a��@St�@Kt�@B-@3S�@#��@��@��@
�!@�@   ?�v�1111111111111111111111111111111111111111111111111111111111111111111111111   A�7AꕁA�r�A��A��A�VA��-A��A��RA�33A�dZA���A�$�A���A���A���A���A���A�(�A�p�A�`BA��A�O�A�A�K�A�l�A�7LA+AshsAlZAat�AX�9AO�#AC��A7�A3�A*z�A$�A�mA��A�
A	A-@�o@旍@�t�@�K�@̛�@��;@�ff@��@�%@�v�@��w@�t�@���@�;d@�ƨ@��H@{��@p1'@a��@St�@Kt�@B-@3S�@#��@��@��@
�!@�@   ?�v�1111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	YB	cTB	y�B
S�B
�-B
��BVB�BE�BW
BcTBcTBcTBbNB]/BW
BR�BP�BO�BL�BG�B6FB"�B�B
��B
�B
�B
�B
q�B
J�B
�B	�B	ŢB	�bB	_;B	J�B	%�B	PB�yB��BɺBŢB�!B��B��B��B��B�B�3B�?BƨB�B��B��B	oB	!�B	;dB	I�B	ZB	|�B	�hB	�!B	��B	�B	�B
+B
#�B
8RB
K�B
ZB
ffB
s�B
u�1111111111111111111111111111111111111111111111111111111111111111111111111   B	YB	cTB	y�B
S�B
�-B
��BVB�BE�BW
BcTBcTBcTBbNB]/BW
BR�BP�BO�BL�BG�B6FB"�B�B
��B
�B
�B
�B
q�B
J�B
�B	�B	ŢB	�bB	_;B	J�B	%�B	PB�yB��BɺBŢB�!B��B��B��B��B�B�3B�?BƨB�B��B��B	oB	!�B	;dB	I�B	ZB	|�B	�hB	�!B	��B	�B	�B
+B
#�B
8RB
K�B
ZB
ffB
s�B
u�1111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080811045804  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080811045812  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080811045814  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080811045821  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080811045821  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080811045822  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080811052338                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080815035052  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080815035056  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080815035056  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080815035100  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080815035101  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080815035101  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20080815035101  CV  TIME            G�O�G�O�F�=~                JA  ARGQrelo2.1                                                                 20080815035101  CV  LAT$            G�O�G�O�A�l�                JA  ARGQrelo2.1                                                                 20080815035101  CV  LON$            G�O�G�O�C�V                JA  ARUP                                                                        20080815050909                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080815035052  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519041412  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519041412  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519041413  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519041414  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519041414  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519041414  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519041414  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519041414  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519041740                      G�O�G�O�G�O�                