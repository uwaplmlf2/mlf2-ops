CDF   '   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   E   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5T   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6h   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9    PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       :4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;H   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       <�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >    SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >0   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A0   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D0   CALIBRATION_DATE            	             
_FillValue                  ,  G0   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G\   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G`   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Gd   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Gh   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Gl   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         G�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         G�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G�Argo profile    2.2 1.2 19500101000000  2900716 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               AA   JA  20080902125124  20091208015334                                  2B  A   APEX-SBE 3418                                                   846 @����1��1   @��谋�B@:T9XbN@`��n��5   ARGOS   B   B   B   @�ffA  A�33A�ffB33BE��Bm��B�33B�ffB�33B�  B���B���C ��C�C��CffC)ffC333C=33G�O�C���C�s3C���C��fC�ٚCǌ�Cр CۦfC噚C�fC��3D�3D�fD�3D�3D�fD��D�fD$�fD)ٚD.��D3�3D8�3D=��DB�3DGٚDN3DTY�DZ��D`� DgfDm` Ds��Dy�fD�,�D�` D��fD�ٚD��D�Y�D��D�\�D��fD�VfD���D�` D�ٚD���111111111111111111114111111111111111111111111111111111111111111111111   @�ffA  A�33A�ffB33BE��Bm��B�33B�ffB�33B�  B���B���C ��C�C��CffC)ffC333C=33G�O�C���C�s3C���C��fC�ٚCǌ�Cр CۦfC噚C�fC��3D�3D�fD�3D�3D�fD��D�fD$�fD)ٚD.��D3�3D8�3D=��DB�3DGٚDN3DTY�DZ��D`� DgfDm` Ds��Dy�fD�,�D�` D��fD�ٚD��D�Y�D��D�\�D��fD�VfD���D�` D�ٚD���111111111111111111114111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�dZA�n�A�=qAᛦAĸRA��A�O�A��7A�&�A�-A���A���A���A�O�A�C�A��\A���A�A��PG�O�A�S�A�bNA���A��7Av�`Am�AfI�A_��AX�AN�DAC
=A;�A/ƨA%?}A�A  A�A�TA;d@��
@�!@�(�@Ցh@�  @�/@��7@��@�|�@�z�@�`B@�?}@�r�@�Z@�^5@�K�@t��@a%@V�@MO�@FE�@?\)@/��@$9X@"�@��@��@/@ Q�?�(�111111111111111111194111111111111111111111111111111111111111111111111   A�dZA�n�A�=qAᛦAĸRA��A�O�A��7A�&�A�-A���A���A���A�O�A�C�A��\A���A�A��PG�O�A�S�A�bNA���A��7Av�`Am�AfI�A_��AX�AN�DAC
=A;�A/ƨA%?}A�A  A�A�TA;d@��
@�!@�(�@Ցh@�  @�/@��7@��@�|�@�z�@�`B@�?}@�r�@�Z@�^5@�K�@t��@a%@V�@MO�@FE�@?\)@/��@$9X@"�@��@��@/@ Q�?�(�111111111111111111194111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B	��B	�jB
��B
�?B
�TB
�B	7B&�B49B49B<jBC�BN�BI�BF�BD�BD�BA�G�O�BB
�NB
�dB
�B
S�B
1'B
{B	��B	��B	�{B	n�B	=qB	�B�B�/B��BȴB�?B��B��B��B��B��B�B�jBǮB��B�HB�B	PB	#�B	8RB	L�B	`BB	�PB	��B	�?B	ɺB	�)B	�B
VB
$�B
8RB
I�B
W
B
gmB
q�B
v�111111111111111111114111111111111111111111111111111111111111111111111   B	��B	��B	��B	�jB
��B
�?B
�TB
�B	7B&�B49B49B<jBC�BN�BI�BF�BD�BD�BA�G�O�BB
�NB
�dB
�B
S�B
1'B
{B	��B	��B	�{B	n�B	=qB	�B�B�/B��BȴB�?B��B��B��B��B��B�B�jBǮB��B�HB�B	PB	#�B	8RB	L�B	`BB	�PB	��B	�?B	ɺB	�)B	�B
VB
$�B
8RB
I�B
W
B
gmB
q�B
v�111111111111111111114111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080902125121  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080902125124  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080902125125  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080902125129  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080902125129  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080902125130  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080902130245                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080906035636  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080906035640  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080906035641  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080906035645  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080906035645  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080906035646  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080906053202                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080906035636  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090520085300  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090520085300  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090520085301  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090520085302  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090520085302  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090520085302  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090520085302  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090520085302  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090520085546                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208012934  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208013118  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208013118  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208013119  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208013120  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091208013120  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208013120  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208013120  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208013120  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208013120  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208013120  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20091208013120  CV  TIME            G�O�G�O�F�j                JA  ARGQrelo2.1                                                                 20091208013120  CV  LAT$            G�O�G�O�A�bN                JA  ARGQrelo2.1                                                                 20091208013120  CV  LON$            G�O�G�O�C��                JA  ARUP                                                                        20091208015334                      G�O�G�O�G�O�                