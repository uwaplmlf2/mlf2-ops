CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901534 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ;A   JA  20081015185800  20090908021648  A9_66116_059                    2C  D   APEX-SBE 2825                                                   846 @�����1   @�����^@3У�
=q@a띲-V1   ARGOS   A   A   A   @�33A��Ai��A�  A�  A���B	33B��B2ffBF  BZ��Bn��B���B�ffB���B�ffB�  B�33B�ffB�  B�  B�  B�ffB���B���C�C33C� C��C33C� C�C$� C)� C.� C3  C833C=  CBffCGffCQ�C[� Ce�3CoffCyffC��fC�� C���C��3C��3C���C��fC��fC�� C��3C�� C�� C���C³3CǙ�C̀ C�� C�s3C۳3C�fC�3C� C�fC���C�� DٚD� D�fD� D�fD� DٚD$��D)�3D.� D3ٚD8� D=�fDB� DG�fDL� DQٚDV�fD[�3D`��De�3DjٚDoٚDt� Dy� D�,�D�p D��3D��3D�  D�ffD��fD���D��D�l�D��fD�� D�#3D�Y�Dڣ3D�� D�  D�i�D�fD�6f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A	��A^ffA�ffA�ffA�33BffB��B/��BC33BX  Bl  B~��B�  B�ffB�  B���B���B�  Bř�Bϙ�Bٙ�B�  B�ffB�ffC ffC� C
��C�fC� C��CffC#��C(��C-��C2L�C7� C<L�CA�3CF�3CPffCZ��Ce  Cn�3Cx�3C�L�C�ffC�@ C�Y�C�Y�C�33C�L�C�L�C�ffC�Y�C�&fC�ffC�33C�Y�C�@ C�&fC�ffC��C�Y�C�L�C�Y�C�&fC�L�C�@ C�ffD��D�3D��D�3D��D�3D��D$� D)�fD.�3D3��D8�3D=��DB�3DG��DL�3DQ��DV��D[�fD`� De�fDj��Do��Dt�3Dy�3D�fD�Y�D���D���D�	�D�P D�� D��fD�3D�VfD�� D�ٚD��D�C3Dڌ�D�ɚD�	�D�S3D� D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A���A�ƨA���A���A�{A앁A�x�AٸRAЬA�&�A�bA�l�A��
Aĝ�A��A� �A��A��A���A��A���A��;A�"�A��A��
A���A���A�ffA�ƨA�VA���A�/A�v�A��-A�hsA��PA�+A���A�A�v�A���A��A�v�A���A�%A�M�A�E�A�~�A���A�bA��+A{��Aup�Aql�An�DAj9XAg��A]XAU�AM/AB�`A@VA=�-A9/A3��A2�A,9XA'��A��A�A�@�1'@�n�@١�@�@�X@��@�5?@��F@���@�b@�X@�ȴ@���@���@��7@�$�@��P@�5?@��m@���@�@+@|Z@n�R@d��@Z=q@M@F��@>��@5��@2��@,�@)�#@$Z@ A�@��@��@9X@V@	��@;d@1@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A���A�ƨA���A���A�{A앁A�x�AٸRAЬA�&�A�bA�l�A��
Aĝ�A��A� �A��A��A���A��A���A��;A�"�A��A��
A���A���A�ffA�ƨA�VA���A�/A�v�A��-A�hsA��PA�+A���A�A�v�A���A��A�v�A���A�%A�M�A�E�A�~�A���A�bA��+A{��Aup�Aql�An�DAj9XAg��A]XAU�AM/AB�`A@VA=�-A9/A3��A2�A,9XA'��A��A�A�@�1'@�n�@١�@�@�X@��@�5?@��F@���@�b@�X@�ȴ@���@���@��7@�$�@��P@�5?@��m@���@�@+@|Z@n�R@d��@Z=q@M@F��@>��@5��@2��@,�@)�#@$Z@ A�@��@��@9X@V@	��@;d@1@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	6FB	6FB	6FB	5?B	49B	>wB	M�B
�XB
��B
�B
ƨB1'B2-BE�BP�BM�Bm�Bo�B~�B�VB�7B�oB��B��B�LB�FB�3B�B�B�B�LB�LB��B��B�7B� Bt�Bl�BhsBW
BM�BI�B>wB.B�BuB%B
��B
�B
�/B
��B
�B
�uB
n�B
ZB
I�B
6FB
$�B	�B	��B	��B	s�B	gmB	YB	E�B	0!B	+B	�B	+B�HBB��B�7B�B�B�B��B��BÖB�HB�B	�B	 �B	+B	=qB	M�B	iyB	�B	��B	��B	�'B	�dB	ĜB	��B	�#B	�`B	�B
B
VB
�B
'�B
,B
8RB
=qB
C�B
H�B
P�B
^5B
jB
p�B
t�B
x�B
z�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	6FB	6FB	6FB	5?B	5?B	?}B	XB
�jB
��B
�B
��B2-B49BH�BQ�BP�Bn�Bq�B�B�hB�DB�uB��B��B�LB�LB�?B�B�B�B�^B�RB�B��B�=B�Bu�Bl�BjBXBM�BJ�B?}B/B�B{B+B
��B
�B
�5B
��B
�B
�{B
o�B
[#B
J�B
7LB
&�B	��B	��B	��B	t�B	hsB	ZB	F�B	1'B	-B	�B	
=B�NBĜB��B�=B�B�B�%B��B��BĜB�HB�B	�B	 �B	,B	=qB	M�B	iyB	�B	��B	��B	�'B	�dB	ĜB	��B	�#B	�`B	�B
B
VB
�B
'�B
,B
8RB
=qB
C�B
H�B
P�B
^5B
jB
p�B
t�B
x�B
z�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           SP(NextCycle)=0.7(dbar)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810281436252008102814362520081028143625200810281437312008102814373120081028143731200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20081015185757  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081015185800  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081015185801  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081015185805  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081015185805  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081015185805  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081015191658                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081019163858  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081019163910  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081019163913  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081019163923  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081019163924  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081019163924  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081019190948                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081019163858  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423045430  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423045430  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423045431  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423045432  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423045432  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423045432  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423045432  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423045432  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423045647                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081019003835  CV  DAT$            G�O�G�O�F��{                JM  ARCAJMQC1.0                                                                 20081028143625  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081028143625  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081028143731  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021619  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021648                      G�O�G�O�G�O�                