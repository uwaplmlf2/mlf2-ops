CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901534 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               8A   JA  20080915185846  20090908021637  A9_66116_056                    2C  D   APEX-SBE 2825                                                   846 @���З�1   @��3�_@3MV�@a�vȴ9X1   ARGOS   A   A   A   @�33A33Ac33A���A�  A�  BffB��B1��BF��BZ  Bm33B�ffB���B�33B�33B�ffB���B�  B�ffBЙ�B�33B�ffB�33B���CL�C� CffCL�CffC� C��C$ffC)  C.�C3  C8L�C=�CB  CF�fCQffC[��Ce33Co� CyffC���C��3C���C���C��3C���C��fC�s3C���C��3C���C�� C�� C�s3Cǌ�C̀ Cљ�C���C۳3C���C�� CꙚC�fC��C�� D��D��D� D��DٚD��D� D$ٚD)�fD.ٚD3� D8� D=�fDB��DGٚDLٚDQٚDV� D[�3D`��De��Dj� Do��Dt�3Dy� D�&fD�ffD���D��3D��D�c3D���D���D�#3D�ffD�� D��3D�#3D�ffDڣ3D��D�#3D�c3D�fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffA^ffA�33A���A噚B33B��B0ffBE��BX��Bl  B��B�33B���B���B���B�  B�ffB���B�  Bٙ�B���BB�  C  C33C�C  C�C33CL�C$�C(�3C-��C2�3C8  C<��CA�3CF��CQ�C[L�Cd�fCo33Cy�C�s3C���C�ffC��fC���C�ffC�� C�L�C�s3C���C�s3C�Y�C���C�L�C�ffC�Y�C�s3C֦fCی�C�s3C噚C�s3C� C�ffC���D��D��D��D��D�fD��D��D$�fD)�3D.�fD3��D8��D=�3DB��DG�fDL�fDQ�fDV��D[� D`��De��Dj��Do�fDt� Dy��D��D�\�D��3D�ٚD�3D�Y�D��3D��3D��D�\�D��fD�ٚD��D�\�Dڙ�D�� D��D�Y�D��D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�(�A���A���A�7A�M�A�5?A� �A�z�A��A� �A�\)Aɴ9A�K�A��TA���A�dZA���A���A�ƨA�&�A�ffA�7LA�=qA��A���A�%A��/A��A���A�1A�;dA��DA�jA��A��jA�VA�/A���A�G�A�{A�&�A�I�A��
A�O�A�z�A�C�A�A|E�Ax1Ap(�Ai��AfZAb��A^�DAZ��AV��AS��AN�AJ-AA�FA<=qA7�7A25?A-O�A%��A �A�Ar�A��AI�A	�w@��@�"�@���@Ӆ@�b@�n�@��;@���@�=q@�C�@���@��/@���@���@�dZ@�1'@���@�K�@�ff@�~�@�z�@��@���@{�@i�#@\(�@PĜ@F�R@=@5V@+�
@'K�@!�#@�@S�@�w@��@�w@z�@	�#@ �@�T@��@  �1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�(�A���A���A�7A�M�A�5?A� �A�z�A��A� �A�\)Aɴ9A�K�A��TA���A�dZA���A���A�ƨA�&�A�ffA�7LA�=qA��A���A�%A��/A��A���A�1A�;dA��DA�jA��A��jA�VA�/A���A�G�A�{A�&�A�I�A��
A�O�A�z�A�C�A�A|E�Ax1Ap(�Ai��AfZAb��A^�DAZ��AV��AS��AN�AJ-AA�FA<=qA7�7A25?A-O�A%��A �A�Ar�A��AI�A	�w@��@�"�@���@Ӆ@�b@�n�@��;@���@�=q@�C�@���@��/@���@���@�dZ@�1'@���@�K�@�ff@�~�@�z�@��@���@{�@i�#@\(�@PĜ@F�R@=@5V@+�
@'K�@!�#@�@S�@�w@��@�w@z�@	�#@ �@�T@��@  �1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
~�B
�B
�B
�B
}�B
|�B
z�B
� B
��B�Bp�B�B�B�7B�JB�%B�oB��B��B��B��B��B�\B�VB�1B�B~�Bz�Br�Bl�BbNBZBS�BN�BH�B?}B5?B2-B(�B �BVB
��B
��B
�sB
�
B
ɺB
�'B
�{B
|�B
T�B
7LB
"�B
uB	��B	�yB	�B	ȴB	�!B	��B	q�B	XB	F�B	.B	�B��B�fB��B��B�qB��B��B�bB�+B�B�B�B��B��B�B		7B	"�B	+B	@�B	VB	iyB	x�B	�VB	��B	�B	�XB	ƨB	��B	�B	�#B	�TB	�B
B
\B
oB
�B
+B
5?B
>wB
E�B
N�B
T�B
ZB
aHB
gmB
l�B
p�B
r�B
u�B
{�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
~�B
�B
�B
�B
}�B
|�B
{�B
�+B
��B&�Bs�B�+B�%B�JB�VB�1B��B��B��B��B��B��B�{B�hB�7B�B� B{�Bs�Bn�BdZB[#BT�BO�BJ�B@�B6FB49B)�B!�B\B  B
��B
�yB
�B
��B
�-B
��B
~�B
W
B
8RB
#�B
{B	��B	�B	�B	ɺB	�'B	��B	s�B	YB	G�B	/B	�B��B�mB��B��B�qB��B��B�hB�1B�B�B�B��B��B�B		7B	"�B	+B	@�B	VB	iyB	x�B	�VB	��B	�B	�XB	ƨB	��B	�B	�#B	�TB	�B
B
\B
oB
�B
+B
5?B
>wB
E�B
N�B
T�B
ZB
aHB
gmB
l�B
p�B
r�B
u�B
{�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<u<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           SP(NextCycle)=0.3(dbar)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809281338002008092813380020080928133800200809281352222008092813522220080928135222200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080915185844  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080915185846  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080915185847  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080915185851  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080915185852  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080915185852  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080915191650                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080919163042  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080919163046  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080919163047  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080919163050  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080919163051  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080919163051  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080919190928                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080919163042  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423045423  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423045423  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423045424  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423045425  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423045425  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423045425  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423045425  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423045425  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423045654                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080928133800  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080928133800  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080928135222  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021603  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021637                      G�O�G�O�G�O�                