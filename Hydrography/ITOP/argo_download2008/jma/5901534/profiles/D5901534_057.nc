CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901534 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               9A   JA  20080925185847  20090908021638  A9_66116_057                    2C  D   APEX-SBE 2825                                                   846 @���+�s1   @���Ř@3\�hr�@aش9Xb1   ARGOS   A   A   A   @�ffA  Aa��A�  A���A陚B33BffB2ffBF  BZ  Bn  B���B�  B���B���B�33B�33B�33B�  B���Bڙ�B�33BB�ffC33C  C  C33C�fC�fC  C$  C)  C.�C3L�C8ffC=L�CB�CF�fCQL�C[L�Ce33CoL�CyffC�s3C��3C��fC��3C��fC�ffC�� C�� C��3C��fC���C���C��fC Cǀ C̀ Cр Cր CۦfC�3C�� C���C�fC��C���D�3D��D�fD�3D��DٚD��D$�3D)� D.�3D3�3D8�fD=�fDB��DG�3DLٚDQ��DV��D[ٚD`ٚDe�fDj�fDo� Dt��Dy� D�#3D�\�D�� D�� D�0 D�i�D�� D��D��D�c3D���D���D�)�D�l�Dڣ3D���D�#3D�ffD�3D�Ff1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��A[33A���A�ffA�ffB	��B��B0��BDffBXffBlffB���B�33B�  B�  B�ffB�ffB�ffB�33B�  B���B�ffB���B���C ��C��C
��C��C� C� C��C#��C(��C-�3C2�fC8  C<�fCA�3CF� CP�fCZ�fCd��Cn�fCy  C�@ C�� C�s3C�� C�s3C�33C�L�C�L�C�� C�s3C�ffC�Y�C�s3C�L�C�L�C�L�C�L�C�L�C�s3C�� C��CꙚC�s3C�Y�C�Y�D��D�3D��D��D�3D� D�3D$��D)�fD.��D3��D8��D=��DB�3DG��DL� DQ� DV� D[� D`� De��Dj��Do�fDt�3Dy�fD�fD�P D��3D��3D�#3D�\�D��3D���D� D�VfD�� D�� D��D�` DږfD�� D�fD�Y�D�fD�9�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�\A�A�;dA�bA��HA���A�7LA���A�bA�-A�VA��A��A��;A���A��TA���A��A�?}A�bA��yA�VA��A��yA��mA�O�A�
=A��
A�x�A�A�JA��9A�  A�%A�C�A�
=A�l�A�ƨA��7A���A�I�A���A�|�A�S�A�|�A�S�A��uA��jA}Az(�Ay�;At9XArn�Af��Ac��A[/AZ�\AP�ALVAF��AD1A?�wA7�TA2=qA'�FA!�hA$�A;dAƨA��A^5A�T@�Ĝ@�%@��@���@�o@���@�@�x�@���@�|�@���@�C�@��#@���@��@��\@�~�@�@���@��@��#@�@y��@m��@^��@SdZ@E�h@9��@4�j@0bN@(�9@$�@ b@t�@��@�/@ �@O�@
n�@�;@�-@�?��h1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�\A�A�;dA�bA��HA���A�7LA���A�bA�-A�VA��A��A��;A���A��TA���A��A�?}A�bA��yA�VA��A��yA��mA�O�A�
=A��
A�x�A�A�JA��9A�  A�%A�C�A�
=A�l�A�ƨA��7A���A�I�A���A�|�A�S�A�|�A�S�A��uA��jA}Az(�Ay�;At9XArn�Af��Ac��A[/AZ�\AP�ALVAF��AD1A?�wA7�TA2=qA'�FA!�hA$�A;dAƨA��A^5A�T@�Ĝ@�%@��@���@�o@���@�@�x�@���@�|�@���@�C�@��#@���@��@��\@�~�@�@���@��@��#@�@y��@m��@^��@SdZ@E�h@9��@4�j@0bN@(�9@$�@ b@t�@��@�/@ �@O�@
n�@�;@�-@�?��h1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
��B
��B
��B
�{B
�bB
�\B
��B�B:^Bo�BaHB_;Bl�BffBr�B~�B|�B}�B{�B{�B{�Bs�Bn�Bn�BjBe`BaHB`BB\)BT�BP�BL�BH�BD�B>wB9XB7LB33B/B-B"�BbBB
�B
�)B
ȴB
��B
�B
��B
�=B
�%B
k�B
_;B
'�B
�B	�B	�`B	�LB	��B	�7B	x�B	ffB	E�B	+B	B�mB�BɺB�^B�3B��B��B�=B�B�B�hB��B�jB�)B�B	B	�B	/B	G�B	VB	y�B	�B	��B	��B	�B	�}B	��B	�#B	�;B	�ZB	�B	��B
�B
%�B
'�B
6FB
9XB
?}B
J�B
O�B
W
B
[#B
cTB
jB
m�B
q�B
v�B
x�B
}�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
��B
��B
��B
�{B
�bB
�bB
�B"�BA�Bu�BffBcTBo�Bk�Bu�B�B~�B� B}�B|�B}�Bt�Bp�Bo�Bk�BffBaHB`BB]/BVBQ�BM�BI�BF�B?}B:^B8RB49B0!B.B#�BhBB
�B
�/B
ȴB
B
�B
��B
�=B
�+B
k�B
bNB
(�B
�B	�B	�mB	�RB	��B	�=B	y�B	hsB	F�B	.B	B�sB�
B��B�dB�9B��B��B�DB�B�B�oB��B�qB�)B�B	B	�B	/B	G�B	VB	y�B	�B	��B	��B	�B	�}B	��B	�#B	�;B	�ZB	�B	��B
�B
%�B
'�B
6FB
9XB
?}B
J�B
O�B
W
B
[#B
cTB
jB
m�B
q�B
v�B
x�B
}�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<D��<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           SP(NextCycle)=0.4(dbar)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810081351512008100813515120081008135151200810081401452008100814014520081008140145200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080925185844  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080925185847  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080925185848  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080925185852  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080925185852  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080925185853  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080925191654                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080929163837  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080929163841  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080929163842  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080929163846  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080929163846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080929163847  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080929191042                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080929163837  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423045425  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423045426  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423045426  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423045427  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423045427  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423045427  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423045427  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423045427  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423045656                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080928223758  CV  DAT$            G�O�G�O�F��v                JM  ARCAJMQC1.0                                                                 20081008135151  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081008135151  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081008140145  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021604  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021638                      G�O�G�O�G�O�                