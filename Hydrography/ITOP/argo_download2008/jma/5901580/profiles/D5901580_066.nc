CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   �   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  �  6�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  �  ;p   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  <l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @X   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  �  DD   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  E@   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  �  I,   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  J(   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  N   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  �  R    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  R�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  �  V�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  W�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  `  [�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                    \0   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                    b0   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                    h0   CALIBRATION_DATE      	   
                
_FillValue                  T  n0   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    n�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    n�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    n�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    n�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  n�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    n�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    n�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    n�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         n�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         n�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        o    HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    oArgo profile    2.2 1.2 19500101000000  5901580 PALAU2008                                                       JAMSTEC Sato Naoki                                              PRES            TEMP            PSAL               BA   JA  20080804003516  20090916051332  A11_5312_066                    2C  D   APEX-SBE 3601                                                   846 @���Dt�1   @����s��@(C��%@_�XbM�1   ARGOS   A   A   A   @���@�  A   A   A@  A`  A�  A�  A���A�  A�  A�  A�  A�  B   B  B  B  B   B(  B0  B8  B@  BH  BP  BX  B`  Bh  Bp  Bx  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�33B�  B�  B���B�  B�  C   C  C  C  C  C
�C  C�fC  C  C  C  C  C  C�CL�C��C"  C$  C&  C(  C*  C,  C-�fC0  C2  C4  C6  C8  C9�fC<  C>  C@  CB  CD  CF  CH  CJ  CL  CN  CP  CR  CT  CV  CX  CZ  C\  C^  C`�Cb  Cd  Cf  Ch  Cj  Cl  Cn  Cp  Cr  Ct  Cv  Cx  Cz  C|  C~  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C��3C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C��C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C��3C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C��C�  C��C��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���@�33A��A!��AA��Aa��A���A���A���A���A���A���A���A���B ffBffBffBffB ffB(ffB0ffB8ffB@ffBHffBPffBXffB`ffBhffBpffBxffB�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�ffB�33B�33B�  B�33B�33C �C�C�C�C�C
33C�C  C�C�C�C�C�C�C33CffC�fC"�C$�C&�C(�C*�C,�C.  C0�C2�C4�C6�C8�C:  C<�C>�C@�CB�CD�CF�CH�CJ�CL�CN�CP�CR�CT�CV�CX�CZ�C\�C^�C`33Cb�Cd�Cf�Ch�Cj�Cl�Cn�Cp�Cr�Ct�Cv�Cx�Cz�C|�C~�C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C�  C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C�  C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C�&f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�
=A�JA�"�A�&�A�$�A�"�A�$�A�&�A�&�A�&�A�(�A�(�A�+A�+A�-A�/A�/A��A���A��/A�~�A��A��A�9XA�~�A�$�A�A痍A�K�A���A旍A�t�A�7LA��
A��A�;dA�x�A�A�+A���A�XA�ZA�^A�v�A�{A� �A���A� �A���A���Aߗ�A�
=Aް!A�p�A�7LA݇+A܃A��A��#A�v�A�VAװ!A�ƨA�"�A�  A��A��A�bNA�1A�M�A�-A��A���A̲-A�^5A�E�AǴ9A�C�Aŧ�AËDA�hsA�XA�oA� �A��-A�/A���A�A�(�A��uA�p�A���A�jA�7LA�(�A�ĜA��A�`BA��A���A�bA��A��A�7LA�?}A��mA�`BA�XA�Q�A�?}A�jA��!A�-A�jA�%A��9A��A�v�A�$�A���A���A��A��A�=qA���A�{A�x�A?}A|-Ax��Avv�At�/Ar�jAo��An��Am|�Ak�Ajv�Ai�wAiG�Ag�Af(�AcdZA_A^E�A]��A[��AX-AV$�AT��ASC�AQƨAOC�AN{AL�AJ�9AGhsAC�ABn�AB5?AA�AA�A@��A>ZA<�A<�A:jA7XA5&�A333A2r�A1��A1G�A/�7A.�A,�`A+|�A*9XA)��A(E�A&��A%�TA$��A$=qA#��A!��A��AĜAO�A�jA�7A�\A�PA�A�A=qA��AXA�;A�\AO�A"�AO�A"�AA�yA��A��A
��A9XA�A|�A��AO�A��A��AoA�#AO�A �`A r�@�33@���@��+@�@���@�t�@�@��@���@�1@�j@���@��H@�R@���@�t�@�"�@���@�D@���@�  @��m@�@�@�1@�w@��@�{@�-@�O�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�
=A�JA�"�A�&�A�$�A�"�A�$�A�&�A�&�A�&�A�(�A�(�A�+A�+A�-A�/A�/A��A���A��/A�~�A��A��A�9XA�~�A�$�A�A痍A�K�A���A旍A�t�A�7LA��
A��A�;dA�x�A�A�+A���A�XA�ZA�^A�v�A�{A� �A���A� �A���A���Aߗ�A�
=Aް!A�p�A�7LA݇+A܃A��A��#A�v�A�VAװ!A�ƨA�"�A�  A��A��A�bNA�1A�M�A�-A��A���A̲-A�^5A�E�AǴ9A�C�Aŧ�AËDA�hsA�XA�oA� �A��-A�/A���A�A�(�A��uA�p�A���A�jA�7LA�(�A�ĜA��A�`BA��A���A�bA��A��A�7LA�?}A��mA�`BA�XA�Q�A�?}A�jA��!A�-A�jA�%A��9A��A�v�A�$�A���A���A��A��A�=qA���A�{A�x�A?}A|-Ax��Avv�At�/Ar�jAo��An��Am|�Ak�Ajv�Ai�wAiG�Ag�Af(�AcdZA_A^E�A]��A[��AX-AV$�AT��ASC�AQƨAOC�AN{AL�AJ�9AGhsAC�ABn�AB5?AA�AA�A@��A>ZA<�A<�A:jA7XA5&�A333A2r�A1��A1G�A/�7A.�A,�`A+|�A*9XA)��A(E�A&��A%�TA$��A$=qA#��A!��A��AĜAO�A�jA�7A�\A�PA�A�A=qA��AXA�;A�\AO�A"�AO�A"�AA�yA��A��A
��A9XA�A|�A��AO�A��A��AoA�#AO�A �`A r�@�33@���@��+@�@���@�t�@�@��@���@�1@�j@���@��H@�R@���@�t�@�"�@���@�D@���@�  @��m@�@�@�1@�w@��@�{@�-@�O�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��B��B��B��B��B��B��B��B��B��B��B��B��B��B��BB�B5?BR�BaHB�=B�3B�^B��B��B�B�`B�fB�B�B��B��B	+B	�B	�B	%�B	@�B	aHB	��B	�`B
#�B
N�B
P�B
F�B
=qB
C�B
P�B
m�B
�PB
��B
��B
�XB
ĜB
ɺB
��B
�NB
��B
=B�BE�B]/BiyB��B��B��B��B��B�'B��B�5B�BVB�B"�B:^B?}B@�B?}B9XB0!B.B0!B8RB:^B6FB.B2-B33B/B)�B#�B�B�BJBB��B��B��B�B�B�TB��B�^B�B��B�JB�Bx�Bk�B\)BM�B?}B0!B"�B�BuB	7B
��B
�B
�ZB
��B
ƨB
�qB
�!B
��B
��B
�{B
�B
w�B
k�B
cTB
]/B
S�B
L�B
G�B
C�B
@�B
=qB
9XB
5?B
)�B
!�B
hB
+B
B	��B	��B	�B	�mB	�5B	��B	ȴB	��B	��B	�jB	�B	��B	��B	��B	��B	��B	��B	��B	��B	�bB	�=B	~�B	o�B	ffB	bNB	bNB	`BB	]/B	YB	YB	T�B	S�B	YB	VB	K�B	@�B	6FB	/B	(�B	$�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	uB	hB	\B	{B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	!�B	&�B	&�B	'�B	'�B	&�B	(�B	33B	8RB	>wB	<jB	;dB	8RB	8RB	5?B	6FB	A�B	J�B	O�B	T�B	YB	[#B	^5B	bNB	dZ11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B��B��B��B��B��B��B��B��B��B��B��B��B��B��BB�B5?BR�BaHB�=B�3B�^B��B��B�B�`B�fB�B�B��B��B	+B	�B	�B	%�B	@�B	aHB	��B	�`B
#�B
N�B
P�B
F�B
=qB
C�B
P�B
m�B
�PB
��B
��B
�XB
ĜB
ɺB
��B
�NB
��B
=B�BE�B]/BiyB��B��B��B��B��B�'B��B�5B�BVB�B"�B:^B?}B@�B?}B9XB0!B.B0!B8RB:^B6FB.B2-B33B/B)�B#�B�B�BJBB��B��B��B�B�B�TB��B�^B�B��B�JB�Bx�Bk�B\)BM�B?}B0!B"�B�BuB	7B
��B
�B
�ZB
��B
ƨB
�qB
�!B
��B
��B
�{B
�B
w�B
k�B
cTB
]/B
S�B
L�B
G�B
C�B
@�B
=qB
9XB
5?B
)�B
!�B
hB
+B
B	��B	��B	�B	�mB	�5B	��B	ȴB	��B	��B	�jB	�B	��B	��B	��B	��B	��B	��B	��B	��B	�bB	�=B	~�B	o�B	ffB	bNB	bNB	`BB	]/B	YB	YB	T�B	S�B	YB	VB	K�B	@�B	6FB	/B	(�B	$�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	uB	hB	\B	{B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	!�B	&�B	&�B	'�B	'�B	&�B	(�B	33B	8RB	>wB	<jB	;dB	8RB	8RB	5?B	6FB	A�B	J�B	O�B	T�B	YB	[#B	^5B	bNB	dZ11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=-0.1(dbar)                                                                                                                                                                                                                                        None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(RecalS error , 0.01(PSS-78))                                                                                                                                                                          None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808042155262008080421552620080804215526200907290000002009072900000020090729000000JA  ARFMfmtp2.2                                                                 20080804003516  IP                  G�O�G�O�G�O�                JA  ARGQrqcp2.6                                                                 20080804003518  QCP$                G�O�G�O�G�O�            FB7CJA  ARUP                                                                        20080804012801                      G�O�G�O�G�O�                                                                                                                IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090909080910  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090909080911  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090909080912  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090909080913  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090909080913  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090909080913  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090909080913  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090909080913  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090909081300                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080804065520  CV  DAT$            G�O�G�O�F�/�                JM  ARCAJMQC1.0                                                                 20080804215526  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080804215526  IP  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090729000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916051117  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916051332                      G�O�G�O�G�O�                