CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   �   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                    6�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                    ;�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  <�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                    D�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  E�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                    I�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  J�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  N�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                    R�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  S�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                    W|   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  X|   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  `  \x   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                    \�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                    b�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                    h�   CALIBRATION_DATE      	   
                
_FillValue                  T  n�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    o,   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    o0   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    o4   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    o8   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  o<   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    o|   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    o�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    o�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         o�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         o�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        o�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    o�Argo profile    2.2 1.2 19500101000000  5901580 PALAU2008                                                       JAMSTEC Sato Naoki                                              PRES            TEMP            PSAL               @A   JA  20080802003509  20090916051348  A11_5312_064                    2C  D   APEX-SBE 3601                                                   846 @��xY <s1   @��y    @(��7Kƨ@_��-1   ARGOS   A   A   A   @�ff@�  A   A   A@  A`  A�  A�  A�  A�  A�  A�  A�  A�  B   B  B  B  B��B(  B0  B8  B@  BH  BP  BX  B`  Bh  Bp  Bx  B�  B�33B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�  B�33B�  B�  B�  B�  B�  B�  B�  B�  B�  C   C�C�fC  C  C
  C  C  C  C  C  C  C  C  C  C  C   C"  C$  C&  C(  C*  C,  C.  C0  C2  C4  C6  C8  C:  C<  C>  C@  CB  CD  CF  CH  CJ  CL�CN  CP  CR  CT  CV  CX  CZ  C\  C^  C`  Cb  Cd  Cf�Ch  Cj  Cl  Cn  Cp  Cr  Ct  Cv  Cx  Cz  C|  C~  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C�  C��D f111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���@�33A��A!��AA��Aa��A���A���A���A���A���A���A���A���B ffBffBffBffB   B(ffB0ffB8ffB@ffBHffBPffBXffB`ffBhffBpffBxffB�33B�ffB�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�33B�ffB�33B�33B�33B�33B�33B�33B�33B�33B�33C �C33C  C�C�C
�C�C�C�C�C�C�C�C�C�C�C �C"�C$�C&�C(�C*�C,�C.�C0�C2�C4�C6�C8�C:�C<�C>�C@�CB�CD�CF�CH�CJ�CL33CN�CP�CR�CT�CV�CX�CZ�C\�C^�C`�Cb�Cd�Cf33Ch�Cj�Cl�Cn�Cp�Cr�Ct�Cv�Cx�Cz�C|�C~�C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��C��D �111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�VA�&�A�+A�+A�+A�+A�&�A�$�A�$�A�(�A�/A�+A�(�A�-A�-A�&�A�-A�33A�=qA�C�A�XA�\)A�`BA�l�A�ffA�M�A�{A�jA�E�A��#A�ĜA�E�A�/A���A�|�A��A�VA���A�z�A�M�A�A�`BA��A䟾A�ffA��HA㕁A�9XA�bA���A⛦A�(�A�A��TA�A�Q�A�"�A��/A�E�A���A�l�A��/A��A�1'A�A�ƨA�`BA֗�Aգ�A�
=A��/A�ƨA�A�A�7LA�=qA���Aʺ^AɾwA�  A�x�A��A��
A���A��A�^5A���A� �A�VA�G�A��uA�1'A���A��A��`A��
A�/A�1'A��mA�%A��A�{A�E�A�A�=qA�C�A�Q�A���A�r�A�33A�;dA��wA�
=A��yA��A�;dA�A��A�ƨA�?}A�E�A��TA��A�r�A��jA��HA�^5A��+A���A��jA�v�A���A�K�A���A{��AzA�AyK�Ax �Av��AuVAs�Aq�AnbNAmdZAl��Al$�Ak"�Ai�#AhI�Ae�AcoAa��A_�#A\��AZ-AV�ATE�AS��ASt�APbALA�AJ{AH�9AF�AE&�ABn�A?��A?��A?�hA=��A<z�A;��A;33A:bNA7%A4��A4A29XA0��A/?}A,��A,1A+x�A)p�A'��A'33A&I�A%G�A!�A��A��AbNA�FA�uA�A�PA&�A��A�AM�A9XA{A�mA��A;dAI�A-A&�A�A�A��A��A^5AA�AbAS�A	�A��A�+AI�A5?A�
AA��A�A =q@���@��y@�V@�x�@���@�S�@�J@�=q@��;@�o@�V@�&�@�\)@�$�@�p�@��@��
@�&�@�@�1'@�z�@�1@�ƨ@��@�-@��@�^@ᙚ@ᙚ@�p�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�VA�&�A�+A�+A�+A�+A�&�A�$�A�$�A�(�A�/A�+A�(�A�-A�-A�&�A�-A�33A�=qA�C�A�XA�\)A�`BA�l�A�ffA�M�A�{A�jA�E�A��#A�ĜA�E�A�/A���A�|�A��A�VA���A�z�A�M�A�A�`BA��A䟾A�ffA��HA㕁A�9XA�bA���A⛦A�(�A�A��TA�A�Q�A�"�A��/A�E�A���A�l�A��/A��A�1'A�A�ƨA�`BA֗�Aգ�A�
=A��/A�ƨA�A�A�7LA�=qA���Aʺ^AɾwA�  A�x�A��A��
A���A��A�^5A���A� �A�VA�G�A��uA�1'A���A��A��`A��
A�/A�1'A��mA�%A��A�{A�E�A�A�=qA�C�A�Q�A���A�r�A�33A�;dA��wA�
=A��yA��A�;dA�A��A�ƨA�?}A�E�A��TA��A�r�A��jA��HA�^5A��+A���A��jA�v�A���A�K�A���A{��AzA�AyK�Ax �Av��AuVAs�Aq�AnbNAmdZAl��Al$�Ak"�Ai�#AhI�Ae�AcoAa��A_�#A\��AZ-AV�ATE�AS��ASt�APbALA�AJ{AH�9AF�AE&�ABn�A?��A?��A?�hA=��A<z�A;��A;33A:bNA7%A4��A4A29XA0��A/?}A,��A,1A+x�A)p�A'��A'33A&I�A%G�A!�A��A��AbNA�FA�uA�A�PA&�A��A�AM�A9XA{A�mA��A;dAI�A-A&�A�A�A��A��A^5AA�AbAS�A	�A��A�+AI�A5?A�
AA��A�A =q@���@��y@�V@�x�@���@�S�@�J@�=q@��;@�o@�V@�&�@�\)@�$�@�p�@��@��
@�&�@�@�1'@�z�@�1@�ƨ@��@�-@��@�^@ᙚ@ᙚ@�p�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB%BBBBBBB%B%BBBBBBBBBB	7BDBuBuB�B�B(�B@�B�1B��B�^BĜB�/B�fB�B	VB	+B	P�B	hsB	w�B	�JB	�{B	�B	�LB	��B	�BB	�B	��B
DB
�B
�B
%�B
$�B
8RB
Q�B
gmB
o�B
s�B
v�B
y�B
�B
�oB
�B
ÖB
�5B
��B{Be`BjB{�B�hB��B�RB��B�B	7BbB�B�B�B�B�B!�B!�B!�B'�B&�B&�B-B2-B0!B0!B2-B1'B/B/B0!B0!B#�B�B1B�B�5B��B�FB�B��B��B��B��B�VB�%By�Bl�BbNBW
BI�BB�B:^B/B"�B�B�B+B
�B
�;B
��B
ÖB
�wB
�dB
�RB
�9B
�B
��B
�PB
|�B
v�B
s�B
n�B
hsB
bNB
\)B
S�B
J�B
F�B
D�B
A�B
=qB
6FB
,B
�B
{B
\B
+B	��B	�B	�ZB	�5B	�)B	�
B	ȴB	�XB	�B	��B	��B	�bB	�1B	�1B	�7B	�=B	�bB	�%B	� B	}�B	y�B	q�B	o�B	k�B	e`B	^5B	XB	R�B	Q�B	T�B	J�B	?}B	8RB	0!B	$�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	{B	PB	bB	uB	{B	{B	{B	�B	�B	{B	{B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	#�B	#�B	 �B	�B	 �B	(�B	0!B	5?B	8RB	7LB	6FB	8RB	7LB	>wB	H�B	N�B	S�B	YB	ZB	]/B	^5B	^5B	`BB	bNB	cT111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B%BBBBBBB%B%BBBBBBBBBB	7BDBuBuB�B�B(�B@�B�1B��B�^BĜB�/B�fB�B	VB	+B	P�B	hsB	w�B	�JB	�{B	�B	�LB	��B	�BB	�B	��B
DB
�B
�B
%�B
$�B
8RB
Q�B
gmB
o�B
s�B
v�B
y�B
�B
�oB
�B
ÖB
�5B
��B{Be`BjB{�B�hB��B�RB��B�B	7BbB�B�B�B�B�B!�B!�B!�B'�B&�B&�B-B2-B0!B0!B2-B1'B/B/B0!B0!B#�B�B1B�B�5B��B�FB�B��B��B��B��B�VB�%By�Bl�BbNBW
BI�BB�B:^B/B"�B�B�B+B
�B
�;B
��B
ÖB
�wB
�dB
�RB
�9B
�B
��B
�PB
|�B
v�B
s�B
n�B
hsB
bNB
\)B
S�B
J�B
F�B
D�B
A�B
=qB
6FB
,B
�B
{B
\B
+B	��B	�B	�ZB	�5B	�)B	�
B	ȴB	�XB	�B	��B	��B	�bB	�1B	�1B	�7B	�=B	�bB	�%B	� B	}�B	y�B	q�B	o�B	k�B	e`B	^5B	XB	R�B	Q�B	T�B	J�B	?}B	8RB	0!B	$�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	{B	PB	bB	uB	{B	{B	{B	�B	�B	{B	{B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	�B	#�B	#�B	 �B	�B	 �B	(�B	0!B	5?B	8RB	7LB	6FB	8RB	7LB	>wB	H�B	N�B	S�B	YB	ZB	]/B	^5B	^5B	`BB	bNB	cT111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=-0.1(dbar)                                                                                                                                                                                                                                        None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(RecalS error , 0.01(PSS-78))                                                                                                                                                                          None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808022155202008080221552020080802215520200907290000002009072900000020090729000000JA  ARFMfmtp2.2                                                                 20080802003509  IP                  G�O�G�O�G�O�                JA  ARGQrqcp2.6                                                                 20080802003511  QCP$                G�O�G�O�G�O�            FB7CJA  ARUP                                                                        20080802012833                      G�O�G�O�G�O�                                                                                                                IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090909080904  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090909080904  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090909080905  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090909080906  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090909080906  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090909080906  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090909080906  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090909080906  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090909081308                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080802065522  CV  DAT$            G�O�G�O�F�+�                JM  ARCAJMQC1.0                                                                 20080802215520  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080802215520  IP  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090729000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916051144  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916051348                      G�O�G�O�G�O�                