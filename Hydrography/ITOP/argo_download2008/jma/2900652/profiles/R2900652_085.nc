CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900652 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               UA   JA  20081018012054  20090501070845                                  2B  A   APEX-SBE 2412                                                   846 @�����]1   @������@8N��O�;@b�l�C��1   ARGOS   A   A   A   @�ffAffAi��A���A���A�ffB33B��B133BG33BY��Bm��B���B�  B�  B�33B�33B�33B���B�  BЙ�B���B�  B���B�33C�C�CffCffC�C��C33C$ffC)L�C.L�C3ffC8� C=� CBffCGffCQ� C[L�Ce33Co33Cy33C�s3C�� C�s3C��fC��3C�� C�� C���C��3C���C���C���C��fC�Cǀ C̀ CѦfCր CۦfC���C��C���C�3C���C�� DٚDٚD��D�3D�3D� D� D$�3D)� D.�fD3ٚD8ٚD=�3DB�3DGٚDL� DQٚDV� D[�3D`�fDe��Dj� Do�3Dt� Dy��D�&fD�i�D��3D��D��D�` D��3D���D�#3D�ffD�� D���D��D�ffDڠ D��D��D�S3D�3D�s31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��Ah  A�  A�  A홙B
��BfgB0��BF��BY34Bm34B�fgB���B���B�  B�  B�  B�fgB���B�fgBٙ�B���BB�  C  C  CL�CL�C  C� C�C$L�C)33C.33C3L�C8ffC=ffCBL�CGL�CQffC[33Ce�Co�Cy�C�ffC�s3C�ffC���C��fC��3C��3C���C��fC�� C���C�� C���C C�s3C�s3Cљ�C�s3Cۙ�C���C� C�� C�fC��C��3D�4D�4D�gD��D��DٚDٚD$��D)��D.� D3�4D8�4D=��DB��DG�4DL��DQ�4DVٚD[��D`� De�gDj��Do��Dt��Dy�4D�#3D�fgD�� D��gD��D�\�D�� D�ٚD�  D�c3D���D��D��D�c3Dڜ�D��gD��D�P D� D�p 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�!A�RA�RA�jA�5?A��A�E�A�|�A�bNA�  A���A�bNA�"�A���A�9XA��A�VA��jA���A�G�A�jA��wA�v�A��TA�ȴA�S�A��`A�VA�`BA���A�ZA�?}A��
A�ZA��+A��+A���A�K�A��jA�S�A��/A��A�n�A��-A��A�33A��A���A}Aw�#AtQ�Anv�Aj��Af��A`��AY/ASt�AN��AIAC�PA>�A<ZA:ȴA4bA.1'A*ȴA!��A�PAoAĜAZ@��@��@�1'@�\)@�9X@���@��^@���@��P@���@��m@�?}@��j@�|�@���@�(�@��^@��@z�@w�w@t1@ol�@kS�@fff@[o@Q�7@K�m@E��@:�H@41@+��@$�@  �@�@�@�@�j@	%@
=@V@G�?�j?���?�\1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�!A�RA�RA�jA�5?A��A�E�A�|�A�bNA�  A���A�bNA�"�A���A�9XA��A�VA��jA���A�G�A�jA��wA�v�A��TA�ȴA�S�A��`A�VA�`BA���A�ZA�?}A��
A�ZA��+A��+A���A�K�A��jA�S�A��/A��A�n�A��-A��A�33A��A���A}Aw�#AtQ�Anv�Aj��Af��A`��AY/ASt�AN��AIAC�PA>�A<ZA:ȴA4bA.1'A*ȴA!��A�PAoAĜAZ@��@��@�1'@�\)@�9X@���@��^@���@��P@���@��m@�?}@��j@�|�@���@�(�@��^@��@z�@w�w@t1@ol�@kS�@fff@[o@Q�7@K�m@E��@:�H@41@+��@$�@  �@�@�@�@�j@	%@
=@V@G�?�j?���?�\1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
{B
{B
oB
bB
\B
8RB
v�B
w�B
ÖB�B33BJ�BbNBn�BhsB`BB_;BVBW
BT�BP�BO�BP�BK�BD�BC�B>wB>wB8RB49B1'B0!B.B+B'�B#�B�B�B�B�BhBB  B
��B
�B
�NB
��B
�XB
��B
�7B
v�B
ZB
I�B
2-B
�B	�B	��B	ÖB	��B	�1B	u�B	l�B	bNB	B�B	'�B	�B�B��B�XB�B{�B_;BK�BE�B<jB>wBD�BVBr�B� B�DB�VB�!BB�#B�B	B	bB	�B	+B	2-B	9XB	J�B	ZB	ffB	��B	ĜB	��B	�fB	��B
B
hB
�B
0!B
=qB
F�B
M�B
YB
`BB
dZB
gmB
o�B
u�B
}�B
~�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
{B
{B
oB
bB
\B
8RB
v�B
w�B
ÖB�B33BJ�BbNBn�BhsB`BB_;BVBW
BT�BP�BO�BP�BK�BD�BC�B>wB>wB8RB49B1'B0!B.B+B'�B#�B�B�B�B�BhBB  B
��B
�B
�NB
��B
�XB
��B
�7B
v�B
ZB
I�B
2-B
�B	�B	��B	ÖB	��B	�1B	u�B	l�B	bNB	B�B	'�B	�B�B��B�XB�B{�B_;BK�BE�B<jB>wBD�BVBr�B� B�DB�VB�!BB�#B�B	B	bB	�B	+B	2-B	9XB	J�B	ZB	ffB	��B	ĜB	��B	�fB	��B
B
hB
�B
0!B
=qB
F�B
M�B
YB
`BB
dZB
gmB
o�B
u�B
}�B
~�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081018012037  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081018012054  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081018012056  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081018012058  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081018012106  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081018012106  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081018012106  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081018012106  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081018012107  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081018014700                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081021162317  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081021162326  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081021162326  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081021162327  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081021162330  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081021162330  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081021162331  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081021162331  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081021162331  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081021190725                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081021162317  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090501070609  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090501070610  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090501070610  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090501070610  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090501070611  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090501070611  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090501070611  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090501070611  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090501070611  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090501070845                      G�O�G�O�G�O�                