CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   k   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4H   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6`   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8x   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :$   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <<   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >T   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  @    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @l   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  B   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D0   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D`   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G`   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J`   CALIBRATION_DATE            	             
_FillValue                  ,  M`   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    M�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    M�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    M�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    M�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  M�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N    HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    NArgo profile    2.2 1.2 19500101000000  2900652 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               OA   JA  20080820011906  20091210050540                                  2B  A   APEX-SBE 2412                                                   846 @��W;1   @����b�@9AG�z�@b�x���1   ARGOS   A   A   A   @���AffAd��A���A���A�ffB
ffB��B2��BE��BZffBnffB�ffB�  B�ffB�  B���B���B���B���B���B���B�ffBB���CffCL�C33C�C�fC�CL�C$L�C)��C.ffC3��C8L�C=�CB33CG� CQL�C[�Ce33CoffCyL�C��3C��3C���C���C���C��fC���C���C���C¦fCǦfC̀ Cѳ3C֙�Cۀ C�� C噚C�fC���C�� C�� D�3D�fD��D�3D�fD�fD�fD:�3D=� DBٚDG� DLٚDQ��DV�3D[�fD`ٚDe�fDj�fDo� Dt� Dy�fD�0 D�` D���D��fD�#3D�i�D��fD�� D��D�i�D���D��fD�  D�s3Dک�D�ٚD��D�Y�D�fD�� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffAd��A���A���A�ffB
ffB��B2��BE��BZffBnffB�ffB�  B�ffB�  B���B���B���B���B���B���B�ffBB���CffCL�C33C�C�fC�CL�C$L�C)��C.ffC3��C8L�C=�CB33CG� CQL�C[�Ce33CoffCyL�C��3C��3C���C���C���C��fC���C���C���C¦fCǦfC̀ Cѳ3C֙�Cۀ C�� C噚C�fC���C�� C�� D�3D�fD��D�3D�fD�fD�fD:�3D=� DBٚDG� DLٚDQ��DV�3D[�fD`ٚDe�fDj�fDo� Dt� Dy�fD�0 D�` D���D��fD�#3D�i�D��fD�� D��D�i�D���D��fD�  D�s3Dک�D�ٚD��D�Y�D�fD�� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A��
A߾wA�x�AޅA܉7A��/A�VA�?}A�ZA���A��A���A��A��A��A��A�r�A�  A���A�+A�ĜA��jA���A�ZA�Q�A�r�A���A��;A�~�A�ZA�S�A��A��wA�VA�v�A��;A���A��A���A�A�hsA�l�A�VA��TA���A��G�O�A��#A|�HAxȴAt�An{Aj�9Ah1'A`1A_"�A]�AW��AR�`AO��AJ��AFĜAB  A=|�A: �A2~�A$�/A�#A+A�;A VG�O�@�-@��9@��H@���@���@�5?@�(�@���@��@���@�S�@�%@~E�@xA�@j��@_�w@T��@IX@@b@8�9@0��@)X@#�@v�@�H@V@�P@
n�@{@�?��;?��H?��+?���11111111111111111111111111111111111111111111111911111111111111111111111191111111111111111111111111111111111 A���A��
A߾wA�x�AޅA܉7A��/A�VA�?}A�ZA���A��A���A��A��A��A��A�r�A�  A���A�+A�ĜA��jA���A�ZA�Q�A�r�A���A��;A�~�A�ZA�S�A��A��wA�VA�v�A��;A���A��A���A�A�hsA�l�A�VA��TA���A��G�O�A��#A|�HAxȴAt�An{Aj�9Ah1'A`1A_"�A]�AW��AR�`AO��AJ��AFĜAB  A=|�A: �A2~�A$�/A�#A+A�;A VG�O�@�-@��9@��H@���@���@�5?@�(�@���@��@���@�S�@�%@~E�@xA�@j��@_�w@T��@IX@@b@8�9@0��@)X@#�@v�@�H@V@�P@
n�@{@�?��;?��H?��+?���11111111111111111111111111111111111111111111111911111111111111111111111191111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�\B
VB
49B
<jB
:^B
�bB
��B8RB�JB��B�=B�uB�9B��B�BYB{�Bu�Bq�BgmBbNBbNB]/BVBT�BL�BI�BG�BD�BB�B>wB=qB<jB:^B8RB9XB8RB7LB5?B1'B1'B-B$�B�BuB
=BG�O�B
�LB
��B
�1B
o�B
S�B
C�B
49B
oB
PB
B	�NB	ƨB	�LB	��B	�uB	}�B	jB	ZB	0!B��B��B��B~�BffG�O�BL�BO�B]/Br�B�\B�-B��B�mB��B	B	PB	#�B	9XB	?}B	iyB	�%B	��B	�jB	�B	�B	��B
B
{B
!�B
,B
:^B
G�B
Q�B
\)B
cTB
iyB
p�B
u�B
z�11111111111111111111111111111111111111111111111911111111111111111111111191111111111111111111111111111111111 B
�\B
VB
49B
<jB
:^B
�bB
��B8RB�JB��B�=B�uB�9B��B�BYB{�Bu�Bq�BgmBbNBbNB]/BVBT�BL�BI�BG�BD�BB�B>wB=qB<jB:^B8RB9XB8RB7LB5?B1'B1'B-B$�B�BuB
=BG�O�B
�LB
��B
�1B
o�B
S�B
C�B
49B
oB
PB
B	�NB	ƨB	�LB	��B	�uB	}�B	jB	ZB	0!B��B��B��B~�BffG�O�BL�BO�B]/Br�B�\B�-B��B�mB��B	B	PB	#�B	9XB	?}B	iyB	�%B	��B	�jB	�B	�B	��B
B
{B
!�B
,B
:^B
G�B
Q�B
\)B
cTB
iyB
p�B
u�B
z�11111111111111111111111111111111111111111111111911111111111111111111111191111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080820011854  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080820011906  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080820011908  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080820011910  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080820011917  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080820011917  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080820011918  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080820011918  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080820011918  QCF$                G�O�G�O�G�O�              40JA  ARGQrqcpt16b                                                                20080820011919  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080820013438                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080822162214  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080822162219  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080822162219  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080822162220  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080822162224  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080822162224  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080822162224  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080822162224  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080822162224  QCF$                G�O�G�O�G�O�              40JA  ARGQrqcpt16b                                                                20080822162224  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080822190642                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080822162214  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090501070555  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090501070555  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090501070555  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090501070556  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090501070557  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090501070557  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090501070557  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090501070557  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090501070557  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090501070850                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091210043245  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091210043316  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091210043317  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091210043317  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091210043317  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091210043318  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091210043318  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091210043318  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091210043318  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091210043319  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091210050540                      G�O�G�O�G�O�                