CDF   .   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900657 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               �A   JA  20080807012114  20090831075629  A9_67027_128                    2C  D   APEX-SBE 2975                                                   846 @��g�7v1   @��D���@7���S�@b#���+1   ARGOS   A   A   A   @�33AffA���A홚B  BD��Bn��B���B���B�33B�33B�33B�  C�3C� C� C  C)L�C3� C=33CGffC[��Co�C�� C���C�� C��fC�ffC�� C���Cǳ3C�� C�s3C�s3C� C��fD� D� D�3D�3D��DٚD��D$� D)ٚD.� D3�3D8��D=�3DB�fDG�3DN  DT9�DZ�3D`� Df�3DmY�Ds��Dy�3D�&fD�ffD�� D��D��D�i�D�� D�i�D��D�c3D��3D�l�D�vf111111111111111111111111111111111111111111111111111111111111111111111111@���A��A�33A�33B��BC��Bm��B�  B�33B���Bƙ�Bٙ�B�ffCffC33C33C�3C)  C333C<�fCG�C[L�Cn��C�Y�C�s3C���C�� C�@ C�Y�C��fCǌ�Cљ�C�L�C�L�C�Y�C�� D��D��D� D� D��D�fD��D$��D)�fD.��D3� D8��D=� DB�3DG� DM��DT&fDZ� D`��Df� DmFfDs�fDy� D��D�\�D��fD�� D�3D�` D��fD�` D�� D�Y�D�ٚD�c3D�l�111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�1A��A�%A���Aӕ�A��HA��A��A�bNA�n�A�Q�A��A�x�A�JA���A�dZA��9A�&�A��A�\)A��A��^A�9XA�$�A�p�A�ƨA{
=Aq|�Ak��A_p�AR^5AI�hA>�yA4ZA(��A jAl�A�AV@�E�@���@޸R@��;@�X@��@���@��@��m@�5?@�o@���@��j@�E�@���@���@��@�  @{�
@tZ@e�-@_�@Sƨ@M`B@C"�@9��@*��@|�@?}@+@
^5@ƨ@ r�111111111111111111111111111111111111111111111111111111111111111111111111A�1A��A�%A���Aӕ�A��HA��A��A�bNA�n�A�Q�A��A�x�A�JA���A�dZA��9A�&�A��A�\)A��A��^A�9XA�$�A�p�A�ƨA{
=Aq|�Ak��A_p�AR^5AI�hA>�yA4ZA(��A jAl�A�AV@�E�@���@޸R@��;@�X@��@���@��@��m@�5?@�o@���@��j@�E�@���@���@��@�  @{�
@tZ@e�-@_�@Sƨ@M`B@C"�@9��@*��@|�@?}@+@
^5@ƨ@ r�111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�B
�B
�!B
��B��B�RB��BǮB��B�RB�XB��B�oB�Bv�Bn�BgmB`BBXBL�BD�B9XB+B{B
��B
ɺB
��B
ffB
I�B
1B	��B	��B	v�B	E�B	�B�B�XB��B�Bz�Bn�BffBffBr�B~�B�B��B�'BŢB��B��B	\B	�B	�B	0!B	S�B	jB	�DB	��B	��B	�dB	��B	�)B	��B
B
�B
2-B
B�B
Q�B
\)B
gmB
n�111111111111111111111111111111111111111111111111111111111111111111111111B
�B
�B
�'B
�B��B�^B��BɺB��B�^B�jB��B�{B�%Bx�Bo�BhsB`BBYBM�BE�B:^B,B�B
��B
��B
��B
gmB
J�B

=B	��B	��B	w�B	G�B	�B�B�^B��B�B{�Bo�BgmBgmBs�B~�B�%B��B�'BƨB��B��B	\B	�B	�B	0!B	S�B	jB	�DB	��B	��B	�dB	��B	�)B	��B
B
�B
2-B
B�B
Q�B
\)B
gmB
n�111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.3(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808150232072008081502320720080815023207200808150247272008081502472720080815024727200908190000002009081900000020090819000000  JA  ARFMdecpA9_b                                                                20080807012109  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080807012114  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080807012115  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080807012115  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080807012119  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080807012119  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080807012120  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080807012120  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080807012120  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080807013522                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080811043014  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080811043019  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080811043019  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080811043020  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080811043024  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080811043024  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080811043024  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080811043024  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080811043024  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080811052210                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828051105  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828051107  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828051116  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828051116  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828051116  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828051116  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828051117  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828065343                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080811043014  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090424013311  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090424013311  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090424013311  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090424013311  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090424013312  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090424013312  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090424013312  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090424013312  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090424013312  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090424014109                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080810110814  CV  DAT$            G�O�G�O�F�5f                JM  ARCAJMQC1.0                                                                 20080815023207  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080815023207  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080815024727  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090819000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075550  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075629                      G�O�G�O�G�O�                