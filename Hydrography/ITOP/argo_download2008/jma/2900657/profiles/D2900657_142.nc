CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900657 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               �A   JA  20081016013157  20090831075643  A9_67027_142                    2C  D   APEX-SBE 2975                                                   846 @��$��.�1   @��'|e�:@8C��$�@bt�j~�1   ARGOS   A   A   A   @���AffA���A���BffBE33Bn  B���B�33B�33Bƙ�B�  B�33C� CL�C  C�C)��C3�C=L�CG�CZ�fCn�fC���C��fC��fC���C��3C���C�s3C�ffCѳ3C�� C噚C� C���DٚD� D��D��D��D��D�3D$ٚD)�fD.�fD3��D8�fD=�fDB��DGٚDN  DTL�DZ� D`ٚDf�3DmL�Ds��Dy��D�33D�i�D���D�� D��D�l�D���D�p D�� D�` D���D�c3D���D�Vf1111111111111111111111111111111111111111111111111111111111111111111111111   @���A  A���A陚B��BC��BlffB�  B�ffB�ffB���B�33B�ffC�C
�fC��C�3C)33C2�3C<�fCF�3CZ� Cn� C�ffC�s3C�s3C�ffC�� C�ffC�@ C�33Cр Cی�C�ffC�L�C�ffD� D�fD�3D�3D�3D� D��D$� D)��D.��D3�3D8��D=��DB� DG� DNfDT33DZ�fD`� DfٚDm33Dss3Dy� D�&fD�\�D���D��3D� D�` D�� D�c3D��3D�S3D�� D�VfD�� D�I�1111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�9XA�C�A�G�A�E�A�5?A�A�A�1'A�ZA�ffA�M�A��A�r�A�=qA�S�A�A�jA�A�A���A�ffA�|�A�r�A��wA��FA��^A�5?A��PA���A��hAx �Aq��Ai&�Aa/AR1'AH�jA??}A4��A+A(��A+A�hAĜA��A5?@�33@�R@�{@�K�@��/@�~�@�{@��@��D@�J@�^5@��@��P@���@��^@�@u�-@m/@f��@Z�H@Sƨ@H�`@9x�@( �@5?@��@&�@
J@�@`B1111111111111111111111111111111111111111111111111111111111111111111111111   A�9XA�C�A�G�A�E�A�5?A�A�A�1'A�ZA�ffA�M�A��A�r�A�=qA�S�A�A�jA�A�A���A�ffA�|�A�r�A��wA��FA��^A�5?A��PA���A��hAx �Aq��Ai&�Aa/AR1'AH�jA??}A4��A+A(��A+A�hAĜA��A5?@�33@�R@�{@�K�@��/@�~�@�{@��@��D@�J@�^5@��@��P@���@��^@�@u�-@m/@f��@Z�H@Sƨ@H�`@9x�@( �@5?@��@&�@
J@�@`B1111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	��B	��B	��B	��B
��B�BS�B�=B��BŢB�jB�RB�!B��B��B�%B{�Bp�B`BBW
BL�B@�B33B(�B�B
��B
�B
�9B
�%B
dZB
:^B
hB	ɺB	��B	q�B	A�B	�B	\B��B�-B��B�oB�PB�=B�PB�JBm�Bs�Bv�B�7B��B�B��B��B�BB��B	oB	�B	6FB	ZB	��B	B	�
B	�TB	�B
oB
,B
=qB
J�B
S�B
`BB
hsB
iy1111111111111111111111111111111111111111111111111111111111111111111111111   B	��B	��B	��B	��B
��B�BW
B�JB��BǮB�}B�^B�3B�B��B�+B|�Bq�BaHBXBM�BA�B33B)�B�B
��B
�B
�?B
�+B
e`B
;dB
uB	��B	��B	r�B	B�B	�B	hB��B�3B��B�uB�VB�DB�VB�PBn�Bt�Bv�B�=B��B�B��B��B�BB��B	oB	�B	6FB	ZB	��B	B	�
B	�TB	�B
oB
,B
=qB
J�B
S�B
`BB
hsB
iy1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.4(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810240208352008102402083520081024020835200810240232382008102402323820081024023238200908190000002009081900000020090819000000  JA  ARFMdecpA9_b                                                                20081016013154  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081016013157  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081016013158  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081016013158  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081016013202  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081016013202  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081016013202  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081016013202  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081016013203  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081016031058                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081020042302  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081020042307  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081020042307  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081020042308  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081020042312  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081020042312  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081020042312  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081020042312  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081020042312  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081020061717                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081020042302  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090424013342  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090424013343  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090424013343  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090424013343  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090424013344  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090424013344  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090424013344  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090424013344  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090424013344  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090424014052                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081019114301  CV  DAT$            G�O�G�O�F��8                JM  ARCAJMQC1.0                                                                 20081024020835  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081024020835  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081024023238  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090819000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075533  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075643                      G�O�G�O�G�O�                