CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901567 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20081008055825  20100219015502  A9_76255_023                    2C  D   APEX-SBE 3466                                                   846 @��/s�1   @��0HpB�@8��G�{@b>$�/�1   ARGOS   A   A   A   @���A33Ac33A���A�33A陚B33B��B2ffBFffBZffBlffB�  B�ffB���B���B���B�ffB���B�33BЙ�B�  B�33BB�  C33C� CL�C��CffCffC�C$� C)L�C.� C3ffC8ffC=  CB�CGL�CQ�C[� CeffCo  CyffC���C�� C���C�� C��3C�� C�� C���C��fC��fC��fC���C��fC C�s3Č�Cѳ3C֦fC�� C���C��C��C� C��fC���D��DٚD�fD�3DٚD� D��D$��D)��D.�3D3��D8�3D=��DBٚDG� DLٚDQ� DVٚD[�3D`� De� Dj�3Do�fDtٚDy��D�&fD�i�D���D��D��D�p D��fD�� D�#3D�p D�� D�� D�#3D�Y�Dک�D��fD�#3D�` D� D��3D��311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA��Aa��A�  A�ffA���B
��BffB2  BF  BZ  Bl  B���B�33B�ffB�ffB�ffB�33B���B�  B�ffB���B�  B�ffB���C�CffC33C� CL�CL�C  C$ffC)33C.ffC3L�C8L�C<�fCB  CG33CQ  C[ffCeL�Cn�fCyL�C���C��3C�� C��3C��fC��3C�s3C�� C���C���C���C�� C���C�s3C�ffC̀ CѦfC֙�C۳3C���C� C� C�s3C���C���D�fD�3D� D��D�3D��D�3D$�fD)�fD.��D3�fD8��D=�fDB�3DGٚDL�3DQ��DV�3D[��D`ٚDe��Dj��Do� Dt�3Dy�fD�#3D�ffD���D��fD��D�l�D��3D���D�  D�l�D���D���D�  D�VfDڦfD��3D�  D�\�D��D�� D�� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�/A�`BA�bNA�\)A�bNA�G�A�JA�=qAݰ!A�33A�A΁A�/A�C�AǮA�(�A�XA�1A��PA���A���A��A�33A�JA�E�A�ƨA��DA��A��A��mA�
=A�x�A�dZA��A��
A��mA�/A��A�ffA�{A�I�A��uA���A�dZA��A�A�A���A�;dA�^5A��A���A�/A��PA�G�A��jA|1Aw�^Ar(�An(�Ah��AdA�A]�A[+AT��AS`BAKC�AE�TABM�A>��A6��A.A�A'+A
=A�A`BA$�@��;@��@�K�@Ώ\@���@�V@��w@�ƨ@��H@���@��h@��!@�dZ@�~�@��H@���@��@�G�@}��@tI�@hQ�@Z��@K��@B�@:^5@3��@/�w@)X@$z�@�@�^@�@1'@��@ȴ@n�@ �u?��H?�$�?�`B11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�/A�`BA�bNA�\)A�bNA�G�A�JA�=qAݰ!A�33A�A΁A�/A�C�AǮA�(�A�XA�1A��PA���A���A��A�33A�JA�E�A�ƨA��DA��A��A��mA�
=A�x�A�dZA��A��
A��mA�/A��A�ffA�{A�I�A��uA���A�dZA��A�A�A���A�;dA�^5A��A���A�/A��PA�G�A��jA|1Aw�^Ar(�An(�Ah��AdA�A]�A[+AT��AS`BAKC�AE�TABM�A>��A6��A.A�A'+A
=A�A`BA$�@��;@��@�K�@Ώ\@���@�V@��w@�ƨ@��H@���@��h@��!@�dZ@�~�@��H@���@��@�G�@}��@tI�@hQ�@Z��@K��@B�@:^5@3��@/�w@)X@$z�@�@�^@�@1'@��@ȴ@n�@ �u?��H?�$�?�`B11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�
B
�
B
�
B
�
B
�
B
�B
�NB(�B�B�VB�wB�`B��B��B�yB��B��B��B�B�B�B�B��B��B�VB�JB�1B�+B�B}�Bw�Bn�Bk�BiyBdZB^5BT�BH�BD�B?}B49B-B(�B$�B!�B�B�B�B\B+B
��B
�HB
��B
�^B
��B
�=B
s�B
ZB
F�B
+B
uB	�B	�HB	ÖB	�LB	�oB	{�B	l�B	XB	6FB	bB�B��B�B��B~�Bp�BgmB`BBcTBgmBr�B�=B��B��B��B��B�BB�B	  B	hB	$�B	.B	5?B	B�B	gmB	�B	��B	�}B	�B	�B	��B
B
\B
�B
%�B
-B
6FB
A�B
K�B
XB
bNB
gmB
n�B
s�B
t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�
B
�
B
�
B
�
B
�
B
�B
�mB.B�1B�uBB�mB��B��B�BB��B��B��B�B��B�B��B��B�\B�PB�7B�1B�B~�By�Bp�Bl�Bk�BffB`BBXBI�BD�BA�B5?B-B)�B$�B!�B�B�B�B\B1B
��B
�NB
��B
�dB
��B
�DB
t�B
[#B
G�B
,B
�B	�B	�TB	ÖB	�XB	�uB	|�B	m�B	ZB	7LB	hB��B��B�!B��B� Bq�BhsBaHBdZBhsBs�B�=B��B��B��B��B�BB�B	  B	hB	$�B	.B	5?B	B�B	gmB	�B	��B	�}B	�B	�B	��B
B
\B
�B
%�B
-B
6FB
A�B
K�B
XB
bNB
gmB
n�B
s�B
t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810210229322008102102293220081021022932200810210252362008102102523620081021025236201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20081008055823  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081008055825  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081008055827  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081008055831  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081008055831  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081008055831  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081008063522                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081012052602  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081012052612  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081012052615  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081012052622  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081012052622  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081012052623  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081012072626                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081012052602  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515091844  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515091845  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515091845  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515091846  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515091846  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515091846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515091846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515091846  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515092140                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081011115344  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20081021022932  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081021022932  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081021025236  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015401  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015502                      G�O�G�O�G�O�                