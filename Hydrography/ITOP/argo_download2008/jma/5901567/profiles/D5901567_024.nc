CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901567 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20081018013509  20100219015456  A9_76255_024                    2C  D   APEX-SBE 3466                                                   846 @���T2(1   @���4��@8�bM��@b=����1   ARGOS   A   A   A   @���A��Ac33A�  A�  A���B
��BffB133BE33BZ��Bn��B�  B�33B�ffB�  B�  B���B���B�  B�  B�ffB�ffB�  B�  CL�C��CffC��CL�C� C  C$ffC)� C.L�C3�C8�C=33CBL�CGL�CQ� CZ�fCd��CoL�Cy��C�� C�� C��3C���C���C��fC���C��fC���C�ٚC��fC�� C��3C�� Cǌ�C�� Cљ�C֙�C۳3C�fC�� C�fC� C�s3C��fD��D� D�3D� D��D�3D��D$�fD)�3D.��D3�fD8� D=ٚDB� DG�3DL�3DQ�fDV��D[��D`�fDe�fDjٚDo� DtٚDy�3D��D�l�D��fD��fD��D�\�D���D��3D�#3D�ffD���D��fD�  D�i�Dڬ�D���D�#3D�\�D�D��D�6f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA33Aa��A�33A�33A�  B
ffB  B0��BD��BZffBnffB���B�  B�33B���B���B���B�ffB���B���B�33B�33B���B���C33C� CL�C� C33CffC�fC$L�C)ffC.33C3  C8  C=�CB33CG33CQffCZ��Cd�3Co33Cy� C��3C��3C��fC���C�� C���C���C���C�� C���C���C��3C��fC³3Cǀ C̳3Cь�C֌�CۦfC���C�3CꙚC�s3C�ffC���D�3DٚD��DٚD�fD��D�fD$� D)��D.�fD3� D8ٚD=�3DB��DG��DL��DQ� DV�fD[�fD`� De� Dj�3Do��Dt�3Dy��D��D�i�D��3D��3D��D�Y�D��fD�� D�  D�c3D���D��3D��D�ffDک�D��D�  D�Y�D�fD��fD�3311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�Q�A�Q�A�Q�A�S�A�VA�VA�1'A��/A�Q�Aٟ�Aش9A�dZA��Aͩ�AǗ�A��;A���A�-A���A�G�A�A�9XA���A���A��A��A�S�A�-A�\)A��FA��A��A�~�A���A���A�I�A�r�A�~�A� �A�~�A��TA���A�l�A��-A��DA��A�v�A�7LA���A���A�
=A��hA�\)A�TAy��AwC�Aq�Al�RAiXAex�Ad��A`�\A[
=AT��AM�^AH�ACƨAAt�A>�RA:�jA3�
A*�!A%`BA-A7LA�\@�E�@� �@�dZ@�ȴ@̃@��^@�1@�/@�@�t�@�/@���@��7@�1@�V@�-@���@��^@|j@qx�@ep�@X�@P�9@D9X@<�D@6�y@/�w@'K�@#�F@��@V@�@��@
M�@��@(�@ �9?�1?��u?�?}11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�Q�A�Q�A�Q�A�S�A�VA�VA�1'A��/A�Q�Aٟ�Aش9A�dZA��Aͩ�AǗ�A��;A���A�-A���A�G�A�A�9XA���A���A��A��A�S�A�-A�\)A��FA��A��A�~�A���A���A�I�A�r�A�~�A� �A�~�A��TA���A�l�A��-A��DA��A�v�A�7LA���A���A�
=A��hA�\)A�TAy��AwC�Aq�Al�RAiXAex�Ad��A`�\A[
=AT��AM�^AH�ACƨAAt�A>�RA:�jA3�
A*�!A%`BA-A7LA�\@�E�@� �@�dZ@�ȴ@̃@��^@�1@�/@�@�t�@�/@���@��7@�1@�V@�-@���@��^@|j@qx�@ep�@X�@P�9@D9X@<�D@6�y@/�w@'K�@#�F@��@V@�@��@
M�@��@(�@ �9?�1?��u?�?}11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
B
ÖB
ÖB
ÖB
ÖB
ÖB
��B|�B�%B��B��B�B�XB�B��B�bB�BbNB_;B�1B��B�7B}�Bs�Bn�BhsBe`BbNB_;BZBT�BN�BI�BC�B@�B;dB5?B1'B/B,B%�B!�B�B�B�BuBDBB
��B
�B
�5B
��B
�dB
��B
�7B
y�B
`BB
E�B
7LB
%�B
�B
%B	�mB	��B	�B	�hB	{�B	p�B	cTB	S�B	5?B	JB��B�dB��B�Bm�BffB^5B\)B]/BjBp�By�B��B�BÖB��B�B��B	{B	&�B	-B	=qB	F�B	jB	�hB	�!B	ǮB	�B	�sB	�B	��B
PB
�B
"�B
2-B
=qB
D�B
L�B
S�B
\)B
e`B
iyB
n�B
s�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
B
ÖB
ÖB
ÖB
ÖB
ĜB
�B}�B�7B��B��B�-B�dB�#B��B�hB�%BgmBcTB�PB��B�JB�Bt�Bo�BiyBgmBdZBaHB[#BVBO�BL�BD�BA�B=qB6FB1'B0!B-B&�B"�B�B�B�BuBJBB
��B
�B
�;B
��B
�jB
��B
�=B
z�B
aHB
F�B
8RB
%�B
�B
+B	�yB	��B	�B	�oB	|�B	q�B	dZB	T�B	6FB	PB��B�qB��B�Bn�BgmB_;B]/B^5BjBq�Bz�B��B�BÖB�B�B��B	{B	&�B	-B	=qB	F�B	jB	�hB	�!B	ǮB	�B	�sB	�B	��B
PB
�B
"�B
2-B
=qB
D�B
L�B
S�B
\)B
e`B
iyB
n�B
s�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810310240112008103102401120081031024011200810310247582008103102475820081031024758201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20081018013506  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081018013509  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081018013510  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081018013513  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081018013514  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081018013514  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081018014958                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081022045734  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081022045738  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081022045739  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081022045743  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081022045743  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081022045744  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081022062105                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081022045734  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515091847  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515091847  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515091847  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515091848  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515091848  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515091849  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515091849  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515091849  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515092143                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081021112930  CV  DAT$            G�O�G�O�F��W                JM  ARCAJMQC1.0                                                                 20081031024011  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081031024011  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081031024758  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015354  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015456                      G�O�G�O�G�O�                