CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901567 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080928060535  20100219015501  A9_76255_022                    2C  D   APEX-SBE 3466                                                   846 @���:�%1   @��
m�<@8�t�j~�@b;I�^1   ARGOS   A   A   A   @���A��A`  A���A�  A�ffB
ffB��B1��BF��BX��Bn  B�  B�  B�33B���B���B�33B���B�  BЙ�B���B噚BB���C  CffCL�C33C�C�C  C$� C)� C.  C3  C8  C=33CB  CG� CQ33C[33Ce33Co33CyffC��3C�� C��3C��3C���C�s3C���C��3C�� C��fC��fC���C��fC�� C���C̦fCљ�Cր CۦfC�� C��C�Y�CC�ffC��3D��D�fD� D�3D�3D��D� D$�fD)ٚD.ٚD3� D8�3D=ٚDB�3DG� DL�3DQ�fDV�fD[� D`ٚDe� Dj��DoٚDt�fDyٚD��D�P D��fD��fD��D�l�D��3D�� D�)�D�ffD�� D��3D�&fD�i�Dک�D��3D�  D�\�D�D��D�� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA��A\��A�  A�ffA���B	��B  B0��BF  BX  Bm33B���B���B���B�33B�33B���B�33Bƙ�B�33B�ffB�33B�33B�ffC ��C33C�C  C�fC�fC��C$L�C)L�C-��C2��C7��C=  CA��CGL�CQ  C[  Ce  Co  Cy33C���C��fC���C���C�s3C�Y�C�s3C���C�ffC���C���C�s3C���C¦fCǳ3Č�Cр C�ffCی�C�ffC�s3C�@ C� C�L�C���D��DٚD�3D�fD�fD� D�3D$ٚD)��D.��D3�3D8�fD=��DB�fDG�3DL�fDQ��DV��D[�3D`��De�3Dj� Do��Dt��Dy��D�fD�I�D�� D�� D�3D�ffD���D�ٚD�#3D�` D���D���D�  D�c3Dڣ3D���D��D�VfD�3D��3D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A�-A��A�n�A�9XA��HA��A�A�%A� �A��A�\)AƲ-A�1'A���A�^5A���A�~�A���A��\A�ĜA�?}A��/A�K�A�jA�r�A���A�&�A���A���A���A��A���A���A�+A��PA�&�A��A�S�A�JA�r�A�p�A�z�A���A���A�dZA� �A�XA��7A��#A�/A�%A�A|ZAwp�Ar�jAo��Al�RAg\)Ab�A\�AX��AS�-AM��AJ  ADȴA@�DA<JA8��A2^5A+�FA��A�#A9XA�@�C�@�G�@��u@��@ɺ^@�E�@��@��9@�(�@�  @��R@�-@��@�A�@��7@�ff@���@���@{o@wK�@n��@b��@U@LZ@B��@>@7�@0bN@%�@`B@Ĝ@"�@l�@j@	hs@5?@^5@ �u?��-?���?�~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A�-A��A�n�A�9XA��HA��A�A�%A� �A��A�\)AƲ-A�1'A���A�^5A���A�~�A���A��\A�ĜA�?}A��/A�K�A�jA�r�A���A�&�A���A���A���A��A���A���A�+A��PA�&�A��A�S�A�JA�r�A�p�A�z�A���A���A�dZA� �A�XA��7A��#A�/A�%A�A|ZAwp�Ar�jAo��Al�RAg\)Ab�A\�AX��AS�-AM��AJ  ADȴA@�DA<JA8��A2^5A+�FA��A�#A9XA�@�C�@�G�@��u@��@ɺ^@�E�@��@��9@�(�@�  @��R@�-@��@�A�@��7@�ff@���@���@{o@wK�@n��@b��@U@LZ@B��@>@7�@0bN@%�@`B@Ĝ@"�@l�@j@	hs@5?@^5@ �u?��-?���?�~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
��B
��B
��B
��B
�uB
u�B
�sBZB�BÖBÖB��BŢB��B��BBB��B�JB�mB�B�}B��B�'B�!B�B��B��B��B�Bu�Bw�Bl�Be`B[#BW
BN�BJ�BH�BF�B=qB/B"�B�B\B
��B
�yB
�NB
��B
B
�dB
�B
��B
�hB
w�B
aHB
Q�B
?}B
#�B

=B	�mB	��B	�}B	��B	��B	{�B	dZB	Q�B	A�B	&�B	+B�B�-B��Bw�BffBbNB]/B_;B]/BjBu�B�B�{B��BB��B�NB�B	+B	�B	�B	8RB	I�B	ZB	u�B	�oB	��B	�jB	��B	�B	�sB	��B
bB
�B
+B
8RB
B�B
I�B
Q�B
XB
bNB
gmB
hsB
n�B
o�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
��B
��B
��B
��B
��B
x�B
�B_;B�9BŢBÖBBȴBB��BĜBŢB��B�PB�B�!BB��B�3B�'B�B��B��B��B�%Bv�Bx�Bm�BffB\)BXBO�BK�BH�BG�B>wB0!B#�B�BbB
��B
�yB
�TB
��B
B
�jB
�!B
��B
�oB
x�B
bNB
R�B
@�B
$�B
JB	�sB	��B	��B	��B	��B	|�B	e`B	R�B	C�B	'�B		7B�B�3B��Bx�BgmBcTB^5B`BB]/Bk�Bv�B�B��B��BB��B�NB�B	+B	�B	�B	8RB	I�B	ZB	u�B	�oB	��B	�jB	��B	�B	�sB	��B
bB
�B
+B
8RB
B�B
I�B
Q�B
XB
bNB
gmB
hsB
n�B
o�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810110253462008101102534620081011025346200810110311412008101103114120081011031141201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20080928060513  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080928060535  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080928060539  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080928060550  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080928060550  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080928060552  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080928073604                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081002054659  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081002054708  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081002054710  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081002054717  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081002054718  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081002054719  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081002062814                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081002054659  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515091842  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515091842  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515091843  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515091844  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515091844  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515091844  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515091844  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515091844  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515092140                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20081001113102  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20081011025346  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081011025346  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081011031141  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015359  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015501                      G�O�G�O�G�O�                