CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901567 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080918051324  20100219015459  A9_76255_021                    2C  D   APEX-SBE 3466                                                   846 @��/q5y�1   @��0v�T@8�-@b6�Q�1   ARGOS   A   A   A   @�  A��Aa��A�33A�  A陚B
ffB  B1��BFffBY��Bm33B�  B�33B���B���B���B���B�  B�  B���B�  B�33B���B���CL�C33C33CL�C�fC�C33C$33C)L�C.L�C3  C833C=�CB�CG  CQ  C[ffCeffCo33Cy33C�� C���C��fC�� C�� C�� C�s3C��3C��3C���C�� C���C���C�Cǳ3C�� Cѳ3C���Cۙ�C�� C�s3C�3C�fC��fC�s3DٚD� DٚD��D�3D� D�3D$� D)�3D.� D3�fD8�3D=�fDBٚDGٚDL� DQ�fDV��D[�fD`��De�3Dj�fDo�3Dt�fDy� D�&fD�Y�D��3D��fD�#3D�i�D��3D���D�&fD�` D�� D�� D�)�D�l�Dڰ D���D�)�D�ffD�D��D�&f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���AffA^ffA���A�ffA�  B	��B33B0��BE��BX��BlffB���B���B�33B�ffB�33B�33B���Bƙ�B�ffBڙ�B���B�ffB�ffC�C  C  C�C�3C�fC  C$  C)�C.�C2��C8  C<�fCA�fCF��CP��C[33Ce33Co  Cy  C��fC�� C���C��fC��fC�ffC�Y�C���C���C�s3C�ffC�� C�s3C�s3CǙ�C̦fCљ�Cֳ3Cۀ C�ffC�Y�CꙚC��C��C�Y�D��D�3D��D� D�fD�3D�fD$�3D)�fD.�3D3��D8�fD=��DB��DG��DL�3DQٚDV� D[��D`��De�fDj��Do�fDt��Dy�3D�  D�S3D���D�� D��D�c3D���D��fD�  D�Y�D���D�ٚD�#3D�ffDک�D��fD�#3D�` D�3D��3D�  11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�r�A�z�A�|�A�x�A�|�A�bNA��A�ĜA��A�^5A�-A֍PA��#A�t�A�x�A�S�A���A���A�A�A�I�A�JA�A��!A��jA��yA�A�A�dZA��DA��mA���A���A�I�A�ĜA�C�A���A���A�x�A���A�dZA��A�C�A��RA�O�A��A��!A�ƨA�v�A��A�$�A��A��FA�oA�;A{|�Av�uAp��Ak��Ah�yAe�#A`$�AZz�AT�AP�AKt�AG�TAB��A?oA;�wA4�A1��A%?}AffA��A�A��@��@���@�V@��#@�S�@�@���@�+@�`B@��@�
=@�t�@�dZ@�p�@�~�@�Q�@��@�O�@}�h@w\)@l��@\9X@R-@I��@C�@;ƨ@5�@*��@$�@|�@��@��@�\@�P@j@�`@@^5?���?�7L?��y11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�r�A�z�A�|�A�x�A�|�A�bNA��A�ĜA��A�^5A�-A֍PA��#A�t�A�x�A�S�A���A���A�A�A�I�A�JA�A��!A��jA��yA�A�A�dZA��DA��mA���A���A�I�A�ĜA�C�A���A���A�x�A���A�dZA��A�C�A��RA�O�A��A��!A�ƨA�v�A��A�$�A��A��FA�oA�;A{|�Av�uAp��Ak��Ah�yAe�#A`$�AZz�AT�AP�AKt�AG�TAB��A?oA;�wA4�A1��A%?}AffA��A�A��@��@���@�V@��#@�S�@�@���@�+@�`B@��@�
=@�t�@�dZ@�p�@�~�@�Q�@��@�O�@}�h@w\)@l��@\9X@R-@I��@C�@;ƨ@5�@*��@$�@|�@��@��@�\@�P@j@�`@@^5?���?�7L?��y11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�uB
�uB
�uB
�uB
�oB
�B
��B%�B`BB{�B�DB�DB�bB��B�HB��B�ZB��B��B�RB�-B�B��B��B�=B� B}�B~�B�Bz�Bu�Bm�BjBdZBffBYBS�BP�BM�BJ�BA�B6FB,B�B�B\BB
��B
�B
�)B
ƨB
�3B
��B
�1B
l�B
L�B
5?B
&�B
�B
B	�sB	��B	�XB	��B	�oB	w�B	k�B	W
B	6FB	(�B��B��B�^B��B�7Bv�Bk�BcTBdZBdZBm�Bm�B� B��B�-B��B�B�B��B	DB	�B	(�B	?}B	M�B	]/B	w�B	��B	�'B	��B	��B	�NB	�B
B
bB
�B
'�B
49B
<jB
C�B
J�B
S�B
ZB
aHB
iyB
p�B
r�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�uB
�uB
�uB
�uB
�uB
�'B
��B(�BdZB~�B�JB�PB��B��B�ZB��B�fB��BÖB�^B�9B�B��B��B�JB�B~�B� B�B|�Bv�Bn�Bk�BffBhsBZBT�BQ�BM�BK�BB�B7LB-B�B�BbBB
��B
�B
�/B
ǮB
�9B
��B
�7B
m�B
M�B
6FB
'�B
�B
B	�B	��B	�^B	��B	�uB	x�B	l�B	YB	7LB	+B��B��B�dB��B�=Bw�Bl�BdZBe`Be`Bm�Bn�B�B��B�3B��B�B�B��B	DB	�B	(�B	?}B	M�B	]/B	w�B	��B	�'B	��B	��B	�NB	�B
B
bB
�B
'�B
49B
<jB
C�B
J�B
S�B
ZB
aHB
iyB
p�B
r�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200810010231052008100102310520081001023105200810010247162008100102471620081001024716201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20080918051317  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080918051324  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080918051325  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080918051333  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080918051333  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080918051334  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080918060451                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080922044333  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080922044337  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080922044337  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080922044341  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080922044342  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080922044342  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080922052248                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080922044333  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515091840  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515091840  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515091840  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515091841  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515091841  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515091841  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515091841  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515091841  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515092139                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081001023105  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20081001023105  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20081001024716  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015402  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015459                      G�O�G�O�G�O�                