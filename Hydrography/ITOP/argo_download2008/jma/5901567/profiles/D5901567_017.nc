CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901567 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080809051350  20100219015451  A9_76255_017                    2C  D   APEX-SBE 3466                                                   846 @��,��o1   @��1�s��@79�"��`@b"�+J1   ARGOS   A   A   A   @���A33Al��A���A�ffA陚B  BffB0��BD��BY33Bm33B�33B�ffB���B���B�33B�ffB���Bƙ�BЙ�B���B㙚BB�33CffC33C��C��C�C�C�C$�C)ffC.L�C3ffC8L�C=L�CA��CG33CQ33CZ�3Cd�fCn�fCy33C��3C���C���C���C���C��3C���C�� C�s3C�s3C�s3C�� C�� C�CǙ�C�� CѦfC֙�C۳3C�fC�3C�3CC�s3C��fD�fD� D��D�3D��D�fD� D$�3D)�3D.ٚD3ٚD8��D=�fDB�3DG�fDL� DQ��DV�3D[� D`��De��Dj� Do��Dt�3Dy�fD�&fD�i�D��3D��3D�)�D�Y�D���D�� D��D�l�D��fD��D�  D�i�Dڬ�D�� D�,�D�ffD��D���D�0 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A33Al��A���A�ffA陚B  BffB0��BD��BY33Bm33B�33B�ffB���B���B�33B�ffB���Bƙ�BЙ�B���B㙚BB�33CffC33C��C��C�C�C�C$�C)ffC.L�C3ffC8L�C=L�CA��CG33CQ33CZ�3Cd�fCn�fCy33C��3C���C���C���C���C��3C���C�� C�s3C�s3C�s3C�� C�� C�CǙ�C�� CѦfC֙�C۳3C�fC�3C�3CC�s3C��fD�fD� D��D�3D��D�fD� D$�3D)�3D.ٚD3ٚD8��D=�fDB�3DG�fDL� DQ��DV�3D[� D`��De��Dj� Do��Dt�3Dy�fD�&fD�i�D��3D��3D�)�D�Y�D���D�� D��D�l�D��fD��D�  D�i�Dڬ�D�� D�,�D�ffD��D���D�0 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A���A��/Aח�A�v�AǇ+A���A�^5A��+A���A���A��A��A�{A�
=A�ZA��\A�G�A�ȴA�1'A���A���A�A�  A�ĜA�5?A�VA�bA�p�A��^A��A��A�dZA��A��jA�n�A�dZA��FA���A��A�1'A�bA�p�A���A�v�A��;A��7A���A� �A��PA|~�Ay��ArJAlĜAh��Ad�yAa?}A[C�AYVATM�AL�AF��AB��A@�A@5?A??}A;oA3\)A.{A'�mAr�A��A��A �`@�+@�\)@ύP@��
@�dZ@���@��@�V@��@��9@��7@�ȴ@��@�M�@�v�@�j@~E�@v@mp�@f{@`��@Tz�@KC�@EO�@<��@6E�@.�R@+�m@%�@�R@��@|�@dZ@l�@9X@
��@ �@�@M�?��?�+?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A���A���A��/Aח�A�v�AǇ+A���A�^5A��+A���A���A��A��A�{A�
=A�ZA��\A�G�A�ȴA�1'A���A���A�A�  A�ĜA�5?A�VA�bA�p�A��^A��A��A�dZA��A��jA�n�A�dZA��FA���A��A�1'A�bA�p�A���A�v�A��;A��7A���A� �A��PA|~�Ay��ArJAlĜAh��Ad�yAa?}A[C�AYVATM�AL�AF��AB��A@�A@5?A??}A;oA3\)A.{A'�mAr�A��A��A �`@�+@�\)@ύP@��
@�dZ@���@��@�V@��@��9@��7@�ȴ@��@�M�@�v�@�j@~E�@v@mp�@f{@`��@Tz�@KC�@EO�@<��@6E�@.�R@+�m@%�@�R@��@|�@dZ@l�@9X@
��@ �@�@M�?��?�+?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
+B
,B
-B
�sBbNB�1Bx�Be`Bq�B�3B�^B�}B�3B�+B��B�-B�'B��B��B��B�\B�1B�B}�Bu�Bp�Be`BaHB^5B\)BZBW
BQ�BK�BE�B>wB<jB6FB33B.B'�B�BPB	7B
��B
�yB
�/B
��B
�9B
�B
��B
�B
`BB
F�B
1'B
�B
VB	�B	�mB	��B	�B	�hB	�B	y�B	w�B	q�B	\)B	;dB	"�B		7B�;B�FB��Bw�BaHBgmBdZBgmBq�B� B�VB��B�XB��B��B�B�B��B	
=B	&�B	,B	>wB	T�B	hsB	t�B	�uB	�B	�wB	��B	�;B	��B
\B
�B
)�B
0!B
6FB
@�B
I�B
R�B
W
B
ZB
_;B
e`B
l�B
r�B
t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
+B
-B
7LB
�BffB�7Bz�BhsBr�B�9B�}B��B�?B�=B��B�3B�-B�B��B��B�oB�=B�B~�Bv�Bs�BffBbNB_;B]/B[#BXBR�BL�BF�B?}B=qB7LB49B.B(�B�BPB
=B  B
�B
�5B
��B
�9B
�B
��B
�+B
aHB
G�B
2-B
 �B
bB	��B	�sB	��B	�B	�oB	�B	y�B	w�B	r�B	^5B	<jB	$�B	
=B�BB�LB��By�BbNBhsBe`BhsBr�B�B�\B��B�^B��B��B�B�B��B	
=B	&�B	,B	>wB	T�B	hsB	t�B	�uB	�B	�wB	��B	�;B	��B
\B
�B
)�B
0!B
6FB
@�B
I�B
R�B
W
B
ZB
_;B
e`B
l�B
r�B
t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200808220244472008082202444720080822024447200808220302142008082203021420080822030214201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20080809051344  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080809051350  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080809051351  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080809051358  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080809051358  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080809051359  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080809053524                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080813050014  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080813050019  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080813050020  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080813050025  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080813050025  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080813050025  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080813052613                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080813050014  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515091830  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515091831  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515091831  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515091832  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515091832  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515091832  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515091832  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515091832  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515092145                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080822024447  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080822024447  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080822030214  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015412  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015451                      G�O�G�O�G�O�                