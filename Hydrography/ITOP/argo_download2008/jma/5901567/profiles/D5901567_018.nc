CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901567 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20080819012735  20100219015500  A9_76255_018                    2C  D   APEX-SBE 3466                                                   846 @��J�B1   @��%��b@7O�;dZ@b)G�z�1   ARGOS   A   A   A   @�  A��Al��A�ffA���A홚B
ffB  B2ffBF  BX��Bo33B���B�  B���B�  B�  B�  B�ffB�33B�33B�33B䙚B�  B�  C  CL�C�C�fCL�CffC� C$  C)L�C.�C2�fC8�C=ffCBL�CG�CQffC[L�Ce��CoffCy� C��3C��fC�� C���C���C�s3C�� C���C��fC���C��3C���C���C³3CǦfC̙�Cљ�C֌�C۳3C�3C�s3C�fC�� C�� C���D� D�3D� D� D�fD�fD�fD$�3D)��D.ٚD3ٚD8ٚD=��DB�fDG� DL��DQ�fDV�fD[�fD`� DeٚDj�3Do�3Dt��Dy�fD�&fD�p D��3D��fD�,�D�p D���D��3D��D�i�D��3D�� D��D�ffDڰ D�� D�&fD�i�D�fD��3D�Ff11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A��Ai��A���A�33A�  B	��B33B1��BE33BX  BnffB�ffB���B�33B���B���B���B�  B���B���B���B�33BB���C ��C�C
�fC�3C�C33CL�C#��C)�C-�fC2�3C7�fC=33CB�CF�fCQ33C[�CeffCo33CyL�C���C���C�ffC�s3C�s3C�Y�C�ffC�� C���C��3C���C�� C�s3C�Cǌ�C̀ Cр C�s3Cۙ�C���C�Y�C��C�fC��fC��3D�3D�fD�3D�3D��D��D��D$�fD)��D.��D3��D8��D=� DB��DG�3DL� DQ��DV��D[��D`�3De��Dj�fDo�fDt� Dy��D�  D�i�D���D�� D�&fD�i�D��3D���D�fD�c3D���D�ٚD�fD�` Dک�D�ٚD�  D�c3D� D���D�@ 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A��/A�ƨA�DA�t�A�"�A�t�A�^5A���A�t�A��A�S�A���A���A�M�A��A�ffA�I�A�l�A�z�A�{A�{A�oA�(�A�dZA��A��A��A�G�A�O�A�^5A�l�A�jA�l�A��A���A�VA�t�A�-A��mA�1'A�ȴA�ZA�{A�n�A���A���A��/A��yA��A�M�A}�mAz1'Aw�Ar�`Amx�Aj�DAdn�A`��A]��AY�TAV$�AQALAE?}A?hsA<��A7VA2�\A-�FA'ƨA�wA�A
��A   @�j@��@ӍP@�-@�A�@�@�&�@�;d@��^@���@�z�@���@��@�O�@��@w
=@o|�@kC�@f�@`b@Rn�@H��@D�@>v�@5@.�R@(bN@#33@V@��@�@bN@dZ@�@�h@�H@%?�j?��u?�Z?��
11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A��/A�ƨA�DA�t�A�"�A�t�A�^5A���A�t�A��A�S�A���A���A�M�A��A�ffA�I�A�l�A�z�A�{A�{A�oA�(�A�dZA��A��A��A�G�A�O�A�^5A�l�A�jA�l�A��A���A�VA�t�A�-A��mA�1'A�ȴA�ZA�{A�n�A���A���A��/A��yA��A�M�A}�mAz1'Aw�Ar�`Amx�Aj�DAdn�A`��A]��AY�TAV$�AQALAE?}A?hsA<��A7VA2�\A-�FA'ƨA�wA�A
��A   @�j@��@ӍP@�-@�A�@�@�&�@�;d@��^@���@�z�@���@��@�O�@��@w
=@o|�@kC�@f�@`b@Rn�@H��@D�@>v�@5@.�R@(bN@#33@V@��@�@bN@dZ@�@�h@�H@%?�j?��u?�Z?��
11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
VB
Q�B
XB
�BB	7B�BuBbB�B'�B,B49B6FBD�BK�BZBaHBhsBl�Bk�BjBffBbNB`BB_;B^5B^5B\)B[#BZBT�BW
BM�BK�BJ�BG�BC�B=qB6FB-B%�B�BuB+B
��B
�B
�5B
��B
�!B
��B
��B
�%B
y�B
bNB
I�B
8RB
�B

=B	��B	�B	�B	�wB	��B	�PB	v�B	iyB	P�B	:^B	#�B	1B��B�B��B�Br�BdZBe`BcTBq�B�B�oB��B�RB��B�NB�B��B	�B	/B	>wB	T�B	bNB	q�B	� B	��B	��B	�B	�yB	��B
%B
VB
�B
%�B
0!B
6FB
?}B
K�B
T�B
[#B
cTB
gmB
l�B
q�B
v�B
v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
XB
R�B
]/B
�3BBPB�B�BoB�B(�B0!B9XB9XBF�BM�B\)BdZBjBl�Bl�Bk�BgmBdZBbNB_;B_;B_;B]/B\)B[#BVBYBN�BK�BJ�BH�BD�B>wB7LB.B&�B�B{B1B
��B
�B
�;B
��B
�!B
��B
��B
�+B
z�B
cTB
J�B
:^B
�B
DB	��B	�B	�B	�}B	��B	�\B	w�B	jB	Q�B	;dB	$�B	
=B��B�B��B�Bs�Be`BffBdZBr�B�B�uB��B�RB��B�NB�B��B	�B	/B	>wB	T�B	bNB	q�B	� B	��B	��B	�B	�yB	��B
%B
VB
�B
%�B
0!B
6FB
?}B
K�B
T�B
[#B
cTB
gmB
l�B
q�B
v�B
v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809010228512008090102285120080901022851200809010241242008090102412420080901024124201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20080819012733  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080819012735  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080819012736  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080819012740  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080819012740  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080819012741  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080819014019                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080823042927  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080823042930  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080823042931  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080823042935  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080823042935  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080823042935  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080823050914                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080823042927  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090515091833  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090515091833  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090515091833  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090515091834  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090515091834  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090515091835  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090515091835  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090515091835  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090515092139                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080822114445  CV  DAT$            G�O�G�O�F�M�                JM  ARCAJMQC1.0                                                                 20080901022851  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080901022851  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080901024124  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015404  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015500                      G�O�G�O�G�O�                