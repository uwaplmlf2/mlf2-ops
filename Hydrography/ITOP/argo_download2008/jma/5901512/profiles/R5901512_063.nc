CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901512 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ?A   JA  20081014055733  20090422063208                                  2B  A   APEX-SBE 2805                                                   846 @����1   @����a@4���+@aHr� Ĝ1   ARGOS   A   A   A   @�33AffAk33A���A�33A홚B	33B33B2ffBE33BZffBn��B���B�33B���B�  B�33B�  B�ffB�33B�33B�  B�33B�  B�33C33C33C� C� C  CL�C�C$  C)ffC.�C3L�C8L�C=L�CB33CG�CQL�C[ffCe33Co33Cy��C���C�� C��3C���C�s3C���C��3C��fC��3C��fC�� C�� C�� C�� CǙ�C̦fCр C֙�CۦfC���C�3CꙚC�fC�� C�� D�3D�3D�3DٚDٚD� D� D$��D)� D.��D3� D8��D=��DB� DG� DL� DQ� DV�fD[�fD`�3De� DjٚDo� Dt��Dy��D�&fD�i�D��fD��fD�  D�p D��3D��fD�fD�i�D��fD�� D��D�l�Dڬ�D��3D�&fD�` D�3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33AffAk33A���A�33A홚B	33B33B2ffBE33BZffBn��B���B�33B���B�  B�33B�  B�ffB�33B�33B�  B�33B�  B�33C33C33C� C� C  CL�C�C$  C)ffC.�C3L�C8L�C=L�CB33CG�CQL�C[ffCe33Co33Cy��C���C�� C��3C���C�s3C���C��3C��fC��3C��fC�� C�� C�� C�� CǙ�C̦fCр C֙�CۦfC���C�3CꙚC�fC�� C�� D�3D�3D�3DٚDٚD� D� D$��D)� D.��D3� D8��D=��DB� DG� DL� DQ� DV�fD[�fD`�3De� DjٚDo� Dt��Dy��D�&fD�i�D��fD��fD�  D�p D��3D��fD�fD�i�D��fD�� D��D�l�Dڬ�D��3D�&fD�` D�3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A�"�A�FA�PA��yA���A�JA�l�A���A��+A�S�A�`BA���A��PA�A���A�  A�bNA�JA���A�~�A��A��9A���A�?}A�-A��+A���A�
=A�G�A��yA�JA���A��+A�ĜA�33A�x�A�{A�"�A�v�A�oA��A��A��7A�r�A��HA�{A`BA|�HAzJAv�ArȴAn1Ah�!Ad  AY33AT�AM��AJ��AD��A?t�A7��A5�#A3C�A/�mA,bA';dA �+A/A�A��AE�A 1@��@�
=@�@�V@�+@�S�@�Q�@�@��P@�ff@���@�(�@��@���@��@�;d@��@��7@���@�33@�9X@z��@n�+@c��@Y�7@O�@E`B@>v�@7��@/�@,Z@&�+@#33@l�@z�@��@ff@��@l�@��@	7L@1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A�"�A�FA�PA��yA���A�JA�l�A���A��+A�S�A�`BA���A��PA�A���A�  A�bNA�JA���A�~�A��A��9A���A�?}A�-A��+A���A�
=A�G�A��yA�JA���A��+A�ĜA�33A�x�A�{A�"�A�v�A�oA��A��A��7A�r�A��HA�{A`BA|�HAzJAv�ArȴAn1Ah�!Ad  AY33AT�AM��AJ��AD��A?t�A7��A5�#A3C�A/�mA,bA';dA �+A/A�A��AE�A 1@��@�
=@�@�V@�+@�S�@�Q�@�@��P@�ff@���@�(�@��@���@��@�;d@��@��7@���@�33@�9X@z��@n�+@c��@Y�7@O�@E`B@>v�@7��@/�@,Z@&�+@#33@l�@z�@��@ff@��@l�@��@	7L@1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B��B��B	aHB	�NB
��B
�mB
�B{BB�BJ�BcTBk�B�\B��B�hB�bB��B��B�B��B��B��B�DBv�BhsBe`B_;B_;B\)B[#BVBR�BM�BJ�BG�BC�B<jB8RB5?B/B$�B�B
��B
�NB
��B
�!B
�B
��B
�VB
z�B
jB
P�B
9XB
 �B	�B	��B	�-B	��B	{�B	dZB	?}B	8RB	1'B	%�B	�B	B�B�
BɺB�LB��B��B��B�'B�LB�LB�RB�}B��B��B	�B	!�B	A�B	K�B	\)B	p�B	}�B	�DB	�hB	��B	�B	�jB	ÖB	��B	�ZB	�B
B
JB
�B
+B
49B
B�B
I�B
T�B
XB
^5B
bNB
gmB
k�B
n�B
t�B
x�B
|�B
� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B��B��B	aHB	�NB
��B
�mB
�B{BB�BJ�BcTBk�B�\B��B�hB�bB��B��B�B��B��B��B�DBv�BhsBe`B_;B_;B\)B[#BVBR�BM�BJ�BG�BC�B<jB8RB5?B/B$�B�B
��B
�NB
��B
�!B
�B
��B
�VB
z�B
jB
P�B
9XB
 �B	�B	��B	�-B	��B	{�B	dZB	?}B	8RB	1'B	%�B	�B	B�B�
BɺB�LB��B��B��B�'B�LB�LB�RB�}B��B��B	�B	!�B	A�B	K�B	\)B	p�B	}�B	�DB	�hB	��B	�B	�jB	ÖB	��B	�ZB	�B
B
JB
�B
+B
49B
B�B
I�B
T�B
XB
^5B
bNB
gmB
k�B
n�B
t�B
x�B
|�B
� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081014055731  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081014055733  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081014055734  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081014055738  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081014055738  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081014055739  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081014062731                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081018042349  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081018042353  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081018042354  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081018042358  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081018042358  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081018042358  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081018061845                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081018042349  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422063015  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422063016  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422063016  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422063017  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422063017  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422063017  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422063017  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422063017  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422063208                      G�O�G�O�G�O�                