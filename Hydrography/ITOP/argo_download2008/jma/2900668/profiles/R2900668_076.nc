CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900668 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               LA   JA  20081003055345  20090522051338                                  2B  A   APEX-SBE 2347                                                   846 @������1   @������@;���-V@b��\(��1   ARGOS   A   A   A   @���AffAk33A���A���A���B
  B��B333BE��BXffBl  B�33B���B�ffB�33B�33B���B���B�  B�  Bڙ�B�33B�ffB���C  C33C
�fC33C� CffC��C$33C)�C.� C3� C8�C=ffCBL�CGL�CQ  C[�Cd�fCoL�CyL�C�s3C���C���C�� C���C�� C�ٚC���C���C��3C���C���C���C¦fCǀ C̀ CѦfC�s3C�s3C�fC�3C�fC�� C��3C���D�fD�3D��D��D� DٚD��D$�fD)��D.ٚD3�3D8�3D=ٚDB�3DG�fDL� DQ��DV�fD[��D`ٚDe�fDjٚDo�3Dt�fDy� D�)�D�p D�� D��D�&fD�c3D���D���D�&fD�i�D��fD��3D�)�D�c3Dڬ�D�ٚD��D�i�D�fD�6f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��Ai��A���A�  A�  B	��B34B2��BE34BX  Bk��B�  B�fgB�33B�  B�  B�fgB���B���B���B�fgB�  B�33B���C �fC�C
��C�CffCL�C� C$�C)  C.ffC3ffC8  C=L�CB33CG33CP�fC[  Cd��Co33Cy33C�ffC�� C���C��3C���C��3C���C�� C�� C��fC�� C���C�� C�C�s3C�s3Cљ�C�ffC�ffC���C�fCꙙC�3C��fC�� D� D��D�4D�gD��D�4D�gD$� D)�4D.�4D3��D8��D=�4DB��DG� DLٚDQ�gDV� D[�4D`�4De� Dj�4Do��Dt� Dy��D�&gD�l�D���D��gD�#3D�` D���D��D�#3D�fgD��3D�� D�&gD�` Dک�D��gD��D�fgD�3D�331111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�-A�/A�5?A�33A�+A��A���A���A�ȴA�O�A��RA���A� �A��FA��A��A�dZA�%A�x�A���A�oA�5?A���A��^A�G�A�v�A�=qA��TA���A�(�A���A��A�/A�l�A��A�S�A���A�VA��FA�bNA���A���A�%A�jA��hA��A�$�A��wA���A��-A�C�A�\)A~��Azz�Au"�Aq��Am
=Ajn�Ae��Ab�DA`�A]AX��AS�PAM�7AH��AC33A>�uA:�A6��A*�+A+A�hAn�Aƨ@�9@�\)@�^5@Χ�@�ƨ@���@�^5@��
@���@���@�n�@�;d@���@�Z@���@��9@�?}@�`B@|��@w
=@l9X@b~�@Y%@R�@I�@C33@9�7@0�@(��@#@`B@bN@9X@�@�
@Q�@/@�?�p�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�-A�/A�5?A�33A�+A��A���A���A�ȴA�O�A��RA���A� �A��FA��A��A�dZA�%A�x�A���A�oA�5?A���A��^A�G�A�v�A�=qA��TA���A�(�A���A��A�/A�l�A��A�S�A���A�VA��FA�bNA���A���A�%A�jA��hA��A�$�A��wA���A��-A�C�A�\)A~��Azz�Au"�Aq��Am
=Ajn�Ae��Ab�DA`�A]AX��AS�PAM�7AH��AC33A>�uA:�A6��A*�+A+A�hAn�Aƨ@�9@�\)@�^5@Χ�@�ƨ@���@�^5@��
@���@���@�n�@�;d@���@�Z@���@��9@�?}@�`B@|��@w
=@l9X@b~�@Y%@R�@I�@C33@9�7@0�@(��@#@`B@bN@9X@�@�
@Q�@/@�?�p�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�3B	�3B	�-B	�-B	�!B	�#B
�7B1'B[#Bm�BiyBe`B]/BgmBk�B^5BbNBiyBdZBQ�BD�B?}B?}B:^B=qB7LB8RB5?B33B2-B0!B-B)�B'�B$�B!�B�B�B�B�B\BDB+BB
��B
��B
�B
�B
�yB
�5B
��B
�RB
��B
�JB
t�B
cTB
J�B
<jB
(�B
�B
oB
B	�B	��B	�-B	��B	� B	k�B	W
B	E�B	PB�B�B�7B`BBD�B9XB7LB9XB7LBB�BT�BaHBr�B�B��B��B�^B��B�ZB�B	+B	 �B	33B	E�B	r�B	�bB	�B	��B	��B	�/B	�B	��B
DB
�B
$�B
.B
9XB
D�B
L�B
VB
]/B
e`B
l�B
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�3B	�3B	�-B	�-B	�!B	�#B
�7B1'B[#Bm�BiyBe`B]/BgmBk�B^5BbNBiyBdZBQ�BD�B?}B?}B:^B=qB7LB8RB5?B33B2-B0!B-B)�B'�B$�B!�B�B�B�B�B\BDB+BB
��B
��B
�B
�B
�yB
�5B
��B
�RB
��B
�JB
t�B
cTB
J�B
<jB
(�B
�B
oB
B	�B	��B	�-B	��B	� B	k�B	W
B	E�B	PB�B�B�7B`BBD�B9XB7LB9XB7LBB�BT�BaHBr�B�B��B��B�^B��B�ZB�B	+B	 �B	33B	E�B	r�B	�bB	�B	��B	��B	�/B	�B	��B
DB
�B
$�B
.B
9XB
D�B
L�B
VB
]/B
e`B
l�B
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081003055342  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081003055345  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081003055346  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081003055350  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081003055350  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081003055350  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081003063315                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081007040625  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081007040631  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081007040632  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081007040639  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081007040639  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081007040640  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20081007040640  CV  TIME            G�O�G�O�F���                JA  ARGQrelo2.1                                                                 20081007040640  CV  LAT$            G�O�G�O�A�|�                JA  ARGQrelo2.1                                                                 20081007040640  CV  LON$            G�O�G�O�C�{                JA  ARUP                                                                        20081007063313                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081007040625  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090522045542  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090522045542  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090522045543  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090522045544  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090522045544  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090522045544  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090522045544  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090522045544  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090522051338                      G�O�G�O�G�O�                