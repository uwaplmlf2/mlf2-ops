CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901523 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               8A   JA  20080905042425  20091208062804                                  2B  A   APEX-SBE 2808                                                   846 @����a1   @����L�A@4�����@`��j~��1   ARGOS   A   A   A   @�33A33Ak33A�ffA�ffA홚B
ffB33B3��BFffBZffBlffB���B���B���B�ffB���B�  B���B�  B���B�  B���B�33B���CL�C  C33C33C�fC�fC�fC$ffC)ffC.ffC3ffC8L�C=L�CB�CG� CQL�CZ��Ce33Cn�fCy�C�� C��3C��3C���C���C���C�� C�s3C�s3C�� C���C��fC���C�CǦfC�� Cѳ3C֌�Cی�C���C�s3CꙚC�s3C�s3C��3D�3D� D�fD��D�3D� DٚD$�fD)�3D.�3D3�3D8��D=��DB��DG��DLٚDQٚDV�fD[�3D`ٚDe��Dj�3Do�3Dt� Dy��D�&fD�i�D���D�� D��D�c3D�� D��D�#3D�i�D�� D���D�#3D�` Dڬ�D��D�#3D�Y�D�fD��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffA^ffA�  A�  A�34B33B  B0fgBC33BW33Bi33B~fgB�33B�33B���B�33B�ffB�  B�ffB�33B�ffB�33B홙B�  C � C33C
ffCffC�C�C�C#��C(��C-��C2��C7� C<� CAL�CF�3CP� CZ  CdffCn�CxL�C��C�L�C�L�C�34C�&gC�34C��C��C��C�Y�C�fgC�@ C�&gC�34C�@ C�Y�C�L�C�&gC�&gC�&gC��C�34C��C��C�L�D� D��D�3D��D� D��D�gD$�3D)� D.� D3� D8��D=�gDB�gDG��DL�gDQ�gDV�3D[� D`�gDe��Dj� Do� Dt��Dy��D��D�P D��3D��fD�  D�I�D��fD�� D�	�D�P D��fD��3D�	�D�FfDړ3D�� D�	�D�@ D�|�D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��FA��RA��A�G�A�oA���A�A���A��HA�JA�=qA���A�1'Aś�A���Aß�A�I�A��A���A�+A���A�-A��!A��A���A�p�A�VA�n�A�S�A���A�?}A��mA�C�A�$�A�ƨA���A�G�A�XA�A�A��PA��A�  A�E�A�~�A��A�C�A�33A��A�`BA�JA�ƨA�ȴA��PA��-A}XAx�/Aq`BAo�Am�^Ai��Ac�#Aap�AY�TASAJ�AEx�A?"�A;?}A6��A/�7A#hsA|�AAffAhs@��@�J@�X@�`B@�l�@�V@��@�x�@� �@���@���@��@��^@��@��R@�|�@�/@��@�C�@~V@p1'@cS�@X1'@M�T@G�P@<9X@6�R@0Ĝ@,I�@(�u@$z�@��@Z@�@1@ �@�@	x�@�T@S�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��FA��RA��A�G�A�oA���A�A���A��HA�JA�=qA���A�1'Aś�A���Aß�A�I�A��A���A�+A���A�-A��!A��A���A�p�A�VA�n�A�S�A���A�?}A��mA�C�A�$�A�ƨA���A�G�A�XA�A�A��PA��A�  A�E�A�~�A��A�C�A�33A��A�`BA�JA�ƨA�ȴA��PA��-A}XAx�/Aq`BAo�Am�^Ai��Ac�#Aap�AY�TASAJ�AEx�A?"�A;?}A6��A/�7A#hsA|�AAffAhs@��@�J@�X@�`B@�l�@�V@��@�x�@� �@���@���@��@��^@��@��R@�|�@�/@��@�C�@~V@p1'@cS�@X1'@M�T@G�P@<9X@6�R@0Ĝ@,I�@(�u@$z�@��@Z@�@1@ �@�@	x�@�T@S�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B/B0!B�B	JB	>wB	]/B	��B	�FB	�TB
F�B
�RB
�BoB=qBZBjB �B1BS�BhsB[#BE�BM�BR�BVBYBZBT�BVB]/Be`Be`BgmB_;B^5BZBZBW
BW
BVBT�BN�BM�BF�B@�B7LB7LB"�BoB+B
��B
�HB
��B
�?B
��B
|�B
XB
N�B
D�B
.B
oB
B	�5B	�qB	�{B	z�B	bNB	P�B	>wB	#�B	B�B�ZBB�B��B��B��B��B�-B�BÖB��B�5B�B	JB	 �B	/B	49B	C�B	YB	m�B	�B	��B	�B	ĜB	�B	�fB	��B

=B
�B
"�B
33B
<jB
E�B
L�B
VB
[#B
aHB
ffB
k�B
o�B
t�B
y�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B/B0!B�B	JB	>wB	]/B	��B	�FB	�TB
F�B
�RB
�BoB=qBZBjB �B1BS�BhsB[#BE�BM�BR�BVBYBZBT�BVB]/Be`Be`BgmB_;B^5BZBZBW
BW
BVBT�BN�BM�BF�B@�B7LB7LB"�BoB+B
��B
�HB
��B
�?B
��B
|�B
XB
N�B
D�B
.B
oB
B	�5B	�qB	�{B	z�B	bNB	P�B	>wB	#�B	B�B�ZBB�B��B��B��B��B�-B�BÖB��B�5B�B	JB	 �B	/B	49B	C�B	YB	m�B	�B	��B	�B	ĜB	�B	�fB	��B

=B
�B
"�B
33B
<jB
E�B
L�B
VB
[#B
aHB
ffB
k�B
o�B
t�B
y�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080904105656  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080905042425  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080905042425  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080905042429  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080905042430  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080905042430  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080905051149                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080904105656  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519072757  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519072757  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519072758  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519072759  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519072759  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519072759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519072759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519072759  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519073003                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208051906  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208052132  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208052132  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208052133  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208052134  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208052134  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208052134  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208052134  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208052134  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208062804                      G�O�G�O�G�O�                