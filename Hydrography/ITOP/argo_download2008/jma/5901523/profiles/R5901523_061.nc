CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   j   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D<   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G<   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J<   CALIBRATION_DATE            	             
_FillValue                  ,  M<   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Mh   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Ml   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Mp   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Mt   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Mx   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901523 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               =A   JA  20081027042536  20091208062819                                  2B  A   APEX-SBE 2808                                                   846 @��\8�91   @��\`�a@5�Ƨ@`��hr�1   ARGOS   A   A   A   A�33A�33B	33B��B2  BF��BY��Bn  B�  B�33B���B�33B�33B�33B�  B�33BЙ�B�ffB�33CL�C  C� CffC$ffC)��C.ffC3L�C8� C=�CBffCG�CQL�C[�Ce  Co� CyffC��fC�� C�� C���C��3C��3C���C��3C�� C��3C���C��3C���C¦fC�� C̳3C�s3C֙�Cۙ�C�� C噚C�� C�� C��3C���D��D�3D�3D�3D�3D�3D� D$�fD)ٚD.� D3��D8��D=ٚDB� DGٚDLٚDQ��DV�3D[�3D`�3De� Dj� Do�fDt�3Dy�fD�#3D�c3D�� D��D�#3D�c3D�� D�� D�#3D�` D��3D���D��D�i�Dڣ3D��D��D�i�D�D�p 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A���A噙BffB  B/33BD  BV��Bk33B33B���B�34B���B���B���B���B���B�34B�  B���C��CL�C��C�3C#�3C(�gC-�3C2��C7��C<fgCA�3CFfgCP��CZfgCdL�Cn��Cx�3C�L�C�ffC�&fC�@ C�Y�C�Y�C�33C�Y�C�&fC�Y�C�@ C�Y�C�@ C�L�C�ffC�Y�C��C�@ C�@ C�&fC�@ C�ffC�ffC�Y�C�@ D� D�fD�fD�fD�fD�fD�3D$��D)��D.�3D3� D8� D=��DB�3DG��DL��DQ��DV�fD[�fD`�fDe�3Dj�3Do��Dt�fDy��D��D�L�D���D��4D��D�L�D���D�ɚD��D�I�D���D��gD�gD�S4Dڌ�D��4D�gD�S4D�4D�Y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��`A���A�9XA��A�VA���A���A���AׅAՏ\A���AЁA�C�A�A�A��HAə�A�x�A���A��A�dZA��A���A�C�A���A��9A�=qA�v�A�1'A��
A�M�A��A���A���A��A�A�VA�A�l�A���A�(�A�;dA�ZA��\A�l�A�O�A�TA|�Aw��Aq?}AmdZAf�\A]�7AX�`AVbNAM��AH��AC��A>�!A85?A2�+A/�7A'?}A�+AA�\At�@��@�@�-@׮@�p�@�v�@��+@�p�@���@��m@�Z@�&�@�bN@��@���@��@�1@��@�"�@��@x1'@t1@f��@Y�^@Q��@E@<��@6�y@1��@,z�@'�;@$�@�;@dZ@�+@@\)@p�@	�@1'1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A��`A���A�9XA��A�VA���A���A���AׅAՏ\A���AЁA�C�A�A�A��HAə�A�x�A���A��A�dZA��A���A�C�A���A��9A�=qA�v�A�1'A��
A�M�A��A���A���A��A�A�VA�A�l�A���A�(�A�;dA�ZA��\A�l�A�O�A�TA|�Aw��Aq?}AmdZAf�\A]�7AX�`AVbNAM��AH��AC��A>�!A85?A2�+A/�7A'?}A�+AA�\At�@��@�@�-@׮@�p�@�v�@��+@�p�@���@��m@�Z@�&�@�bN@��@���@��@�1@��@�"�@��@x1'@t1@f��@Y�^@Q��@E@<��@6�y@1��@,z�@'�;@$�@�;@dZ@�+@@\)@p�@	�@1'1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�B	"�B	VB	ZB	gmB	��B	�;B
@�B
YB
p�B
�7B
�B;dBT�B^5B��B"�B��B�B��B�B��B��B��B�9B��B�JB�\B�{B�oB�\Bt�BbNBYBN�BK�B?}B2-B'�B�B1B
��B
�sB
��B
�}B
��B
�bB
t�B
W
B
C�B
�B	�B	�5B	��B	��B	�{B	|�B	gmB	L�B	7LB	-B	DB�B��B��B�B��B��B�B��B�LB�jBǮB�B�ZB	  B	DB	 �B	2-B	D�B	ZB	m�B	t�B	�B	�\B	��B	�?B	��B	�B	�B
B
\B
�B
"�B
2-B
9XB
D�B
I�B
P�B
XB
`BB
ffB
k�B
n�B
s�B
v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B	�B	"�B	VB	ZB	gmB	��B	�;B
@�B
YB
p�B
�7B
�B;dBT�B^5B��B"�B��B�B��B�B��B��B��B�9B��B�JB�\B�{B�oB�\Bt�BbNBYBN�BK�B?}B2-B'�B�B1B
��B
�sB
��B
�}B
��B
�bB
t�B
W
B
C�B
�B	�B	�5B	��B	��B	�{B	|�B	gmB	L�B	7LB	-B	DB�B��B��B�B��B��B�B��B�LB�jBǮB�B�ZB	  B	DB	 �B	2-B	D�B	ZB	m�B	t�B	�B	�\B	��B	�?B	��B	�B	�B
B
\B
�B
"�B
2-B
9XB
D�B
I�B
P�B
XB
`BB
ffB
k�B
n�B
s�B
v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081024125707  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081027042536  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081027042537  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081027042541  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081027042541  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081027042542  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20081027042542  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20081027042542  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081027062204                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081024125707  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519072810  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519072810  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519072810  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519072811  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090519072811  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519072811  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519072811  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519072811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090519072811  QCF$                G�O�G�O�G�O�            4100JA  ARGQaqcp2.8b                                                                20090519072811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519072811  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20090519072812  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519073014                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208051906  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208052134  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208052135  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208052135  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208052136  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208052136  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208052137  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208052137  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208052137  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208062819                      G�O�G�O�G�O�                