CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901523 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               6A   JA  20080812011947  20090519073005                                  2B  A   APEX-SBE 2808                                                   846 @���Otn�1   @��Ԝ��@5r� ě�@`�=p��
1   ARGOS   A   A   A   @�ffAffAk33A�ffA�33A陚B
  BffB1��BE��BZ��BnffB�33B���B���B���B�  B�33B�33B�33B�  B���B�  B���B�ffCffC33C33C�C� C�C�fC$�C)ffC.33C3� C833C=��CB� CG��CQ�C[�CeL�Co  Cy33C�� C�� C��3C���C���C�� C�� C���C�� C��3C�s3C��3C��fC¦fCǦfC̀ Cь�Cր Cۙ�C�3C��C�fC��C��C��3D�3D� D�3D�fDٚD�3DٚD$��D)��D.� D3��D8ٚD=ٚDB�3DG��DL� DQ� DV� D[�fD`�3De�3Dj�3DoٚDtٚDy� D�)�D�i�D��fD��fD�&fD�ffD���D��fD��D�l�D��fD�ٚD�)�D�ffDک�D��D�#3D�c3D�fD�` 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��A33A`  A���A���A�  B33B��B.��BB��BX  Bk��B��B�fgB�34B�34B���B���B���B���Bϙ�B�fgB㙚B�fgB�  C �3C� C
� CfgC��CfgC33C#fgC(�3C-� C2��C7� C<�gCA��CF�gCPfgCZfgCd��CnL�Cx� C�&fC�ffC�Y�C�@ C�s3C�&fC�ffC�33C�&fC�Y�C��C�Y�C�L�C�L�C�L�C�&fC�33C�&fC�@ C�Y�C�33C�L�C�33C�33C�Y�D�fD�3D�fD��D��D�fD��D$� D)� D.�3D3� D8��D=��DB�fDG� DL�3DQ�3DV�3D[��D`�fDe�fDj�fDo��Dt��Dy�3D�4D�S4D�� D�� D� D�P D��gD�� D�gD�VgD�� D��4D�4D�P Dړ4D��4D��D�L�D� D�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�E�A�
=A��A蕁A�9A��`A� �A߅A�A�A���A�jA�O�A�hsA�x�A���A��
A�jA�oA�1A�33A�;dA��;A��wA���A�p�A��A�`BA��A��A��A�+A���A�JA�A�=qA��RA�M�A��A��A�E�A���A�;dA�E�A���A���A�%A���A���A�I�A���A��/A�1'A�hsA~v�Az1'Au��Ao�;Am��AiVAg�A_�-AY�AW`BAS��AMVAG�PA@jA=+A3��A)��At�A%A{A	�TA �/@��-@�@�V@�@���@�ȴ@���@��@�~�@�+@�ȴ@�hs@�  @��m@�t�@�hs@�C�@��@z�\@m�-@ax�@QG�@IX@A%@;�F@7�P@1��@-V@'\)@!�#@\)@(�@1'@��@��@�@	7L@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A�E�A�
=A��A蕁A�9A��`A� �A߅A�A�A���A�jA�O�A�hsA�x�A���A��
A�jA�oA�1A�33A�;dA��;A��wA���A�p�A��A�`BA��A��A��A�+A���A�JA�A�=qA��RA�M�A��A��A�E�A���A�;dA�E�A���A���A�%A���A���A�I�A���A��/A�1'A�hsA~v�Az1'Au��Ao�;Am��AiVAg�A_�-AY�AW`BAS��AMVAG�PA@jA=+A3��A)��At�A%A{A	�TA �/@��-@�@�V@�@���@�ȴ@���@��@�~�@�+@�ȴ@�hs@�  @��m@�t�@�hs@�C�@��@z�\@m�-@ax�@QG�@IX@A%@;�F@7�P@1��@-V@'\)@!�#@\)@(�@1'@��@��@�@	7L@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B�B�B�!BȴB�B��B	49B	gmB	�B	�9B	ɺB
�B
dZB
dZB
��B
��B\B@�B?}BB�Be`Bv�B��B�B��B�hB�B�B�B�+B�7B�7B�%B�%B�+B�%B�B� B{�Bn�BbNBo�Bl�BdZBW
BI�B?}B0!B�B\B
��B
�sB
��B
�qB
��B
�7B
s�B
VB
J�B
33B
$�B
  B	�NB	��B	B	��B	�7B	l�B	]/B	2-B	hB�B�#BǮB�dB�'B�3B�RB�^B�^B��BÖB�B�B��B	VB	�B	%�B	9XB	@�B	[#B	p�B	�1B	��B	�!B	��B	��B	�NB	�B
B
oB
�B
.B
5?B
=qB
G�B
O�B
XB
^5B
cTB
iyB
o�B
v�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B�B�B�!BȴB�B��B	49B	gmB	�B	�9B	ɺB
�B
dZB
dZB
��B
��B\B@�B?}BB�Be`Bv�B��B�B��B�hB�B�B�B�+B�7B�7B�%B�%B�+B�%B�B� B{�Bn�BbNBo�Bl�BdZBW
BI�B?}B0!B�B\B
��B
�sB
��B
�qB
��B
�7B
s�B
VB
J�B
33B
$�B
  B	�NB	��B	B	��B	�7B	l�B	]/B	2-B	hB�B�#BǮB�dB�'B�3B�RB�^B�^B��BÖB�B�B��B	VB	�B	%�B	9XB	@�B	[#B	p�B	�1B	��B	�!B	��B	��B	�NB	�B
B
oB
�B
.B
5?B
=qB
G�B
O�B
XB
^5B
cTB
iyB
o�B
v�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080812011934  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080812011947  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080812011951  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080812011958  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080812011959  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080812012000  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080812013502                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080815161440  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080815161453  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080815161454  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080815161512  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080815161513  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080815161514  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080815191154                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080815161440  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519072752  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519072753  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519072753  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519072754  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519072754  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519072754  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519072754  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519072754  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519073005                      G�O�G�O�G�O�                