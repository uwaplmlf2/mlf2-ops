CDF   *   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   o   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K   CALIBRATION_DATE            	             
_FillValue                  ,  N   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N4   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N8   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N<   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N@   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  ND   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5901523 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               5A   JA  20080803014310  20091208062815                                  2B  A   APEX-SBE 2808                                                   846 @��YQ�1   @��^�;*@5��Q�@`�\(�1   ARGOS   B   B   B   @�33AffA^ffA�33A���A陚B
��BffB2  BFffB[33Bm33B�ffB�33B�  B���B�  B���B�ffB�ffBЙ�B�  B�ffB���B���C��CL�CL�CL�C� C33C��C$� C)ffC.ffC3�C8L�C=�CB33CGffCQL�C[L�CeL�CoffCy� C�� C���C��fC��fC��fC��3C���C��fC�� C��fC��fC��3C��fC�Cǳ3C̦fCљ�C֌�C�s3C���C噚C�s3C�s3C���C���D�3D�3D��D��D� D� DٚD$�fD)ٚD.�fD3�fD8�fD=ٚDB�3DG� DL� DQٚDV��D[��D`��De�3G�O�D�Y�D�� D�� D�0 D�c3D��3D��fD��D�` D��fD�� D�&fD�c3Dڣ3D�� D��D�Y�D� D��3111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111141111111111111111111 @�33AffAVffA�33A���A噚B��BffB0  BDffBY33Bk33B~��B�33B�  B���B�  B���B�ffB�ffBϙ�B�  B�ffB���B���C�C��C
��C��C  C�3CL�C$  C(�fC-�fC2��C7��C<��CA�3CF�fCP��CZ��Cd��Cn�fCy  C�@ C���C�ffC�ffC�ffC�s3C�Y�C�ffC�@ C�ffC�ffC�s3C�ffC�Y�C�s3C�ffC�Y�C�L�C�33C�Y�C�Y�C�33C�33C�Y�C�Y�D�3D�3D��D��D� D� D��D$�fD)��D.�fD3�fD8�fD=��DB�3DG� DL� DQ��DV��D[��D`��De�3G�O�D�I�D�� D�� D�  D�S3D��3D��fD�	�D�P D��fD�� D�fD�S3Dړ3D�� D�	�D�I�D� D��3111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111141111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��#A��A�`BA�9XA��A�ZA�
=A�dZA�M�A�VA�E�A�K�A��A�A�t�A�bNA�JA�jA�ȴA���A�(�A�/A�XA���A�A�A� �A�A�ffA�;dA�t�A�x�A��A��/A��wA��A���A�-A���A��A�n�A�VA��A�/A��A��A���A���A�|�A�7LA�hsA��A���A��`A��/A��A���A�n�Az �AtȴAp�Am�Ag�FAb=qA^bAXbASl�AO��AL~�AH��A@ȴA9A0ȴA'�A"�A��A
1'A �/@�Z@�33@��@�K�@Ƈ+@�ȴ@��7@�bN@��P@���@��@�ȴ@�bG�O�@z��@k�m@bM�@RM�@Hb@Bn�@<I�@5/@-�@+@&��@   @�@1'@��@-@ȴ@@�w@p�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111941111111111111111111 A��#A��A�`BA�9XA��A�ZA�
=A�dZA�M�A�VA�E�A�K�A��A�A�t�A�bNA�JA�jA�ȴA���A�(�A�/A�XA���A�A�A� �A�A�ffA�;dA�t�A�x�A��A��/A��wA��A���A�-A���A��A�n�A�VA��A�/A��A��A���A���A�|�A�7LA�hsA��A���A��`A��/A��A���A�n�Az �AtȴAp�Am�Ag�FAb=qA^bAXbASl�AO��AL~�AH��A@ȴA9A0ȴA'�A"�A��A
1'A �/@�Z@�33@��@�K�@Ƈ+@�ȴ@��7@�bN@��P@���@��@�ȴ@�bG�O�@z��@k�m@bM�@RM�@Hb@Bn�@<I�@5/@-�@+@&��@   @�@1'@��@-@ȴ@@�w@p�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111941111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
-B
2-B
7LB
5?B
T�B
P�B
N�B
R�B
[#B
dZB
��B
�FB�BC�BPB
�ZB
�fB
�B1B5?B1'B9XBdZBhsBdZBZB]/BiyBhsBn�B�DB�\B�B�B~�By�Bx�Bu�Bn�Bq�Bw�Bt�B^5BXBZBS�BI�B<jB5?B.B�BPB
��B
�B
��B
�XB
��B
�+B
l�B
XB
@�B
&�B

=B	�B	�B	��B	�!B	��B	�\B	n�B	I�B	-B	\B�mB�/BƨB�B��B�wBÖB�wBĜB��B�)B�B	  B	+B	PB	�B	5?B	J�G�O�B	�9B	��B	�/B	�B
B
oB
 �B
,B
7LB
=qB
H�B
O�B
W
B
^5B
dZB
iyB
n�B
r�B
v�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111141111111111111111111 B
-B
2-B
7LB
5?B
T�B
P�B
N�B
R�B
[#B
dZB
��B
�FB�BC�BPB
�ZB
�fB
�B1B5?B1'B9XBdZBhsBdZBZB]/BiyBhsBn�B�DB�\B�B�B~�By�Bx�Bu�Bn�Bq�Bw�Bt�B^5BXBZBS�BI�B<jB5?B.B�BPB
��B
�B
��B
�XB
��B
�+B
l�B
XB
@�B
&�B

=B	�B	�B	��B	�!B	��B	�\B	n�B	I�B	-B	\B�mB�/BƨB�B��B�wBÖB�wBĜB��B�)B�B	  B	+B	PB	�B	5?B	J�G�O�B	�9B	��B	�/B	�B
B
oB
 �B
,B
7LB
=qB
H�B
O�B
W
B
^5B
dZB
iyB
n�B
r�B
v�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111141111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080803014244  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080803014310  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080803014314  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080803014326  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080803014326  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080803014327  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080803014327  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20080803014329  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080803023651                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080805155814  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080805155818  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080805155819  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080805155823  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080805155823  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080805155823  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080805155823  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16b                                                                20080805155824  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080805191057                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080805155814  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519072749  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519072750  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519072750  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519072752  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090519072752  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519072752  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519072752  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519072752  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519072752  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519072752  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519073012                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208051905  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208052129  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208052129  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208052130  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208052131  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091208052131  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208052131  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208052131  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208052131  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208052131  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208052131  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208062815                      G�O�G�O�G�O�                