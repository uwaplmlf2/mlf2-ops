CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900989 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               uA   JA  20080826185150  20090423044658                                  2B  A   1575                                                            846 @��Q�1   @�띓'O@4�O�;dZ@bHj~��#1   ARGOS   A   A   A   @���A  Ad��A�ffAř�A홚B
��B33B333BF��BX��Bo33B�33B�33B���B�  B�ffB���B�ffB���B�  Bڙ�B䙚B�33B�  C��C� C33C  C�C  C33C#��C)�C-�3C3�C8L�C=L�CB  CGL�CQ33C[ffCe33Co�Cy33C��fC�� C���C�s3C��fC��3C�� C��fC�� C���C�� C��3C���C CǙ�C̳3Cь�Cր Cۀ C�3C���C�fC�� C��3C���D��DٚD�fD��DٚD��DٚD$�fD)�3D.ٚD3�3D8�fD=�fDB�3DG� DL� DQ��DVٚD[�3D`�3De�3Dj�3DoٚDt� Dy�fD�)�D�p D�� D��D�)�D�l�D��fD�ٚD��D�i�D���D�� D�&fD�ffDڰ D���D��D�Y�D��D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A33A`  A�  A�34A�34B	��B  B2  BE��BW��Bn  B���B���B�33B�ffB���B�  B���B�33B�ffB�  B�  BB�ffCL�C33C
�fC�3C��C�3C�fC#� C(��C-ffC2��C8  C=  CA�3CG  CP�fC[�Cd�fCn��Cx�fC�� C���C�s4C�L�C�� C���C���C�� C���C�fgC�Y�C���C�s4C�Y�C�s4Č�C�fgC�Y�C�Y�C���C�gC� CC��C�fgD��D�gD�3D�gD�gD��D�gD$�3D)� D.�gD3� D8�3D=�3DB� DG��DL��DQ��DV�gD[� D`� De� Dj� Do�gDt��Dy�3D�  D�ffD��fD�� D�  D�c3D���D�� D� D�` D�� D��fD��D�\�DڦfD��3D� D�P D�3D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�9XA�9XA�7LA�=qA��mA�VA��
A�9XA��/A��TA´9A��A�1A���A�7LA���A���A�ffA���A�I�A�Q�A��;A��/A��A��A���A�S�A�I�A���A�I�A���A�33A��;A�^5A�|�A���A�A� �A�ffA���A��PA���A�=qA���A���A�1A��jA���A~��Ayt�Aw
=Ao��Ah�HAd��A`A�A^�jAX�/AP��ANn�AI�AB5?A>=qA9�A4E�A-�A)dZA$(�A"-A��A-A��A�^@��#@۝�@Ѓ@���@��`@��@��@�9X@�/@��^@��-@�C�@�{@�7L@�~�@|��@v��@sdZ@n�+@j�\@f�y@d�@`�u@XA�@Q�#@I7L@>�y@8bN@2��@-p�@'�w@#�m@�h@��@�@��@V@
J@`B@�^?�j?�$�?�Z1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�9XA�9XA�7LA�=qA��mA�VA��
A�9XA��/A��TA´9A��A�1A���A�7LA���A���A�ffA���A�I�A�Q�A��;A��/A��A��A���A�S�A�I�A���A�I�A���A�33A��;A�^5A�|�A���A�A� �A�ffA���A��PA���A�=qA���A���A�1A��jA���A~��Ayt�Aw
=Ao��Ah�HAd��A`A�A^�jAX�/AP��ANn�AI�AB5?A>=qA9�A4E�A-�A)dZA$(�A"-A��A-A��A�^@��#@۝�@Ѓ@���@��`@��@��@�9X@�/@��^@��-@�C�@�{@�7L@�~�@|��@v��@sdZ@n�+@j�\@f�y@d�@`�u@XA�@Q�#@I7L@>�y@8bN@2��@-p�@'�w@#�m@�h@��@�@��@V@
J@`B@�^?�j?�$�?�Z1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
oB
oB
oB
{B
49B
33B
l�B
�B
��B:^B�B�FB1B��B%B��B�fB��B��B��B��B��B��B�VB~�Bv�Bn�BdZB^5B[#BYBVBT�BQ�BL�BG�BD�B@�B=qB:^B49B%�B�B�B	7B
��B
�B
��B
�'B
��B
�7B
w�B
>wB
+B
�B
JB	�B	��B	�jB	��B	�B	t�B	`BB	H�B	/B	�B	
=B	  B�B�
B�B�BcTBXBYB^5Bl�B�\B�3B�LB�/B�B��B	B	uB	&�B	7LB	L�B	aHB	x�B	�uB	��B	��B	�B	�}B	�#B	�B
B
�B
&�B
2-B
8RB
A�B
R�B
XB
bNB
iyB
n�B
t�B
w�B
{�B
}�B
�B
�PB
�h1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
oB
oB
oB
{B
49B
33B
l�B
�B
��B:^B�B�FB1B��B%B��B�fB��B��B��B��B��B��B�VB~�Bv�Bn�BdZB^5B[#BYBVBT�BQ�BL�BG�BD�B@�B=qB:^B49B%�B�B�B	7B
��B
�B
��B
�'B
��B
�7B
w�B
>wB
+B
�B
JB	�B	��B	�jB	��B	�B	t�B	`BB	H�B	/B	�B	
=B	  B�B�
B�B�BcTBXBYB^5Bl�B�\B�3B�LB�/B�B��B	B	uB	&�B	7LB	L�B	aHB	x�B	�uB	��B	��B	�B	�}B	�#B	�B
B
�B
&�B
2-B
8RB
A�B
R�B
XB
bNB
iyB
n�B
t�B
w�B
{�B
}�B
�B
�PB
�h1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20080826185148  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080826185150  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080826185151  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080826185151  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080826185155  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080826185155  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080826185155  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080826185155  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080826185156  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080826191242                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080830155240  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080830155257  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080830155259  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080830155301  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080830155310  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080830155310  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080830155311  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080830155311  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080830155312  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20080830155312  CV  TIME            G�O�G�O�F�\�                JA  ARGQrelo2.1                                                                 20080830155312  CV  LAT$            G�O�G�O�A�l�                JA  ARGQrelo2.1                                                                 20080830155312  CV  LON$            G�O�G�O�CCT                JA  ARUP                                                                        20080830202652                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080830155240  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423044144  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423044144  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090423044145  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423044145  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423044146  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423044146  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423044146  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423044146  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423044146  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423044658                      G�O�G�O�G�O�                