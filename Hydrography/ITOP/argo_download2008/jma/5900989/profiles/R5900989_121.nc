CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900989 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               yA   JA  20081005185213  20090423044723                                  2B  A   1575                                                            846 @��� <�v1   @���ax9�@5E�Q�@bF-V1   ARGOS   A   A   A   @�ffA  AfffA���A�  A陚B
ffB��B333BE��BZ  Bm33B�  B�ffB���B�33B�  B���B�33B�  B���Bٙ�B�ffB���B�ffC33CL�C��C�3C� CffC�C$33C)L�C.33C2��C8L�C<�3CB�CF�3CQ33C[ffCe��Co�3Cy33C��3C���C���C�s3C���C�� C���C���C��3C���C��fC��3C���C���Cǌ�C�s3C�ffC֌�C�Y�C�s3C�ffCꙚC�fC�ffC��fD��D��D��D��DٚD��D� D$� D)��D.ٚD3� D8ٚD=�3DB�3DG�3DLٚDQ��DVٚD[��D`�3De�3DjٚDo�3DtٚDy�3D�0 D�p D�� D�ٚD�&fD�ffD���D��fD�)�D�i�D�� D��D��D�i�Dڣ3D��fD�)�D�c3D� D� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��A`  A�fgA���A�fgB��B  B1��BD  BXffBk��B�33B���B���B�ffB�33B���B�ffB�33B�  B���B♙B�  B���C ��C�gC34CL�C�C  C�4C#��C(�gC-��C2fgC7�gC<L�CA�4CFL�CP��C[  Ce34CoL�Cx��C�� C�fgC�fgC�@ C�Y�C�L�C�Y�C�fgC�� C���C�s3C�� C�Y�C�C�Y�C�@ C�33C�Y�C�&gC�@ C�33C�fgC�s3C�33C�s3D�3D�3D�3D�3D� D�3D�fD$�fD)� D.� D3�fD8� D=��DB��DG��DL� DQ� DV� D[�3D`��De��Dj� Do��Dt� Dy��D�#3D�c3D��3D���D��D�Y�D���D�ٙD��D�\�D��3D���D� D�\�DږfD�ٙD��D�VfD�3D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�?}A훦A�`BA�5?A��A��A�FA�l�A�A���A���A�jA�VA�ƨA�p�A��A���A���A�=qA���A��A��-A��uA�  A�VA���A�M�A��wA��PA�
=A��!A�7LA�$�A��A��A�C�A�bNA��\A�5?A��jA�ZA�^5A�XA�A���A�A��^A��A��A�9XA��!A��HA}VAs|�Ah��A`��A[ƨAW��ARA�AJ1AFbAD^5A>bNA:z�A6jA3�FA.1A(M�A#��Ap�A�RA	�w@�@���@���@�(�@�G�@ʧ�@��@��^@��#@���@��@���@���@��H@���@�dZ@�p�@�|�@�-@� �@|z�@u`B@pb@b^5@Z�!@So@Lj@CdZ@<��@7��@1��@+S�@%�@ bN@=q@V@
=@@K�@��@�F@ ��?�{1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�?}A훦A�`BA�5?A��A��A�FA�l�A�A���A���A�jA�VA�ƨA�p�A��A���A���A�=qA���A��A��-A��uA�  A�VA���A�M�A��wA��PA�
=A��!A�7LA�$�A��A��A�C�A�bNA��\A�5?A��jA�ZA�^5A�XA�A���A�A��^A��A��A�9XA��!A��HA}VAs|�Ah��A`��A[ƨAW��ARA�AJ1AFbAD^5A>bNA:z�A6jA3�FA.1A(M�A#��Ap�A�RA	�w@�@���@���@�(�@�G�@ʧ�@��@��^@��#@���@��@���@���@��H@���@�dZ@�p�@�|�@�-@� �@|z�@u`B@pb@b^5@Z�!@So@Lj@CdZ@<��@7��@1��@+S�@%�@ bN@=q@V@
=@@K�@��@�F@ ��?�{1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	ffB	hsB	hsB	gmB	dZB	bNB	� B
�B
u�B
��B
��B  B#�B-B@�Bu�B�B�#B�;B�#B�)B�/B�ZB�B�B�5B��BƨB�3B��B��B��B�{B�1B� B}�Bz�Bv�Br�Bm�BffB\)BQ�BF�B;dB0!B,B%�B�B
��B
�B
�/B
��B
{�B
D�B
�B
+B	�B	��B	�B	��B	�PB	o�B	_;B	N�B	@�B	(�B	�B	+B�yB��B�LB��B��B��B�hB��B��B�B�9BǮB�B�NB��B	%B	�B	$�B	/B	2-B	?}B	=qB	dZB	r�B	}�B	�{B	�XB	�
B	�B	��B
JB
 �B
)�B
5?B
A�B
H�B
M�B
T�B
\)B
cTB
l�B
r�B
x�B
|�B
�B
�%1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	ffB	hsB	hsB	gmB	dZB	bNB	� B
�B
u�B
��B
��B  B#�B-B@�Bu�B�B�#B�;B�#B�)B�/B�ZB�B�B�5B��BƨB�3B��B��B��B�{B�1B� B}�Bz�Bv�Br�Bm�BffB\)BQ�BF�B;dB0!B,B%�B�B
��B
�B
�/B
��B
{�B
D�B
�B
+B	�B	��B	�B	��B	�PB	o�B	_;B	N�B	@�B	(�B	�B	+B�yB��B�LB��B��B��B�hB��B��B�B�9BǮB�B�NB��B	%B	�B	$�B	/B	2-B	?}B	=qB	dZB	r�B	}�B	�{B	�XB	�
B	�B	��B
JB
 �B
)�B
5?B
A�B
H�B
M�B
T�B
\)B
cTB
l�B
r�B
x�B
|�B
�B
�%1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20081005185210  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081005185213  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20081005185213  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081005185214  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081005185218  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081005185218  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081005185218  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081005185218  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081005185218  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081005191523                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20081010034945  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081010034950  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20081010034950  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081010034951  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081010034955  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20081010034955  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081010034955  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20081010034955  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081010034955  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20081010060737                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20081010034945  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423044153  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423044154  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090423044154  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423044154  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423044155  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423044155  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423044155  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423044155  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423044155  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423044723                      G�O�G�O�G�O�                