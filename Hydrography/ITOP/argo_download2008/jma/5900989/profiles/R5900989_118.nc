CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900989 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               vA   JA  20080905185228  20090423044724                                  2B  A   1575                                                            846 @���41   @��.Es@4�1&�y@bD�/��1   ARGOS   A   A   A   @�  A33Ac33A�33A�33A���B��B33B2��BF  BXffBn��B�  B���B�ffB�  B�  B���B�  B�33B�ffB�ffB�33B�ffB���C�CffC� CL�CL�C� C�C$�C)� C.  C3� C8� C=�CB  CG�CQ�C[�CeL�Co33Cy� C��fC���C��3C��fC���C��fC��fC���C��3C���C��fC��3C�� C Cǀ C̙�CѦfC�ffC�s3C�s3C噚CꙚC�3C��fC�� DٚD� D��D�3D�fD� D�3D$ٚD)��D.��D3��D8� D=�fDB�fDG�fDL� DQٚDV��D[� D`ٚDe��Dj�3DoٚDtٚDyٚD��D�p D�� D��D�&fD�` D��fD��fD�)�D�i�D���D���D�0 D�c3Dک�D���D�&fD�i�D� D� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A  A`  A���A���A�33B  BffB2  BE33BW��Bn  B���B�34B�  B���B���B�34B���B���B�  B�  B���B�  B�34C �gC33CL�C�C�CL�C�gC#�gC)L�C-��C3L�C8L�C<�gCA��CF�gCP�gCZ�gCe�Co  CyL�C���C�s3C���C���C�� C���C���C�s3C���C��3C���C���C�ffC�ffC�ffC̀ Cь�C�L�C�Y�C�Y�C� C� CC��C��fD��D�3D� D�fD��D�3D�fD$��D)��D.� D3� D8�3D=ٙDBٙDG��DL�3DQ��DV��D[�3D`��De� Dj�fDo��Dt��Dy��D�gD�i�D���D��4D�  D�Y�D�� D�� D�#4D�c4D��gD��gD�)�D�\�Dڣ4D��gD�  D�c4D�D�	�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�DA��AA�^5A�?}A�
=A�A�K�A�%A�A��ÃA�r�A�K�A��A���A��wA��wA��A� �A���A���A�+A� �A�C�A�?}A�A���A�E�A�JA��;A��yA��DA�t�A���A��A�A�A�-A�5?A��+A��+A��TA��A�$�A�1'A���A��A�t�A���A�VA|��Ay|�ArffAi��Ad��Aa33AWG�AO��AMG�AF�+AAx�A=�^A7��A5+A/;dA(VA$�uA"JA��A�wA{A9X@��@�-@�~�@�1'@�G�@��m@�?}@�-@�@��@�/@��H@�E�@��h@�J@{��@up�@qG�@m�@i�@g;d@fff@`1'@V$�@JJ@D1@?l�@8Ĝ@3dZ@-�-@)��@%@��@M�@�R@�H@+@
��@b@�?�|�?��H?�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�DA��AA�^5A�?}A�
=A�A�K�A�%A�A��ÃA�r�A�K�A��A���A��wA��wA��A� �A���A���A�+A� �A�C�A�?}A�A���A�E�A�JA��;A��yA��DA�t�A���A��A�A�A�-A�5?A��+A��+A��TA��A�$�A�1'A���A��A�t�A���A�VA|��Ay|�ArffAi��Ad��Aa33AWG�AO��AMG�AF�+AAx�A=�^A7��A5+A/;dA(VA$�uA"JA��A�wA{A9X@��@�-@�~�@�1'@�G�@��m@�?}@�-@�@��@�/@��H@�E�@��h@�J@{��@up�@qG�@m�@i�@g;d@fff@`1'@V$�@JJ@D1@?l�@8Ĝ@3dZ@-�-@)��@%@��@M�@�R@�H@+@
��@b@�?�|�?��H?�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
��B
��B
��B
��B
��B
�sBoBF�B�hBƨBɺB��B��B��B�B�B�B�B�mB�BɺBƨB�dB�LB��B�uB�7B�B�B�Bz�Bs�Bp�BjBcTB]/BXBT�BH�BB�B<jB:^B2-B(�B�B\B
��B
�NB
�#B
��B
�B
��B
w�B
M�B
33B
�B	�sB	ƨB	�FB	��B	�B	r�B	[#B	O�B	6FB	�B	bB	B�B�NB�3B��B�bB�=Bz�BYB^5Bq�B�bB��B�LB��B�;B�B	�B	/B	G�B	dZB	o�B	�B	�DB	��B	�B	�3B	��B	�HB	��B

=B
{B
(�B
:^B
F�B
Q�B
XB
_;B
`BB
e`B
jB
m�B
t�B
v�B
}�B
�7B
�DB
�h1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
��B
��B
��B
��B
��B
�sBoBF�B�hBƨBɺB��B��B��B�B�B�B�B�mB�BɺBƨB�dB�LB��B�uB�7B�B�B�Bz�Bs�Bp�BjBcTB]/BXBT�BH�BB�B<jB:^B2-B(�B�B\B
��B
�NB
�#B
��B
�B
��B
w�B
M�B
33B
�B	�sB	ƨB	�FB	��B	�B	r�B	[#B	O�B	6FB	�B	bB	B�B�NB�3B��B�bB�=Bz�BYB^5Bq�B�bB��B�LB��B�;B�B	�B	/B	G�B	dZB	o�B	�B	�DB	��B	�B	�3B	��B	�HB	��B

=B
{B
(�B
:^B
F�B
Q�B
XB
_;B
`BB
e`B
jB
m�B
t�B
v�B
}�B
�7B
�DB
�h1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20080905185226  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080905185228  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080905185229  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080905185229  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080905185233  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080905185233  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080905185233  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080905185233  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080905185234  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080905191328                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080909155627  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080909155644  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080909155647  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080909155648  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080909155657  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080909155657  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080909155658  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080909155658  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080909155659  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080909190559                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20080909155627  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423044146  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423044147  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090423044147  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423044147  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423044148  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423044148  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423044148  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423044148  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423044148  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423044724                      G�O�G�O�G�O�                