CDF   -   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   ?   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  4�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  5   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  6   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  7   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  7L   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  8H   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  9�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  :�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  :�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  ;�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  <�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =(   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @(   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C(   CALIBRATION_DATE            	             
_FillValue                  ,  F(   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    FT   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    FX   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    F\   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    F`   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Fd   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    F�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    F�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    F�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         F�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         F�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        F�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    F�Argo profile    2.2 1.2 19500101000000  5901578 PALAU2008                                                       JAMSTEC Sato Naoki                                              PRES            TEMP            PSAL               ?A   JA  20080803093146  20091207062824                                  2B  A   APEX-SBE 3604                                                   846 @��w�B��1   @��yd�	�@6&fffff@`L1&�x�1   ARGOS   B   B   B   @���A��Ac33A�  A�  A�33B
��B��B133BF��BY��Bm33B���B�  B�ffB�  B���B���G�O�B���B�33C� C33CL�CffC  C�fC�C$ffCQ33CBL�CG� CQ�C[L�Ce33Co�CyffC�� C�� C�� C���C���C��fC��fC�ffC�ffC��3C���C���C�� C�� Cǀ C̳3C�s3C�s3Cۀ C�ffC�s3C��C� C�ffC���C�Y�111111111111111111411111111114111111111111111111111111111111111 @���A��Ac33A�  A�  A�33B
��B��B133BF��BY��Bm33B���B�  B�ffB�  B���B���G�O�B���B�33C� C33CL�CffC  C�fC�C$ffCQ33CBL�CG� CQ�C[L�Ce33Co�CyffC�� C�� C�� C���C���C��fC��fC�ffC�ffC��3C���C���C�� C�� Cǀ C̳3C�s3C�s3Cۀ C�ffC�s3C��C� C�ffC���C�Y�111111111111111111411111111114111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ZA藍A�VA��A�%A���A�~�A��A�=qA��A���A���AҋDA��yA��
A��A��`G�O�A���A�A�K�A�ZA��A��FA��A��mA�A�A�z�G�O�A���A��A��9A��A�p�A�ffA��7A��A�r�A��^A���A���A�1'A�M�A���A��/A�`BA��A���A|bNAs�-An  AiO�Ac��A_hsA[�AV�AP��AK�AES�A>�A8��A4 �A/��111111111111111119411111111193111111111111111111111111111111111 A�ZA藍A�VA��A�%A���A�~�A��A�=qA��A���A���AҋDA��yA��
A��A��`G�O�A���A�A�K�A�ZA��A��FA��A��mA�A�A�z�G�O�A���A��A��9A��A�p�A�ffA��7A��A�r�A��^A���A���A�1'A�M�A���A��/A�`BA��A���A|bNAs�-An  AiO�Ac��A_hsA[�AV�AP��AK�AES�A>�A8��A4 �A/��111111111111111119411111111193111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	/B	��B	��B	��B	��B	�wB	��B
'�B
]/B
iyB
k�B
n�B
u�B
�PB
�bB
�3B
�jB
G�O�BK�BF�BP�B_;BjB�7B�7B�+B�VG�O�B�1B�B~�Bm�BbNB]/BT�BQ�BK�BF�B49B)�B�BuBB
�B
�)B
��B
�B
�hB
dZB
H�B
0!B
{B
  B	�B	��B	�FB	��B	�B	cTB	O�B	6FB	#�111111111111111111411111111193111111111111111111111111111111111 B	/B	��B	��B	��B	��B	�wB	��B
'�B
]/B
iyB
k�B
n�B
u�B
�PB
�bB
�3B
�jB
G�O�BK�BF�BP�B_;BjB�7B�7B�+B�VG�O�B�1B�B~�Bm�BbNB]/BT�BQ�BK�BF�B49B)�B�BuBB
�B
�)B
��B
�B
�hB
dZB
H�B
0!B
{B
  B	�B	��B	�FB	��B	�B	cTB	O�B	6FB	#�111111111111111111411111111193111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080803093038  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080803093146  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080803093149  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080803093157  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080803093157  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080803093157  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080803093157  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20080803093158  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080803100108                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080806043835  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080806043841  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080806043842  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080806043846  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080806043846  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080806043846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8a                                                                20080806043846  QCF$                G�O�G�O�G�O�            4100JA  ARUP                                                                        20080806051217                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080806043835  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416023952  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416023953  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416023953  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416023954  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090416023954  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416023954  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416023954  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416023954  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090416023954  QCF$                G�O�G�O�G�O�             100JA  ARGQaqcp2.8b                                                                20090416023954  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416023954  QCF$                G�O�G�O�G�O�             100JA  ARGQrqcpt16b                                                                20090416023954  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090416025624                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207060333  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207061308  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207061309  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207061309  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207061310  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207061310  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207061310  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207061310  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207061310  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8c                                                                20091207061310  QCF$                G�O�G�O�G�O�             100JA  ARGQaqcp2.8c                                                                20091207061310  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207061310  QCF$                G�O�G�O�G�O�             100JA  ARGQrqcpt16b                                                                20091207061310  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091207062824                      G�O�G�O�G�O�                