CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   =   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  4�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  5   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  5�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  6�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  7,   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  8    TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  8`   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  9T   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  :H   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  :�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  ;|   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  <�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    <�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    ?�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    B�   CALIBRATION_DATE            	             
_FillValue                  ,  E�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    F   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    F   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    F   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    F   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  F   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    F\   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Fl   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Fp   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         F�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         F�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        F�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    F�Argo profile    2.2 1.2 19500101000000  5901578 PALAU2008                                                       JAMSTEC Sato Naoki                                              PRES            TEMP            PSAL               WA   JA  20080830043912  20091207062845                                  2B  A   APEX-SBE 3604                                                   846 @��x���1   @��z�   @7wKƧ�@`E?|�h1   ARGOS   A   A   A   @�33A  Aa��A���A�  A�ffB
ffB33B333BF��BZ  Bo33B�33B���B�ffB�  B�ffB�33B�ffB�ffBЙ�B�ffB䙚BB�ffC �fC��C��CL�CffC��C��C$� C)��C.L�C3L�C8ffC���C�� C���C�� C��fC���C��3C��fC��3C��fC��fC�� Cǌ�C̦fCљ�Cֳ3C�ffC�3C噚C�L�C��C� C�ffC�s31111111111111111111111111111111111111111111111111111111111111   @�  AffA`  A���A�33A陙B
  B��B2��BFfgBY��Bn��B�  B���B�33B���B�33B�  B�33B�33B�fgB�33B�fgB�fgB�33C ��C� C� C33CL�C� C�3C$ffC)� C.33C333C8L�C�� C�s3C�� C��3C���C�� C��fC���C��fC���C���C³3Cǀ C̙�Cь�C֦fC�Y�C�fC��C�@ C� C�s3C�Y�C�ff1111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�-A�jA�1'A��FA�A왚A�t�A�7LA���A�A�33A��HA�ffA�A�A�Q�A���A�l�A�(�A�&�A�oA��A�JA��HA�t�A�A�A��!A�&�A���A��^A��-A��
A���A���A��hA�{A�z�A��-A���A��jA��\A��\A�v�A��\A�5?A���A�bNA��A���A|��At��Ap  Ai��Ad��A`E�A[%AW�#AS\)AN��AI�;AF�yAD��1111111111111111111111111111111111111111111111111111111111111   A�-A�jA�1'A��FA�A왚A�t�A�7LA���A�A�33A��HA�ffA�A�A�Q�A���A�l�A�(�A�&�A�oA��A�JA��HA�t�A�A�A��!A�&�A���A��^A��-A��
A���A���A��hA�{A�z�A��-A���A��jA��\A��\A�v�A��\A�5?A���A�bNA��A���A|��At��Ap  Ai��Ad��A`E�A[%AW�#AS\)AN��AI�;AF�yAD��1111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B	��B	�;B	�BB	�)B	�B	��B	�)B	�B
\B
C�B
hsB
jB
�B
��B
��B
��B
�!B
��B
�sB
�mB%B-B33B8RBT�Be`Bm�B{�B~�B�B~�B�B�1B�%B�7BB�B@�B;dB1'B"�BDB
��B
��B
�BB
ȴB
�'B
�\B
gmB
Q�B
49B
�B
B	�sB	�B	��B	�B	��B	�%B	|�1111111111111111111111111111111111111111111111111111111111111   B	��B	��B	��B	�;B	�BB	�)B	�B	��B	�)B	�B
\B
C�B
hsB
jB
�B
��B
��B
��B
�!B
��B
�sB
�mB%B-B33B8RBT�Be`Bm�B{�B~�B�B~�B�B�1B�%B�7BB�B@�B;dB1'B"�BDB
��B
��B
�BB
ȴB
�'B
�\B
gmB
Q�B
49B
�B
B	�sB	�B	��B	�B	��B	�%B	|�1111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080830043906  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080830043912  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080830043913  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080830043917  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20080830043917  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080830043917  QCP$                G�O�G�O�G�O�            FB40JA  ARUP                                                                        20080830051203                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080830043906  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416024046  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416024046  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416024047  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416024048  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090416024048  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416024048  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416024048  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416024048  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416024048  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416024048  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090416025641                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207060339  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207061346  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207061346  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207061347  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207061348  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207061348  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207061348  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207061348  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207061349  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091207062845                      G�O�G�O�G�O�                