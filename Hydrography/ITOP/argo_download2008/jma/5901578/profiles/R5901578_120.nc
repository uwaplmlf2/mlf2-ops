CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   A   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  4�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5,   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       60   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  74   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7x   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  8|   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  :�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  <   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       <T   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  =X   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C�   CALIBRATION_DATE            	             
_FillValue                  ,  F�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    F�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    F�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    F�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    F�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  F�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         G(   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         G,   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G0   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G4Argo profile    2.2 1.2 19500101000000  5901578 PALAU2008                                                       JAMSTEC Sato Naoki                                              PRES            TEMP            PSAL               xA   JA  20080929125735  20091207062802                                  2B  A   APEX-SBE 3604                                                   846 @��)Vٲ1   @��P��H@4��;dZ@`9XbN1   ARGOS   A   A   A   @�ffA��Ad��A���A���A�ffB��B  B1��BD��B[33BnffB���B�33B���B�33B���B�ffB���B�33B�  B�  B�  B�33B�ffC� C��C� C� C��C��C33C$L�C(�fC.�C3�C8L�C=� CB33CGL�CQ�C[  Ce  Co33Cy��C��fC���C�� C���C���C���C��fC���C��fC��3C�� C���C�� C�� CǦfC̀ C�� C֌�Cی�C���11111111111111111111111111111111111111111111111111111111111111111   @�33A33Ac33A���A�  A陙BfgB��B134BDfgBZ��Bn  B�fgB�  B���B�  B���B�33B�fgB�  B���B���B���B�  B�33CffC� CffCffC� C� C�C$33C(��C.  C3  C833C=ffCB�CG33CQ  CZ�fCd�fCo�Cy� C���C�� C��3C���C���C���C���C���C���C��fC��3C���C��3C³3CǙ�C�s3Cѳ3Cր Cۀ C���11111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�A�+A�7A�DA�DA�PA�hA�PA�t�A囦A�ȴA�5?Aް!A���A�x�AԍPA�jA�~�A�5?A�bNA���A���AāAþwA�dZA�S�A��`A�l�A�ZA��A��wA���A���A���A�r�A�E�A�VA�t�A�v�A�G�A�
=A�`BA�Q�A�ffA�ȴA�n�A���A���A���A��A��-A�bNA�VA�bNA���A��/A}��AwO�Aq�mAm��Ah^5A`ffA[�TAU/11111111111111111111111111111111111111111111111111111111111111111   A�A�A�+A�7A�DA�DA�PA�hA�PA�t�A囦A�ȴA�5?Aް!A���A�x�AԍPA�jA�~�A�5?A�bNA���A���AāAþwA�dZA�S�A��`A�l�A�ZA��A��wA���A���A���A�r�A�E�A�VA�t�A�v�A�G�A�
=A�`BA�Q�A�ffA�ȴA�n�A���A���A���A��A��-A�bNA�VA�bNA���A��/A}��AwO�Aq�mAm��Ah^5A`ffA[�TAU/11111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�HB�BB�BB�BB�BB�BB�BB�BB�NB�mB	B	?}B	�VB	�fB
_;B
o�B
�B
��B
��B
�B
��B
�B{B$�B,B9XBL�BW
B_;Bu�B~�B�%B�%B�=B�=B�JB�DB�DB�hB�VB}�Bq�BjBaHB\)BS�BQ�BK�B@�B1'B$�B�BPBB
�B
�B
�dB
��B
{�B
aHB
J�B
,B
%B	�B	��11111111111111111111111111111111111111111111111111111111111111111   B�HB�BB�BB�BB�BB�BB�BB�BB�NB�mB	B	?}B	�VB	�fB
_;B
o�B
�B
��B
��B
�B
��B
�B{B$�B,B9XBL�BW
B_;Bu�B~�B�%B�%B�=B�=B�JB�DB�DB�hB�VB}�Bq�BjBaHB\)BS�BQ�BK�B@�B1'B$�B�BPBB
�B
�B
�dB
��B
{�B
aHB
J�B
,B
%B	�B	��11111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20080929125727  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080929125735  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080929125736  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080929125740  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080929125740  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080929125740  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20080929130455                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081002055120  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081002055132  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081002055133  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081002055137  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081002055138  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081002055138  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20081002062847                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081002055120  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416024207  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416024207  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416024207  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416024208  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416024208  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416024208  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416024208  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416024209  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090416025603                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207060347  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207061425  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207061426  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207061427  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207061428  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207061428  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207061428  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207061428  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207061428  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091207062802                      G�O�G�O�G�O�                