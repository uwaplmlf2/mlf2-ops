CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   C   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  4�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5<   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6H   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  7T   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  ;    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;D   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  <P   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       <�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  =�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C�   CALIBRATION_DATE            	             
_FillValue                  ,  F�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    F�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G    HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    GL   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G\   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G`   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Gp   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Gt   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Gx   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G|Argo profile    2.2 1.2 19500101000000  5901578 PALAU2008                                                       JAMSTEC Sato Naoki                                              PRES            TEMP            PSAL               A   JA  20081009042708  20091207062814                                  2B  A   APEX-SBE 3604                                                   846 @��x�www1   @��yi@4��-@_�\(�1   ARGOS   A   A   A   @�ffA��Ac33A���A�ffA�ffB��B  B133BFffBZffBl��B�33B�  B�  B���B�  B�33B�  B���Bљ�B�ffB�33B�33B���CL�C�fC��CffC� CL�CffC$ffC)33C.L�C3�C8ffC=� CB� CG��CQL�C[33Ce33C��fC���C�s3C���C���C�s3C��3C���C��3C��fC�CǙ�C̳3Cь�Cր C�s3C�ffC�s3C�L�C��C��fC��fD� D331111111111111111111111111111111111111111111111111111111111111111111 @�  A��A`  A�33A���A���B  B33B0ffBE��BY��Bl  B��B���B���B�fgB���B���B���B�fgB�34B�  B���B���B�fgC�C�3CfgC33CL�C�C33C$33C)  C.�C2�gC833C=L�CBL�CGfgCQ�C[  Ce  C���C�s3C�Y�C�s3C�� C�Y�C���C��3C���C���C�s3Cǀ C̙�C�s3C�ffC�Y�C�L�C�Y�C�33C�s3C��C���D�3D&f1111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�bNA�dZA�l�A�  A�A�\A�bNA�(�A�  A���A��A�JA埾A��A�dZA�t�AݼjAܙ�A��A�n�A���Aϗ�A��A�bA�/A�bA��DA�VA���A�|�A�t�A���A�+A�~�A��\A��FA��+A�n�A�A�ĜA��jA���A���A�?}A�Q�A�ZA�A��A��TA��`A�x�A�v�A�hA|5?Az�`Atr�An�AfjAc�A^bNAYdZAV��AS�wAM�AIXA=��A=X1111111111111111111111111111111111111111111111111111111111111111111 A�bNA�dZA�l�A�  A�A�\A�bNA�(�A�  A���A��A�JA埾A��A�dZA�t�AݼjAܙ�A��A�n�A���Aϗ�A��A�bA�/A�bA��DA�VA���A�|�A�t�A���A�+A�~�A��\A��FA��+A�n�A�A�ĜA��jA���A���A�?}A�Q�A�ZA�A��A��TA��`A�x�A�v�A�hA|5?Az�`Atr�An�AfjAc�A^bNAYdZAV��AS�wAM�AIXA=��A=X1111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B��B��B�hB��B��B��B��B�BŢB�ZB�fB�B��B	�B	O�B	^5B	s�B	��B	��B
C�B
^5B
�!B
��B{B33B>wB@�BM�B`BBn�Bm�Bz�B�B�B�JB�VB�JB�VB��B�1B�B~�BN�BI�B=qB33B �BPB
�B
�
B
B
��B
��B
�VB
l�B
K�B
$�B
oB	��B	�TB	�B	ŢB	��B	��B	dZB	bN1111111111111111111111111111111111111111111111111111111111111111111 B��B��B��B�hB��B��B��B��B�BŢB�ZB�fB�B��B	�B	O�B	^5B	s�B	��B	��B
C�B
^5B
�!B
��B{B33B>wB@�BM�B`BBn�Bm�Bz�B�B�B�JB�VB�JB�VB��B�1B�B~�BN�BI�B=qB33B �BPB
�B
�
B
B
��B
��B
�VB
l�B
K�B
$�B
oB	��B	�TB	�B	ŢB	��B	��B	dZB	bN1111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20081009042701  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20081009042708  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20081009042709  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20081009042713  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20081009042713  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20081009042713  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20081009042714  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20081009062904                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20081009042701  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416024222  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416024223  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416024223  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416024224  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090416024224  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416024224  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416024224  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416024224  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416024224  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416024224  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090416025613                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207060349  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207061434  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207061434  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207061435  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207061436  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207061436  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207061436  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207061436  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207061436  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091207062814                      G�O�G�O�G�O�                