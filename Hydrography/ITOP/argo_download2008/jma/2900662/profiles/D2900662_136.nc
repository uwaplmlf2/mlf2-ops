CDF   .   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900662 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               �A   JA  20080827051003  20090907083625  A9_67032_136                    2C  D   APEX-SBE 2980                                                   846 @�����~1   @��nQ@7,�C��@`�S���1   ARGOS   A   A   A   @�ffA33A�33A���B��BFffBnffB���B���B�ffB�ffB���BC� C��CffC� C)33C333C=L�CGffC[  CoffC�� C�� C��fC���C��fC��3C��fCǀ Cљ�C۳3C�fCC��3D� D�3DٚDٚD�fD�3D��D$� D)�3D.�fD3�3D8� D=ٚDB� DGٚDN�DT` DZ� D`�3Dg�DmY�Ds�fDy� D�#3D�ffD�� D�� D�#3D�ffD��D�p D��D�ffD���D�S3D�� 111111111111111111111111111111111111111111111111111111111111111111111111@�33A��A�ffA�  BffBF  Bn  B���B���B�33B�33Bڙ�B�ffCffC� CL�CffC)�C3�C=33CGL�CZ�fCoL�C�s3C��3C���C�� C���C��fC���C�s3Cь�CۦfC噚C��C��fD��D��D�3D�3D� D��D�3D$��D)��D.� D3��D8ٚD=�3DBٚDG�3DN3DTY�DZ��D`��DgfDmS3Ds� Dy��D�  D�c3D���D���D�  D�c3D��fD�l�D��fD�c3D�ٚD�P D���111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�oA��A�A��;A�~�A�S�A��A�=qA�  A�\)A�  A�9XA�E�A�n�A��A���A�$�A�jA� �A�jA�ĜA��-A�
=A��/A�+A�x�Ayp�Ao�AhI�A`�AY�AQ%AG�TA<��A0ȴA$ĜA��A�AS�@�j@�R@�~�@�@�V@��`@�E�@�?}@��T@���@��@�"�@���@���@��F@��@���@K�@r��@k�F@b�\@Yx�@Ol�@J��@C��@=`B@,�D@@@v�@�w@��?�=q111111111111111111111111111111111111111111111111111111111111111111111111A�oA��A�A��;A�~�A�S�A��A�=qA�  A�\)A�  A�9XA�E�A�n�A��A���A�$�A�jA� �A�jA�ĜA��-A�
=A��/A�+A�x�Ayp�Ao�AhI�A`�AY�AQ%AG�TA<��A0ȴA$ĜA��A�AS�@�j@�R@�~�@�@�V@��`@�E�@�?}@��T@���@��@�"�@���@���@��F@��@���@K�@r��@k�F@b�\@Yx�@Ol�@J��@C��@=`B@,�D@@@v�@�w@��?�=q111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	"�B	"�B	.B	aHB	��B
e`B
�B%B<jB�B��B��B�B�B�Bu�BffBdZBXBQ�BM�B8RB&�B+B
�NB
��B
�B
bNB
9XB
�B	��B	��B	�B	t�B	;dB	�B�B�BƨB�9B�3B��B��B��B��B�B�LB�wB��B�TB��B	�B	33B	F�B	R�B	\)B	iyB	�+B	��B	�B	�jB	��B	�#B	�B	��B
�B
=qB
H�B
ZB
k�B
y�B
�111111111111111111111111111111111111111111111111111111111111111111111111B	"�B	"�B	0!B	gmB	�)B
gmB
�B1B>wB�B��B��B�B�'B�'Bv�BffBe`BXBR�BN�B9XB'�B1B
�TB
��B
�B
cTB
:^B
�B	��B	��B	�B	v�B	=qB	�B�B�BǮB�?B�9B��B��B��B��B�B�LB�}B��B�ZB��B	�B	33B	F�B	R�B	\)B	iyB	�+B	��B	�B	�jB	��B	�#B	�B	��B
�B
=qB
H�B
ZB
k�B
y�B
�111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809040805532008090408055320080904080553200809040822132008090408221320080904082213200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080827050954  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080827051003  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080827051003  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080827051004  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080827051012  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080827051012  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080827051012  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080827051012  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080827051013  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080827054506                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080829003928  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080829003928  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080829003932  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080829003932  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080829003932  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080829003932  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080829003933  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080829021347                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080831053254  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080831053311  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080831053314  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080831053315  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080831053324  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080831053324  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080831053324  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080831053324  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080831053326  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080831065904                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080831053254  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416003726  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416003726  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416003726  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416003727  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416003728  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416003728  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416003728  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416003728  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416003728  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416004147                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080830175411  CV  DAT$            G�O�G�O�F�]�                JM  ARCAJMQC1.0                                                                 20080904080553  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080904080553  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080904082213  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090907083446  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090907083625                      G�O�G�O�G�O�                