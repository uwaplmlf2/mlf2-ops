CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900662 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               �A   JA  20080901050235  20090907083625  A9_67032_137                    2C  D   APEX-SBE 2980                                                   846 @�����1   @������@72n��O�@`��Q�1   ARGOS   A   A   A   @�ffA��A�33A홚B33BE��Bm33B�  B�  B�ffB�  Bڙ�B�33CL�C�C�C33C)�3C3� C=33CG�C[L�Co33C�s3C�� C���C���C��fC�s3C���Cǳ3CѦfCۙ�C��C�� C��fD��D��DٚDٚD�fD�fDٚD$� D)�fD.� D3�3D8ٚD=��DB�3DG�fDN�DTFfDZ�fD`ٚDg3DmS3Ds�fDy��D�)�D�i�D���D��3D�#3D�i�D��D�i�D�� D�l�D��D�i�D�P 111111111111111111111111111111111111111111111111111111111111111111111111@�33A33A�ffA���B��BE33Bl��B���B���B�33B���B�ffB�  C33C  C  C�C)��C3ffC=�CG  C[33Co�C�ffC��3C���C�� C���C�ffC�� CǦfCљ�Cی�C� C�3C���D�fD�3D�3D�3D� D� D�3D$��D)� D.ٚD3��D8�3D=�3DB��DG� DNfDT@ DZ� D`�3Dg�DmL�Ds� Dy�3D�&fD�ffD���D�� D�  D�ffD��fD�ffD���D�i�D��fD�ffD�L�111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�5?A�1'A�33A�9XA��A��A���A�ZA�dZA�?}A��\A�I�A�l�A�M�A���A�z�A��HA�
=A�ȴA���A���A�ZA�hsA�33A��DA~��Au`BAk��Ad �AZ��AOXAIS�AB��A:Q�A6�DA/l�A$bNA-A��A��A�@��!@�;d@���@ύP@��@���@�@��@�v�@���@�V@��#@��P@��#@�j@��+@}@st�@h��@Wl�@BM�@@1'@8��@7��@+"�@!hs@|�@?}@�R?��w?���111111111111111111111111111111111111111111111111111111111111111111111111A�5?A�1'A�33A�9XA��A��A���A�ZA�dZA�?}A��\A�I�A�l�A�M�A���A�z�A��HA�
=A�ȴA���A���A�ZA�hsA�33A��DA~��Au`BAk��Ad �AZ��AOXAIS�AB��A:Q�A6�DA/l�A$bNA-A��A��A�@��!@�;d@���@ύP@��@���@�@��@�v�@���@�V@��#@��P@��#@�j@��+@}@st�@h��@Wl�@BM�@@1'@8��@7��@+"�@!hs@|�@?}@�R?��w?���111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	@�B	A�B	@�B	=qB	��B
��B
�B
�sBPB/B=qBw�B�1B�Bs�BgmBbNBW
BM�BG�BD�B7LB"�BB
�;B
�B
�B
R�B
)�B	�B	�RB	��B	~�B	W
B	ZB	33B	�B��B�B��B�wB�B��B��B�B�dBȴB�B�sB�B��B	+B	�B	'�B	>wB	C�B	[#B	jB	�B	��B	�}B	�B	�B
B
B
�B
1'B
E�B
_;B
o�B
|�B
�+111111111111111111111111111111111111111111111111111111111111111111111111B	@�B	A�B	B�B	F�B	��B
��B
�B
�B\B1'B@�Bx�B�7B�%Bu�BiyBcTBYBN�BH�BE�B8RB#�BB
�BB
�B
�B
S�B
+B	�B	�XB	��B	� B	XB	[#B	5?B	�B��B�B��B�}B�B��B�B�!B�jBɺB�B�sB�B��B	+B	�B	'�B	>wB	C�B	[#B	jB	�B	��B	�}B	�B	�B
B
B
�B
1'B
E�B
_;B
o�B
|�B
�+111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200809090832062008090908320620080909083206200809090846312008090908463120080909084631200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20080901050228  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080901050235  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080901050236  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080901050236  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080901050241  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080901050241  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080901050241  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080901050241  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080901050242  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080901052420                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080905042846  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20080905042851  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080905042852  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080905042852  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080905042856  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080905042856  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080905042856  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080905042856  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080905042857  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080905051232                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20080905042846  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416003728  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416003729  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416003729  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416003729  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416003730  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416003730  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416003730  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416003730  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416003730  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416004147                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20080904170550  CV  DAT$            G�O�G�O�F�g�                JM  ARCAJMQC1.0                                                                 20080909083206  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080909083206  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080909084631  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090907083447  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090907083625                      G�O�G�O�G�O�                