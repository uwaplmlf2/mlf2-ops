CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2901161 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20080917195805  20080917195805  0052_40949_011                  2C  D   APEX_SBE_3702                                                   846 @��M-��1   @�𬁄\�@5��    @a���   1   ARGOS   A   A   A   @�  A��Aa��A�ffA���A�  B	��B  B0��BF��BZffBn  B���B�33B�33B�  B�33B�  B�33B�  B�ffB�33B�  B�  B���C33C� CffC�C� C  C33C$ffC)� C.33C3�C8L�C=33CBL�CF�3CQ�C[��CeL�CoffCx�fC���C��fC�ffC���C��fC��fC��3C��3C�� C���C��fC�� C��fC³3CǦfC̀ Cь�C֦fC�L�C�s3C�Y�C�@ C� C�s3C�� D� D�3DٚD�3D��D��D�3D$�3D)�3D.��D3�fD8��D=ٚDB�3DG��DLٚDQٚDVٚD[�3D`�3De�fDj��Do��Dt�3Dy� D�#3D�i�D�� D��fD�0 D�i�D���D���D�,�D�l�D���D���D��D�p DڦfD�� D��D�\�D�fD�)�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A��Aa��A�ffA���A�  B	��B  B0��BF��BZffBn  B���B�33B�33B�  B�33B�  B�33B�  B�ffB�33B�  B�  B���C33C� CffC�C� C  C33C$ffC)� C.33C3�C8L�C=33CBL�CF�3CQ�C[��CeL�CoffCx�fC���C��fC�ffC���C��fC��fC��3C��3C�� C���C��fC�� C��fC³3CǦfC̀ Cь�C֦fC�L�C�s3C�Y�C�@ C� C�s3C�� D� D�3DٚD�3D��D��D�3D$�3D)�3D.��D3�fD8��D=ٚDB�3DG��DLٚDQٚDVٚD[�3D`�3De�fDj��Do��Dt�3Dy� D�#3D�i�D�� D��fD�0 D�i�D���D���D�,�D�l�D���D���D��D�p DڦfD�� D��D�\�D�fD�)�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�A��A�A��A�x�A���A�33A�t�A�
=A�S�A��HA���A�hsA�Q�Aß�A�\)A�?}A�\)A��PA�
=A�|�A�p�A��PA�hsA�Q�A���A�ƨA�v�A�^5A�v�A�|�A�^5A���A�v�A�jA�A���A��FA��A��A��jA��`A��/A�oA��\A�XA�/A���A{�TAv��Ar�+AmƨAjbNAhz�Ad~�A\�AWAR��AJbNAF��AC&�A@E�A;hsA3��A0bNA*-A(�\A$�A&�A�TAM�A��A`B@��@�h@�n�@ΰ!@ÍP@��m@���@���@��@�ff@�@�=q@�E�@��9@��y@�9X@�Q�@���@�M�@|�@y��@t��@e�@Y&�@M�-@C"�@6ff@2�@*��@&$�@ 1'@�#@j@�@�@C�@	��@��@��@��@ 1'?�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A��A�A��A�x�A���A�33A�t�A�
=A�S�A��HA���A�hsA�Q�Aß�A�\)A�?}A�\)A��PA�
=A�|�A�p�A��PA�hsA�Q�A���A�ƨA�v�A�^5A�v�A�|�A�^5A���A�v�A�jA�A���A��FA��A��A��jA��`A��/A�oA��\A�XA�/A���A{�TAv��Ar�+AmƨAjbNAhz�Ad~�A\�AWAR��AJbNAF��AC&�A@E�A;hsA3��A0bNA*-A(�\A$�A&�A�TAM�A��A`B@��@�h@�n�@ΰ!@ÍP@��m@���@���@��@�ff@�@�=q@�E�@��9@��y@�9X@�Q�@���@�M�@|�@y��@t��@e�@Y&�@M�-@C"�@6ff@2�@*��@&$�@ 1'@�#@j@�@�@C�@	��@��@��@��@ 1'?�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
l�B
k�B
l�B
k�B
~�B
�?B
ÖB
�
B
�BB
�B
��B'�BW
B�B��B��B�B�FBÖB�;B��B��B��BǮB�'B��B�bB�+B�B�B�B�B}�Bz�Br�BcTB\)BQ�BF�BH�B49B�B1B
�B
�TB
��B
�XB
�B
�VB
s�B
^5B
H�B
6FB
,B
uB	�B	��B	�jB	��B	�B	s�B	iyB	O�B	.B	�B	  B��B�`B��BB��B� Bo�BcTBP�BK�B_;B�bB��B��B�wB��B�5B��B	PB	)�B	7LB	C�B	R�B	e`B	w�B	�=B	�\B	��B	��B	��B	�qB	�B	�mB	��B
1B
�B
"�B
,B
:^B
F�B
N�B
W
B
[#B
^5B
dZB
jB
p�B
t�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
l�B
k�B
l�B
l�B
��B
��B
ƍB
�UB
��B
�B
�B*CB_B��B��B�?B��B�
B�B�BԦB͵B��BʎB��B�ZB��B�2B��B�/B��B�mB~XB|	Bu.BdpB]�BSBGYBJ6B5�B:B	�B
�vB
�hB
��B
��B
�B
��B
t�B
_kB
I�B
6�B
-B
�B	��B	�B	��B	��B	�B	t�B	j�B	R"B	.�B	jB	 �B��B�B��B�4B��B��BpQBd�BRWBLgB`B��B�vB��B�B�.BޗB�,B	�B	*'B	7{B	C�B	SGB	e�B	x.B	�wB	��B	��B	�B	�(B	��B	�GB	�B	��B
TB
�B
"�B
,&B
:xB
F�B
N�B
WB
[-B
^FB
deB
j�B
p�B
t�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;��
;��
;��
;��
;��
;��
;��
;��
;��
;��;��
;��
<��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201029162009072010291620090720102916  HZ  ARGQ                                                                        20080917195805  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080917195805  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102916  IP                  G�O�G�O�G�O�                