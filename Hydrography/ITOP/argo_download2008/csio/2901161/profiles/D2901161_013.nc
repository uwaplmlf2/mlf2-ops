CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2901161 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20081104221037  20081104221037  0052_40949_013                  2C  D   APEX_SBE_3702                                                   846 @���fOD1   @���=ѻ@5H��   @a�I�   1   ARGOS   A   A   A   @���A��Ac33A�ffA�33A���B
  B33B2��BE��BZ  Bo��B�ffB�ffB�  B�33B�ffB�ffB�33B���BЙ�B�33B�33B���B�  C�CffC33C33CL�CffCL�C$ffC)  C.L�C3�C7��C=33CB� CG� CQ� C[ffCeL�Cn�fCx�fC���C���C��fC�� C�ffC��fC�� C��fC���C��fC��fC���C���C¦fCǀ C̙�CѦfC�s3CۦfC���C�� C�fC���C��C��fD� D�3D�3D� D��DٚDٚD$� D)�fD.��D3ٚD8�3D=�fDBٚDG�fDL��DQ�3DV� D[�3D`� De�3DjٚDo�3DtٚDy�3D�#3D�p D��fD���D�)�D�c3D�� D���D�,�D�c3D�� D��fD�  D�ffDڣ3D���D�)�D�VfD�D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��Ac33A�ffA�33A���B
  B33B2��BE��BZ  Bo��B�ffB�ffB�  B�33B�ffB�ffB�33B���BЙ�B�33B�33B���B�  C�CffC33C33CL�CffCL�C$ffC)  C.L�C3�C7��C=33CB� CG� CQ� C[ffCeL�Cn�fCx�fC���C���C��fC�� C�ffC��fC�� C��fC���C��fC��fC���C���C¦fCǀ C̙�CѦfC�s3CۦfC���C�� C�fC���C��C��fD� D�3D�3D� D��DٚDٚD$� D)�fD.��D3ٚD8�3D=�fDBٚDG�fDL��DQ�3DV� D[�3D`� De�3DjٚDo�3DtٚDy�3D�#3D�p D��fD���D�)�D�c3D�� D���D�,�D�c3D�� D��fD�  D�ffDڣ3D���D�)�D�VfD�D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A� �A�x�A�ffA�Q�A��A��
A���A�S�A웦A�{A�C�A�-Aذ!A�9XA���A��A�jA��/A��FA�M�A���A��PA��A��A���A��A���A�?}A�`BA��A�1'A���A��A�x�A��mA�A�A�7LA�+A��+A��hA��-A�%A��A��jA�v�A|=qAvjAr��Ao��Aj��Ai/Ag��A\{AZ{AY�AO��AG7LAE\)A=K�A:bNA6��A3%A1p�A,�A+��A%+A�
Av�A��A1'A ��@�  @�^5@�(�@�C�@�5?@�A�@���@�ƨ@��@��F@���@��y@�-@�"�@���@�C�@�Z@|z�@w�;@o��@h�9@c�m@a7L@` �@YG�@Ol�@G�w@A%@:��@2~�@)�#@#33@��@�#@ȴ@�@��@/@	X@ȴ@�h@��?�1'?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A� �A�x�A�ffA�Q�A��A��
A���A�S�A웦A�{A�C�A�-Aذ!A�9XA���A��A�jA��/A��FA�M�A���A��PA��A��A���A��A���A�?}A�`BA��A�1'A���A��A�x�A��mA�A�A�7LA�+A��+A��hA��-A�%A��A��jA�v�A|=qAvjAr��Ao��Aj��Ai/Ag��A\{AZ{AY�AO��AG7LAE\)A=K�A:bNA6��A3%A1p�A,�A+��A%+A�
Av�A��A1'A ��@�  @�^5@�(�@�C�@�5?@�A�@���@�ƨ@��@��F@���@��y@�-@�"�@���@�C�@�Z@|z�@w�;@o��@h�9@c�m@a7L@` �@YG�@Ol�@G�w@A%@:��@2~�@)�#@#33@��@�#@ȴ@�@��@/@	X@ȴ@�h@��?�1'?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�HB
�5B
�5B
�/B
�)B
�)B
�B
��B
��B
��BDB�B_;B��B��B��B��B��B��BǮB�}B�?B��B��B��B�oB�\B�DB~�Br�Bo�BiyBcTB_;BVBF�B:^B.B"�B�BB
�B
�fB
��B
�wB
�oB
s�B
cTB
O�B
8RB
0!B
#�B	�B	�NB	�B	�B	�=B	�B	\)B	N�B	<jB	+B	!�B	DB	%B�B��B�qB�'B��Bu�Bl�BgmBR�B^5Bu�B��B�jB�BB��B	�B	(�B	8RB	P�B	W
B	_;B	[#B	x�B	u�B	z�B	�PB	��B	��B	�B	�B	�qB	��B	�yB	��B
B
VB
$�B
1'B
<jB
F�B
K�B
N�B
VB
\)B
cTB
gmB
o�B
v�B
}�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
��B
�GB
�JB
�\B
�iB
�RB
�hB
կB
ӴB
�B/B�BedB�B��B�OB��B��B��B˕B��B��B�YB��B��B�B�B��B�CBs0Bp�BjLBdB`�BX�BH�B<3B/�B$�B�B�B
�^B
��B
�mB
��B
��B
t�B
d;B
Q)B
8�B
0�B
&�B	�B	�B	ۑB	�EB	��B	�$B	] B	O�B	=lB	+�B	#B	�B	�B�B�SB��B�xB��Bv�BmBiBT?B_7BvFB�zB�4B��B�ZB	�B	)+B	8�B	Q+B	W>B	_�B	[B	yRB	u�B	{9B	��B	��B	�B	�!B	�>B	��B	�B	�B	��B
2B
}B
$�B
1BB
<~B
F�B
K�B
N�B
VB
\<B
cbB
gvB
o�B
v�B
~B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
<љ;���;��
<��;��
;��
;��
;�p�;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201029172009072010291720090720102917  HZ  ARGQ                                                                        20081104221037  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20081104221037  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102917  IP                  G�O�G�O�G�O�                