CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2901161 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               	A   HZ  20080828091823  20080828091823  0052_40949_009                  2C  D   APEX_SBE_3702                                                   846 @�묹u0�1   @����n@5�r�   @b�    1   ARGOS   A   A   A   @�33A��Ac33A���A���A���B33BffB2��BF  BXffBk��B�ffB�ffB���B�ffB�  B�33B���B�  B�33B���B䙚B���B���C� CL�C� CffC33CL�C�C$ffC)33C-�fC2�fC833C=33CA�fCF��CQ33C[33Cd�fCoL�CyffC��fC���C���C���C��3C�� C��fC��3C���C�� C��fC�s3C�s3C�� Cǌ�Č�Cљ�C�s3C�� C�3C�3C��C��C��fC�s3D�fD�3DٚD��D�3D��D��D$�fD)��D.�3D3��D8�3D=��DB�fDG� DL��DQٚDV� D[ٚD`� De�3Dj� Do�fDt�fDy� D�,�D�c3D���D��D��D�` D���D��fD�)�D�p D��3D�� D�  D�c3Dڣ3D���D��D�Y�D�3D�vf1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��Ac33A���A���A���B33BffB2��BF  BXffBk��B�ffB�ffB���B�ffB�  B�33B���B�  B�33B���B䙚B���B���C� CL�C� CffC33CL�C�C$ffC)33C-�fC2�fC833C=33CA�fCF��CQ33C[33Cd�fCoL�CyffC��fC���C���C���C��3C�� C��fC��3C���C�� C��fC�s3C�s3C�� Cǌ�Č�Cљ�C�s3C�� C�3C�3C��C��C��fC�s3D�fD�3DٚD��D�3D��D��D$�fD)��D.�3D3��D8�3D=��DB�fDG� DL��DQٚDV� D[ٚD`� De�3Dj� Do�fDt�fDy� D�,�D�c3D���D��D��D�` D���D��fD�)�D�p D��3D�� D�  D�c3Dڣ3D���D��D�Y�D�3D�vf1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�=qA�;dA�=qA��A�{A���A�VA�+Aǟ�A�|�A� �A��#A��9A��A��hA�ĜA�t�A���A�x�A�G�A���A��`A���A���A��TA���A�;dA�"�A�/A�A�A���A�-A�&�A���A�ƨA��A�Q�A���A�~�A�l�A�ƨA�ƨA�;dA���A�;dA�VA�"�A� �A|�DAu��Aqx�An  Ai�AfVAd�Aa��A\ĜAYƨAW7LAMx�AG��AD�9A;�A2�A.VA)�TA%�7A!/A�AhsA��A�DA�@�`B@�J@��@�-@�5?@���@�t�@�Q�@��D@�K�@�
=@�5?@�dZ@�r�@�-@��@��@�ff@��@�  @�b@y��@k��@eO�@V��@N��@EV@;"�@/|�@'�@$�@v�@��@X@t�@`B@	X@�D@J?���?��y?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�=qA�;dA�=qA��A�{A���A�VA�+Aǟ�A�|�A� �A��#A��9A��A��hA�ĜA�t�A���A�x�A�G�A���A��`A���A���A��TA���A�;dA�"�A�/A�A�A���A�-A�&�A���A�ƨA��A�Q�A���A�~�A�l�A�ƨA�ƨA�;dA���A�;dA�VA�"�A� �A|�DAu��Aqx�An  Ai�AfVAd�Aa��A\ĜAYƨAW7LAMx�AG��AD�9A;�A2�A.VA)�TA%�7A!/A�AhsA��A�DA�@�`B@�J@��@�-@�5?@���@�t�@�Q�@��D@�K�@�
=@�5?@�dZ@�r�@�-@��@��@�ff@��@�  @�b@y��@k��@eO�@V��@N��@EV@;"�@/|�@'�@$�@v�@��@X@t�@`B@	X@�D@J?���?��y?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
p�B
p�B
o�B
�B
��B
�ZB&�BB�BaHBH�B~�B��B��B��B��B��B��B��B�B��B��B��B�JB�B�%B~�By�Br�BjBffB`BBZBP�BI�BC�B@�B;dB6FB0!B)�B �B�B�BB
��B
�`B
��B
�dB
��B
t�B
\)B
G�B
2-B
!�B
�B
1B	�B	�yB	�B	��B	�\B	�B	XB	1'B	!�B	bB��B�sB�5B�#BĜB�LB��B�1B{�Bs�Bv�B}�B��B�!B��B�;B�fB��B	{B	)�B	9XB	F�B	N�B	gmB	v�B	�%B	��B	�B	�-B	��B	�ZB	�sB	�B	��B
+B
�B
 �B
(�B
7LB
C�B
F�B
H�B
VB
]/B
e`B
k�B
s�B
x�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
p�B
p�B
p�B
�`B
��B
�B)�BGHBe�BL�B:B��B�6B��B�=B��B�vB��B��B��B��B�|B�?B��B��B�B{�Bt�BkhBf�Ba�B[�BREBK�BDaBA!B= B7tB11B*�B!DB�B�B�B
��B
�~B
��B
�SB
�B
u�B
]B
H�B
3B
"<B
ZB
	�B	�wB	�!B	بB	�fB	�6B	��B	Z�B	28B	#B	�B�B�-BޣB�?BŤB��B��B�QB}hBt�BwpB~�B�B�nB�*B�cB��B�VB	�B	*?B	9�B	F�B	O'B	g�B	w B	�|B	��B	�SB	�jB	�B	�B	�B	��B	�B
^B
�B
 �B
)B
7`B
C�B
F�B
H�B
VB
]FB
enB
k�B
s�B
x�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;��
;��
;��
;��
<-�;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201029152009072010291520090720102915  HZ  ARGQ                                                                        20080828091823  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080828091823  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102915  IP                  G�O�G�O�G�O�                