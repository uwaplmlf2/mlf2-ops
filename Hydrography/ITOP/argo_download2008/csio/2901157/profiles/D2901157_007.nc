CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   p   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4\   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C<   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E,   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H,   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K,   CALIBRATION_DATE            	             
_FillValue                  ,  N,   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    NX   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N\   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N`   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Nd   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Nh   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  2901157 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20080805092311  20080805092311  0048_80054_007                  2C  D   APEX_SBE_3698                                                   846 @����K�1   @�����4@7j��   @`U��   1   ARGOS   A   A   A   @�33A  Ah  A�  A�33A噚B33B33B2  BFffBX��BlffB���B�  B�  B���B�  B�33B�ffB�ffB�  Bڙ�B�ffB�33B���CffC33CffC  C�CL�C  C$ffC)��C.ffC3��C8L�C=ffCB33CGffCQL�CZ�fCd�fCoffCyffC��fC���C��3C���C���C��3C���C��3C���C���C��fC��fC��fC�s3C�� C�� C֌�CۦfC�fC�fC�� C�ffC��fC�� DٚD� D�3D��D� D��DٚD$� D)��D.��D3� D8��D=� DB�3DG� DLٚDQ�3DV�3D[��D`�3De�fDjٚDo�3Dt�fDyٚD�)�D�i�D���D��fD�)�D�i�D���D��3D�  D�c3D���D��fD�#3D�p Dڣ3D���D���D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�33A  Ah  A�  A�33A噚B33B33B2  BFffBX��BlffB���B�  B�  B���B�  B�33B�ffB�ffB�  Bڙ�B�ffB�33B���CffC33CffC  C�CL�C  C$ffC)��C.ffC3��C8L�C=ffCB33CGffCQL�CZ�fCd�fCoffCyffC��fC���C��3C���C���C��3C���C��3C���C���C��fC��fC��fC�s3C�� C�� C֌�CۦfC�fC�fC�� C�ffC��fC�� DٚD� D�3D��D� D��DٚD$� D)��D.��D3� D8��D=� DB�3DG� DLٚDQ�3DV�3D[��D`�3De�fDjٚDo�3Dt�fDyٚD�)�D�i�D���D��fD�)�D�i�D���D��3D�  D�c3D���D��fD�#3D�p Dڣ3D���D���D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A�^5A�RA睲A�uA�A�v�A�l�A�`BA�\A�%A�VA���A�n�A�"�Aԟ�Aҧ�A�/A�;dA˅A��A���A©�A�5?A�$�A�1A�p�A�%A��A�ƨA���A�A��A���A��A��`A�dZA��A��A�v�A��A���A�^5A���A�O�A���A���A�t�A�(�A�E�A��jA�1'A��+A���A���A�A�A�7LA�&�A��A|��Av�!Al�Ag��Ad$�A_�FAW�
AP5?AK��AF�+A?��A6(�A1dZA(��A �jA��AK�A�@��F@��@�9@�l�@���@�(�@��!@���@�+@���@�j@��D@��@���@��^@�b@��@�33@y��@kS�@^$�@T9X@L��@HQ�@=/@6�+@/�@&5?@��@�F@
=@1@�@33@�H1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A�^5A�RA睲A�uA�A�v�A�l�A�`BA�\A�%A�VA���A�n�A�"�Aԟ�Aҧ�A�/A�;dA˅A��A���A©�A�5?A�$�A�1A�p�A�%A��A�ƨA���A�A��A���A��A��`A�dZA��A��A�v�A��A���A�^5A���A�O�A���A���A�t�A�(�A�E�A��jA�1'A��+A���A���A�A�A�7LA�&�A��A|��Av�!Al�Ag��Ad$�A_�FAW�
AP5?AK��AF�+A?��A6(�A1dZA(��A �jA��AK�A�@��F@��@�9@�l�@���@�(�@��!@���@�+@���@�j@��D@��@���@��^@�b@��@�33@y��@kS�@^$�@T9X@L��@HQ�@=/@6�+@/�@&5?@��@�F@
=@1@�@33@�H1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�fB	�B	�B	�
B	�B	�#B	�/B	�5B	�/B	��B
/B
Q�B
;dB
I�B
hsB
n�B
k�B
l�B
�1B
�B
�NBB7LBT�Bt�Br�B�B�DB�B�B�%B�+B�1B�+B�=B�B�+B�B{�Bz�Bw�Bo�BiyB]/BR�BI�BL�BF�B;dB2-B)�B �B�BVB  B
�B
�#B
ĜB
�B
�bB
o�B
:^B
!�B
bB	��B	��B	�?B	��B	�uB	r�B	J�B	7LB	�B��B�B�jB��BȴB�wB�'B��B�B�qB��B�HB�B�B��B	-B	F�B	N�B	XB	VB	[#B	y�B	�7B	��B	�jB	��B	�;B	�B
B
hB
�B
#�B
0!B
<jB
C�B
J�B
P�B
n�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	�{B	�SB	�&B	�B	�5B	�9B	�BB	�SB	�'B	�gB
1�B
S�B
<�B
K�B
i�B
pQB
m�B
nB
��B
�B
�vB�B9�BV�Bv�Bt'B�\B�OB� B�B��B��B�8B��B�;B��B�~B�	B}3B{�ByXBpOBj�B^KBS�BJIBM�BG�B<RB2�B*�B!B�B/B �B
�B
�+B
ŬB
�qB
��B
p�B
;�B
"�B
�B	��B	�B	�kB	�4B	�VB	tB	KmB	8�B	�B�KB�jB��B͜B�|B�(B�%B�>B��B��B�]B�B��B�B��B	-&B	F�B	OnB	X�B	VFB	[[B	zB	�wB	��B	��B	�B	�SB	�B
2B
�B
�B
#�B
06B
<�B
C�B
J�B
P�B
n�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;��
;��
;��
;��
;��
;��
;��
;��
;��
;�R,;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907200325532009072003255320090720032553  HZ  ARGQ                                                                        20080805092311  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080805092311  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720032553  IP                  G�O�G�O�G�O�                