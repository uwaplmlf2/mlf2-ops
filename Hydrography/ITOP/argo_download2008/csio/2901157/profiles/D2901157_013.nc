CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   r   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4d   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =T   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  AX   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C    PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E\   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         O,   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         O0   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O4   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O8Argo profile    2.2 1.2 19500101000000  2901157 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20090107115857  20090107115857  0048_80054_013                  2C  D   APEX_SBE_3698                                                   846 @����\1   @��횼�@5E��   @`��    1   ARGOS   A   A   A   @���AffAfffA���A�  A�  B	��B33B133BFffB[��BnffB�33B�  B�ffB�33B�33B�ffB�ffB�33B�ffB�  B���B�ffB���C�C�fC
�3C�fC��C  C��C#�fC)  C-�fC3�C8L�C=L�CB��CGL�CQ� C[ffCe�Co� Cx�fC���C�� C��fC�� C�s3C�� C���C���C��3C��fC���C�� C���C�s3Cǌ�C�Y�Cь�C�ffCۀ C�� C�3C�3C�� C��fC���D�3D� D��D�fD�3DٚD� D$�3D)��D.�3D3�3D8��D=� DB��DG��DL� DQ� D`�3De� Dj�fDo�3Dt��DyٚD�)�D�l�D�� D���D�&fD�l�D��3D���D��D�c3D���D��3D�)�D�i�Dڠ D��3D��D�i�D�D���D�Ff111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���AffAfffA���A�  A�  B	��B33B133BFffB[��BnffB�33B�  B�ffB�33B�33B�ffB�ffB�33B�ffB�  B���B�ffB���C�C�fC
�3C�fC��C  C��C#�fC)  C-�fC3�C8L�C=L�CB��CGL�CQ� C[ffCe�Co� Cx�fC���C�� C��fC�� C�s3C�� C���C���C��3C��fC���C�� C���C�s3Cǌ�C�Y�Cь�C�ffCۀ C�� C�3C�3C�� C��fC���D�3D� D��D�fD�3DٚD� D$�3D)��D.�3D3�3D8��D=� DB��DG��DL� DQ� D`�3De� Dj�fDo�3Dt��DyٚD�)�D�l�D�� D���D�&fD�l�D��3D���D��D�c3D���D��3D�)�D�i�Dڠ D��3D��D�i�D�D���D�Ff111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�VA�VA�9XA�bA�wA�DA�JA���A�ĜA��A؝�A���A��A�"�A�z�A�t�A��A�-A���A���A��
A���A�A�v�A��\A���A�`BA�ffA���A��\A��PA�ĜA��A���A�"�A��\A�A�5?A�Q�A��A��A��A�$�A�n�A�&�A�n�A�7LA���A��TA?}A|1Az{Aw/ArbAlz�AhJAa��A\��AT��AN�`AJ{AE`BA@JA>�DA:��A4�A+�A&�A"�+A7LAZA�\AQ�A�DA j@���@�ƨ@�E�@̣�@�{@�\)@�Ĝ@�9X@�@�b@��R@��@�G�@��@��#@}�@x�u@up�@j��@\I�@NE�@IX@B��@;�@4z�@/
=@&�R@#dZ@!��@��@x�@�@��@�`@V@
�@��@(�@(�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�VA�VA�9XA�bA�wA�DA�JA���A�ĜA��A؝�A���A��A�"�A�z�A�t�A��A�-A���A���A��
A���A�A�v�A��\A���A�`BA�ffA���A��\A��PA�ĜA��A���A�"�A��\A�A�5?A�Q�A��A��A��A�$�A�n�A�&�A�n�A�7LA���A��TA?}A|1Az{Aw/ArbAlz�AhJAa��A\��AT��AN�`AJ{AE`BA@JA>�DA:��A4�A+�A&�A"�+A7LAZA�\AQ�A�DA j@���@�ƨ@�E�@̣�@�{@�\)@�Ĝ@�9X@�@�b@��R@��@�G�@��@��#@}�@x�u@up�@j��@\I�@NE�@IX@B��@;�@4z�@/
=@&�R@#dZ@!��@��@x�@�@��@�`@V@
�@��@(�@(�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�ZB	�ZB	�HB	�5B	�5B	�5B	�yB	�BB	�B	�;B	��B
G�B
gmB
��B
�XB
�B
�TBbB{BPB-B9XB?}BS�BO�BS�BVBW
BVBW
BN�BJ�BN�BM�BM�BK�BG�BD�BA�B;dB33B'�B�B1B
��B
�B
�/B
ǮB
�9B
��B
��B
�=B
|�B
ffB
J�B
33B
�B	��B	��B	�9B	��B	�7B	r�B	jB	ZB	B�B	!�B	PB	B��B�BB��BB�XB�B�B��B��B��B�LB�qBȴB�
B�ZB��B	bB	�B	B�B	P�B	`BB	k�B	v�B	}�B	�B	��B	�ZB	��B
+B
�B
%�B
49B
;dB
J�B
O�B
ZB
bNB
gmB
jB
m�B
q�B
u�B
y�B
~�B
� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B	�eB	�}B	�xB	ނB	�iB	޲B	�{B	��B	��B	�|B
�B
K�B
mVB
�uB
�sB
�=B
�B�B�BB/�B;DBA�BV�BP�BU*BV�BW�BW@BW�BO�BKVBOQBNQBNeBM.BHtBEtBB�B<VB4/B(�B�B�B
��B
�B
�SB
��B
��B
��B
�B
��B
~4B
g�B
K�B
4�B
�B	��B	�]B	��B	��B	��B	s B	k{B	[�B	EB	#IB	ZB	B��B� B��B�jB�BB�'B�2B��B��B��B��B��B�]B��B��B�LB	�B	B	B�B	Q+B	`�B	k�B	v�B	~'B	�HB	�B	�tB	�B
MB
�B
%�B
4_B
;vB
J�B
O�B
Z-B
b`B
gyB
j�B
m�B
q�B
u�B
y�B
~�B
�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;��
;��
;��
;��
;��
;��
;��
;��
;��
;�WJ;���;��
;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907200325562009072003255620090720032556  HZ  ARGQ                                                                        20090107115857  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20090107115857  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720032556  IP                  G�O�G�O�G�O�                