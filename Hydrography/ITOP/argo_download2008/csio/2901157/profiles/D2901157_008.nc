CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   r   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4d   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =T   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  AX   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C    PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E\   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         O,   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         O0   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O4   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O8Argo profile    2.2 1.2 19500101000000  2901157 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20080815093532  20080815093532  0048_80054_008                  2C  D   APEX_SBE_3698                                                   846 @��n:Ӡ1   @��n�+<@7�$�   @`d�    1   ARGOS   A   A   A   @���A33Ah  A�33A���A�  B
  B��B2��BF  BY33BlffB���B�ffB���B���B�ffB�  B���B���B�ffB�33B�33B�  B�33C33CL�C��C�C33CL�C$L�C)33C.L�C3ffC8L�C=��CBffCG�CQ� C[�CeffCn�fCyffC��3C�� C���C��fC���C���C�s3C�s3C���C�� C�� C�� C��3C�� CǦfC�� CѦfC֦fC۳3C�3C�fC� C�fC��fC�� DٚD�3D�fD� DٚD�fDٚD$�fD)� D.� D3��D8�3D=� DB�fDG�fDL�3DQ�fDV��D[��D`��De�3DjٚDo� Dt��Dy��D�)�D�c3D���D��fD�  D�ffD��fD�� D�,�D�c3D���D���D�&fD�i�DڦfD���D��D�ffD�D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���A33Ah  A�33A���A�  B
  B��B2��BF  BY33BlffB���B�ffB���B���B�ffB�  B���B���B�ffB�33B�33B�  B�33C33CL�C��C�C33CL�C$L�C)33C.L�C3ffC8L�C=��CBffCG�CQ� C[�CeffCn�fCyffC��3C�� C���C��fC���C���C�s3C�s3C���C�� C�� C�� C��3C�� CǦfC�� CѦfC֦fC۳3C�3C�fC� C�fC��fC�� DٚD�3D�fD� DٚD�fDٚD$�fD)� D.� D3��D8�3D=� DB�fDG�fDL�3DQ�fDV��D[��D`��De�3DjٚDo� Dt��Dy��D�)�D�c3D���D��fD�  D�ffD��fD�� D�,�D�c3D���D���D�&fD�i�DڦfD���D��D�ffD�D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A���A虚A���A�~�A�hsA�^5A�/A�A�A��A�ȴA؃A���A�G�AХ�A��A�?}A�&�A��A��A�z�A��7A��A���A�-A�jA��hA�(�A��7A�p�A��wA��#A���A�?}A�A�A���A��A�G�A�ĜA�hsA�oA�ƨA���A�v�A��7A�M�A��A��^A��A�ffA��A�ffA�9XA���A���A���A~VAz�Au/ApjAm�FAg�#AbE�A^�+AY��AQ��AK%AFbNA?��A7C�A0  A'%A#l�AA�A�7A	;dA��@��@�I�@�j@�ȴ@�  @��@�9X@�$�@��@�^5@�=q@���@���@��@�O�@���@�Z@y�@p�@ct�@W�w@MO�@F�@@Q�@7��@0Ĝ@*�H@"�H@K�@�7@�/@��@@
�@�@�@�^111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A���A���A虚A���A�~�A�hsA�^5A�/A�A�A��A�ȴA؃A���A�G�AХ�A��A�?}A�&�A��A��A�z�A��7A��A���A�-A�jA��hA�(�A��7A�p�A��wA��#A���A�?}A�A�A���A��A�G�A�ĜA�hsA�oA�ƨA���A�v�A��7A�M�A��A��^A��A�ffA��A�ffA�9XA���A���A���A~VAz�Au/ApjAm�FAg�#AbE�A^�+AY��AQ��AK%AFbNA?��A7C�A0  A'%A#l�AA�A�7A	;dA��@��@�I�@�j@�ȴ@�  @��@�9X@�$�@��@�^5@�=q@���@���@��@�O�@���@�Z@y�@p�@ct�@W�w@MO�@F�@@Q�@7��@0Ĝ@*�H@"�H@K�@�7@�/@��@@
�@�@�@�^111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBr�Br�B�JB��B	(�B	��B	��B	B	�B
%�B
L�B
P�B
[#B
ffB
z�B
�B
��B
�B
��BPB'�B�B
��B�BaHB�B��B�VB�B�B��B��B��B��B��B��B��B��B�oB�B�Bu�Bn�B_;BW
BK�B?}B33B �BuB1B
��B
�mB
��B
��B
�B
��B
�7B
hsB
T�B
D�B
-B
PB	��B	�;B	�jB	��B	�PB	x�B	S�B	2-B	hB	B�yB��BƨB��B�wB�^B�^B�wB�LB��BǮB��B�HB��B	B	{B	&�B	=qB	I�B	R�B	e`B	�B	��B	�LB	��B	�5B	�B	��B

=B
�B
�B
(�B
6FB
@�B
I�B
O�B
YB
`BB
e`B
l�B
q�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  Br�Bs�B��B�uB	(ZB	��B	�+B	�B	��B
*rB
N�B
SB
]�B
hB
}�B
��B
ЫB
�kB
�uB�B)�B�B
��BBbB��B��B��BB��B��B�0B��B��B�HB��B��B�B��B�?B�Bw@Bo�B_�BXBMB@nB4�B!�B3B�B
��B
�;B
��B
�xB
��B
��B
��B
i�B
U�B
FB
.}B
MB	�+B	�]B	�B	��B	��B	zB	T�B	3pB	�B	^B�B�BǦB�*B�$B�@B� B��B��B�B��B͏B��B�0B	YB	�B	'SB	=�B	I�B	S;B	e�B	�@B	��B	��B	��B	�UB	�B	��B

^B
�B
�B
)	B
6aB
@�B
I�B
O�B
Y*B
`SB
eoB
l�B
q�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;��
;��
;��
;��
;��
;��
;��
;��
;æq;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907200325542009072003255420090720032554  HZ  ARGQ                                                                        20080815093532  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080815093532  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720032554  IP                  G�O�G�O�G�O�                