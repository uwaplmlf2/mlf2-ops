CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   q   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4`   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =@   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A<   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C    PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Ct   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E8   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    Eh   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Hh   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Kh   CALIBRATION_DATE            	             
_FillValue                  ,  Nh   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         O   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         O   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    OArgo profile    2.2 1.2 19500101000000  2901157 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20080924184621  20080924184621  0048_80054_012                  2C  D   APEX_SBE_3698                                                   846 @��m�'qf1   @��m�:�@5WK�   @`y�`   1   ARGOS   A   A   A   @�  A33Ac33A�  A�33A���B
  B��B2ffBG��BZ  Bo33B�33B���B���B�ffB���B�ffB���B�  B�ffB���B�33B���B�33C� CffC��C� CffCffCffC$L�C)��C.� C333C8L�C<�fCA�fCG  CQ33C[ffCe��Co33Cy� C��3C�� C�� C���C���C��3C���C���C�� C�� C���C�C�� C̳3C�s3Cր Cی�C�ffC�ffC�ffCC���C���DٚDٚD��D�3D��D��D�fD$��D)�3D.ٚD3ٚD8�fD=��DBٚDG��DL�fDQ� DV�3D[��D`ٚDe��Dj� Do�3DtٚDy� D�  D�p D���D�� D�0 D�i�D���D��fD�,�D�i�D��fD��fD�0 D�l�Dڠ D���D�#3D�` D�D�\�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�  A33Ac33A�  A�33A���B
  B��B2ffBG��BZ  Bo33B�33B���B���B�ffB���B�ffB���B�  B�ffB���B�33B���B�33C� CffC��C� CffCffCffC$L�C)��C.� C333C8L�C<�fCA�fCG  CQ33C[ffCe��Co33Cy� C��3C�� C�� C���C���C��3C���C���C�� C�� C���C�C�� C̳3C�s3Cր Cی�C�ffC�ffC�ffCC���C���DٚDٚD��D�3D��D��D�fD$��D)�3D.ٚD3ٚD8�fD=��DBٚDG��DL�fDQ� DV�3D[��D`ٚDe��Dj� Do�3DtٚDy� D�  D�p D���D�� D�0 D�i�D���D��fD�,�D�i�D��fD��fD�0 D�l�Dڠ D���D�#3D�` D�D�\�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�A�DA靲A��A靲A�5?A�VA��Aߙ�A�hsA�|�A�`BA�\)A°!A��A�G�A�1A��PA���A�1'A�n�A���A�/A�p�A�S�A���A�`BA� �A��hA��A�VA�\)A�1'A���A��TA���A��-A�JA�;dA�7LA�bA���A�p�A�(�A�^5A��hA�$�A�ĜA��A�9XA|�HAy7LAs�AohsAjbNAc�A_�
A[?}AS��AL�DAH�ADVA@�DA:9XA7+A1/A)/A%��Ap�AhsAA��@�33@���@��/@ёh@ȃ@�~�@��F@���@�/@��u@��j@��/@�9X@�Ĝ@��9@�=q@��@~�@wl�@q��@m�-@c33@Y�7@Nȴ@CdZ@<��@4�D@.�y@(��@'K�@$�j@!��@�@x�@�+@hs@
=@��@Q�@V@�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�A�DA靲A��A靲A�5?A�VA��Aߙ�A�hsA�|�A�`BA�\)A°!A��A�G�A�1A��PA���A�1'A�n�A���A�/A�p�A�S�A���A�`BA� �A��hA��A�VA�\)A�1'A���A��TA���A��-A�JA�;dA�7LA�bA���A�p�A�(�A�^5A��hA�$�A�ĜA��A�9XA|�HAy7LAs�AohsAjbNAc�A_�
A[?}AS��AL�DAH�ADVA@�DA:9XA7+A1/A)/A%��Ap�AhsAA��@�33@���@��/@ёh@ȃ@�~�@��F@���@�/@��u@��j@��/@�9X@�Ĝ@��9@�=q@��@~�@wl�@q��@m�-@c33@Y�7@Nȴ@CdZ@<��@4�D@.�y@(��@'K�@$�j@!��@�@x�@�+@hs@
=@��@Q�@V@�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��B��B��B��B��B	�B	�B	}�B	}�B	��B
�B
N�B
~�B
��B
�yB
�HB
��B
��B{BffBgmB�B��B��B��B�VBy�B]/BT�BS�BO�BT�B[#BYBS�BVBR�BO�BN�BL�BK�BC�B33B �B�B
=B
��B
�BB
��B
��B
��B
�%B
gmB
T�B
@�B
"�B
bB	��B	ȴB	�B	�uB	�B	q�B	XB	H�B	2-B	�B	oB�B�)BƨB�B�B�B�-B��B�B�LBB��B�mB��B	1B	�B	)�B	/B	=qB	L�B	VB	hsB	z�B	�1B	�hB	�XB	��B	�B	�B
B
bB
"�B
0!B
A�B
M�B
S�B
\)B
aHB
ffB
hsB
q�B
v�B
{�B
� B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B��B��B��B��B�bB	�B	%�B	SB	�
B	��B
�B
R�B
�nB
�B
�B
�B
�B
��B�BhBg�B��B��B�B��B��B{�B^�BU�BTsBQ�BWB\6BZ.BU=BV�BS�BP�BO�BM�BL�BD�B4�B!'BB
�B
�	B
�]B
�zB
�zB
��B
��B
hoB
VIB
B8B
#�B
�B	��B	ʫB	�7B	�zB	�$B	s_B	X�B	JFB	4\B	�B	�B�B�@B�&B��B�GB��B�B��B��B�$B�LB�lB��B�?B	�B	B	*�B	/uB	=�B	MB	VdB	h�B	{!B	�bB	��B	��B	�B	�B	��B
9B
~B
"�B
0-B
A�B
M�B
TB
\;B
aXB
f~B
h�B
q�B
v�B
{�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;��
;��
;��
;��
;��
;��
;��;��
;� <��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907200325562009072003255620090720032556  HZ  ARGQ                                                                        20080924184621  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080924184621  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720032556  IP                  G�O�G�O�G�O�                