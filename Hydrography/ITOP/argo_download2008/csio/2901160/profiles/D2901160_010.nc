CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2901160 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               
A   HZ  20080906091636  20080906091636  0051_40948_010                  2C  D   APEX_SBE_3701                                                   846 @����E�1   @����(��@3q�   @a���   1   ARGOS   A   A   A   @�ffA��AfffA�  Ař�A���B
ffBffB2��BFffBXffBlffB�ffB�ffB�33B���B�ffB�ffB���B���B�33B�ffB�  B�33B���C��CffCL�CffC�fC�fC  C#�3C)L�C.33C3ffC8ffC=L�CBffCGL�CQffC[33CeffCo�Cy  C�� C���C��3C��fC�� C��3C���C��fC�� C�� C��3C���C�s3C�ffC�ffC̳3Cљ�C֌�CۦfC���C�fC�� C�s3C���C��3D��D�fDٚD�fDٚD��D� D$��D)ٚD.ٚD3�3D8ٚD=�3DB�fDG� DL��DQ�3DV� D[��D`�fDe�3Dj�fDo��Dt�3Dy�3D�&fD�` D��3D���D��D�ffD���D��D�&fD�i�D���D���D�  D�ffDڠ D��3D�,�D�ffD�fD��3D� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA��AfffA�  Ař�A���B
ffBffB2��BFffBXffBlffB�ffB�ffB�33B���B�ffB�ffB���B���B�33B�ffB�  B�33B���C��CffCL�CffC�fC�fC  C#�3C)L�C.33C3ffC8ffC=L�CBffCGL�CQffC[33CeffCo�Cy  C�� C���C��3C��fC�� C��3C���C��fC�� C�� C��3C���C�s3C�ffC�ffC̳3Cљ�C֌�CۦfC���C�fC�� C�s3C���C��3D��D�fDٚD�fDٚD��D� D$��D)ٚD.ٚD3�3D8ٚD=�3DB�fDG� DL��DQ�3DV� D[��D`�fDe�3Dj�fDo��Dt�3Dy�3D�&fD�` D��3D���D��D�ffD���D��D�&fD�i�D���D���D�  D�ffDڠ D��3D�,�D�ffD�fD��3D� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A훦A���A�l�A�XA�`BA�$�A�A䛦A�
=A�p�A˾wA�z�A��A��^A�^5A��A���A�/A��A��A�p�A���A�&�A�hsA��\A�+A�;dA�JA��A�ZA�l�A�hsA��-A�{A�G�A���A�/A��`A�&�A��!A��A�hsA��+A�hsA��PA�\)A�^5A���A�~�A|�HAt�AnffAdbNA`�DA]�mAW�AT�+AO&�AH�+AC�A;��A3�PA*��A)�PA&-A"��A�AA��AĜA�A {@�=q@ާ�@��@���@�J@�ff@���@��9@�@�I�@�I�@�"�@�
=@��;@��H@�
=@��T@~v�@~��@|�@vE�@r=q@nV@g|�@d�@[t�@W�P@I��@DI�@;�F@1�#@,z�@%��@!�7@�@G�@1@�`@@
��@|�@dZ@��@��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A훦A���A�l�A�XA�`BA�$�A�A䛦A�
=A�p�A˾wA�z�A��A��^A�^5A��A���A�/A��A��A�p�A���A�&�A�hsA��\A�+A�;dA�JA��A�ZA�l�A�hsA��-A�{A�G�A���A�/A��`A�&�A��!A��A�hsA��+A�hsA��PA�\)A�^5A���A�~�A|�HAt�AnffAdbNA`�DA]�mAW�AT�+AO&�AH�+AC�A;��A3�PA*��A)�PA&-A"��A�AA��AĜA�A {@�=q@ާ�@��@���@�J@�ff@���@��9@�@�I�@�I�@�"�@�
=@��;@��H@�
=@��T@~v�@~��@|�@vE�@r=q@nV@g|�@d�@[t�@W�P@I��@DI�@;�F@1�#@,z�@%��@!�7@�@G�@1@�`@@
��@|�@dZ@��@��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
dZB
dZB
n�B
cTB
]/B
aHB
e`B
��B
��B
�5B��B��BĜB�B�7Bo�Bu�Bp�Bn�Bk�BgmBu�Bv�Bx�Bv�Bt�BdZBbNB[#BN�BI�BD�BC�B@�B<jB8RB5?B/B+B%�B�BhBB
�B
�yB
�;B
��B
�dB
�B
��B
o�B
M�B
�B
PB
B	�NB	��B	�FB	��B	�B	[#B	7LB	uB	VB	  B�B��BĜB�-B��B�DB�Br�Bv�Bs�B�B�BÖB�
B��B	{B	5?B	@�B	R�B	e`B	q�B	�hB	�!B	�RB	�^B	��B	�)B	�sB	�B	��B
+B
DB
�B
�B
0!B
49B
=qB
H�B
VB
\)B
aHB
e`B
hsB
hsB
k�B
t�B
w�B
z�B
{�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
dXB
d�B
o�B
d�B
]mB
a�B
i~B
��B
�B
��B�3B�uB��B��B�ABs�Bx�Br)BpBmBk"Bv!Bw{By�Bx"BxpBd�Bb�B_�BO�BJ�BEVBD"BAJB=B8�B6�B/�B+�B&�B*BRB&B
�B
�B
߻B
�TB
��B
�B
��B
q1B
PsB
�B
B
�B	� B	�cB	�B	��B	�B	][B	9�B	�B	PB	 �B�|B�.B�B�KB�qB��B�Bt!BxBt�B��B��B��B�PB�IB	�B	5�B	@�B	SLB	e�B	rB	�oB	�@B	��B	�eB	�B	�pB	�B	��B	��B
<B
nB
�B
�B
0<B
4`B
=�B
H�B
V#B
\=B
a^B
esB
h�B
h�B
k�B
t�B
w�B
z�B
{�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;��
;��
;��
;��
;��
;��
;��
;��
;��
<���;��;��
;�f�;�o�;�x;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201028542009072010285420090720102854  HZ  ARGQ                                                                        20080906091636  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080906091636  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102854  IP                  G�O�G�O�G�O�                