CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2901160 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20081006091648  20081006091648  0051_40948_013                  2C  D   APEX_SBE_3701                                                   846 @��w͎�1   @��w�]�R@4U   @b�`   1   ARGOS   A   A   A   @�33A��Ac33A���A�33A�ffB	33B��B0��BFffBZffBm��B�ffB���B�33B�  B���B�ffB�33B�ffB�33B���B䙚B홚B�  C� C�3C� CffC�C�fCL�C#��C(��C.33C3  C7��C=  CB  CGL�CQ33C[�Ce� CoL�Cx�fC���C���C���C�ffC���C��3C��fC�� C���C��3C��3C�ffC��3C C�s3C̳3Cь�Cֳ3Cۙ�C���C�3C�� C�3C��fC�ffD�fD�3DٚDٚD�fD�3D�fD$��D)ٚD.��D3� D8��D=� DB�3DGٚDL� DQ��DVٚD[��D`�fDeٚDj� Do�fDt��DyٚD�&fD�c3D��fD��fD�#3D�c3D��fD�� D�0 D�` D���D��3D��D�ffDڦfD��fD�)�D�VfD�3D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��Ac33A���A�33A�ffB	33B��B0��BFffBZffBm��B�ffB���B�33B�  B���B�ffB�33B�ffB�33B���B䙚B홚B�  C� C�3C� CffC�C�fCL�C#��C(��C.33C3  C7��C=  CB  CGL�CQ33C[�Ce� CoL�Cx�fC���C���C���C�ffC���C��3C��fC�� C���C��3C��3C�ffC��3C C�s3C̳3Cь�Cֳ3Cۙ�C���C�3C�� C�3C��fC�ffD�fD�3DٚDٚD�fD�3D�fD$��D)ٚD.��D3� D8��D=� DB�3DGٚDL� DQ��DVٚD[��D`�fDeٚDj� Do�fDt��DyٚD�&fD�c3D��fD��fD�#3D�c3D��fD�� D�0 D�` D���D��3D��D�ffDڦfD��fD�)�D�VfD�3D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��AAAA��AA�Q�A�|�A�O�A�E�AޮA��A�oA�/AѬAϟ�A���Aȉ7A�G�AîA��A��A�p�A�ȴA�O�A�1'A��A��7A�
=A��wA�A���A��A�;dA��yA�ZA�=qA��#A��A�hsA��A�dZA�1'A�`BA��yA�ƨA�z�A��`A�^5A�+A��uA��`A{�^At��AodZAk�Ac|�A\��AVbNANJAG?}AA�A4��A+��A&VA!p�A�-Ap�AO�A��A5?Az�@��@���@�;d@�7L@��@ǥ�@�
=@��P@� �@�Q�@�bN@�"�@��w@�Q�@�A�@�1'@���@��@��/@�I�@�j@��@y�@s�
@g�w@T1@M/@G+@B��@9%@/�;@)�#@$I�@"M�@�/@Q�@�/@;d@	��@v�@1@ A�?�ƨ?�+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 AAAA��AA�Q�A�|�A�O�A�E�AޮA��A�oA�/AѬAϟ�A���Aȉ7A�G�AîA��A��A�p�A�ȴA�O�A�1'A��A��7A�
=A��wA�A���A��A�;dA��yA�ZA�=qA��#A��A�hsA��A�dZA�1'A�`BA��yA�ƨA�z�A��`A�^5A�+A��uA��`A{�^At��AodZAk�Ac|�A\��AVbNANJAG?}AA�A4��A+��A&VA!p�A�-Ap�AO�A��A5?Az�@��@���@�;d@�7L@��@ǥ�@�
=@��P@� �@�Q�@�bN@�"�@��w@�Q�@�A�@�1'@���@��@��/@�I�@�j@��@y�@s�
@g�w@T1@M/@G+@B��@9%@/�;@)�#@$I�@"M�@�/@Q�@�/@;d@	��@v�@1@ A�?�ƨ?�+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	)�B	(�B	(�B	(�B	&�B	 �B	(�B	ZB
0!B
W
B
�+B
��B
�B+B�BB)�BC�BgmB�DB��B�B�!B�FB�RB�^B�-B�dB�B�?B�B��B��B�+B�B|�Bq�Bl�Be`B]/BI�B=qB1'B �BoB%B
�B
�fB
�B
��B
�B
�JB
gmB
O�B
7LB
oB	�B	��B	��B	�1B	iyB	2-B	JB��B�mB�B��BɺBÖB�^B�?B�9B��B��B��B��B�FB��B��B�B�B	1B	�B	5?B	M�B	hsB	�B	�uB	��B	��B	�B	�-B	�qB	��B	�;B	�`B	��B
B
�B
$�B
,B
.B
9XB
B�B
F�B
M�B
ZB
_;B
dZB
m�B
t�B
w�B
}�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	*B	)B	)B	)B	'2B	 �B	+�B	c'B
2�B
[B
��B
�GB
��B	B�B�B+�BE�Bi�B�B�B��B��B�#B��B�5B�aB��B��B��B�+B�B��B��B�5B~TBsfBnBf�B_XBJ�B>JB2aB!�BBoB
�yB
�B
��B
�B
��B
�B
h�B
P�B
9@B
8B	�HB	�B	��B	��B	l�B	4�B	�B�:B�B��BρB�6B�DB�7B�aB�`B��B�B�VB��B��BˠB�oB�'B�B	�B	�B	5�B	N-B	h�B	�KB	��B	��B	��B	�B	�}B	��B	�+B	�qB	�B	�B
=B
�B
%B
,1B
.0B
9sB
B�B
F�B
M�B
Z/B
_UB
dtB
m�B
t�B
w�B
~B
�B
�#1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;��
;��
;��
;��
;��
;��
;��
<�^;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201028562009072010285620090720102856  HZ  ARGQ                                                                        20081006091648  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20081006091648  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102856  IP                  G�O�G�O�G�O�                