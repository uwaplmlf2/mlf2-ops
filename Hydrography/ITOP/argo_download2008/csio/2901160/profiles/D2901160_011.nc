CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2901160 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20080916073016  20080916073016  0051_40948_011                  2C  D   APEX_SBE_3701                                                   846 @��ql�d 1   @��q��eC@3�   @a��@   1   ARGOS   A   A   A   @���A  Al��A���A�33A�ffBffB��B133BDffBY33Bl  B�  B�  B���B�ffB�  B���B���B�  B�  B�  B���BB�  CffC�CffCL�CffCffCL�C$� C)33C.33C3L�C8� C=ffCB�CG33CQ  C[ffCe� Co  CyffC�� C�� C��3C�� C���C��fC��fC�� C���C���C��3C���C���C CǙ�C̀ Cь�C֙�Cۀ C�L�C�fC�3C�� C�� C��fDٚD�3D�fD� D� D� D�3D$�fD)�3D.��D3� D8�fD=�3DB� DG�3DL� DQ��DV�3D[� D`� De� Dj�3Do� Dt�3Dy��D�&fD�l�D��3D�ٚD�)�D�p D��3D���D�)�D�i�D���D��3D�&fD�p Dڠ D��3D�)�D�` D�fD��D�9�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A  Al��A���A�33A�ffBffB��B133BDffBY33Bl  B�  B�  B���B�ffB�  B���B���B�  B�  B�  B���BB�  CffC�CffCL�CffCffCL�C$� C)33C.33C3L�C8� C=ffCB�CG33CQ  C[ffCe� Co  CyffC�� C�� C��3C�� C���C��fC��fC�� C���C���C��3C���C���C CǙ�C̀ Cь�C֙�Cۀ C�L�C�fC�3C�� C�� C��fDٚD�3D�fD� D� D� D�3D$�fD)�3D.��D3� D8�fD=�3DB� DG�3DL� DQ��DV�3D[� D`� De� Dj�3Do� Dt�3Dy��D�&fD�l�D��3D�ٚD�)�D�p D��3D���D�)�D�i�D���D��3D�&fD�p Dڠ D��3D�)�D�` D�fD��D�9�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�M�A�-A���A�9A� �A��A��A�VAѩ�A�oA�VAŶFA�1A�VA��A�p�A��A��A���A�K�A���A�JA���A�S�A���A�~�A�  A�I�A���A��PA��A�
=A��yA�~�A�|�A���A�+A��DA�p�A��;A��A�x�A��^A��A�jA���A��`A�=qA�5?A}��A{AxM�Aq�hAm`BAe�wA^�AZ=qAQ%AH �AA�#A=l�A:��A6��A0I�A*�RA%%A�^A;dA��A�#AS�@� �@�%@�~�@�A�@��`@��@�=q@�?}@�`B@�&�@��P@��w@�hs@�
=@���@��-@�G�@���@��@|Z@xbN@p��@m��@o�w@f�y@^�R@W�@P�`@B�!@=`B@8r�@2n�@)hs@v�@�@@��@
=@��@
=q@�@�T@��@@11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�M�A�-A���A�9A� �A��A��A�VAѩ�A�oA�VAŶFA�1A�VA��A�p�A��A��A���A�K�A���A�JA���A�S�A���A�~�A�  A�I�A���A��PA��A�
=A��yA�~�A�|�A���A�+A��DA�p�A��;A��A�x�A��^A��A�jA���A��`A�=qA�5?A}��A{AxM�Aq�hAm`BAe�wA^�AZ=qAQ%AH �AA�#A=l�A:��A6��A0I�A*�RA%%A�^A;dA��A�#AS�@� �@�%@�~�@�A�@��`@��@�=q@�?}@�`B@�&�@��P@��w@�hs@�
=@���@��-@�G�@���@��@|Z@xbN@p��@m��@o�w@f�y@^�R@W�@P�`@B�!@=`B@8r�@2n�@)hs@v�@�@@��@
=@��@
=q@�@�T@��@@11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
9XB
E�B
P�B
m�B
�BhBD�BO�B�B�oB��B�'B�?B�9B�3B�B�VB�PB��B��B��B�uB��B�bB��B�B��B��B�\B�PBr�Be`BYBS�BJ�BF�BF�BD�BE�BB�B0!B%�BuB%B
��B
��B
�B
�B
�jB
��B
�{B
�B
`BB
I�B
"�B
  B	�yB	�qB	�oB	w�B	e`B	XB	E�B	,B	bB��B�mB��B�jB�B�JB�Bx�Bo�Bu�B�1B��B�'B��B��B	JB	�B	)�B	Q�B	aHB	jB	{�B	�=B	�oB	��B	��B	ǮB	ȴB	�;B	�B
B
%B
�B
&�B
33B
7LB
>wB
D�B
L�B
XB
^5B
dZB
iyB
m�B
q�B
s�B
u�B
y�B
{�B
}�B
}�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
9|B
FB
R�B
rB
��B�BFBT�B��B��B��B�TB��B�]B��B��B�-B�dB��B�B��B�zB��B��B�B��B��B�/B�WB��Bs�BfhBY�BT�BK�BGBGOBE�BF7BDB0�B'5BOB�B
�*B
�/B
�UB
ۯB
�B
�/B
�]B
��B
aaB
K�B
$�B
&B	��B	��B	�B	y
B	fB	Y8B	GcB	-�B	�B�UB�BӽB��B�JB��B�jBy�Bp�Bv�B��B��B�-BЀB�B	wB	�B	*�B	R$B	azB	j�B	|EB	�fB	��B	��B	�+B	�B	��B	�0B	�B
2B
IB
�B
'%B
3MB
7dB
>�B
D�B
L�B
X&B
^IB
doB
i�B
m�B
q�B
s�B
u�B
y�B
{�B
}�B
}�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;��
;��
;��
;��;�~�;�N�;��
;�8a;��
;�;��
;��
;��
;��
;��
;�TO;��
;��o;��
;��
;���;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201028552009072010285520090720102855  HZ  ARGQ                                                                        20080916073016  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080916073016  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102855  IP                  G�O�G�O�G�O�                