CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   q   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4`   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =@   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A<   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C    PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Ct   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E8   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    Eh   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Hh   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Kh   CALIBRATION_DATE            	             
_FillValue                  ,  Nh   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         O   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         O   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    OArgo profile    2.2 1.2 19500101000000  2901159 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20081015101945  20081015101945  0050_80056_014                  2C  D   APEX_SBE_3700                                                   846 @���0�d1   @���m˩�@5gl�   @a,`   1   ARGOS   A   A   A   @�ffAffAh  A���A���A���B	��B  B1��BF��BZ  Bn��B�  B�ffB���B���B�ffB�ffB�33B�  B���B���B���B�  B�  CL�CffC�C�fC  CL�CffC$ffC)�3C.� C3ffC8L�C=L�CB33CG33CQffC[� CeL�Co  Cy�C�� C���C���C���C��3C�� C���C�� C���C�� C��fC��fC�� C CǦfC̙�C֦fC���C�fC�� C�� C��fC���D�fD�fD��D�3D��DٚD�fD$�3D)�3D.� D3ٚD8��D=ٚDB��DG� DL�3DQ� DV��D[�fD`�3De�3DjٚDo��Dt� Dy��D�#3D�s3D��fD��D�#3D�p D���D�� D�0 D�c3D��fD��D�,�D�l�Dڰ D���D�  D�Y�D� D�vf11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�ffAffAh  A���A���A���B	��B  B1��BF��BZ  Bn��B�  B�ffB���B���B�ffB�ffB�33B�  B���B���B���B�  B�  CL�CffC�C�fC  CL�CffC$ffC)�3C.� C3ffC8L�C=L�CB33CG33CQffC[� CeL�Co  Cy�C�� C���C���C���C��3C�� C���C�� C���C�� C��fC��fC�� C CǦfC̙�C֦fC���C�fC�� C�� C��fC���D�fD�fD��D�3D��DٚD�fD$�3D)�3D.� D3ٚD8��D=ٚDB��DG� DL�3DQ� DV��D[�fD`�3De�3DjٚDo��Dt� Dy��D�#3D�s3D��fD��D�#3D�p D���D�� D�0 D�c3D��fD��D�,�D�l�Dڰ D���D�  D�Y�D� D�vf11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�A�jA��HA�S�A�VA�hsA�l�A�O�A���A�C�A��TA�XA�1A��A���A��A�XA�VA�=qA��A��A��wA�v�A��A���A�ȴA��mA��A��A�x�A��A��/A��\A�ĜA���A��#A��HA�r�A���A�S�A��7A��A�A��!A���A�A��9A�?}A���A���A~�AwAs�An(�Ajr�Ac��A\bNAW33ARbAN �AJ1A>��A77LA2�HA.A)�TA%�^A!�mA  A�HA��A �+@���@�+@ڗ�@Ѓ@ǶF@��w@��@�l�@�1@��9@��@��!@�;d@�/@���@��h@��F@~��@yx�@tI�@n5?@`r�@V�y@M�@G�@>{@5�@.ȴ@*�@$�@!�@v�@o@E�@��@�@9X@�@ȴ@@ �u11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�A�jA��HA�S�A�VA�hsA�l�A�O�A���A�C�A��TA�XA�1A��A���A��A�XA�VA�=qA��A��A��wA�v�A��A���A�ȴA��mA��A��A�x�A��A��/A��\A�ĜA���A��#A��HA�r�A���A�S�A��7A��A�A��!A���A�A��9A�?}A���A���A~�AwAs�An(�Ajr�Ac��A\bNAW33ARbAN �AJ1A>��A77LA2�HA.A)�TA%�^A!�mA  A�HA��A �+@���@�+@ڗ�@Ѓ@ǶF@��w@��@�l�@�1@��9@��@��!@�;d@�/@���@��h@��F@~��@yx�@tI�@n5?@`r�@V�y@M�@G�@>{@5�@.ȴ@*�@$�@!�@v�@o@E�@��@�@9X@�@ȴ@@ �u11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�JB	�hB	�B
~�BL�B=qB�BB?}BffBP�BW
BgmBiyBiyB�bBx�BbNBgmBhsBl�BhsBm�Bk�Bo�Bl�BgmBcTBaHBaHBZBS�BS�BM�BH�BG�BB�B=qB:^B7LB1'B'�B�B{B
=B
��B
�B
�/B
��B
�'B
��B
�B
l�B
H�B
5?B
oB	�B	�B	ĜB	�-B	��B	e`B	B�B	1'B	�B	hB	B�B��B�XB��B��B�bB�7B�uB��B��B�-B�B�B	+B	!�B	0!B	E�B	[#B	aHB	m�B	o�B	jB	t�B	~�B	�=B	��B	�'B	ÖB	�;B	�B	��B

=B
�B
,B
<jB
@�B
H�B
O�B
W
B
_;B
gmB
k�B
t�B
z�B
}�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	�JB	�UB	��B
dBP�B>�B"�B�BB�Bp�BYxBYLBk&Bi�Bk�B�B}�Be�Bh�Bj�Bl�Bj�Bn�Bl�BqdBm_Bh{BdBb�Ba�B[%BU0BT�BN�BI�BH�BC�B=�B;B8(B1�B(�B^BB	B
�B
�@B
��B
�WB
��B
�cB
�3B
m�B
I�B
7B
NB	� B	�vB	ŤB	�JB	�-B	fsB	C�B	2{B	�B	�B	 B�dB�<B�AB��B�rB��B�'B�=B�NB��B�B؉B�B	�B	"B	0WB	E�B	[UB	a�B	m�B	pB	j�B	t�B	5B	��B	��B	�TB	��B	�[B	�B	�B

_B
�B
,$B
<wB
@�B
H�B
O�B
WB
_MB
g|B
k�B
t�B
z�B
~B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;��
;��
;��
;��
;��
;��
;��9;��
;��
<#l<
*_;��
;��
;��
;��
;��
;�mj;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201028352009072010283520090720102835  HZ  ARGQ                                                                        20081015101945  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20081015101945  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102835  IP                  G�O�G�O�G�O�                