CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2901159 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20080806091633  20080806091633  0050_80056_007                  2C  D   APEX_SBE_3700                                                   846 @��0��1   @��0Ɗ�@4��    @a\�`   1   ARGOS   A   A   A   @���AffAi��A�ffA�33A���B	��B��B2  BFffBZffBl��B�33B�  B���B�33B�ffB���B���Bƙ�B�  Bڙ�B�33B�33B�ffCffCL�CL�C� CL�CL�C� C$� C)33C.33C333C8  C<�fCA�3CGffCQ� C[L�Ce33CoL�Cy�C��3C�� C�s3C�� C���C�� C���C���C��fC�� C���C���C�� C�Cǳ3C�� C�s3C֦fCۙ�C���C�3C�3C�fC�� C��3D� D�fD��DٚD� D�3D�3D$�fD)��D.ٚD3�3D8�fD=�3DB�3DG� DL� DQ��DVٚD[� D`� DeٚDj� Do��Dt�fDy��D�,�D�p D���D���D�fD�` D���D�� D��D�c3D�� D��3D��D�l�Dڬ�D���D�&fD�c3D�D��fD� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���AffAi��A�ffA�33A���B	��B��B2  BFffBZffBl��B�33B�  B���B�33B�ffB���B���Bƙ�B�  Bڙ�B�33B�33B�ffCffCL�CL�C� CL�CL�C� C$� C)33C.33C333C8  C<�fCA�3CGffCQ� C[L�Ce33CoL�Cy�C��3C�� C�s3C�� C���C�� C���C���C��fC�� C���C���C�� C�Cǳ3C�� C�s3C֦fCۙ�C���C�3C�3C�fC�� C��3D� D�fD��DٚD� D�3D�3D$�fD)��D.ٚD3�3D8�fD=�3DB�3DG� DL� DQ��DVٚD[� D`� DeٚDj� Do��Dt�fDy��D�,�D�p D���D���D�fD�` D���D�� D��D�c3D�� D��3D��D�l�Dڬ�D���D�&fD�c3D�D��fD� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A�-A��A�t�A�=qA�O�A���A��A��TAĩ�A�&�A�VA�bNA�-A��wA��A��9A�S�A�dZA�JA�-A��mA�r�A���A��;A��TA��A�$�A�"�A�1A���A�;dA��A���A�^5A�1A�VA��
A�?}A��yA���A�A��mA�ƨA� �A��A�A�ƨA���A�(�A~{Av�uAp��Am��Aj�AeƨA`��A\n�AX�yAN�AFr�AA;dA;|�A:�A6�\A0~�A*-A&��A�AƨA=qA|�A�^A ��@�A�@�%@˾w@�@���@�p�@���@�V@��@��@���@�hs@��j@��@���@�I�@�+@�^5@l�@v�@i�@\�@S"�@K"�@C�m@<�@49X@/�;@+33@&@"-@�@o@�@��@�@�@
�@
=@Z@7L@%11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A�-A��A�t�A�=qA�O�A���A��A��TAĩ�A�&�A�VA�bNA�-A��wA��A��9A�S�A�dZA�JA�-A��mA�r�A���A��;A��TA��A�$�A�"�A�1A���A�;dA��A���A�^5A�1A�VA��
A�?}A��yA���A�A��mA�ƨA� �A��A�A�ƨA���A�(�A~{Av�uAp��Am��Aj�AeƨA`��A\n�AX�yAN�AFr�AA;dA;|�A:�A6�\A0~�A*-A&��A�AƨA=qA|�A�^A ��@�A�@�%@˾w@�@���@�p�@���@�V@��@��@���@�hs@��j@��@���@�I�@�+@�^5@l�@v�@i�@\�@S"�@K"�@C�m@<�@49X@/�;@+33@&@"-@�@o@�@��@�@�@
�@
=@Z@7L@%11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�)B	�#B	�#B	�B	��B
-B
VB
�oB
�B
ƨB
ǮB
ŢB
��B
�B�B\)B[#Br�Bw�Bx�B�B�B�B�Bu�Bp�BhsBo�B�{B��B��B�7B� Bz�Bq�BiyBe`BaHB\)BQ�B@�B6FB"�B�B
��B
�B
�mB
�5B
��B
�?B
��B
y�B
bNB
O�B
@�B
'�B
\B	��B	�mB	�RB	�{B	v�B	^5B	W
B	I�B	+B	uB	%B�B�;BɺB�dB�B��B�wB�!B��B��B�?B�wB�5B�mB	%B	�B	8RB	N�B	`BB	m�B	r�B	y�B	�7B	��B	�B	�?B	�RB	ƨB	��B	�`B	��B
1B
{B
�B
+B
9XB
C�B
K�B
T�B
`BB
cTB
jB
p�B
t�B
y�B
}�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	�.B	�9B	�yB	�MB	�nB
1�B
Z9B
�0B
��B
�EB
�0B
ƇB
��B
��B�B]�B]CBtaBx3BzvB�EB��B�=B��Bv�Bq�BjBp�B��B��B�B��B�EB|Br�Bj*Be�Ba�B]HBS
BAkB7�B#�B�B
��B
�B
��B
�B
�ZB
�YB
��B
{IB
cB
P�B
A�B
)0B
�B	��B	��B	��B	��B	xFB	^�B	X	B	KXB	,�B	dB	�B�3B�WBʷB��B��B�B��B�mB�<B��B��B��B�sB��B	�B	B	8�B	O<B	`�B	m�B	r�B	zDB	��B	��B	�iB	��B	��B	��B	�%B	�B	� B
UB
�B
�B
+B
9lB
C�B
K�B
UB
`UB
c_B
j�B
p�B
t�B
y�B
~B
�!B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;��
;��
;��
;��
<�v�;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201028322009072010283220090720102832  HZ  ARGQ                                                                        20080806091633  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080806091633  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102832  IP                  G�O�G�O�G�O�                