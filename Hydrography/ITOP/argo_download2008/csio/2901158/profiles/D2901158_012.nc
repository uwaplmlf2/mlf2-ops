CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2901158 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20080924091635  20080924091635  0049_80055_012                  2C  D   APEX_SBE_3699                                                   846 @��W;1   @��9��~@5 �    @`�\    1   ARGOS   A   A   A   @�ffAffAh  A�  A�  A���B
ffB��B2  BE��BZ��Bn  B�33B�ffB���B�33B�33B���B�33B�33B���B���B�ffB���B���C  C� C��CffCffC33C�C$  C)�C.ffC333C8  C<��CB33CF�fCP��CZ�fCe�Cn��CyffC�� C��3C��fC���C�� C�s3C�� C���C��3C�ffC�� C��3C��3C�� CǙ�Č�Cљ�C�s3C�s3C�s3C�ffC�3C�fC�� C��3D�3DٚD�fDٚD�3D�fD�3D$� D)��D.�fD3�3D8��D=�3DB�3DGٚDL�3DQ�3DV�3D[� D`�fDe��Dj� Do�3DtٚDyٚD�,�D�p D��fD���D�fD�p D���D��3D�)�D�c3D���D���D�,�D�l�DڦfD��3D��D�` D��D��3D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffAffAh  A�  A�  A���B
ffB��B2  BE��BZ��Bn  B�33B�ffB���B�33B�33B���B�33B�33B���B���B�ffB���B���C  C� C��CffCffC33C�C$  C)�C.ffC333C8  C<��CB33CF�fCP��CZ�fCe�Cn��CyffC�� C��3C��fC���C�� C�s3C�� C���C��3C�ffC�� C��3C��3C�� CǙ�Č�Cљ�C�s3C�s3C�s3C�ffC�3C�fC�� C��3D�3DٚD�fDٚD�3D�fD�3D$� D)��D.�fD3�3D8��D=�3DB�3DGٚDL�3DQ�3DV�3D[� D`�fDe��Dj� Do�3DtٚDyٚD�,�D�p D��fD���D�fD�p D���D��3D�)�D�c3D���D���D�,�D�l�DڦfD��3D��D�` D��D��3D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��`A��A�A���A��A��TA��yA��A�A�A�"�Aן�A�dZA�jAöFA�ƨA���A���A��7A���A���A�S�A���A���A�A��A�r�A�G�A�\)A��yA�;dA�bA�|�A��mA���A���A���A�O�A�p�A��+A���A�33A� �A�&�A��TA�ĜA��\A�VA��yA�VA�1A�9XA�Azv�AtI�AohsAj=qAa��AZ�\AQ�AL�+AFM�A@�A;��A6��A0��A(�A$�DA �A�A?}A~�AK�@��F@���@�^5@�+@ѩ�@��T@�9X@��m@�
=@�`B@��@��w@�33@��@�~�@�~�@��@�I�@�J@�(�@|z�@w��@t�@h�@W�w@O+@H �@@1'@8�u@4�j@-O�@)��@&$�@"n�@�T@&�@5?@��@�@��@A�@�/@@�H11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��`A��A�A���A��A��TA��yA��A�A�A�"�Aן�A�dZA�jAöFA�ƨA���A���A��7A���A���A�S�A���A���A�A��A�r�A�G�A�\)A��yA�;dA�bA�|�A��mA���A���A���A�O�A�p�A��+A���A�33A� �A�&�A��TA�ĜA��\A�VA��yA�VA�1A�9XA�Azv�AtI�AohsAj=qAa��AZ�\AQ�AL�+AFM�A@�A;��A6��A0��A(�A$�DA �A�A?}A~�AK�@��F@���@�^5@�+@ѩ�@��T@�9X@��m@�
=@�`B@��@��w@�33@��@�~�@�~�@��@�I�@�J@�(�@|z�@w��@t�@h�@W�w@O+@H �@@1'@8�u@4�j@-O�@)��@&$�@"n�@�T@&�@5?@��@�@��@A�@�/@@�H11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�B	�B	�B	�B	�B	�B	D�B	��B
+B
  B	��B
hB
.B
@�B
w�B
��B
�B
ɺB
�
B
�BBVBhB�B�B'�B,BI�BO�BP�Bl�B~�B}�Bz�Bu�Br�Bn�Bk�BgmBdZBS�BG�B5?B,B!�B�BB
�B
�NB
��B
�wB
��B
�DB
m�B
T�B
;dB
uB	�B	��B	�B	�oB	|�B	hsB	O�B	5?B	�B	
=B�B�5B��BÖB�dB�-B�9B�!B�B�B�B�B�9BǮB�B�HB��B	B	�B	%�B	E�B	^5B	w�B	�B	�\B	��B	��B	��B	�jB	�B	�B
B
JB
�B
 �B
2-B
?}B
H�B
N�B
VB
^5B
cTB
hsB
k�B
o�B
t�B
y�B
}�B
~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	�B	�B	�B	�B	�B	�B	G�B	��B
	�B
�B
 �B
�B
7�B
FB
zUB
��B
�B
�2B
ڥB
�
B�BB*B�B�B(�B-�BKBP�BRBm(B�vB~�B{�Bv�Bs9BowBlTBh)Be�BT�BH�B6aB-B"cB�BOB
�B
�SB
��B
��B
�UB
��B
n�B
VJB
=�B
JB	��B	�:B	��B	��B	~RB	i�B	Q�B	7|B	�B	B��B�rB�7BĥB�:B��B�B��B�&B��B��B�qB�<B�&B�aB��B�'B	�B	�B	&�B	E�B	^:B	xB	�?B	��B	��B	��B	�B	��B	�-B	��B
7B
mB
�B
 �B
2?B
?�B
H�B
N�B
VB
^EB
cfB
h�B
k�B
o�B
t�B
y�B
}�B
~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;��
;��
;��
;��
;��
;��
;��
;��;��
;��
;��
;��
<�9;��X;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201028132009072010281320090720102813  HZ  ARGQ                                                                        20080924091635  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080924091635  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102813  IP                  G�O�G�O�G�O�                