CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   p   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4\   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C<   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E,   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H,   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K,   CALIBRATION_DATE            	             
_FillValue                  ,  N,   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    NX   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N\   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N`   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Nd   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Nh   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  2901158 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20080815093532  20080815093532  0049_80055_008                  2C  D   APEX_SBE_3699                                                   846 @��7��@1   @��m��@7W�@   @`lĠ   1   ARGOS   A   A   A   @���A33Ah  A���A�  A���B	��B��B2  BE33BX��Bk33B�33B���B�  B�  B�33B���B�ffB�  B���B�ffB䙚B���B�  C� C�3C��C��C� C� C33C$ffC)  C.ffC3  C8� C=ffCB33CG� CQ��C[�Ce33CoffCy�C���C���C���C�� C�� C�s3C��fC��fC��3C�� C���C��fC���C�C�� C̦fCљ�C֦fCۦfC�� C�fC�s3C�3C��fC���D� D� D�fD� D� D�3D�3D$��D)�fD.�fD3�fD8��D=��DBٚDG�fDL� D`��DeٚDj�3Do��Dt�3DyٚD�  D�p D���D��fD�&fD�ffD���D��fD��D�l�D��3D���D�&fD�i�DڦfD���D�  D�ffD�D�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A33Ah  A���A�  A���B	��B��B2  BE33BX��Bk33B�33B���B�  B�  B�33B���B�ffB�  B���B�ffB䙚B���B�  C� C�3C��C��C� C� C33C$ffC)  C.ffC3  C8� C=ffCB33CG� CQ��C[�Ce33CoffCy�C���C���C���C�� C�� C�s3C��fC��fC��3C�� C���C��fC���C�C�� C̦fCљ�C֦fCۦfC�� C�fC�s3C�3C��fC���D� D� D�fD� D� D�3D�3D$��D)�fD.�fD3�fD8��D=��DBٚDG�fDL� D`��DeٚDj�3Do��Dt�3DyٚD�  D�p D���D��fD�&fD�ffD���D��fD��D�l�D��3D���D�&fD�i�DڦfD���D�  D�ffD�D�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�bNA�`BA�ZA��A�x�A�O�A�ƨA�JA�l�AܑhAھwA�=qA�{A�33AӬA�A�A�O�A� �A��A�x�A��A���A�"�A®A���A��A�5?A�bA�t�A��DA��/A�
=A��hA��A���A�C�A��DA��A��A��^A���A�dZA�33A���A���A�K�A���A�Q�A��/A��FA�%A��RA�VA���A��A�ffA��^A�ȴA~��AyƨAs�hAlM�Afn�A^ffAX  AQ&�AL�AEK�AB=qA?dZA5"�A,��A$bA�A��A�9A�@���@�M�@@��m@؛�@�n�@�^5@���@�
=@�$�@� �@�9X@�;d@��@��@��@q�7@b�H@X�@NE�@I%@CdZ@=�@6$�@-O�@&��@ Ĝ@�j@;d@�m@\)@t�@��@�@^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�bNA�`BA�ZA��A�x�A�O�A�ƨA�JA�l�AܑhAھwA�=qA�{A�33AӬA�A�A�O�A� �A��A�x�A��A���A�"�A®A���A��A�5?A�bA�t�A��DA��/A�
=A��hA��A���A�C�A��DA��A��A��^A���A�dZA�33A���A���A�K�A���A�Q�A��/A��FA�%A��RA�VA���A��A�ffA��^A�ȴA~��AyƨAs�hAlM�Afn�A^ffAX  AQ&�AL�AEK�AB=qA?dZA5"�A,��A$bA�A��A�9A�@���@�M�@@��m@؛�@�n�@�^5@���@�
=@�$�@� �@�9X@�;d@��@��@��@q�7@b�H@X�@NE�@I%@CdZ@=�@6$�@-O�@&��@ Ĝ@�j@;d@�m@\)@t�@��@�@^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�7B�7B�7B�7B��B��B��B	x�B	�
B
B
/B
>wB
=qB
S�B
ZB
O�B
?}B
-B
G�B
\)B
�hB
�B%B\B�BP�B��B��B��B��B�BB�;B�BB�TB�mB�HB�B�5BƨB�RB��B��B� BhsBbNBXBI�B?}B49B,B!�B�B
=B
��B
�;B
ɺB
�?B
�B
��B
�B
dZB
?}B
�B	��B	�B	�}B	�B	�PB	~�B	l�B	?}B	�B	B�B�#B��B�wB�RB�3B�-B�9B�B��B�dB��B�
B	PB	�B	+B	7LB	A�B	Q�B	{�B	��B	�?B	��B	�5B	�yB	��B
B
{B
 �B
+B
5?B
B�B
K�B
R�B
YB
`BB
iyB
o�B
t�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B�9B�?B��B�dB��B�fB�HB	{B	خB
�B
0rB
>�B
@B
VB
\5B
Q�B
BB
.B
H.B
]�B
�1B
�qBeBMBQBQ�B�wB�+B˰B�'B� B��B�MB� B��B�BڬB��B��B�<B��B��B��BiJBcBY�BJ�B@�B4�B,�B"iB7BB
�.B
�B
�B
��B
��B
��B
��B
f8B
@�B
!�B	�zB	��B	��B	��B	�*B	�B	m�B	@�B	 �B	B�B�nB�B�EB�EB��B� B�B��B��B�B�)BןB	�B	B	+mB	7�B	A�B	R.B	|,B	��B	�oB	��B	�QB	�B	��B
@B
�B
 �B
+B
5TB
B�B
K�B
SB
Y+B
`QB
i�B
o�B
t�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201028112009072010281120090720102811  HZ  ARGQ                                                                        20080815093532  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080815093532  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102811  IP                  G�O�G�O�G�O�                