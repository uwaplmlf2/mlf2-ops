CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2901158 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20081014091743  20081014091743  0049_80055_014                  2C  D   APEX_SBE_3699                                                   846 @��	�Y�1   @��@E�@5���   @`�v�   1   ARGOS   A   A   A   @���A  AfffA���A�ffA�  B
ffB��B3��BF  BZ��Bn��B�  B�ffB�ffB�  B�ffB�  B���B�  B�  B���B�ffB���B�ffCffC�CffCL�C33CL�C33C$� C)L�C.33C3ffC8L�C=� CBL�CGffCP�fC[� Ce� Co33Cy�C���C���C�� C��3C�s3C���C���C�� C�ffC��fC��fC���C���C¦fCǙ�C̙�CѦfC֦fCۙ�C�3C�3C��C�fC��fC�� D��D� D��D�3DٚD� D� D$� D)�3D.� D3� D8�3D=�3DBٚDG�fDL��DQ� DV�fD[�fD`��De�3DjٚDo� Dt� DyٚD��D�s3D��3D��3D��D�Y�D���D���D�  D�VfD��3D�� D��D�i�Dڠ D��D�)�D�c3D�fD��D��311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A  AfffA���A�ffA�  B
ffB��B3��BF  BZ��Bn��B�  B�ffB�ffB�  B�ffB�  B���B�  B�  B���B�ffB���B�ffCffC�CffCL�C33CL�C33C$� C)L�C.33C3ffC8L�C=� CBL�CGffCP�fC[� Ce� Co33Cy�C���C���C�� C��3C�s3C���C���C�� C�ffC��fC��fC���C���C¦fCǙ�C̙�CѦfC֦fCۙ�C�3C�3C��C�fC��fC�� D��D� D��D�3DٚD� D� D$� D)�3D.� D3� D8�3D=�3DBٚDG�fDL��DQ� DV�fD[�fD`��De�3DjٚDo� Dt� DyٚD��D�s3D��3D��3D��D�Y�D���D���D�  D�VfD��3D�� D��D�i�Dڠ D��D�)�D�c3D�fD��D��311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��AꙚA�uA�7A�bNA���A�wA�ƨA�~�A�5?Aڰ!Aҕ�A�1'A�jA�O�A�"�A���A��7A�jA��A�33A��!A��A���A�bNA���A�VA���A��FA�p�A��TA��-A��A�
=A���A��`A�A�A��A�1'A�"�A�/A��-A�p�A�M�A��HA�A�?}A��A���A���A���A�Az�Av�RAr(�Ao;dAj=qAc�A^��A[��AT�!AK+AC+A=�A8 �A1VA.A(��A#\)A�TA�^A�FA��A|�@���@���@��;@��;@�  @ʰ!@�?}@�Ĝ@� �@���@���@�-@�{@���@�C�@�ff@���@�dZ@���@��@\)@z�\@k�
@\�j@RJ@K33@Co@<9X@5�h@.��@)G�@#�@;d@��@��@�@\)@��@
�!@ �@O�@=q@-11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111AꙚA�uA�7A�bNA���A�wA�ƨA�~�A�5?Aڰ!Aҕ�A�1'A�jA�O�A�"�A���A��7A�jA��A�33A��!A��A���A�bNA���A�VA���A��FA�p�A��TA��-A��A�
=A���A��`A�A�A��A�1'A�"�A�/A��-A�p�A�M�A��HA�A�?}A��A���A���A���A�Az�Av�RAr(�Ao;dAj=qAc�A^��A[��AT�!AK+AC+A=�A8 �A1VA.A(��A#\)A�TA�^A�FA��A|�@���@���@��;@��;@�  @ʰ!@�?}@�Ĝ@� �@���@���@�-@�{@���@�C�@�ff@���@�dZ@���@��@\)@z�\@k�
@\�j@RJ@K33@Co@<9X@5�h@.��@)G�@#�@;d@��@��@�@\)@��@
�!@ �@O�@=q@-11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�BB�5B�5B�)B��B��B�^B��B	1B	gmB	��B
B
bB
!�B
F�B
�B
��B
�FB
��B
�HB
�B
��B+B�B7LB9XB[#B_;Be`B��B�hB�+B}�B�B|�Bv�Bk�BffBbNBZBL�BF�B,B�B\BB
��B
�B
�5B
��B
��B
�\B
x�B
cTB
Q�B
9XB
�B
+B	��B	��B	��B	� B	aHB	K�B	0!B	#�B	oB��B�B�HB�
B��B�3B�!B��B��B��B��B��B��B�wBĜB�B�B	  B	{B	(�B	49B	D�B	dZB	r�B	�B	��B	��B	�B	ÖB	��B	�TB	�B
%B
uB
$�B
1'B
;dB
D�B
M�B
ZB
aHB
gmB
jB
p�B
s�B
v�B
z�B
~�B
~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B�]B�UB�pBܓB�QB�}B��B� B	1B	m�B	զB
�B
6B
$�B
J�B
�@B
��B
�eB
ϨB
�B
�JB
��B	)B�B8B:tB]B`kBe�B�{B��B�'B�0B��B~{Bx�Bl8BgtBc7B[aBM`BH-B-?B�B9B�B
��B
�B
ߴB
��B
�-B
�sB
zB
dB
S5B
;B
�B
�B	��B	�XB	��B	��B	b�B	M�B	0�B	%JB	�B�~B��B�)B�B�B��B��B�"B��B�HB�LB�B��B�%B�WB��B��B	 [B	�B	)/B	4�B	EB	d�B	r�B	�LB	��B	��B	�JB	��B	�)B	�vB	��B
FB
�B
$�B
1BB
;�B
D�B
M�B
Z1B
a[B
g{B
j�B
p�B
s�B
v�B
z�B
 B
~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;��
;��
;��
;��
;��
;��
;��
;��
;�Ć;�p;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201028142009072010281420090720102814  HZ  ARGQ                                                                        20081014091743  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20081014091743  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102814  IP                  G�O�G�O�G�O�                