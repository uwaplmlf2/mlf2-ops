CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   n   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4T   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6|   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :\   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J�   CALIBRATION_DATE            	             
_FillValue                  ,  M�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N    HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N`   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Np   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Nt   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  2901155 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20081104230219  20081104230219  0046_80052_012                  2C  D   APEX_SBE_3696                                                   846 @��&;�0*1   @��&mj�e@8S3@   @_��    1   ARGOS   A   A   A   A   A�  A���A�33B	��B  B133BF  BZ  Bm��B�  B���B���B���B�33B�ffB�33B�  B�ffB�  B�33B�ffB�  C� CL�C�CL�C�C33CL�C$�C)  C.ffC3L�C8� C=ffCBL�CG� CQffC[�Ce� Cn��CyffC��fC��fC��3C��fC�� C�s3C��3C��3C���C���C�� C��fC�� C�CǙ�Č�Cь�C�s3C�s3C�s3C�3C�fCC��3C��fD� D�3D� D��D� D� D�3D$�3D)��D.�fD3�fD8� D=� DB� DG�3DL� DQ��DV�3D[�fD`� De� Dj� DoٚDt� Dy��D�)�D�i�D�� D��3D�#3D�i�D�� D��3D�,�D�i�D���D��3D�#3D�l�Dڰ D�Y�D�f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A   A�  A���A�33B	��B  B133BF  BZ  Bm��B�  B���B���B���B�33B�ffB�33B�  B�ffB�  B�33B�ffB�  C� CL�C�CL�C�C33CL�C$�C)  C.ffC3L�C8� C=ffCBL�CG� CQffC[�Ce� Cn��CyffC��fC��fC��3C��fC�� C�s3C��3C��3C���C���C�� C��fC�� C�CǙ�Č�Cь�C�s3C�s3C�s3C�3C�fCC��3C��fD� D�3D� D��D� D� D�3D$�3D)��D.�fD3�fD8� D=� DB� DG�3DL� DQ��DV�3D[�fD`� De� Dj� DoٚDt� Dy��D�)�D�i�D�� D��3D�#3D�i�D�� D��3D�,�D�i�D���D��3D�#3D�l�Dڰ D�Y�D�f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�VA�oA���A���Aޛ�A�S�AݾwA�"�A�%Aۙ�AہA�hsA��A�ĜAՁAӺ^A��A��mA���AɓuAǁAĸRA�=qA���A��HA���A�&�A�/A�  A�ĜA���A�  A��A��A��PA�p�A��A���A�K�A�jA� �A�5?A��HA�+A��DA�z�A�1'A�Q�A�(�A���A��A���A�A���AyƨAu�7Ao��Ak&�Aep�AaA\��AX�+ARZAO+AH�AE�FA=�A9�A5�A//A(=qA%A%A�AbN@�b@�V@��/@��y@ͺ^@�=q@� �@�l�@�/@�hs@��@��H@���@� �@���@�(�@� �@��@\)@nff@c��@W;d@K�@?��@8�9@333@.��@&ȴ@#@�@I�@��@~�@�@/11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�VA�oA���A���Aޛ�A�S�AݾwA�"�A�%Aۙ�AہA�hsA��A�ĜAՁAӺ^A��A��mA���AɓuAǁAĸRA�=qA���A��HA���A�&�A�/A�  A�ĜA���A�  A��A��A��PA�p�A��A���A�K�A�jA� �A�5?A��HA�+A��DA�z�A�1'A�Q�A�(�A���A��A���A�A���AyƨAu�7Ao��Ak&�Aep�AaA\��AX�+ARZAO+AH�AE�FA=�A9�A5�A//A(=qA%A%A�AbN@�b@�V@��/@��y@ͺ^@�=q@� �@�l�@�/@�hs@��@��H@���@� �@���@�(�@� �@��@\)@nff@c��@W;d@K�@?��@8�9@333@.��@&ȴ@#@�@I�@��@~�@�@/11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�#B	�B�BǮB�B�yB��B��B	1B	DB	DB	VB	"�B	0!B	�TB
VB
P�B
x�B
�DB
�bB
�#B(�B	7BBBG�Bn�Bm�BXBt�Bv�BW
BYBjBZB^5BjB�B�DB�Bx�BcTBgmBaHBW
BD�B8RB1'B �BhB
��B
�#B
�wB
��B
�B
p�B
R�B
8RB
�B

=B	�B	�NB	ĜB	�9B	��B	�+B	ffB	VB	C�B	+B	uB�B�B��B�jB�'B�B��B�3BB�B	  B	2-B	I�B	49B	#�B	�B	�B	/B	D�B	t�B	z�B	�%B	��B	�B	�RB	��B	�fB	��B
B
�B
�B
$�B
)�B
49B
@�B
I�B
R�B
hsB
m�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B�8B	�B�XB�IB�qB�B�YB��B	�B	nB	mB	�B	#RB	4�B	�B
CB
T�B
z�B
��B
�4B
ݹB*4B
zBBBHBo�Bo}BY�Bu�By7BYdBYNBk�B[6B`BlAB��B��B�"BzSBc�BhABb�BX�BF7B8�B2�B!�BxB
�eB
ܟB
��B
��B
�6B
rB
T"B
9�B
�B
�B	��B	��B	�nB	��B	��B	�4B	g�B	V�B	DwB	,B	B�B��B�9B��B�0B�B�cB��B�)B�B	 B	2dB	J�B	5)B	$VB	�B	�B	/�B	D�B	uB	{'B	�vB	��B	�3B	��B	�B	�B	��B
;B
�B
�B
$�B
*B
4HB
@�B
I�B
SB
h}B
m�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907200316432009072003164320090720031643  HZ  ARGQ                                                                        20081104230219  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20081104230219  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720031643  IP                  G�O�G�O�G�O�                