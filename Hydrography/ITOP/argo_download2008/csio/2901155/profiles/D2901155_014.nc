CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2901155 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20081013091840  20081013091840  0046_80052_014                  2C  D   APEX_SBE_3696                                                   846 @��)I�t1   @��-�pB�@7t�   @_�`   1   ARGOS   A   A   A   @�  A33Ac33A���A�ffA�33B��BffB1��BF  BZ  BlffB���B�  B�  B���B�ffB�  B�  B�ffB�ffB�33B���B�ffB���C�3C��C� C� CffC�fC� C$��C)L�C.33C333C8� C=� CB33CG� CQ�C[��CeffCoffCyL�C�� C��3C�s3C���C���C�s3C�� C���C��fC��3C��3C���C���C�Cǳ3C̦fC��fC�� C�� C���C�3CꙚCC��3C��3D�fD��D� D� D��D� D��D$� D)�fD.�fD3��D8��D=��DB��DGٚDL�3DQ��DV��D[�fD`ٚDe�fDj��Do��Dt��DyٚD�  D�` D���D���D�33D�c3D�� D���D�,�D�\�D���D��D�fD�p Dڣ3D��3D�)�D�Y�D� D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A33Ac33A���A�ffA�33B��BffB1��BF  BZ  BlffB���B�  B�  B���B�ffB�  B�  B�ffB�ffB�33B���B�ffB���C�3C��C� C� CffC�fC� C$��C)L�C.33C333C8� C=� CB33CG� CQ�C[��CeffCoffCyL�C�� C��3C�s3C���C���C�s3C�� C���C��fC��3C��3C���C���C�Cǳ3C̦fC��fC�� C�� C���C�3CꙚCC��3C��3D�fD��D� D� D��D� D��D$� D)�fD.�fD3��D8��D=��DB��DGٚDL�3DQ��DV��D[�fD`ٚDe�fDj��Do��Dt��DyٚD�  D�` D���D���D�33D�c3D�� D���D�,�D�\�D���D��D�fD�p Dڣ3D��3D�)�D�Y�D� D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A۾wA�-A���A״9A׋DA�dZA֧�A�
=A�  A�p�A�t�A���AуA�C�A�A�I�A�I�A̶FA�A�A�x�A��#A���A�C�A�S�A�(�A��/A�|�A�I�A���A��A���A�\)A�?}A���A�dZA��\A�5?A��A�XA�hsA���A��A�A�A���A�=qA�t�A��A���A��^A�l�A�|�A�VA�E�A��!A�oA��DA{��Ax�9Ar(�Al�9AfQ�A_S�AY/AU��AShsAP��AKx�AF��AAhsA>jA2�`A(�A �9A+A�PA��@�5?@�M�@��#@ܼj@θR@ȼj@�A�@��!@�
=@���@��m@��@��`@��9@��@��y@��`@�l�@�+@{�F@u�@o
=@`�u@U�-@I&�@9X@0�u@,��@(�u@#��@#@�T@�9@��@�^@ff@
��@�;@/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A۾wA�-A���A״9A׋DA�dZA֧�A�
=A�  A�p�A�t�A���AуA�C�A�A�I�A�I�A̶FA�A�A�x�A��#A���A�C�A�S�A�(�A��/A�|�A�I�A���A��A���A�\)A�?}A���A�dZA��\A�5?A��A�XA�hsA���A��A�A�A���A�=qA�t�A��A���A��^A�l�A�|�A�VA�E�A��!A�oA��DA{��Ax�9Ar(�Al�9AfQ�A_S�AY/AU��AShsAP��AKx�AF��AAhsA>jA2�`A(�A �9A+A�PA��@�5?@�M�@��#@ܼj@θR@ȼj@�A�@��!@�
=@���@��m@��@��`@��9@��@��y@��`@�l�@�+@{�F@u�@o
=@`�u@U�-@I&�@9X@0�u@,��@(�u@#��@#@�T@�9@��@�^@ff@
��@�;@/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�B��B	%�B	@�B	C�B	E�B	O�B	H�B	K�B	P�B	e`B	ffB	r�B	u�B	w�B	�+B	��B	��B	��B	�B	�LB	ǮB	�;B
ZB
�)B+BS�BL�B{�BdZBe`Br�B� B�B|�B�%B�VB��B�B��B~�Bm�BXBK�BC�B<jB33B(�B�BhB
=B
��B
�ZB
��B
�-B
��B
�bB
}�B
_;B
C�B
#�B
B	�fB	��B	ŢB	�RB	��B	�VB	t�B	e`B	9XB	�B��B��B�}B�B�B�jB�B�B�B	\B	�B	�B��B	B	�B	A�B	O�B	J�B	Q�B	]/B	n�B	y�B	�B	�B	��B	�ZB	�sB	��B
VB
uB
�B
'�B
0!B
?}B
K�B
P�B
T�B
aHB
hsB
n�B
v�B
|�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�<B��B	&�B	@�B	C�B	FUB	P|B	I�B	LRB	Q�B	e�B	g�B	s�B	v�B	x�B	�B	�B	�B	��B	��B	�[B	�B	�GB
\�B
܀B-0BU�BN=B~�Bf�Bg5Bt�B��B�2B~�B�]B�^B�MB��B��B��Bo�BX�BLsBD�B=�B3�B*BEB�BnB
��B
�"B
��B
�uB
�_B
�!B
�B
`�B
E>B
%�B
�B	�[B	ҌB	�^B	��B	��B	��B	u�B	f�B	:�B	�B�qB�B��B��B��B�`B��B��B�-B	�B	�B	pB��B	gB	�B	A�B	P�B	J�B	RCB	]aB	o
B	z5B	�LB	�=B	��B	�B	�B	�+B
�B
�B
�B
(B
08B
?�B
K�B
P�B
UB
a\B
h�B
n�B
v�B
|�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907200316442009072003164420090720031644  HZ  ARGQ                                                                        20081013091840  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20081013091840  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720031644  IP                  G�O�G�O�G�O�                