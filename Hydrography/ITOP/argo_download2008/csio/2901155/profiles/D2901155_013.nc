CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2901155 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20081003091813  20081003091813  0046_80052_013                  2C  D   APEX_SBE_3696                                                   846 @���N���1   @���|e�8@8X�    @_�n�   1   ARGOS   A   A   A   @�  A  Ac33A���A���A�33BffB33B0��BD  BY��Bl��B�33B�33B���B���B���B�  B���B�  B�  Bۙ�B�  B���B�  C33C� C� C33CffC� C��C$ffC)ffC.� C3L�C8�3C=ffCBffCG� CQL�CZ�fCe  CoffCyffC��3C���C��fC�� C��3C���C�s3C���C���C���C���C�� C��fC���Cǌ�C�� Cљ�C֌�CۦfC�3C� C�� C�� C���C��fD�3D�3D� D��D� D�3D�3D$�fD)�fD.��D3�fD8�fD=� DBٚDG� DL�3DQ��DV� D[��D`��De��Dj�fDo�3Dt� Dy�fD�#3D�ffD���D��3D�fD�l�D��3D���D�  D�` D��fD�� D�)�D�c3DڦfD�ٚD��D�ffD� D���D�P 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�  A  Ac33A���A���A�33BffB33B0��BD  BY��Bl��B�33B�33B���B���B���B�  B���B�  B�  Bۙ�B�  B���B�  C33C� C� C33CffC� C��C$ffC)ffC.� C3L�C8�3C=ffCBffCG� CQL�CZ�fCe  CoffCyffC��3C���C��fC�� C��3C���C�s3C���C���C���C���C�� C��fC���Cǌ�C�� Cљ�C֌�CۦfC�3C� C�� C�� C���C��fD�3D�3D� D��D� D�3D�3D$�fD)�fD.��D3�fD8�fD=� DBٚDG� DL�3DQ��DV� D[��D`��De��Dj�fDo�3Dt� Dy�fD�#3D�ffD���D��3D�fD�l�D��3D���D�  D�` D��fD�� D�)�D�c3DڦfD�ٚD��D�ffD� D���D�P 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�A�+A�7A�7A�+A�x�A�n�A�hsA�bNA�\)A��A�\)A�JAߍPAۣ�A���A���A֣�A�I�A�C�A��A��A�r�A�n�Aú^A�VA�ffA�hsA���A��A�?}A���A��A�n�A�VA�JA�jA�=qA��yA�=qA�bNA���A��A�9XA��`A��`A�A��!A��A��#A�Q�A�n�A�S�A�jA�x�A��A~��Ay�hAr�+Am��Ai%Ae��AbbA^9XAV�HAT��AN�uAFM�AA�A<�A8��A2ffA(�\A��A��A^5A�9A33AX@��@�z�@ָR@ʟ�@�
=@��R@�1'@���@��@��!@��R@��w@��@��!@���@�`B@�G�@u�@g�@\Z@P�@E�h@=/@5�@/�;@'��@$�j@\)@"�@�@t�@G�@{@
��@�@E�@ff11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�A�+A�7A�7A�+A�x�A�n�A�hsA�bNA�\)A��A�\)A�JAߍPAۣ�A���A���A֣�A�I�A�C�A��A��A�r�A�n�Aú^A�VA�ffA�hsA���A��A�?}A���A��A�n�A�VA�JA�jA�=qA��yA�=qA�bNA���A��A�9XA��`A��`A�A��!A��A��#A�Q�A�n�A�S�A�jA�x�A��A~��Ay�hAr�+Am��Ai%Ae��AbbA^9XAV�HAT��AN�uAFM�AA�A<�A8��A2ffA(�\A��A��A^5A�9A33AX@��@�z�@ָR@ʟ�@�
=@��R@�1'@���@��@��!@��R@��w@��@��!@���@�`B@�G�@u�@g�@\Z@P�@E�h@=/@5�@/�;@'��@$�j@\)@"�@�@t�@G�@{@
��@�@E�@ff11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB~�B~�B~�B~�B~�B}�B}�B|�B}�B}�B�B�=B��B	B	ffB	�{B	�B
B
N�B
�VB
�B
�?B
ĜBBbB�BD�BT�Be`B|�B�B�B�+B�B�1B��B�+B�B�B�PB�Bv�Bp�BjB\)BM�BF�B;dB+B�B�BVB
��B
�TB
��B
��B
��B
�B
]/B
F�B
.B
�B
	7B	�B	��B	��B	�B	�1B	t�B	cTB	O�B	33B	hB��B�`B�BƨB��BƨBȴB��B��B�B��B	bB	+B	
=B	33B	(�B	$�B	H�B	m�B	x�B	s�B	u�B	��B	B	�}B	ɺB	�/B	�B	��B
+B
oB
 �B
+B
7LB
A�B
I�B
S�B
[#B
bNB
hsB
k�B
n�B
n�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B~�B~�B~�B~�BB}�B}�B|�B}�B~JB��B�IB�;B	yB	g�B	�5B	�DB
?B
SKB
�B
��B
��B
�B�BB�BE�BV�Bh~B}vB�aB��B��B�WB�0B�hB�PB�AB��B�B�_Bw�Bq�Bk�B]�BN�BG�B=B,B�BB�B
��B
�LB
ѤB
�VB
��B
��B
^�B
G�B
.�B
�B

6B	��B	ՉB	�gB	�-B	��B	u�B	c�B	P�B	4�B	�B��B�1B��B�~B��BǉB�B��B��B��B��B	�B	,NB	
$B	3�B	)�B	%"B	H�B	m�B	ygB	t5B	vB	��B	��B	��B	��B	�`B	�B	��B
IB
�B
 �B
+B
7bB
A�B
I�B
TB
[4B
b`B
hB
k�B
n�B
n�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907200316442009072003164420090720031644  HZ  ARGQ                                                                        20081003091813  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20081003091813  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720031644  IP                  G�O�G�O�G�O�                