CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2901162 CHINA ARGO PROJECT                                              JIANPING XU & JIANHUA ZHANG                                     PRES            TEMP            PSAL               A   HZ  20080808092008  20080808092008  0053_40950_007                  2C  D   APEX_SBE_3703                                                   846 @���0[�1   @���P6�@5���   @b���   1   ARGOS   A   A   A   @�ffA��Ah  A���A�ffA홚B33B��B2ffBE��BZ  Bn��B���B���B�  B�  B�  B���B���B�ffBЙ�B���B�  BB�ffCffC��C�CffCffC  CL�C$  C(��C.33C3�C7��C=33CB  CGL�CQL�C[33Ce� Co� CyL�C��fC�Y�C�� C���C���C���C�ffC��fC���C��fC�� C��fC��3C�C�s3C̳3Cр C֙�C۳3C�fC���C�fC�� C��3C�� D�fD��D� D� D�3D� DٚD$� D)� D.�3D3�3D8�3D=ٚDB� DG�3DLٚDQ��DV��D[��D`�3DeٚDjٚDo�3Dt� Dy�fD�&fD�p D���D��fD�#3D�\�D��fD��D�,�D�l�D��fD�ٚD�)�D�p Dڣ3D���D�)�D�l�D�3D��fD�)�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA��Ah  A���A�ffA홚B33B��B2ffBE��BZ  Bn��B���B���B�  B�  B�  B���B���B�ffBЙ�B���B�  BB�ffCffC��C�CffCffC  CL�C$  C(��C.33C3�C7��C=33CB  CGL�CQL�C[33Ce� Co� CyL�C��fC�Y�C�� C���C���C���C�ffC��fC���C��fC�� C��fC��3C�C�s3C̳3Cр C֙�C۳3C�fC���C�fC�� C��3C�� D�fD��D� D� D�3D� DٚD$� D)� D.�3D3�3D8�3D=ٚDB� DG�3DLٚDQ��DV��D[��D`�3DeٚDjٚDo�3Dt� Dy�fD�&fD�p D���D��fD�#3D�\�D��fD��D�,�D�l�D��fD�ٚD�)�D�p Dڣ3D���D�)�D�l�D�3D��fD�)�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�%A�v�A�hsA�ffA�1A��A�33A�S�A�+Aԥ�A�?}A���A�
=A��A�ZA���A�ƨA���A�"�A�E�A��A�1A�+A�{A��9A�G�A��`A���A�ȴA���A�hsA�M�A��FA��A�\)A��A��A��A��^A�`BA�ZA�9XA���A��TA�oA��mA�\)A}�-AyO�Ax~�Ap�Am��Ak"�Ae|�A`�AY7LARALĜAJ �AD�uA@�\A<Q�A7XA0�HA,ĜA&E�A#�hA�A��A��A��AVA�@�\@蛦@���@պ^@�`B@��@��R@��@��@��@��+@�~�@�j@��@�^5@�|�@��@�n�@�ff@��u@�33@��@z�@n5?@a��@Rn�@J�@Dj@:M�@4Z@,�D@(r�@$I�@ r�@��@�@Z@��@	��@K�@�F@G�@G�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�%A�v�A�hsA�ffA�1A��A�33A�S�A�+Aԥ�A�?}A���A�
=A��A�ZA���A�ƨA���A�"�A�E�A��A�1A�+A�{A��9A�G�A��`A���A�ȴA���A�hsA�M�A��FA��A�\)A��A��A��A��^A�`BA�ZA�9XA���A��TA�oA��mA�\)A}�-AyO�Ax~�Ap�Am��Ak"�Ae|�A`�AY7LARALĜAJ �AD�uA@�\A<Q�A7XA0�HA,ĜA&E�A#�hA�A��A��A��AVA�@�\@蛦@���@պ^@�`B@��@��R@��@��@��@��+@�~�@�j@��@�^5@�|�@��@�n�@�ff@��u@�33@��@z�@n5?@a��@Rn�@J�@Dj@:M�@4Z@,�D@(r�@$I�@ r�@��@�@Z@��@	��@K�@�F@G�@G�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	��B
aHB
ffB
e`B
[#B
��B
��B�BK�BffB� B�+B�!B��B��B�fB�mB�;B��B��BhBPB  B��BȴBĜB�B��B�1B{�Bz�Bz�Bw�Bs�BhsBZBQ�BJ�BD�B8RB�B%B
�B
�HB
��B
�3B
��B
�\B
|�B
r�B
K�B
=qB
,B
PB	�sB	ŢB	��B	�B	z�B	`BB	Q�B	?}B	(�B	oB	%B�B�ZB��BǮB�dB��B�oB�%BffBaHBgmBjBn�Br�B�%B�=B��B�9B��B�mB��B	bB	+B	K�B	e`B	|�B	�%B	�hB	�B	�RB	��B	�B	��B	��B
DB
�B
$�B
1'B
33B
<jB
E�B
L�B
Q�B
VB
]/B
_;B
aHB
l�B
p�B
v�B
v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	΋B
aYB
frB
e�B
]RB
��B
�!B�BS�BhdB�kB�_B��BӄB�)B�B�B�BհB�BVB BB��B�B��B�9B�KB�&B|%B{ B{�BybBuMBj�B[BSBK�BE�B9�B!JBNB
�B
�B
��B
�B
��B
�tB
})B
t�B
L�B
>&B
-tB
�B	�4B	ǓB	��B	��B	|fB	aNB	S!B	@�B	*�B	�B	�B�oB�!BБB��B��B��B�SB��Bg+Bb
Bh*BkzBoYBslB�B��B��B��B�@B�B�5B	�B	*�B	K�B	e�B	}EB	�RB	��B	�MB	�sB	�B	��B	�)B	��B
cB
�B
$�B
1JB
3HB
<B
E�B
L�B
R B
VB
]IB
_QB
aWB
l�B
p�B
v�B
v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;啁;�b�;�a;幧;���<�<S�;���<-�;��;���<��<�&<��;���;�< �;�z�;��;臸;�LF< ��;眕<
�;���;�ڷ;�8;�;7;���;�;�hl;�\\;���;��;��*;�v;��;�u�;��.;�/�;��;�ZP;�m+;��Y;� k;��:;�v;��;�~B;�M�;��L;�|M;��b;��;�";��\;鐭;�;�x@;��;��;�`\;�;�f_;��;���;�N�;�s;��;�`;�n�;�$�;�{�;�;�6;旯;��;椩;��;�;�+�;�;�	�;��;�z.;�v;兓;�`�;�l�;�&;��;�q�;�k�;�|G;�i�;�{�;�y�;冝;�kN;�h�;�q�;�gW;�k3;�d";�d,;�c�;�c�;�c�;�c�;�fO;�d�;�b;�c";�a�;�``;�``PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; OW: No significant salinity drift detected; r=1.000000                                                                                                                                         The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(OW & CTM error, SBE sensor accuracy)                                                                                                                                                    200907201029352009072010293520090720102935  HZ  ARGQ                                                                        20080808092008  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20080808092008  QCF$                G�O�G�O�G�O�0               HZ  ARSQOW  1.1 Coriolis CTD dataset                                            20090720102935  IP                  G�O�G�O�G�O�                