CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   ?   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   units         decibar    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   	valid_min                	valid_max         F;�    
resolution        =���      �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   units         decibar    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   	valid_min                	valid_max         F;�    
resolution        =���      �  3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  4�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   
resolution        =���      �  5   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min         �      	valid_max         B      
resolution        :�o      �  6   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  7   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min         �      	valid_max         B      
resolution        :�o      �  7L   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  8H   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   
resolution        :�o      �  8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     units         psu    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min                	valid_max         B(     
resolution        :�o      �  9�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  :�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     units         psu    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min                	valid_max         B(     
resolution        :�o      �  :�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  @  ;�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   
resolution        :�o      �  ;�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  <�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =(   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @(   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C(   CALIBRATION_DATE            	             
_FillValue                  ,  F(   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    FT   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    FX   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    F\   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    F`   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Fd   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    F�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    F�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    F�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   units         decibar    
_FillValue        G�O�        F�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    units         decibar    
_FillValue        G�O�        F�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        F�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    F�Argo profile    2.2 1.2 19500101000000  2900637 Argo METRI/KMA                                                  Sang Boom Ryoo                                                  PRES            TEMP            PSAL               IA   KM  20080811050909  20091106143400  A0_01864_006                    2C  D   APEX-SBE 3070                                                   846 @�絽p��1   @���S�m:@5�����@`d1&�y1   ARGOS   A   A   A   A33A�33A�  B��BG��Bn  B���B�ffB�ffB�  B���B�ffC��CL�C�C33C)ffC3ffC=L�CG33C[� CoffC��fC���C��3C�� C���C���C��3CǦfCр C�� C噚C�fC���DٚD� DٚD��D�3D�3D�fD$�3D)��D.�fD3ٚD8��D=�fDB��DG�3DN�DT@ DZ� D`��Dg  DmL�Ds� Dy�fD��D�l�D���D�ffD���111111111111111111111111111111111111111111111111111111111111111 A33A�33A�  B��BG��Bn  B���B�ffB�ffB�  B���B�ffC��CL�C�C33C)ffC3ffC=L�CG33C[� CoffC��fC���C��3C�� C���C���C��3CǦfCр C�� C噚C�fC���DٚD� DٚD��D�3D�3D�fD$�3D)��D.�fD3ٚD8��D=�fDB��DG�3DN�DT@ DZ� D`��Dg  DmL�Ds� Dy�fD��D�l�D���D�ffD���111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�JA�ȴA�A��A�G�A�dZA��
A�p�A��A�-AőhA��^A�ƨA�K�A��A�JA��A���A���A�r�A�"�A���A�`BA�~�A�ffA��A��TA�1'A~�9Aul�AnVAa�AV�uAO\)AA�A6��A)O�A33A�!A�+A	/A�h@�b@�|�@ߍP@��#@���@�%@��h@���@�%@���@�=q@�(�@�7L@�/@��@��
@��j@k33@T�@CS�@5��111111111111111111111111111111111111111111111111111111111111111 A�JA�ȴA�A��A�G�A�dZA��
A�p�A��A�-AőhA��^A�ƨA�K�A��A�JA��A���A���A�r�A�"�A���A�`BA�~�A�ffA��A��TA�1'A~�9Aul�AnVAa�AV�uAO\)AA�A6��A)O�A33A�!A�+A	/A�h@�b@�|�@ߍP@��#@���@�%@��h@���@�%@���@�=q@�(�@�7L@�/@��@��
@��j@k33@T�@CS�@5��111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��B��B��B�B�B	ZB	�B
Q�B
t�B
aHB
�#B33BXB� B�PB�oB��B��B�=B�Bx�Bq�BffBQ�B7LB�BB
�B
��B
r�B
J�B
DB	��B	�9B	v�B	B�B	VB�B�fB��BƨB�RBȴBĜBÖB�3B��B�LB��B�BB��B	PB	$�B	J�B	jB	�bB	��B	�FB	��B	��B	�mB
+B
+111111111111111111111111111111111111111111111111111111111111111 B�dB�dB�dB�}B�B	T{B	��B
L0B
o B
[�B
�gB-wBRTBzDB��B��B��B��B��BcBsBk�B`�BL0B1�B�B
�PB
�HB
�'B
mB
EB
�B	�VB	��B	q'B	=B	�B�)B��B�^B� B��B�-B�B�B��B�VB��B�EBںB�GB	�B	VB	E9B	d�B	��B	�OB	��B	�vB	�?B	��B
�B
%`111111111111111111111111111111111111111111111111111111111111111 9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷ9ѷPRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            No sifnificant pressure drift detected                                                                                                                                                                                                                          No significant temprature drift detected                                                                                                                                                                                                                        Significant salinity drift present; OW weighted least squares fit is adopted;       Map Scales:[x:8/4,y:4/2]; max_breaks=4;  T<5.0DegC                                                                                                                          200911061432002009110614320020091106143200  KM      ARGQ1.0                                                                 20080812011003  QCP$PSAL            G�O�G�O�G�O�  11110110111111KD  ARSQOW  V1.1SeHyD and ARGO_for_DMQC Climatology Version 2009V02             20091106143200  IP  PSAL            G�O�G�O�G�O�                