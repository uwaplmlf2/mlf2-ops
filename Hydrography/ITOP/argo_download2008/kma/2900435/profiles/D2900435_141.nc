CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   E   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   units         decibar    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   	valid_min                	valid_max         F;�    
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   units         decibar    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   	valid_min                	valid_max         F;�    
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   
resolution        =���       5T   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min         �      	valid_max         B      
resolution        :�o       6h   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min         �      	valid_max         B      
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   
resolution        :�o       9    PSAL         
      	   	long_name         PRACTICAL SALINITY     units         psu    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min                	valid_max         B(     
resolution        :�o       :4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;H   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     units         psu    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min                	valid_max         B(     
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   
resolution        :�o       <�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >    SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >0   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A0   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D0   CALIBRATION_DATE            	             
_FillValue                  ,  G0   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G\   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G`   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Gd   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Gh   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Gl   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   units         decibar    
_FillValue        G�O�        G�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    units         decibar    
_FillValue        G�O�        G�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G�Argo profile    2.2 1.2 19500101000000  2900435 Argo METRI/KMA                                                  Kim, Jeong-Sik                                                  PRES            TEMP            PSAL               �A   KM  20080916000810  20090731120100  A0_01864_006                    2C  D   APEX-SBE 1881                                                   846 @��\�$1   @���@3ٙ����@_}����1   ARGOS   A   B   B   A  A�33A�  B��BF  Bl  B���B�33B�  B�ffB�33B���C�3CffC33C� C)� C3ffC=� CG33C[L�Co�C��3C���C���C�� C���C��3C���C�� CѦfCۦfC��C�fC���DٚD�3D�3D��D�3D��D� D$�3D)�3D.� D3�3D8ٚD=� DBٚDG� DN3DTY�DZ�3D`�fDg�DmS3Ds��Dy�3D�&fD�l�D�� D�p D��fD�p D��fD�l�D��fD�i�D���111111111111111111111111111111111111111111111111111111111111111111111   A  A�33A�  B��BF  Bl  B���B�33B�  B�ffB�33B���C�3CffC33C� C)� C3ffC=� CG33C[L�Co�C��3C���C���C�� C���C��3C���C�� CѦfCۦfC��C�fC���DٚD�3D�3D��D�3D��D� D$�3D)�3D.� D3�3D8ٚD=� DBٚDG� DN3DTY�DZ�3D`�fDg�DmS3Ds��Dy�3D�&fD�l�D�� D�p D��fD�p D��fD�l�D��fD�i�D���111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�x�A�^5A���A��A��A�ĜA�A��AٸRA׏\A��A�&�A�bNA�hsA�=qA��#A���A�%A���A�O�A��PA���A�
=A���A�r�A��/A�x�Au��Ai��A`�HAYK�AMt�AHz�A9�A/��A%%AVAhsA	�-A�@�j@���@�$�@���@�O�@���@�/@���@��j@��y@��F@�Q�@��@�{@��@���@�p�@��7@p�@_��@J�!@=�@6�@,j@$9X@�F@�@��@�w111111111111111111111111111111111111111111111111111114111111111111111   A�x�A�^5A���A��A��A�ĜA�A��AٸRA׏\A��A�&�A�bNA�hsA�=qA��#A���A�%A���A�O�A��PA���A�
=A���A�r�A��/A�x�Au��Ai��A`�HAYK�AMt�AHz�A9�A/��A%%AVAhsA	�-A�@�j@���@�$�@���@�O�@���@�/@���@��j@��y@��F@�Q�@��G�O�@��@���@�p�@��7@p�@_��@J�!@=�@6�@,j@$9X@�F@�@��@�w111111111111111111111111111111111111111111111111111114111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oG�O�;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBgmBe`BYB?}BC�BW
BjB�NB	t�B	��B
�B
�B6FBz�B�JB�B�B��B�PB�DB}�Br�BffBQ�B0!B
��B
�XB
r�B
;dB
bB	�B	�^B	��B	n�B	L�B	33B	{B��B�B��B��B�fB��BB�)B	�B	)�B	6FB	N�B	bNB	l�B	v�B	}�A��HB	��B	�jB	�XB	��B	�B
B
 �B
=qB
R�B
bNB
m�B
v�B
}�B
�B
�V111111111111111111111111111111111111111111111111111134311111111111111   BZBXBK�B2-B6FBI�B]/B��B	gmB	��B
JB
�HB'�Bl�B}�B��B��B��B~�B|�Bo�BdZBXBC�B!�B
�sB
�B
e`B
.B
B	�/B	�B	��B	aHB	?}B	%�B	+B�B�TB�B�B�B��B�?B��B		7B	�B	(�B	A�B	T�B	_;B	iyG�O�G�O�G�O�B	�B	�B	ÖB	�ZB	��B
uB
0!B
E�B
T�B
`BB
iyB
p�B
w�B
�111111111111111111111111111111111111111111111111111144411111111111111   :�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�oG�O�G�O�G�O�:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�oPRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            No sifnificant pressure drift detected                                                                                                                                                                                                                          No significant temprature drift detected                                                                                                                                                                                                                        Significant salinity drift present; OW weighted least squares fit is adopted;       Map Scales:[x:8/4,y:4/2]; max_breaks=4;  T<5.0DegC                                                                                                                          200907311200002009073112000020090731120000  KM      ARGQ1.0                                                                 20080916211002  QCP$PSAL            G�O�G�O�G�O�  11110110111111SI  ARSQSIQCV2.0SeHyD/IOHB in OW calc. & Argo as subjective check outside OW;   20090730152415  CF  TEMP            D`�fD`�f?�                  SI  ARSQSIQCV2.0SeHyD/IOHB in OW calc. & Argo as subjective check outside OW;   20090730152415  CF  PSAL            D`�fD`�f@@                                                                                                                  IP                  G�O�G�O�G�O�                    ARSQ                                                                                                            G�O�G�O�G�O�                KD      OW  V1.1SeHyD & ARGO_for_DMQC Climatology Version 2009V02               20090731120000      PSAL            G�O�G�O�G�O�                