CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  2900964 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090807155802  20100301001029  A14_86545_018                   2C  D   APEX-SBE 4144                                                   846 @�A�o���1   @�A��id@;��vȴ9@as
=p��1   ARGOS   B   B   B   @�33A)��A{33A�33A�ffA�ffB��B   B733BJ  B^ffBrffB�  B�ffB���B�33B�  B�  B���B�  B�  B���B���B�33B���C  CffC�C33C  C�fC�fC$�fC(��C,��C4ffC9ffC>�CC  CH� CQ�fC\�Cf33Co�fCz  C�&fC��3C��fC�  C��3C�ٚC�33C�� C��C�33C��C��C��C���C�  C�33C��C��C�&fC��C�@ C��C�  C��C�  D��D3DfD�D  D�D�3D$�fD)��D/fD4fD9  D>  DC  DG��DM3DR3DW�D\fDa�Df3DkfDp3Dt��DzfD�S3D�y�D��fD��D�L�D�s3D�� D��D�FfD���D�� D�3D�0 DԌ�D��3D��D�@ D�|�D�ɚD��D��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114@���A.ffA�  A���A���A���B  B!33B8ffBK33B_��Bs��B���B�  B�ffB���B���B���B�ffBș�Bә�B�ffB�ffB���B�ffCL�C�3CffC� CL�C33C 33C%33C)�C-�C4�3C9�3C>ffCCL�CH��CR33C\ffCf� Cp33CzL�C�L�C��C��C�&fC��C�  C�Y�C��fC�@ C�Y�C�@ C�33C�@ C��3C�&fC�Y�C�33C�33C�L�C�@ C�ffC�@ C�&fC�33C�&fD�D&fD�D,�D33D,�D fD$��D*  D/�D4�D933D>3DC33DH  DM&fDR&fDW,�D\�Da  Df&fDk�Dp&fDu  Dz�D�\�D��3D�� D�fD�VfD�|�D�ɚD�fD�P D��fD�ɚD��D�9�DԖfD���D�fD�I�D�fD��3D�fG�O�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��G�O�A�VA�=qA�VA�oA�
=A�C�A��yA��;A���A��FA��
A�x�A��!A��`A�A�n�A�VA�JA�O�A�ȴA���A���A�`BA�"�A��jA�z�A�9XA��A�1A��A���A��A�"�A��DA�\)A��-A�G�A�bA��#A�VA��jA�5?A��;A��uA�{A�I�A�VA��PA�O�A��DA��A�\)A�A|�A{%AudZApȴAlȴAf=qAb�A\^5AWG�AU�^AT�AP�/AI��A?G�A;+A9ƨA5A/��A,bNA�AO�A�
@��@���@�r�@�C�@җ�@�v�@�7L@� �@�V@�^5@�t�@�1@�?}@�r�@���@��@�x�@}p�@y��@pr�@f��@\�@U�T@N��@Fȴ@A7L@7��@.{@&�@"��@@7L@"�@ȴ@
=q@ff@C�?��D?�r�?�l�?�l�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114A�VA�=qA�VA�oA�
=A�C�A��yA��;A���A��FA��
A�x�A��!A��`A�A�n�A�VA�JA�O�A�ȴA���A���A�`BA�"�A��jA�z�A�9XA��A�1A��A���A��A�"�A��DA�\)A��-A�G�A�bA��#A�VA��jA�5?A��;A��uA�{A�I�A�VA��PA�O�A��DA��A�\)A�A|�A{%AudZApȴAlȴAf=qAb�A\^5AWG�AU�^AT�AP�/AI��A?G�A;+A9ƨA5A/��A,bNA�AO�A�
@��@���@�r�@�C�@җ�@�v�@�7L@� �@�V@�^5@�t�@�1@�?}@�r�@���@��@�x�@}p�@y��@pr�@f��@\�@U�T@N��@Fȴ@A7L@7��@.{@&�@"��@@7L@"�@ȴ@
=q@ff@C�?��D?�r�?�l�G�O�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oG�O�B	��B	��B	�B	��B	��B
0!B
n�B
�%B
�uB
��B
��B
��B
�B
�B
��B
�wB
�qB
ĜB
B
ÖB
ǮB
��B
��B
��B
��B
�B
��B
�B
�
B
�)B
�B
�B
�#B
�B
�B
�B
�;B
�`B
�B
�B
��B
��B
��B
��B
��B
�B
�yB
�;B
��B
ɺB
�XB
�B
��B
�uB
�DB
q�B
_;B
L�B
/B
�B
B	�B	�B	�TB	��B	�9B	�=B	x�B	v�B	dZB	P�B	>wB	B��B��B�\B�=B�VB�+B�DB�+B�B�hB��B��B�'BɺBŢB�`B�B	
=B	$�B	-B	:^B	M�B	cTB	z�B	�PB	��B	�FB	ÖB	�B	�B
  B

=B
�B
�B
-B
:^B
F�B
P�B
[#B
e`B
k�B
m�B
m�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114B	��B	��B	��B	��B
%B
;dB
r�B
�7B
��B
��B
��B
�B
�B
�'B
��B
�wB
�wB
ŢB
ĜB
ĜB
ȴB
��B
��B
��B
��B
�B
�B
�B
�
B
�)B
�B
�B
�)B
�B
�B
�B
�;B
�`B
�B
�B
��B
��B  B
��B
��B
�B
�B
�BB
��B
��B
�^B
�B
��B
�uB
�JB
r�B
`BB
N�B
0!B
�B
B	�B	�B	�ZB	�B	�LB	�DB	x�B	w�B	e`B	P�B	@�B	+B��B��B�bB�DB�\B�1B�JB�1B�B�hB��B��B�-BɺBƨB�`B�B	DB	$�B	-B	:^B	M�B	cTB	z�B	�PB	��B	�FB	ÖB	�B	�B
  B

=B
�B
�B
-B
:^B
F�B
P�B
[#B
e`B
k�B
m�G�O�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114<#�
<#�
<49X<#�
<T��<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
G�O�PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=-0.3(dbar)                                                                                                                                                                                                                                        None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         201002221300392010022213003920100222130039201002231254062010022312540620100223125406201002260000002010022600000020100226000000  JA  ARFMdecpA14                                                                 20090807155801  IP                  G�O�G�O�G�O�                JM  ARFMDECPA14                                                                 20090809173152  IP                  G�O�G�O�G�O�                JM  ARFMBITP1                                                                   20090807155801  SV                  C�  C�&fG�O�                JM  ARGQRQCP1                                                                   20090809173152  QCP$                G�O�G�O�G�O�            FB7CJM  ARGQRQCP1                                                                   20090809173152  QCF$                D��D��G�O�             100JM  ARCAJMQC1.0                                                                 20100222130039  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20100222130039  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20100223125406  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100226000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100301000948  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100301001029                      G�O�G�O�G�O�                