CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900964 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091005125713  20100301001025  A14_86545_024                   2C  D   APEX-SBE 4144                                                   846 @�P���ˣ1   @�PÏ�@@<O�;dZ@a*n��O�1   ARGOS   A   A   A   @s33A&ffAy��A�  A�  A�  BffB!��B6ffBJ��B]��Br  B�33B���B�ffB�33B�33B���B�  B�  B���Bܙ�B�33B�ffB�33C  C33C�fCL�CffCffC � C$�3C)�fC/�C3��C9  C>ffCB��CHL�CR��C\33CfffCp  Cz�C��fC��fC��fC��C�33C�ٚC��C��fC��C�L�C�  C�  C��3C�ٚC�� C�33C�� C���C��fC��fC�Y�C�33C�33C�@ C��fD  DfD�D�DfD  D 3D$�3D*3D/3D4fD8�3D>  DCfDH  DM�DR&fDV��D\  Da�De��Dk  Do�3DufDz�D�P D��fD�� D�  D�<�D�vfD��3D��D�9�D�l�D��3D�3D�0 D�i�DڶfD�� D�<�D�l�D�D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A+33A~ffA�ffA�ffA�ffB��B"��B7��BL  B^��Bs33B���B�33B�  B���B���B�ffB���Bř�B�ffB�33B���B�  B���CL�C� C33C��C�3C�3C ��C%  C*33C/ffC3�fC9L�C>�3CC�CH��CR�fC\� Cf�3CpL�CzffC��C��C��C�33C�Y�C�  C�@ C��C�33C�s3C�&fC�&fC��C�  C��fC�Y�C��fC��3C��C��C� C�Y�C�Y�C�ffC��D3D�D,�D  D�D33D &fD%fD*&fD/&fD4�D9fD>3DC�DH33DM,�DR9�DW�D\3Da  Df�Dk33DpfDu�Dz,�D�Y�D�� D�ɚD�	�D�FfD�� D���D�fD�C3D�vfD���D��D�9�D�s3D�� D���D�FfD�vfD�3D�ɚ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��/Aޡ�Aއ+A��A�$�A�~�A�n�A�33A�r�A��`A�XA���A��A�  A�~�A��7A��A��\A���A�K�A�x�A�-A��A���A�7LA�&�A���A��A�p�A�z�A��
A��A��A�(�A���A�  A���A��TA��uA���A���A�^5A�M�A��A�ƨA��`A���A�G�A��#A��RA��9A���A���A��`A�A~9XAy�hAsK�Am|�Ahn�Ac�^Aa�A^I�AY�^AR�DAM�TAIx�AD^5A>ĜA81A/|�A%�AVA-@�7L@�7L@�p�@�;d@�j@�9X@�7L@�"�@��\@���@�=q@���@�~�@�I�@��F@��@�ff@���@}�-@y��@up�@h�@\9X@P��@I��@A�^@7K�@0��@+S�@%�h@!&�@�@Q�@�F@�;@�
@��@�@33@��?�;d1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��/Aޡ�Aއ+A��A�$�A�~�A�n�A�33A�r�A��`A�XA���A��A�  A�~�A��7A��A��\A���A�K�A�x�A�-A��A���A�7LA�&�A���A��A�p�A�z�A��
A��A��A�(�A���A�  A���A��TA��uA���A���A�^5A�M�A��A�ƨA��`A���A�G�A��#A��RA��9A���A���A��`A�A~9XAy�hAsK�Am|�Ahn�Ac�^Aa�A^I�AY�^AR�DAM�TAIx�AD^5A>ĜA81A/|�A%�AVA-@�7L@�7L@�p�@�;d@�j@�9X@�7L@�"�@��\@���@�=q@���@�~�@�I�@��F@��@�ff@���@}�-@y��@up�@h�@\9X@P��@I��@A�^@7K�@0��@+S�@%�h@!&�@�@Q�@�F@�;@�
@��@�@33@��?�;d1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
v�B
u�B
t�B
n�B
N�B
@�B
M�B
G�B
`BB
o�B
�B
��B
��B
��B
�B
�fB
�TB
�B
�B
��B
��BBBDB
��B
�#B
�)B
�#B
�)B
�;B
�TB
�TB
�`B
�`B
�`B
�yB
�sB
�B
�B
�B
��BBBB  B
��B
��B
�B
�B
�B
�ZB
��B
ĜB
�B
��B
��B
�+B
k�B
P�B
:^B
(�B
�B
\B	��B	�)B	ǮB	�?B	��B	�7B	n�B	Q�B	+B	B�B�^B��B�uB�uB�\B�{B�{B��B��B�'BĜB�TB�B��B		7B	�B	 �B	,B	:^B	H�B	^5B	p�B	�oB	�B	�?B	ƨB	�HB	�B	��B
DB
�B
�B
+B
6FB
?}B
L�B
R�B
]/B
cTB
e`B
k�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
v�B
u�B
u�B
q�B
O�B
@�B
N�B
L�B
bNB
r�B
�7B
�'B
��B
��B
�B
�mB
�ZB
�B
��B
��B
��B%BBPB
��B
�)B
�/B
�#B
�/B
�BB
�TB
�TB
�fB
�fB
�fB
�yB
�yB
�B
�B
�B
��BBBB  B
��B
��B
�B
�B
�B
�`B
��B
ŢB
�B
��B
��B
�7B
l�B
Q�B
;dB
)�B
�B
bB	��B	�/B	ȴB	�FB	��B	�DB	o�B	R�B	-B	B�#B�dB��B�{B�{B�bB��B��B��B��B�-BĜB�TB��B��B		7B	�B	 �B	,B	:^B	H�B	^5B	p�B	�oB	�B	�?B	ƨB	�HB	�B	��B
DB
�B
�B
+B
6FB
?}B
L�B
R�B
]/B
cTB
e`B
k�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=-0.3(dbar)                                                                                                                                                                                                                                        None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200910180735472009101807354720091018073547200910180837442009101808374420091018083744201002260000002010022600000020100226000000  JA  ARFMdecpA14                                                                 20091005125712  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091005125713  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091005125713  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091005125714  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091005125715  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091005125715  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091005125715  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091005125715  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091005125715  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091005130132                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091009005827  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091009005903  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091009005903  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091009005903  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091009005905  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091009005905  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091009005905  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091009005905  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091009005905  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091009010329                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20091008171322  CV  DAT$            G�O�G�O�F��                JM  ARCAJMQC1.0                                                                 20091018073547  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20091018073547  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20091018083744  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100226000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100301000945  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100301001025                      G�O�G�O�G�O�                