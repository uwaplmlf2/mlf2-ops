CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  2900964 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090816065641  20100301001028  A14_86545_019                   2C  D   APEX-SBE 4144                                                   846 @�D;�%1   @�D=�@;��t�j@ad     1   ARGOS   A   A   A   @�ffA��A{33A�33A���A���B	33B#��B733BH��B^  BpffB�  B�33B�ffB�33B�ffB���B�ffB�  B�ffB�ffB�ffB�ffB���C33C�fC��C�fC�C� C 33C$��C*ffC.�fC4ffC9  C=�fCB�3CH� CRffCZ� Cf� Cp� CzL�C�&fC��3C�33C��3C�  C�  C��3C�ٚC�  C��3C�33C�&fC�  C�  C��C�  C���C�&fC�&fC�ٚC�33C�ٚC�L�C�L�C�@ D  D  D��D��DfD�D &fD%  D*�D/�D3��D8�3D=ٚDC  DHfDM  DQ�3DWfD\fDa  DffDj�fDo�3Dt�fDz�D�S3D�|�D��fD�	�D�<�D���D��fD�3D�@ D�y�D���D���D�I�D�0 Dڹ�D���D�@ D퉚D���D�3D�� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�  A��A�  A���A�33A�33B
ffB$��B8ffBJ  B_33Bq��B���B���B�  B���B�  B�ffB�  Bř�B�  B�  B�  B�  B�33C� C33C�C33CffC��C � C%�C*�3C/33C4�3C9L�C>33CC  CH��CR�3CZ��Cf��Cp��Cz��C�L�C��C�Y�C��C�&fC�&fC��C�  C�&fC�ٚC�Y�C�L�C�&fC�&fC�33C�&fC��3C�L�C�L�C�  C�Y�C�  C�s3C�s3C�ffD3D3D  D  D�D  D 9�D%33D*  D/,�D4�D9fD=��DC33DH�DM33DRfDW�D\�Da33Df�Dj��DpfDt��Dz  D�\�D��fD�� D�3D�FfD��fD�� D��D�I�D��3D��fD�3D�S3D�9�D��3D�3D�I�D�3D��fD��D�ɚ11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�+A�A�p�A�jA��A�S�A���A���A��yA�G�A���A�C�A��mA��A�33A�S�A�5?A���A��A��;A�-A�XA�\)A�
=A��9A��A�ƨA�1A���A��`A�$�A��HA�;dA��jA��A���A��A��hA���A��A�^5A�33A��A��TA�
=A���A�x�A�hsA�VA��PA��A�ƨA�I�A�M�Ay�wAv�Au�Asx�Ap�AhȴAb�`AaO�A`I�AR�!AN��AMdZAM�AKx�AIl�A;G�A5hsA'�A!�-A7LA��@�5?@�X@�;d@��@�G�@\@���@�K�@���@���@���@�n�@��D@��@���@�o@�33@�p�@|�@up�@i�@^��@U/@J��@C33@;�m@5�@-�@(A�@!X@~�@ȴ@��@I�@
��@�u@ff@~�?�5??�~�?�^511111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�+A�A�p�A�jA��A�S�A���A���A��yA�G�A���A�C�A��mA��A�33A�S�A�5?A���A��A��;A�-A�XA�\)A�
=A��9A��A�ƨA�1A���A��`A�$�A��HA�;dA��jA��A���A��A��hA���A��A�^5A�33A��A��TA�
=A���A�x�A�hsA�VA��PA��A�ƨA�I�A�M�Ay�wAv�Au�Asx�Ap�AhȴAb�`AaO�A`I�AR�!AN��AMdZAM�AKx�AIl�A;G�A5hsA'�A!�-A7LA��@�5?@�X@�;d@��@�G�@\@���@�K�@���@���@���@�n�@��D@��@���@�o@�33@�p�@|�@up�@i�@^��@U/@J��@C33@;�m@5�@-�@(A�@!X@~�@ȴ@��@I�@
��@�u@ff@~�?�5??�~�?�^511111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�)B�)B	9XB	~�B	�;B	��B
hB
XB
�B
�JB
��B
��B
��B
��B
�B
�B
�FB
�dB
�wB
B
B
B
ÖB
B
��B
ĜB
ŢB
��B
�B
�HB
��B
��B
��B
�B
�B
��B
��B
ȴB
�)B
�BB
�mB
�sB
�B
�B
�B
�B
�B
�B
�mB
�B
��B
�XB
�9B
��B
�+B
w�B
s�B
k�B
_;B
<jB
#�B
�B
hB	�HB	��B	��B	ȴB	��B	�3B	~�B	dZB	0!B	{B�B�B��B�\B�%B�B�B� B�B�+B��B��B�9BǮB�B�fB�B	
=B	�B	$�B	33B	B�B	`BB	v�B	�bB	��B	�wB	��B	�;B	�B	��B
bB
 �B
(�B
5?B
B�B
F�B
K�B
P�B
\)B
ffB
jB
j11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B�)B�/B	:^B	�B	�mB
DB
�B
^5B
�B
�\B
��B
��B
��B
��B
�B
�!B
�LB
�jB
�}B
B
ÖB
ÖB
ÖB
B
B
ĜB
ƨB
��B
�#B
�NB
��B
��B
��B
�B
�B
��B
��B
ɺB
�)B
�BB
�mB
�sB
�B
�B
�B
�B
�B
�B
�sB
�#B
��B
�XB
�?B
��B
�1B
w�B
s�B
l�B
aHB
>wB
#�B
�B
{B	�NB	��B	��B	ȴB	B	�LB	� B	ffB	1'B	�B�B�!B��B�bB�+B�%B�B�B�B�1B��B��B�?BǮB�B�fB�B	
=B	�B	$�B	33B	B�B	`BB	v�B	�bB	��B	�wB	��B	�;B	�B	��B
bB
 �B
(�B
5?B
B�B
F�B
K�B
P�B
\)B
ffB
jB
j11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<�C�<T��<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=-0.3(dbar)                                                                                                                                                                                                                                        None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200908290820322009082908203220090829082032200908290948402009082909484020090829094840201002260000002010022600000020100226000000  JA  ARFMdecpA14                                                                 20090816065640  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090816065641  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090816065641  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090816065642  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090816065643  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090816065643  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090816065643  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090816065643  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090816065643  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090816070006                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090820005850  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090820005942  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090820005943  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090820005943  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090820005944  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090820005944  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090820005945  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090820005945  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090820005945  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090820010409                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090819172424  CV  DAT$            G�O�G�O�F�!�                JM  ARCAJMQC1.0                                                                 20090829082032  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090829082032  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090829094840  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100226000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100301000947  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100301001028                      G�O�G�O�G�O�                