CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   D   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  5    PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5D   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6T   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  7d   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       :   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  ;   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;`   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  <p   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       <�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  =�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C�   CALIBRATION_DATE            	             
_FillValue                  ,  F�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G$   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G(   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G,   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G0   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Gp   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         G�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         G�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G�Argo profile    2.2 1.2 19500101000000  5900657 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               �A   JA  20091006065415  20091010040703                                  2B  A   937                                                             846 @�Q	33331   @�Q	W��G@,����@`[Ƨ1   ARGOS   A   A   A   @�33AffA�33A�  B33BE��BnffB���B�33B���B�  B�ffB�ffC� C�C�fCL�C)L�C3� C=�3CG� C[  Co33C�� C���C�� C��fC�� C��3C���Cǳ3Cр C�s3C�fC�fC���D�3D� D� D��D��D�3D�3D$��D)�fD.�3D3� D8�fD=��DB��DG�fDN�DTY�DZ��D`�3Dg  DmS3Ds�3Dy�3D�)�D�i�D��fD�ٚD�,�D�` D��fD�|�D��311111111111111111111111111111111111111111111111111111111111111111111@�  A��A�ffA�33B��BE34Bn  B�fgB�  B���B���B�33B�33CffC  C��C33C)33C3ffC=��CGffCZ�fCo�C��3C�� C��3C���C��3C��fC�� CǦfC�s3C�ffC噙CC���D��DٚDٚD�gD�4D��D��D$�gD)� D.��D3��D8� D=�4DB�4DG� DNgDTS4DZ�4D`��Df��DmL�Ds��Dy��D�&gD�fgD��3D��gD�)�D�\�D��3D�y�D�� 11111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�I�A���A�!A�7A���A�A� �A݃AܓuA۶FAڸRA���A׮AҸRA��/A�oA�+A���A��A�  A�ĜA�$�A��7A��TAq��AT�AF1A:�DA/K�A!��A�;A^5A��@�"�@�33@ۍP@�G�@��@�p�@��
@��@�J@�z�@���@���@�V@�"�@�"�@��@��`@��!@�5?@��H@�-@�$�@�$�@~�y@y��@u�-@nE�@b��@XĜ@O��@G+@@��@4z�@*M�@*M�11111111111111111111111111111111111111111111111111111111111111111111A�I�A���A�!A�7A���A�A� �A݃AܓuA۶FAڸRA���A׮AҸRA��/A�oA�+A���A��A�  A�ĜA�$�A��7A��TAq��AT�AF1A:�DA/K�A!��A�;A^5A��@�"�@�33@ۍP@�G�@��@�p�@��
@��@�J@�z�@���@���@�V@�"�@�"�@��@��`@��!@�5?@��H@�-@�$�@�$�@~�y@y��@u�-@nE�@b��@XĜ@O��@G+@@��@4z�@*M�@*M�11111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	D�B	E�B	D�B	D�B	�fB
�'B��BhsBl�Bp�Bs�Bt�Bo�B^5BE�B�B�B�BB�B��BG�BB
��B
=qB	�jB	�PB	cTB	=qB	PB	B	:^B	6FB��B	B	�B	O�B	t�B	�=B	��B	�^B	ɺB	��B	�BB	�B	��B	��B

=B
PB
�B
�B
"�B
&�B
-B
1'B
6FB
9XB
;dB
=qB
@�B
G�B
M�B
Q�B
VB
ZB
bNB
jB
j11111111111111111111111111111111111111111111111111111111111111111111B	D�B	E�B	D�B	D�B	�fB
�'B��BhsBl�Bp�Bs�Bt�Bo�B^5BE�B�B�B�BB�B��BG�BB
��B
=qB	�jB	�PB	cTB	=qB	PB	B	:^B	6FB��B	B	�B	O�B	t�B	�=B	��B	�^B	ɺB	��B	�BB	�B	��B	��B

=B
PB
�B
�B
"�B
&�B
-B
1'B
6FB
9XB
;dB
=qB
@�B
G�B
M�B
Q�B
VB
ZB
bNB
jB
j11111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA4_a                                                                20091006065413  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091006065415  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091006065415  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091006065415  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091006065416  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091006065417  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091006065417  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091006065417  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091006065417  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091006065417  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091006070107                      G�O�G�O�G�O�                JA  ARFMdecpA4_a                                                                20091010035908  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091010040314  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091010040314  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091010040315  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091010040315  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091010040316  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091010040316  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091010040316  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091010040316  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091010040316  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091010040703                      G�O�G�O�G�O�                