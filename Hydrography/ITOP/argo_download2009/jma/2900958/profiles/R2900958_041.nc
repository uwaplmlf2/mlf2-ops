CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   G   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       4    PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5d   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9    TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9H   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       :d   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       =,   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >H   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >x   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Ax   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Dx   CALIBRATION_DATE            	             
_FillValue                  ,  Gx   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    H   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    H   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         H   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         H   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        H    HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    H$Argo profile    2.2 1.2 19500101000000  2900958 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               )A   JA  20090810185929  20090814130037                                  2B  A   APEX-SBE 4254                                                   846 @�B�
=p�1   @�B�L���@=a�7Kƨ@b��/��1   ARGOS   A   A   A   A+33A���A�33B"��BF��BpffB�ffB���B�ffB���B�  B�  CffC�3C�fC��C*  C433C>� CHL�C\� Cp� C�L�C���C��3C�@ C�&fC�L�C�L�C��3C��3C�� C�  C��C�33D��D�fD� D�fD�D�D �D%fD)�fD.�3D4&fD9  D>  DB��DHfDN9�DS�3DZ� Da�DgFfDm��Ds� Dy��D�I�D��fD���D�3D�<�D�y�D�fD�p D���D�vfD�	�D퉚D�I�11111111111111111111111111111111111111111111111111111111111111111111111 A(  A�33A���B"  BF  Bo��B�  B�fgB�  B�34Bܙ�BC33C� C�3C��C)��C4  C>L�CH�C\L�CpL�C�33C��3C�ٙC�&fC��C�33C�33C�ٙC�ٙCۦfC��fC��3C��D��DٙD�3DٙD�D�D �D$��D)ٙD.�fD4�D8�3D>3DB��DG��DN,�DS�fDZ�3Da�Dg9�Dm� Ds�3Dy� D�C4D�� D��gD��D�6gD�s4D�  D�i�D��gD�p D�4D�4D�C411111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�&�A�wA�
=A�z�A��9A��9A�  A�bA���A�&�A���A�l�A��A���A���A�;dA��^A�-A���A�I�A���A��A�\)A��uA� �A�ȴA|�Ap�/Ac��A]�
AY`BAUC�AH��A@�A6�9A.9XA&�uA��A�A��@��@�V@�S�@���@�ȴ@��u@��+@���@���@�dZ@�+@��w@�/@�(�@�A�@���@�O�@|�/@j=q@b�@W��@SC�@J��@Ahs@2=q@%��@~�@�\@	��@I�?�V11111111111111111111111111111111111111111111111111111111111111111111111 A�&�A�wA�
=A�z�A��9A��9A�  A�bA���A�&�A���A�l�A��A���A���A�;dA��^A�-A���A�I�A���A��A�\)A��uA� �A�ȴA|�Ap�/Ac��A]�
AY`BAUC�AH��A@�A6�9A.9XA&�uA��A�A��@��@�V@�S�@���@�ȴ@��u@��+@���@���@�dZ@�+@��w@�/@�(�@�A�@���@�O�@|�/@j=q@b�@W��@SC�@J��@Ahs@2=q@%��@~�@�\@	��@I�?�V11111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
DB
uB
%�B
gmB
��B
��B
�}B
��B
�B�B
�B
�TB
�qB
�wB
�jB
�dB
ĜB
��B
��B
��B
�B
�;B
�/B
�B
��B
�?B
��B
^5B
$�B
DB	��B	�NB	�B	�B	T�B	1'B	VB�B�wB�PB[#BN�B8RB,B�B�B"�B@�BO�B_;Br�B�\B��B�wB��B�fB��B	VB	C�B	bNB	q�B	�JB	��B	�wB	�`B
  B
�B
2-B
F�B
XB
e`11111111111111111111111111111111111111111111111111111111111111111111111 B
DB
uB
%�B
gmB
��B
��B
�}B
��B
�B�B
�B
�TB
�qB
�wB
�jB
�dB
ĜB
��B
��B
��B
�B
�;B
�/B
�B
��B
�?B
��B
^5B
$�B
DB	��B	�NB	�B	�B	T�B	1'B	VB�B�wB�PB[#BN�B8RB,B�B�B"�B@�BO�B_;Br�B�\B��B�wB��B�fB��B	VB	C�B	bNB	q�B	�JB	��B	�wB	�`B
  B
�B
2-B
F�B
XB
e`11111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090810185928  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090810185929  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090810185930  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090810185930  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090810185931  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090810185931  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090810185931  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090810185931  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090810185932  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090810190655                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090814125603  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090814125655  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090814125655  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090814125656  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090814125657  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090814125657  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090814125657  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090814125657  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090814125657  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090814130037                      G�O�G�O�G�O�                