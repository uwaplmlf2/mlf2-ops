CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900684 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               dA   JA  20091021035636  20091024220449                                  2B  A   APEX-SBE 2810                                                   846 @�T�+�1   @�T�)&N!@;%�S���@b��E���1   ARGOS   A   A   A   @���A  Ai��A���A���A�ffB��B��B333BF��B[33Bm��B���B���B���B�33B�ffB�  B���Bƙ�B�ffBڙ�B���B���B�  C�C�C�C� C33CffCL�C$ffC)� C.� C3��C8� C=ffCB� CGffCQ33C[33CeL�Co33CyL�C��3C���C���C���C��3C���C�ٚC�� C���C�� C��3C���C��fC CǙ�Č�Cљ�C֦fC�s3C�3C�fC�ffC�� C��fC�� D�3D�fD� D�3D��D��D�fD$ٚD)��D.��D3�fD8�3D=�3DB�3DG� DL�3DQ�3DV��D[�3D`�3De��Dj� DoٚDt� Dy�fD�#3D�\�D��3D��3D�)�D�p D���D���D�#3D�i�D��fD���D�)�D�Y�Dڠ D�� D�#3D�Y�D�3D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA��AffgA�33A�33A���B
��B��B2ffBF  BZffBl��B�fgB�fgB�34B���B�  B���B�34B�34B�  B�34B�fgB�fgB���C �gC�gC
�gCL�C  C33C�C$33C)L�C.L�C3fgC8L�C=33CBL�CG33CQ  C[  Ce�Co  Cy�C���C�s3C�� C�� C���C�s3C�� C��fC�� C��fC���C�� C���C�ffCǀ C�s3Cр C֌�C�Y�C���C��C�L�C�fC��C��fD�fD��D�3D�fD��D��DٙD$��D)� D.� D3��D8�fD=�fDB�fDG�3DL�fDQ�fDV��D[�fD`�fDe��Dj�3Do��Dt�3Dy��D��D�VgD���D���D�#4D�i�D��4D��gD��D�c4D�� D��gD�#4D�S4Dڙ�D�ٚD��D�S4D��D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�^A�RA�A�|�A�S�A���A۝�A�;dA��
A��TA�  A�$�A�S�A��#A��uA�5?A��A�VA��+A�33A��HA�jA�n�A�^5A��mA��HA�A���A�/A�A��A�O�A��A�S�A��TA��7A�?}A�jA�oA��A�XA�{A�33A��mA���A�K�A�G�A��`A��A�A~=qAzffAw�mAs��Aq�hAo��An�uAi��Ad��A`JAZ�jAV�jAS��AQ`BALM�AE��A@ĜA9�-A733A3"�A&1'A -AdZA5?A r�@���@�@�C�@�S�@�r�@�{@�%@���@��`@���@�hs@��y@��u@�@��@�I�@���@��@t��@qx�@c��@V@M�-@B�@9G�@1%@*=q@#�m@ �u@Z@r�@��@�@�
@�;@��?�|�?�v�?��?�b1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�^A�RA�A�|�A�S�A���A۝�A�;dA��
A��TA�  A�$�A�S�A��#A��uA�5?A��A�VA��+A�33A��HA�jA�n�A�^5A��mA��HA�A���A�/A�A��A�O�A��A�S�A��TA��7A�?}A�jA�oA��A�XA�{A�33A��mA���A�K�A�G�A��`A��A�A~=qAzffAw�mAs��Aq�hAo��An�uAi��Ad��A`JAZ�jAV�jAS��AQ`BALM�AE��A@ĜA9�-A733A3"�A&1'A -AdZA5?A r�@���@�@�C�@�S�@�r�@�{@�%@���@��`@���@�hs@��y@��u@�@��@�I�@���@��@t��@qx�@c��@V@M�-@B�@9G�@1%@*=q@#�m@ �u@Z@r�@��@�@�
@�;@��?�|�?�v�?��?�b1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B
%B
\B
�B
%�B
��B
�PB
��B
�^B
�?B
��B
ɺB
��B
��B
�TB
�B
��B
��B
�B
�B
�B
��B  B  BB
��B
�B
�B
�`B
�fB
�B
��B
��B
��B
�B
�B
�B
�sB
�`B
��B
��B
��B
��B
�B
�B
�HB
�B
ƨB
�!B
��B
}�B
p�B
^5B
T�B
M�B
F�B
0!B
�B
B	�B	�HB	�B	ɺB	�3B	��B	�B	bNB	W
B	A�B	1B�B�LB�hBhsBW
BJ�B@�B=qB<jB>wBO�B\)BbNBjB��B�B��B�NB�fB�yB�B	PB	+B	33B	VB	� B	��B	�RB	��B	�NB	�B
B
\B
�B
"�B
2-B
;dB
B�B
L�B
ZB
aHB
cTB
hsB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	��B
%B
\B
�B
%�B
��B
�PB
��B
�^B
�?B
��B
ɺB
��B
��B
�TB
�B
��B
��B
�B
�B
�B
��B  B  BB
��B
�B
�B
�`B
�fB
�B
��B
��B
��B
�B
�B
�B
�sB
�`B
��B
��B
��B
��B
�B
�B
�HB
�B
ƨB
�!B
��B
}�B
p�B
^5B
T�B
M�B
F�B
0!B
�B
B	�B	�HB	�B	ɺB	�3B	��B	�B	bNB	W
B	A�B	1B�B�LB�hBhsBW
BJ�B@�B=qB<jB>wBO�B\)BbNBjB��B�B��B�NB�fB�yB�B	PB	+B	33B	VB	� B	��B	�RB	��B	�NB	�B
B
\B
�B
"�B
2-B
;dB
B�B
L�B
ZB
aHB
cTB
hsB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091021035635  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091021035636  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091021035636  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091021035637  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091021035637  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091021035638  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091021035638  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091021035638  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091021035638  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091021035638  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091021040416                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091024215536  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091024215835  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091024215835  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091024215835  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091024215836  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091024215837  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091024215837  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091024215837  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091024215837  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091024215837  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091024220449                      G�O�G�O�G�O�                