CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900684 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ]A   JA  20090812065638  20090815220325                                  2B  A   APEX-SBE 2810                                                   846 @�C5��6�1   @�C5�З�@;��hr�@b� ě��1   ARGOS   A   A   A   @�  A��Ad��A�  A���A���B
ffB��B2��BE��BX  BlffB���B�  B���B���B���B�33B�33B���BЙ�Bڙ�B�  B���B�33CL�CL�C� C33C  C� CL�C$�C)�C.  C3  C8� C=��CB� CG��CQ� C[  Ce� CoL�Cy33C���C���C���C��fC�� C���C��fC�Y�C�Y�C�ffC���C��fC���C�� CǦfČ�Cѳ3Cֳ3Cۀ C�� C�Y�C�3C�fC��3C���D�fDٚD� D�fDٚD�3D��D$�fD)�3D.�3D3ٚD8ٚD=�3DBٚDG�3DL�3DQ� DV�fD[��D`��De� Dj��Do��Dt� Dy�fD�  D�Y�D��fD���D�)�D�i�D�� D�� D��D�ffD�� D���D�&fD�p Dڬ�D��3D�&fD�\�D� D�S31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA��A`  A���A�fgA�fgB	33BfgB1��BDfgBV��Bk33B�33B�ffB�  B�  B�  B���B���B�33B�  B�  B�ffB�33B���C  C  C33C�fC�3C33C  C#��C(��C-�3C2�3C833C=L�CB33CGL�CQ33CZ�3Ce33Co  Cx�fC�s4C�fgC�s4C�� C�Y�C�s4C�� C�34C�34C�@ C�s4C�� C��gC�Cǀ C�fgCь�C֌�C�Y�C�Y�C�34C��C� C��C�s4D�3D�gD��D�3D�gD� D��D$�3D)� D.� D3�gD8�gD=� DB�gDG� DL� DQ��DV�3D[��D`�gDe��Dj��Do��Dt��Dy�3D�fD�P D���D��3D�  D�` D��fD��fD� D�\�D��fD��3D��D�ffDڣ3D�ٙD��D�S3D�fD�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�JA�z�A�\)A��Aߝ�A�?}A�jA��PA�A��A��\A���A�O�A��7A��TA�{A�A�A��DA��A���A��wA�ĜA���A�n�A��A�7LA��jA�A�A�oA�~�A��A��A���A�dZA���A��A���A�-A�ĜA�JA�^5A���A�1'A�^5A�hsA���A���A���A���A�&�A���A}`BAxz�Au�#As�#Ap-An1Aj�!Ae�#A^ȴAZE�AU?}ARĜAPQ�AKK�AF��AAt�A=�TA6I�A0ffA#p�AjA��A��@�7L@�dZ@ڗ�@�O�@�@��R@��u@��#@��7@�dZ@���@��@���@��D@�A�@�O�@��!@���@�p�@|�@z=q@mp�@a�#@X  @R�@H1'@<�j@7\)@/�P@(�@"��@�j@�@�D@�+@
n�@b@��@G�?��?��u1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�JA�z�A�\)A��Aߝ�A�?}A�jA��PA�A��A��\A���A�O�A��7A��TA�{A�A�A��DA��A���A��wA�ĜA���A�n�A��A�7LA��jA�A�A�oA�~�A��A��A���A�dZA���A��A���A�-A�ĜA�JA�^5A���A�1'A�^5A�hsA���A���A���A���A�&�A���A}`BAxz�Au�#As�#Ap-An1Aj�!Ae�#A^ȴAZE�AU?}ARĜAPQ�AKK�AF��AAt�A=�TA6I�A0ffA#p�AjA��A��@�7L@�dZ@ڗ�@�O�@�@��R@��u@��#@��7@�dZ@���@��@���@��D@�A�@�O�@��!@���@�p�@|�@z=q@mp�@a�#@X  @R�@H1'@<�j@7\)@/�P@(�@"��@�j@�@�D@�+@
n�@b@��@G�?��?��u1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
49B
,B
\B
\B
�B
'�B
{�B
}�B
�oB
�\B
��B
��B
�9B
��B
�NB
�B
��BB1B	7BDBPB\B�BJB%B
��B
�BB	7BBB
��B
��B
��B
��B
��B
�B
�yB
�BB
�mB
�B
�B
�B
�yB
�NB
�#B
��B
��B
�FB
��B
�oB
|�B
p�B
gmB
T�B
I�B
:^B
$�B
1B	�B	�/B	��B	ĜB	�!B	��B	�%B	t�B	R�B	7LB��B�
B�B�1BgmBM�B<jB5?B2-B.B49B@�B\)Bl�B|�B�=B��B�?B��B�HB�B��B	%B	�B	 �B	G�B	l�B	�B	��B	�'B	ŢB	��B	�TB	�B	��B
\B
�B
-B
<jB
F�B
L�B
T�B
^5B
bNB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
49B
,B
\B
\B
�B
'�B
{�B
}�B
�oB
�\B
��B
��B
�9B
��B
�NB
�B
��BB1B	7BDBPB\B�BJB%B
��B
�BB	7BBB
��B
��B
��B
��B
��B
�B
�yB
�BB
�mB
�B
�B
�B
�yB
�NB
�#B
��B
��B
�FB
��B
�oB
|�B
p�B
gmB
T�B
I�B
:^B
$�B
1B	�B	�/B	��B	ĜB	�!B	��B	�%B	t�B	R�B	7LB��B�
B�B�1BgmBM�B<jB5?B2-B.B49B@�B\)Bl�B|�B�=B��B�?B��B�HB�B��B	%B	�B	 �B	G�B	l�B	�B	��B	�'B	ŢB	��B	�TB	�B	��B
\B
�B
-B
<jB
F�B
L�B
T�B
^5B
bNB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090812065637  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090812065638  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090812065639  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090812065639  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090812065639  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090812065640  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090812065640  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090812065640  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090812065640  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090812065640  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090812070341                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090815215542  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090815215811  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090815215812  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090815215812  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090815215812  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090815215813  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090815215813  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090815215813  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090815215813  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090815215814  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090815220325                      G�O�G�O�G�O�                