CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   i   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J   CALIBRATION_DATE            	             
_FillValue                  ,  M   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    MD   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    MH   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    ML   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    MP   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  MT   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  2900684 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               _A   JA  20090902095521  20091209003000                                  2B  A   APEX-SBE 2810                                                   846 @�H3N��1   @�H5C��@;H1&�x�@b���vȴ1   ARGOS   A   A   A   @���AffAd��A���A���A�33B33B��B2  BFffBY��Bm��B�  B���B���B���B�ffB�ffB�ffBƙ�B�33B�ffB���BB���CL�CffC�C)L�C.33C3�C7�fC=ffCB�CG  CQffCZ�3Cd��Cn��CyffC��3C��3C�� C���C���C�� C�s3C���C�� C���C���C�� C��3C³3C�� C̦fCѦfCֳ3Cۀ C�fC�fC�s3C�s3C��3C��fD�3D�fDٚD��DٚD�3D��D$ٚD)ٚD.��D3� D8�3D=��DB�fDG�3DL��Dj�3Do�fDt� Dy��D�  D�l�D���D��fD��D�Y�D���D��D��D�l�D�� D���D��D�ffDڦfD��3D�)�D�` D�D�Ff111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�fgA33Aa��A�  A�33A홙B
ffB  B133BE��BX��Bl��B���B�34B�fgB�fgB�  B�  B�  B�34B���B�  B�fgB�34B�34C�C33C
�gC)�C.  C2�gC7�3C=33CA�gCF��CQ33CZ� Cd��Cn��Cy33C���C���C��fC�� C�� C�ffC�Y�C�� C�ffC�� C�� C�ffC���C�CǦfČ�Cь�C֙�C�ffC���C��C�Y�C�Y�C���C���D�fDٙD��D� D��D�fD��D$��D)��D.��D3�3D8�fD=��DB��DG�fDL� Dj�fDo��Dt�3Dy� D��D�fgD��gD�� D�gD�S4D��gD��4D�gD�fgD���D��gD�gD�` Dڠ D���D�#4D�Y�D�4D�@ 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�=qA�?}A�?}A�C�A�E�A�/A�n�A�bA��TA�M�A�1'A�hsA���A�x�A��A��mA�{A���A��/A��A�`BA���A��mA���A�  A���A��TA��A���A�z�A�&�A��9A�=qA��!A�;dA�;dA���A�bNA��A�~�A�&�A�ȴA���A�bNA��A�E�A��A�mA"�A|�DAx�Aul�Ap�9Am7LAi?}Ac�A]l�AY/AV �AO�AJ��AG`BADffA?��A8VA)+A��AZA�@��@�(�@���@�z�@�$�@�@�{@�~�@�(�@�V@��F@�
=@�@x��@sdZ@k�F@\z�@T�D@P  @J~�@=`B@7��@1�@( �@"�@@^5@|�@�@5?@	�^@5?@C�@ bN?�C�?�ȴ111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�=qA�?}A�?}A�C�A�E�A�/A�n�A�bA��TA�M�A�1'A�hsA���A�x�A��A��mA�{A���A��/A��A�`BA���A��mA���A�  A���A��TA��A���A�z�A�&�A��9A�=qA��!A�;dA�;dA���A�bNA��A�~�A�&�A�ȴA���A�bNA��A�E�A��A�mA"�A|�DAx�Aul�Ap�9Am7LAi?}Ac�A]l�AY/AV �AO�AJ��AG`BADffA?��A8VA)+A��AZA�@��@�(�@���@�z�@�$�@�@�{@�~�@�(�@�V@��F@�
=@�@x��@sdZ@k�F@\z�@T�D@P  @J~�@=`B@7��@1�@( �@"�@@^5@|�@�@5?@	�^@5?@C�@ bN?�C�?�ȴ111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B	��B	��B	��B
1B
Q�B
�B
��B
��B
�B
�B
��B
��B
��B
��B
�B
�B
�TB
�BB	7BbBBB\BuB
=BBBVBbB	7B+BB
��B
�B
��B
��B
��B
�B
�B
�BB
��B
ǮB
�RB
�B
��B
��B
�VB
|�B
m�B
XB
I�B
5?B
�B
B	�B	�BB	ŢB	�'B	��B	�uB	{�B	[#B	�B�
B��B� BcTBP�BF�BB�B>wB=qB@�BM�Bn�B�+B��B�3B	bB	#�B	/B	:^B	e`B	�1B	��B	�B	ÖB	��B	�/B	�B
B
bB
�B
$�B
5?B
=qB
H�B
Q�B
YB
`BB
ffB
l�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	��B	��B	��B	��B	��B
1B
Q�B
�B
��B
��B
�B
�B
��B
��B
��B
��B
�B
�B
�TB
�BB	7BbBBB\BuB
=BBBVBbB	7B+BB
��B
�B
��B
��B
��B
�B
�B
�BB
��B
ǮB
�RB
�B
��B
��B
�VB
|�B
m�B
XB
I�B
5?B
�B
B	�B	�BB	ŢB	�'B	��B	�uB	{�B	[#B	�B�
B��B� BcTBP�BF�BB�B>wB=qB@�BM�Bn�B�+B��B�3B	bB	#�B	/B	:^B	e`B	�1B	��B	�B	ÖB	��B	�/B	�B
B
bB
�B
$�B
5?B
=qB
H�B
Q�B
YB
`BB
ffB
l�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090902095520  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090902095521  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090902095522  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090902095522  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090902095522  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090902095523  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090902095523  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090902095523  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090902095523  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090902095523  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090902095523  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090902095523  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090902100200                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090904215501  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090904215744  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090904215745  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090904215745  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090904215745  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090904215746  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090904215746  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090904215746  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090904215746  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090904215746  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090904215746  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090904215746  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090904220208                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208073152  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208073355  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208073356  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091208073356  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208073356  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208073358  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208073358  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208073358  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208073358  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208073358  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091209003000                      G�O�G�O�G�O�                