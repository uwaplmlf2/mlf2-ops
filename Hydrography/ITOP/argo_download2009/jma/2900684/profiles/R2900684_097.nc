CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900684 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               aA   JA  20090921095549  20090924220208                                  2B  A   APEX-SBE 2810                                                   846 @�M7�8!1   @�M8)���@:��;dZ@b��$�/1   ARGOS   A   A   A   @�ffA  Ah  A�  A�ffA홚B33B��B0��BE33BZffBnffB�33B�  B�  B�  B���B�ffB�ffB�ffB�  Bڙ�B䙚B���B�33CL�CL�C  C�fC�fC  C33C#�fC)�C.L�C3ffC7�fC<�fCBffCGL�CQ�C[� Ce33Co� CyffC���C��fC��3C���C���C���C���C���C���C���C�� C���C���C¦fCǙ�C̙�CѦfC֌�CۦfC���C��C� C�3C�� C���D��D� D�3D��D� DٚD� D$ٚD)�fD.� D3�fD8� D=�3DB�fDG�3DL�3DQ� DV� D[� D`�3De��DjٚDoٚDt� Dy�3D�)�D�c3D���D��3D��D�i�D���D���D�)�D�p D��fD���D�#3D�p DڦfD��D�&fD�` D�fD�i�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��Aa��A���A�33A�fgB	��B  B/33BC��BX��Bl��B�ffB�33B�33B�33B�  B���B���Bř�B�33B���B���B�  B�ffC �gC�gC
��C� C� C��C��C#� C(�4C-�gC3  C7� C<� CB  CF�gCP�4C[�Cd��Co�Cy  C�Y�C�s3C�� C�fgC�Y�C�fgC�Y�C�fgC�fgC���C���C�fgC�fgC�s3C�fgC�fgC�s3C�Y�C�s3C�fgC�Y�C�L�C� C��C�Y�D�3D�fD��D�3D�fD� D�fD$� D)��D.�fD3��D8�fD=��DB��DG��DL��DQ�fDV�fD[�fD`��De�3Dj� Do� Dt�fDy��D��D�VfD���D��fD��D�\�D�� D�� D��D�c3D���D�� D�fD�c3Dڙ�D���D��D�S3D�D�\�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A�"�A�&�A�&�A�+A�/A�(�A��A���A�Q�A�l�A�/A���A�1A�%A���A���A��`A��#A��A�~�A��-A��
A�^5A��-A��wA��A���A�^5A�bA�ffA�G�A���A��yA��PA�ĜA�jA�oA��#A��A�\)A���A�XA�r�A��\A���A�l�A�ȴA��7A�-A}
=Ax1'As%An^5Ajr�Ae&�Ab�!A]p�AZffAVbAP�jAK&�AH  AC�mA<��A8��A0�DA,��A({A ȴAXAG�A�@��`@噚@�Ĝ@��@ÍP@�-@�=q@�M�@���@��@��@��#@�ȴ@���@�(�@�33@���@{33@t1@n�@ko@c�F@X  @T9X@Ihs@@r�@9�7@1��@*�!@#ƨ@ �u@{@��@t�@A�@�D@;d@9X@ ��?�/?�K�?�t�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A�"�A�&�A�&�A�+A�/A�(�A��A���A�Q�A�l�A�/A���A�1A�%A���A���A��`A��#A��A�~�A��-A��
A�^5A��-A��wA��A���A�^5A�bA�ffA�G�A���A��yA��PA�ĜA�jA�oA��#A��A�\)A���A�XA�r�A��\A���A�l�A�ȴA��7A�-A}
=Ax1'As%An^5Ajr�Ae&�Ab�!A]p�AZffAVbAP�jAK&�AH  AC�mA<��A8��A0�DA,��A({A ȴAXAG�A�@��`@噚@�Ĝ@��@ÍP@�-@�=q@�M�@���@��@��@��#@�ȴ@���@�(�@�33@���@{33@t1@n�@ko@c�F@X  @T9X@Ihs@@r�@9�7@1��@*�!@#ƨ@ �u@{@��@t�@A�@�D@;d@9X@ ��?�/?�K�?�t�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
]/B
]/B
]/B
]/B
]/B
\)B
XB
L�B
�mB
�B
�TB
�B
�B
�B{B0!B49B0!B+B�B�B�B
�B
�HB
�)B
�
B
�B
�B
��B
�B
��B
�B
�ZB
�`B
�B
�B
�B
�sB
�B
�fB
�NB
�HB
�B
�fB
�/B
��B
ƨB
�wB
�B
��B
�hB
y�B
`BB
L�B
:^B
$�B
�B
B	��B	�BB	ȴB	�3B	��B	�\B	q�B	_;B	;dB	'�B	�B�BǮB��B�\B_;BJ�BB�B6FB9XBC�BG�BS�B`BBu�B�B�7B��B�?B��B�`B	B	{B	,B	:^B	C�B	S�B	u�B	�DB	��B	��B	��B	�5B	�B
B
PB
�B
'�B
33B
;dB
E�B
O�B
XB
`BB
e`B
l�B
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
]/B
]/B
]/B
]/B
]/B
\)B
XB
L�B
�mB
�B
�TB
�B
�B
�B{B0!B49B0!B+B�B�B�B
�B
�HB
�)B
�
B
�B
�B
��B
�B
��B
�B
�ZB
�`B
�B
�B
�B
�sB
�B
�fB
�NB
�HB
�B
�fB
�/B
��B
ƨB
�wB
�B
��B
�hB
y�B
`BB
L�B
:^B
$�B
�B
B	��B	�BB	ȴB	�3B	��B	�\B	q�B	_;B	;dB	'�B	�B�BǮB��B�\B_;BJ�BB�B6FB9XBC�BG�BS�B`BBu�B�B�7B��B�?B��B�`B	B	{B	,B	:^B	C�B	S�B	u�B	�DB	��B	��B	��B	�5B	�B
B
PB
�B
'�B
33B
;dB
E�B
O�B
XB
`BB
e`B
l�B
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090921095548  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090921095549  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090921095549  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090921095549  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090921095550  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090921095551  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090921095551  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090921095551  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090921095551  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090921095551  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090921100202                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090924215536  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090924215751  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090924215752  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090924215752  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090924215753  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090924215755  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090924215755  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090924215755  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090924215755  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090924215755  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090924220208                      G�O�G�O�G�O�                