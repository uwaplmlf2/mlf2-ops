CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900684 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               cA   JA  20091011095533  20091014220114                                  2B  A   APEX-SBE 2810                                                   846 @�R4 �Q)1   @�R4Kx��@:��Q�@b����l�1   ARGOS   A   A   A   @���AffAh  A���A�33A�33B	33B��B2  BF  BZ  Bm33B�ffB�33B�33B���B�ffB���B���B�  B���B���B�ffB�  B���CL�CL�C  C�C33CL�C�C#�fC)  C-��C2��C7�fC=  CBffCG  CP�3CZ��CeffCoffCyffC�� C���C��3C��3C�� C��fC�s3C��fC��3C��3C�� C�� C�� C�Cǳ3C̳3Cѳ3C֌�Cۀ C�fC�s3C�fC���C��fC���D� D��D� D�fD�fD�fD� D$�fD)��D.�3D3�fD8ٚD=��DB��DG� DL��DQ�fDV�fD[�fD`�3De� Dj�fDo�3Dt� DyٚD�  D�ffD���D���D�&fD�ffD���D�� D�&fD�l�D��3D��fD�,�D�Y�Dڬ�D��3D�,�D�\�D�fD�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA33Ad��A�33A���A陙BffB  B133BE33BY33BlffB�  B���B���B�34B�  B�fgB�fgBƙ�B�fgB�fgB�  BB�34C�C�C
��C�gC  C�C�gC#�3C(��C-��C2��C7�3C<��CB33CF��CP� CZ��Ce33Co33Cy33C��fC�s3C���C���C��fC���C�Y�C���C���C���C��fC��fC��fC�s3CǙ�C̙�Cљ�C�s3C�ffC���C�Y�C��C�3C��C�s3D�3D� D�3D��D��DٙD�3D$��D)� D.�fD3��D8��D=� DB��DG�3DL� DQ��DV��D[��D`�fDe�3Dj��Do�fDt�3Dy��D��D�` D��4D��gD�  D�` D��gD�ٚD�  D�fgD���D�� D�&gD�S4DڦgD���D�&gD�VgD� D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�7LA�=qA�?}A�?}A�A�A�E�A�C�A�/A�A�A�7LA��wA�  A�Q�A��yA��A�VA��FA��RA���A��A�`BA��hA��HA��!A��A��^A��A�p�A��A�VA�ffA��A�ƨA�bA�n�A��PA�
=A��\A�ƨA�ZA���A��DA��`A��;A�l�A��7A�"�A�M�A��`A� �A�mA|  Aw;dAp��Ak�;Af��Act�A`��A]t�AXQ�AUhsAPjAK%AE��AA�mA>�yA6E�A2^5A1�A,I�A ��A��AXAx�@�\)@�-@�1@��@�S�@җ�@ēu@���@��j@�X@�t�@�C�@��@��`@�V@�E�@���@�"�@�@y�@p��@c�
@Vv�@O��@I��@@r�@8��@1�#@)��@#dZ@��@&�@O�@7L@�@	7L@�@�?�|�?��?�`B1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�7LA�=qA�?}A�?}A�A�A�E�A�C�A�/A�A�A�7LA��wA�  A�Q�A��yA��A�VA��FA��RA���A��A�`BA��hA��HA��!A��A��^A��A�p�A��A�VA�ffA��A�ƨA�bA�n�A��PA�
=A��\A�ƨA�ZA���A��DA��`A��;A�l�A��7A�"�A�M�A��`A� �A�mA|  Aw;dAp��Ak�;Af��Act�A`��A]t�AXQ�AUhsAPjAK%AE��AA�mA>�yA6E�A2^5A1�A,I�A ��A��AXAx�@�\)@�-@�1@��@�S�@җ�@ēu@���@��j@�X@�t�@�C�@��@��`@�V@�E�@���@�"�@�@y�@p��@c�
@Vv�@O��@I��@@r�@8��@1�#@)��@#dZ@��@&�@O�@7L@�@	7L@�@�?�|�?��?�`B1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
I�B
I�B
I�B
J�B
I�B
I�B
I�B
P�B
K�B
T�B
��B
�B
��B
�
B
��B
�
B
�B
�/B
�BB
�ZB
�NB
�fB
�B
��B
��B
��B
��B
��B
�B
�B
�B
�B
�B
�fB
�yB
�`B
�TB
�`B
�B
�B
�TB
�mB
��B
�B
�`B
�)B
ȴB
��B
�^B
��B
��B
�DB
s�B
VB
@�B
'�B
�B
\B
  B	�sB	�#B	ȴB	�B	��B	�B	t�B	K�B	:^B	5?B	�B�mB��B��Bm�B_;BT�BL�BH�B@�B9XB:^B<jBJ�BbNBv�B�+B�oB��B�3B�B�NB��B	PB	�B	49B	T�B	~�B	��B	�B	��B	��B	�;B	�B
B
�B
"�B
-B
7LB
A�B
K�B
VB
^5B
dZB
iyB
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
I�B
I�B
I�B
J�B
I�B
I�B
I�B
P�B
K�B
T�B
��B
�B
��B
�
B
��B
�
B
�B
�/B
�BB
�ZB
�NB
�fB
�B
��B
��B
��B
��B
��B
�B
�B
�B
�B
�B
�fB
�yB
�`B
�TB
�`B
�B
�B
�TB
�mB
��B
�B
�`B
�)B
ȴB
��B
�^B
��B
��B
�DB
s�B
VB
@�B
'�B
�B
\B
  B	�sB	�#B	ȴB	�B	��B	�B	t�B	K�B	:^B	5?B	�B�mB��B��Bm�B_;BT�BL�BH�B@�B9XB:^B<jBJ�BbNBv�B�+B�oB��B�3B�B�NB��B	PB	�B	49B	T�B	~�B	��B	�B	��B	��B	�;B	�B
B
�B
"�B
-B
7LB
A�B
K�B
VB
^5B
dZB
iyB
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091011095532  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091011095533  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091011095533  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091011095534  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091011095534  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091011095535  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091011095535  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091011095535  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091011095535  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091011095535  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091011100246                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091014215444  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091014215731  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091014215731  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091014215732  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091014215732  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091014215733  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091014215733  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091014215733  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091014215733  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091014215733  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091014220114                      G�O�G�O�G�O�                