CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   I   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D�   CALIBRATION_DATE            	             
_FillValue                  ,  G�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    H   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    H   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    H   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    H   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  H   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    HT   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Hd   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Hh   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Hx   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         H|   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        H�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    H�Argo profile    2.2 1.2 19500101000000  2900957 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               (A   JA  20090804215710  20090808160445                                  2B  A   APEX-SBE 4253                                                   846 @�AhZ��91   @�Ah���@;Y�"��`@a�r� Ĝ1   ARGOS   A   A   A   @�33A+33A�ffA�33B"��BJffBr��B�ffB���B���B�ffB�  B���C33C�fC� C L�C*�C4L�C=�fCG�3C[�fCp33C��fC��fC��3C��fC���C�ٚC�@ C��C�&fC�@ C��fC��C�� D�D3D�D�D� D,�D �D%  D)��D/3D4&fD9�D>  DC�DG��DNFfDT9�DZ�3Da&fDg` Dm�fDs�3Dy��D�C3D�vfD��fD���D�L�D��3D�fD���D�fD�y�D���D�3D��fD�` 1111111111111111111111111111111111111111111111111111111111111111111111111   @���A0  A���A���B$  BK��Bt  B�  B�fgB�fgB�  Bܙ�B�fgC� C33C��C ��C*fgC4��C>33CH  C\33Cp� C��C��C��C��C��3C�  C�ffC�@ C�L�C�ffC��C�33C��fD,�D&fD,�D  D�3D@ D ,�D%33D*�D/&fD49�D9,�D>33DC,�DH�DNY�DTL�DZ�fDa9�Dgs3Dm��Ds�fDz�D�L�D�� D�� D�4D�VgD���D� D��gD� Dԃ4D�4D��D�� D�i�1111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A���A�hA�x�A�
=A�ffA���A�+A���A��+A���A���A�r�A�K�A�G�A�XA��uA�A�"�A�bA��!A�A�A�VA�z�A�JA���A��jA�p�A|jAv�RAm�AdQ�A[�
AW+AMAK&�A9dZA,��A"bNA�-A�A
��@��@�@�%@�/@���@���@�C�@�V@���@�l�@���@�o@��@��;@�+@z�H@t�/@c�@T��@OK�@IG�@@�@9��@)X@��@V@V@��?�\)?��T?��j1111111111111111111111111111111111111111111111111111111111111111111111111   A���A���A�hA�x�A�
=A�ffA���A�+A���A��+A���A���A�r�A�K�A�G�A�XA��uA�A�"�A�bA��!A�A�A�VA�z�A�JA���A��jA�p�A|jAv�RAm�AdQ�A[�
AW+AMAK&�A9dZA,��A"bNA�-A�A
��@��@�@�%@�/@���@���@�C�@�V@���@�l�@���@�o@��@��;@�+@z�H@t�/@c�@T��@OK�@IG�@@�@9��@)X@��@V@V@��?�\)?��T?��j1111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�RB
�FB
��B
^5B
�B
�B
��B
ŢB
��B
�B
�BB
�#B
�B
�BB
�`B
�fB
�mB
�yB
��B
��B
�B
��B
�B
�TB
��B
�yB
��B
�3B
��B
~�B
Q�B
+B
B	�B	ƨB	�XB	o�B	5?B		7B�yB��B�B��B��B�DB~�Bn�B�B�hB��B��BĜB�)B�`B�sB	+B	�B	1'B	<jB	hsB	�DB	��B	��B	�wB	��B	�B
uB
)�B
<jB
S�B
cTB
q�B
r�1111111111111111111111111111111111111111111111111111111111111111111111111   B
�RB
�FB
��B
^5B
�B
�B
��B
ŢB
��B
�B
�BB
�#B
�B
�BB
�`B
�fB
�mB
�yB
��B
��B
�B
��B
�B
�TB
��B
�yB
��B
�3B
��B
~�B
Q�B
+B
B	�B	ƨB	�XB	o�B	5?B		7B�yB��B�B��B��B�DB~�Bn�B�B�hB��B��BĜB�)B�`B�sB	+B	�B	1'B	<jB	hsB	�DB	��B	��B	�wB	��B	�B
uB
)�B
<jB
S�B
cTB
q�B
r�1111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090804215708  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090804215710  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090804215710  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090804215710  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090804215712  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090804215712  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090804215712  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090804215712  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090804215712  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090804220211                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090808155812  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090808155932  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090808155933  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090808155933  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090808155934  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090808155934  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090808155934  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090808155934  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090808155934  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090808160445                      G�O�G�O�G�O�                