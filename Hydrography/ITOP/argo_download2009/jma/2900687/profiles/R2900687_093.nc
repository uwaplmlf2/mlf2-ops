CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900687 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ]A   JA  20090905005455  20090908190201                                  2B  A   APEX-SBE 2968                                                   846 @�I2OC�-1   @�I2�/�c@:&fffff@b6V�u1   ARGOS   A   A   A   @�  A  Ac33A���A�33A�33B
ffB��B2  BFffBX��Bl��B�33B�  B�  B�ffB�ffB�  B�33B���B�ffBڙ�B�33BB���C��C��C33C� CL�CL�CL�C$  C)ffC-��C3�C8ffC=33CB  CF�fCP��C[L�Ce��CoffCyffC���C�� C�� C�� C�� C�� C�� C���C���C��3C���C��fC�� C³3CǦfC̙�Cѳ3C�s3Cۀ C���C�ffC�fC�ffC�ffC��fDٚD�fD�3D�3D��D��D� D$ٚD)��D.��D3ٚD8�fD=� DBٚDG�fDL�fDQ� DV�fD[ٚD`� De�3Dj��DoٚDt�3Dy� D��D�l�D�� D��fD��D�ffD�� D��fD�,�D�` D���D���D��D�\�DڦfD���D�&fD�VfD�fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA33A^ffA�34A���A���B	33B��B0��BE33BW��Bk��B���B�ffB�ffB���B���B�ffB���B�33B���B�  B㙙B�  B�33CL�CL�C
�fC33C  C  C  C#�3C)�C-� C2��C8�C<�fCA�3CF��CP� C[  CeL�Co�Cy�C�s4C�Y�C�Y�C���C���C���C�Y�C��gC��gC���C�fgC�� C���C�Cǀ C�s4Cь�C�L�C�Y�C�fgC�@ C� C�@ C�@ C�� D�gD�3D� D� D�gDٚD��D$�gD)��D.��D3�gD8�3D=��DB�gDG�3DL�3DQ��DV�3D[�gD`��De� Dj�gDo�gDt� Dy��D�3D�c3D��fD���D� D�\�D��fD���D�#3D�VfD�� D��3D�3D�S3Dڜ�D��3D��D�L�D��D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�A�A�%A�%A��mA�&�A�{AA�K�A�VA��DA�9XA��A��A�ffA���A�$�A�r�A�VA��
A��^A��^A�%A��^A���A���A���A��\A�9XA��A��wA�33A��TA�`BA�~�A��
A���A�G�A�1'A���A�z�A��A�A�7LA��TA�O�A�  A�9XA��!A��!A|=qAzJAt��AqhsAm\)Aj��Ad��A_t�AX��AVn�AN  AK��AE
=A@M�A>�DA:bA6�/A5G�A1A)S�A#��A��AƨAv�@�Z@�@ڰ!@��@ǍP@��-@�?}@��^@�&�@��^@�@�Q�@�E�@��@��y@���@��P@�1'@�;@|1@t�/@hQ�@Yhs@PĜ@F��@@Q�@:J@1��@*-@!�#@��@��@��@�-@	hs@
=@�^?��?�dZ?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A�A�A�%A�%A��mA�&�A�{AA�K�A�VA��DA�9XA��A��A�ffA���A�$�A�r�A�VA��
A��^A��^A�%A��^A���A���A���A��\A�9XA��A��wA�33A��TA�`BA�~�A��
A���A�G�A�1'A���A�z�A��A�A�7LA��TA�O�A�  A�9XA��!A��!A|=qAzJAt��AqhsAm\)Aj��Ad��A_t�AX��AVn�AN  AK��AE
=A@M�A>�DA:bA6�/A5G�A1A)S�A#��A��AƨAv�@�Z@�@ڰ!@��@ǍP@��-@�?}@��^@�&�@��^@�@�Q�@�E�@��@��y@���@��P@�1'@�;@|1@t�/@hQ�@Yhs@PĜ@F��@@Q�@:J@1��@*-@!�#@��@��@��@�-@	hs@
=@�^?��?�dZ?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
jB
jB
k�B
jB
jB
e`B"�B!�B�B�B.B<jB33B�BhB
=BB
��B
��B
��B
��B
��B
�B
�B
�B
��B
��B
��B  B
=B�B�B�B�B�B�B �B(�B&�B%�B"�B�BuB%B
��B
�B
�NB
�B
ƨB
�LB
�B
�{B
�+B
n�B
^5B
J�B
<jB
"�B
B	�B	�5B	�XB	�B	�PB	x�B	o�B	[#B	M�B	D�B	0!B	JB�B��B��Bu�B[#BI�BD�B@�BA�BF�BP�B^5BjB�B�bB��B�-BB�
B�fB��B	hB	(�B	33B	F�B	hsB	�bB	��B	��B	��B	�HB	��B
B
{B
 �B
+B
33B
?}B
J�B
O�B
\)B
bNB
ffB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
jB
jB
k�B
jB
jB
e`B"�B!�B�B�B.B<jB33B�BhB
=BB
��B
��B
��B
��B
��B
�B
�B
�B
��B
��B
��B  B
=B�B�B�B�B�B�B �B(�B&�B%�B"�B�BuB%B
��B
�B
�NB
�B
ƨB
�LB
�B
�{B
�+B
n�B
^5B
J�B
<jB
"�B
B	�B	�5B	�XB	�B	�PB	x�B	o�B	[#B	M�B	D�B	0!B	JB�B��B��Bu�B[#BI�BD�B@�BA�BF�BP�B^5BjB�B�bB��B�-BB�
B�fB��B	hB	(�B	33B	F�B	hsB	�bB	��B	��B	��B	�HB	��B
B
{B
 �B
+B
33B
?}B
J�B
O�B
\)B
bNB
ffB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090905005454  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090905005455  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090905005455  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090905005455  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090905005456  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090905005457  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090905005457  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090905005457  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090905005457  QCF$                G�O�G�O�G�O�             300JA  ARGQaqcp2.8b                                                                20090905005457  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090905005457  QCF$                G�O�G�O�G�O�             300JA  ARGQrqcpt16b                                                                20090905005457  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090905005958                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090908185623  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090908185843  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090908185843  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090908185843  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090908185844  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090908185845  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090908185845  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090908185845  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090908185845  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090908185845  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090908190201                      G�O�G�O�G�O�                