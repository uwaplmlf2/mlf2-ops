CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   d   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  4,   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  6    PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  9�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  ;�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  ?   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  A   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    C4   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    F4   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    I4   CALIBRATION_DATE            	             
_FillValue                  ,  L4   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    L`   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Ld   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Lh   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Ll   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Lp   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    L�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    L�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    L�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         L�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         L�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        L�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    L�Argo profile    2.2 1.2 19500101000000  2900687 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ^A   JA  20090915005551  20090918190251                                  2B  A   APEX-SBE 2968                                                   846 @�K�R��d1   @�K��Յ�@:���o@b/t�j~�1   ARGOS   A   A   A   @���A  AfffA���A���A�ffB33B��B2ffBFffBZ  Bm��B�33B�  B���B�33B�33B���B�33B�33B�  B�  B�ffB홚B���C ��C33CL�C� C��CL�C��C$ffC)ffC.��C3L�C8�C=� CB�CG� CQ33CZ��Ce33Co�Cy�C�� C��3C���C��3C��3C���C�s3C�s3C���C���C�� C�ffC���C�CǙ�Č�Cљ�C֦fCۙ�C�� C�fC�fC�fC� C�s3D��D�fD��D�3D��D�fD��D$�3D)� D.��D3ٚD8�3D=��DB��DG�3DLٚDQ��DV�fD[�3D`�3De��DjٚDo� Dt� Dy�3D�#3D�p D���D��fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�fgAffAd��A���A���A陙B
��B34B2  BF  BY��Bm34B�  B���B�fgB�  B�  B���B�  B�  B���B���B�33B�fgB���C �3C�C33CffC� C33C� C$L�C)L�C.� C333C8  C=ffCB  CGffCQ�CZ�3Ce�Co  Cy  C��3C��fC���C��fC��fC�� C�ffC�ffC�� C���C�s3C�Y�C���C Cǌ�C̀ Cь�C֙�Cی�C�3C噙CꙙCC�s3C�ffD�gD� D�gD��D�4D� D�4D$��D)��D.�gD3�4D8��D=�4DB�4DG��DL�4DQ�gDV� D[��D`��De�gDj�4Do��DtٚDy��D�  D�l�D���D��3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�+A�A�\A�+A��A�jA��A�(�Aե�AмjA�ĜA�-A�ƨA�l�A�9XA��yA���A��A��A���A�+A��yA��TA�ȴA��PA��A��mA���A�n�A���A���A�M�A�S�A�ZA�(�A��FA�K�A��jA�ĜA���A�-A��PA�\)A��;A�&�A��HA�$�A��A��A�p�A�I�A�G�A��RA~��A{XAz9XAw��As�^ApVAi�Agp�Ae
=Aa�7A]p�AU/AP��AJ�`AF�!AA�^A?A3|�A'hsA#�A��A
�\A�^@���@�|�@◍@У�@��@��F@��T@�|�@�1@�{@���@��^@�9X@���@���@��m@~��@x�@q��@`��@Y&�@P��@K�
@K��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�+A�A�\A�+A��A�jA��A�(�Aե�AмjA�ĜA�-A�ƨA�l�A�9XA��yA���A��A��A���A�+A��yA��TA�ȴA��PA��A��mA���A�n�A���A���A�M�A�S�A�ZA�(�A��FA�K�A��jA�ĜA���A�-A��PA�\)A��;A�&�A��HA�$�A��A��A�p�A�I�A�G�A��RA~��A{XAz9XAw��As�^ApVAi�Agp�Ae
=Aa�7A]p�AU/AP��AJ�`AF�!AA�^A?A3|�A'hsA#�A��A
�\A�^@���@�|�@◍@У�@��@��F@��T@�|�@�1@�{@���@��^@�9X@���@���@��m@~��@x�@q��@`��@Y&�@P��@K�
@K��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
cTB
dZB
cTB
dZB
u�B
�B
ƨBPBy�B�B�B�^B�{Bv�BhsB=qB49B1'B-B �B�BB
��B
��B
��B
��BBBB
��B
��BB
=B%BB
��B
��B
��B
�B
��B	7B�BbB  B
��B
��B
��B
�B
�BB
�B
��B
ŢB
�B
��B
�bB
�=B
|�B
hsB
W
B
7LB
.B
!�B
oB	��B	�B	B	��B	�uB	~�B	r�B	<jB	+B��B�RB�uB}�B`BBQ�BG�BA�BB�BJ�B\)Bn�By�B� B��B�9B��B�B		7B	oB	+B	7LB	I�B	z�B	�\B	��B	�'B	�'1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
cTB
dZB
cTB
dZB
u�B
�B
ƨBPBy�B�B�B�^B�{Bv�BhsB=qB49B1'B-B �B�BB
��B
��B
��B
��BBBB
��B
��BB
=B%BB
��B
��B
��B
�B
��B	7B�BbB  B
��B
��B
��B
�B
�BB
�B
��B
ŢB
�B
��B
�bB
�=B
|�B
hsB
W
B
7LB
.B
!�B
oB	��B	�B	B	��B	�uB	~�B	r�B	<jB	+B��B�RB�uB}�B`BBQ�BG�BA�BB�BJ�B\)Bn�By�B� B��B�9B��B�B		7B	oB	+B	7LB	I�B	z�B	�\B	��B	�'B	�'1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090915005549  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090915005551  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090915005551  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090915005551  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090915005552  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090915005553  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090915005553  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090915005553  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090915005553  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090915005553  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090915010307                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090918185617  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090918185843  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090918185844  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090918185844  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090918185844  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090918185845  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090918185845  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090918185845  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090918185845  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090918185846  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090918190251                      G�O�G�O�G�O�                