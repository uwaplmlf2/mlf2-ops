CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900687 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               _A   JA  20090924215546  20090928190202                                  2B  A   APEX-SBE 2968                                                   846 @�N+��91   @�N,W��@9Η�O�;@b��E�1   ARGOS   A   A   A   @���AffAh  A���A�  A���B
ffB��B2��BF  BZffBl��B��B���B�ffB�33B�ffB���B�  B�33B�33B�ffB�ffB�  B�  CffCffC�C  C  C33C  C#��C)ffC.  C3L�C7�fC<�fCB� CGL�CQ�C[33Ce�3CoffCy�C��fC���C�� C��fC�� C���C��fC���C���C��fC��3C�� C���C³3CǙ�C̦fC�� C�s3C۳3C�s3C��C�fCC��fC��3D�3D� D�3DٚD� DٚDٚD$�3D)��D.�3D3��D8� D=ٚDB��DG�3DLٚDQ�fDV�fD[�3D`��De�fDj��DoٚDt� Dy� D�,�D�c3D���D�� D��D�i�D�� D��D��D�c3D�� D��fD�  D�l�Dڬ�D��3D�  D�` D�D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA��AfffA�  A�33A�  B
  BfgB2fgBE��BZ  BlfgB34B�fgB�33B�  B�33B�fgB���B�  B�  B�33B�33B���B���CL�CL�C  C�fC�fC�C�fC#�3C)L�C-�fC333C7��C<��CBffCG33CQ  C[�Ce��CoL�Cy  C���C���C��3C���C��3C�� C���C�� C���C���C��fC��3C���C¦fCǌ�C̙�Cѳ3C�ffCۦfC�ffC� CꙙC��C���C��fD��DٚD��D�4D��D�4D�4D$��D)�gD.��D3�gD8ٚD=�4DB�4DG��DL�4DQ� DV� D[��D`�4De� Dj�gDo�4DtٚDyٚD�)�D�` D��gD���D��D�fgD���D��gD��D�` D���D��3D��D�i�Dک�D�� D��D�\�D�gD�ɚ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ƨA���A�ĜA� �A��A�{A�
=A���A��/A��AĶFA��+A���A��A��A�1'A��A�z�A���A�r�A��+A���A��RA���A�A��A��HA�1A���A�=qA�C�A���A�VA�&�A��A��!A���A��jA��A�ffA�1A�7LA�n�A� �A�ffA�r�A��\A��^A�\)A�ffA�r�A�Ay�At��Ao�Aj��Ahn�Ab��A^��A\��AY��AT  AP1AL�AHr�AC�TA@�A9�A1t�A-hsA'K�A��A�A��@���@���@�o@�A�@�;d@�x�@��@��!@���@��@�x�@��w@�1@��F@���@��@�@|z�@vȴ@p  @l9X@`�@X��@Kƨ@?+@;�@6v�@0bN@)�@ �@l�@dZ@�-@E�@
^5@�;@�@�?��-?���?�?}1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�ƨA���A�ĜA� �A��A�{A�
=A���A��/A��AĶFA��+A���A��A��A�1'A��A�z�A���A�r�A��+A���A��RA���A�A��A��HA�1A���A�=qA�C�A���A�VA�&�A��A��!A���A��jA��A�ffA�1A�7LA�n�A� �A�ffA�r�A��\A��^A�\)A�ffA�r�A�Ay�At��Ao�Aj��Ahn�Ab��A^��A\��AY��AT  AP1AL�AHr�AC�TA@�A9�A1t�A-hsA'K�A��A�A��@���@���@�o@�A�@�;d@�x�@��@��!@���@��@�x�@��w@�1@��F@���@��@�@|z�@vȴ@p  @l9X@`�@X��@Kƨ@?+@;�@6v�@0bN@)�@ �@l�@dZ@�-@E�@
^5@�;@�@�?��-?���?�?}1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�BH�BG�BH�BJ�BK�BM�BL�BH�BC�BP�BP�BP�BM�BL�B?}BQ�BJ�BN�B6FBJB  B
��B
��B
��B
��B
��B
��BB%B%B
��B
�B
�NB
�;B
�;B
�HB
��B�B�BDB
��B
�B
��B
��B
��B
�B
�B
�ZB
�BB
��B
�dB
��B
�B
jB
O�B
;dB
1'B
�B
+B	��B	�B	��B	�wB	�B	��B	�B	o�B	T�B	0!B	�B	B��B��B�=Bl�BL�B?}B:^B9XBD�BO�BZBo�B�+B��B�jB��B�fB	B	hB	�B	,B	<jB	O�B	\)B	|�B	�\B	�!B	��B	�B	�TB	�B
  B
oB
�B
�B
+B
<jB
F�B
M�B
VB
[#B
e`B
jB
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 BH�BG�BH�BJ�BK�BM�BL�BH�BC�BP�BP�BP�BM�BL�B?}BQ�BJ�BN�B6FBJB  B
��B
��B
��B
��B
��B
��BB%B%B
��B
�B
�NB
�;B
�;B
�HB
��B�B�BDB
��B
�B
��B
��B
��B
�B
�B
�ZB
�BB
��B
�dB
��B
�B
jB
O�B
;dB
1'B
�B
+B	��B	�B	��B	�wB	�B	��B	�B	o�B	T�B	0!B	�B	B��B��B�=Bl�BL�B?}B:^B9XBD�BO�BZBo�B�+B��B�jB��B�fB	B	hB	�B	,B	<jB	O�B	\)B	|�B	�\B	�!B	��B	�B	�TB	�B
  B
oB
�B
�B
+B
<jB
F�B
M�B
VB
[#B
e`B
jB
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090924215545  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090924215546  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090924215546  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090924215547  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090924215547  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090924215548  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090924215548  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090924215548  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090924215548  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090924215548  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090924220122                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090928185559  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090928185833  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090928185833  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090928185834  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090928185834  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090928185835  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090928185835  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090928185835  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090928185835  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090928185835  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090928190202                      G�O�G�O�G�O�                