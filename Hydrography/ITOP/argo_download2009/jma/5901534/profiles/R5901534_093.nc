CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901534 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ]A   JA  20090920155627  20090924100025                                  2B  A   APEX-SBE 2825                                                   846 @�M}� �1   @�M���m@6�     @b`9XbN1   ARGOS   A   A   A   @�  A��Ac33A�  A�33A�  B
ffB��B2��BFffBY33BnffB���B�  B���B�ffB���B���B���B�ffB���Bڙ�B���B홚B���CL�CffCL�CffCffC33C  C$  C)  C.L�C3  C8�C=��CB��CG��CQ33C[� Ce�Co33CyffC��fC�� C���C��fC�ffC���C��fC���C��fC�s3C��fC��fC���C³3Cǳ3C̀ Cь�C֌�C�s3C�fC�fC��C���C���C�� DٚDٚD��DٚDٚDٚD� D$� D)��D.�3D3ٚD8��D=�3DBٚDG�fDL�3DQ��DV��D[�3D`��De�3Dj��DoٚDt� Dy��D�)�D�` D�� D�� D�)�D�l�D��fD���D�)�D�\�D��fD��3D�#3D�c3Dڬ�D��D��D�ffD�3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A34A\��A���A�  A���B��B33B133BD��BW��Bl��B�  B�33B�  B���B���B�  B�  Bř�B�  B���B�  B���B�  C �gC  C
�gC  C  C��C��C#��C(��C-�gC2��C7�4C=34CB34CG34CP��C[�Cd�4Cn��Cy  C�s3C�L�C�Y�C�s3C�33C�Y�C�s3C�Y�C�s3C�@ C�s3C�s3C���C Cǀ C�L�C�Y�C�Y�C�@ C�s3C�s3C�Y�CC���C���D� D� D�3D� D� D� D�fD$�fD)�3D.��D3� D8�3D=��DB� DG��DL��DQ�3DV� D[��D`�3De��Dj�3Do� Dt�fDy�3D��D�S3D��3D��3D��D�` D���D�� D��D�P D���D��fD�fD�VfDڠ D���D��D�Y�D�fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A��A�"�A�$�A�(�A�&�A��mA��A��`A���A��;Aȇ+A���A�VA��A��TA�A�A�
=A���A�dZA�9XA��A�\)A�5?A�M�A�A��A��;A���A��yA�\)A��HA���A�|�A�ĜA���A��A�x�A�O�A��A���A�`BA�l�A�`BA��A��A���A�A�dZA�+A�A{?}Av�yAq%AjbNAc33A]K�AZ�AVJAPbNAKt�AI��AFE�A=�
A7�A1A+l�A&jA#t�A�9A�;A
(�@���@�@�V@܃@�"�@��D@�Q�@�b@�r�@�&�@���@���@�@�5?@�K�@�(�@��/@�hs@�K�@�j@�w@t��@f�+@a��@MO�@D�D@:J@6@-�@&��@  �@��@��@��@
�@A�@�@~�@ bN?��?��P?�F1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A��A�"�A�$�A�(�A�&�A��mA��A��`A���A��;Aȇ+A���A�VA��A��TA�A�A�
=A���A�dZA�9XA��A�\)A�5?A�M�A�A��A��;A���A��yA�\)A��HA���A�|�A�ĜA���A��A�x�A�O�A��A���A�`BA�l�A�`BA��A��A���A�A�dZA�+A�A{?}Av�yAq%AjbNAc33A]K�AZ�AVJAPbNAKt�AI��AFE�A=�
A7�A1A+l�A&jA#t�A�9A�;A
(�@���@�@�V@܃@�"�@��D@�Q�@�b@�r�@�&�@���@���@�@�5?@�K�@�(�@��/@�hs@�K�@�j@�w@t��@f�+@a��@MO�@D�D@:J@6@-�@&��@  �@��@��@��@
�@A�@�@~�@ bN?��?��P?�F1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B0!B1'B1'B0!B0!B/B/B.B49B��B�RB�/B��B��Bu�BjBt�B;dB(�B/B0!B=qBJ�BO�BL�BH�BH�BB�BA�B>wB�B&�B(�B)�B+B&�B%�B"�B�B�BoBJB  B
��B
�B
�B
�BB
�B
ŢB
��B
�-B
��B
�VB
w�B
\)B
;dB
�B
  B	�B	�/B	��B	��B	��B	�VB	cTB	F�B	,B	bB��B�B��B�?B�VBp�BaHB]/B_;B[#Be`Bx�B�hB��B�dBB�B�B��B	1B	�B	)�B	E�B	T�B	O�B	gmB	hsB	�JB	�'B	�dB	��B	�ZB	�B	��B
PB
 �B
1'B
<jB
I�B
R�B
YB
_;B
ffB
k�B
p�B
v�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B0!B1'B1'B0!B0!B/B/B.B49B��B�RB�/B��B��Bu�BjBt�B;dB(�B/B0!B=qBJ�BO�BL�BH�BH�BB�BA�B>wB�B&�B(�B)�B+B&�B%�B"�B�B�BoBJB  B
��B
�B
�B
�BB
�B
ŢB
��B
�-B
��B
�VB
w�B
\)B
;dB
�B
  B	�B	�/B	��B	��B	��B	�VB	cTB	F�B	,B	bB��B�B��B�?B�VBp�BaHB]/B_;B[#Be`Bx�B�hB��B�dBB�B�B��B	1B	�B	)�B	E�B	T�B	O�B	gmB	hsB	�JB	�'B	�dB	��B	�ZB	�B	��B
PB
 �B
1'B
<jB
I�B
R�B
YB
_;B
ffB
k�B
p�B
v�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090920155626  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090920155627  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090920155627  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090920155628  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090920155628  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090920155629  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090920155629  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090920155629  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090920155629  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090920155629  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090920160215                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090924095502  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090924095707  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090924095707  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090924095707  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090924095708  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090924095709  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090924095709  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090924095709  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090924095709  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090924095709  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090924100025                      G�O�G�O�G�O�                