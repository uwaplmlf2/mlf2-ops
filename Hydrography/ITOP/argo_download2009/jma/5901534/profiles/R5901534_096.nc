CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901534 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               `A   JA  20091020125623  20091024100110                                  2B  A   APEX-SBE 2825                                                   846 @�T��R�1   @�T�oz�@7�I�^@bZ-V1   ARGOS   A   A   A   @���A��Ac33A�  A���A�ffB
  B��B1��BDffBZ��Bo33B�ffB�ffB�33B���B�ffB�  B���Bƙ�B�33B�ffB�ffB�  B���CL�C33CffCffC33C�fCffC$ffC)ffC.� C3��C8L�C=ffCB33CGL�CQL�C[ffCe33Co�Cy33C���C�ffC�ffC���C�s3C���C�� C��fC���C�� C��3C�� C�� C¦fC�s3C̳3CѦfCֳ3CۦfC���C�ٚCꙚCC�� C��3D�fD� D��D��D��D�3DٚD$� D)�3D.��D3��D8��D=�fDB�fDG�3DL��DQ� DV��D[��D`��DeٚDj��Do�fDt� Dy� D�&fD�c3D���D��fD�)�D�c3D���D�� D�&fD�i�D���D���D�  D�c3Dڬ�D���D�)�D�l�D�D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A��A^ffA���A�34A�  B��B��B0fgBC33BY��Bn  B��B���B���B�33B���B�ffB�  B�  Bϙ�B���B���B�ffB�  C  C�fC�C�C�fC��C�C$�C)�C.33C3L�C8  C=�CA�fCG  CQ  C[�Cd�fCn��Cx�fC�s4C�@ C�@ C�fgC�L�C�s4C���C�� C�s4C���C���C�Y�C�Y�C C�L�Č�Cр C֌�Cۀ C�fgC�4C�s4C�s4C���C���D�3D��D�gD��D��D� D�gD$��D)� D.�gD3�gD8��D=�3DB�3DG� DL��DQ��DV��D[��D`�gDe�gDj��Do�3Dt��Dy��D��D�Y�D��3D���D�  D�Y�D��3D��fD��D�` D�� D��3D�fD�Y�Dڣ3D��3D�  D�c3D� D�y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�G�A�5?A��A�%A���A�wA�v�A�`BA�33A���A�`BA�{AčPA�ȴA�hsA�VA�bA�ffA�v�A���A��A��hA��FA� �A��jA��\A�z�A��/A��A��A���A���A�?}A�?}A���A�5?A�ƨA�jA�VA��yA�x�A� �A���A�+A�bNA�1'A�5?A���A��DA��RA��A}
=Ay��AuS�Am�hAk`BAiƨAe�
AbE�A^I�A\��AW�^AT(�APr�AM�AEC�A?hsA:{A5�A.��A#�#A5?A��A��A�@���@�!@�hs@� �@Ĭ@���@��@���@�z�@��@�^5@��@�$�@��#@� �@�z�@��@zn�@u�@o�;@b^5@U��@L��@E�@>�+@6{@17L@,I�@%��@+@Ĝ@/@��@Q�@9X@b@V@�@ A�?�(�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�G�A�5?A��A�%A���A�wA�v�A�`BA�33A���A�`BA�{AčPA�ȴA�hsA�VA�bA�ffA�v�A���A��A��hA��FA� �A��jA��\A�z�A��/A��A��A���A���A�?}A�?}A���A�5?A�ƨA�jA�VA��yA�x�A� �A���A�+A�bNA�1'A�5?A���A��DA��RA��A}
=Ay��AuS�Am�hAk`BAiƨAe�
AbE�A^I�A\��AW�^AT(�APr�AM�AEC�A?hsA:{A5�A.��A#�#A5?A��A��A�@���@�!@�hs@� �@Ĭ@���@��@���@�z�@��@�^5@��@�$�@��#@� �@�z�@��@zn�@u�@o�;@b^5@U��@L��@E�@>�+@6{@17L@,I�@%��@+@Ĝ@/@��@Q�@9X@b@V@�@ A�?�(�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�BBB
��BBBB%B1BB
��B
��B
�B
��B
=BPB�B1'BG�BJ�BI�B\)BC�BDBDBJBoBVB
��B%�BA�B:^B2-B"�B�B%BB
��BB%B	7B�BVB
=B
��B
��B
�B
�HB
��B
��B
�RB
��B
��B
�+B
p�B
O�B
E�B
;dB
(�B
�B
%B	��B	�ZB	��B	B	�'B	�DB	p�B	XB	@�B	!�B�B�B�B��Bw�BiyB]/BT�BXB^5BdZBo�B�B�{B�B�qB�)B�B	B	�B	'�B	6FB	K�B	dZB	t�B	��B	�FB	�B	�fB	��B
  B
DB
�B
!�B
/B
:^B
A�B
G�B
L�B
W
B
`BB
ffB
k�B
r�B
t�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 BBB
��BBBB%B1BB
��B
��B
�B
��B
=BPB�B1'BG�BJ�BI�B\)BC�BDBDBJBoBVB
��B%�BA�B:^B2-B"�B�B%BB
��BB%B	7B�BVB
=B
��B
��B
�B
�HB
��B
��B
�RB
��B
��B
�+B
p�B
O�B
E�B
;dB
(�B
�B
%B	��B	�ZB	��B	B	�'B	�DB	p�B	XB	@�B	!�B�B�B�B��Bw�BiyB]/BT�BXB^5BdZBo�B�B�{B�B�qB�)B�B	B	�B	'�B	6FB	K�B	dZB	t�B	��B	�FB	�B	�fB	��B
  B
DB
�B
!�B
/B
:^B
A�B
G�B
L�B
W
B
`BB
ffB
k�B
r�B
t�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091020125622  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091020125623  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091020125624  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091020125624  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091020125624  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091020125625  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091020125625  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091020125625  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091020125625  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091020125625  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091020130311                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091024095457  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091024095650  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091024095651  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091024095651  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091024095651  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091024095652  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091024095652  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091024095652  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091024095652  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091024095653  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091024100110                      G�O�G�O�G�O�                