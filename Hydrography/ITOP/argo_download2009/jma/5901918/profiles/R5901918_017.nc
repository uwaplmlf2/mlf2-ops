CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   p   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4\   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C<   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E,   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H,   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K,   CALIBRATION_DATE            	             
_FillValue                  ,  N,   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    NX   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N\   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N`   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Nd   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Nh   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5901918 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090914215658  20091204075620                                  2B  A   APEX-SBE 4055                                                   846 @�KV�So�1   @�KYM��@=�I�^5?@bcƧ1   ARGOS   B   B   B   @�  A!��As33A���Aə�A�ffB��B$  B6  BG33B\  Bo��B�  B�33B���B���B���B�  B���B�ffB�33B�  B�33B�  B�33C  C��C��C� C� C�3C��C%  C*ffC/  C3�3C8��C>ffCC33CH33CR33C\� CfffCpL�Cz33C��C�33C�33C��3C���C�33C��C��3C�&fC�33C�&fC�33C��C��3C��C��fC��C��C�  G�O�C�33D  D  D��D  D  D��D &fD%3D*�D/�D3��D8�3D>�DC  DH  DM�DQ��DV��D\�D`� Df  Dk�Dp�Du3Dy��D�<�D�� D�� D�fD�<�D��fD���D�fD�L�D�� D�� D�  D�6fDԀ D�� D�3D�I�D�vfD��D���D�L�1111111111111111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111111111111@�ffA$��AvffA�fgA�34A�  B��B$��B6��BH  B\��BpfgB�ffB���B�33B�33B�33B�ffB�33B���Bә�B�ffB晙B�ffB���C33C  C��C�3C�3C�fC   C%33C*��C/33C3�fC8��C>��CCffCHffCRffC\�3Cf��Cp� CzffC�&gC�L�C�L�C��C��gC�L�C�34C��C�@ C�L�C�@ C�L�C�34C��C�&gC�  C�&gC�&gC��G�O�C�L�D�D,�DgD�D,�D��D 33D%  D*&gD/�D3��D9  D>&gDC,�DH�DM�DRgDWgD\�D`��Df,�Dk�Dp&gDu  Dy��D�C3D��fD��fD��D�C3D���D��3D��D�S3D��fD��fD�fD�<�DԆfD��fD�	�D�P D�|�D��3D�� D�S31111111111111111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�p�A�I�A��A�bNA�I�A�"�A�ZA�dZA�%A�ȴA�G�A���A��A�$�A��A���A�v�A��A���A�$�A�`BA�n�A�"�A�VA��!A���A�A�A���A��A��9A���A���A�%A���A�1A��uA�$�A���A�^5A���A�p�A�JA�jA��#A�ffA�(�A��!A��A�bA��-A��A|�\Ay�^Av5?As?}Ao�AmVAi��Agx�Adz�Aa
=A^-AX�!G�O�A8��A3�TA,9XA&�Ap�A�At�A �!@��@�Z@�b@��@���@�p�@�r�@��@�@��@�|�@�Q�@���@���@���@�@�X@�ff@~��@qX@b�\@W�;@O�;@I7L@>�+@5��@.ff@(1'@#33@V@M�@V@M�@�@�@�u@�@��?�j?�(�1111111111111111111111111111111111111111111111111111111111111119411111111111111111111111111111111111111111111111A�p�A�I�A��A�bNA�I�A�"�A�ZA�dZA�%A�ȴA�G�A���A��A�$�A��A���A�v�A��A���A�$�A�`BA�n�A�"�A�VA��!A���A�A�A���A��A��9A���A���A�%A���A�1A��uA�$�A���A�^5A���A�p�A�JA�jA��#A�ffA�(�A��!A��A�bA��-A��A|�\Ay�^Av5?As?}Ao�AmVAi��Agx�Adz�Aa
=A^-AX�!G�O�A8��A3�TA,9XA&�Ap�A�At�A �!@��@�Z@�b@��@���@�p�@�r�@��@�@��@�|�@�Q�@���@���@���@�@�X@�ff@~��@qX@b�\@W�;@O�;@I7L@>�+@5��@.ff@(1'@#33@V@M�@V@M�@�@�@�u@�@��?�j?�(�1111111111111111111111111111111111111111111111111111111111111119411111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
?}B
A�B
N�B
k�B
�B
�{B
��B
��B33B`BB_;BiyBS�B!�B#�B49B�B+B9XB)�BoBBB�B�BbB	7B
=B	7B%BB
��B
��B
��B
��B
��B
��B
��B
��B
��B
�B
�B
�yB
�5B
��B
B
�?B
�B
��B
��B
��B
�DB
�B
o�B
aHB
Q�B
B�B
7LB
0!B
�B
bB
B	�B	��G�O�B	F�B	%�B	PB�;B�FB�BjB>wB.B&�B �B&�B7LBM�BVBcTBs�B�B�hB��B�XB��B�B�B��B	
=B	,B	P�B	iyB	{�B	�hB	�B	��B	��B	�fB	�B
B
VB
�B
#�B
-B
7LB
B�B
L�B
T�B
\)B
^51111111111111111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111111111111B
?}B
A�B
N�B
k�B
�B
�{B
��B
��B33B`BB_;BiyBS�B!�B#�B49B�B+B9XB)�BoBBB�B�BbB	7B
=B	7B%BB
��B
��B
��B
��B
��B
��B
��B
��B
��B
�B
�B
�yB
�5B
��B
B
�?B
�B
��B
��B
��B
�DB
�B
o�B
aHB
Q�B
B�B
7LB
0!B
�B
bB
B	�B	��G�O�B	F�B	%�B	PB�;B�FB�BjB>wB.B&�B �B&�B7LBM�BVBcTBs�B�B�hB��B�XB��B�B�B��B	
=B	,B	P�B	iyB	{�B	�hB	�B	��B	��B	�fB	�B
B
VB
�B
#�B
-B
7LB
B�B
L�B
T�B
\)B
^51111111111111111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090914215657  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090914215658  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090914215659  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090914215659  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090914215700  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090914215700  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090914215700  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090914215700  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090914215700  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090914215700  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090914215700  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090914220116                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090917095649  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090917095736  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090917095737  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090917095737  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090917095738  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090917095738  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090917095738  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090917095738  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090917095738  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090917095738  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090917095738  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090917100120                      G�O�G�O�G�O�                JA  ARFMdecpA14d                                                                20091204075332  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204075503  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204075504  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204075504  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204075505  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091204075505  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204075505  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204075505  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204075505  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204075505  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204075505  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091204075620                      G�O�G�O�G�O�                