CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   i   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J   CALIBRATION_DATE            	             
_FillValue                  ,  M   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    MD   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    MH   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    ML   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    MP   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  MT   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901918 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090905005637  20091204075619                                  2B  A   APEX-SBE 4055                                                   846 @�Hܼ�u�1   @�H�A
��@=��vȴ9@bf��O�;1   ARGOS   A   A   A   A���B  B!33B5��BHffBY��BrffB�ffB���B���B�ffB�  B���B�33B�  B�33Bܙ�B�ffB�  C� CL�C��C%�C*� C.�fC4ffC8��C>33CCL�CG�3CQffC\ffCfffCp�Cz� C�33C�&fC�33C��fC�&fC�@ C�33C�ٚC��C�Y�C�@ C�&fC�&fC�&fC�&fC�33C�ٚC��C��C�ٚC�&fC��fC�ٚC��C�s3D  D3D�fD� D��D3D   D$��D)��D.�fD4�D8��D>�DC  DH3DL��DR�DW�D\�DafDe��Dk�DpfDt��Dz�D�P D�vfD��3D��D�<�D���D��3D���D�9�D�� D�ɚD�fD�C3D�p Dڼ�D��D�I�D� D�� D�y�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�fgB��B"  B6fgBI33BZfgBs33B���B�  B�33B���B�ffB�33B���B�ffBә�B�  B���B�ffC�3C� C   C%L�C*�3C/�C4��C9  C>ffCC� CG�fCQ��C\��Cf��CpL�Cz�3C�L�C�@ C�L�C�  C�@ C�Y�C�L�C��4C�&gC�s4C�Y�C�@ C�@ C�@ C�@ C�L�C��4C�34C�34C��4C�@ C�  C��4C�&gC���D,�D  D�3D��DgD  D ,�D$��D)��D.�3D4&gD8��D>�DC,�DH  DL��DR�DW�D\&gDa3De��Dk&gDp3Dt��Dz�D�VfD�|�D�əD�3D�C3D�� D�əD�3D�@ D��fD�� D��D�I�D�vfD��3D�3D�P D�fD��fD�� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A؟�A��A���A�t�A�A�Q�A�t�A�l�A��;A��TA��A�\)A��A�l�A�XA�v�A�+A�n�A��-A���A���A��^A�%A��A�VA�JA�|�A�33A���A���A�Q�A�bA��DA��A��#A�dZA�=qA��-A�z�A��mA�\)A��A}K�Az1'Ax  ArA�Ao��Al1Ai�mAf��AcA_�AZ9XAWx�AT�AO?}AK�AH�+AD{A?p�A61A*A��A�A-A9X@��`@��@ؼj@�;d@�@��y@�`B@���@��@���@��m@�
=@���@�~�@�Q�@�n�@���@�  @z=q@qhs@e@Y�^@O+@G|�@=��@7l�@0  @( �@$1@@�P@ƨ@  @`B@��@p�@-?��?��H111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A؟�A��A���A�t�A�A�Q�A�t�A�l�A��;A��TA��A�\)A��A�l�A�XA�v�A�+A�n�A��-A���A���A��^A�%A��A�VA�JA�|�A�33A���A���A�Q�A�bA��DA��A��#A�dZA�=qA��-A�z�A��mA�\)A��A}K�Az1'Ax  ArA�Ao��Al1Ai�mAf��AcA_�AZ9XAWx�AT�AO?}AK�AH�+AD{A?p�A61A*A��A�A-A9X@��`@��@ؼj@�;d@�@��y@�`B@���@��@���@��m@�
=@���@�~�@�Q�@�n�@���@�  @z=q@qhs@e@Y�^@O+@G|�@=��@7l�@0  @( �@$1@@�P@ƨ@  @`B@��@p�@-?��?��H111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
H�B
VB
l�B
�B
�VB
��B
�3B
��B
�jB
��B
��B
��B
��B
�B
�B
�B
�/B
�5B
��B
��B
��BoB
=B
=BB
��B
��B\BuB�BPB
��BB
��B
��B
�B
�NB
�/B
��B
B
�-B
��B
�uB
�%B
z�B
\)B
Q�B
B�B
8RB
,B
�B
%B	�B	�NB	��B	�jB	�B	��B	�+B	u�B	K�B	�B�B��B��B�%B`BBO�BA�B>wB>wB9XB7LB\)BjB{�B�oB��B��B��B�/B�fB��B	+B	�B	49B	P�B	q�B	�DB	��B	�9B	��B	�5B	�B	��B

=B
�B
!�B
,B
5?B
B�B
J�B
Q�B
ZB
_;111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B
H�B
VB
l�B
�B
�VB
��B
�3B
��B
�jB
��B
��B
��B
��B
�B
�B
�B
�/B
�5B
��B
��B
��BoB
=B
=BB
��B
��B\BuB�BPB
��BB
��B
��B
�B
�NB
�/B
��B
B
�-B
��B
�uB
�%B
z�B
\)B
Q�B
B�B
8RB
,B
�B
%B	�B	�NB	��B	�jB	�B	��B	�+B	u�B	K�B	�B�B��B��B�%B`BBO�BA�B>wB>wB9XB7LB\)BjB{�B�oB��B��B��B�/B�fB��B	+B	�B	49B	P�B	q�B	�DB	��B	�9B	��B	�5B	�B	��B

=B
�B
!�B
,B
5?B
B�B
J�B
Q�B
ZB
_;111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090905005636  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090905005637  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090905005637  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090905005638  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090905005639  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090905005639  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090905005639  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090905005639  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090905005639  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090905005639  QCF$                G�O�G�O�G�O�            4100JA  ARGQaqcp2.8b                                                                20090905005639  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090905005639  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20090905005639  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090905010058                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090907125713  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090907125758  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090907125759  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090907125759  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090907125800  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090907125800  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090907125800  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090907125800  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090907125800  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090907125800  QCF$                G�O�G�O�G�O�            4100JA  ARGQaqcp2.8b                                                                20090907125800  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090907125800  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20090907125801  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090907130045                      G�O�G�O�G�O�                JA  ARFMdecpA14d                                                                20091204075331  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204075501  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204075501  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204075502  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204075503  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204075503  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204075503  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204075503  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204075503  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091204075619                      G�O�G�O�G�O�                