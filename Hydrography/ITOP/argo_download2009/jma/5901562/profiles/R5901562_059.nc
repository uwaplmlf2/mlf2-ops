CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   j   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D<   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G<   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J<   CALIBRATION_DATE            	             
_FillValue                  ,  M<   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Mh   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Ml   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Mp   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Mt   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Mx   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901562 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ;A   JA  20090913185708  20091208012257                                  2B  A   APEX-SBE 3469                                                   846 @�K<}�1   @�K�?V@+Y�+@`(�\1   ARGOS   B   B   B   @���A��AfffA���A�  A���B	33BffB133BD  BY��Bn��B�ffB�33B�33B�ffB�33B�ffB���B���B�33B�  B���BB�ffC��CffCffCL�C�CL�CffC$��C)� C.��C3��C8�C=33CB� CG� CQ�3C[� CeffCo� Cy� C��3C�� C���C��fC��fC���C�� C���C�� C��fC���C�ffC�s3C�Cǌ�C̀ CѦfC֌�C۳3C���C�fCꙚC��C��fC�� DٚDٚDٚD� D��D�fD�fD$��D)�fD.�3D3��DQ�fDV��D[�3D`��De� Dj��Do� Dt��Dy�3D�)�D�i�D���D���D�)�D�i�G�O�D��3D��D�s3Dڬ�D��fD�)�D�` D� D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114111111111  @���A��AfffA���A�  A���B	33BffB133BD  BY��Bn��B�ffB�33B�33B�ffB�33B�ffB���B���B�33B�  B���BB�ffC��CffCffCL�C�CL�CffC$��C)� C.��C3��C8�C=33CB� CG� CQ�3C[� CeffCo� Cy� C��3C�� C���C��fC��fC���C�� C���C�� C��fC���C�ffC�s3C�Cǌ�C̀ CѦfC֌�C۳3C���C�fCꙚC��C��fC�� DٚDٚDٚD� D��D�fD�fD$��D)�fD.�3D3��DQ�fDV��D[�3D`��De� Dj��Do� Dt��Dy�3D�)�D�i�D���D���D�)�D�i�G�O�D��3D��D�s3Dڬ�D��fD�)�D�` D� D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�`BA�Q�A�ZA�33A�-A�/A�+A�(�A�(�A�$�A�$�A�&�A�+A曦A�A�7A�A��A݋DA�"�Aܲ-A���Aڝ�A�+A���A���AΝ�A�;dA��A��A�v�A��A���A���A���A�C�A���A�"�A�7LA�ƨA��+A�
=A��+A��A�bNA�v�Az5?AsAlv�Ac�TAX �AJI�ACx�A<M�A4ZA.��A(��A#|�A!��A   A��A(�A�AA�
A��A1'A�@��@�J@�x�@�hs@�7L@��@ǝ�@�`B@���@�E�@�@�x�@��7@��D@���@�l�@�I�@�p�@���@�"�@�j@���@y&�@i�#@]@W;d@Qx�G�O�@(�u@#��@l�@�!@�y@��@1'@5?@ƨ@	%1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111194111111111  A�`BA�Q�A�ZA�33A�-A�/A�+A�(�A�(�A�$�A�$�A�&�A�+A曦A�A�7A�A��A݋DA�"�Aܲ-A���Aڝ�A�+A���A���AΝ�A�;dA��A��A�v�A��A���A���A���A�C�A���A�"�A�7LA�ƨA��+A�
=A��+A��A�bNA�v�Az5?AsAlv�Ac�TAX �AJI�ACx�A<M�A4ZA.��A(��A#|�A!��A   A��A(�A�AA�
A��A1'A�@��@�J@�x�@�hs@�7L@��@ǝ�@�`B@���@�E�@�@�x�@��7@��D@���@�l�@�I�@�p�@���@�"�@�j@���@y&�@i�#@]@W;d@Qx�G�O�@(�u@#��@l�@�!@�y@��@1'@5?@ƨ@	%1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111194111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B��B��B��B��B��B��B��B��B��B��B��B�
B
DBk�BŢB�BB�B-B@�BF�BYBZBO�B=qB�B�BoBBBB\B{BhBVB+B�#B�VB�?B��BQ�B?}B
��B
��B
�B
G�B
B�B
!�B	��B	ȴB	��B	�B	��B	�1B	q�B	cTB	gmB	iyB	jB	iyB	p�B	|�B	�B	�oB	�DB	�VB	�VB	�B	�B	s�B	^5B	hsB	� B	��B	�B	�dB	ŢB	��B	�B	�TB
hB
uB
�B
�B
�B
$�B
&�B
+B
.B
49B
<jB
?}B
E�B
J�G�O�B
gmB
k�B
p�B
u�B
x�B
|�B
� B
�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111194111111111  B��B��B��B��B��B��B��B��B��B��B��B��B�
B
DBk�BŢB�BB�B-B@�BF�BYBZBO�B=qB�B�BoBBBB\B{BhBVB+B�#B�VB�?B��BQ�B?}B
��B
��B
�B
G�B
B�B
!�B	��B	ȴB	��B	�B	��B	�1B	q�B	cTB	gmB	iyB	jB	iyB	p�B	|�B	�B	�oB	�DB	�VB	�VB	�B	�B	s�B	^5B	hsB	� B	��B	�B	�dB	ŢB	��B	�B	�TB
hB
uB
�B
�B
�B
$�B
&�B
+B
.B
49B
<jB
?}B
E�B
J�G�O�B
gmB
k�B
p�B
u�B
x�B
|�B
� B
�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111194111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090913185707  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090913185708  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090913185708  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090913185709  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090913185710  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090913185710  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090913185710  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090913185710  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090913185710  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090913185710  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090913185710  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090913190349                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090916095619  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090916095829  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090916095829  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090916095829  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090916095831  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090916095831  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090916095831  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090916095831  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090916095831  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090916095831  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090916095831  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090916100542                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207075715  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207075838  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207075839  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207075839  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207075840  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207075840  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207075840  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207075840  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207075841  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207075841  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207075841  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208012257                      G�O�G�O�G�O�                