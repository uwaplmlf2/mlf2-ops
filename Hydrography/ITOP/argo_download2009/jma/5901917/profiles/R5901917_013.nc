CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901917 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090804005801  20090807190212                                  2B  A   APEX-SBE 4056                                                   846 @�A-HpB�1   @�A-�+�|@7{"��`B@b 1&�x�1   ARGOS   A   A   A   ?���@�ffAFffA�ffA�  B ��B33B*  B>  BPffBc33Bx��B���B�ffB�  B���B�33B�33B���B˙�B���B�ffB���B�ffB���C��C�fC33C��C  CffC L�C%�C*  C/33C4L�C9  C>ffCB�3CH�CQ� C[  Ce33Cn�3Cx� C�s3C�� C�@ C�@ C�Y�C�L�C�33C�ٚC�� C�@ C��C�@ C��C��C�33C�@ C��C��C�@ C�33C�ffC�Y�C�@ C� C��3D�fD� D�fD��D�fD3D�3D%@ D*�D/3D3��D9L�D>Y�DCs3DH� DMy�DR` DW,�D\�fDa�3Df��Dk�3Dp33Du�3Dz��D�� D��D�` D���D�  D�P D��fD�� D�C3D���D��3D�6fD�y�D�3D�	�D��D�0 D�0 D�ٚD�p D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��A  AS33A���A�ffB  BffB-33BA33BS��BfffB|  B�34B�  B���B�34B���B���B�fgB�34B�fgB�  B�fgB�  B�34C��C�3C  CfgC��C33C!�C%�gC*��C0  C5�C9��C?33CC� CH�gCRL�C[��Cf  Co� CyL�C�ٙC��fC��fC��fC�� C��3C���C�@ C�&fC��fC�� C��fC�s3C CǙ�C̦fC�s3Cր CۦfCߙ�C���C�� C�fC��fC��D��D3D�D  D�DFfD &fD%s3D*L�D/FfD4,�D9� D>��DC�fDH�3DM��DR�3DW` D\ٙDa�fDf��Dk�fDpffDv&fD{  D�ٚD�34D�y�D��gD��D�i�D�� D�	�D�\�D��4D���D�P Dϓ4D�,�D�#4D�6gD�I�D�I�D��4D���D��g11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�1A�VA�bA��
A�A�/A��A�t�A�bA�C�A�O�A���A��!A�+A��A�5?A���A��
A�ĜA���A�  A���A�A�A�^5A���A�r�A��9A��FA���A�G�A���A��`A�C�A�ƨA��DA�G�A�-A�9XA�ȴA���A��A�ĜA�A�-A���A�`BA���A� �A�bNA~�\Az��Au?}Ao�Al-Ai"�Ael�A`��A^�uAU��AQdZAH��AC�#A<��A7"�A3K�A0A�A.jA)t�A#\)A"�!AA~�A	dZ@��`@���@�`B@���@�V@��-@��w@��/@�Q�@��h@��w@���@�p�@�A�@���@��7@{�@y��@u�h@s�@n@i�7@["�@P �@G�@@�9@8��@2-@*M�@$1@5?@x�@��@�@�@	hs@��@(�@X?��D?���?�t�?�S�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�1A�VA�bA��
A�A�/A��A�t�A�bA�C�A�O�A���A��!A�+A��A�5?A���A��
A�ĜA���A�  A���A�A�A�^5A���A�r�A��9A��FA���A�G�A���A��`A�C�A�ƨA��DA�G�A�-A�9XA�ȴA���A��A�ĜA�A�-A���A�`BA���A� �A�bNA~�\Az��Au?}Ao�Al-Ai"�Ael�A`��A^�uAU��AQdZAH��AC�#A<��A7"�A3K�A0A�A.jA)t�A#\)A"�!AA~�A	dZ@��`@���@�`B@���@�V@��-@��w@��/@�Q�@��h@��w@���@�p�@�A�@���@��7@{�@y��@u�h@s�@n@i�7@["�@P �@G�@@�9@8��@2-@*M�@$1@5?@x�@��@�@�@	hs@��@(�@X?��D?���?�t�?�S�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
0!B
/B
0!B
7LB
T�B1B\B�B$�B�B�B{B%�B7LB2-B-B�B�B�B{BbBJBDB�B�B!�B&�B/B6FB<jB>wB@�B<jB=qB8RB7LB2-B,B'�B,B �BhBB
��B
�B
�#B
��B
�jB
�B
��B
�VB
r�B
\)B
J�B
:^B
%�B
oB
B	�/B	ǮB	��B	�1B	hsB	M�B	>wB	/B	$�B	JB��B�BŢB��B�JBjBVBD�BH�BI�BaHBm�By�B�{B�BƨB��B�mB	B	�B	&�B	:^B	A�B	L�B	Q�B	ffB	r�B	��B	�'B	ɺB	�
B	�`B	��B
VB
�B
$�B
/B
:^B
J�B
O�B
XB
]/B
ffB
jB
r�B
v�B
y�B
z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
0!B
/B
0!B
7LB
T�B1B\B�B$�B�B�B{B%�B7LB2-B-B�B�B�B{BbBJBDB�B�B!�B&�B/B6FB<jB>wB@�B<jB=qB8RB7LB2-B,B'�B,B �BhBB
��B
�B
�#B
��B
�jB
�B
��B
�VB
r�B
\)B
J�B
:^B
%�B
oB
B	�/B	ǮB	��B	�1B	hsB	M�B	>wB	/B	$�B	JB��B�BŢB��B�JBjBVBD�BH�BI�BaHBm�By�B�{B�BƨB��B�mB	B	�B	&�B	:^B	A�B	L�B	Q�B	ffB	r�B	��B	�'B	ɺB	�
B	�`B	��B
VB
�B
$�B
/B
:^B
J�B
O�B
XB
]/B
ffB
jB
r�B
v�B
y�B
z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090804005759  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090804005801  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090804005801  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090804005802  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090804005803  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090804005803  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090804005803  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090804005803  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090804005803  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090804010238                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090807185747  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090807185850  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090807185850  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090807185851  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090807185852  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090807185852  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090807185852  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090807185852  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090807185852  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090807190212                      G�O�G�O�G�O�                