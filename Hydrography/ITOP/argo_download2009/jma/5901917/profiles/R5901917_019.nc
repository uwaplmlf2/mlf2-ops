CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901917 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091004095614  20091014100328                                  2B  A   APEX-SBE 4056                                                   846 @�P3��R�1   @�P3����@6ܬ1&�@bVvȴ9X1   ARGOS   F   F   F   A�  B  BffB4  BG��B\  Bk33B���B���B�  B�ffB���B�  B�  B�  B�ffB�33B���B�  B�  C�3CL�C��C��C� C��C  C$33C)L�C.33C2�fC7�3C=� CA��CF� CK� CP� CV� CX�3C]  Ci� Cs33C}33C�� C��3C���C�Y�C�@ C�Y�C���C��3C���C�� C��C�@ C�33C�  C�@ C�&fC��C��C�ٚC��C�ٚC���C�33C��3C��fC���D9�D,�DL�D�DٚD&fD 9�D%3D*  D/3D43D9�D=�3DB�3DG��DL�3DQ��DV��D[�fD`� De�fDjٚDo�fDt�3Dy�fD~��D��3D��3D�fD�\�D��fD��3D�3D�FfD���D���D�  D�P DЃ3D֠ D��fD�)�D�l�D�fD���D�ff4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 B��B��B2  BG��B[34Bo��B~��B���B�fgB���B�33B���B���B���B���B�33B�  BB���CffC��C33C� C�3CffC� C#�fC)�C.33C3�C7��C<��CBffCF� CKffCPffCUffC[ffC]��Ca�fCnffCx�C��C��3C�&fC��C���C��3C���C�  C�ffC�  C��3C���C��3C��fC�s3C˳3CЙ�CՀ Cڀ C�L�C� C�L�C�@ C�fC�ffC��D  Ds4DfgD�gDFgD4D` D!s4D&L�D+Y�D0L�D5L�D:S4D?,�DD,�DIgDN�DSgDXgD]  Db�Dg  Dl4Dq  Du��Dz� D�4D�@ D�p D��3D���D�33D�` D�� D��3D�&gD�VgDĜ�D���D�  D�<�D݃3D��gD�	�D�S3D�i�D�34444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�z�Aܲ-A�VAҾwA��wA�7LA��A��A�+A�C�A���A��uA���A�p�A� �A�5?A���A���A��A�
=A���A�A�|�A�"�A�9XA���A�ffA��A��A�XA�I�A���A��/A�~�A�
=A�z�A�
=A��jA��A��A��TA��A��\A��#A�
=A�~�A�ffA~�jAx1Ar(�Anv�Ah�\AdE�A^�/A\��AYS�AV��AO��AJ��AEO�A@��A<r�A6bA2�A,�\A)��A#`BA�AjA�A�@�bN@���@�M�@��@�$�@���@��@�@�{@��/@���@�-@�E�@�o@�ƨ@���@��m@��7@��@y�#@w�@s��@mV@g�@Y�^@M�T@A��@:�!@4j@/l�@'�@!x�@�m@;d@��@l�@I�@ �@/@��?��h?�x�?�?�t�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 A�z�Aܲ-A�VAҾwA��wA�7LA��A��A�+A�C�A���A��uA���A�p�A� �A�5?A���A���A��A�
=A���A�A�|�A�"�A�9XA���A�ffA��A��A�XA�I�A���A��/A�~�A�
=A�z�A�
=A��jA��A��A��TA��A��\A��#A�
=A�~�A�ffA~�jAx1Ar(�Anv�Ah�\AdE�A^�/A\��AYS�AV��AO��AJ��AEO�A@��A<r�A6bA2�A,�\A)��A#`BA�AjA�A�@�bN@���@�M�@��@�$�@���@��@�@�{@��/@���@�-@�E�@�o@�ƨ@���@��m@��7@��@y�#@w�@s��@mV@g�@Y�^@M�T@A��@:�!@4j@/l�@'�@!x�@�m@;d@��@l�@I�@ �@/@��?��h?�x�?�?�t�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
ƨB
��B
��B
��B
��BBM�BZBcTBXBK�BC�BF�B<jB.B,B�B�B1BB
��BhB
��B
�B
�B
�B
�/B
�/B
�NB
�fB
�B
��B
��B
��B
�B
��B
��B
��BB
��B
��B
�B
�ZB
��B
ĜB
�-B
��B
� B
dZB
Q�B
6FB
�B
1B	��B	�B	�5B	�wB	��B	�VB	z�B	dZB	J�B	5?B	�B	PB�B�5B��B�?B�Bt�BgmBXBaHBW
BZB`BBk�B� B��B�XB��B�BB�B	VB	�B	6FB	C�B	D�B	iyB	r�B	� B	�B	�bB	��B	�LB	��B	�5B	�B	��B
	7B
�B
-B
<jB
F�B
L�B
R�B
T�B
^5B
dZB
k�B
p�B
t�B
{�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 B
�B
ƨB
��B
��B
��B
��BBM�BZBcTBXBK�BC�BF�B<jB.B,B�B�B1BB
��BhB
��B
�B
�B
�B
�/B
�/B
�NB
�fB
�B
��B
��B
��B
�B
��B
��B
��BB
��B
��B
�B
�ZB
��B
ĜB
�-B
��B
� B
dZB
Q�B
6FB
�B
1B	��B	�B	�5B	�wB	��B	�VB	z�B	dZB	J�B	5?B	�B	PB�B�5B��B�?B�Bt�BgmBXBaHBW
BZB`BBk�B� B��B�XB��B�BB�B	VB	�B	6FB	C�B	D�B	iyB	r�B	� B	�B	�bB	��B	�LB	��B	�5B	�B	��B
	7B
�B
-B
<jB
F�B
L�B
R�B
T�B
^5B
dZB
k�B
p�B
t�B
{�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20091004095613  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091004095614  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091004095614  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091004095615  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091004095616  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091004095616  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091004095616  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091004095616  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091004095616  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091004100030                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091006185708  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091006185811  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091006185812  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091006185812  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091006185813  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091006185813  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091006185813  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091006185813  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091006185813  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091006190311                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091006185708  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091014075156  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091014075156  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091014075156  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091014075158  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091014075158  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091014075158  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20091014075158  QCF$                G�O�G�O�G�O�            8000JA  ARGQaqcp2.8b                                                                20091014075158  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091014075158  QCF$                G�O�G�O�G�O�            8000JA  ARGQrqcpt16b                                                                20091014075158  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091014100328                      G�O�G�O�G�O�                