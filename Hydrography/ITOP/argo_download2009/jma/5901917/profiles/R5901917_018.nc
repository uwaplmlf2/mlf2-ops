CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901917 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090924095630  20091204075610                                  2B  A   APEX-SBE 4056                                                   846 @�M����1   @�M���t�@6��E��@bT��E�1   ARGOS   F   F   F   A�  A���B ��B  B(  B=��BO33Bc33Bq33B�ffB�ffB���B�ffB�  B�33B�  B�  B�ffB�  B�ffB�B�33C  C��CL�C� C�3C33C!�3C&� C+ffC1ffC3��C8�C@  CD�3CI� CNL�CS33CX�Ca�fCk�fCvL�C��C��fC��C���C���C��C��3C��C�ٚC��C��3C�� C�� C��fCř�C�ffC���CԦfC��3CަfC�3C虚C��3C� C�L�C�33D �3D��D
�fD��D��D�3Ds3D#�fD(�fD-��D2��D7s3D<` DA� DFY�DK` DP9�DUFfDZy�D_l�DdffDi9�DmS3DsFfDxS3D}` D��D��D�FfD���D��3D�� D�I�D�|�D�� D��3D�C3Dɐ D��3D�	�D�<�D�p D��3D�� D�,�D���4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 A�33A�  BfgB'��B;��BQ34Bb��Bv��B�ffB�33B�33B�fgB�33B���B�  B���B���B�33B���B�33B�fgC� C�fC� C33CffC��C"�C&��C+ffC0L�C6L�C8� C=  CD�fCI��CNffCS33CX�C]  Cf��Cp��C{33C�@ C�Y�C�� C�@ C�@ C�� C�ffC���C�L�C�� C�ffC�33C�33C��C��C�ٙC�@ C��C�ffC��C�&fC��C�&fC��3C�� C��fD��D�4D� D�gD�gD��D��D$� D)� D.�gD3�4D8��D=��DB��DG�4DL��DQs4DV� D[�4D`�gDe� Djs4Dn��Dt� Dy��D~��D��gD���D��3D�9�D�p D�<�D��gD��D�l�D�� D�� D�,�D�` D֦gD�ٚD��D�` D�|�D�ɚD�Fg4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�bNA盦A��A���A���A�  A�=qA�E�A�G�A���A�ȴA���A�v�A��/A��FA��
A�33A��A�7LA�z�A��A��A�l�A��jA��9A�K�A���A�bNA��FA��jA�ȴA�1A��A�  A��A��yA��A��HA�A�ĜA���A��hA��A�ȴA��FA���A�
=A| �Ax��At��Aq�Ak�FAe�-A`JA[p�AU�-AP�jAK�^AF�A@VA:VA6�A.r�A&M�A v�A�A\)A��A��A;dA�@���@��@�V@�v�@�E�@�@�{@��@��!@�t�@���@���@���@��`@��!@�ȴ@}p�@w;d@s�
@m�h@j��@eO�@d�j@bM�@YX@Q��@G
=@A��@9X@1��@+�
@$�/@�w@@1'@�@�@�D@�@��@�H?���?��?�1'4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 A�bNA盦A��A���A���A�  A�=qA�E�A�G�A���A�ȴA���A�v�A��/A��FA��
A�33A��A�7LA�z�A��A��A�l�A��jA��9A�K�A���A�bNA��FA��jA�ȴA�1A��A�  A��A��yA��A��HA�A�ĜA���A��hA��A�ȴA��FA���A�
=A| �Ax��At��Aq�Ak�FAe�-A`JA[p�AU�-AP�jAK�^AF�A@VA:VA6�A.r�A&M�A v�A�A\)A��A��A;dA�@���@��@�V@�v�@�E�@�@�{@��@��!@�t�@���@���@���@��`@��!@�ȴ@}p�@w;d@s�
@m�h@j��@eO�@d�j@bM�@YX@Q��@G
=@A��@9X@1��@+�
@$�/@�w@@1'@�@�@�D@�@��@�H?���?��?�1'4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�oB
�'B
�B
��B'�B1'B\)Bt�BM�BL�B+B/B'�B'�B.B-B1'B"�B�BhB�B�B!�B&�B.B,B)�B$�B �B�B�B{BbBPB
=B	7B1BB
��B
��B
��B
�B
�`B
�/B
��B
ɺB
�!B
�oB
�B
n�B
_;B
C�B
(�B
PB	��B	�)B	��B	�B	��B	v�B	XB	G�B	�B��B�ZB�B��B�jB�!B��B�1Bm�B`BBT�BW
BaHBu�Bo�B�+B�uB��B�FB�wB��B�fB	B	�B	.B	B�B	N�B	_;B	iyB	x�B	~�B	�%B	��B	�qB	��B	�yB	�B
B
PB
�B
�B
,B
33B
C�B
L�B
XB
_;B
bNB
iyB
o�B
s�B
v�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 B
�oB
�'B
�B
��B'�B1'B\)Bt�BM�BL�B+B/B'�B'�B.B-B1'B"�B�BhB�B�B!�B&�B.B,B)�B$�B �B�B�B{BbBPB
=B	7B1BB
��B
��B
��B
�B
�`B
�/B
��B
ɺB
�!B
�oB
�B
n�B
_;B
C�B
(�B
PB	��B	�)B	��B	�B	��B	v�B	XB	G�B	�B��B�ZB�B��B�jB�!B��B�1Bm�B`BBT�BW
BaHBu�Bo�B�+B�uB��B�FB�wB��B�fB	B	�B	.B	B�B	N�B	_;B	iyB	x�B	~�B	�%B	��B	�qB	��B	�yB	�B
B
PB
�B
�B
,B
33B
C�B
L�B
XB
_;B
bNB
iyB
o�B
s�B
v�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090924095629  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090924095630  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090924095630  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090924095630  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090924095632  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090924095632  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090924095632  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090924095632  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090924095632  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090924100106                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090926185718  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090926185838  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090926185838  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090926185839  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090926185840  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090926185840  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090926185840  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090926185840  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090926185840  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090926190353                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090926185718  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091014075153  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091014075153  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091014075154  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091014075155  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091014075155  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091014075155  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20091014075155  QCF$                G�O�G�O�G�O�            8000JA  ARGQaqcp2.8b                                                                20091014075155  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091014075155  QCF$                G�O�G�O�G�O�            8000JA  ARGQrqcpt16b                                                                20091014075155  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091014100424                      G�O�G�O�G�O�                JA  ARFMdecpA14d                                                                20091204074803  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204074842  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204074843  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204074843  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204074844  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204074844  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204074844  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8c                                                                20091204074844  QCF$                G�O�G�O�G�O�            8000JA  ARGQaqcp2.8c                                                                20091204074844  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204074844  QCF$                G�O�G�O�G�O�            8000JA  ARGQrqcpt16b                                                                20091204074845  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204075610                      G�O�G�O�G�O�                