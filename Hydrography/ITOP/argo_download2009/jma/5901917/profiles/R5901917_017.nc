CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901917 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090913005648  20091014100105                                  2B  A   APEX-SBE 4056                                                   846 @�K-%��Y1   @�K-JU�l@7KC��%@bM&�x��1   ARGOS   F   F   F   A�  A�33A���A���BffB%33B:  BM��Ba33BnffB�33B���B�ffB�  B���B�33B�33B�33B�ffB�33B�ffB�ffB�ffC�3C�fC33C33C33C� C"  C&��C,L�C0�3C6�C:�3C?L�CD��CI�3CNffCX�C`�fCl��Cv� C�33C�L�C�Y�C�L�C�Y�C��C�33C�Y�C���C�Y�C�s3C�ffC�� C�� Cƙ�Cˌ�CЌ�C�ffCڌ�C�@ C�  C�33C�ٚC�ffC���C���D��D�fDL�D��D��D�fD!��D&s3D+s3D0Y�D5�3D:� D?�3DDs3DIl�DN��DR�3DX�3D]y�Dby�Dg` Dl�3DqffDv�fD{� D�fD�9�D�` D���D���D�fD�vfD���D�� D�)�D�\�Dȩ�D��3D��D�S3DቚD�ɚD��D�I�D�� D�� 4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 A�  A�33A���BffBffB/33BD  BW��Bk33BxffB�33B���B�ffB�  B���B�33B�33B�33B�ffB�33B�ffB�ffC33C33CffC�3C�3C�3C  C$� C)L�C.��C333C8��C=33CA��CGL�CL33CP�fCZ��CcffCo�Cy  C�s3C���C���C���C���C�L�C�s3C���C�ٚC���C��3C��fC�� C�  C�ٚC���C���C֦fC���C�� C�@ C�s3C��C��fC���C�ٚDL�D	&fD��DY�D9�DFfD"L�D'3D,3D0��D633D;@ D@33DE3DJ�DO,�DSs3DY33D^�Dc�Dh  Dm33DrfDw&fD|  D�VfD���D�� D���D�L�D�ffD��fD���D�0 D�y�D¬�D���D�#3D�l�Dۣ3D�ٚD��D�\�D���D�@ D�� 4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A�ȴA�  A�dZA��A�?}A��;A���A�r�A��A�n�A�ƨA�x�A�n�A�~�A���A�p�A�~�A���A�n�A��HA�|�A���A��A�O�A�\)A��/A��hA��PA�ƨA��jA��A��yA�$�A��\A�hsA���A�=qA��A�|�A��A��A�(�A�VA��jA�bNA�hsA�+A�+A&�A{Ax�HAu+An��Aj�HAe�-Ab9XA[dZAU��AR��ALv�AE�^AA�A;�A2A+p�A"ffA��Av�A�A��A @�r�@�n�@��@�%@�&�@���@��@���@��H@�t�@��@���@���@�O�@�j@��@���@}�h@|��@s��@n@e��@]�@S�F@J�@>��@6�y@0��@)�7@!�@�@Ĝ@�D@�7@�@	��@V@S�@X?���?�r�?�S�?�M�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 A���A�ȴA�  A�dZA��A�?}A��;A���A�r�A��A�n�A�ƨA�x�A�n�A�~�A���A�p�A�~�A���A�n�A��HA�|�A���A��A�O�A�\)A��/A��hA��PA�ƨA��jA��A��yA�$�A��\A�hsA���A�=qA��A�|�A��A��A�(�A�VA��jA�bNA�hsA�+A�+A&�A{Ax�HAu+An��Aj�HAe�-Ab9XA[dZAU��AR��ALv�AE�^AA�A;�A2A+p�A"ffA��Av�A�A��A @�r�@�n�@��@�%@�&�@���@��@���@��H@�t�@��@���@���@�O�@�j@��@���@}�h@|��@s��@n@e��@]�@S�F@J�@>��@6�y@0��@)�7@!�@�@Ĝ@�D@�7@�@	��@V@S�@X?���?�r�?�S�?�M�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
�B
��B
�B
ĜB
�5BB{B�B�B�B)�B"�BZBp�B� By�BVBK�B5?B)�B2-B'�B.B-B2-B#�B$�B$�BbB
�5B
��B
��B
��B
ǮB
��B
��B
�B
�)B
�TB
�BbBB
��B
�B
�ZB
�B
ÖB
�-B
��B
�bB
�B
p�B
S�B
>wB
$�B
oB	�B	�B	ǮB	�B	�=B	v�B	ZB	2-B	{B�B�
B�jB��B�JBq�BdZBhsBgmBk�Bl�Bo�B{�B�%B�=B��B�?B�B�yB�B��B	
=B	�B	7LB	ZB	_;B	u�B	� B	��B	�B	�}B	��B	�sB	��B
+B
�B
�B
'�B
49B
9XB
E�B
N�B
XB
_;B
ffB
jB
p�B
v�B
x�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 B
�B
�B
��B
�B
ĜB
�5BB{B�B�B�B)�B"�BZBp�B� By�BVBK�B5?B)�B2-B'�B.B-B2-B#�B$�B$�BbB
�5B
��B
��B
��B
ǮB
��B
��B
�B
�)B
�TB
�BbBB
��B
�B
�ZB
�B
ÖB
�-B
��B
�bB
�B
p�B
S�B
>wB
$�B
oB	�B	�B	ǮB	�B	�=B	v�B	ZB	2-B	{B�B�
B�jB��B�JBq�BdZBhsBgmBk�Bl�Bo�B{�B�%B�=B��B�?B�B�yB�B��B	
=B	�B	7LB	ZB	_;B	u�B	� B	��B	�B	�}B	��B	�sB	��B
+B
�B
�B
'�B
49B
9XB
E�B
N�B
XB
_;B
ffB
jB
p�B
v�B
x�4444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090913005647  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090913005648  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090913005648  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090913005649  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090913005650  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090913005650  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090913005650  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090913005650  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090913005650  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090913010122                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090916185709  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090916185814  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090916185814  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090916185815  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090916185816  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090916185816  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090916185816  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090916185816  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090916185816  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090916190215                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090916185709  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091014075151  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091014075151  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091014075151  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091014075153  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091014075153  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091014075153  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20091014075153  QCF$                G�O�G�O�G�O�            8000JA  ARGQaqcp2.8b                                                                20091014075153  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091014075153  QCF$                G�O�G�O�G�O�            8000JA  ARGQrqcpt16b                                                                20091014075153  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091014100105                      G�O�G�O�G�O�                