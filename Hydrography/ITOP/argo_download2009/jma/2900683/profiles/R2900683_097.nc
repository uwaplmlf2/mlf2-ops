CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900683 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               aA   JA  20090913215546  20090917190206                                  2B  A   APEX-SBE 2821                                                   846 @�KlY��b1   @�Kl��#�@=�bM��@a���
=q1   ARGOS   A   A   A   @�  A  Ah  A���A�  A陚B
��B��B333BE��BY33Bn  B���B���B�33B�  B���B���B���Bƙ�B�33B�  B�  B�  B���C  C33C  C� C�fC�C33C$L�C(��C.  C333C7��C=33CB�CG  CP�fC[33CeffCo� CyL�C��fC��3C���C��fC���C�� C�� C�� C�s3C���C��3C�� C���C�CǦfC̙�CѦfC�s3Cی�C�fC�ffC�fC�ffC���C�� DٚD�fD� D�3D�fD�fD��D$� D)ٚD.� D3ٚD8�3D=� DB��DG��DL� DQٚDV�fD[� D`� De�3Dj�fDo�3Dt��Dy� D�)�D�` D��fD��fD��D�ffD�� D��D��D�\�D�� D��fD�#3D�ffDڣ3D��3D�)�D�` D�D�Vf1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A  Ah  A���A�  A陚B
��B��B333BE��BY33Bn  B���B���B�33B�  B���B���B���Bƙ�B�33B�  B�  B�  B���C  C33C  C� C�fC�C33C$L�C(��C.  C333C7��C=33CB�CG  CP�fC[33CeffCo� CyL�C��fC��3C���C��fC���C�� C�� C�� C�s3C���C��3C�� C���C�CǦfC̙�CѦfC�s3Cی�C�fC�ffC�fC�ffC���C�� DٚD�fD� D�3D�fD�fD��D$� D)ٚD.� D3ٚD8�3D=� DB��DG��DL� DQٚDV�fD[� D`� De�3Dj�fDo�3Dt��Dy� D�)�D�` D��fD��fD��D�ffD�� D��D��D�\�D�� D��fD�#3D�ffDڣ3D��3D�)�D�` D�D�Vf1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�VA�VA�JAفA��A�  AЕ�A�S�A���A�x�A���A���A�x�A���A� �A�-A��DA�ƨA���A��
A��A��DA�A�A�+A�;dA�K�A��+A�`BA��hA�C�A���A��A�?}A�A���A�-A��\A���A���A�I�A�z�A�7LA��A��^A��mA���A��uA�=qA�-A���A��mA�XA��AzZAwK�As�An{Ai7LAb��A`�`AU�7AS��AQ�FALZAH$�A7`BA5��A4�A4�jA2^5A ��AM�A�wA=qA`BA��@�z�@웦@�x�@�O�@�
=@���@�
=@�  @�J@�  @�bN@���@���@��u@���@�K�@� �@��j@��h@x �@f��@Y�#@QG�@I�7@B�@9G�@/+@+�
@%�@$�@�@�m@�u@�@	��@j@ Q�?�(�?��u1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�VA�VA�JAفA��A�  AЕ�A�S�A���A�x�A���A���A�x�A���A� �A�-A��DA�ƨA���A��
A��A��DA�A�A�+A�;dA�K�A��+A�`BA��hA�C�A���A��A�?}A�A���A�-A��\A���A���A�I�A�z�A�7LA��A��^A��mA���A��uA�=qA�-A���A��mA�XA��AzZAwK�As�An{Ai7LAb��A`�`AU�7AS��AQ�FALZAH$�A7`BA5��A4�A4�jA2^5A ��AM�A�wA=qA`BA��@�z�@웦@�x�@�O�@�
=@���@�
=@�  @�J@�  @�bN@���@���@��u@���@�K�@� �@��j@��h@x �@f��@Y�#@QG�@I�7@B�@9G�@/+@+�
@%�@$�@�@�m@�u@�@	��@j@ Q�?�(�?��u1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	=qB	=qB	<jB	:^B	s�B	�B	��B	�3B	��B
�B
C�B
�B
��B
��B
�B
�9B
ƨB
ɺB
��B
��B
��B
��B
��B
��B
�B
��B
��B
��B
��B
��B
��B
��B
��B
�/B
�TB
�TB
�`B
�yB
�mB
�B
�B
�NB
�NB
�HB
�BB
�sB
�mB
�;B
�
B
��B
�}B
�!B
��B
�B
u�B
dZB
L�B
9XB
�B
{B	�sB	�HB	�B	ÖB	��B	gmB	bNB	\)B	]/B	Q�B	PB��B�B�yB�TB��B��B��B��B��B�uB�uB�uB��B��B�B�?B�
B�B�mB�B��B	1B	uB	.B	@�B	iyB	�%B	��B	��B	�XB	��B	�`B	�B	��B
PB
�B
%�B
/B
7LB
@�B
L�B
W
B
]/B
aH1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	=qB	=qB	<jB	:^B	s�B	�B	��B	�3B	��B
�B
C�B
�B
��B
��B
�B
�9B
ƨB
ɺB
��B
��B
��B
��B
��B
��B
�B
��B
��B
��B
��B
��B
��B
��B
��B
�/B
�TB
�TB
�`B
�yB
�mB
�B
�B
�NB
�NB
�HB
�BB
�sB
�mB
�;B
�
B
��B
�}B
�!B
��B
�B
u�B
dZB
L�B
9XB
�B
{B	�sB	�HB	�B	ÖB	��B	gmB	bNB	\)B	]/B	Q�B	PB��B�B�yB�TB��B��B��B��B��B�uB�uB�uB��B��B�B�?B�
B�B�mB�B��B	1B	uB	.B	@�B	iyB	�%B	��B	��B	�XB	��B	�`B	�B	��B
PB
�B
%�B
/B
7LB
@�B
L�B
W
B
]/B
aH1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090913215545  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090913215546  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090913215547  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090913215547  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090913215548  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090913215548  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090913215548  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090913215548  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090913215548  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090913215548  QCF$                G�O�G�O�G�O�            1000JA  ARGQaqcp2.8b                                                                20090913215548  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090913215548  QCF$                G�O�G�O�G�O�            1000JA  ARGQrqcpt16b                                                                20090913215548  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090913220128                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090917185605  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090917185809  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090917185810  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090917185810  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090917185811  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090917185811  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090917185811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090917185811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090917185811  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090917190206                      G�O�G�O�G�O�                