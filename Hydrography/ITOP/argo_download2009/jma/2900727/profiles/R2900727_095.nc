CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   3   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  9h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  9�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  :h   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  :�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;h   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  <4   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  <h   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  =4   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  >4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  ?    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ?4   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  @    PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  @4   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  A    DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  A�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  B    DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  B�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  C    	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    L   CALIBRATION_DATE            	             
_FillValue                  8  P   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    PD   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    PH   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    PL   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    PP   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  PT   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    P�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    P�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    P�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         P�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         P�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        P�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    P�Argo profile    2.2 1.2 19500101000000  2900727 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL            DOXY               _A   JA  20090818005654  20090821160639                                  2B  A   APEX-SBE 3381                                                   846 @�D�~ܺ�1   @�D���%@8�fffff@b)?|�h1   ARGOS   A   A   A       @陚A0  Aq��A�33A�ffB33B933B^��B�ffB���B�33B���B���B�  B�33C�CL�C��C&  C/�3C9  CC33CM33CW�CaL�Cj��Cu��C�  C�� C��3C�ٚC�� C���C��3C�ffC�ffC�s3C�� C�33C�&fC��3C�� C�� Cό�Cԙ�Cٙ�C��C�L�C��C��C�&f111111111111111111111111111111111111111111111111111 @�fgA.ffAp  A�ffAљ�B��B8��B^fgB�33B���B�  B���Bә�B���B�  C  C33C�3C%�fC/��C8�fCC�CM�CW  Ca33Cj� Cu� C�fC��3C��fC���C��3C���C��fC�Y�C�Y�C�ffC�s3C�&fC��C��fCĳ3Cʳ3Cπ CԌ�Cٌ�C��C�@ C� C��C��111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A��A��A�  Aݗ�A�\)A�?}A�"�A�1'A���A�VA�hsA���A��wA�O�A�I�A��A�A�z�A��A��FA�&�A�~�A���A�A�A��wA�?}A���A�=qA���A�5?A�ZA��TA��+A��^A���A}\)A{oAw��Ap�HAo+Af�/A`�!A\�jAVz�ANn�AJ�\AE�A@�DA<�111111111111111111111111111111111111111111111111111 A��A��A��A��A�  Aݗ�A�\)A�?}A�"�A�1'A���A�VA�hsA���A��wA�O�A�I�A��A�A�z�A��A��FA�&�A�~�A���A�A�A��wA�?}A���A�=qA���A�5?A�ZA��TA��+A��^A���A}\)A{oAw��Ap�HAo+Af�/A`�!A\�jAVz�ANn�AJ�\AE�A@�DA<�111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
� B
�B
�B
�B
�B
~�B
�B
�B
�B
�mB
�HB
��B
��B
�
B
ɺB
ǮB
ȴB
�B
�yB
�mB
�mB
�mB
�B
�`B
�NB
�HB
�BB
�NB
�BB
��B
��B
ŢB
�qB
�XB
�9B
��B
�oB
}�B
q�B
bNB
B�B
:^B
uB	��B	�NB	ƨB	��B	�PB	x�B	^5B	L�111111111111111111111111111111111111111111111111111 B
� B
�B
�B
�B
�B
~�B
�B
�B
�B
�mB
�HB
��B
��B
�
B
ɺB
ǮB
ȴB
�B
�yB
�mB
�mB
�mB
�B
�`B
�NB
�HB
�BB
�NB
�BB
��B
��B
ŢB
�qB
�XB
�9B
��B
�oB
}�B
q�B
bNB
B�B
:^B
uB	��B	�NB	ƨB	��B	�PB	x�B	^5B	L�111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C<|�C<�uC=W
C:�sC:�CH��CRCW~�CV��CYD�CU�BCU�XCR�;CM�CM?;CI�}CH��CE?CC��CC��CDY�CC�^CC��CEc�CEG�CDM�CA�7C@�CA�CA�!CB�CB�CA�PC@��C@�C=�sC;��C;��C;bC9��C9
=C7�C7=�C7b�C7XRC7/\C6T�C62�C5�C52oC3�N000000000000000000000000000000000000000000000000000 C<|�C<�uC=W
C:�sC:�CH��CRCW~�CV��CYD�CU�BCU�XCR�;CM�CM?;CI�}CH��CE?CC��CC��CDY�CC�^CC��CEc�CEG�CDM�CA�7C@�CA�CA�!CB�CB�CA�PC@��C@�C=�sC;��C;��C;bC9��C9
=C7�C7=�C7b�C7XRC7/\C6T�C62�C5�C52oC3�N000000000000000000000000000000000000000000000000000 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA12a                                                                20090818005652  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090818005654  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090818005654  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090818005655  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090818005656  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090818005656  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090818005656  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090818005656  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090818005656  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090818010401                      G�O�G�O�G�O�                JA  ARFMdecpA12a                                                                20090821155834  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090821160052  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090821160052  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090821160053  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090821160054  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090821160054  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090821160054  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090821160054  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090821160054  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090821160639                      G�O�G�O�G�O�                