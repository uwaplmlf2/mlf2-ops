CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   3   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  9h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  9�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  :h   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���      �  :�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ;h   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  <4   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  <h   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  =4   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  >4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  ?    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  ?4   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  @    PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  @4   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  A    DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  A�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  B    DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  4  B�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o      �  C    	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    L   CALIBRATION_DATE            	             
_FillValue                  8  P   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    PD   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    PH   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    PL   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    PP   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  PT   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    P�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    P�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    P�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         P�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         P�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        P�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    P�Argo profile    2.2 1.2 19500101000000  2900727 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL            DOXY               iA   JA  20091007095618  20091010160435                                  2B  A   APEX-SBE 3381                                                   846 @�Q*��Q�1   @�Q*�T�'@8��hr�@a�I�^5?1   ARGOS   A   A   A       @�ffA4��A{33A�ffA�  B  B933Ba��B�33B���B�  B���Bә�B�  B�33CffCL�CffC%�3C/��C9  CB�fCMffCW��Ca  CjL�CuL�C  C�33C���C��3C��fC�ٚC��fC���C��3C��fC��fC���C��fC��3Cŀ C�ffC�L�C�ffC�L�C�Y�C�ffC���C�  C��111111111111111111111111111111111111111111111111111 @�33A333Ay��A���A�33B��B8��Ba34B�  B�fgB���B�fgB�fgB���B�  CL�C33CL�C%��C/�3C8�fCB��CML�CW� C`�fCj33Cu33C~�fC�&fC�� C��fC���C���C���C�� C��fC���C�ٙC�� C���C��fC�s3C�Y�C�@ C�Y�C�@ C�L�C�Y�C�� C��3C��111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�jA�A�A�ƨA���A���A�9A�JA��
A�XA��/A�XA�O�A�n�A�33A�ȴA�p�A�$�A�$�A�ĜA��7A�VA�+A�A�A��TA���A��A��A���A�r�A��-A~ �Av��AsS�Ap�AkO�Ah��AgVAc;dA_hsA[dZAV��AO��AC�AA�A<�`A:1'A7��A1��A,��A);d111111111111111111111111111111111111111111111111111 A�jA�A�A�ƨA���A���A�9A�JA��
A�XA��/A�XA�O�A�n�A�33A�ȴA�p�A�$�A�$�A�ĜA��7A�VA�+A�A�A��TA���A��A��A���A�r�A��-A~ �Av��AsS�Ap�AkO�Ah��AgVAc;dA_hsA[dZAV��AO��AC�AA�A<�`A:1'A7��A1��A,��A);d111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
ɺB
ɺB
��B
��B
��B
��B
��B
�XB
��B+BDB+BBB
��B
��B
��B
��B
��B
��B
�B
�B
�mB
�;B
�
B
��B
ƨB
��B
�RB
��B
�hB
� B
]/B
K�B
?}B
%�B
�B
{B
  B	�B	�B	ÖB	��B	q�B	_;B	K�B	?}B	33B	�B	B�111111111111111111111111111111111111111111111111111 B
ɺB
ɺB
��B
��B
��B
��B
��B
�XB
��B+BDB+BBB
��B
��B
��B
��B
��B
��B
�B
�B
�mB
�;B
�
B
��B
ƨB
��B
�RB
��B
�hB
� B
]/B
K�B
?}B
%�B
�B
{B
  B	�B	�B	ÖB	��B	q�B	_;B	K�B	?}B	33B	�B	B�111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C:bNC<��C=h�C>;C>�PC?T�C<�{C4��CQ �C]��C^
�CY�7CSz�CN��CF��CA��C@(1C?��C=� C=��C>F%C>�C?�RC?�XCA.VCAk�CAjCAGmC?�oC>.VC=�C=;C<<�C<0�C;��C;I�C:ՁC:/�C:=�C9��C9uC8]�C8�LC7GmC7ZC5��C5FC4�C2��C1K�C/�R000000000000000000000000000000000000000000000000000 C:bNC<��C=h�C>;C>�PC?T�C<�{C4��CQ �C]��C^
�CY�7CSz�CN��CF��CA��C@(1C?��C=� C=��C>F%C>�C?�RC?�XCA.VCAk�CAjCAGmC?�oC>.VC=�C=;C<<�C<0�C;��C;I�C:ՁC:/�C:=�C9��C9uC8]�C8�LC7GmC7ZC5��C5FC4�C2��C1K�C/�R000000000000000000000000000000000000000000000000000 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA12a                                                                20091007095617  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091007095618  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091007095619  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091007095619  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091007095620  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091007095620  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091007095620  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091007095620  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091007095620  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091007100218                      G�O�G�O�G�O�                JA  ARFMdecpA12a                                                                20091010155740  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091010155948  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091010155948  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091010155949  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091010155950  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091010155950  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091010155950  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091010155950  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091010155950  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091010160435                      G�O�G�O�G�O�                