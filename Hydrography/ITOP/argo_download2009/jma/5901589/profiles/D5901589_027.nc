CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   i   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  C�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Dx   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Mx   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Vx   CALIBRATION_DATE      	   
                
_FillValue                  �  _x   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    _�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    _�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `    HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    `H   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    `X   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    `\   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         `l   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         `p   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        `t   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    `xArgo profile    2.2 1.2 19500101000000  5901589 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091013035755  20100301012433  A14_86524_027                   2C  D   APEX-SBE 4058                                                   846 @�R��З�1   @�R��}��@(��\(��@`�$�/�1   ARGOS   A   A   A   A&ffAs33A�  A���A�33B��B��B5��BJffB[33Bq��B���B�  B�ffB�ffB�33B���B�  B�  B�33Bܙ�B���B���B�33CL�CL�C
�3CffC� C33C� C%� C*�3C/� C433C9�C>  CC� CH� CR�C\L�Ce�fCp� Cz� C�@ C�L�C�33C��fC�33C�33C�&fC��C�&fC��3C��C��3C�&fC��C��3C�&fC�33C��C�L�C�@ C��C��fC��C��fC�33D&fD�D�DfD3D�D �D$� D)�fD/  D4  D9  D>�DC3DG�3DL��DQ�3DW  D\  Da�Df  Dj�3DpfDu3Dy� D�9�D���D���D��3D�0 D���D���D�  D�FfD���D�� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   AffAk33A�  A���A�33B��B��B3��BHffBY33Bo��B���B�  B�ffB�ffB�33B���B�  B�  B�33Bۙ�B���B���B�33C��C��C
33C�fC  C�3C  C%  C*33C/  C3�3C8��C=� CC  CH  CQ��C[��CeffCp  Cz  C�  C��C��3C��fC��3C��3C��fC�ٚC��fC��3C���C��3C��fC�ٚCǳ3C��fC��3C�ٚC��C�  C�ٚC�fC���C��fC��3DfD��D��D�fD�3D��D��D$� D)�fD.� D4  D9  D=��DB�3DG�3DLٚDQ�3DV� D\  D`��De� Dj�3Do�fDt�3Dy� D�)�D�y�D���D��3D�  D�y�D���D�� D�6fD�y�D�� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�VA�;dA�O�A���A�bNA镁A�DA���A癚A�7LA�-A�M�A߼jA�hsA��mA�"�A�Q�A���A�E�A�bAՍPA�+A�1A�jA�S�A�ZA�33A�ZA�l�A�/A���A��A��hA���A�oA�~�A�n�A���A���A�ƨA���A��At�DAq�Ak��AXȴAMAC|�A6�DA0�A+�;A'�A!�hA^5A;dA�AƨA�hAffAȴA^5A�hAr�Al�@��y@���@���@�/@��-@�Z@�r�@�"�@�O�@���@�t�@�p�@�@��y@�  @���@��
@��m@��u@��-@�~�@���@���@�7L@�ff@�K�@���@�p�@�l�@�7L@�J@��j@K�@t9X@k��@`Ĝ@U�@K�@C�F@<��@:�\111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�VA�;dA�O�A���A�bNA镁A�DA���A癚A�7LA�-A�M�A߼jA�hsA��mA�"�A�Q�A���A�E�A�bAՍPA�+A�1A�jA�S�A�ZA�33A�ZA�l�A�/A���A��A��hA���A�oA�~�A�n�A���A���A�ƨA���A��At�DAq�Ak��AXȴAMAC|�A6�DA0�A+�;A'�A!�hA^5A;dA�AƨA�hAffAȴA^5A�hAr�Al�@��y@���@���@�/@��-@�Z@�r�@�"�@�O�@���@�t�@�p�@�@��y@�  @���@��
@��m@��u@��-@�~�@���@���@�7L@�ff@�K�@���@�p�@�l�@�7L@�J@��j@K�@t9X@k��@`Ĝ@U�@K�@C�F@<��@:�\111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	7LB	I�B	P�B	�B	�!B
B
_;B
��B
��B
�qB
�5B�BB+B8RBD�BH�BK�BVBG�BD�BD�BC�BA�B7LB�B�B�B�NB��BĜB�jB�B��B��B�BffBN�B0!BuB
��B
ɺB
��B
P�B
A�B
&�B	�B	�dB	�B	�B	�!B	�-B	�XB	�wB	ÖB	ǮB	��B	��B	��B	��B	ĜB	�qB	B	ƨB	��B	�NB	��B	��B	�B	�B	�B	�B	��B	��B	��B
B
B
%B
1B
	7B
DB
JB
bB
hB
{B
�B
�B
�B
�B
�B
"�B
"�B
&�B
'�B
'�B
,B
49B
5?B
:^B
?}B
D�B
K�B
P�B
VB
ZB
]/111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	7LB	I�B	P�B	�B	�-B
%B
_;B
��B
��B
�qB
�HB�NB-B9XBE�BI�BL�BW
BH�BE�BE�BD�BB�B=qB�B�B�B�`B�
BɺB�wB�-B��B��B�+BiyBQ�B49B�B
��B
��B
��B
Q�B
C�B
+B	�#B	�wB	�!B	�B	�'B	�3B	�dB	�}B	ÖB	ȴB	��B	��B	��B	��B	ƨB	�wB	B	ƨB	��B	�NB	��B	��B	�B	�B	�B	��B	��B	��B	��B
B
B
%B
1B
	7B
DB
JB
bB
hB
{B
�B
�B
�B
�B
�B
"�B
"�B
&�B
'�B
'�B
,B
49B
5?B
:^B
?}B
D�B
K�B
P�B
VB
ZB
]/111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.5(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200910260759562009102607595620091026075956200910260925552009102609255520091026092555201002260000002010022600000020100226000000  JA  ARFMdecpA14                                                                 20091013035754  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091013035755  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091013035756  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091013035756  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091013035757  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091013035757  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091013035757  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091013035757  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091013035757  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091013040516                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091016215702  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091016215821  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091016215821  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091016215822  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091016215823  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091016215823  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091016215823  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091016215823  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091016215823  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091016220539                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20091016171407  CV  DAT$            G�O�G�O�F���                JM  ARGQJMQC1.0                                                                 20091016171407  CV  LAT$            G�O�G�O�ADb                JM  ARGQJMQC1.0                                                                 20091016171407  CV  LON$            G�O�G�O�C��                JM  ARCAJMQC1.0                                                                 20091026075956  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20091026075956  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20091026092555  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100226000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100301012344  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100301012433                      G�O�G�O�G�O�                