CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   i   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  C�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Dx   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Mx   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Vx   CALIBRATION_DATE      	   
                
_FillValue                  �  _x   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    _�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    _�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `    HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    `H   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    `X   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    `\   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         `l   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         `p   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        `t   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    `xArgo profile    2.2 1.2 19500101000000  5901589 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090923035722  20100301012432  A14_86524_025                   2C  D   APEX-SBE 4058                                                   846 @�M���1   @�M�З�+@(      @`��^5?}1   ARGOS   A   A   A   A33Al��A���A�33A홚BffB"ffB533BI��B^  Bq33B�  B�  B�  B���B�33B�  B���B���B�ffB�33B�33B�  B�ffC�fC33C�CffC�CffCffC%�C*��C/ffC4��C9��C>�CC33CHL�CR�C\33Cf��CpffCzL�C��C�L�C��3C��C�  C�@ C�@ C�  C��C�@ C�&fC�33C�s3C��C�@ C�  C�@ C�&fC��C�  C�&fC�33C��C�&fC���D�3D�fDfD  D�D��D��D%  D)�3D/fD43D93D=��DC�DH  DLٚDQ� DV��D[� Da3De�3Dj��DpfDu�Dz�D�I�D��fD�ɚD��3D�FfD���D��3D�fD�6fD��3D��3111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A��AfffA���A�  A�ffB
��B ��B3��BH  B\ffBo��B�33B�33B�33B�  B�ffB�33B�  B�  Bљ�B�ffB�ffB�33B���C� C��C�3C  C�3C  C  C$�3C*33C/  C433C9ffC=�3CB��CG�fCQ�3C[��Cf33Cp  Cy�fC��fC��C�� C��fC���C��C��C���C�ٚC��C��3C�  C�@ C��fC��C���C��C��3C��fC���C��3C�  C�ٚC��3C���DٚD��D��D�fD�3D� D� D$�fD)ٚD.��D3��D8��D=� DC  DG�fDL� DQ�fDV� D[�fD`��DeٚDj� Do��Du  Dz  D�<�D�y�D���D��fD�9�D�|�D��fD���D�)�D�vfD��f111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�A�+A�7A�hA虚A蟾A�-A�RA��
A���A��#A�dZA�G�A�  A�E�A��A��Aܡ�A�"�Aۛ�A��/A�x�A�S�A�VA�bAΙ�A�  A��yA�x�A���A���A�JA�ȴA�1'A���A���A���A�XA��A�ffA���As��A[�^AN-ADȴA<Q�A3ƨA/�A*�!A$  A\)A�FA�AJA��A%A
��A	oA\)A"�AJAG�A�A�A �@�|�@��@�p�@�C�@�p�@�ȴ@�dZ@�I�@�K�@�|�@ˍP@�+@§�@��@���@���@�\)@�X@�\)@�X@�-@��^@���@��@���@�@���@�(�@���@��@���@��9@u�@f�@\9X@T�@L1@Ep�@=p�@:��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�A�+A�7A�hA虚A蟾A�-A�RA��
A���A��#A�dZA�G�A�  A�E�A��A��Aܡ�A�"�Aۛ�A��/A�x�A�S�A�VA�bAΙ�A�  A��yA�x�A���A���A�JA�ȴA�1'A���A���A���A�XA��A�ffA���As��A[�^AN-ADȴA<Q�A3ƨA/�A*�!A$  A\)A�FA�AJA��A%A
��A	oA\)A"�AJAG�A�A�A �@�|�@��@�p�@�C�@�p�@�ȴ@�dZ@�I�@�K�@�|�@ˍP@�+@§�@��@���@���@�\)@�X@�\)@�X@�-@��^@���@��@���@�@���@�(�@���@��@���@��9@u�@f�@\9X@T�@L1@Ep�@=p�@:��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	��B	��B	��B	��B	��B	��B	��B	��B	�-B�+B��BBB�B"�B;dB:^B[#B^5BR�B`BBG�BA�B1'B!�BoB��BuB�yB�B�`B�ZB�BB�B��B�qBo�BI�BPB
�B
n�B
�B	ŢB	�hB	r�B	\)B	[#B	[#B	dZB	M�B	P�B	O�B	n�B	��B	�LB	�B	�B	�/B	�fB	�fB	�mB	��B	��B
  B	��B	�B	�B	�yB	�B	�B	��B	��B	��B	��B	��B	��B
  B
B
+B
DB
\B
bB
hB
oB
{B
�B
�B
�B
�B
�B
"�B
#�B
%�B
)�B
.B
0!B
5?B
=qB
D�B
H�B
N�B
S�B
XB
^5B
_;111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	��B	��B	��B	��B	��B	��B	��B	��B	�FB�7B��BBB�B#�B;dB;dB[#B^5BT�BbNBI�BD�B33B"�B{B��B�B�B�B�yB�ZB�NB�;B��BȴBs�BO�BoB
�'B
r�B
%�B	ɺB	�{B	t�B	^5B	\)B	\)B	ffB	N�B	Q�B	P�B	o�B	��B	�RB	�B	�B	�/B	�fB	�fB	�sB	��B	��B
  B	��B	�B	�B	�yB	�B	�B	��B	��B	��B	��B	��B	��B
  B
B
+B
DB
\B
bB
hB
oB
{B
�B
�B
�B
�B
�B
"�B
#�B
%�B
)�B
.B
0!B
5?B
=qB
D�B
H�B
N�B
S�B
XB
^5B
_;111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.4(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200910060752482009100607524820091006075248200910060804022009100608040220091006080402201002260000002010022600000020100226000000  JA  ARFMdecpA14                                                                 20090923035720  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090923035722  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090923035722  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090923035723  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090923035725  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090923035725  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090923035725  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090923035725  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090923035725  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090923040434                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090926215741  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090926215855  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090926215856  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090926215856  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090926215858  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090926215858  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090926215858  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090926215858  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090926215858  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20090926215858  CV  TIME            G�O�G�O�                    JA  ARGQrelo2.1                                                                 20090926215858  CV  LAT$            G�O�G�O�A@                  JA  ARGQrelo2.1                                                                 20090926215858  CV  LON$            G�O�G�O�C��                JA  ARUP                                                                        20090926220503                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090926163115  CV  DAT$            G�O�G�O�F�m�                JM  ARGQJMQC1.0                                                                 20090926163115  CV  LAT$            G�O�G�O�A?��                JM  ARGQJMQC1.0                                                                 20090926163115  CV  LON$            G�O�G�O�C�                JM  ARCAJMQC1.0                                                                 20091006075248  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20091006075248  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20091006080402  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100226000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100301012345  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100301012432                      G�O�G�O�G�O�                