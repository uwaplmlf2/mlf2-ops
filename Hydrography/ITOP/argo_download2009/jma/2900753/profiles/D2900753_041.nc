CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  2900753 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               )A   JA  20091007065657  20100219001305  A9_76277_041                    2C  D   APEX-SBE 3522                                                   846 @�Q/�S�1   @�Q4���@6�-@b0bM��1   ARGOS   A   A   A   @�ffA33A`  A�  A�33A陚B	33B��B333BD��B[33BnffB�ffB�33B�  B�  B���B�ffB�  B�33BЙ�B�ffB���B���B���C� CL�CL�C� CffC� CL�C$33C)L�C.L�C3ffC833C=ffCBffCGffCQ33C[� Ce� CoL�Cy� C���C���C�� C��fC��fC��fC��fC�� C��3C��3C��3C��fC��fC¦fC�� C̳3C�� C֦fCۀ C�� C噚C��CC��3C�� D��D�3D� D��D�fD�3D�3D$�3D)� D.��D3��D8� D=��DB�fDG�fDL�3DQ�fDV� D[��D`� DeٚDjٚDo�3DtٚDy� D�,�D�` D���D���D�&fD�` D��3D��D��D�c3D���D���D�&fD�Y�Dڣ3D��3D�#3D�c3D�D��fD�� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�33A��A^ffA�33A�ffA���B��BffB2��BDffBZ��Bn  B�33B�  B���B���B�ffB�33B���B�  B�ffB�33B䙚BB���CffC33C33CffCL�CffC33C$�C)33C.33C3L�C8�C=L�CBL�CGL�CQ�C[ffCeffCo33CyffC���C�� C��3C���C���C���C���C��3C��fC��fC��fC���C���C�Cǳ3C̦fCѳ3C֙�C�s3C�3C��C� C��C��fC�s3D�3D��D��D�fD� D��D��D$��D)ٚD.�fD3�fD8��D=�3DB� DG� DL��DQ� DV��D[�fD`��De�3Dj�3Do��Dt�3DyٚD�)�D�\�D���D�ٚD�#3D�\�D�� D��fD��D�` D��fD�ٚD�#3D�VfDڠ D�� D�  D�` D�fD��3D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A嗍A囦A噚A噚A坲A��A��A��A埾A�bNA�z�A��A���A���A�$�A�n�A�E�A�JA��A�1'A�VA��jA��PA� �A�l�A��A�{A��-A�JA�ffA��A��A�%A�1A���A���A�S�A�bA�C�A��9A�oA��^A�x�A�|�A���A���A���A���A���A��uA}�;A{��Ay33Aup�Ar1An�!Ag�FAaK�A[�AW�PARv�AN�uAG��ABVA<�+A9�A3�FA*9XA(��A%\)AA�A(�A�@�(�@�I�@�z�@��@�r�@�&�@���@���@��9@�x�@�  @���@���@���@�dZ@���@��;@� �@��@�1'@|j@v�y@jM�@\�@P�u@Lj@D�@8A�@-O�@$��@E�@�@�@x�@V@C�@ �@j@X?�j?�l�?�Z?�!11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A嗍A囦A噚A噚A坲A��A��A��A埾A�bNA�z�A��A���A���A�$�A�n�A�E�A�JA��A�1'A�VA��jA��PA� �A�l�A��A�{A��-A�JA�ffA��A��A�%A�1A���A���A�S�A�bA�C�A��9A�oA��^A�x�A�|�A���A���A���A���A���A��uA}�;A{��Ay33Aup�Ar1An�!Ag�FAaK�A[�AW�PARv�AN�uAG��ABVA<�+A9�A3�FA*9XA(��A%\)AA�A(�A�@�(�@�I�@�z�@��@�r�@�&�@���@���@��9@�x�@�  @���@���@���@�dZ@���@��;@� �@��@�1'@|j@v�y@jM�@\�@P�u@Lj@D�@8A�@-O�@$��@E�@�@�@x�@V@C�@ �@j@X?�j?�l�?�Z?�!11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
B
��B
��B
��B
��B
��B
��B
��B
��B
�B
�B
��B%B�B�B�B�B�BoBhB�B,B'�B%B
��B
�B
�HB
�#B
�B
�
B
��B
��B
��B
�
B
�B
�
B
�/B
�BB
�sB
��BB+BB
��B
��B
�B
�B
�`B
�B
�}B
��B
��B
�hB
�B
r�B
dZB
O�B
/B
oB	��B	�`B	��B	�dB	��B	~�B	hsB	YB	>wB	{B	
=B��B��B��Bz�Bk�BgmBiyBl�Bq�Bz�B�B�oB��B�jB�B�B	%B	{B	�B	+B	9XB	A�B	]/B	r�B	{�B	�7B	��B	ÖB	�/B	��B	��B	��B
bB
"�B
/B
:^B
D�B
K�B
Q�B
XB
_;B
ffB
l�B
r�B
w�B
z�B
}�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
��B
��B
��B
��B
��B
��B
��B
��B
��B
��BBbB�B!�B�B�B�B�B{B�B.B+B+B
��B
�B
�NB
�)B
�B
�B
��B
��B
��B
�B
�B
�B
�/B
�BB
�yB
��BB1BB
��B
��B
�B
�B
�fB
�)B
��B
�B
��B
�oB
�%B
s�B
e`B
Q�B
1'B
uB	��B	�fB	��B	�qB	��B	� B	iyB	[#B	@�B	�B	DB��B��B��B{�Bl�BhsBjBm�Br�B{�B�B�uB��B�jB�B�B	%B	{B	�B	+B	9XB	A�B	]/B	r�B	{�B	�7B	��B	ÖB	�/B	��B	��B	��B
bB
"�B
/B
:^B
D�B
K�B
Q�B
XB
_;B
ffB
l�B
r�B
w�B
z�B
}�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200910200150322009102001503220091020015032200910200235342009102002353420091020023534201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20091007065656  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091007065657  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091007065658  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091007065658  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091007065700  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091007065700  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091007065700  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091007065700  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091007065700  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091007070308                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091010215909  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091010220058  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091010220059  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091010220059  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091010220100  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091010220100  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091010220100  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091010220100  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091010220101  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091010220620                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20091010113006  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20091020015032  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20091020015032  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20091020023534  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219001202  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219001305                      G�O�G�O�G�O�                