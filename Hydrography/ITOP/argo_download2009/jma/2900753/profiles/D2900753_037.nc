CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  2900753 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               %A   JA  20090828035729  20100219001300  A9_76277_037                    2C  D   APEX-SBE 3522                                                   846 @�G/�&1   @�G/�,@7]/��w@b*��"��1   ARGOS   A   A   A   @�33A33Aa��A���A���A���B��BffB0ffBFffBZffBn  B�  B�ffB���B���B�33B�ffB�ffB�33B�33B�  B�33BB�33C� C33C  C33C33C33C  C#�fC)33C.� C3� C8L�C=��CB33CG  CQ33C[�Cd�fCoL�Cy�C���C�� C�� C���C�� C���C�� C�� C���C�ffC���C�ffC��3C���CǙ�C̙�CѦfCֳ3C۳3C�s3C噚C�3C�s3C� C��fD� D��D� DٚD��D��D��D$��D)��D.�fD3�3D8ٚD=�fDB�fDG�3DL�fDQ�fDV� D[�fD`�fDe� Dj��Do�3Dt�3Dy� D�#3D�` D�� D��D�  D�i�D��fD��3D�&fD�ffD���D��3D�  D�p DڦfD���D�&fD�\�D�D���D�@ 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�33A33Aa��A���A���A���B��BffB0ffBFffBZffBn  B�  B�ffB���B���B�33B�ffB�ffB�33B�33B�  B�33BB�33C� C33C  C33C33C33C  C#�fC)33C.� C3� C8L�C=��CB33CG  CQ33C[�Cd�fCoL�Cy�C���C�� C�� C���C�� C���C�� C�� C���C�ffC���C�ffC��3C���CǙ�C̙�CѦfCֳ3C۳3C�s3C噚C�3C�s3C� C��fD� D��D� DٚD��D��D��D$��D)��D.�fD3�3D8ٚD=�fDB�fDG�3DL�fDQ�fDV� D[�fD`�fDe� Dj��Do�3Dt�3Dy� D�#3D�` D�� D��D�  D�i�D��fD��3D�&fD�ffD���D��3D�  D�p DڦfD���D�&fD�\�D�D���D�@ 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�oA�{A�{A�JA�uA�ffA�+A�VA�v�Aԉ7A��A��AʍPA�XAüjA��yA�A��FA�M�A��A�ȴA�dZA��mA�1'A��TA�ƨA��A���A���A�K�A��A�ZA���A���A�A�A��;A�=qA�1'A��9A��A�M�A�`BA��A�{A��uA��A�bNA���A�A��A��A|v�A{oAw�-At�Apr�Al��Ae�-Ab1A\9XAVv�AR��AL{AFv�A?A=�mA6�A2(�A/�A)oAȴA�A
�uAhs@���@�\)@�o@��@˝�@�`B@��#@�5?@��;@�33@�=q@���@��@�I�@��9@�A�@���@}�@|I�@{@r�@b��@VE�@P�9@H�@A&�@9G�@6��@.5?@'l�@ ��@�R@��@p�@J@�R@�m@��@��?���?�=q?�X11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�oA�{A�{A�JA�uA�ffA�+A�VA�v�Aԉ7A��A��AʍPA�XAüjA��yA�A��FA�M�A��A�ȴA�dZA��mA�1'A��TA�ƨA��A���A���A�K�A��A�ZA���A���A�A�A��;A�=qA�1'A��9A��A�M�A�`BA��A�{A��uA��A�bNA���A�A��A��A|v�A{oAw�-At�Apr�Al��Ae�-Ab1A\9XAVv�AR��AL{AFv�A?A=�mA6�A2(�A/�A)oAȴA�A
�uAhs@���@�\)@�o@��@˝�@�`B@��#@�5?@��;@�33@�=q@���@��@�I�@��9@�A�@���@}�@|I�@{@r�@b��@VE�@P�9@H�@A&�@9G�@6��@.5?@'l�@ ��@�R@��@p�@J@�R@�m@��@��?���?�=q?�X11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
B
�}B
�}B
�}B
�}B
�wB
�jB
��B
�;B
�#B
��BBDB)�B�B%�B&�B;dBL�B2-B�Be`BW
B@�BB
��B
��B
�B
��B
�B
�B
�B
�B
�B
�yB
��B�B,B.B/B+B(�B$�B �B�BbB  B
�B
�B
ƨB
�LB
��B
�oB
�JB
~�B
n�B
YB
D�B
$�B
oB	��B	�/B	ɺB	�B	�bB	p�B	gmB	I�B	5?B	'�B	PB�;B�dB��B�bB�%B�B}�B}�B�B�7B��B��B��B�?BŢB�mB��B	VB	(�B	@�B	P�B	gmB	�B	�{B	��B	��B	��B	�BB	�B	��B
  B
�B
!�B
+B
49B
:^B
?}B
E�B
P�B
VB
[#B
]/B
hsB
m�B
s�B
t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�}B
�}B
�}B
�}B
�wB
��B
��B
�NB
�;B
��BBVB-B�B'�B(�B=qBO�B6FB�Be`BXBC�B%B  B
��B
��B
��B
�B
�B
�B
�B
�B
�B
��B�B.B/B0!B+B)�B%�B!�B�BhBB
�B
�#B
ǮB
�RB
��B
�oB
�PB
� B
o�B
ZB
F�B
%�B
{B	��B	�5B	��B	�B	�oB	p�B	iyB	J�B	6FB	)�B	VB�BB�qB��B�hB�+B�B~�B~�B�B�=B��B��B��B�?BŢB�mB��B	VB	)�B	@�B	P�B	gmB	�B	�{B	��B	��B	��B	�BB	�B	��B
  B
�B
!�B
+B
49B
:^B
?}B
E�B
P�B
VB
[#B
]/B
hsB
m�B
s�B
t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909100231012009091002310120090910023101200909100425092009091004250920090910042509201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090828035727  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090828035729  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090828035729  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090828035729  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090828035730  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090828035730  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090828035731  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090828035731  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090828035731  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090828040319                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090831185752  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090831185907  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090831185907  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090831185908  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090831185909  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090831185909  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090831185909  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090831185909  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090831185909  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090831190307                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090831103608  CV  DAT$            G�O�G�O�F�9�                JM  ARCAJMQC1.0                                                                 20090910023101  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090910023101  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090910042509  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219001150  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219001300                      G�O�G�O�G�O�                