CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  2900753 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               $A   JA  20090818095637  20100219001259  A9_76277_036                    2C  D   APEX-SBE 3522                                                   846 @�D��Q)�1   @�D��+�@7>��"��@b1&�y1   ARGOS   A   A   A   @���A��Aa��A�33A���A陚B��BffB0��BD��BZ  BnffB�33B�ffB�  B�33B�33B���B�  B�  B�33B�ffB�33B���B���C  C  CffCL�C� C� CL�C$L�C)33C.L�C3� C8  C=�CB  CG  CQ� CZ��CeffCo33Cy�C��fC��3C���C��3C�� C��3C�s3C���C�� C���C�� C��fC���C�CǙ�Č�Cь�C֙�Cۀ C�3C�3C�ffC�ffC��C��fD�3D�3D�fDٚD��D��D� D$��D)�3D.�3D3�3D8� D=ٚDB� DG� DLٚDQ�fDV�fD[�fD`�3DeٚDj� DoٚDt�fDy�3D�)�D�l�D�� D���D��D�l�D�� D�� D�)�D�ffD��3D��D�,�D�s3Dڜ�D��fD�)�D�ffD� D�� D� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A33A`  A�ffA���A���B33B  B0ffBDffBY��Bn  B�  B�33B���B�  B�  B�ffB���B���B�  B�33B�  B홚B�ffC �fC�fCL�C33CffCffC33C$33C)�C.33C3ffC7�fC=  CA�fCF�fCQffCZ�3CeL�Co�Cy  C���C��fC���C��fC��3C��fC�ffC�� C�s3C�� C�s3C���C�� C�Cǌ�C̀ Cр C֌�C�s3C�fC�fC�Y�C�Y�C� C���D��D��D� D�3D�fD�fD��D$�fD)��D.��D3��D8ٚD=�3DBٚDGٚDL�3DQ� DV� D[� D`��De�3DjٚDo�3Dt� Dy��D�&fD�i�D���D��D��D�i�D���D���D�&fD�c3D�� D��fD�)�D�p Dڙ�D��3D�&fD�c3D��D���D��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A앁A�uA�DA�\A�|�A�&�A�AӃA���A�7LA�jA�ffAå�A��hA���A�K�A��!A�E�A�VA���A�=qA�C�A�oA��TA���A�A�(�A�~�A�I�A��+A���A���A�E�A��A��A�A�A��/A��#A�{A��+A��
A�$�A�I�A��jA���A�=qA�A�p�A��A�+A���A~�HAzĜAw�Aq�Am�Ah�yAaA[�AV-AS�AOl�AG��ADI�A@��A8��A7�A2A-A(�DA+A��A
A�7@���@�A�@��@�S�@�ƨ@��H@�j@���@���@�"�@�-@���@�hs@��!@��@���@�O�@y��@q%@mO�@f�@Y��@YX@S�m@K��@Dj@=/@6��@-@'��@"�!@dZ@@��@{@
n�@r�@��@C�@ Ĝ?�(�?�(�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A앁A�uA�DA�\A�|�A�&�A�AӃA���A�7LA�jA�ffAå�A��hA���A�K�A��!A�E�A�VA���A�=qA�C�A�oA��TA���A�A�(�A�~�A�I�A��+A���A���A�E�A��A��A�A�A��/A��#A�{A��+A��
A�$�A�I�A��jA���A�=qA�A�p�A��A�+A���A~�HAzĜAw�Aq�Am�Ah�yAaA[�AV-AS�AOl�AG��ADI�A@��A8��A7�A2A-A(�DA+A��A
A�7@���@�A�@��@�S�@�ƨ@��H@�j@���@���@�"�@�-@���@�hs@��!@��@���@�O�@y��@q%@mO�@f�@Y��@YX@S�m@K��@Dj@=/@6��@-@'��@"�!@dZ@@��@{@
n�@r�@��@C�@ Ĝ?�(�?�(�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
B
��B
��B
��B
��B
��B
��B
��B
�B
��B
��B
=B1B+BVB\B\B{BuBoBPBVB	7B1B%BBBB
��B
��BVBDB\B�B�B�BoBhBuBuB{BuBuB\B+B
��B
��B
�sB
�)B
��B
ÖB
�?B
��B
�JB
y�B
_;B
L�B
5?B
VB	�B	�#B	��B	�LB	��B	�%B	u�B	T�B	J�B	8RB	�B	VB�BȴB��B��B�bB�\B�B�B�1B�DB�{B��B�qBÖBƨB�HB�B	�B	%�B	2-B	T�B	T�B	e`B	q�B	�B	��B	��B	�5B	��B
B
DB
�B
$�B
0!B
5?B
>wB
I�B
R�B
XB
]/B
bNB
gmB
n�B
r�B
v�B
v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
��B
��B
��B
��B
��B
��B
��B
�B
��B
��BJBDBPBhBoBoB�B�B{BbBhB	7B1B%BBBB
��BB\BPBbB�B�B�BoBoB{B{B�B{BuBbB+B  B
��B
�yB
�)B
��B
ĜB
�FB
��B
�PB
z�B
`BB
M�B
7LB
bB	�B	�)B	��B	�XB	��B	�+B	w�B	VB	K�B	9XB	�B	\B�B��B��B��B�hB�bB�B�B�1B�JB��B��B�wBÖBƨB�HB�B	�B	%�B	2-B	T�B	T�B	e`B	q�B	�B	��B	��B	�5B	��B
B
DB
�B
$�B
0!B
5?B
>wB
I�B
R�B
XB
]/B
bNB
gmB
n�B
r�B
v�B
v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200908310136102009083101361020090831013610200908310354252009083103542520090831035425201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090818095636  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090818095637  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090818095638  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090818095638  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090818095639  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090818095639  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090818095639  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090818095639  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090818095639  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090818100234                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090821185940  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090821190123  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090821190124  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090821190124  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090821190125  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090821190125  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090821190125  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090821190125  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090821190125  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090821190641                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090821104358  CV  DAT$            G�O�G�O�F�%{                JM  ARCAJMQC1.0                                                                 20090831013610  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090831013610  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090831035425  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219001148  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219001259                      G�O�G�O�G�O�                