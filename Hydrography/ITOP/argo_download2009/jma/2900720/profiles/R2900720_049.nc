CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   I   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D�   CALIBRATION_DATE            	             
_FillValue                  ,  G�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    H   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    H   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    H   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    H   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  H   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    HT   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Hd   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Hh   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Hx   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         H|   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        H�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    H�Argo profile    2.2 1.2 19500101000000  2900720 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               1A   JA  20091009095545  20091013010150                                  2B  A   APEX-SBE 3422                                                   846 @�Q��Ř1   @�Q�˩�e@7����@`����m1   ARGOS   A   A   A   @�ffA��A�ffA�33B  BF  Bm��B�33B�33B�ffB�  B���B�  C ��C�CffC�3C)33C3�C=ffCGffC[L�Co��C��fC��fC�s3C�s3C���C��fC���Cǳ3C�� Cۙ�C� C�3C��fD�fD� D� D��D� D��D� D$�fD)�3D.ٚD3�3D8ٚD=� DB�fDGٚDN�DT33DZ�fD`�fDf��Dm@ Ds� Dy� D��D�ffD���D�� D�fD�` D�ٚD�ffD�� D�ffD���D�c3D��D�#31111111111111111111111111111111111111111111111111111111111111111111111111   @���A��A�  A���B��BD��BlfgB���B���B���B�ffB�33B�ffC � C
��C�CffC(�fC2��C=�CG�C[  CoL�C�� C�� C�L�C�L�C�s4C�� C�fgCǌ�Cљ�C�s4C�Y�C��C�� D�3D��D��D�gD��D��D��D$�3D)� D.�gD3� D8�gD=��DB�3DG�gDNgDT  DZs3D`�3Df�gDm,�Dsl�Dy��D�3D�\�D��3D��fD��D�VfD�� D�\�D��fD�\�D��3D�Y�D�� D��1111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�Aԗ�AԁA�`BA�{A��mA��
AЍPA�bAǅA�v�A��A�v�A�;dA�&�A���A�Q�A���A��wA��A���A���A�;dA���A���A���A�7LA%Aw&�Ai`BA`�\AWl�ALv�AF��A9�A0$�A'\)A"v�AC�A
Q�A��@��@݉7@٩�@�`B@̛�@��@��@��@��@�G�@��@�&�@���@��D@��/@��/@�x�@}/@s@[�F@S@Q7L@J�!@A&�@4��@'K�@Ĝ@�`@	��@M�?�1?�;d?�
=1111111111111111111111111111111111111111111111111111111111111111111111111   Aԗ�AԁA�`BA�{A��mA��
AЍPA�bAǅA�v�A��A�v�A�;dA�&�A���A�Q�A���A��wA��A���A���A�;dA���A���A���A�7LA%Aw&�Ai`BA`�\AWl�ALv�AF��A9�A0$�A'\)A"v�AC�A
Q�A��@��@݉7@٩�@�`B@̛�@��@��@��@��@�G�@��@�&�@���@��D@��/@��/@�x�@}/@s@[�F@S@Q7L@J�!@A&�@4��@'K�@Ĝ@�`@	��@M�?�1?�;d?�
=1111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
I�B
J�B
G�B
A�B
D�B
J�B
J�B
L�B
s�B
�B{B1'B5?B=qB<jBE�B,BbBoBJBhB�BuB
��B
�TB
�wB
��B
}�B
?}B
�B	�B	�qB	��B	ffB	D�B	$�B	bB�fB�}B�B��B�hB�hB�hB�bB��B��B�BB�
B�`B	  B	�B	/B	A�B	N�B	cTB	o�B	�B	�-B	ĜB	ȴB	�B	�B
B
!�B
A�B
O�B
`BB
o�B
x�B
�B
�V1111111111111111111111111111111111111111111111111111111111111111111111111   B
I�B
J�B
G�B
A�B
D�B
J�B
J�B
L�B
s�B
�B{B1'B5?B=qB<jBE�B,BbBoBJBhB�BuB
��B
�TB
�wB
��B
}�B
?}B
�B	�B	�qB	��B	ffB	D�B	$�B	bB�fB�}B�B��B�hB�hB�hB�bB��B��B�BB�
B�`B	  B	�B	/B	A�B	N�B	cTB	o�B	�B	�-B	ĜB	ȴB	�B	�B
B
!�B
A�B
O�B
`BB
o�B
x�B
�B
�V1111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091009095544  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091009095545  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091009095546  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091009095546  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091009095547  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091009095547  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091009095547  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091009095547  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091009095548  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091009100230                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091013005355  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091013005739  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091013005740  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091013005740  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091013005741  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091013005741  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091013005741  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091013005741  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091013005742  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091013010150                      G�O�G�O�G�O�                