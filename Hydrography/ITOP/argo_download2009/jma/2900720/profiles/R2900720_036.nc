CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   E   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5T   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6h   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9    PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       :4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;H   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       <�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >    SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >0   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A0   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D0   CALIBRATION_DATE            	             
_FillValue                  ,  G0   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G\   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G`   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Gd   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Gh   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Gl   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         G�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         G�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G�Argo profile    2.2 1.2 19500101000000  2900720 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               $A   JA  20090806185505  20091207021944                                  2B  A   APEX-SBE 3422                                                   846 @�A�0[�1   @�A����@8���Q�@`��Q�1   ARGOS   B   B   B   @�ffA��A���A���B��BF  Bn  B���B�ffB�33B�ffB�ffB�ffC �fC33CffC  C)33C2�fC=33CGffC[  Co� C��fC���C���C���C�ffC��3C�� C�� Cь�Cی�C�s3CC��3D�3DٚDٚD� D�fD�3D�3D$� D)��D.�fD3� D8�3D=�3DB�fDG� DN3DT@ DZ�3D`�3Df�3DmS3Ds�fDy��G�O�D�l�D���D�Y�D�� D�i�D��3D�l�D�� D���111111111111111111111111111111111111111111111111111111111114111111111   @�33A33A�  A�  BfgBE��Bm��B�fgB�33B�  B�33B�33B�33C ��C�CL�C�fC)�C2��C=�CGL�CZ�fCoffC���C�� C���C�� C�Y�C��fC��3Cǳ3Cр Cۀ C�ffC��C��fD��D�4D�4D��D� D��D��D$ٚD)�4D.� D3ٚD8��D=��DB� DGٚDN�DT9�DZ��D`��Df��DmL�Ds� Dy�gG�O�D�i�D��D�VgD���D�fgD�� D�i�D���D�ɚ111111111111111111111111111111111111111111111111111111111114111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�(�A�33A�E�A�-AɅA��`A�M�A�ȴA�n�A��A���A��uA���A��PA�G�A��A�ȴA�~�A���A�G�A�ȴA�Q�A�  A��A���A��A���A�/A�n�AxE�ApbAh�uAa�TAY�AKAB�HA:I�A,��A�A%AG�AK�@���@�t�@ݙ�@Ӯ@�~�@��H@���@�M�@�$�@�$�@�;d@�+@�;d@�V@�7L@��G�O�@G�w@B=q@3S�@'K�@�@I�@V@+@ ��@   111111111111111111111111111111111111111111111111111111111194111111111   A�(�A�33A�E�A�-AɅA��`A�M�A�ȴA�n�A��A���A��uA���A��PA�G�A��A�ȴA�~�A���A�G�A�ȴA�Q�A�  A��A���A��A���A�/A�n�AxE�ApbAh�uAa�TAY�AKAB�HA:I�A,��A�A%AG�AK�@���@�t�@ݙ�@Ӯ@�~�@��H@���@�M�@�$�@�$�@�;d@�+@�;d@�V@�7L@��G�O�@G�w@B=q@3S�@'K�@�@I�@V@+@ ��@   111111111111111111111111111111111111111111111111111111111194111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�7B
�=B
�=B
�%B
��B
��B
��B
��B
��B
�B
�#B
��B
��B
��B
�
B
�
B
�
B
�B
�)B
�BB%B
=BJB\B�B	7B
�fB
�?B
�B
^5B
;dB
�B	�B	�XB	��B	n�B	49B��B�B�}B��B��B�VB�+B�1B�JB�{B��B��B�jB�B�NB��B	\B	(�B	33B	M�G�O�B	�
B	�NB
B
�B
6FB
J�B
W
B
dZB
p�B
r�111111111111111111111111111111111111111111111111111111111194111111111   B
�7B
�=B
�=B
�%B
��B
��B
��B
��B
��B
�B
�#B
��B
��B
��B
�
B
�
B
�
B
�B
�)B
�BB%B
=BJB\B�B	7B
�fB
�?B
�B
^5B
;dB
�B	�B	�XB	��B	n�B	49B��B�B�}B��B��B�VB�+B�1B�JB�{B��B��B�jB�B�NB��B	\B	(�B	33B	M�G�O�B	�
B	�NB
B
�B
6FB
J�B
W
B
dZB
p�B
r�111111111111111111111111111111111111111111111111111111111194111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090806185504  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090806185505  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090806185506  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090806185506  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090806185507  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090806185507  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090806185507  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090806185507  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090806185507  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090806185507  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090806185507  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090806190221                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090809035513  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090809035750  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090809035751  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090809035751  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090809035752  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090809035752  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090809035752  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090809035752  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090809035752  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090809035752  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090809035752  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090809040236                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207004944  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207005005  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207005006  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207005006  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207005007  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207005007  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207005007  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207005007  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207005007  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207005007  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207005007  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091207021944                      G�O�G�O�G�O�                