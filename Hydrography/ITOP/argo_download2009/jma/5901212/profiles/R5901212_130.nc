CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   i   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J   CALIBRATION_DATE            	             
_FillValue                  ,  M   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    MD   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    MH   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    ML   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    MP   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  MT   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               �A   JA  20090909095643  20090913040404                                  2B  A   2404                                                            846 @�JHeC �1   @�JH�4��@+���v�@`z^5?|�1   ARGOS   A   A   A   @�33AffAh  A�ffA���A���B	33B  B2  BFffBZffBnffB�  B���B�33B���B���B�  B���Bƙ�BЙ�B���B䙚B�  B�  CffC33C��C�3CL�CL�C33C$L�C)�C.L�C3� C8� C=� CBffCG33CQ��C[��CeffCoffCy�C��fC�� C��3C��3C��3C��3C�� C��3C�� C�� C�� C���C��3C�� CǙ�C�� C�� C֌�Cی�C���C�s3C�fC���C�� C��3D� D�3D��D� D�3D� DٚD$��D)ٚD.� D3��D8��D=�3DB�3DG��DL� DQٚDV��D[��D`ٚDe��Dj�3Do� Dt� Dy�fD�&fD�ffD���D��D�&fD�l�D��fD��3D�#3D��3111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @���A33Ad��A���A�33A�33BffB33B133BE��BY��Bm��B���B�fgB���B�fgB�34B���B�fgB�34B�34B�fgB�34BB���C33C  C��C� C�C�C  C$�C(�gC.�C3L�C8L�C=L�CB33CG  CQfgC[fgCe33Co33Cx�gC���C��fC���C���C���C���C�ffC���C��fC��fC��fC��3C���C¦fCǀ C̦fCѦfC�s3C�s3C�� C�Y�C��C�3C��fC���D�3D�fD� D�3D�fD�3D��D$� D)��D.�3D3��D8� D=�fDB�fDG� DL�3DQ��DV� D[� D`��De� Dj�fDo�3Dt�3Dy��D�  D�` D��4D��4D�  D�fgD�� D���D��D�|�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�hsA�l�A�r�A�x�A�x�A镁A�VA���A�ƨA�wA�RA�FA�
=A�ĜA�  A�+A��A��A�bA�(�A�~�A��;AնFAӝ�AҶFAС�Aͩ�A���AŰ!A���A�XA��/A�ĜA�M�A�O�A���A���A��PA�+A�A�1A���A��HA���A�At�DAk�Ab��AYoATjAO/AKG�AG�
A>�DA8^5A4�RA/�A+p�A%��A��AA&�A�A��A��A"�A�j@���@�M�@�(�@���@أ�@�V@�|�@�&�@��y@�=q@���@�9X@��@��@���@��h@��\@�I�@�ƨ@�A�@��D@��#@���@��+@���@�G�@��@�Z@t��@l�j@b-@W�@N�R@G�@A��@;"�@4Z@0��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�hsA�l�A�r�A�x�A�x�A镁A�VA���A�ƨA�wA�RA�FA�
=A�ĜA�  A�+A��A��A�bA�(�A�~�A��;AնFAӝ�AҶFAС�Aͩ�A���AŰ!A���A�XA��/A�ĜA�M�A�O�A���A���A��PA�+A�A�1A���A��HA���A�At�DAk�Ab��AYoATjAO/AKG�AG�
A>�DA8^5A4�RA/�A+p�A%��A��AA&�A�A��A��A"�A�j@���@�M�@�(�@���@أ�@�V@�|�@�&�@��y@�=q@���@�9X@��@��@���@��h@��\@�I�@�ƨ@�A�@��D@��#@���@��+@���@�G�@��@�Z@t��@l�j@b-@W�@N�R@G�@A��@;"�@4Z@0��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	B	B	PB	�B	A�B	r�B	��B	�B	�
B	�B	�B	�B
�B� B�?B�}B��B�mB�HB�B��B�B�BB�LB�-B�fB�B�-B�B�RBƨB��B��B1B%BB��B�BȴB�?Bq�B'�B
�HB
�B
w�B
G�B
�B	�B	��B	ȴB	�LB	��B	�VB	l�B	T�B	B�B	1'B	(�B	�B	DB	JB	B��B�B��B	{B	#�B	'�B	B��B	JB	)�B	I�B	dZB	�B	��B	��B	�LB	��B	�/B	�TB	�B	��B	��B
B
DB
uB
�B
�B
"�B
(�B
-B
/B
33B
5?B
;dB
>wB
C�B
H�B
M�B
Q�B
T�B
YB
]/B
`B111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	B	B	PB	�B	A�B	r�B	��B	�B	�
B	�B	�B	�B
�B� B�?B�}B��B�mB�HB�B��B�B�BB�LB�-B�fB�B�-B�B�RBƨB��B��B1B%BB��B�BȴB�?Bq�B'�B
�HB
�B
w�B
G�B
�B	�B	��B	ȴB	�LB	��B	�VB	l�B	T�B	B�B	1'B	(�B	�B	DB	JB	B��B�B��B	{B	#�B	'�B	B��B	JB	)�B	I�B	dZB	�B	��B	��B	�LB	��B	�/B	�TB	�B	��B	��B
B
DB
uB
�B
�B
"�B
(�B
-B
/B
33B
5?B
;dB
>wB
C�B
H�B
M�B
Q�B
T�B
YB
]/B
`B111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090909095642  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090909095643  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090909095644  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090909095644  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090909095644  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090909095645  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090909095645  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090909095645  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090909095645  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090909095645  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090909100452                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090913035546  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090913035901  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090913035901  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090913035901  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090913035902  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090913035903  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090913035903  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090913035903  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090913035903  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090913035903  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090913040404                      G�O�G�O�G�O�                