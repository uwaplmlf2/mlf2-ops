CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   i   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  C�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J   CALIBRATION_DATE            	             
_FillValue                  ,  M   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    MD   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    MH   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    ML   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    MP   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  MT   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               �A   JA  20090929155650  20091003070309                                  2B  A   2404                                                            846 @�OLVٱ�1   @�OL�m�@+�����@`a�$�/1   ARGOS   A   A   A   @�ffA��A`  A�  A���A�33B	33B��B1��BE33BZ  Bm33B���B�  B���B�ffB�ffB���B�33B���B���B�ffB�  B�33B�  C� C��C� C��C33C  C33C$  C)��C.ffC3� C8� C=ffCB33CG��CQ� C[33Ce  Cn�fCyffC��3C��3C�� C��fC���C��fC��fC��3C��fC���C��3C���C���C�CǦfC̳3CѦfC֦fC۳3C�Y�C� C�ffC�� C�� C�� D�3D��D��D�fDٚD�fD�fD$�3D)� D.� D3ٚD8� D=� DB� DG��DL� DQٚDV�fD[� D`�fDe�3Dj��Do� Dt��Dy�fD�,�D�i�D���D��fD�&fD�c3D��3D���D��D�)�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�  A��A\��A�ffA�  A陙BffB  B0��BDffBY33BlffB�34B���B�fgB�  B�  B�34B���B�fgB�fgB�  B䙚B���B���CL�C��CL�CfgC  C��C  C#��C)fgC.33C3L�C8L�C=33CB  CG��CQL�C[  Cd��Cn�3Cy33C���C���C��fC���C�� C���C���C���C���C�s3C���C�� C�� C�s3Cǌ�C̙�Cь�C֌�Cۙ�C�@ C�ffC�L�C�fC��fC��fD�fD��D��D��D��D��D��D$�fD)�3D.�3D3��D8�3D=�3DB�3DG� DL�3DQ��DV��D[�3D`��De�fDj� Do�3Dt� Dy��D�&gD�c4D��4D�� D�  D�\�D���D��gD�4D�#4111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�z�A�^A��A��A蟾A蛦A蕁A�!A蟾A��mA䗍A�O�A��A��A��Aޥ�A�;dA�bA�jA� �A�jAв-A�A��#A�K�A�VA���A�S�A���A�O�A�bNA��`A�;dA�ƨA��jA�A�C�A���A��yA�XA�t�A�?}A�9XA��A���AO�Au�PAodZA^�HAZ~�AR��AH�uA<r�A5A0�RA)K�A$z�A"��A�A�A�hA
�`A+A1'@��@�I�@�O�@�
=@ݲ-@�&�@�A�@�G�@å�@��@��@�Z@��/@��@�I�@�`B@�b@�/@��!@��P@�%@�;d@�C�@�ƨ@�-@��@�l�@�V@�ff@��@{�F@t9X@hb@`b@W�@O;d@E��@?K�@9%@2�@,Z111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�z�A�^A��A��A蟾A蛦A蕁A�!A蟾A��mA䗍A�O�A��A��A��Aޥ�A�;dA�bA�jA� �A�jAв-A�A��#A�K�A�VA���A�S�A���A�O�A�bNA��`A�;dA�ƨA��jA�A�C�A���A��yA�XA�t�A�?}A�9XA��A���AO�Au�PAodZA^�HAZ~�AR��AH�uA<r�A5A0�RA)K�A$z�A"��A�A�A�hA
�`A+A1'@��@�I�@�O�@�
=@ݲ-@�&�@�A�@�G�@å�@��@��@�Z@��/@��@�I�@�`B@�b@�/@��!@��P@�%@�;d@�C�@�ƨ@�-@��@�l�@�V@�ff@��@{�F@t9X@hb@`b@W�@O;d@E��@?K�@9%@2�@,Z111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	bB	
=B	
=B	PB	VB	bB	bB	!�B
�=B%�B�=B�jB��B�;B�;B�;B�B�;B��B�qB�dB�B�B��B��B�
B��B%BBB��B��B��B��B�B�B�#B��BÖB��BD�B
=B
��B
��B
�B
ffB
A�B
(�B	�B	�5B	��B	�\B	gmB	L�B	C�B	,B	#�B	�B	oB	B	B��B	+B	oB	 �B	$�B	bB	hB	�B	)�B	;dB	`BB	w�B	��B	�B	ÖB	��B	�5B	�sB	�B	��B
B
1B
VB
oB
�B
�B
 �B
#�B
'�B
-B
/B
2-B
33B
8RB
:^B
A�B
D�B
H�B
L�B
Q�B
W
B
[#B
aHB
e`111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	bB	
=B	
=B	PB	VB	bB	bB	!�B
�=B%�B�=B�jB��B�;B�;B�;B�B�;B��B�qB�dB�B�B��B��B�
B��B%BBB��B��B��B��B�B�B�#B��BÖB��BD�B
=B
��B
��B
�B
ffB
A�B
(�B	�B	�5B	��B	�\B	gmB	L�B	C�B	,B	#�B	�B	oB	B	B��B	+B	oB	 �B	$�B	bB	hB	�B	)�B	;dB	`BB	w�B	��B	�B	ÖB	��B	�5B	�sB	�B	��B
B
1B
VB
oB
�B
�B
 �B
#�B
'�B
-B
/B
2-B
33B
8RB
:^B
A�B
D�B
H�B
L�B
Q�B
W
B
[#B
aHB
e`111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090929155649  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090929155650  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090929155650  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090929155651  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090929155651  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090929155652  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090929155652  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090929155652  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090929155652  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090929155652  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090929160344                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091003065507  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091003065752  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091003065752  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091003065752  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091003065753  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091003065754  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091003065754  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091003065754  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091003065754  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091003065754  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091003070309                      G�O�G�O�G�O�                