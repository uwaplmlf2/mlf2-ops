CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   j   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D<   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G<   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J<   CALIBRATION_DATE            	             
_FillValue                  ,  M<   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Mh   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Ml   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Mp   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Mt   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Mx   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               �A   JA  20091009095609  20091013010347                                  2B  A   2404                                                            846 @�Q�����1   @�Q�����@+�ě��T@`Z��O�;1   ARGOS   A   A   A   @���A33Ad��A���A�33A�  B	33B  B1��BE33BZ��Bm��B�33B���B���B���B�  B�  B���B�  B�  B�ffB�ffB�33B�ffC� C�C�3C��C��C��CL�C$33C)L�C.33C3ffC8� C=�3CBL�CG�CQ��C[ffCe�CoffCy33C��3C�ٚC��3C���C��3C���C�� C�� C���C��3C���C��fC���C���Cǳ3C̦fCѦfCֳ3CۦfC�fC�3C�3C�s3C���C�� D�fD�fD��D�fD� D� D�3D$�fD)� D.� D3�3D8�3D=�3DB�3DGٚDL��DQ�fDV�fD[� D`ٚDe��Dj�fDoٚDtٚDy�fD�#3D�\�D�� D�� D�,�D�c3D���D���D�#3D�s3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���A��Ac33A�  A�ffA�33B��B��B134BD��BZfgBm34B�  B�fgB�fgB�fgB���B���B�fgB���B���B�33B�33B�  B�33CffC  C��C� C� C� C33C$�C)33C.�C3L�C8ffC=��CB33CG  CQ� C[L�Ce  CoL�Cy�C��fC���C��fC���C��fC���C��3C��3C�� C��fC���C���C���C�� CǦfC̙�Cљ�C֦fCۙ�C���C�fC�fC�ffC�� C��3D� D� D�gD� DٚDٚD��D$� D)��D.��D3��D8��D=��DB��DG�4DL�gDQ� DV� D[��D`�4De�gDj� Do�4Dt�4Dy� D�  D�Y�D���D���D�)�D�` D���D��D�  D�p D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�\A�uA�\A�hA�uA�uA蕁A�hsA���A�"�A�%A���A晚A�VA�ZA���A�oA�  A�33A�O�A�XA��AؑhAև+A�x�Aҗ�AЅA�ffAǍPAƙ�A�ZA�~�A�%A�~�A���A�C�A�  A��
A���A��HA�oA�=qA�-A�S�A�A�A~�/Av��AkAdA[dZANr�AA�hA<��A4ȴA&�A"�A�yA��A��A	�^A��AK�@��@�  @�/@�S�@�F@�~�@�E�@���@�/@��@�-@�hs@��@��@���@��@�l�@���@�;d@�
=@���@��h@��!@��j@�~�@�(�@�%@�p�@�\)@�Ĝ@�M�@}��@yhs@sƨ@l��@aX@U?}@J�!@B��@;��@5O�@/K�@)hs@)&�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�\A�uA�\A�hA�uA�uA蕁A�hsA���A�"�A�%A���A晚A�VA�ZA���A�oA�  A�33A�O�A�XA��AؑhAև+A�x�Aҗ�AЅA�ffAǍPAƙ�A�ZA�~�A�%A�~�A���A�C�A�  A��
A���A��HA�oA�=qA�-A�S�A�A�A~�/Av��AkAdA[dZANr�AA�hA<��A4ȴA&�A"�A�yA��A��A	�^A��AK�@��@�  @�/@�S�@�F@�~�@�E�@���@�/@��@�-@�hs@��@��@���@��@�l�@���@�;d@�
=@���@��h@��!@��j@�~�@�(�@�%@�p�@�\)@�Ĝ@�M�@}��@yhs@sƨ@l��@aX@U?}@J�!@B��@;��@5O�@/K�@)hs@)&�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	cTB	cTB	dZB	dZB	dZB	dZB	dZB	x�B	��B
�B
G�B
hsB
}�B
��B
��B
�TBB0!B�DB��B�B�)B
=BB{B2-B2-B�sB
=BoBuB�BVBB��B��B��B�B�BB��B�hBJ�B{B
�mB
��B
x�B
S�B
"�B
%B	�BB	�-B	x�B	gmB	G�B	�B	  B�B	1B	B	DB	JB	�B	�B	VB	JB	uB	B	JB	�B	.B	I�B	r�B	�%B	��B	�B	�qB	��B	�ZB	�B	��B	��B
B
	7B
hB
�B
�B
�B
�B
%�B
+B
-B
0!B
2-B
8RB
8RB
;dB
?}B
D�B
H�B
N�B
T�B
XB
]/B
e`B
iyB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B	cTB	cTB	dZB	dZB	dZB	dZB	dZB	x�B	��B
�B
G�B
hsB
}�B
��B
��B
�TBB0!B�DB��B�B�)B
=BB{B2-B2-B�sB
=BoBuB�BVBB��B��B��B�B�BB��B�hBJ�B{B
�mB
��B
x�B
S�B
"�B
%B	�BB	�-B	x�B	gmB	G�B	�B	  B�B	1B	B	DB	JB	�B	�B	VB	JB	uB	B	JB	�B	.B	I�B	r�B	�%B	��B	�B	�qB	��B	�ZB	�B	��B	��B
B
	7B
hB
�B
�B
�B
�B
%�B
+B
-B
0!B
2-B
8RB
8RB
;dB
?}B
D�B
H�B
N�B
T�B
XB
]/B
e`B
iyB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091009095608  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091009095609  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091009095610  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091009095610  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091009095610  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091009095611  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091009095611  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091009095611  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091009095611  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091009095611  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091009100329                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091013005404  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091013005749  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091013005749  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091013005750  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091013005750  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091013005751  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091013005751  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091013005751  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091013005751  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091013005751  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091013010347                      G�O�G�O�G�O�                