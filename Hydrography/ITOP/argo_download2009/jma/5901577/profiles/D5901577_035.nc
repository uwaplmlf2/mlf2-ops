CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901577 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               #A   JA  20090823125617  20100219015623  A9_76288_035                    2C  D   APEX-SBE 3533                                                   846 @�F���1   @�FHpB�@3��E���@b�$�/1   ARGOS   A   A   A   @���A��Aa��A�  A�33A�33B	��B33B3��BE33BY33Bn��B���B�ffB�  B���B�33B�33B�  B�  B�33B���B�ffB�  B���C� CL�CL�C33C� CffCffC$� C)�C.33C333C833C=L�CA�fCG33CP�fC[33Ce33Co33Cy�C�ffC��fC���C�� C��3C���C���C���C��fC��3C���C���C��fC�CǙ�C̦fCѦfC�s3Cۀ C�s3C��C�3C�3C��fC�� D��D��D�fDٚD�3DٚDٚD$ٚD)�fD.�fD3�3D8ٚD=� DB�fDGٚDL��DQ� DV�3D[�3D`��De��Dj� Do�3Dt�3Dy� D�#3D�l�D���D�� D��D�l�D��3D��3D�#3D�Y�D���D��D�,�D�ffDڦfD�� D�)�D�i�D�D��3D�@ 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA  A`  A�33A�ffA�ffB	33B��B333BD��BX��BnffB�ffB�33B���B�ffB�  B�  B���B���B�  Bڙ�B�33B���B���CffC33C33C�CffCL�CL�C$ffC)  C.�C3�C8�C=33CA��CG�CP��C[�Ce�Co�Cy  C�Y�C���C���C��3C��fC���C�� C���C���C��fC���C���C���C�Cǌ�C̙�Cљ�C�ffC�s3C�ffC� C�fC�fC���C��3D�3D�3D� D�3D��D�3D�3D$�3D)� D.� D3��D8�3D=ٚDB� DG�3DL�fDQ��DV��D[��D`�3De�fDjٚDo��Dt��Dy��D�  D�i�D���D���D��D�i�D�� D�� D�  D�VfD���D��fD�)�D�c3Dڣ3D���D�&fD�ffD�fD�� D�<�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A��/A�RA�A��A�A�A�A�|�A���A޸RA�hsA��mA�K�A���A�~�AŸRA�G�A���A��-A��A���A��^A��;A�M�A���A��FA�p�A��`A�  A��A���A��RA��uA�bNA��A��A�hsA�t�A���A�;dA���A��\A�\)A���A���A�p�A�I�A��A�Q�Az�At��Al�Ah��Ac?}A[�-ATJAJ��AC33A@�A:�A6bA0^5A.�A*{A#�A�^AK�A1'AQ�A��@�@��@�|�@�M�@�O�@��T@��u@���@��@�v�@���@�A�@�Z@���@��F@�~�@��@�C�@��`@�n�@��y@��m@���@�~�@�`B@v$�@j�\@^E�@P1'@I�#@E�T@B��@:M�@3��@.�@*^5@$9X@�@^5@�T@�#@	x�@�u@�@ bN?�C�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A��/A�RA�A��A�A�A�A�|�A���A޸RA�hsA��mA�K�A���A�~�AŸRA�G�A���A��-A��A���A��^A��;A�M�A���A��FA�p�A��`A�  A��A���A��RA��uA�bNA��A��A�hsA�t�A���A�;dA���A��\A�\)A���A���A�p�A�I�A��A�Q�Az�At��Al�Ah��Ac?}A[�-ATJAJ��AC33A@�A:�A6bA0^5A.�A*{A#�A�^AK�A1'AQ�A��@�@��@�|�@�M�@�O�@��T@��u@���@��@�v�@���@�A�@�Z@���@��F@�~�@��@�C�@��`@�n�@��y@��m@���@�~�@�`B@v$�@j�\@^E�@P1'@I�#@E�T@B��@:M�@3��@.�@*^5@$9X@�@^5@�T@�#@	x�@�u@�@ bN?�C�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
B{�B|�B}�B~�B�^BĜB�mB�5B�?B��B�3BoB	7B�B�BB��B�#B�BB�B��B�{B�BjBB�B>wBZBbNBC�B+B"�B �B!�B!�B�B�B�B�B�B�B�B	7B
��B
�B
�BB
�#B
ŢB
�B
�7B
cTB
=qB
0!B
JB	�ZB	ĜB	�uB	r�B	aHB	F�B	33B	�B	�B	B�B��B�9B�B�oB�=Bx�Bo�Bm�Bp�By�B�B�hB��B�FBɺB�BB��B	�B	0!B	@�B	N�B	iyB	�+B	�{B	��B	�'B	�^B	��B	�B	�5B	��B
bB
�B
)�B
1'B
6FB
8RB
?}B
D�B
H�B
L�B
S�B
ZB
_;B
cTB
gmB
s�B
t�B
z�B
� B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B{�B|�B}�B~�B�^BĜB�yB�HB�jB��B�XB�BJB�B�B%B  B�/B�B%B�!B��B��B�Bp�BC�B?}B\)BdZBE�B-B"�B!�B"�B#�B�B�B�B�B�B�B�B
=B
��B
�B
�BB
�/B
ƨB
�B
�DB
e`B
>wB
1'B
VB	�fB	ƨB	��B	s�B	cTB	G�B	5?B	�B	�B	%B�B��B�9B�B�{B�DBy�Bp�Bn�Bq�Bz�B�B�oB��B�FB��B�BB��B	�B	0!B	@�B	N�B	iyB	�+B	�{B	��B	�'B	�^B	��B	�B	�5B	��B
bB
�B
)�B
1'B
6FB
8RB
?}B
D�B
H�B
L�B
S�B
ZB
_;B
cTB
gmB
s�B
t�B
z�B
� B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909051338572009090513385720090905133857200909051611292009090516112920090905161129201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090823125616  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090823125617  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090823125617  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090823125618  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090823125619  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090823125619  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090823125619  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090823125619  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090823125619  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090823130102                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090827035650  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090827035822  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090827035823  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090827035823  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090827035824  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090827035824  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090827035824  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090827035824  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090827035824  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090827040255                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090826164355  CV  DAT$            G�O�G�O�F�08                JM  ARCAJMQC1.0                                                                 20090905133857  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090905133857  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090905161129  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015552  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015623                      G�O�G�O�G�O�                