CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901577 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               &A   JA  20090922125618  20100219015619  A9_76288_038                    2C  D   APEX-SBE 3533                                                   846 @�M�8�w1   @�M�'qj@3��Q�@b�~��"�1   ARGOS   A   A   A   @�  A  AfffA�ffA���A�ffB��B  B2ffBF��BX��BlffB�33B�ffB�ffB�33B���B���B���B�33B�  Bڙ�B���BB���C� CL�C� C33CffCL�CL�C$  C)L�C.�C3L�C8L�C=L�CB  CF��CQ  CZ��Ce� CoL�Cy��C��fC�� C��fC�� C�� C���C���C���C�� C��3C�� C��fC��fC³3Cǌ�C̦fCь�C֦fC�s3C�3C�� C�fC���C���C���DٚD�3D��D�3DٚDٚD� D$��D)�fD.�3D3� D8� D=�3DB��DGٚDL�fDQ�3DV�fD[��D`��De��Dj�fDo� Dt��Dy� D�  D�i�D���D���D��D�i�D�� D�� D�  D�i�D���D�� D�&fD�p Dڠ D��fD�#3D�l�D�3D��D�\�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���AffAd��A���A���A陚BffB��B2  BFffBXffBl  B�  B�33B�33B�  B���B���B�ffB�  B���B�ffB䙚B�ffB�ffCffC33CffC�CL�C33C33C#�fC)33C.  C333C833C=33CA�fCF�3CP�fCZ�3CeffCo33Cy� C���C�s3C���C�s3C��3C���C���C���C��3C��fC��3C���C���C¦fCǀ C̙�Cр C֙�C�ffC�fC�3CꙚC�� C��C���D�3D��D�3D��D�3D�3DٚD$�fD)� D.��D3��D8��D=��DB�fDG�3DL� DQ��DV� D[�3D`�fDe�fDj� DoٚDt�fDyٚD��D�ffD���D��D�fD�ffD���D���D��D�ffD���D���D�#3D�l�Dڜ�D��3D�  D�i�D� D��fD�Y�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�^A��A�^A�A�ZA�$�A�
=A���A���A�FA��A�G�A�z�A�n�A�p�A��A�t�AʑhA�jA���A�G�A�S�A���A��PA�/A��7A���A���A�9XA��^A��mA��A�9XA�jA���A��+A�1A��A�hsA��A���A�  A�n�A�v�A��wA��jA���A��/A���A�K�A|ȴAv �Ak��Act�AZ�`AVM�AT�AOƨAG�-AA7LA:5?A2VA.=qA&�!A"�!AdZA-A�FA"�AjAj@�`B@��@�O�@ڟ�@ʰ!@�7L@���@�$�@���@��j@�/@���@�{@�ƨ@���@�V@�J@�M�@��y@�Q�@�bN@��@�-@�@so@j��@^@V@O�w@G;d@>�y@6�+@.�y@*J@$(�@�/@  @(�@��@�;@p�@
M�@V@�7@   11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�^A��A�^A�A�ZA�$�A�
=A���A���A�FA��A�G�A�z�A�n�A�p�A��A�t�AʑhA�jA���A�G�A�S�A���A��PA�/A��7A���A���A�9XA��^A��mA��A�9XA�jA���A��+A�1A��A�hsA��A���A�  A�n�A�v�A��wA��jA���A��/A���A�K�A|ȴAv �Ak��Act�AZ�`AVM�AT�AOƨAG�-AA7LA:5?A2VA.=qA&�!A"�!AdZA-A�FA"�AjAj@�`B@��@�O�@ڟ�@ʰ!@�7L@���@�$�@���@��j@�/@���@�{@�ƨ@���@�V@�J@�M�@��y@�Q�@�bN@��@�-@�@so@j��@^@V@O�w@G;d@>�y@6�+@.�y@*J@$(�@�/@  @(�@��@�;@p�@
M�@V@�7@   11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
B
�
B
�B
�#B
�B
��B
��B
��B
��B
�
B
�B
�B�B_;BĜB�%B�bB�bBs�B_;B>wB33B'�BbB��B�B�'B��B��B�7B�B{�Bw�Bo�Be`BaHB_;BZBN�B:^B&�B"�B�BoB
��B
�B
�B
��B
ŢB
�^B
��B
�B
e`B
E�B
	7B	�NB	��B	B	��B	�1B	hsB	H�B	)�B	�B��B�fB�B�}B�'B�B��B�VB�=B�B�B� B�B�PB��B��BĜB�B�B	B	-B	B�B	e`B	y�B	�VB	��B	�?B	�qB	��B	��B	�B	�yB
	7B
{B
!�B
+B
1'B
:^B
@�B
F�B
L�B
Q�B
W
B
]/B
aHB
e`B
iyB
l�B
o�B
s�B
x�B
~�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�B
�B
�#B
�B
��B
��B
��B
��B
�
B
�B
�B�BbNB��B�7B�uB�uBx�Be`B@�B49B,BoBB�5B�9B��B��B�=B�B{�Bx�Bq�BffBaHB`BB[#BR�B<jB&�B#�B�BuB  B
�B
�B
��B
ƨB
�dB
��B
�+B
hsB
G�B
DB	�TB	��B	ÖB	�B	�=B	jB	J�B	+B	�B��B�mB�B��B�'B�!B�B�\B�DB�B�B�B�B�PB��B�BĜB�B�B	B	-B	B�B	e`B	y�B	�VB	��B	�?B	�qB	��B	��B	�B	�yB
	7B
{B
!�B
+B
1'B
:^B
@�B
F�B
L�B
Q�B
W
B
]/B
aHB
e`B
iyB
l�B
o�B
s�B
x�B
~�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200910051446552009100514465520091005144655200910051728322009100517283220091005172832201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090922125617  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090922125618  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090922125618  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090922125619  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090922125620  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090922125620  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090922125620  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090922125620  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090922125620  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090922130037                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090926035632  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090926035810  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090926035810  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090926035811  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090926035812  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090926035812  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090926035812  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090926035812  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090926035812  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090926040138                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090925172317  CV  DAT$            G�O�G�O�F�l8                JM  ARGQJMQC1.0                                                                 20090925172317  CV  LON$            G�O�G�O�CSu                JM  ARCAJMQC1.0                                                                 20091005144655  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20091005144655  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20091005172832  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015559  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015619                      G�O�G�O�G�O�                