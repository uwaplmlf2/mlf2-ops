CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901577 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               %A   JA  20090912125606  20100219015625  A9_76288_037                    2C  D   APEX-SBE 3533                                                   846 @�K��/�1   @�K*@3�$�/@b��t�j1   ARGOS   A   A   A   @�33AffAd��A�  A�33A�  B	33BffB1��BF  BY��Bn��B�  B�33B�  B���B���B���B�  B�ffB�33B�33B�33B�ffB���CL�C��C33C� C� C� CffC$��C)33C.L�C3�C8  C=�CA�fCF��CP�fC[33Ce  CoffCyL�C�� C�� C���C�ffC���C���C��fC���C���C���C�� C�� C���C CǙ�C̦fC�� C�s3CۦfC�3C�ffCꙚC�Y�C�� C���DٚD�3D��DٚD� DٚD� D$ٚD)�fD.�3D3�fD8ٚD=�fDB��DGٚDL��DQ�3DV� D[��D`��De��Dj� Do�3DtٚDy��D�)�D�ffD�� D��fD�,�D�ffD���D��fD�#3D�ffD���D�� D�fD�ffDڬ�D��3D�)�D�ffD��D��3D�0 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA  A^ffA���A�  A���B��B��B0  BDffBX  Bm33B�33B�ffB�33B�  B���B���B�33Bƙ�B�ffB�ffB�ffB홚B���C �fCffC
��C�C�C�C  C$33C(��C-�fC2�3C7��C<�3CA� CFffCP� CZ��Cd��Co  Cx�fC���C�L�C�ffC�33C�ffC�ffC�s3C���C���C�ffC���C�L�C�ffC�L�C�ffC�s3Cь�C�@ C�s3C�� C�33C�ffC�&fC��C���D� D��D�3D� D�fD� D�fD$� D)��D.��D3��D8� D=��DB�3DG� DL� DQ��DV�fD[� D`�3De� Dj�fDo��Dt� Dy�3D��D�Y�D��3D�ٚD�  D�Y�D�� D�ٚD�fD�Y�D�� D��3D�	�D�Y�Dڠ D��fD��D�Y�D� D��fD�#311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A���A��
A���A��;A�VA�A��`A�FA�A�ȴA�E�A�7LA�`BA�ĜAͧ�A��A�A�A�r�A���A��A��wA�A��`A�  A�x�A�Q�A��A��A��TA���A���A��FA�VA�K�A��\A��^A�{A��#A���A���A���A�`BA�1A�1'A���AhsA{Apr�Am�
AjjAa��AXv�AT�AN�uAJ��AFE�A@1'A=��A<�A6�A&�RA%`BA r�A�;A�#A�A	�A	��Ap�@��@噚@�Ĝ@�v�@��/@�~�@���@�\)@�ƨ@��@�&�@�j@�r�@�^5@��F@�V@�;d@�x�@�K�@��@�z�@���@�bN@w��@s��@jn�@b~�@\�@N�+@B�H@>$�@8r�@3ƨ@-@(Ĝ@ ��@�D@�`@9X@�P@��@�y@�@ 1'?�ƨ?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A���A���A��
A���A��;A�VA�A��`A�FA�A�ȴA�E�A�7LA�`BA�ĜAͧ�A��A�A�A�r�A���A��A��wA�A��`A�  A�x�A�Q�A��A��A��TA���A���A��FA�VA�K�A��\A��^A�{A��#A���A���A���A�`BA�1A�1'A���AhsA{Apr�Am�
AjjAa��AXv�AT�AN�uAJ��AFE�A@1'A=��A<�A6�A&�RA%`BA r�A�;A�#A�A	�A	��Ap�@��@噚@�Ĝ@�v�@��/@�~�@���@�\)@�ƨ@��@�&�@�j@�r�@�^5@��F@�V@�;d@�x�@�K�@��@�z�@���@�bN@w��@s��@jn�@b~�@\�@N�+@B�H@>$�@8r�@3ƨ@-@(Ĝ@ ��@�D@�`@9X@�P@��@�y@�@ 1'?�ƨ?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
B�VB�\B�\B�bB�9B�BB�B��BB
=B�BP�B�B��Bv�Bm�Bs�Bp�BgmBP�BB�B+B�B�RB�B��B��B��B��B�\B�1Bt�B^5BP�BA�B<jB6FB,B#�B�B  B
�B
�`B
ǮB
�FB
��B
�{B
w�B
G�B
<jB
'�B
  B	��B	�wB	��B	�uB	}�B	e`B	ZB	S�B	49B��B�B�5B��B�wB��B�uB�\B�Bs�Bo�Bq�Bx�B�B��B�?B��B�BB��B	\B	:^B	\)B	n�B	�B	�oB	��B	��B	�B	��B	��B	��B	�`B	��B
%B
{B
�B
!�B
-B
8RB
<jB
C�B
G�B
M�B
R�B
W
B
\)B
aHB
gmB
k�B
p�B
w�B
{�B
�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B�VB�\B�\B�bB�9B�BB�B��BBDB�BT�B�=B�By�Bp�Bu�Bs�BjBR�BI�B	7B��B�^B�B��B��B��B��B�bB�DBx�BaHBR�BB�B=qB8RB-B%�B�BB
�B
�mB
ȴB
�LB
��B
��B
z�B
H�B
=qB
)�B
B	��B	�}B	��B	�{B	� B	ffB	ZB	VB	8RB��B�B�;B��B��B��B�uB�hB�Bt�Bp�Br�By�B�B��B�?B��B�BB��B	\B	:^B	\)B	n�B	�B	�oB	��B	��B	�B	��B	��B	��B	�`B	��B
%B
{B
�B
!�B
-B
8RB
<jB
C�B
G�B
M�B
R�B
W
B
\)B
aHB
gmB
k�B
p�B
w�B
{�B
�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.4(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909250823202009092508232020090925082320200909251103572009092511035720090925110357201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090912125605  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090912125606  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090912125606  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090912125606  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090912125607  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090912125607  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090912125608  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090912125608  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090912125608  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090912130059                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090916095637  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090916095835  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090916095835  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090916095836  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090916095837  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090916095837  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090916095837  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090916095837  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090916095837  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090916100457                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090915172335  CV  DAT$            G�O�G�O�F�X8                JM  ARGQJMQC1.0                                                                 20090915172335  CV  DAT$            G�O�G�O�F�X9                JM  ARCAJMQC1.0                                                                 20090925082320  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090925082320  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090925110357  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015551  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015625                      G�O�G�O�G�O�                