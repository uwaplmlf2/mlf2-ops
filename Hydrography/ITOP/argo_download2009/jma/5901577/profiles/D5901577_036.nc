CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901577 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               $A   JA  20090902125557  20100219015624  A9_76288_036                    2C  D   APEX-SBE 3533                                                   846 @�H��g(�1   @�H�o���@3��t�j@b�l�C��1   ARGOS   A   A   A   @�33A  A`  A���A�  A�ffB
��BffB1��BF  BZ��Bn��B�  B�  B�  B�  B���B�ffB�  B���B�  B�  B�  B���B�ffCL�CL�CL�CffC��C� CffC$L�C)ffC.ffC3� C8  C=L�CBffCG33CQ33C[� Ce33CoL�Cy�C���C���C���C�s3C��fC�� C���C�� C��fC���C��3C���C��3C�C�� C̳3Cѳ3C֌�CۦfC���C�� C��C��C���C�� D� DٚD�3D� D� D�fD� D$�3D)�3D.��D3� D8� D=ٚDB� DG��DL�3DQ��DV�fD[��D`ٚDe��DjٚDo��Dt�fDy�3D��D�i�D���D���D��D�i�D���D���D�&fD�i�D���D��D�  D�ffDڦfD��3D�  D�` D�fD��3D�C311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@y��A��A\��A�33A�ffA���B
  B��B0��BE33BZ  Bn  B���B���B���B���B�ffB�  B���B�ffBЙ�Bڙ�B䙚B�ffB�  C�C�C�C33CffCL�C33C$�C)33C.33C3L�C7��C=�CB33CG  CQ  C[L�Ce  Co�Cx�fC�s3C�s3C�s3C�Y�C���C�ffC�� C��fC���C�s3C���C�s3C���C CǦfC̙�Cљ�C�s3Cی�C�� C�fC�s3C�s3C� C��fD�3D��D�fD�3D�3D��D�3D$�fD)�fD.� D3�3D8�3D=��DB�3DG� DL�fDQ��DV��D[� D`��De� Dj��Do��Dt��Dy�fD�3D�c3D��3D��fD�fD�c3D��fD��fD�  D�c3D��3D��3D��D�` Dڠ D���D��D�Y�D� D���D�<�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A�5?A�&�A��A�A���A蟾A�r�A�M�A��A��A���A�5?Aȩ�AľwAþwA¬A�XA��TA�  A��FA��A���A�v�A���A���A��A���A���A���A��A��\A��A��yA��A�bNA��!A�Q�A�r�A�p�A��wA�ȴA��7A���A�7LA�=qAz�At�!Al�Ag+Ac��A`��AXr�AUp�AS;dAL��AJ~�AD5?A?\)A8ȴA0$�A+�^A'ƨA#/A��A�
AVAG�A	�A�D@��@�P@�-@�@���@���@�{@��9@�`B@�(�@���@��u@�O�@��7@���@���@���@�hs@��@���@�p�@��@�7L@~V@z-@t�/@g|�@W
=@K��@D�/@;t�@333@-O�@(�@&5?@|�@1@Ĝ@�@�@�P@�@�@?}@%@ �911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A�5?A�&�A��A�A���A蟾A�r�A�M�A��A��A���A�5?Aȩ�AľwAþwA¬A�XA��TA�  A��FA��A���A�v�A���A���A��A���A���A���A��A��\A��A��yA��A�bNA��!A�Q�A�r�A�p�A��wA�ȴA��7A���A�7LA�=qAz�At�!Al�Ag+Ac��A`��AXr�AUp�AS;dAL��AJ~�AD5?A?\)A8ȴA0$�A+�^A'ƨA#/A��A�
AVAG�A	�A�D@��@�P@�-@�@���@���@�{@��9@�`B@�(�@���@��u@�O�@��7@���@���@���@�hs@��@���@�p�@��@�7L@~V@z-@t�/@g|�@W
=@K��@D�/@;t�@333@-O�@(�@&5?@|�@1@Ĝ@�@�@�P@�@�@?}@%@ �911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
BɺBȴBɺBɺBƨBŢBĜB�hB��B�oB�7B�B�+Bz�Bx�Bq�BiyBT�BE�B>wB1'B)�B�B��B�BB��BŢB�3B��B��B�=Bk�BdZBZBL�BA�B=qB(�B!�BoB
��B
�B
�fB
��B
�qB
��B
�B
bNB
9XB
�B
PB	��B	�5B	ÖB	�9B	��B	�DB	l�B	S�B	>wB	"�B	
=B��B�yB��B�dB�-B��B�hB�7Bv�Bn�BjBm�Bo�B�1B��B��B�RB�NB�B	�B	33B	S�B	m�B	�1B	��B	��B	�9B	�qB	ǮB	��B	�5B	�yB	��B
  B
�B
$�B
1'B
6FB
<jB
D�B
J�B
N�B
P�B
YB
\)B
`BB
dZB
k�B
n�B
s�B
w�B
{�B
~�B
~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111BɺBȴBɺBɺBƨBŢB��B��B��B��B�PB�1B�1B~�By�Br�Bl�BW
BF�B@�B2-B,B �B��B�HB�BǮB�?B��B��B�\Bl�Be`B\)BM�BC�B?}B)�B#�B{B
��B
�B
�mB
�B
�wB
��B
�B
dZB
:^B
�B
VB	��B	�;B	ĜB	�FB	��B	�PB	m�B	VB	@�B	#�B	DB��B�B��B�jB�3B��B�oB�=Bw�Bn�Bk�Bn�Bp�B�7B��B��B�XB�NB�B	�B	33B	S�B	m�B	�1B	��B	��B	�9B	�qB	ǮB	��B	�5B	�yB	��B
  B
�B
$�B
1'B
6FB
<jB
D�B
J�B
N�B
P�B
YB
\)B
`BB
dZB
k�B
n�B
s�B
w�B
{�B
~�B
~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909150823382009091508233820090915082338200909151034112009091510341120090915103411201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090902125556  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090902125557  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090902125557  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090902125558  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090902125559  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090902125559  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090902125559  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090902125559  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090902125559  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090902125954                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090906035606  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090906035734  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090906035735  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090906035735  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090906035736  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090906035736  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090906035736  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090906035736  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090906035736  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090906040136                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090905223854  CV  DAT$            G�O�G�O�F�D8                JM  ARCAJMQC1.0                                                                 20090915082338  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090915082338  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090915103411  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015551  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015624                      G�O�G�O�G�O�                