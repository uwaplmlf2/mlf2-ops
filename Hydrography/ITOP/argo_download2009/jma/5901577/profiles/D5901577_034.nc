CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   p   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4\   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :|   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C<   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   E�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   N�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   W�   CALIBRATION_DATE      	   
                
_FillValue                  �  `�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a\   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    al   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    ap   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        a�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    a�Argo profile    2.2 1.2 19500101000000  5901577 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               "A   JA  20090813100113  20100219015624  A9_76288_034                    2C  D   APEX-SBE 3533                                                   846 @�C�����1   @�C�a��*@3� ě��@b��-V1   ARGOS   A   A   A   @�33A  Ac33A���A�ffA�  B
  B��B2��BF  BZ��Bm��B�33B�ffB�ffB���B�  B�  B�  B�ffB�ffBڙ�B�33B�  B�  C� C� CffCffC�fC�3CffC$��C)L�C.33C3ffC8L�C=�CBL�CG  CP�fC[  CeffCo� CyffC��fC�� C���C�Y�C�s3C��3C���C���C�� C��3C�� C���C���C�� CǙ�C̦fC�� C֌�Cۙ�C�� C���C�3C�� C�� C���DٚD�3D�fD� D� D� D�fD$� D)ٚD.��D3� D8�3D=�3DB� DG�3DL� DQ�3DV�3D[��D`�fDe�3Dj��Do� Dt�fDy��D�)�D�ffD��3D�� D�  D�` D��fD��fD�fD�` D��3D�� D�#3D�ffDڦfD���D�|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�  AffAa��A�  A���A�33B	��BffB2ffBE��BZffBm33B�  B�33B�33B�ffB���B���B���B�33B�33B�ffB�  B���B���CffCffCL�CL�C��C��CL�C$� C)33C.�C3L�C833C=  CB33CF�fCP��CZ�fCeL�CoffCyL�C���C�s3C�� C�L�C�ffC��fC�� C�� C��3C��fC��3C���C�� C³3Cǌ�C̙�Cѳ3Cր Cی�C�3C�� C�fC�3C��3C���D�3D��D� D��DٚDٚD� D$ٚD)�3D.�3D3��D8��D=��DBٚDG��DL��DQ��DV��D[�fD`� De��Dj�fDo��Dt� Dy�3D�&fD�c3D�� D���D��D�\�D��3D��3D�3D�\�D�� D���D�  D�c3Dڣ3D��D�y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�z�A��yA�1'A�XA��A�x�A��A�^A�`BA�;dAݴ9AօA�
=A� �A�~�A��TA�z�A�v�A�ZA�7LA�bNA��!A� �A��PA��A�r�A��;A�^5A�~�A�7LA��/A��A���A��`A�5?A���A�7LA�  A�"�A���A��A�+A�|�A�1'A��A��yA��jA���A�K�AS�A~ �Aw�At�uAk�Agp�Ab�/A_\)A\A�AS��AJȴAD�A9�A4��A0�!A.��A.JA({A jAA�A�-A�yA��@���@�w@�o@�"�@ŉ7@��w@�;d@�5?@�^5@�{@��@��T@���@�`B@�V@�bN@��@�x�@���@�;d@�/@�@��j@vv�@q�7@b-@XA�@P��@IG�@@�@5`B@0A�@+o@'+@#�@I�@9X@ �@dZ@S�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�z�A��yA�1'A�XA��A�x�A��A�^A�`BA�;dAݴ9AօA�
=A� �A�~�A��TA�z�A�v�A�ZA�7LA�bNA��!A� �A��PA��A�r�A��;A�^5A�~�A�7LA��/A��A���A��`A�5?A���A�7LA�  A�"�A���A��A�+A�|�A�1'A��A��yA��jA���A�K�AS�A~ �Aw�At�uAk�Agp�Ab�/A_\)A\A�AS��AJȴAD�A9�A4��A0�!A.��A.JA({A jAA�A�-A�yA��@���@�w@�o@�"�@ŉ7@��w@�;d@�5?@�^5@�{@��@��T@���@�`B@�V@�bN@��@�x�@���@�;d@�/@�@��j@vv�@q�7@b-@XA�@P��@IG�@@�@5`B@0A�@+o@'+@#�@I�@9X@ �@dZ@S�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
Bv�Bx�B�B��B��B�
B��B�dB�!B��B^5Bl�BW
Bx�B�1B�1B�hB�=Bq�B�7B�Bt�BbNBs�Bn�BffBXBH�B>wB=qBN�BA�B=qB0!B:^B?}BK�BH�BB�B?}B2-B%�B�B
=B
��B
�fB
�B
ƨB
�3B
��B
��B
o�B
]/B
9XB
"�B

=B	��B	�sB	�dB	�hB	r�B	@�B	+B	�B	�B	oB��B�mB�5B��B�XB��Bv�Bt�Bq�Bt�B|�B�bB��B��B��B�B	�B	1'B	F�B	YB	q�B	~�B	�oB	��B	��B	�}B	ȴB	�)B	�BB	��B
+B
�B
$�B
(�B
0!B
8RB
B�B
G�B
L�B
O�B
S�B
\)B
e`B
jB
n�B
n�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111Bv�By�B�B��B��B�
B��B�jB�3B��BdZBp�B[#By�B�7B�JB�{B�JBu�B�DB�Bv�Be`Bv�Bo�BhsB\)BK�B@�B@�BO�BA�B>wB33B:^BA�BM�BJ�BB�B@�B33B&�B�BDB
��B
�mB
�#B
ǮB
�9B
��B
��B
p�B
_;B
:^B
#�B
DB	��B	�B	�qB	�uB	u�B	A�B	,B	�B	�B	{B	  B�sB�;B��B�^B��Bw�Bu�Br�Bu�B}�B�bB��B�B��B�B	�B	1'B	F�B	YB	q�B	~�B	�oB	��B	��B	�}B	ȴB	�)B	�BB	��B
+B
�B
$�B
(�B
0!B
8RB
B�B
G�B
L�B
O�B
S�B
\)B
e`B
jB
n�B
n�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200908260743582009082607435820090826074358200908260939112009082609391120090826093911201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090813100111  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090813100113  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090813100114  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090813100114  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090813100115  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090813100115  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090813100115  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090813100115  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090813100116  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090813102414                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090817035834  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090817040045  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090817040046  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090817040046  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090817040047  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090817040047  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090817040047  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090817040047  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090817040048  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090817041034                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090816164602  CV  DAT$            G�O�G�O�F�8                JM  ARCAJMQC1.0                                                                 20090826074358  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090826074358  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090826093911  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015553  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015624                      G�O�G�O�G�O�                