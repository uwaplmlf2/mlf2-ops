CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   t   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F4   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O4   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X4   CALIBRATION_DATE      	   
                
_FillValue                  �  a4   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    b   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    b   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    b   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b(   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b,   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b0   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    b4Argo profile    2.2 1.2 19500101000000  5901577 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               (A   JA  20091012095614  20100219015627  A9_76288_040                    2C  D   APEX-SBE 3533                                                   846 @�R��u�1   @�R���?`@4�z�H@b�+I�1   ARGOS   A   B   B   @�33A33Ad��A�  A�  A�ffB
  BffB2ffBF  BZ  Bm33B�33B�33B�33B�  B���B�  B�ffB���B�  B�  B䙚B�33B�33C� C�C�C33CffCffC�C$��C)ffC.��C3L�C8� C=33CBL�CG�CQ33C[ffCd��CoffCy�C��fC���C��fC��fC�s3C�s3C�� C���C�ٚC���C��fC���C���C�Cǀ C̀ Cь�C�� Cۙ�C�fC�� CꙚC�3C��fC��fD� D��D�3D�3D��D�3D��D$�fD)�fD.ٚD3��D8�fD=ٚDB�3DGٚDL� DQ� DV��D[�fD`�fDe��Dj�fDo��Dt��Dy�fD�,�D�c3D��3D���D�&fD�ffD�� D��D�#3D�i�D��fD��fD��D�l�Dک�D��fD�fD�VfD�3D�� D��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@y��A  Aa��A�ffA�ffA���B	33B��B1��BE33BY33BlffB��B���B���B���B�33B���B�  B�ffBЙ�Bٙ�B�33B���B���CL�C�fC
�fC  C33C33C�fC$ffC)33C.ffC3�C8L�C=  CB�CF�fCQ  C[33Cd��Co33Cx�fC���C��3C���C���C�Y�C�Y�C�ffC�s3C�� C��3C���C�s3C�� C�s3C�ffC�ffC�s3C֦fCۀ C���C�fC� CC��C���D�3D� D�fD�fD� D�fD� D$��D)��D.��D3� D8ٚD=��DB�fDG��DL�3DQ�3DV� D[��D`��De� Dj��Do��Dt��Dy��D�&fD�\�D���D��fD�  D�` D���D��3D��D�c3D�� D�� D�fD�ffDڣ3D�� D� D�P D��D��D�f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��mA�9A�DA�n�A�ZA�XA�ZA�ZA�VA�^5A�VA�%A���Aԥ�A��mA�bNA�33A�AȮA�  A�t�A�C�A�bA��+A�33A�-A�v�A��HA��/A��A�  A�=qA�A�/A�Q�A���A���A��A�7LA�`BA��A�K�A�jA��+A�?}A��A��hA�jA��A`BAzVAt=qAm��Agt�Ab(�A\�9AW7LAU`BAL��AI\)AB�!A>��A9�FA7�
A1�A,ȴA(�\A!�An�A��A�
@���@�(�@���@���@�9X@��@��F@�C�@�(�@�  @�r�@�@���@���@�{@��@�hs@�+@��\@�K�@�I�@��/@��@�K�@z��@k"�@b��@W\)@Ihs@?�P@6�R@0  @+t�@(��@%�@ �9@�h@�#@V@V@�@Q�@�@
�\@
��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��mA�9A�DA�n�A�ZA�XA�ZA�ZA�VA�^5A�VA�%A���Aԥ�A��mA�bNA�33A�AȮA�  A�t�A�C�A�bA��+A�33A�-A�v�A��HA��/A��A�  A�=qA�A�/A�Q�A���A���A��A�7LA�`BA��A�K�A�jA��+A�?}A��A��hA�jA��A`BAzVAt=qAm��Agt�Ab(�A\�9AW7LAU`BAL��AI\)AB�!A>��A9�FA7�
A1�A,ȴA(�\A!�An�A��A�
@���@�(�@���@���@�9X@��@��F@�C�@�(�@�  @�r�@�@���@���@�{@��@�hs@�+@��\@�K�@�I�@��/@��@�K�@z��@k"�@b��@W\)@Ihs@?�P@6�R@0  @+t�@(��@%�@ �9@�h@�#@V@V@�@Q�@�@
�\G�O�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
G�O�B
ɺB
��B
��B
��B
��B
��B
��B
��B
��B
�BF�B��B�BBB��B+B�B�BbBBǮB�wB�B��B��B�{B�hB�DB� By�BffBR�B9XB,B#�B(�B'�B#�BuB�B,B-B$�B�B
��B
�HB
�}B
��B
��B
y�B
VB
5?B
uB	��B	�yB	��B	ÖB	��B	�JB	jB	XB	?}B	49B	�B	
=B��B�TB��BÖB�9B��B��B�-BB�BB��B��B	\B	�B	�B	�B	+B	7LB	F�B	S�B	iyB	y�B	�DB	�VB	��B	�B	ĜB	��B	�B	��B
uB
�B
(�B
8RB
@�B
G�B
L�B
R�B
T�B
W
B
\)B
_;B
cTB
hsB
l�B
q�B
s�B
x�B
|�B
{�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
ɺB
��B
��B
��B
��B
��B
��B
��B
��B
�)BJ�B��B�BBB��B1B�B�BoB	7BȴB��B�B��B��B��B�oB�JB�B}�BiyBVB;dB.B%�B)�B(�B&�B{B�B-B.B&�B�BB
�TB
��B
��B
��B
z�B
XB
6FB
{B	��B	�B	��B	ŢB	��B	�VB	k�B	YB	@�B	6FB	�B	DB��B�ZB�
BĜB�FB��B��B�3BÖB�BB��B��B	\B	�B	�B	�B	+B	7LB	F�B	S�B	iyB	y�B	�DB	�VB	��B	�B	ĜB	��B	�B	��B
uB
�B
(�B
8RB
@�B
G�B
L�B
R�B
T�B
W
B
\)B
_;B
cTB
hsB
l�B
q�B
s�B
x�B
|�G�O�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111114<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
G�O�PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200910250751282009102507512820091025075128200911191143262009111911432620091119114326201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20091012095612  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091012095614  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091012095614  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091012095615  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091012095616  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091012095616  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091012095616  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091012095616  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091012095616  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091012100129                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091016035629  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091016035745  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091016035745  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091016035746  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091016035747  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091016035747  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091016035747  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091016035747  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091016035747  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091016040142                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20091015164012  CV  DAT$            G�O�G�O�F��3                JM  ARSQJMQC1.0                                                                 20091015164012  CF  TEMP            D��D��G�O�                JM  ARSQJMQC1.0                                                                 20091015164012  CF  PSAL            D��D��G�O�                JM  ARCAJMQC1.0                                                                 20091025075128  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20091025075128  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20091119114326  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219015554  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219015627                      G�O�G�O�G�O�                