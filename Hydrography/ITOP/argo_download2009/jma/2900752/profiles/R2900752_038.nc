CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2900752 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               &A   JA  20090906005508  20090909160242                                  2B  A   APEX-SBE 3452                                                   846 @�Ig3��1   @�Igy��@<C�
=p�@a�$�/�1   ARGOS   A   A   A   @�  A  A`  A�  A�33A�ffB	33BffB2��BF��BZ��Bo33B�33B���B�  B�  B���B���B�ffB�  B�  B���B�33B���B���C33CL�C33C  C  CL�CL�C$  C(�fC-��C3ffC8ffC=��CB� CGL�CQ��C[� Ce� Co33CyL�C�� C��3C�� C��fC���C�s3C���C�� C�� C��fC���C���C�� C�Cǳ3Č�Cѳ3Cֳ3Cی�C�3C�fC� C��C��fC��fDٚD�fD�3D��D�fD�fD� D$�fD)��D.� D3� D8��D=� DB�3DG�3DL�3DQٚDVٚD[�3D`��De�3DjٚDo�fDtٚDyٚD�#3D�i�D�� D��3D�,�D�ffD���D��D�)�D�c3D��3D�� D�&fD�c3Dڣ3D���D��D�c3D�fD���D�3311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A��A\��A�ffA���A���BffB��B2  BF  BZ  BnffB���B�34B���B���B�fgB�fgB�  Bƙ�BЙ�B�fgB���B�fgB�34C  C�C  C��C��C�C�C#��C(�3C-��C333C833C=fgCBL�CG�CQfgC[L�CeL�Co  Cy�C��fC���C�ffC���C�s3C�Y�C�s3C��fC��fC���C�s3C�s3C�ffC�s3CǙ�C�s3Cљ�C֙�C�s3C���C��C�ffC�s3C��C���D��D��D�fD� D��D��D�3D$��D)� D.�3D3�3D8� D=�3DB�fDG�fDL�fDQ��DV��D[�fD`��De�fDj��Do��Dt��Dy��D��D�c4D���D���D�&gD�` D��gD��4D�#4D�\�D���D�ٚD�  D�\�Dڜ�D��gD�4D�\�D� D��gD�,�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�$�A� �A�33A�5?A�G�A���A�;dAݮAЃA���A�&�A�ZA�ƨA�&�A���A��A� �A���A�ffA���A��A���A�hsA�"�A��wA�`BA���A��9A�M�A�
=A��mA���A�/A���A��uA���A�x�A� �A���A���A�^5A��TA�$�A�ZA��`A�A�-A�ffA�33A��A�dZAdZA}+A{�-AwG�Ar^5Am|�Ak�Ai��Ae�A^�AW�AU&�ASoAMp�AEt�ACA@��A>�jA=`BA(�uA%\)A�/A��A�wA�@�5?@�~�@� �@��@���@��u@���@��@��@�ƨ@���@��9@���@�K�@�Z@��@�{@�Q�@��T@e@^@SdZ@D(�@;�@9hs@5�T@2�@-/@&@ �u@��@Ĝ@�@z�@+@�@+@&�?�"�?�S�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�$�A� �A�33A�5?A�G�A���A�;dAݮAЃA���A�&�A�ZA�ƨA�&�A���A��A� �A���A�ffA���A��A���A�hsA�"�A��wA�`BA���A��9A�M�A�
=A��mA���A�/A���A��uA���A�x�A� �A���A���A�^5A��TA�$�A�ZA��`A�A�-A�ffA�33A��A�dZAdZA}+A{�-AwG�Ar^5Am|�Ak�Ai��Ae�A^�AW�AU&�ASoAMp�AEt�ACA@��A>�jA=`BA(�uA%\)A�/A��A�wA�@�5?@�~�@� �@��@���@��u@���@��@��@�ƨ@���@��9@���@�K�@�Z@��@�{@�Q�@��T@e@^@SdZ@D(�@;�@9hs@5�T@2�@-/@&@ �u@��@Ĝ@�@z�@+@�@+@&�?�"�?�S�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	VB	T�B	YB	ZB	��B	��B
PB	��B	��B
(�B
N�B
~�B
��B
��B
��B
�?B
��B
��B
��B
�#B
�B
�`B
�yB
�yB
�B
�yB
�mB
�ZB
��B
�BBB  B  B
��B
��B
��B
��B
��B
��B
��B
�B
�B
�yB
�fB
�ZB
�;B
��B
��B
�FB
��B
��B
�hB
�7B
u�B
`BB
K�B
D�B
9XB
'�B
B	�yB	�5B	��B	�^B	��B	�oB	�DB	~�B	o�B	 �B	�B�B�`B�B�}B�DBo�B�7B�=B�DB�uB��B��B��BBŢB��B�B��B	VB	hB	�B	�B	$�B	bNB	s�B	�B	�B	ƨB	��B	�
B	�5B	�B	��B
	7B
�B
�B
�B
%�B
49B
<jB
G�B
VB
_;B
iy11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	VB	T�B	YB	ZB	��B	��B
PB	��B	��B
(�B
N�B
~�B
��B
��B
��B
�?B
��B
��B
��B
�#B
�B
�`B
�yB
�yB
�B
�yB
�mB
�ZB
��B
�BBB  B  B
��B
��B
��B
��B
��B
��B
��B
�B
�B
�yB
�fB
�ZB
�;B
��B
��B
�FB
��B
��B
�hB
�7B
u�B
`BB
K�B
D�B
9XB
'�B
B	�yB	�5B	��B	�^B	��B	�oB	�DB	~�B	o�B	 �B	�B�B�`B�B�}B�DBo�B�7B�=B�DB�uB��B��B��BBŢB��B�B��B	VB	hB	�B	�B	$�B	bNB	s�B	�B	�B	ƨB	��B	�
B	�5B	�B	��B
	7B
�B
�B
�B
%�B
49B
<jB
G�B
VB
_;B
iy11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090906005507  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090906005508  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090906005508  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090906005509  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090906005510  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090906005510  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090906005510  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090906005510  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090906005510  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090906010033                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090909155729  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090909155906  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090909155906  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090909155907  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090909155908  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090909155908  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090909155908  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090909155908  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090909155908  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090909160242                      G�O�G�O�G�O�                