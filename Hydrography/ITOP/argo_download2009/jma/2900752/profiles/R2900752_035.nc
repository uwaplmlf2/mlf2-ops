CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2900752 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               #A   JA  20090807035628  20090810190418                                  2B  A   APEX-SBE 3452                                                   846 @�A���
1   @�A�x��@;�     @a����F1   ARGOS   A   A   A   @���A33AfffA�33A���A홚B	��BffB2  BF  BZffBl  B���B���B�  B�  B���B���B�  B���B�  B�ffB�33B�33B���CL�CffCffC33C33CffCL�C$� C)  C.L�C3�C8  C=� CBL�CGffCQ33C[L�CeffCoffCy�C���C�� C�� C��3C�s3C�s3C���C���C�� C��fC���C���C���C�� Cǌ�C�� Cѳ3C֦fC�� C�� C�3C�3CC��3C��3D��D� D� D�3D��D�3D� D$ٚD)� D.��D3�3D8��D=�fDB� DG�3DL�fDQ��DV��D[�3D`��De��Dj�3DoٚDt�fDy�fD�)�D�p D�� D�� D�#3D�c3D�� D�� D��D�ffD�� D��fD�)�D�` Dڰ D��D�&fD�\�D�3D�� D��311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�fgA��Ad��A�ffA���A���B	34B  B1��BE��BZ  Bk��B���B���B���B���B�fgB���B���Bƙ�B���B�33B�  B�  B�fgC33CL�CL�C�C�CL�C33C$ffC(�fC.33C3  C7�fC=ffCB33CGL�CQ�C[33CeL�CoL�Cy  C���C�s3C�s3C��fC�ffC�ffC���C���C��3C���C���C���C���C³3Cǀ C̳3CѦfC֙�C۳3C�3C�fC�fC��C��fC��fD�gDٚD��D��D�gD��DٚD$�4D)��D.�gD3��D8�gD=� DBٚDG��DL� DQ�gDV�gD[��D`�gDe�gDj��Do�4Dt� Dy� D�&gD�l�D���D���D�  D�` D���D���D��D�c3D���D��3D�&gD�\�Dڬ�D��gD�#3D�Y�D� D���D�� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�9A�jA�jA�A�!A�ƨA�&�A��A��A�%A���A�1A���A��A��RA�z�A�VA�A��yA��;A�v�A��yA���A��`A��A��jA��TA�`BA���A���A��RA�ƨA��HA���A�\)A�{A�;dA�ȴA��A��9A�jA���A�ĜA�t�A���A�dZA��uA��-A���A�E�A�%A��A�M�A�A�A~��Az��AvbAtbNAq��Ap$�Ai�^AedZAbZA_hsA]��AT�HAK��AJ�AB�uA?��A:bA.1A%��A�TA-A�AA�@��R@�^@�1'@�5?@�7L@���@��w@��@���@�=q@�V@�Q�@��D@�;d@��u@�A�@���@�9X@t��@d1@["�@N��@B�H@=/@4��@/�w@-�@)��@��@7L@�m@�u@z�@�T@C�@ ��?��?�?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�9A�jA�jA�A�!A�ƨA�&�A��A��A�%A���A�1A���A��A��RA�z�A�VA�A��yA��;A�v�A��yA���A��`A��A��jA��TA�`BA���A���A��RA�ƨA��HA���A�\)A�{A�;dA�ȴA��A��9A�jA���A�ĜA�t�A���A�dZA��uA��-A���A�E�A�%A��A�M�A�A�A~��Az��AvbAtbNAq��Ap$�Ai�^AedZAbZA_hsA]��AT�HAK��AJ�AB�uA?��A:bA.1A%��A�TA-A�AA�@��R@�^@�1'@�5?@�7L@���@��w@��@���@�=q@�V@�Q�@��D@�;d@��u@�A�@���@�9X@t��@d1@["�@N��@B�H@=/@4��@/�w@-�@)��@��@7L@�m@�u@z�@�T@C�@ ��?��?�?���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
�B
�B
�B
�B
�%B
��B
ÖB
ǮB
��B
��B
��B
��B
��B
��B
�B
�#B
�fBB1B�B\B
��B
��B
�B
�B
��B
�B
�B
�B
��B  B
�B
��B
��B
��B
��B
��BB
��BB%BVB%B
��B
��B
�B
�B
�TB
�BB
��B
�}B
�?B
�B
��B
�B
q�B
iyB
\)B
P�B
5?B
#�B
�B
1B	��B	�B	�B	��B	�B	w�B	_;B	0!B	JB��B�#BȴB�!B��B��B�\B��B��B��B��B�B�-B�RBŢB��B�B�B��B	1B	PB	 �B	J�B	l�B	�B	��B	�XB	ƨB	�B	�fB	�B	��B
JB
�B
(�B
1'B
=qB
P�B
YB
\)B
_;B
hsB
j11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�B
�B
�B
�B
�B
�%B
��B
ÖB
ǮB
��B
��B
��B
��B
��B
��B
�B
�#B
�fBB1B�B\B
��B
��B
�B
�B
��B
�B
�B
�B
��B  B
�B
��B
��B
��B
��B
��BB
��BB%BVB%B
��B
��B
�B
�B
�TB
�BB
��B
�}B
�?B
�B
��B
�B
q�B
iyB
\)B
P�B
5?B
#�B
�B
1B	��B	�B	�B	��B	�B	w�B	_;B	0!B	JB��B�#BȴB�!B��B��B�\B��B��B��B��B�B�-B�RBŢB��B�B�B��B	1B	PB	 �B	J�B	l�B	�B	��B	�XB	ƨB	�B	�fB	�B	��B
JB
�B
(�B
1'B
=qB
P�B
YB
\)B
_;B
hsB
j11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090807035626  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090807035628  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090807035628  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090807035628  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090807035629  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090807035629  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090807035630  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090807035630  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090807035630  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090807040124                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090810185816  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090810190046  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090810190046  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090810190047  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090810190048  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090810190048  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090810190048  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090810190048  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090810190048  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090810190418                      G�O�G�O�G�O�                