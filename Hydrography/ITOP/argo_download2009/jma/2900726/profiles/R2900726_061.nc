CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2900726 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               =A   JA  20091019005651  20091022190259                                  2B  A   APEX-SBE 3464                                                   846 @�T2M���1   @�T4�Es@8��
=p�@a�hr� �1   ARGOS   A   A   A   @�  A  A`  A�33A���A�  B	��B33B2ffBF  B[33Bn  B�33B���B�33B�33B���B���B���B�33Bљ�B�  B���B���B�ffCffC33CL�C� C  C�CL�C$L�C)�C.33C3� C833C=33CBffCGffCQ33C[ffCeL�Co33Cy�C���C��3C���C��fC�s3C�� C���C���C���C�� C��3C���C���C�Cǌ�C�� Cь�C֙�Cۙ�C���C�� C�3C�� C��fC��fD�fD��D��D�3D�fD�3D�fD$�fD)��D.��D3�fD8ٚD=� DB� DG� DL�fDQ�fDV��D[ٚD`ٚDe��Dj�fDo� Dt��DyٚD�#3D�l�D�� D�� D�  D�\�D�� D���D��D�i�D��3D��3D�,�D�ffDک�D��3D�,�D�` D�D��fD��311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA33A[33A���A�fgA陚BfgB  B133BD��BZ  Bl��B���B�  B���B���B�  B�33B�  Bƙ�B�  B�ffB�33B�33B���C�C�fC  C33C�3C��C  C$  C(��C-�fC333C7�fC<�fCB�CG�CP�fC[�Ce  Cn�fCx��C�s4C���C�s4C�� C�L�C�Y�C��gC�fgC�s4C���C���C�s4C�fgC�s4C�fgC̙�C�fgC�s4C�s4C�gC噚C��CC� C�� D�3D��D��D� D�3D� D�3D$�3D)��D.��D3�3D8�gD=��DB��DG��DL�3DQ�3DV�gD[�gD`�gDe��Dj�3Do��Dt��Dy�gD��D�c3D��fD��fD�fD�S3D��fD��3D�3D�` D���D�ٙD�#3D�\�Dڠ D�ٙD�#3D�VfD� D���D�y�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��;A�ȴA�jA�x�A�~�A�A�7A�DA�x�A�33A�5?A�A��A��A�5?A�XA���A���A��DA�VA���A�ZA�~�A�ĜA���A���A��A��A�ffA���A���A��uA�r�A�oA��jA��A�A�ĜA��A��A��9A�O�A�XA�&�A�A�bNA��-A�A{AwAr�HArjAn�Aj-Ag�PAbr�A]�AW`BAP��AMG�AI�ACS�A?"�A;
=A8M�A3K�A-|�A*Q�A$��A"ffAt�A��AbN@�K�@�\)@އ+@�?}@�A�@��@��-@�@��7@�\)@��+@�=q@���@�&�@�Ĝ@�hs@�A�@�$�@\)@{�F@q��@m/@\Z@SC�@L9X@H�`@A��@8r�@1X@+dZ@%�@�P@"�@�w@M�@5?@	�@�j@X?���?��u?���?��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��;A�ȴA�jA�x�A�~�A�A�7A�DA�x�A�33A�5?A�A��A��A�5?A�XA���A���A��DA�VA���A�ZA�~�A�ĜA���A���A��A��A�ffA���A���A��uA�r�A�oA��jA��A�A�ĜA��A��A��9A�O�A�XA�&�A�A�bNA��-A�A{AwAr�HArjAn�Aj-Ag�PAbr�A]�AW`BAP��AMG�AI�ACS�A?"�A;
=A8M�A3K�A-|�A*Q�A$��A"ffAt�A��AbN@�K�@�\)@އ+@�?}@�A�@��@��-@�@��7@�\)@��+@�=q@���@�&�@�Ĝ@�hs@�A�@�$�@\)@{�F@q��@m/@\Z@SC�@L9X@H�`@A��@8r�@1X@+dZ@%�@�P@"�@�w@M�@5?@	�@�j@X?���?��u?���?��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
hsB
��B
�HB
�TB
�`B
�yB
�B
�yB
�BB
�B
�#B
�B
�`B
�B�BhB	7BJBPB
=BDBDBPBJBVBoBoB�B�B�B�B�B�B�B{BuBhBVB	7B
��B
��B
�B
�#B
��B
ŢB
�?B
��B
�DB
w�B
e`B
aHB
O�B
:^B
0!B
�B	��B	�;B	��B	�'B	��B	�B	l�B	[#B	N�B	6FB	�B	PB��B�B��B�B�%BgmBS�BF�B?}BH�BL�BVBgmBw�B�DB��B�B��B�)B�B	B	1B	uB	"�B	,B	L�B	\)B	�1B	��B	�!B	�dB	��B	�NB	�B	��B

=B
�B
"�B
,B
8RB
A�B
L�B
ZB
bNB
iyB
p�B
t�B
t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�B
hsB
��B
�HB
�TB
�`B
�yB
�B
�yB
�BB
�B
�#B
�B
�`B
�B�BhB	7BJBPB
=BDBDBPBJBVBoBoB�B�B�B�B�B�B�B{BuBhBVB	7B
��B
��B
�B
�#B
��B
ŢB
�?B
��B
�DB
w�B
e`B
aHB
O�B
:^B
0!B
�B	��B	�;B	��B	�'B	��B	�B	l�B	[#B	N�B	6FB	�B	PB��B�B��B�B�%BgmBS�BF�B?}BH�BL�BVBgmBw�B�DB��B�B��B�)B�B	B	1B	uB	"�B	,B	L�B	\)B	�1B	��B	�!B	�dB	��B	�NB	�B	��B

=B
�B
"�B
,B
8RB
A�B
L�B
ZB
bNB
iyB
p�B
t�B
t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091019005650  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091019005651  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091019005653  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091019005654  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091019005655  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091019005655  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091019005655  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091019005655  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091019005655  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091019010135                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091022185753  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091022185948  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091022185948  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091022185948  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091022185950  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091022185950  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091022185950  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091022185950  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091022185950  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091022190259                      G�O�G�O�G�O�                