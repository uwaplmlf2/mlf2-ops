CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2900726 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ;A   JA  20090929035757  20091002190116                                  2B  A   APEX-SBE 3464                                                   846 @�O3oP1   @�O3�c��@8�33333@a�     1   ARGOS   A   A   A   @�ffA  Ad��A���Ař�A�  B
ffB  B333BDffBZffBo33B���B���B�33B�33B���B�  B���B�33B���B�ffB���B���B���C�C� C  C33C33C  C�fC#�fC(��C.ffC3L�C833C=�CB�CG� CQffC[L�CeL�Co33Cy33C���C�� C��3C�ffC��fC�� C��3C�s3C��3C��fC��fC��fC�� C�Cǀ C̦fCр C�� Cۙ�C�3C�� CꙚCC���C���D��D�3D��D��D� D� D�3D$�3D)�fD.��D3ٚD8�fD=� DB� DG�fDL� DQ��DV�fD[��D`�fDe�fDj� DoٚDt� Dy�fD��D�l�D���D��3D�,�D�l�D���D��3D��D�p D��fD���D��D�Y�Dڠ D��fD�)�D�VfD�3D��3D�f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�33AffAc33A���A���A�33B
  B��B2��BD  BZ  Bn��B���B�fgB�  B�  B�fgB���B���B�  BЙ�B�33B䙚BB�fgC  CffC
�fC�C�C�fC��C#��C(�3C.L�C333C8�C=  CB  CGffCQL�C[33Ce33Co�Cy�C���C�s3C��fC�Y�C���C�s3C��fC�ffC��fC���C���C���C��3C�C�s3C̙�C�s3Cֳ3Cی�C�fC�3C��C��C��C���D�gD��D�gD�gDٚDٚD��D$��D)� D.�gD3�4D8� D=ٚDBٚDG� DL��DQ�gDV� D[�gD`� De� DjٚDo�4DtٚDy� D�gD�i�D���D�� D�)�D�i�D��gD�� D��D�l�D��3D�ٚD��D�VgDڜ�D��3D�&gD�S3D� D�� D�311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A曦A��A��A��A��A�v�A���A�
=A�jA�r�A�{A˸RA�ĜA�ƨA��A�`BA��A�9XA���A��A��A��\A�ZA�G�A���A��PA�JA��A��A��+A���A��yA��jA��A���A�ȴA�VA���A�VA�A�JA�A���A��HA��FA�VA���A�?}A�n�A}�#A|=qAw�;Av1ArjAl��Aj��Ae+A_oA\��AY��AS�
AO�;AI�TAA��A<��A2�DA,��A)"�A'A ^5AC�A`BA�!A�@��w@�@ߝ�@Ӆ@��@�"�@�M�@�z�@�G�@�~�@��@���@���@�`B@��@�Q�@�hs@y&�@up�@s�m@r^5@f5?@[�@Q7L@I�7@?�@6�+@0  @%�T@�P@S�@��@33@�m@�@�m@ Q�?�ƨ?���?���?�Ĝ?�  11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A曦A��A��A��A��A�v�A���A�
=A�jA�r�A�{A˸RA�ĜA�ƨA��A�`BA��A�9XA���A��A��A��\A�ZA�G�A���A��PA�JA��A��A��+A���A��yA��jA��A���A�ȴA�VA���A�VA�A�JA�A���A��HA��FA�VA���A�?}A�n�A}�#A|=qAw�;Av1ArjAl��Aj��Ae+A_oA\��AY��AS�
AO�;AI�TAA��A<��A2�DA,��A)"�A'A ^5AC�A`BA�!A�@��w@�@ߝ�@Ӆ@��@�"�@�M�@�z�@�G�@�~�@��@���@���@�`B@��@�Q�@�hs@y&�@up�@s�m@r^5@f5?@[�@Q7L@I�7@?�@6�+@0  @%�T@�P@S�@��@33@�m@�@�m@ Q�?�ƨ?���?���?�Ĝ?�  11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�jB
�jB
�qB
�jB
�dB
�RB
�9B
�3B
�^B
��BBBDBbB�B�B�B�B!�B!�B �B�B�B�B�BuB{BuBbBhBoBbBVBbB{B�B�B{B{BoB\B+B
��B
�B
�NB
�B
ƨB
�!B
��B
��B
�bB
{�B
r�B
_;B
H�B
;dB
#�B
+B	��B	�B	��B	�jB	��B	x�B	^5B	49B	�B	VB	B�fB��B�!B��B�%BaHBQ�BF�B?}B=qBD�BM�Br�B�bB��B��B�^BǮB�)B�B	
=B	�B	8RB	B�B	G�B	L�B	q�B	��B	�LB	ȴB	�/B	�B	��B
\B
�B
$�B
0!B
7LB
F�B
Q�B
[#B
cTB
iyB
m�B
r�B
v�B
x�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�jB
�jB
�qB
�jB
�dB
�RB
�9B
�3B
�^B
��BBBDBbB�B�B�B�B!�B!�B �B�B�B�B�BuB{BuBbBhBoBbBVBbB{B�B�B{B{BoB\B+B
��B
�B
�NB
�B
ƨB
�!B
��B
��B
�bB
{�B
r�B
_;B
H�B
;dB
#�B
+B	��B	�B	��B	�jB	��B	x�B	^5B	49B	�B	VB	B�fB��B�!B��B�%BaHBQ�BF�B?}B=qBD�BM�Br�B�bB��B��B�^BǮB�)B�B	
=B	�B	8RB	B�B	G�B	L�B	q�B	��B	�LB	ȴB	�/B	�B	��B
\B
�B
$�B
0!B
7LB
F�B
Q�B
[#B
cTB
iyB
m�B
r�B
v�B
x�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090929035755  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090929035757  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090929035757  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090929035757  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090929035759  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090929035759  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090929035759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090929035759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090929035759  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090929040320                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091002185632  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091002185805  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091002185805  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091002185806  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091002185807  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091002185807  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091002185807  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091002185807  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091002185807  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091002190116                      G�O�G�O�G�O�                