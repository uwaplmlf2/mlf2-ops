CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901532 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ]A   JA  20090923095506  20100219014839  A9_66114_093                    2C  D   APEX-SBE 2823                                                   846 @�M�!_�%1   @�M�k$�@42���m@bpě��T1   ARGOS   A   A   A   @�33AffAh  A�33A�33A�  B	��B��B2ffBFffBZffBl��B�  B�  B���B���B�  B�ffB�ffB�  B�ffB�ffB�  B�ffB�33CffC� C� C��C33C33C33C#�fC)  C.L�C3�C7�fC=33CBL�CG� CQffC[  Cd�fCo�Cx�fC�� C�� C��fC���C�� C�Y�C���C�ffC���C��3C���C���C��3C¦fCǳ3C̙�Cь�C�s3Cۙ�C���C�3CꙚC�3C�� C�� D�fD� D��D� D�3D� DٚD$� D)��D.ٚD3�fD8��D=ٚDB�3DG��DL�3DQ� DVٚD[�3D`� DeٚDj��Do��Dt�3Dy�fD�,�D�c3D��fD��fD�,�D�ffD��fD���D�  D�i�D���D���D�#3D�l�Dڠ D���D�  D�c3D� D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A��A^ffA�ffA�ffA�33B33B33B0  BD  BX  BjffB��B���B�ffB�ffB���B�33B�33B���B�33B�33B���B�33B�  C ��C�fC
�fC  C��C��C��C#L�C(ffC-�3C2� C7L�C<��CA�3CF�fCP��CZffCdL�Cn� CxL�C�33C�33C�Y�C�@ C�33C��C�@ C��C�@ C�ffC�� C�� C�ffC�Y�C�ffC�L�C�@ C�&fC�L�C�� C�ffC�L�C�ffC�s3C�33D� D��D�fD��D��D��D�3D$��D)�fD.�3D3� D8�fD=�3DB��DG�fDL��DQ��DV�3D[��D`��De�3Dj�fDo�fDt��Dy� D��D�P D��3D��3D��D�S3D��3D�ٚD��D�VfD��fD�ɚD� D�Y�Dڌ�D�ɚD��D�P D��D�l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�hA�hA�uA�uA畁A��A� �A�{A���A�x�A���A�VA��AЮA��A�%A���A�n�AÙ�A�Q�A�ƨA��A��A�v�A�33A�A��PA�^5A�VA���A�VA��^A�1'A��A��A�XA��A�"�A��hA��A�ƨA�^5A���A�A�A��RA��hA�(�A��jA��!A|Q�Axz�AsS�Al��AghsAd�`Ab�A[��AX�/APv�AN�9AEoAAhsA<v�A6v�A0�RA-�A%A��Ap�A��A�A9X@���@�o@�p�@�7L@�A�@�j@���@��7@���@���@�A�@��H@��
@���@��7@��@�=q@�V@��#@��9@�J@��R@��u@}?}@r��@h��@Vff@H�@:��@4(�@-��@(bN@&$�@!��@@�@��@  @(�@	x�@
=@33@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�hA�hA�uA�uA畁A��A� �A�{A���A�x�A���A�VA��AЮA��A�%A���A�n�AÙ�A�Q�A�ƨA��A��A�v�A�33A�A��PA�^5A�VA���A�VA��^A�1'A��A��A�XA��A�"�A��hA��A�ƨA�^5A���A�A�A��RA��hA�(�A��jA��!A|Q�Axz�AsS�Al��AghsAd�`Ab�A[��AX�/APv�AN�9AEoAAhsA<v�A6v�A0�RA-�A%A��Ap�A��A�A9X@���@�o@�p�@�7L@�A�@�j@���@��7@���@���@�A�@��H@��
@���@��7@��@�=q@�V@��#@��9@�J@��R@��u@}?}@r��@h��@Vff@H�@:��@4(�@-��@(bN@&$�@!��@@�@��@  @(�@	x�@
=@33@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB.B.B/B8RBH�BVBW
BW
BW
BiyB�BZB[#B\)BXB\)B[#BQ�BE�B33B)�B'�B{B1BB��B�`B��B�FB��B�JB~�Bp�BcTB[#BS�BK�B:^B1'B'�B�B
=BB
��B
�TB
��B
ÖB
�?B
�'B
�%B
t�B
]/B
:^B
�B
uB
B	�ZB	��B	�B	��B	t�B	aHB	L�B	0!B	�B	B�B��B�dB�-B��B�7Bz�Bn�Bq�Bq�By�B�7B��B�dB�B��B	uB	7LB	J�B	]/B	l�B	u�B	|�B	�B	��B	��B	�!B	ƨB	��B	�B	��B
1B
�B
$�B
%�B
1'B
5?B
=qB
H�B
J�B
O�B
ZB
aHB
dZB
iyB
m�B
q�B
v�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B.B.B/B8RBH�BVBW
BW
BXBp�B"�B\)B\)B^5BZB_;B]/BT�BI�B5?B,B+B�B1BB��B�sB�B�XB��B�VB�Br�BdZB\)BVBM�B;dB2-B(�B�B
=BB
��B
�ZB
��B
ĜB
�?B
�3B
�+B
u�B
_;B
;dB
 �B
{B
+B	�`B	��B	�B	��B	u�B	bNB	N�B	1'B	�B	+B�B�B�jB�3B��B�=B{�Bp�Bq�Br�Bz�B�7B��B�dB�B��B	uB	7LB	J�B	]/B	l�B	u�B	|�B	�B	��B	��B	�!B	ƨB	��B	�B	��B
1B
�B
$�B
%�B
1'B
5?B
=qB
H�B
J�B
O�B
ZB
aHB
dZB
iyB
m�B
q�B
v�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.6(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200910060751082009100607510820091006075108200910060803362009100608033620091006080336201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090923095505  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090923095506  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090923095506  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090923095506  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090923095507  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090923095508  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090923095508  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090923095508  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090923095508  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090923095508  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090923100216                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090927005511  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090927005723  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090927005723  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090927005724  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090927005724  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090927005725  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090927005725  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090927005725  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090927005725  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090927005725  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090927010224                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090926163048  CV  DAT$            G�O�G�O�F�m�                JM  ARCAJMQC1.0                                                                 20091006075108  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20091006075108  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20091006080336  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219014757  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219014839                      G�O�G�O�G�O�                