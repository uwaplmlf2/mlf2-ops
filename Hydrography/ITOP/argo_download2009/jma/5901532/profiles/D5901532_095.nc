CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901532 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               _A   JA  20091013095431  20100219014843  A9_66114_095                    2C  D   APEX-SBE 2823                                                   846 @�R����1   @�R�|�	@4�$�/�@bhj~��#1   ARGOS   A   A   A   @�  AffAfffA�  A�33A�  B	��BffB0��BF  BX��Bo33B���B�  B���B�  B���B�  B���B�ffB�ffB�ffB�  B���B���CffC� CffC33C� C��C�3C$�C)33C.� C3� C8  C=�CB� CG��CQ� C[ffCe�Co� CyL�C���C��fC�� C��fC���C�Y�C���C���C���C�� C��3C���C���C¦fCǌ�Č�Cѳ3C�s3Cۙ�C�� C��C�fC�� C�� C���D��D�3D� D�3D��D�3D�3D$� D)��D.ٚD3ٚD8�fD=�fDBٚDG�3DL� DQ� DVٚD[� D`�3De�3Dj� DoٚDtٚDy�3D�&fD�l�D���D�� D�#3D�p D���D��D�  D�\�D���D�� D�)�D�` Dڜ�D���D��D�` D�D�s31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33A[33A�ffA���A�ffB��B��B.  BC33BV  BlffB�33B���B�ffB���B�ffB���B�33B�  B�  B�  B㙚B�ffB�ffC �3C��C
�3C� C��C�C  C#ffC(� C-��C2��C7L�C<ffCA��CF�fCP��CZ�3CdffCn��Cx��C�@ C�L�C�&fC�L�C�@ C�  C�@ C�33C�33C�ffC�Y�C�@ C�33C�L�C�33C�33C�Y�C��C�@ C�&fC�33C�L�C�ffC�ffC�33D��D�fD�3D�fD� D�fD�fD$�3D)� D.��D3��D8��D=��DB��DG�fDL�3DQ�3DV��D[�3D`�fDe�fDj�3Do��Dt��Dy�fD� D�VfD��fD�ɚD��D�Y�D��fD��3D�	�D�FfD��fD�ɚD�3D�I�DچfD��fD�3D�I�D�3D�\�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�"�A�"�A�&�A�"�A�(�A��A�A���A��A�7A�A䗍A��
A�l�AѮAЍPA��
Aκ^A�;dA���Aư!A���A��A�7LA��A��mA�M�A��uA�C�A�ƨA�E�A��
A���A�%A��-A�n�A���A���A��/A�z�A�G�A�1A�;dA���A��-A��jA��A�;dA�^5A�p�A���A��A{�wAv��AtQ�Aq`BAh��Aa�AZ��ATA�AI�7AB�yA=
=A7��A4bA-�TA"�9A�DAoAA
~�A��@�&�@�`B@���@��`@ȓu@��!@��y@���@�1@��j@���@��@��@�G�@�S�@�`B@��@��P@���@��7@�j@��#@�b@\)@l�@`1'@X��@Kt�@A��@;S�@4I�@.��@&{@�@C�@v�@(�@��@�@Q�@�H?�I�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�"�A�"�A�&�A�"�A�(�A��A�A���A��A�7A�A䗍A��
A�l�AѮAЍPA��
Aκ^A�;dA���Aư!A���A��A�7LA��A��mA�M�A��uA�C�A�ƨA�E�A��
A���A�%A��-A�n�A���A���A��/A�z�A�G�A�1A�;dA���A��-A��jA��A�;dA�^5A�p�A���A��A{�wAv��AtQ�Aq`BAh��Aa�AZ��ATA�AI�7AB�yA=
=A7��A4bA-�TA"�9A�DAoAA
~�A��@�&�@�`B@���@��`@ȓu@��!@��y@���@�1@��j@���@��@��@�G�@�S�@�`B@��@��P@���@��7@�j@��#@�b@\)@l�@`1'@X��@Kt�@A��@;S�@4I�@.��@&{@�@C�@v�@(�@��@�@Q�@�H?�I�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB{B{B�B{B�B{B\B\BPB{B�B�B��B33BI�BW
B]/BcTBm�BcTB`BB&�B+BuBVBB�B�TB�#BǮB�dB��B�uB�\B�+B� By�Bo�BT�B>wB9XB/B+B�BJB+B
��B
�B
��B
�qB
�3B
��B
�B
k�B
_;B
L�B
 �B
B	�5B	�}B	�VB	p�B	T�B	>wB	,B	bB�ZB��B�wB��B��B�Bz�Bt�Bn�Bu�B�B��B�?B�B�B	
=B	#�B	8RB	=qB	VB	bNB	k�B	~�B	�JB	��B	�3B	�^B	��B	�B	�ZB	�fB	��B
bB
�B
%�B
.B
5?B
;dB
D�B
J�B
P�B
W
B
ZB
^5B
ffB
l�B
s�B
x�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B{B{B�B{B�B{B\B\BPB{B�B(�B��B5?BJ�BXB^5Be`Bn�BffBdZB)�B,B�BbB+B�B�ZB�5BȴB�wB��B�{B�bB�1B�Bz�Bq�BYB?}B;dB/B,B!�BPB1B
��B
�B
��B
�qB
�9B
��B
�%B
l�B
`BB
N�B
"�B
+B	�BB	B	�bB	r�B	VB	?}B	.B	uB�`B��B��B��B��B�%B{�Bu�Bp�Bv�B�B��B�FB�B�B	
=B	#�B	8RB	=qB	VB	bNB	k�B	~�B	�JB	��B	�3B	�^B	��B	�B	�ZB	�fB	��B
bB
�B
%�B
.B
5?B
;dB
D�B
J�B
P�B
W
B
ZB
^5B
ffB
l�B
s�B
x�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.7(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200910260757332009102607573320091026075733200910260923212009102609232120091026092321201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20091013095429  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091013095431  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091013095431  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091013095431  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091013095431  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091013095433  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091013095433  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091013095433  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091013095433  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091013095433  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091013100110                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091016215506  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091016215749  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091016215749  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091016215750  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091016215750  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091016215751  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091016215751  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091016215751  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091016215751  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091016215751  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091016220443                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20091016171344  CV  LAT$            G�O�G�O�A�/                JM  ARGQJMQC1.0                                                                 20091016171344  CV  LON$            G�O�G�O�CC                JM  ARCAJMQC1.0                                                                 20091026075733  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20091026075733  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20091026092321  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219014801  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219014843                      G�O�G�O�G�O�                