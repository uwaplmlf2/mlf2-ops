CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901532 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               XA   JA  20090804035819  20100219014835  A9_66114_088                    2C  D   APEX-SBE 2823                                                   846 @�A9@E�1   @�A;��s@4'�z�H@b�bM��1   ARGOS   A   A   A   @�  A  AfffA�ffA�33A�33B
  BffB2  BF  BXffBn  B�  B�  B���B�  B���B���B�  B�33B�  B�33B�33B�ffB���C� CL�CL�CffCL�C�3C��C$��C)�C.� C333C7�fC=L�CB33CG� CQ� C[33Ce  Co  Cy� C�� C��3C��3C�� C��3C��fC��3C�� C���C��3C���C���C�� C�Cǳ3C̀ CѦfC�ffCۙ�C�ffC�ffC�3C�3C��fC���D��D�3D�fDٚD� D�3D� D$�3D)� D.�3D3��D8��D=� DB��DGٚDL� DQ�fDV��D[��D`�fDe� Dj��Do�3Dt�3Dy�3D�,�D�i�D���D�� D�#3D�l�D��3D��fD�  D�p D�� D��D��D�i�Dڬ�D��3D��D�\�D�3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA33AY��A�  A���A���B��B33B.��BB��BU33Bj��B~��B�ffB�  B�ffB�33B�  B�ffBę�B�ffBؙ�B♚B���B�33C �3C� C
� C��C� C�fC��C#��C(L�C-�3C2ffC7�C<� CAffCF�3CP�3CZffCd33Cn33Cx�3C��C�L�C�L�C�Y�C�L�C�@ C�L�C��C�33C�L�C�ffC�&fC�Y�C�33C�L�C��C�@ C�  C�33C�  C�  C�L�C�L�C�@ C�&fD�fD� D�3D�fD��D� D��D$� D)��D.� D3��D8��D=��DB��DG�fDL��DQ�3DV��D[��D`�3De��Dj��Do� Dt� Dy� D�3D�P D��3D��fD�	�D�S3D���D���D�fD�VfD��fD�� D�3D�P Dړ3D�ɚD�3D�C3D�D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�jA�wA�wA���A�9A��A蛦A��A�v�A�33A��A���A���A�%A�=qA�JA��A���A�M�A�A²-A���A�I�A�G�A�{A��;A�"�A�jA�r�A��A��!A�`BA�S�A���A�9XA��A���A��A���A�JA��RA��!A��\A��9A��wA�  A�bA��7A��;A���A���A�ȴA}33Aw��Asl�Al��AdȴA_��AXbAQ��AL�AC�^A@n�A<=qA7��A2�A.��A)�A#;dA9XA�AQ�A�@���@�V@���@��T@��@���@�$�@���@�^5@�E�@�J@�x�@���@��+@��/@��u@�/@���@�x�@��@�C�@�$�@~E�@o\)@c��@\j@PbN@H �@B�!@;��@5@0A�@'�@"�\@{@��@�h@�@5?@	�7@�@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�jA�wA�wA���A�9A��A蛦A��A�v�A�33A��A���A���A�%A�=qA�JA��A���A�M�A�A²-A���A�I�A�G�A�{A��;A�"�A�jA�r�A��A��!A�`BA�S�A���A�9XA��A���A��A���A�JA��RA��!A��\A��9A��wA�  A�bA��7A��;A���A���A�ȴA}33Aw��Asl�Al��AdȴA_��AXbAQ��AL�AC�^A@n�A<=qA7��A2�A.��A)�A#;dA9XA�AQ�A�@���@�V@���@��T@��@���@�$�@���@�^5@�E�@�J@�x�@���@��+@��/@��u@�/@���@�x�@��@�C�@�$�@~E�@o\)@c��@\j@PbN@H �@B�!@;��@5@0A�@'�@"�\@{@��@�h@�@5?@	�7@�@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB^5B]/B^5B^5B^5B^5B\)B_;BjBR�B5?B)�B"�BDB��B�uB�\B�=B�B�B�Bz�Bz�B|�Bq�B_;B^5BE�BC�B=qB49B'�BbBBJBPBVB
=B
=B2-BP�BA�B,B!�B\B	7B
��B
�sB
�B
ȴB
�wB
�B
�VB
u�B
\)B
6FB
PB	�B	ǮB	��B	�JB	jB	XB	E�B	33B	�B	bB��B�ZB��B�-B��B�%Bv�BjBiyB�B��B�-B��B�BB�B	JB	(�B	J�B	jB	� B	�%B	��B	�B	�jB	ĜB	��B	��B	�HB	��B
%B
{B
�B
"�B
.B
33B
:^B
@�B
F�B
N�B
S�B
VB
[#B
cTB
gmB
l�B
q�B
t�B
w�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B^5B]/B^5B^5B^5B^5B]/B`BBq�BXB7LB+B$�BbB��B�{B�bB�DB�B�B�B|�B{�B�Br�BaHBbNBH�BE�B>wB5?B+B{BBJBPBVBJBDB33BQ�BC�B-B"�B\B
=B
��B
�yB
�B
ɺB
�}B
�'B
�\B
v�B
^5B
8RB
VB	�B	ɺB	�B	�VB	k�B	YB	F�B	49B	 �B	hB��B�`B��B�9B��B�+Bw�Bk�BjB�B��B�-B��B�BB�B	PB	(�B	J�B	jB	� B	�%B	��B	�B	�jB	ĜB	��B	��B	�HB	��B
%B
{B
�B
"�B
.B
33B
:^B
@�B
F�B
N�B
S�B
VB
[#B
cTB
gmB
l�B
q�B
t�B
w�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.8(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200908170801142009081708011420090817080114200908170913182009081709131820090817091318201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090804035818  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090804035819  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090804035820  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090804035820  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090804035820  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090804035822  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090804035822  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090804035822  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090804035822  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090804035822  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090804040802                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090807215556  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090807215844  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090807215845  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090807215845  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090807215845  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090807215846  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090807215846  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090807215846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090807215846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090807215846  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090807220335                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090909004927  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090909004927  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090909004929  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090909004929  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090909004929  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090909004929  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090909004929  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090909010832                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090807171125  CV  DAT$            G�O�G�O�F�	�                JM  ARCAJMQC1.0                                                                 20090817080114  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090817080114  CV  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090817091318  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219014806  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219014835                      G�O�G�O�G�O�                