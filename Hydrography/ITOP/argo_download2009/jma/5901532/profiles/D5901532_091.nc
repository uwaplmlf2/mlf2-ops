CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901532 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               [A   JA  20090903095500  20100219014837  A9_66114_091                    2C  D   APEX-SBE 2823                                                   846 @�H�
m�!1   @�H���Q�@4
��n�@btZ�11   ARGOS   A   A   A   @�  AffAfffA�33A�ffA�ffB	��BffB2ffBG33BZffBm��B���B�33B���B�  B�  B�ffB�33B�  B�33B�33B�33B���B�33C33CffC�3C��C�3C� C33C$ffC)� C.33C3  C8�C=33CBffCG  CQL�C[L�Ce� Co� Cy33C��fC�s3C���C�s3C�� C��fC�ٚC���C��fC��3C���C��fC��3C�Cǳ3C̳3CѦfC�s3Cۀ C�� C�3C�3C�fC��3C�� D�fD�fD�3D�fD��D��D� D$��D)ٚD.� D3� D8�fD=��DB�3DG� DL�3DQ� DV��D[�3D`�3De�3Dj�3DoٚDtٚDy�fD�0 D�p D���D�� D�)�D�p D���D��fD��D�l�D���D��fD�)�D�` Dڠ D���D�#3D�S3D� D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  AffA^ffA�33A�ffA�ffB��BffB0ffBE33BXffBk��B���B�33B���B�  B�  B�ffB�33B�  B�33B�33B�33B���B�33C �3C�fC33C�C33C  C�3C#�fC)  C-�3C2� C7��C<�3CA�fCF� CP��CZ��Ce  Co  Cx�3C�ffC�33C�Y�C�33C�@ C�ffC���C���C�ffC�s3C�L�C�ffC�s3C�L�C�s3C�s3C�ffC�33C�@ C�� C�s3C�s3C�ffC�s3C�� D�fD�fD�3D�fD��D��D� D$��D)��D.� D3� D8�fD=��DB�3DG� DL�3DQ� DV��D[�3D`�3De�3Dj�3Do��Dt��Dy�fD�  D�` D���D�� D��D�` D���D��fD��D�\�D���D��fD��D�P Dڐ D���D�3D�C3D� D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��#A��A���A���A���A���A�JA�A��`A�A�$�A�Q�A�K�A��A�`BA� �A��
AɸRA�VA��TA�Að!A�C�A�=qA��A�\)A�^5A�O�A���A���A��/A�G�A���A�A�"�A��
A�/A�1A�O�A�=qA�ffA�ƨA�/A��A��#A�ȴA�9XA��FA�M�A�  A�
=A|ȴAs�Am��AkVAe�hAaC�A^ �AX�AL��ADJA>��A6-A,��A'��A"^5A  A��A�
A
~�AE�@�|�@�ƨ@�\@պ^@���@�1'@�%@�S�@���@���@��F@��@���@�33@�E�@���@�`B@�\)@�1'@��R@���@�9X@�^5@� �@t�@jn�@_�@U`B@G�P@=��@:=q@5/@.@';d@"�\@ff@Ĝ@�D@r�@Z@�;@�H@G�?��h1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��#A��A���A���A���A���A�JA�A��`A�A�$�A�Q�A�K�A��A�`BA� �A��
AɸRA�VA��TA�Að!A�C�A�=qA��A�\)A�^5A�O�A���A���A��/A�G�A���A�A�"�A��
A�/A�1A�O�A�=qA�ffA�ƨA�/A��A��#A�ȴA�9XA��FA�M�A�  A�
=A|ȴAs�Am��AkVAe�hAaC�A^ �AX�AL��ADJA>��A6-A,��A'��A"^5A  A��A�
A
~�AE�@�|�@�ƨ@�\@պ^@���@�1'@�%@�S�@���@���@��F@��@���@�33@�E�@���@�`B@�\)@�1'@��R@���@�9X@�^5@� �@t�@jn�@_�@U`B@G�P@=��@:=q@5/@.@';d@"�\@ff@Ĝ@�D@r�@Z@�;@�H@G�?��h1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB)�B.B1'B1'B2-B33B8RB^5B�sB�fB��B�;B�B�B��B��B��B�
B�ZB�B��B��BÖB�dB��B�?B�RB�9B��B��B�bB�%B~�Bu�Bn�BgmB^5BXBO�BI�B<jB0!B'�B�BB
��B
�B
�HB
��B
�wB
��B
�DB
jB
M�B
6FB
�B

=B	��B	�B	�{B	l�B	S�B	.B	PB��B�ZB��B�wB��B�hB�Bw�Br�Bn�Br�Bx�B�B��B�FB��B��B��B	�B	6FB	L�B	e`B	x�B	�B	�DB	��B	�B	ÖB	��B	�;B	�sB	��B
	7B
bB
�B
"�B
,B
49B
<jB
C�B
B�B
N�B
T�B
W
B
\)B
dZB
iyB
p�B
t�B
x�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B)�B.B1'B1'B2-B33B8RB_;B�B�yB�#B�TB�B�B��B��B��B�B�`B�#B��B��BȴB�qB��B�LB�^B�LB��B��B�oB�1B�Bw�Bo�BjB_;BZBP�BJ�B=qB1'B(�B�B%B
��B
�B
�NB
��B
�}B
�B
�PB
l�B
N�B
7LB
�B
DB	��B	�/B	��B	m�B	VB	0!B	VB��B�fB��B��B�B�oB�Bx�Bs�Bo�Bs�By�B�B��B�FB��B�B��B	�B	6FB	L�B	e`B	x�B	�B	�DB	��B	�B	ÖB	��B	�;B	�sB	��B
	7B
bB
�B
"�B
,B
49B
<jB
C�B
B�B
N�B
T�B
W
B
\)B
dZB
iyB
p�B
t�B
x�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.5(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909160732372009091607323720090916073237200909160746122009091607461220090916074612201002130000002010021300000020100213000000  JA  ARFMdecpA9_b                                                                20090903095459  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090903095500  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090903095501  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090903095501  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090903095501  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090903095502  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090903095502  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090903095502  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090903095502  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090903095502  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090903100213                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090906215536  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090906215824  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090906215825  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090906215825  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090906215825  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090906215826  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090906215826  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090906215826  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090906215826  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090906215827  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090906220429                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090909004932  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090909004932  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090909004933  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090909004933  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090909004933  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090909004933  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090909004933  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090909010757                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090906164334  CV  DAT$            G�O�G�O�F�E�                JM  ARCAJMQC1.0                                                                 20090916073237  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090916073237  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090916074612  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100213000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100219014758  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100219014837                      G�O�G�O�G�O�                