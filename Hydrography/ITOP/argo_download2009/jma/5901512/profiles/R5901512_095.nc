CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901512 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               _A   JA  20090830035704  20090902190210                                  2B  A   APEX-SBE 2805                                                   846 @�G��ò�1   @�G��Q�@2��Q�@`��hr�1   ARGOS   A   A   A   @���A��Ac33A�33A���A�  B	��B��B333BE��BZ  Bm33B�  B���B�ffB���B���B�  B�  B���B�ffB�  B���B�33B�  CffC33CL�C  C� CL�CL�C$ffC)  C.ffC3� C8ffC=33CB33CG  CQ33C[  Ce  Co��CyffC��3C���C���C�� C�� C���C��fC���C��3C��3C��fC���C��3C Cǳ3Č�CѦfC֙�CۦfC�� C��C�fC�Y�C�ffC���D��D��D��D� D� D�fD��D$ٚD)ٚD.� D3��D8�fD=ٚDB� DGٚDL� DQ�3DV�fD[�3D`�3De� DjٚDo�fDt�3Dy��D��D�p D��3D��D��D�` D��3D�� D�,�D�ffD�� D�ٚD�)�D�ffDڦfD�� D�#3D�` D� D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��Ac33A�33A���A�  B	��B��B333BE��BZ  Bm33B�  B���B�ffB���B���B�  B�  B���B�ffB�  B���B�33B�  CffC33CL�C  C� CL�CL�C$ffC)  C.ffC3� C8ffC=33CB33CG  CQ33C[  Ce  Co��CyffC��3C���C���C�� C�� C���C��fC���C��3C��3C��fC���C��3C Cǳ3Č�CѦfC֙�CۦfC�� C��C�fC�Y�C�ffC���D��D��D��D� D� D�fD��D$ٚD)ٚD.� D3��D8�fD=ٚDB� DGٚDL� DQ�3DV�fD[�3D`�3De� DjٚDo�fDt�3Dy��D��D�p D��3D��D��D�` D��3D�� D�,�D�ffD�� D�ٚD�)�D�ffDڦfD�� D�#3D�` D� D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ffA�l�A�dZA�K�A�1'A���A���A��A���A��mAؾwAׅA��`A�{A�p�A͋DA���A�A�l�A��#A��A�"�A��FA���A�VA�ĜA��RA�9XA��A�5?A�x�A��#A�/A�n�A��;A��-A�hsA��A�jA�-A�VA��7A�Q�A�&�A��hA�hsA��A�?}A��
A���A��A�Ax^5ArM�Al��Af�DAa�7AZjAU
=AN�AG�FABz�A=?}A9�wA4��A0r�A-�A'33A!�^Az�A��A
-A�`@��@�Z@�$�@ͩ�@�^5@�@�=q@��h@�9X@�^5@���@�t�@��@�+@���@�Ĝ@��w@���@�/@�(�@�-@��D@t��@g��@\Z@Tz�@N@IX@B=q@;dZ@5/@/K�@+dZ@&{@!x�@�j@�@"�@��@`B@
��@`B1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�ffA�l�A�dZA�K�A�1'A���A���A��A���A��mAؾwAׅA��`A�{A�p�A͋DA���A�A�l�A��#A��A�"�A��FA���A�VA�ĜA��RA�9XA��A�5?A�x�A��#A�/A�n�A��;A��-A�hsA��A�jA�-A�VA��7A�Q�A�&�A��hA�hsA��A�?}A��
A���A��A�Ax^5ArM�Al��Af�DAa�7AZjAU
=AN�AG�FABz�A=?}A9�wA4��A0r�A-�A'33A!�^Az�A��A
-A�`@��@�Z@�$�@ͩ�@�^5@�@�=q@��h@�9X@�^5@���@�t�@��@�+@���@�Ĝ@��w@���@�/@�(�@�-@��D@t��@g��@\Z@Tz�@N@IX@B=q@;dZ@5/@/K�@+dZ@&{@!x�@�j@�@"�@��@`B@
��@`B1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B'�B'�B$�B$�B$�B#�B<jB�+B	�+B
$�B
�!B
��B
ǮB
�B
��B
��B
��B
��B
�fB
�
B
�B
��BhBXB[#BZBl�B�B�B�7Bp�B�JBe`BQ�BcTB�B�Bs�Bo�Bk�B]/BN�BE�B;dB/B&�B�BVB
��B
�fB
ŢB
�'B
|�B
aHB
D�B
'�B
DB	�yB	��B	�FB	��B	� B	iyB	^5B	J�B	=qB	49B	�B	JB��B��B�BB�}B��B�LB��B�
B�TB��B	DB	'�B	A�B	T�B	jB	�+B	�uB	��B	�!B	�!B	�dB	��B	�B	�mB	�B	��B
�B
"�B
,B
49B
<jB
C�B
K�B
R�B
YB
^5B
cTB
hsB
m�B
r�B
w�B
z�B
~�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B'�B'�B$�B$�B$�B#�B<jB�+B	�+B
$�B
�!B
��B
ǮB
�B
��B
��B
��B
��B
�fB
�
B
�B
��BhBXB[#BZBl�B�B�B�7Bp�B�JBe`BQ�BcTB�B�Bs�Bo�Bk�B]/BN�BE�B;dB/B&�B�BVB
��B
�fB
ŢB
�'B
|�B
aHB
D�B
'�B
DB	�yB	��B	�FB	��B	� B	iyB	^5B	J�B	=qB	49B	�B	JB��B��B�BB�}B��B�LB��B�
B�TB��B	DB	'�B	A�B	T�B	jB	�+B	�uB	��B	�!B	�!B	�dB	��B	�B	�mB	�B	��B
�B
"�B
,B
49B
<jB
C�B
K�B
R�B
YB
^5B
cTB
hsB
m�B
r�B
w�B
z�B
~�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090830035702  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090830035704  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090830035704  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090830035704  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090830035705  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090830035705  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090830035706  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090830035706  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090830035706  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090830040401                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090902185517  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090902185756  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090902185757  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090902185757  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090902185758  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090902185758  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090902185758  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090902185758  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090902185758  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090902190210                      G�O�G�O�G�O�                