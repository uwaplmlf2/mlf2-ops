CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900965 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090817125803  20090821070400                                  2B  A   APEX-SBE 4143                                                   846 @�D��m�1   @�D�HpB�@<���R@b2M���1   ARGOS   A   A   A   @�  A$��Aq��A���A�  A�  BffB!33B6ffBH��B^��BrffB�33B���B�  B�ffB�33B�ffB���B���B�ffB���B�33B�  B���C� C33C��C� CffCffC�fC%  C*33C.��C4L�C9�C>  CC�CE�CR�C\ffCf� Co�fCz  C�@ C��fC�@ C��C��3C��fC��3C�  C��3C��C�L�C��C�  C�&fC�ٚC�@ C��3C��C�  C��3C�&fC�L�C�@ C�33C�33D�3D�3D�fD3D�D  D��D%�D*�D/�D3ٚD9&fD>  DC3DG�fDM3DR  DWfD\�Da�Df3Dj�fDp3Dt�3Dy�3D�P D�|�D���D���D�FfD�vfD���D��fD�<�D�|�D��fD�fD�9�D�l�D�� D�3D�0 D�fD� D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A)��AvfgA�33A�ffA�ffB��B"ffB7��BJ  B`  Bs��B���B�fgB���B�  B���B�  B�34B�fgB�  B�fgB���B홚B�34C��C� C�gC��C�3C�3C 33C%L�C*� C/�C4��C9fgC>L�CCfgCEfgCRfgC\�3Cf��Cp33CzL�C�ffC��C�ffC�@ C��C��C��C�&fC��C�@ C�s3C�@ C�&fC�L�C�  C�ffC��C�@ C�&fC��C�L�C�s3C�ffC�Y�C�Y�DfD�fD��D&fD  D3D �D%  D*,�D/,�D3��D99�D>3DC&fDG��DM&fDR3DW�D\  Da,�Df&fDj��Dp&fDufDzfD�Y�D��gD��gD�4D�P D�� D��gD�  D�FgD��gD�� D� D�C4D�vgD�ɚD��D�9�D� D�D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�z�A蕁A�ƨA�uA���A�E�A��A��`A�r�A��FA�jA��HA��uA���A�p�A�1'A��A�t�A���A�G�A��9A�$�A���A��FA���A���A��hA�A�I�A�&�A�A�A�VA��
A�VA��RA�;dA��A��A�+A�&�A��!A�`BA��yA��A�  A�M�A���A��wA�A���A�r�A��jA~1A|��Ax��AsdZAq��An��Ak�mAh�`Af��Ad�jA^  ATn�AM|�AG�wAD��A>$�A9"�A5��A*ffA#t�AG�AƨAz�A�!@��#@�`B@�V@�5?@�ff@�V@��@�5?@��@�  @��u@�J@��R@���@�j@�Q�@��j@��@y%@k��@ct�@Y7L@PbN@H�u@A�@8A�@1��@+ƨ@%�h@��@��@�@ �@��@�`@v�@�\?���?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�z�A蕁A�ƨA�uA���A�E�A��A��`A�r�A��FA�jA��HA��uA���A�p�A�1'A��A�t�A���A�G�A��9A�$�A���A��FA���A���A��hA�A�I�A�&�A�A�A�VA��
A�VA��RA�;dA��A��A�+A�&�A��!A�`BA��yA��A�  A�M�A���A��wA�A���A�r�A��jA~1A|��Ax��AsdZAq��An��Ak�mAh�`Af��Ad�jA^  ATn�AM|�AG�wAD��A>$�A9"�A5��A*ffA#t�AG�AƨAz�A�!@��#@�`B@�V@�5?@�ff@�V@��@�5?@��@�  @��u@�J@��R@���@�j@�Q�@��j@��@y%@k��@ct�@Y7L@PbN@H�u@A�@8A�@1��@+ƨ@%�h@��@��@�@ �@��@�`@v�@�\?���?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�%B
�B
�1B
�=B
}�B
iyB
v�B
�B
�+B
�VB
��B
��B
��B
��B
��B
�)B
�B
��B
��B
�B
�sB
�`B
�#B
�B
��B
�/B
�/B
��B
�
B
�B
�fB
�NB
�`B
�;B
�;B
�B
�B
�B
�B
��B
��BPB
=B+BB
��B
�B
�ZB
�)B
��B
�jB
�B
��B
��B
�B
m�B
e`B
T�B
I�B
<jB
33B
(�B
B	�
B	�LB	��B	��B	n�B	[#B	H�B	�B	B�mB��B��B�oBk�B\)BS�BP�BT�B_;BffBq�B�B��B��B�3BÖB��B�)B�B	+B	�B	1'B	W
B	hsB	�%B	��B	�9B	ɺB	�)B	�B	��B
	7B
�B
!�B
1'B
<jB
D�B
M�B
S�B
\)B
cTB
iy1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�%B
�B
�1B
�=B
}�B
iyB
v�B
�B
�+B
�VB
��B
��B
��B
��B
��B
�)B
�B
��B
��B
�B
�sB
�`B
�#B
�B
��B
�/B
�/B
��B
�
B
�B
�fB
�NB
�`B
�;B
�;B
�B
�B
�B
�B
��B
��BPB
=B+BB
��B
�B
�ZB
�)B
��B
�jB
�B
��B
��B
�B
m�B
e`B
T�B
I�B
<jB
33B
(�B
B	�
B	�LB	��B	��B	n�B	[#B	H�B	�B	B�mB��B��B�oBk�B\)BS�BP�BT�B_;BffBq�B�B��B��B�3BÖB��B�)B�B	+B	�B	1'B	W
B	hsB	�%B	��B	�9B	ɺB	�)B	�B	��B
	7B
�B
!�B
1'B
<jB
D�B
M�B
S�B
\)B
cTB
iy1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090817125802  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090817125803  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090817125804  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090817125804  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090817125805  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090817125805  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090817125805  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090817125805  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090817125806  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090817130505                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090821065923  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090821070008  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090821070008  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090821070009  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090821070010  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090821070010  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090821070010  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090821070010  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090821070010  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090821070400                      G�O�G�O�G�O�                