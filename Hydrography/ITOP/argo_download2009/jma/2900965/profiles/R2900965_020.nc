CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  2900965 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090827125632  20090831071513                                  2B  A   APEX-SBE 4143                                                   846 @�G
 a#1   @�G
��r�@<�
=p��@b=O�;dZ1   ARGOS   A   A   A   @���A��Ax  A�33A�33A�33B  B  B6  BJffB\ffBs33B���B�  B�33B�ffB�ffB�  B�  Bș�B���B�  B�  B���B�  CffCffC33C�C�C�fC �C$��C*L�C/L�C4  C8��C>L�CC  CG�3CR� C\ffCfffCp� CzL�C�  C�ٚC��C��3C�&fC��C���C��fC��3C�&fC�&fC�L�C�&fC��C��C��C�&fC��3C��fC�33C�33C���C��C��C�&fD  D�D  D��D��D&fD �D%  D*  D/3D3�fD8��D=�3DB�3DH&fDM�DR�DV� D\�Da�Df�Dk3Dp�Dt� Dy�3D�C3D�� D���D��3D�S3D��fD��fD�fD�C3D���D���D� D�9�DԀ D�� D�  D�I�D�|�D�ɚD���D�` 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�34AfgA|��A���A͙�A���B33B33B733BK��B]��BtffB�34B���B���B�  B�  B���B���B�34B�fgBܙ�B癚B�fgB���C�3C�3C� CfgCfgC33C fgC$�gC*��C/��C4L�C8�gC>��CCL�CH  CR��C\�3Cf�3Cp��Cz��C�&fC�  C�@ C��C�L�C�@ C��3C��C��C�L�C�L�C�s3C�L�C�33C�33C�33C�L�C��C��C�Y�C�Y�C��3C�33C�33C�L�D3D,�D33D�D  D9�D ,�D%3D*33D/&fD3��D9�D>fDB�fDH9�DM,�DR,�DV�3D\,�Da  Df,�Dk&fDp,�Dt�3DzfD�L�D���D��gD���D�\�D�� D�� D� D�L�D��gD��gD��D�C4Dԉ�D�ٚD�	�D�S4D�gD��4D��gD�i�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�  A�hA�A�=qA�(�A�+A�t�Aח�A�z�A�G�A�O�A��yA��PA��FA�%A�r�A��#A��A��yA�33A��A�9XA��wA�bA��A�Q�A�~�A�XA��A��mA�z�A���A�ffA�O�A�$�A��9A���A���A��A�A�A��9A���A�Q�A��A��A���A��A�"�A�9XA�=qA�%A���A���A�\)A��A|�9Av{At �AqXAoAk�mAgx�A_�;A[��AWG�AR1'AI�^AF�uA@��A?�A7�^A/�A'��A�wA�A��AV@��@���@��@���@��@�-@�-@�hs@�~�@�$�@�{@�
=@��@�ȴ@���@���@���@|��@r-@e`B@Z��@O+@G�@@A�@5O�@/+@( �@$�@�@�
@ff@�#@V@
�!@�@z�@��?�(�?��m11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�  A�hA�A�=qA�(�A�+A�t�Aח�A�z�A�G�A�O�A��yA��PA��FA�%A�r�A��#A��A��yA�33A��A�9XA��wA�bA��A�Q�A�~�A�XA��A��mA�z�A���A�ffA�O�A�$�A��9A���A���A��A�A�A��9A���A�Q�A��A��A���A��A�"�A�9XA�=qA�%A���A���A�\)A��A|�9Av{At �AqXAoAk�mAgx�A_�;A[��AWG�AR1'AI�^AF�uA@��A?�A7�^A/�A'��A�wA�A��AV@��@���@��@���@��@�-@�-@�hs@�~�@�$�@�{@�
=@��@�ȴ@���@���@���@|��@r-@e`B@Z��@O+@G�@@A�@5O�@/+@( �@$�@�@�
@ff@�#@V@
�!@�@z�@��?�(�?��m11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�BB
��B
��BB
��B%B�B�BuBB
��B
��BbB�BK�B\B\BB
�B
�B
�HB
��B
��B
B
�qB
ŢB
��B
��B
��B
�;B
�5B
�B
��B
ɺB
��B
��B
��B�B�B{B�B�B�BoB\BJB1B
��B
��B
�B
�ZB
��B
ǮB
�LB
��B
�\B
u�B
l�B
aHB
YB
G�B
33B
{B
B	�B	��B	��B	�oB	{�B	t�B	R�B	33B	bB�B�;B�!B�PBt�Be`BYBR�BVB[#BdZBr�B�%B��B��B�!BĜB�B��B	B	VB	%�B	J�B	e`B	� B	��B	�3B	ƨB	�)B	�B
B
JB
�B
 �B
)�B
6FB
@�B
I�B
P�B
YB
_;B
gmB
iy11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111BB
��B
��BB
��B%B�B�BuBB
��B
��BbB�BK�B\B\BB
�B
�B
�HB
��B
��B
B
�qB
ŢB
��B
��B
��B
�;B
�5B
�B
��B
ɺB
��B
��B
��B�B�B{B�B�B�BoB\BJB1B
��B
��B
�B
�ZB
��B
ǮB
�LB
��B
�\B
u�B
l�B
aHB
YB
G�B
33B
{B
B	�B	��B	��B	�oB	{�B	t�B	R�B	33B	bB�B�;B�!B�PBt�Be`BYBR�BVB[#BdZBr�B�%B��B��B�!BĜB�B��B	B	VB	%�B	J�B	e`B	� B	��B	�3B	ƨB	�)B	�B
B
JB
�B
 �B
)�B
6FB
@�B
I�B
P�B
YB
_;B
gmB
iy11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090827125631  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090827125632  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090827125632  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090827125633  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090827125634  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090827125634  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090827125634  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090827125634  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090827125634  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090827130026                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090831070740  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090831071009  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090831071009  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090831071010  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090831071011  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090831071011  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090831071011  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090831071011  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090831071012  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090831071513                      G�O�G�O�G�O�                