CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   k   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4H   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6`   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8x   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :$   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <<   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >T   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  @    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @l   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  B   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D0   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D`   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G`   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J`   CALIBRATION_DATE            	             
_FillValue                  ,  M`   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    M�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    M�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    M�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    M�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  M�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N    HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    NArgo profile    2.2 1.2 19500101000000  5901560 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               >A   JA  20091013035702  20091208012259                                  2B  A   APEX-SBE 3468                                                   846 @�Rd��,1   @�Rf���@@26ȴ9X@^�$�/�1   ARGOS   B   B   B   @�ffA  A`  A�  A�  A���B
  B  B1��BD��BY��Bl��B�ffB�ffB���B�33B���B���B�  B�33B���B�ffB�ffBB�ffCL�C33CffC33CL�C��C� C$� C)L�G�O�CGffCQ�CZ�fCe33Co�CyffC��3C��fC��3C�s3C�� C�s3C�ٚC�� C���C��fC���C��fC���C³3Cǳ3C̀ Cь�Cր C۳3C���C�fC�3C�� C���C�� D��D��DٚD� D��DٚDٚD$�fD)�3D.� D3ٚD8ٚD=�fDB��DG��DL� DQ� Do�3Dt��Dy� D�)�D�i�D���D��3D�#3D�i�D���D��fD��D�l�D���D��fD�  D�Y�Dڣ3D��3D�#3D�i�D� D��3D�<�11111111111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA  A`  A�  A�  A���B
  B  B1��BD��BY��Bl��B�ffB�ffB���B�33B���B���B�  B�33B���B�ffB�ffBB�ffCL�C33CffC33CL�C��C� C$� C)L�G�O�CGffCQ�CZ�fCe33Co�CyffC��3C��fC��3C�s3C�� C�s3C�ٚC�� C���C��fC���C��fC���C³3Cǳ3C̀ Cь�Cր C۳3C���C�fC�3C�� C���C�� D��D��DٚD� D��DٚDٚD$�fD)�3D.� D3ٚD8ٚD=�fDB��DG��DL� DQ� Do�3Dt��Dy� D�)�D�i�D���D��3D�#3D�i�D���D��fD��D�l�D���D��fD�  D�Y�Dڣ3D��3D�#3D�i�D� D��3D�<�11111111111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ƨA�p�A�C�A�VA㙚A�;dA�5?A��HA�7A�x�A���A�oA�5?A�?}A��AדuA��AɼjA�5?AÑhA��A��7A��A�oA��A��HA��A�5?A�jA�{A�JA�p�A�x�G�O�A�|�A��A��FA�ƨA���A�VA���A���A��A�oA~bAyAp�9Ag?}A]t�AZ�\AWS�ATJAKdZAC�-A<-A2��A.�A'�;A$�DA!+A�A33A%Al�AK�A=q@�&�@�bN@�@�j@�!@��`@�h@� �@��
@�p�@�hs@�@��m@���@���@���@���@�@�o@��^@�Ĝ@y��@h �@_+@W��@M�@HĜ@B��@<��@:-@4��@.ff@)��@&E�@ Ĝ@?}@Q�@�^@o@p�@"�11111111111111111111111111111111194111111111111111111111111111111111111111111111111111111111111111111111111 A�ƨA�p�A�C�A�VA㙚A�;dA�5?A��HA�7A�x�A���A�oA�5?A�?}A��AדuA��AɼjA�5?AÑhA��A��7A��A�oA��A��HA��A�5?A�jA�{A�JA�p�A�x�G�O�A�|�A��A��FA�ƨA���A�VA���A���A��A�oA~bAyAp�9Ag?}A]t�AZ�\AWS�ATJAKdZAC�-A<-A2��A.�A'�;A$�DA!+A�A33A%Al�AK�A=q@�&�@�bN@�@�j@�!@��`@�h@� �@��
@�p�@�hs@�@��m@���@���@���@���@�@�o@��^@�Ĝ@y��@h �@_+@W��@M�@HĜ@B��@<��@:-@4��@.ff@)��@&E�@ Ĝ@?}@Q�@�^@o@p�@"�11111111111111111111111111111111194111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B	�B	��B	�B	�B	�B	��B	��B	�dB	�B	��B
B
S�B
~�BJB1'B  B
�B,B�B�B�B�B�B'�B:^BD�B[#Bl�BiyBcTBl�Bt�G�O�B_;BT�BI�B<jB&�B�B
��B
ɺB
��B
v�B
bNB
:^B
VB	�B	�B	��B	�qB	��B	|�B	]/B	7LB	&�B	�B	�B	�B	�B	DB	  B��B��B��B�`B	�B	�B	/B	-B	 �B	'�B	-B	(�B	5?B	F�B	_;B	_;B	y�B	��B	�3B	��B	��B	��B	��B	�mB	�B
%B
oB
&�B
0!B
49B
:^B
D�B
G�B
K�B
T�B
YB
\)B
aHB
ffB
k�B
p�B
w�B
~�B
�11111111111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	��B	�B	��B	�B	�B	�B	��B	��B	�dB	�B	��B
B
S�B
~�BJB1'B  B
�B,B�B�B�B�B�B'�B:^BD�B[#Bl�BiyBcTBl�Bt�G�O�B_;BT�BI�B<jB&�B�B
��B
ɺB
��B
v�B
bNB
:^B
VB	�B	�B	��B	�qB	��B	|�B	]/B	7LB	&�B	�B	�B	�B	�B	DB	  B��B��B��B�`B	�B	�B	/B	-B	 �B	'�B	-B	(�B	5?B	F�B	_;B	_;B	y�B	��B	�3B	��B	��B	��B	��B	�mB	�B
%B
oB
&�B
0!B
49B
:^B
D�B
G�B
K�B
T�B
YB
\)B
aHB
ffB
k�B
p�B
w�B
~�B
�11111111111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091013035701  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091013035702  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091013035703  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091013035703  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091013035704  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091013035704  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091013035704  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091013035704  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091013035704  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091013035704  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091013035704  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091013040530                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091015155542  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091015155658  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091015155658  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091015155659  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091015155700  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091015155700  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091015155700  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091015155700  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091015155700  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091015155700  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091015155700  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091015160030                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207081346  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207081624  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207081624  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207081625  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207081626  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207081626  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207081626  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207081626  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207081626  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207081626  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207081627  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208012259                      G�O�G�O�G�O�                