CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   l   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4L   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6h   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :4   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <P   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >l   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  @   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  B8   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  DT   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J�   CALIBRATION_DATE            	             
_FillValue                  ,  M�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    M�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    M�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    M�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    M�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  M�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N    HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N$   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N(   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N,   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N0Argo profile    2.2 1.2 19500101000000  5901560 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               7A   JA  20090804065719  20091208012320                                  2B  A   APEX-SBE 3468                                                   846 @�@�!_�1   @�@��>@0
~��"�@_3"��`B1   ARGOS   B   B   B   @���AffA`  A�  A���A�ffB
  B��B133BDffBY33Bm��B���B�33B���B���B�ffB�  B�  B���B�ffBڙ�B���B�33B�33CffCL�CffC�CL�C�fCffC$  C)��C.ffC3� C8ffC=��CBL�CGffCQ  CZ��Ce33Co  Cy33C���C�� C�� C���C�ffC���C��3C��3C�� C��fC���C��fC�� C�Cǳ3Č�C�� C�ٚC۳3C���C��C�fC��C��3C��3D�3D��D��D� D��D�3D��D$��DA�DBٚDG��DL�3DQٚDV�3D[� D`ٚDe��Dj�3G�O�D���D��D�,�D�i�D�� D��fD�,�D�i�D�� D��fD��D�p Dک�D��D�)�D�l�D�fD�ٚD���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111141111111111111111111@���AffA`  A�  A���A�ffB
  B��B133BDffBY33Bm��B���B�33B���B���B�ffB�  B�  B���B�ffBڙ�B���B�33B�33CffCL�CffC�CL�C�fCffC$  C)��C.ffC3� C8ffC=��CBL�CGffCQ  CZ��Ce33Co  Cy33C���C�� C�� C���C�ffC���C��3C��3C�� C��fC���C��fC�� C�Cǳ3Č�C�� C�ٚC۳3C���C��C�fC��C��3C��3D�3D��D��D� D��D�3D��D$��DA�DBٚDG��DL�3DQٚDV�3D[� D`ٚDe��Dj�3G�O�D���D��D�,�D�i�D�� D��fD�,�D�i�D�� D��fD��D�p Dک�D��D�)�D�l�D�fD�ٚD���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111141111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�?}A�E�A�-A�/A�VA���A藍A�-A�A�wAߙ�Aݗ�A�p�A�l�Aֲ-A��#A�p�A�G�A�VA��A��A��A�K�A�ffA��A�JA���A�E�A�1'A��A�`BA�XA�C�A��RA�1'A�z�A�C�A�n�A�VA�?}A��DA��A�1'A�-A�5?A�ƨA���A��A�bA|��Ay�PAw��An�\Am�wAhZAb�A^ffAV�/ALz�ACG�A;C�A6Q�A2ĜA, �A&�HA z�A%A��A�`A  @�z�@�@��@噚@���@�~�@ָRG�O�@�  @�;d@���@��m@�p�@�S�@�`B@�/@��G�O�@d�@W|�@O�P@J=q@EO�@?l�@9��@3dZ@,�@(�9@$9X@"�\@ b@��@�;@J@p�@1'@O�@9X111111111111111111111111111111111111111111111111111111111111111111111111111119111111111941111111111111111111A�?}A�E�A�-A�/A�VA���A藍A�-A�A�wAߙ�Aݗ�A�p�A�l�Aֲ-A��#A�p�A�G�A�VA��A��A��A�K�A�ffA��A�JA���A�E�A�1'A��A�`BA�XA�C�A��RA�1'A�z�A�C�A�n�A�VA�?}A��DA��A�1'A�-A�5?A�ƨA���A��A�bA|��Ay�PAw��An�\Am�wAhZAb�A^ffAV�/ALz�ACG�A;C�A6Q�A2ĜA, �A&�HA z�A%A��A�`A  @�z�@�@��@噚@���@�~�@ָRG�O�@�  @�;d@���@��m@�p�@�S�@�`B@�/@��G�O�@d�@W|�@O�P@J=q@EO�@?l�@9��@3dZ@,�@(�9@$9X@"�\@ b@��@�;@J@p�@1'@O�@9X111111111111111111111111111111111111111111111111111111111111111111111111111119111111111941111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	!�B	"�B	&�B	&�B	+B	/B	6FB	�B	�;B	�`B	�B
B
�B
DB
�B
5?B
@�B
�VB
��B
��B
��B
ȴB
ƨB
�B
�`B
�BBhB�B#�B#�B"�B�B-B7LB8RB=qBjBk�Bk�Be`B]/BN�B@�B33B�B
��B
�#B
�!B
�VB
�B
s�B
J�B
D�B
+B
hB	��B	�B	��B	{�B	[#B	I�B	;dB	)�B	�B	oB	�B	hB�B��B��B��B�B��B	JB	;dB	XG�O�B	�wB	��B	�5B	�B	��B
B
B
hB
�B
�G�O�B
6FB
;dB
@�B
D�B
H�B
P�B
YB
aHB
e`B
ffB
l�B
o�B
q�B
t�B
y�B
|�B
�B
�+B
�+111111111111111111111111111111111111111111111111111111111111111111111111111119111111111141111111111111111111B	!�B	"�B	&�B	&�B	+B	/B	6FB	�B	�;B	�`B	�B
B
�B
DB
�B
5?B
@�B
�VB
��B
��B
��B
ȴB
ƨB
�B
�`B
�BBhB�B#�B#�B"�B�B-B7LB8RB=qBjBk�Bk�Be`B]/BN�B@�B33B�B
��B
�#B
�!B
�VB
�B
s�B
J�B
D�B
+B
hB	��B	�B	��B	{�B	[#B	I�B	;dB	)�B	�B	oB	�B	hB�B��B��B��B�B��B	JB	;dB	XG�O�B	�wB	��B	�5B	�B	��B
B
B
hB
�B
�G�O�B
6FB
;dB
@�B
D�B
H�B
P�B
YB
aHB
e`B
ffB
l�B
o�B
q�B
t�B
y�B
|�B
�B
�+B
�+111111111111111111111111111111111111111111111111111111111111111111111111111119111111111141111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090804065718  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090804065719  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090804065720  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090804065720  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090804065721  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090804065721  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090804065721  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090804065721  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090804065721  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090804065721  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090804065721  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090804070321                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090806155825  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090806155953  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090806155953  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090806155954  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090806155955  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090806155955  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090806155955  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090806155955  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090806155955  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090806155955  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090806155955  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090806160343                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207081344  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207081616  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207081616  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207081617  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207081618  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207081618  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207081618  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207081618  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207081618  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207081618  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207081618  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208012320                      G�O�G�O�G�O�                