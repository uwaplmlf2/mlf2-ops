CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   p   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4\   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C<   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E,   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H,   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K,   CALIBRATION_DATE            	             
_FillValue                  ,  N,   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    NX   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N\   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N`   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Nd   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Nh   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5901560 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               9A   JA  20090824065635  20091208012332                                  2B  A   APEX-SBE 3468                                                   846 @�E�G�{1   @�E���	@1&fffff@__��v�1   ARGOS   B   B   B   @�  A��Ad��A���A���G�O�BY��BlffB�ffB���B���B�  B���B�ffB�33Bƙ�B�33B�33B�33B���B���C� C�C� C�C�3C�3C��C$L�C)33C.33C3�C833C=33CA��CGL�CQ�C[�Ce��CoffCyL�C�� C�� C�� C�s3C�s3C��3C���C�� C��fC���C�� C�� C��fC�Cǳ3C̳3Cѳ3C�� CۦfC�fC� C��C� C� C���D��D�fD� DٚDٚD� D��D$�3D)ٚD.ٚD3��D8�3D=��DB��DG�fDL� DQ�3DVٚD[�3D`� De�fDj��Do� Dt�fDyٚD�)�D�ffD��3D�ٚD�,�D�c3D�� D��3D��D�` D���D��fD�#3D�i�Dڜ�D��D�  D�ffD��D��3D��31111141111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�  A��Ad��A���A���G�O�BY��BlffB�ffB���B���B�  B���B�ffB�33Bƙ�B�33B�33B�33B���B���C� C�C� C�C�3C�3C��C$L�C)33C.33C3�C833C=33CA��CGL�CQ�C[�Ce��CoffCyL�C�� C�� C�� C�s3C�s3C��3C���C�� C��fC���C�� C�� C��fC�Cǳ3C̳3Cѳ3C�� CۦfC�fC� C��C� C� C���D��D�fD� DٚDٚD� D��D$�3D)ٚD.ٚD3��D8�3D=��DB��DG�fDL� DQ�3DVٚD[�3D`� De�fDj��Do� Dt�fDyٚD�)�D�ffD��3D�ٚD�,�D�c3D�� D��3D��D�` D���D��fD�#3D�i�Dڜ�D��D�  D�ffD��D��3D��31111141111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�5?A�/A��A��G�O�A�=qAП�A�JA�G�A�dZA¾wA��A���A��mA�ȴA��FA�33A��yA�A�+A��7A���A�33A���A�5?A���A���A�1A�ZA�{A��A�`BA��A�A���A��RA�z�A��yA���A�ZA�A��A��jA���A|�RAx�DAr�uAil�AdbA^�yA[��AU�AO�AH^5AB=qA:^5A4ȴA0�jA+�^A$Q�A!�PAoA��A��A�wA?}A	S�A{@�z�@�r�@�`B@���@���@��;@�ff@�ƨ@�ȴ@��@��@���@�;d@�Z@�t�@�@�ff@�r�@��#@��@�
=@�r�@}O�@s�@h�@a�@Z-@V�y@N�+@H  @B�\@;��@4j@-�h@)��@$j@�P@(�@Q�@�@��@�h@
�H@	��1111941111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�5?A�/A��A��G�O�A�=qAП�A�JA�G�A�dZA¾wA��A���A��mA�ȴA��FA�33A��yA�A�+A��7A���A�33A���A�5?A���A���A�1A�ZA�{A��A�`BA��A�A���A��RA�z�A��yA���A�ZA�A��A��jA���A|�RAx�DAr�uAil�AdbA^�yA[��AU�AO�AH^5AB=qA:^5A4ȴA0�jA+�^A$Q�A!�PAoA��A��A�wA?}A	S�A{@�z�@�r�@�`B@���@���@��;@�ff@�ƨ@�ȴ@��@��@���@�;d@�Z@�t�@�@�ff@�r�@��#@��@�
=@�r�@}O�@s�@h�@a�@Z-@V�y@N�+@H  @B�\@;��@4j@-�h@)��@$j@�P@(�@Q�@�@��@�h@
�H@	��1111941111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
+B
%B
B	�B	�mG�O�B
aHB
�\B
�hB
��B
��B
��B
��B
��B
��B
�!B
ÖB
ĜB
ŢB
��B
��B
��B
�#BPB#�B,BbNBp�B� B~�Bs�Bm�B^5BT�BO�BG�BA�B/B�BB
��B
�5B
�wB
��B
�=B
r�B
W
B
'�B
VB	��B	�fB	ȴB	�-B	�oB	w�B	[#B	M�B	D�B	8RB	�B	�B	�B	�B	�B	oB	+B��B	B	bB	�B	7LB	VB	)�B	33B	R�B	��B	�jB	ĜB	��B	�B	��B	��B
	7B
hB
�B
�B
#�B
&�B
"�B
$�B
$�B
/B
5?B
7LB
=qB
A�B
E�B
G�B
K�B
N�B
S�B
XB
`BB
hsB
hsB
k�B
m�B
p�B
v�B
|�B
�B
�1111141111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
+B
%B
B	�B	�mG�O�B
aHB
�\B
�hB
��B
��B
��B
��B
��B
��B
�!B
ÖB
ĜB
ŢB
��B
��B
��B
�#BPB#�B,BbNBp�B� B~�Bs�Bm�B^5BT�BO�BG�BA�B/B�BB
��B
�5B
�wB
��B
�=B
r�B
W
B
'�B
VB	��B	�fB	ȴB	�-B	�oB	w�B	[#B	M�B	D�B	8RB	�B	�B	�B	�B	�B	oB	+B��B	B	bB	�B	7LB	VB	)�B	33B	R�B	��B	�jB	ĜB	��B	�B	��B	��B
	7B
hB
�B
�B
#�B
&�B
"�B
$�B
$�B
/B
5?B
7LB
=qB
A�B
E�B
G�B
K�B
N�B
S�B
XB
`BB
hsB
hsB
k�B
m�B
p�B
v�B
|�B
�B
�1111141111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090824065634  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090824065635  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090824065636  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090824065636  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090824065637  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090824065637  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090824065637  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090824065637  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090824065637  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090824065637  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090824065637  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090824070239                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090826155644  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090826155814  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090826155815  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090826155815  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090826155816  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090826155816  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090826155816  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090826155816  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090826155816  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090826155816  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090826155816  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090826160128                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207081345  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207081618  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207081619  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207081619  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207081621  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207081621  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207081621  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207081621  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207081621  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207081621  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207081621  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208012332                      G�O�G�O�G�O�                