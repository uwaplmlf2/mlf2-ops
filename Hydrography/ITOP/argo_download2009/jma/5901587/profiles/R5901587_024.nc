CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901587 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090910125829  20090914040307                                  2B  A   APEX-SBE 4068                                                   846 @�J��41   @�J�3�a@3Vȴ9X@`�1&�x�1   ARGOS   A   A   A   @�  A��Aq��A�ffA�33A���B33B ��B733BJ  B^  Br  B���B���B���B���B�ffB�  B�33B�ffB�33B���B�ffB�33B�ffC  C��CffC�fC33C��C��C%33C)L�C/� C4�3C9� C>33CC��CHffCR�C\ffCe� CpffCxffC��C�33C��3C��C���C��3C��C���C�33C�L�C�33C��fC�@ C�� C��fC�33C���C�&fC��C���C�33C��C��3C��fC�33D3D�D  D3D��D3D�3D%  D*fD/�D3��D9�D>fDB�3DG��DM�DQ� DW�D[��DafDf�Dj�fDo��Du  Dz  D�@ D��fD�ɚD�3D�6fD�s3D���D�3D�L�D�s3D�ɚD��fD�@ DԆfD�ɚD�3D�C3D�i�D�� D�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A&fgA{34A�33A�  A���B��B#33B9��BLffB`ffBtffB�  B���B�  B���B���B�33B�ffBə�B�ffB�  B癙B�ffB���C��C34C  C� C��CfgC 34C%��C)�gC0�C5L�C:�C>��CD34CI  CR�4C]  Cf�Cq  Cy  C�Y�C�� C�@ C�fgC��C�@ C�Y�C��C�� C���C�� C�33C���C��C�33C̀ C��C�s3C�fgC�ٚC� C�Y�C�@ C�33C�� D9�D33D&fD9�D  D9�D �D%&fD*,�D/@ D43D9@ D>,�DC�DH  DM33DQ�fDW33D\  Da,�Df33Dk�Dp  Du&fDz&fD�S3D���D���D�fD�I�D��fD�� D�fD�` D��fD���D�	�D�S3Dԙ�D���D�fD�VfD�|�D��3D�\�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A�A�^5A�O�A�I�A��A���A���A�
=A��A�%A��A֧�AЏ\A�JA�XAǼjA��;APA��A� �A�ZA��uA��\A�/A�  A�z�A�G�A�VA�|�A�-A��#A�bA�VA��hA�&�A��A�(�A��A�{A��FA�Q�A��A��jA��;A�5?A��RA��;A�ƨA�7LA{�Aul�Am�mAh^5Ac�AZVAU�^APbAL(�AD��A?�A;�;A3&�A,�A&n�AdZA�A�
Al�AA�@��7@�"�@�V@�@�j@ʗ�@��-@�j@��w@�ȴ@�\)@��;@���@�S�@��9@��#@��w@�  @�z�@�5?@~$�@w�P@r=q@l��@f��@` �@St�@L�j@F��@>��@8bN@1��@+t�@(  @#t�@;d@��@�P@��@K�@��@1'@��@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A�A�^5A�O�A�I�A��A���A���A�
=A��A�%A��A֧�AЏ\A�JA�XAǼjA��;APA��A� �A�ZA��uA��\A�/A�  A�z�A�G�A�VA�|�A�-A��#A�bA�VA��hA�&�A��A�(�A��A�{A��FA�Q�A��A��jA��;A�5?A��RA��;A�ƨA�7LA{�Aul�Am�mAh^5Ac�AZVAU�^APbAL(�AD��A?�A;�;A3&�A,�A&n�AdZA�A�
Al�AA�@��7@�"�@�V@�@�j@ʗ�@��-@�j@��w@�ȴ@�\)@��;@���@�S�@��9@��#@��w@�  @�z�@�5?@~$�@w�P@r=q@l��@f��@` �@St�@L�j@F��@>��@8bN@1��@+t�@(  @#t�@;d@��@�P@��@K�@��@1'@��@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B��B��B��B��B��B��B��B	dZB	��B
	7B
A�B
�=B
y�B
��B
�B{B�B8RBM�BO�BO�B`BBdZBD�B)�B!�B'�B'�B1'B_;BYBR�BH�BL�B;dBJ�BS�BP�BK�BA�B:^B-B �B{B
��B
�yB
��B
�^B
��B
�PB
iyB
G�B
-B
{B	�sB	��B	�FB	��B	�B	o�B	_;B	9XB	�B	
=B��B�mB�/B��BɺB�B��B��B��BĜBǮB�BB	B��B	,B	@�B	J�B	dZB	jB	w�B	�oB	��B	�!B	�dB	�jB	ƨB	��B	��B	�;B	�sB
B
uB
�B
+B
2-B
8RB
@�B
I�B
N�B
S�B
YB
`BB
ffB
k�B
p�B
t�B
y�B
}�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B��B��B��B��B��B��B��B	dZB	��B
	7B
A�B
�=B
y�B
��B
�B{B�B8RBM�BO�BO�B`BBdZBD�B)�B!�B'�B'�B1'B_;BYBR�BH�BL�B;dBJ�BS�BP�BK�BA�B:^B-B �B{B
��B
�yB
��B
�^B
��B
�PB
iyB
G�B
-B
{B	�sB	��B	�FB	��B	�B	o�B	_;B	9XB	�B	
=B��B�mB�/B��BɺB�B��B��B��BĜBǮB�BB	B��B	,B	@�B	J�B	dZB	jB	w�B	�oB	��B	�!B	�dB	�jB	ƨB	��B	��B	�;B	�sB
B
uB
�B
+B
2-B
8RB
@�B
I�B
N�B
S�B
YB
`BB
ffB
k�B
p�B
t�B
y�B
}�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090910125829  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090910125829  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090910125830  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090910125830  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090910125831  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090910125831  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090910125831  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090910125831  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090910125832  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090910130216                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090914035831  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090914035933  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090914035933  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090914035934  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090914035935  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090914035935  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090914035935  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090914035935  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090914035935  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090914040307                      G�O�G�O�G�O�                