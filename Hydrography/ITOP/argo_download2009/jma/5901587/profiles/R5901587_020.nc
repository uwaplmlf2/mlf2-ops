CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901587 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090801125717  20090805040322                                  2B  A   APEX-SBE 4068                                                   846 @�@�ߒ��1   @�@�4��@2�/��w@`�7Kƨ1   ARGOS   A   A   A   @�ffA   AvffA�  A�  A���BffB"��B733BJffB^  Br  B���B���B�  B���B�ffB���B���B���BЙ�B�33B�ffBB���C� CffCffC33C� C� C ffC%ffC)�3C/L�C433C933C>��CC��CHL�CRffC\L�Ce�fCo�3Cz��C��C�33C�33C��C�@ C���C�Y�C�33C�33C�&fC��C�  C�&fC��fC��C�  C�@ C�Y�C�  C�  C�33C�@ C�ٚC�  C�@ D3DfD  D��D3D��D   D%  D)��D/  D43D8�3D>�DC  DG�3DL��DR�DW  D\�DafDffDk�Do��DtffDz3D�I�D���D�� D���D�<�D�s3D��3D��fD�C3D�p D���D�  D�I�Dԉ�D��3D�fD�FfD��D�� D�3D�  11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA(  A~ffA�  A�  A���BffB$��B933BLffB`  Bt  B���B���B�  B���B�ffB���B���B���Bљ�B�33B�ffB�B���C  C�fC�fC�3C  C  C �fC%�fC*33C/��C4�3C9�3C?�CD�CH��CR�fC\��CfffCp33C{�C�L�C�s3C�s3C�L�C�� C��C���C�s3C�s3C�ffC�Y�C�@ C�ffC�&fC�Y�C�@ Cр Cי�C�@ C�@ C�s3C� C��C�@ C�� D33D&fD@ D�D33D�D   D%  D*�D/  D433D93D>,�DC  DH3DL��DR,�DW  D\,�Da&fDf&fDk,�Dp�Dt�fDz33D�Y�D���D�� D��D�L�D��3D��3D�fD�S3D�� D���D� D�Y�Dԙ�D��3D�fD�VfD��D�� D�#3D�0 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�RAA�\A�DA�7A��yA�$�A�9XA�ȴA�dZA٥�A�&�A�S�A��A���A�p�A͉7A�-A�7LA�ffA���A��;A�33A��uA�%A��hA�/A���A�ffA�7LA��-A�t�A�~�A�$�A�&�A���A�ZA�A�{A��uA��PA��mA��A���A�~�A���A�\)A��A��A�jA|�At��Ao��Ah~�A` �AX�AT��AO�AI�AE\)AA��A>�jA/
=A*��A$�AoAI�At�A�A�Ax�A;d@��^@���@ߕ�@�@�o@+@�^5@�@�Ĝ@�Z@�|�@��\@��@��`@�^5@�bN@���@��\@�
=@�7L@zJ@tZ@p�@hĜ@_+@TI�@L(�@DI�@>v�@5�@0��@*�!@&�+@"��@�@�#@�P@��@^5@  @t�@	X@�@�T11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�RAA�\A�DA�7A��yA�$�A�9XA�ȴA�dZA٥�A�&�A�S�A��A���A�p�A͉7A�-A�7LA�ffA���A��;A�33A��uA�%A��hA�/A���A�ffA�7LA��-A�t�A�~�A�$�A�&�A���A�ZA�A�{A��uA��PA��mA��A���A�~�A���A�\)A��A��A�jA|�At��Ao��Ah~�A` �AX�AT��AO�AI�AE\)AA��A>�jA/
=A*��A$�AoAI�At�A�A�Ax�A;d@��^@���@ߕ�@�@�o@+@�^5@�@�Ĝ@�Z@�|�@��\@��@��`@�^5@�bN@���@��\@�
=@�7L@zJ@tZ@p�@hĜ@_+@TI�@L(�@DI�@>v�@5�@0��@*�!@&�+@"��@�@�#@�P@��@^5@  @t�@	X@�@�T11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B��B�B�!B�^B��B	bB	�VB
�B
A�B
]/B
�RB
��B
�fB
�BB
�B
B
�#BJB.BaHBhsBffBx�B^5BjBl�B��B��B�}B�}B�}B�qB�LB�!B��B��B�\B�DBdZBK�BG�B5?B�B1B
�yB
�}B
��B
��B
�7B
dZB
M�B
$�B	��B	�B	ǮB	�B	�\B	�B	o�B	`BB	$�B	uB	  B�B�ZB�B��BÖB�FB�-B��B��B��B�B�?B�wB��B�B	uB	!�B	C�B	k�B	�VB	�\B	��B	��B	��B	�3B	�jB	ÖB	��B	��B	�5B	��B
\B
#�B
1'B
;dB
C�B
I�B
P�B
VB
\)B
_;B
e`B
hsB
k�B
n�B
r�B
u�B
z�B
|�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B��B��B�B�!B�^B��B	bB	�VB
�B
A�B
]/B
�RB
��B
�fB
�BB
�B
B
�#BJB.BaHBhsBffBx�B^5BjBl�B��B��B�}B�}B�}B�qB�LB�!B��B��B�\B�DBdZBK�BG�B5?B�B1B
�yB
�}B
��B
��B
�7B
dZB
M�B
$�B	��B	�B	ǮB	�B	�\B	�B	o�B	`BB	$�B	uB	  B�B�ZB�B��BÖB�FB�-B��B��B��B�B�?B�wB��B�B	uB	!�B	C�B	k�B	�VB	�\B	��B	��B	��B	�3B	�jB	ÖB	��B	��B	�5B	��B
\B
#�B
1'B
;dB
C�B
I�B
P�B
VB
\)B
_;B
e`B
hsB
k�B
n�B
r�B
u�B
z�B
|�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090801125716  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090801125717  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090801125718  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090801125718  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090801125719  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090801125719  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090801125719  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090801125719  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090801125719  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090801130109                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090805035828  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090805035935  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090805035935  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090805035935  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090805035937  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090805035937  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090805035937  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090805035937  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090805035937  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090805040322                      G�O�G�O�G�O�                