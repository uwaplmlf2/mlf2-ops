CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901587 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091020125807  20091024040213                                  2B  A   APEX-SBE 4068                                                   846 @�T�DDDD1   @�T��c]�@3ffffff@`�ě��T1   ARGOS   A   A   A   @���AffAq��A���Aə�A�ffB��B"  B533BJ  B]33Bq��B�  B�  B���B�  B�33B�  B�ffB���B���B�ffB���B�ffB�33C� CffC  C  CffC��C   C$�fC*��C/��C4ffC9� C>33CC� CG��CQ��C[ffCf��CpffCz  C�@ C�&fC�33C��3C��3C�ٚC�  C��fC�L�C�&fC�@ C�@ C�33C�&fC�&fC��C��fCր C�Y�C�&fC��C��fC��C�  C�33DfD  D��D�D  D  D�3D$�fD*�D/fD4  D8��D=�3DB�fDG�3DM�DQ��DV�3D[��D`�3Df  Dk  Do�fDu�Dz  D�P D�l�D��3D�  D�@ D��3D���D�  D�@ D�|�D�ɚD��D�L�DԀ D�ɚD��3D�I�D�y�D��D��fD��f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A&ffAy��A���A͙�A�ffB��B$  B733BL  B_33Bs��B�  B�  B���B�  B�33B�  B�ffB���B���B�ffB���B�ffB�33C  C�fC� C� C�fCL�C � C%ffC+L�C0�C4�fC:  C>�3CD  CHL�CRL�C[�fCg�Cp�fCz� C�� C�ffC�s3C�33C�33C��C�@ C�&fC���C�ffC�� C�� C�s3C�ffC�ffC�L�C�&fC�� Cܙ�C�ffC�Y�C�&fC�Y�C�@ C�s3D&fD  DٚD9�D  D@ D 3D%fD*9�D/&fD4@ D9�D>3DCfDH3DM9�DR�DW3D\�Da3Df  Dk  DpfDu,�Dz  D�` D�|�D��3D� D�P D��3D���D� D�P D���D�ٚD��D�\�DԐ D�ٚD�3D�Y�D퉚D���D��fD��f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�A�A�A�I�A�7LA�=qA��A�VA�A��/A���AҴ9Aҥ�Aҏ\A�JA�v�A�v�A�;dA��
A�z�Aƛ�A�{A��/A�bA��DA�9XA�A�{A��hA�;dA��9A�^5A��A�Q�A���A�  A��A��`A��A���A�ȴA�O�A��DA�A�A�A�dZA��A�G�A|��Axv�Ap�HAl�uAg&�AdE�A\ĜAV9XAN�\AHbAE��A=�FA6��A1�wA+��A(�A!ƨA  AƨAA�A�A7LA
r�Ao@�|�@ߍP@�ff@őh@��;@�b@�@���@���@�V@�%@��@���@�\)@�Ĝ@�/@��@�K�@�A�@���@���@�@}?}@u`B@f5?@_+@U@N�@Gl�@@�`@:��@6ff@0Q�@*�!@&@!&�@�j@G�@�j@�`@��@
��@�@O�@V11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�A�A�A�A�I�A�7LA�=qA��A�VA�A��/A���AҴ9Aҥ�Aҏ\A�JA�v�A�v�A�;dA��
A�z�Aƛ�A�{A��/A�bA��DA�9XA�A�{A��hA�;dA��9A�^5A��A�Q�A���A�  A��A��`A��A���A�ȴA�O�A��DA�A�A�A�dZA��A�G�A|��Axv�Ap�HAl�uAg&�AdE�A\ĜAV9XAN�\AHbAE��A=�FA6��A1�wA+��A(�A!ƨA  AƨAA�A�A7LA
r�Ao@�|�@ߍP@�ff@őh@��;@�b@�@���@���@�V@�%@��@���@�\)@�Ĝ@�/@��@�K�@�A�@���@���@�@}?}@u`B@f5?@_+@U@N�@Gl�@@�`@:��@6ff@0Q�@*�!@&@!&�@�j@G�@�j@�`@��@
��@�@O�@V11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
6FB
6FB
5?B
6FB
5?B
5?B
5?B
5?B
6FB
6FB
7LB
8RB
9XB
?}B
F�B
E�B
G�B
aHB
�B
�?B�BM�B_;BiyBo�BhsBe`B^5B\)BT�BYB[#B_;BaHBXBK�B@�B;dB49B,B�B\B
��B
�TB
��B
ÖB
��B
�=B
s�B
O�B
8RB
�B
JB	�sB	ȴB	��B	�DB	|�B	XB	=qB	&�B	uB	1B�B�NB��BŢB�^B�3B�B��B�uB�+B��B��B�B�^B�TB��B	�B	2-B	L�B	_;B	p�B	�B	�uB	��B	�B	�RB	��B	ɺB	ɺB	��B	�)B	�sB
B
oB
�B
1'B
9XB
A�B
G�B
K�B
O�B
T�B
[#B
_;B
dZB
hsB
o�B
s�B
w�B
z�B
~�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
6FB
6FB
5?B
6FB
5?B
5?B
5?B
5?B
6FB
6FB
7LB
8RB
9XB
?}B
F�B
E�B
G�B
aHB
�B
�?B�BM�B_;BiyBo�BhsBe`B^5B\)BT�BYB[#B_;BaHBXBK�B@�B;dB49B,B�B\B
��B
�TB
��B
ÖB
��B
�=B
s�B
O�B
8RB
�B
JB	�sB	ȴB	��B	�DB	|�B	XB	=qB	&�B	uB	1B�B�NB��BŢB�^B�3B�B��B�uB�+B��B��B�B�^B�TB��B	�B	2-B	L�B	_;B	p�B	�B	�uB	��B	�B	�RB	��B	ɺB	ɺB	��B	�)B	�sB
B
oB
�B
1'B
9XB
A�B
G�B
K�B
O�B
T�B
[#B
_;B
dZB
hsB
o�B
s�B
w�B
z�B
~�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20091020125806  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091020125807  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091020125807  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091020125807  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091020125809  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091020125809  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091020125809  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091020125809  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091020125809  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091020130245                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091024035732  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091024035813  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091024035813  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091024035814  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091024035815  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091024035815  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091024035815  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091024035815  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091024035815  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091024040213                      G�O�G�O�G�O�                