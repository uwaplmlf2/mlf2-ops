CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901913 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090927005658  20090930190511                                  2B  A   APEX-SBE 4141                                                   846 @�N�n]L;1   @�N�OW�@7Qhr� �@b��C��1   ARGOS   A   A   A   @�33A33A`  A�ffA�  A�ffB  B#33B7��BK��B^��BrffB�33B���B�33B���B�  B���B�ffB���B�33B�33B�33B�  B���B���C  C� CL�C� CffC ��C%33C*  C/33C4ffC9�C=��CB��CH� CR� C\� Cf��Co�fCzL�C�&fC�  C��C��3C��C�@ C�@ C�Y�C��C��C�  C�&fC�L�C�  C��C�  C�  C�  C��C�3C��C�33C�&fC�33C��3D3D��D�fDfDfDfD��D$��D*�D/  D43D9fD>  DC  DH  DM�DR�DW3D\�D`�3Df�Dj��Do�fDt�3Dz3D�FfD���D�� D�3D�C3D�|�D�ɚD� D�I�D�vfD���D�  D�L�D�y�Dڼ�D�	�D�0 D�p D��D�� D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���AffAc33A�  Aə�A�  B��B$  B8fgBLfgB_��Bs33B���B�  B���B�33B�ffB�  B���B�33Bҙ�Bܙ�B噙B�ffB�33C �C33C�3C� C�3C��C ��C%ffC*33C/ffC4��C9L�C>  CB��CH�3CR�3C\�3Cf��Cp�Cz� C�@ C��C�34C��C�&gC�Y�C�Y�C�s4C�&gC�&gC��C�@ C�fgC��C�34C��C��C��C�&gC���C�34C�L�C�@ C�L�C��D  DgD�3D3D3D3D��D%gD*&gD/�D4  D93D>,�DC�DH,�DM�DR&gDW  D\&gDa  Df&gDkgDo�3Du  Dz  D�L�D��3D��fD�	�D�I�D��3D�� D�fD�P D�|�D��3D�fD�S3DԀ D��3D� D�6fD�vfD��3D��fD��311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�I�A�G�A�I�A�I�A�?}A���A�hA���A�E�Aȩ�A��A�jA��A�5?A���A�A�-A���A�A�^5A��9A�p�A��PA��A��FA��A��-A�JA��hA�x�A���A��9A�n�A���A��A�t�A��A�Q�A�bA�/A�v�A�%A��
A�
=A�5?A�VA�?}A�ĜA��#A~��Az^5Avr�As"�AoG�Ak��Ag��AaC�A[�AV�/AQAO��AL1'AF�AB-A;�FA0VA(�`A$bNA#��A�yAbNAZ@�V@�$�@ץ�@�t�@��@��@��;@���@��@�`B@��h@��@���@��y@��y@�&�@���@��;@��/@�=q@}�@yX@t��@fv�@U�h@L(�@EV@=V@3�
@)�^@#@+@�D@&�@�D@Q�@O�@A�@5?@�?��;?�?�K�?�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�I�A�G�A�I�A�I�A�?}A���A�hA���A�E�Aȩ�A��A�jA��A�5?A���A�A�-A���A�A�^5A��9A�p�A��PA��A��FA��A��-A�JA��hA�x�A���A��9A�n�A���A��A�t�A��A�Q�A�bA�/A�v�A�%A��
A�
=A�5?A�VA�?}A�ĜA��#A~��Az^5Avr�As"�AoG�Ak��Ag��AaC�A[�AV�/AQAO��AL1'AF�AB-A;�FA0VA(�`A$bNA#��A�yAbNAZ@�V@�$�@ץ�@�t�@��@��@��;@���@��@�`B@��h@��@���@��y@��y@�&�@���@��;@��/@�=q@}�@yX@t��@fv�@U�h@L(�@EV@=V@3�
@)�^@#@+@�D@&�@�D@Q�@O�@A�@5?@�?��;?�?�K�?�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B)�B)�B)�B+B)�B.BbB&�BBhB+B.B#�B"�B�B&�B)�B:^B?}B>wB<jBP�B[#BbNB`BB]/BW
BL�BI�BF�B?}B8RB5?B.B(�B%�B!�B�B�B�BVB  B
��B
�B
�BB
�B
�
B
��B
�FB
��B
�1B
u�B
dZB
P�B
>wB
)�B

=B	�B	�)B	B	�dB	��B	�DB	w�B	ZB	(�B	%B�B�B�/B�^B��Bp�BYBVBR�B^5Bo�Bt�B�VB��B�B�FB��B�
B�B��B		7B	�B	'�B	S�B	^5B	t�B	}�B	�%B	��B	�wB	��B	�5B	�B	��B
bB
�B
%�B
-B
49B
=qB
D�B
J�B
VB
[#B
^5B
k�B
r�B
v�B
v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B)�B)�B)�B+B)�B.BbB&�BBhB+B.B#�B"�B�B&�B)�B:^B?}B>wB<jBP�B[#BbNB`BB]/BW
BL�BI�BF�B?}B8RB5?B.B(�B%�B!�B�B�B�BVB  B
��B
�B
�BB
�B
�
B
��B
�FB
��B
�1B
u�B
dZB
P�B
>wB
)�B

=B	�B	�)B	B	�dB	��B	�DB	w�B	ZB	(�B	%B�B�B�/B�^B��Bp�BYBVBR�B^5Bo�Bt�B�VB��B�B�FB��B�
B�B��B		7B	�B	'�B	S�B	^5B	t�B	}�B	�%B	��B	�wB	��B	�5B	�B	��B
bB
�B
%�B
-B
49B
=qB
D�B
J�B
VB
[#B
^5B
k�B
r�B
v�B
v�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090927005657  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090927005658  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090927005659  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090927005700  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090927005701  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090927005701  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090927005701  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090927005701  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090927005701  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090927010145                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090930185940  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090930190041  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090930190041  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090930190041  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090930190042  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090930190042  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090930190042  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090930190042  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090930190043  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090930190511                      G�O�G�O�G�O�                