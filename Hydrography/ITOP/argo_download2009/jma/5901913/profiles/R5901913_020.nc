CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901913 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090828005658  20090831160523                                  2B  A   APEX-SBE 4141                                                   846 @�G';�0*1   @�G'ffff@7��-V@b�ȴ9X1   ARGOS   A   A   A   @���A#33Ap  A�  A�  A�ffBffB#33B4��BF��B]33Bp��B���B���B���B�33B�ffB�ffB�  B�33B���B�33B���B�33B���C�fC33C��C33C� C�C L�C$�fC*  C/33C3��C9ffC>ffCB� CH  CQ��C\33Cc��CpffCz� C�&fC�33C��C�&fC�@ C�&fC��3C��fC�  C��C�@ C�&fC��3C��fC��fC�&fC��3C�33C�ٚC�  C�ٚC�@ C�  C�33C�  D  D�3D3D�D��D�D �D$�fD)��D/3D3�fD93D>&fDB��DH3DL��DR&fDV��D\fDa3De�3Dk3Dp3DufDz3D�<�D��fD���D�fD�L�D�� D�ɚD� D�I�D�p D�ɚD��fD�0 DԌ�Dڳ3D�� D�P D�vfD�D�fD��311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�34A(  At��A�ffA�ffA���B��B$ffB6  BH  B^ffBr  B�fgB�fgB�34B���B�  B�  B���B���B�fgB���B�fgB���B�fgC33C� C�C� C��CfgC ��C%33C*L�C/� C4�C9�3C>�3CB��CHL�CQ�gC\� Cc�gCp�3Cz��C�L�C�Y�C�33C�L�C�ffC�L�C��C��C�&fC�@ C�ffC�L�C��C��C��C�L�C��C�Y�C�  C�&fC�  C�ffC�&fC�Y�C�&fD33DfD&fD,�D�D  D   D$��D*  D/&fD3ٙD9&fD>9�DC  DH&fDM  DR9�DW�D\�Da&fDffDk&fDp&fDu�Dz&fD�FgD�� D��4D� D�VgD���D��4D��D�S4D�y�D��4D�  D�9�DԖgDڼ�D���D�Y�D� D��4D� D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�jA�n�A�v�A�^5A�JA���A�ZA�ƨA��A�n�A��A���A��A��mA���A�Q�A�x�A���A�A��A�\)A� �A��A�ZA���A���A�l�A���A�hsA��jA�^5A���A���A��/A���A�VA�v�A�\)A���A�7LA���A�5?A�  A�A�Q�A�M�A�^5A���A�{A{��AyAtn�Am��AiAeƨA^�AXn�AV�jASp�AP��AL$�AIC�AG
=AC�
A<��A:ZA.A'��A!VA�yA��A$�@�@�@��/@Ӆ@�@�p�@�S�@�@�X@�\)@���@�V@��^@�S�@�|�@��@���@�~�@��u@��@�%@x �@n�@f��@\�@U�@HbN@B�\@6��@0�@'�@�P@�@�P@�D@M�@K�@
n�@�@�m@J?�v�?���?�
=11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�jA�n�A�v�A�^5A�JA���A�ZA�ƨA��A�n�A��A���A��A��mA���A�Q�A�x�A���A�A��A�\)A� �A��A�ZA���A���A�l�A���A�hsA��jA�^5A���A���A��/A���A�VA�v�A�\)A���A�7LA���A�5?A�  A�A�Q�A�M�A�^5A���A�{A{��AyAtn�Am��AiAeƨA^�AXn�AV�jASp�AP��AL$�AIC�AG
=AC�
A<��A:ZA.A'��A!VA�yA��A$�@�@�@��/@Ӆ@�@�p�@�S�@�@�X@�\)@���@�V@��^@�S�@�|�@��@���@�~�@��u@��@�%@x �@n�@f��@\�@U�@HbN@B�\@6��@0�@'�@�P@�@�P@�D@M�@K�@
n�@�@�m@J?�v�?���?�
=11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
S�B
T�B
S�B
XB
\)B
G�B
�=B
ÖB-B/B#�B �B2-B0!B(�B"�B�B�B�BoBDB
��B1'B�B%B
��B
��B
��B+BJBDB+BPBBB  B
��B
�B
�B
�fB
�B
��B
�B
�B
�
B
��B
�jB
�B
��B
�PB
}�B
ffB
E�B
.B
�B	��B	�5B	��B	ŢB	�RB	��B	��B	�1B	z�B	\)B	M�B	hB��B�BB��B��B�=Bq�Be`BXBW
B^5BdZBo�B�B�=B��B�TB�B	PB	�B	'�B	9XB	<jB	A�B	L�B	XB	cTB	w�B	��B	�B	�dB	��B	�)B	�fB	��B
	7B
�B
'�B
7LB
>wB
C�B
J�B
R�B
\)B
cTB
hsB
k�B
p�B
w�B
x�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
S�B
T�B
S�B
XB
\)B
G�B
�=B
ÖB-B/B#�B �B2-B0!B(�B"�B�B�B�BoBDB
��B1'B�B%B
��B
��B
��B+BJBDB+BPBBB  B
��B
�B
�B
�fB
�B
��B
�B
�B
�
B
��B
�jB
�B
��B
�PB
}�B
ffB
E�B
.B
�B	��B	�5B	��B	ŢB	�RB	��B	��B	�1B	z�B	\)B	M�B	hB��B�BB��B��B�=Bq�Be`BXBW
B^5BdZBo�B�B�=B��B�TB�B	PB	�B	'�B	9XB	<jB	A�B	L�B	XB	cTB	w�B	��B	�B	�dB	��B	�)B	�fB	��B
	7B
�B
'�B
7LB
>wB
C�B
J�B
R�B
\)B
cTB
hsB
k�B
p�B
w�B
x�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090828005656  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090828005658  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090828005658  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090828005658  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090828005700  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090828005700  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090828005700  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090828005700  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090828005700  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090828010237                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090831155911  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090831160034  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090831160035  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090831160035  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090831160036  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090831160036  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090831160036  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090831160036  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090831160036  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090831160523                      G�O�G�O�G�O�                