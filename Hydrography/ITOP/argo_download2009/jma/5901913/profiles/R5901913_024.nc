CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901913 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091007035809  20091010160448                                  2B  A   APEX-SBE 4141                                                   846 @�Q&]��1   @�Q&����@7      @b�$�/1   ARGOS   A   A   A   @�  A   Ak33A���A�  A�33B  B!33B4  BJ  B\ffBp��B�  B�33B���B�  B�33B�33B�33B�ffB���B���B���B�  B���B�  CL�C�C� C33CffC ��C%33C*  C.�fC4� C933C=��CCffCH  CQ�fC[�3Ce33CpffCz  C��3C��C�&fC�&fC��C��C��fC�33C�� C��C��C�ffC��C��C�  C��C�&fC�ٚC��fC�&fC�&fC��C��C��C�L�D��D�D�DfD�DfD��D%  D*fD.��D4fD9&fD>3DC  DG�3DM  DRfDW�D\  DafDf  Dk�Do��DtٚDz�D�9�D�� D��fD�3D�<�D�s3D��fD��D�L�D�y�D�� D��fD�FfD�l�D��3D�3D�<�D�vfD�fD���D�@ 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA#33AnffA�34Aə�A���B��B"  B4��BJ��B]33Bq��B�ffB���B�33B�ffB���B���B���B���B�33B�33B�33B�ffB�33B�ffC� CL�C�3CffC��C ��C%ffC*33C/�C4�3C9ffC>  CC��CH33CR�C[�fCeffCp��Cz33C��C�34C�@ C�@ C�&gC�34C�  C�L�C�ٚC�&gC�&gC�� C�&gC�&gC��C�&gC�@ C��4C�  C�@ C�@ C�34C�34C�&gC�fgD��D&gD&gD3D&gD3D��D%,�D*3D/gD43D933D>  DC,�DH  DM�DR3DW�D\,�Da3Df�Dk�DpgDt�gDz�D�@ D��fD���D�	�D�C3D�y�D���D�3D�S3D�� D��fD���D�L�D�s3D�əD�	�D�C3D�|�D��D��3D�Ff11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��TA���A���A���A���A�  A�A�%A�A�A�z�A�ffA��
A��A�bA�/A��RA���A�A��A�XA�A�^5A��PA�ZA�v�A�K�A�bNA�ȴA��mA�XA���A��
A���A��A�oA��DA�{A� �A�z�A���A�1'A�C�A�/A��A�A�1'A��hA�G�A��FAS�AyC�Au
=ApjAl��Aj�jAct�A_G�AZ1'AWXARn�AK��AH�AD��AC��A=��A;;dA7\)A,��A$ZA�A�AV@�E�@�@�z�@�K�@�|�@�1@���@�\)@�t�@�j@��D@��F@���@�&�@�Q�@���@�V@y&�@qG�@p�u@mO�@f��@^��@W\)@Q7L@I7L@=`B@3S�@,�j@%�h@!&�@I�@��@S�@��@	��@�w@�m@�?�|�?�"�?��u11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A��TA���A���A���A���A�  A�A�%A�A�A�z�A�ffA��
A��A�bA�/A��RA���A�A��A�XA�A�^5A��PA�ZA�v�A�K�A�bNA�ȴA��mA�XA���A��
A���A��A�oA��DA�{A� �A�z�A���A�1'A�C�A�/A��A�A�1'A��hA�G�A��FAS�AyC�Au
=ApjAl��Aj�jAct�A_G�AZ1'AWXARn�AK��AH�AD��AC��A=��A;;dA7\)A,��A$ZA�A�AV@�E�@�@�z�@�K�@�|�@�1@���@�\)@�t�@�j@��D@��F@���@�&�@�Q�@���@�V@y&�@qG�@p�u@mO�@f��@^��@W\)@Q7L@I7L@=`B@3S�@,�j@%�h@!&�@I�@��@S�@��@	��@�w@�m@�?�|�?�"�?��u11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�BL�BL�BK�BK�BK�BL�BK�BM�BL�BL�BM�BJ�B/BC�BB�B$�B�B#�B+B)�BA�B2-B&�B&�BH�BI�BH�B8RB,B�B�B�BB
��B
�B
�B
��B	7B7LB:^B33B&�B�BJBB
��B
�yB
�HB
�B
ŢB
�FB
��B
�B
l�B
VB
E�B
9XB
�B
B	�B	�/B	ĜB	��B	��B	�+B	�B	dZB	ZB	E�B	�B�B�qB��Bu�BgmB]/B_;B\)BdZBp�B�=B��B��B��B�/B�B��B	\B	/B	E�B	Q�B	cTB	t�B	� B	��B	�B	��B	��B	�B	�mB	�B	��B
DB
�B
"�B
-B
49B
>wB
L�B
S�B
YB
`BB
jB
m�B
r�B
u�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111BL�BL�BK�BK�BK�BL�BK�BM�BL�BL�BM�BJ�B/BC�BB�B$�B�B#�B+B)�BA�B2-B&�B&�BH�BI�BH�B8RB,B�B�B�BB
��B
�B
�B
��B	7B7LB:^B33B&�B�BJBB
��B
�yB
�HB
�B
ŢB
�FB
��B
�B
l�B
VB
E�B
9XB
�B
B	�B	�/B	ĜB	��B	��B	�+B	�B	dZB	ZB	E�B	�B�B�qB��Bu�BgmB]/B_;B\)BdZBp�B�=B��B��B��B�/B�B��B	\B	/B	E�B	Q�B	cTB	t�B	� B	��B	�B	��B	��B	�B	�mB	�B	��B
DB
�B
"�B
-B
49B
>wB
L�B
S�B
YB
`BB
jB
m�B
r�B
u�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20091007035808  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091007035809  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091007035810  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091007035810  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091007035811  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091007035811  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091007035811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091007035811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091007035811  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091007040218                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091010155911  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091010160005  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091010160005  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091010160006  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091010160007  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091010160007  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091010160007  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091010160007  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091010160007  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091010160448                      G�O�G�O�G�O�                