CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901913 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090917005702  20090920160244                                  2B  A   APEX-SBE 4141                                                   846 @�L&�`U1   @�L+R�@70�`A�7@b���v�1   ARGOS   A   A   A   @���A&ffAk33A���A���A�33B��B!33B3��BI33B\ffBp��B�  B�ffB�33B���B�  B�33B�  B�  B�ffBݙ�B晚B�B�  CL�C�fCL�C��CL�C� C � C%� C*�C/� C4�C9  C>33CB� CH33CQffC\L�CfffCp� Cz33C�&fC��3C�&fC��3C��C��C��C�ٚC�&fC��3C��C�&fC��C�@ C��C�33C�&fC��3C��C��C�  C�33C�  C�33C��3D�fD3D�3D�D3D�D fD%3D*�D/fD4fD9  D>  DC�DH�DL��DR�DW  D\  D`��DffDk�Dp�Dt�fDz&fD�C3D�� D��fD�fD�FfD�p D���D��D�FfD�� D��3D���D�<�D�i�D�� D�  D�9�D� D�fD�� D�c311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�fgA+33Ap  A�33A�33A���B  B"ffB4��BJffB]��Br  B���B�  B���B�fgB���B���B���Bə�B�  B�34B�34B�34B���C��C33C��C�C��C��C ��C%��C*fgC/��C4fgC9L�C>� CB��CH� CQ�3C\��Cf�3Cp��Cz� C�L�C��C�L�C��C�33C�33C�33C�  C�L�C��C�33C�L�C�33C�ffC�@ C�Y�C�L�C��C�@ C�33C�&fC�Y�C�&fC�Y�C��D��D&fDfD,�D&fD  D �D%&fD*,�D/�D4�D93D>3DC  DH,�DM  DR,�DW3D\33Da�Df�Dk,�Dp  Dt��Dz9�D�L�D���D�� D� D�P D�y�D��4D�gD�P D���D���D�4D�FgD�s4D�ɚD�	�D�C4D퉚D�� D�ٚD�l�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�
=A�VA�oA�$�A��
A�A��Aß�A� �A�1A�=qA���A�C�A�v�A���A��`A�;dA�VA�  A�$�A�
=A�S�A���A��
A�A�E�A��+A�7LA��A��A��A�VA�\)A��A��A�^5A�Q�A���A�l�A�ffA�%A�-A�K�A�x�A���A�bNA�ĜA~VAz�uAw|�AtE�Ao�;Aj��Afz�A_�A[7LAW
=AP��AOhsAG�A@-A;A4��A.^5A)A&��A"JA�A��A��A�A M�@��-@ݺ^@��T@���@�o@��@���@�t�@��@�X@�t�@���@� �@���@�t�@�%@���@��@~V@zJ@v��@r��@h�@W;d@P��@H�u@:J@0 �@,��@%`B@ �u@��@�/@C�@�w@1@	x�@��@��?��w?��#?���?�9X11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�A�
=A�VA�oA�$�A��
A�A��Aß�A� �A�1A�=qA���A�C�A�v�A���A��`A�;dA�VA�  A�$�A�
=A�S�A���A��
A�A�E�A��+A�7LA��A��A��A�VA�\)A��A��A�^5A�Q�A���A�l�A�ffA�%A�-A�K�A�x�A���A�bNA�ĜA~VAz�uAw|�AtE�Ao�;Aj��Afz�A_�A[7LAW
=AP��AOhsAG�A@-A;A4��A.^5A)A&��A"JA�A��A��A�A M�@��-@ݺ^@��T@���@�o@��@���@�t�@��@�X@�t�@���@� �@���@�t�@�%@���@��@~V@zJ@v��@r��@h�@W;d@P��@H�u@:J@0 �@,��@%`B@ �u@��@�/@C�@�w@1@	x�@��@��?��w?��#?���?�9X11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
x�B
w�B
w�B
v�B
t�B
{�B
�bB
�?B
��B
��B{BuB�B"�B)�B-B2-B1'B0!B<jBB�BG�B^5BdZBZBZBQ�BM�BG�B8RB1'B'�B#�B�B�B�B�BbBPB
=B%B
��B
�B
�BB
��B
��B
�dB
�B
��B
�bB
y�B
iyB
S�B
<jB
&�B
%B	�B	�)B	��B	�FB	�VB	m�B	T�B	;dB	�B	
=B��B�sB�B�jB��B�VBv�B[#BQ�BN�BM�BP�Bp�B{�B�hB��B�BB�B	B	.B	<jB	F�B	\)B	ffB	o�B	y�B	�B	�PB	��B	��B	�)B	�mB	��B
+B
PB
�B
$�B
2-B
=qB
B�B
K�B
S�B
\)B
e`B
iyB
m�B
s�B
x�B
z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
x�B
w�B
w�B
v�B
t�B
{�B
�bB
�?B
��B
��B{BuB�B"�B)�B-B2-B1'B0!B<jBB�BG�B^5BdZBZBZBQ�BM�BG�B8RB1'B'�B#�B�B�B�B�BbBPB
=B%B
��B
�B
�BB
��B
��B
�dB
�B
��B
�bB
y�B
iyB
S�B
<jB
&�B
%B	�B	�)B	��B	�FB	�VB	m�B	T�B	;dB	�B	
=B��B�sB�B�jB��B�VBv�B[#BQ�BN�BM�BP�Bp�B{�B�hB��B�BB�B	B	.B	<jB	F�B	\)B	ffB	o�B	y�B	�B	�PB	��B	��B	�)B	�mB	��B
+B
PB
�B
$�B
2-B
=qB
B�B
K�B
S�B
\)B
e`B
iyB
m�B
s�B
x�B
z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090917005701  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090917005702  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090917005702  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090917005703  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090917005704  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090917005704  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090917005704  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090917005704  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090917005704  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090917010154                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090920155818  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090920155914  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090920155915  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090920155915  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090920155916  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090920155916  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090920155916  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090920155916  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090920155916  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090920160244                      G�O�G�O�G�O�                