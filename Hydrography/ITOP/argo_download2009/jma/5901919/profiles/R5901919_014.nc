CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901919 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090815095643  20090819010331                                  2B  A   APEX-SBE 4054                                                   846 @�D�~K1   @�D�%�@<�$�/�@a�$�/�1   ARGOS   A   A   A   @l��A&ffAt��A���A͙�A�ffB33B!33B4��BHffBV��BrffB�  B�  B�33B���B���B�33B�33B�33B���B�33B�33B���B�  C  C�fC�fC� C33C�fC ffC%33C*33C/� C3�fC9� C>  CCffCG�fCR�C\ffCfffCp��CzL�C�@ C��C�ٚC�33C��fC�  C�@ C�&fC�&fC��fC�@ C�33C��C��3C�&fC�  C�33C��3C�&fC��C��fC�&fC�Y�C�&fC�33D  DfD3D�fD�fD3D   D$��D*fD.��D3�3D9fD>�DB�3DH  DL��DR  DW  D\�D`��DffDk�Do�fDt�3Dz�D�9�D��3D���D���D�P D��fD��3D�	�D�<�D���D�Y�D���D�9�DԐ Dڹ�D���D�<�D�|�D�ɚD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A+33Ay��A�  A�  A���BffB"ffB6  BI��BX  Bs��B���B���B���B�fgB�34B���B���B���B�fgB���B���B�fgB���CL�C33C33C��C� C33C �3C%� C*� C/��C433C9��C>L�CC�3CH33CRfgC\�3Cf�3Cp�gCz��C�ffC�33C�  C�Y�C��C�&fC�ffC�L�C�L�C��C�ffC�Y�C�@ C��C�L�C�&fC�Y�C��C�L�C�@ C��C�L�C�� C�L�C�Y�D33D�D&fD��D��D&fD 33D%  D*�D/  D4fD9�D>  DCfDH3DM�DR33DW3D\  D`� Df�Dk,�Do��Dt�fDz,�D�C4D���D��gD�gD�Y�D�� D���D�4D�FgD��gD�c4D�4D�C4Dԙ�D��4D�gD�FgD�gD��4D�41111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�+A���A�A�ĜA��HA���A���A���A��^A��FA��mA���A�oA�
=A���A���A��+A��`A���A��A���A�?}A���A���A��A��RA��7A�I�A�&�A��#A�;dA�
=A�n�A�?}A��A��A���A�z�A�bA��A��\A�\)A��A�A�$�A�XA��jA��DA�ĜA�n�A�%A�E�A��A��A~1AyC�Ap$�Al�/Af�Acp�A`��A_oA[p�AY�AX1AT=qAP�DAI�;AD��A7K�A0�A'%AG�A;dA�@�X@��@��@��@�33@���@�G�@��;@��@��^@��@��;@��-@�J@�9X@��-@~E�@{�
@uO�@g�@]@T�@G
=@=@7��@2��@.5?@'l�@ �u@@��@o@�@
^5@l�@�
@�#?��?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A�+A���A�A�ĜA��HA���A���A���A��^A��FA��mA���A�oA�
=A���A���A��+A��`A���A��A���A�?}A���A���A��A��RA��7A�I�A�&�A��#A�;dA�
=A�n�A�?}A��A��A���A�z�A�bA��A��\A�\)A��A�A�$�A�XA��jA��DA�ĜA�n�A�%A�E�A��A��A~1AyC�Ap$�Al�/Af�Acp�A`��A_oA[p�AY�AX1AT=qAP�DAI�;AD��A7K�A0�A'%AG�A;dA�@�X@��@��@��@�33@���@�G�@��;@��@��^@��@��;@��-@�J@�9X@��-@~E�@{�
@uO�@g�@]@T�@G
=@=@7��@2��@.5?@'l�@ �u@@��@o@�@
^5@l�@�
@�#?��?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B
uB
{B
�B
6FB
F�B
��B
ǮB
�wB
��B
�B
��B
�B
�9B
�dB
�qB
��B
��B
��B
��B
��B
��B
��B
��B
ŢB
��B
��B
�}B
�qB
�wB
�^B
�qB
�LB
�FB
�jB
ǮB
��B
��B
��B
��B
��B
�/B
�5B
�ZB
�;B
�)B
�/B
�ZB
�TB
�#B
��B
B
�B
��B
�uB
~�B
W
B
H�B
1'B
$�B
�B
{B
B	��B	��B	�B	��B	�9B	��B	_;B	H�B	hB�sB��B�DBm�B]/BG�BhBBhB(�BJ�B[#Bu�B�PB��B�^BŢB�;B�B	B	PB	�B	?}B	bNB	}�B	��B	�LB	��B	�B	�yB	�B
  B
JB
�B
%�B
2-B
=qB
B�B
M�B
Q�B
W
B
_;1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	��B
uB
{B
�B
6FB
F�B
��B
ǮB
�wB
��B
�B
��B
�B
�9B
�dB
�qB
��B
��B
��B
��B
��B
��B
��B
��B
ŢB
��B
��B
�}B
�qB
�wB
�^B
�qB
�LB
�FB
�jB
ǮB
��B
��B
��B
��B
��B
�/B
�5B
�ZB
�;B
�)B
�/B
�ZB
�TB
�#B
��B
B
�B
��B
�uB
~�B
W
B
H�B
1'B
$�B
�B
{B
B	��B	��B	�B	��B	�9B	��B	_;B	H�B	hB�sB��B�DBm�B]/BG�BhBBhB(�BJ�B[#Bu�B�PB��B�^BŢB�;B�B	B	PB	�B	?}B	bNB	}�B	��B	�LB	��B	�B	�yB	�B
  B
JB
�B
%�B
2-B
=qB
B�B
M�B
Q�B
W
B
_;1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090815095642  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090815095643  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090815095644  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090815095644  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090815095645  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090815095645  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090815095645  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090815095645  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090815095645  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090815100123                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090819005744  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090819005845  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090819005845  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090819005845  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090819005847  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090819005847  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090819005847  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090819005847  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090819005847  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090819010331                      G�O�G�O�G�O�                