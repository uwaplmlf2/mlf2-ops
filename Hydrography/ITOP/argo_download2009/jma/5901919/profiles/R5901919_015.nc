CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901919 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090825035757  20090829010313                                  2B  A   APEX-SBE 4054                                                   846 @�F|�41   @�F}3�ax@<�~��"�@a��G�{1   ARGOS   A   A   A   @�  A+33A�  A�33A�  A�B33B!33B6ffBHffB]��Bp��B���B���B�  B���B�  B�ffB�  B�  B�  B���B�33B���B�33C�3CL�C  C�fC  C�C L�C%ffC*� C.��C4  C9�C>ffCCffCG�3CR� C\ffCeffCo�fCx� C�  C�L�C�&fC��C��C��C���C�ٚC�&fC��3C���C�L�C�@ C��C��C�@ C�@ C�33C��C�33C��C��3C�ٚC��C�ٚDfD  D�DfD�3D3D �D%�D)��D/fD3� D9�D>3DC�DH  DM  DRfDW  D\�Da�DffDk�Do��Du�DzfD�C3D�� D���D�fD�L�D���D�ɚD� D�@ D���D���D�	�D�I�D�p D�� D�	�D�P D�p D��D�� D�s311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A0  A�ffA���A�ffA�  BffB"ffB7��BI��B^��Br  B�34B�34B���B�34B���B�  B���Bə�Bә�B�fgB���B�fgB���C  C��CL�C33CL�CfgC ��C%�3C*��C/�C4L�C9fgC>�3CC�3CH  CR��C\�3Ce�3Cp33Cx��C�&fC�s3C�L�C�33C�@ C�33C��3C�  C�L�C��C��3C�s3C�ffC�@ C�33C�ffC�ffC�Y�C�33C�Y�C�33C��C�  C�@ C�  D�D33D  D�DfD&fD   D%  D*  D/�D3�3D9,�D>&fDC,�DH33DM3DR�DW33D\  Da,�Df�Dk  Dp�Du  Dz�D�L�D���D��gD� D�VgD��4D��4D��D�I�D��gD��gD�4D�S4D�y�D�ٚD�4D�Y�D�y�D�gD�ٚD�|�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��mA��mA�9XA�ffA�dZA�oA���A�M�A��A���A��^A�  A�{A���A�A���A��A��A�jA�(�A�dZA��DA���A���A� �A�\)A�"�A�`BA��/A�A���A��A�;dA���A��-A���A���A��7A�1A��A�E�A�O�A�v�A���A�I�A���A���A�jA�dZA�&�A��HA�x�A��9A�jA�?}Az��Av(�Ap�jAj �Af�Ad�HAc��A`�jA[�
ASK�AQ\)AM�^AG/AD��A;�^A*��A"�HAjA"�A�^@���@��@�`B@�1'@�33@���@�n�@��\@�b@�C�@��h@���@�J@�`B@�E�@��!@}V@yX@uO�@ko@a&�@UO�@Lj@Dz�@<j@3@+dZ@&�+@$��@��@��@��@�+@
��@\)@�@M�?��?�X?�b11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A��mA��mA�9XA�ffA�dZA�oA���A�M�A��A���A��^A�  A�{A���A�A���A��A��A�jA�(�A�dZA��DA���A���A� �A�\)A�"�A�`BA��/A�A���A��A�;dA���A��-A���A���A��7A�1A��A�E�A�O�A�v�A���A�I�A���A���A�jA�dZA�&�A��HA�x�A��9A�jA�?}Az��Av(�Ap�jAj �Af�Ad�HAc��A`�jA[�
ASK�AQ\)AM�^AG/AD��A;�^A*��A"�HAjA"�A�^@���@��@�`B@�1'@�33@���@�n�@��\@�b@�C�@��h@���@�J@�`B@�E�@��!@}V@yX@uO�@ko@a&�@UO�@Lj@Dz�@<j@3@+dZ@&�+@$��@��@��@��@�+@
��@\)@�@M�?��?�X?�b11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	gmB	dZB	��B
T�B
hsB
}�B
��B
�!B
�?B
�FB
�XB
B
��B
��B
��B
��B
��B
��B
�B
�B
�B
�B
�/B
�)B
�B
�B
��B
��B
ŢB
��B
��B
��B
��B
ÖB
�qB
�}B
��B
��B
�/B
�5B
�mB
�BB
�B
�
B
��B
�5B
�ZB
�`B
�`B
�BB
�B
��B
B
�?B
��B
��B
�B
p�B
YB
=qB
0!B
'�B
!�B
uB
B	�)B	��B	�wB	��B	��B	q�B	,B	
=B�HB��BjBJ�B=qB0!B/B{BBhB6FBJ�B]/Bw�B��B�BĜB��B�sB	B	JB	�B	@�B	cTB	{�B	��B	�B	B	�B	�B	�B	��B
+B
�B
+B
2-B
<jB
F�B
L�B
S�B
YB
bNB
cT11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	gmB	dZB	��B
T�B
hsB
}�B
��B
�!B
�?B
�FB
�XB
B
��B
��B
��B
��B
��B
��B
�B
�B
�B
�B
�/B
�)B
�B
�B
��B
��B
ŢB
��B
��B
��B
��B
ÖB
�qB
�}B
��B
��B
�/B
�5B
�mB
�BB
�B
�
B
��B
�5B
�ZB
�`B
�`B
�BB
�B
��B
B
�?B
��B
��B
�B
p�B
YB
=qB
0!B
'�B
!�B
uB
B	�)B	��B	�wB	��B	��B	q�B	,B	
=B�HB��BjBJ�B=qB0!B/B{BBhB6FBJ�B]/Bw�B��B�BĜB��B�sB	B	JB	�B	@�B	cTB	{�B	��B	�B	B	�B	�B	�B	��B
+B
�B
+B
2-B
<jB
F�B
L�B
S�B
YB
bNB
cT11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090825035756  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090825035757  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090825035757  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090825035758  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090825035759  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090825035759  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090825035759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090825035759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090825035759  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090825040317                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090829005705  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090829005825  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090829005826  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090829005826  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090829005827  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090829005827  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090829005827  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090829005827  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090829005827  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090829010313                      G�O�G�O�G�O�                