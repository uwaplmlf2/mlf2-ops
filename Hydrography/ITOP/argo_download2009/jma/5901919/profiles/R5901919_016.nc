CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   k   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4H   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6`   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8x   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :$   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <<   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >T   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  @    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @l   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  B   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D0   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D`   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G`   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J`   CALIBRATION_DATE            	             
_FillValue                  ,  M`   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    M�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    M�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    M�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    M�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  M�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N    HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    NArgo profile    2.2 1.2 19500101000000  5901919 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090905155702  20091204091417                                  2B  A   APEX-SBE 4054                                                   846 @�I ��1   @�I�6�@<��t�j@a�Ƨ1   ARGOS   B   B   B   @�  A   Ad��A�33A�33A���B��B#��B4��BG33B^��Bq33B�33B���B���B�33B�  B�33B�33B�ffB�33B�ffB���B�33B���C� C33C33CL�C� C  C � C%  C)��G�O�CF  CR33C\�Cf��Cp  Cz�C��3C�@ C�33C�33C��3C�  C�  C���C�L�C�33C��C��3C�ٚC�&fC�@ C�&fC�@ C��C�33C�  C��3C�33C�  C��C�&fDfD� D��D�D  DfD fD$� D)�3D/3D43D9  D=��DB��DH  DL��DQ� DW�D\�D`��Df  Dj�fDpfDt��DzfD�C3D�y�D�ɚD���D�I�D�vfD��3G�O�D�I�D�|�D��3D�fD�C3D��D�� D�` 11111111111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111411111111 @���A$��Ai��A���A͙�A�33B��B$��B6  BHffB`  BrffB���B�fgB�fgB���B���B���B���B�  B���B�  B�fgB���B�34C��C� C� C��C��CL�C ��C%L�C)�gG�O�CFL�CR� C\fgCf�gCpL�CzfgC��C�ffC�Y�C�Y�C��C�&fC�&fC��3C�s3C�Y�C�@ C��C�  C�L�C�ffC�L�C�ffC�33C�Y�C�&fC��C�Y�C�&fC�33C�L�D�D�3D�D,�D33D�D �D$�3D*fD/&fD4&fD93D>  DC  DH3DM  DQ�3DW  D\,�Da�Df33Dj��Dp�Du  Dz�D�L�D��4D��4D�gD�S4D�� D���G�O�D�S4DԆgD���D� D�L�D�gD�ɚD�i�11111111111111111111111111111111114111111111111111111111111111111111111111111111111111111111111111411111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A�oA�JA�1A��
A�33A�^5A��A���A��7A��A�ffA�Q�A��A�O�A��PA���A�VA�G�A�~�A��-A�?}A���A�O�A�C�A�XA���A��A��FA�VA��A�{G�O�A���A��A��A�=qA���A���A��DA��^A���A��A��/A���A���A�S�A%Az��As��Ap�Al�`Ah�uAd�`A_�A[/AY+AQ��AMXAE��AC�A>��A:ffA4�A-�7A��A�7A+@��R@���@��/@�bN@�J@�z�@�9X@���@�;d@�9X@�^5@��@��@�o@�M�@��@�5?@�C�@;d@y7L@t�D@q&�@j~�@Z��@PĜ@G�w@@�u@:~�G�O�@^5@1@K�@
��@1'@�j@��?��?�~�11111111111111111111111111111111194111111111111111111111111111111111111111111111111111111111111119411111111 A��A��A�oA�JA�1A��
A�33A�^5A��A���A��7A��A�ffA�Q�A��A�O�A��PA���A�VA�G�A�~�A��-A�?}A���A�O�A�C�A�XA���A��A��FA�VA��A�{G�O�A���A��A��A�=qA���A���A��DA��^A���A��A��/A���A���A�S�A%Az��As��Ap�Al�`Ah�uAd�`A_�A[/AY+AQ��AMXAE��AC�A>��A:ffA4�A-�7A��A�7A+@��R@���@��/@�bN@�J@�z�@�9X@���@�;d@�9X@�^5@��@��@�o@�M�@��@�5?@�C�@;d@y7L@t�D@q&�@j~�@Z��@PĜ@G�w@@�u@:~�G�O�@^5@1@K�@
��@1'@�j@��?��?�~�11111111111111111111111111111111194111111111111111111111111111111111111111111111111111111111111119411111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	bNB	dZB	dZB	iyB	jB	s�B	e`B
R�B
�B
�bB
��B
��B
��B
��B
�B
�9B
�qB
�wB
B
ÖB
ÖB
��B
��B
��B
��B
��B
��B
�)B
��B
��B
ƨB
�#B
�TB
�TG�O�B
�fB
�B
�B
��B
�B
�B
�B
�sB
�BB
��B
�jB
�B
��B
��B
�B
ffB
ZB
F�B
49B
$�B
	7B	��B	�B	�
B	�wB	��B	�{B	�B	k�B	N�B	2-B�BB�wB�DBgmBO�BT�B.BoB	7B\B"�B>wBZBffBr�B�1B��B�B�9B�B�fB��B	VB	�B	.B	@�B	bNB	�B	��B	�FB	ɺG�O�B
�B
&�B
49B
=qB
C�B
L�B
T�B
[#B
aH11111111111111111111111111111111114111111111111111111111111111111111111111111111111111111111111119411111111 B	bNB	dZB	dZB	iyB	jB	s�B	e`B
R�B
�B
�bB
��B
��B
��B
��B
�B
�9B
�qB
�wB
B
ÖB
ÖB
��B
��B
��B
��B
��B
��B
�)B
��B
��B
ƨB
�#B
�TB
�TG�O�B
�fB
�B
�B
��B
�B
�B
�B
�sB
�BB
��B
�jB
�B
��B
��B
�B
ffB
ZB
F�B
49B
$�B
	7B	��B	�B	�
B	�wB	��B	�{B	�B	k�B	N�B	2-B�BB�wB�DBgmBO�BT�B.BoB	7B\B"�B>wBZBffBr�B�1B��B�B�9B�B�fB��B	VB	�B	.B	@�B	bNB	�B	��B	�FB	ɺG�O�B
�B
&�B
49B
=qB
C�B
L�B
T�B
[#B
aH11111111111111111111111111111111114111111111111111111111111111111111111111111111111111111111111119411111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090905155701  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090905155702  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090905155702  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090905155703  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090905155704  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090905155704  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090905155704  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090905155704  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090905155704  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090905155704  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090905155704  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090905160152                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090908005708  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090908005827  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090908005827  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090908005828  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090908005829  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090908005829  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090908005829  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090908005829  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090908005829  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090908005829  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090908005829  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090908010230                      G�O�G�O�G�O�                JA  ARFMdecpA14d                                                                20091204075838  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204075924  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204075924  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204075925  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204075926  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091204075926  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204075926  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204075926  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204075926  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204075926  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204075926  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091204091417                      G�O�G�O�G�O�                