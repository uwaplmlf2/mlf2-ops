CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901919 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090914035820  20090917220329                                  2B  A   APEX-SBE 4054                                                   846 @�K{�JU�1   @�K{��-�@<�-@a�dZ�1   ARGOS   A   A   A   @���A#33A{33A���Aə�A���B  B"  B533BI��B^  BrffB�  B���B�  B�  B���B�  B���Bř�B�ffB�33B�ffB���B���C�C��C  CL�C��C� C   C$��C*33C.�fC3��C9ffC>ffCC33CG�fCQ�3C\��CfffCp�Cz�C��fC��3C�  C��C�ٚC��3C��C�  C�&fC�&fC��C�L�C�33C��C�&fC��C�&fC�@ C��fC�  C�&fC�@ C��C�@ C��D  D  D� D�fD3D� D fD%  D*fD/fD3�3D93D=��DC�DH�DM�DR  DV��D[ٚDafDffDj��Do��Dt��Dz3D�FfD�y�D��fD�  D�C3D���D�� D�3D�@ D�|�D��3D� D�6fDԌ�D��fD� D�@ D�fD��D��fD�ff11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�fgA)��A���A�  A���A�  B��B#��B6��BK34B_��Bt  B���B�fgB���B���B���B���B�fgB�fgB�33B�  B�33B�B���C� C33CffC�3C33C�fC ffC%  C*��C/L�C4  C9��C>��CC��CHL�CR�C]  Cf��Cp� Cz� C��C�&fC�33C�@ C��C�&fC�L�C�33C�Y�C�Y�C�L�C�� C�ffC�L�C�Y�C�@ C�Y�C�s3C��C�33C�Y�C�s3C�@ C�s3C�L�D�D�D��D  D,�D��D   D%9�D*  D/  D4�D9,�D>4DC34DH&gDM34DR�DWgD[�4Da  Df  Dk4DpgDu4Dz,�D�S3D��gD��3D��D�P D��gD���D� D�L�D���D�� D��D�C3Dԙ�D��3D��D�L�D�3D�ɚD��3D�s311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A��A�7A�C�A��A�|�A�=qA�v�A��A�oA�$�A��A�G�A�p�A��A��TA�^5A�"�A�O�A� �A�-A��#A��hA�7LA�A�(�A��
A�|�A�I�A�r�A���A�\)A��TA���A�t�A�K�A�$�A�A��HA�$�A�\)A�1'A�"�A�9XA�hsA��DA��PA�hsA�{A��FA�C�A���A��7A���A�A|ffAx5?As%Ao��AeA`$�A[�-AV��ASƨAJ�AC��A?��A6ĜA0bA)XA�mA�\Av�AM�@�;d@���@ҏ\@ģ�@�{@���@�t�@�(�@��@��y@���@�Q�@�\)@��P@�K�@�p�@y7L@t�D@m��@j^5@`  @Wl�@Ol�@Ix�@A&�@7�@0��@)��@&ȴ@"M�@�\@O�@-@
=@
M�@ff@S�@   ?���?��+?��T11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��A��A��A�7A�C�A��A�|�A�=qA�v�A��A�oA�$�A��A�G�A�p�A��A��TA�^5A�"�A�O�A� �A�-A��#A��hA�7LA�A�(�A��
A�|�A�I�A�r�A���A�\)A��TA���A�t�A�K�A�$�A�A��HA�$�A�\)A�1'A�"�A�9XA�hsA��DA��PA�hsA�{A��FA�C�A���A��7A���A�A|ffAx5?As%Ao��AeA`$�A[�-AV��ASƨAJ�AC��A?��A6ĜA0bA)XA�mA�\Av�AM�@�;d@���@ҏ\@ģ�@�{@���@�t�@�(�@��@��y@���@�Q�@�\)@��P@�K�@�p�@y7L@t�D@m��@j^5@`  @Wl�@Ol�@Ix�@A&�@7�@0��@)��@&ȴ@"M�@�\@O�@-@
=@
M�@ff@S�@   ?���?��+?��T11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B��B	  B	B	o�B	o�B	k�B

=B
aHB
{�B
�=B
��B
��B
��B
�3B
�wB
��B
��B
��B
��B
��B
�B
�B
�B
�#B
�#B
�sB
�NB
�
B
��B
��B
��B
ɺB
B
��B
��B
�wB
�jB
��B
B
ŢB
��B
�TB
�mB
�TB
�HB
�mB
�fB
�sB
�TB
�B
��B
ĜB
��B
�qB
��B
��B
�VB
{�B
gmB
S�B
+B
uB
B	�B	�)B	�3B	��B	�B	_;B	G�B	(�B��B�
B�?Bu�BP�BB�B0!B �B�B�B1'B>wBT�By�B�\B��B�XB��B�ZB�B	1B	�B	)�B	33B	R�B	n�B	�%B	��B	�?B	��B	�5B	�B	��B
1B
�B
!�B
)�B
1'B
=qB
H�B
P�B
ZB
aHB
ffB
ff11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B��B	  B	B	o�B	o�B	k�B

=B
aHB
{�B
�=B
��B
��B
��B
�3B
�wB
��B
��B
��B
��B
��B
�B
�B
�B
�#B
�#B
�sB
�NB
�
B
��B
��B
��B
ɺB
B
��B
��B
�wB
�jB
��B
B
ŢB
��B
�TB
�mB
�TB
�HB
�mB
�fB
�sB
�TB
�B
��B
ĜB
��B
�qB
��B
��B
�VB
{�B
gmB
S�B
+B
uB
B	�B	�)B	�3B	��B	�B	_;B	G�B	(�B��B�
B�?Bu�BP�BB�B0!B �B�B�B1'B>wBT�By�B�\B��B�XB��B�ZB�B	1B	�B	)�B	33B	R�B	n�B	�%B	��B	�?B	��B	�5B	�B	��B
1B
�B
!�B
)�B
1'B
=qB
H�B
P�B
ZB
aHB
ffB
ff11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090914035819  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090914035820  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090914035820  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090914035821  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090914035822  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090914035822  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090914035822  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090914035822  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090914035822  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090914040400                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090917215744  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090917215846  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090917215846  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090917215847  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090917215848  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090917215848  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090917215848  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090917215848  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090917215848  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090917220329                      G�O�G�O�G�O�                