CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901919 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091004035706  20091007220220                                  2B  A   APEX-SBE 4054                                                   846 @�Pz    1   @�PzA;�@=n��P@a�(�\1   ARGOS   A   A   A   @�  A+33At��A�  A�  A�  B33B ��B4��B@ffB]��Bp  B�ffB�  B�33B���B�ffB���B���B�33B�  B���B�ffB���B�ffC� C33C� C� C33CL�C   C$�3C*ffC/  C3�3C933C=��CC� CH33CQ��CZ� Cf�3CpffCzffC��C�&fC��3C�&fC�  C��C�� C���C�33C�  C�33C��C�33C�  C��C��C�33C�  C�@ C�&fC��3C���C�L�C�&fC��D�D3D��D  D�D�D �D$�3D*  D/  D4  D9�D>  DCfDHfDMfDRfDV�fD[y�Da3De�3Dj�3Dp�Dt��DzfD�@ D�� D���D�3D�C3D���D��fD�	�D�L�D��fD�ɚD��D�6fD�s3D�� D�3D�<�D�vfD�3D��3D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A1��A{33A�33A�33A�33B��B"fgB6fgBB  B_34Bq��B�33B���B�  B�fgB�33B�fgB�fgB�  B���Bؙ�B�33B�B�33C�fC��C�fC�fC��C�3C ffC%�C*��C/ffC4�C9��C>33CC�fCH��CR33CZ�fCg�Cp��Cz��C�L�C�Y�C�&fC�Y�C�33C�@ C��3C�  C�ffC�33C�ffC�L�C�ffC�33C�@ C�L�C�ffC�33C�s3C�Y�C�&fC�  C�� C�Y�C�@ D&gD,�DgD�D&gD34D &gD%�D*�D/�D4�D9&gD>9�DC  DH  DM  DR  DW  D[�4Da,�Df�Dk�Dp34DugDz  D�L�D���D�ɚD� D�P D��gD��3D�gD�Y�D��3D��gD��D�C3DԀ D���D� D�I�D�3D�� D�  D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��Aѝ�A�E�A��A���A��;A�ffA�E�Aĉ7A���A��A�I�A�%A��\A�  A�M�A�K�A���A��yA�&�A�jA���A���A��A�oA���A���A�Q�A��wA��A�bNA� �A�S�A��FA�O�A�v�A��9A��DA���A��A�1'A��DA���A��jA��7A��jA���A���A��A�1A�VA~ZAx-Ar��Ak�mAi�7Ag�;Af�uAd�+Aa��A_
=AX�AW��ATffAQ��ANE�AK;dAE
=A<��A4bNA+�A�AXA	�A�/@�S�@�@Ձ@�z�@��F@�ƨ@��D@�j@�  @�`B@�V@�O�@��u@�A�@�z�@���@�w@y�^@sƨ@p�9@h��@XbN@P�@G��@C@<(�@6V@/;d@)x�@&�+@ r�@�@�y@z�@��@(�@	��@p�@��?�V?��m11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A��Aѝ�A�E�A��A���A��;A�ffA�E�Aĉ7A���A��A�I�A�%A��\A�  A�M�A�K�A���A��yA�&�A�jA���A���A��A�oA���A���A�Q�A��wA��A�bNA� �A�S�A��FA�O�A�v�A��9A��DA���A��A�1'A��DA���A��jA��7A��jA���A���A��A�1A�VA~ZAx-Ar��Ak�mAi�7Ag�;Af�uAd�+Aa��A_
=AX�AW��ATffAQ��ANE�AK;dAE
=A<��A4bNA+�A�AXA	�A�/@�S�@�@Ձ@�z�@��F@�ƨ@��D@�j@�  @�`B@�V@�O�@��u@�A�@�z�@���@�w@y�^@sƨ@p�9@h��@XbN@P�@G��@C@<(�@6V@/;d@)x�@&�+@ r�@�@�y@z�@��@(�@	��@p�@��?�V?��m11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	ƨB	�?B	�B	�wB	��B	ǮB	�;B
oB
�B
"�B
5?B
6FB
�B
&�B
9XB
<jB
1'B
�hB
�XB
��B
��B
��B
��B
��B
�)B
�BB
�BB
�BB
�HB
�HB
�HB
�NB
�TB
�fB
�B
�B
�B
�B
�B
�B
�B
�B
�sB
�TB
�;B
�B
��B
ɺB
�RB
��B
��B
� B
hsB
H�B
?}B
8RB
2-B
'�B
�B
bB	��B	�B	�TB	�B	ǮB	�XB	��B	w�B	P�B	0!B	B��B��B�PBu�B_;BH�BA�B=qB:^B=qB@�BM�Bu�B�PB��B�qB��B�TB��B��B	DB	�B	&�B	?}B	l�B	�%B	��B	�B	B	��B	�sB	�B	��B
+B
�B
"�B
)�B
8RB
?}B
D�B
O�B
W
B
^5B
`B11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	��B	ƨB	�?B	�B	�wB	��B	ǮB	�;B
oB
�B
"�B
5?B
6FB
�B
&�B
9XB
<jB
1'B
�hB
�XB
��B
��B
��B
��B
��B
�)B
�BB
�BB
�BB
�HB
�HB
�HB
�NB
�TB
�fB
�B
�B
�B
�B
�B
�B
�B
�B
�sB
�TB
�;B
�B
��B
ɺB
�RB
��B
��B
� B
hsB
H�B
?}B
8RB
2-B
'�B
�B
bB	��B	�B	�TB	�B	ǮB	�XB	��B	w�B	P�B	0!B	B��B��B�PBu�B_;BH�BA�B=qB:^B=qB@�BM�Bu�B�PB��B�qB��B�TB��B��B	DB	�B	&�B	?}B	l�B	�%B	��B	�B	B	��B	�sB	�B	��B
+B
�B
"�B
)�B
8RB
?}B
D�B
O�B
W
B
^5B
`B11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20091004035705  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091004035706  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091004035706  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091004035706  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091004035708  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091004035708  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091004035708  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091004035708  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091004035708  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091004040133                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091007215723  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091007215824  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091007215825  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091007215825  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091007215826  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091007215826  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091007215826  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091007215826  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091007215826  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091007220220                      G�O�G�O�G�O�                