CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901521 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               \A   JA  20090826035617  20090829160317                                  2B  A   APEX-SBE 2807                                                   846 @�F�z��1   @�F�lGL�@3��Q�@`�����1   ARGOS   A   A   A   @�ffAffAh  A���A���A�33B
  B��B2  BG33BZ  Bn  B�  B�  B�33B�33B�  B���B�33B�  BЙ�B���B䙚B���B�ffCffC33C�CffC� C��C� C$L�C)33C.ffC3� C833C=L�CB  CG33CQL�C[33Ce� Co� Cy� C��3C���C���C���C���C���C��3C��fC��3C��fC���C���C�� C CǙ�C̳3CѦfC���C���C�� C�fC�fCC��3C��fD��DٚDٚD� D� D��D�fD$�3D)�3D.�fD3ٚD8�fD=� DB�fDG� DL��DQ�fDVٚD[ٚD`� De��DjٚDo� Dt��Dy�fD�0 D�i�D���D���D�0 D�` D���D�ٚD�,�D�i�D��fD��fD�&fD�p Dک�D���D�#3D�Y�D�D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��AfffA���A�  A�ffB	��BfgB1��BF��BY��Bm��B���B���B�  B�  B���B�fgB�  B���B�fgBڙ�B�fgBB�33CL�C�C  CL�CffC� CffC$33C)�C.L�C3ffC8�C=33CA�fCG�CQ33C[�CeffCoffCyffC��fC���C���C�� C���C�� C��fC���C��fC���C���C�� C��3C�s3Cǌ�C̦fCљ�C�� C�� C�3C噙CꙙC��C��fC���D�4D�4D�4D��D��D�4D� D$��D)��D.� D3�4D8� D=ٚDB� DG��DL�gDQ� DV�4D[�4D`ٚDe�gDj�4Do��Dt�gDy� D�,�D�fgD���D�ٚD�,�D�\�D��gD��gD�)�D�fgD��3D��3D�#3D�l�DڦgD�ٚD�  D�VgD�gD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�r�A�jA�
=AꙚAꗍA�-A�uA�A�I�A؏\A��A�jA��A���AāA��A��A�\)A��HA���A�`BA�&�A��#A�z�A���A���A�ƨA���A��9A�$�A�C�A�;dA�l�A���A���A��A��^A��A�z�A���A��FA�33A�A�A�?}A�JA�A{�Av�yAn^5Ait�Aa`BA[�;ATA�AM?}AHA�AB�RA?%A=\)A4�`A-"�A(1A%|�A�+AK�A=qAbA��AQ�A�7@�|�@��;@�33@��@ՙ�@υ@�t�@��@��\@��@��^@��@�Q�@��@��`@�\)@���@� �@���@���@��
@�p�@���@��u@xbN@p��@f�+@^$�@W�@N5?@D��@?|�@6ȴ@1�@*�!@%��@"M�@5?@�@��@��@r�@��@	�#@v�@ƨ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�r�A�jA�
=AꙚAꗍA�-A�uA�A�I�A؏\A��A�jA��A���AāA��A��A�\)A��HA���A�`BA�&�A��#A�z�A���A���A�ƨA���A��9A�$�A�C�A�;dA�l�A���A���A��A��^A��A�z�A���A��FA�33A�A�A�?}A�JA�A{�Av�yAn^5Ait�Aa`BA[�;ATA�AM?}AHA�AB�RA?%A=\)A4�`A-"�A(1A%|�A�+AK�A=qAbA��AQ�A�7@�|�@��;@�33@��@ՙ�@υ@�t�@��@��\@��@��^@��@�Q�@��@��`@�\)@���@� �@���@���@��
@�p�@���@��u@xbN@p��@f�+@^$�@W�@N5?@D��@?|�@6ȴ@1�@*�!@%��@"M�@5?@�@��@��@r�@��@	�#@v�@ƨ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�BjBo�B��BɺB��B	�B	8RB	R�B	��B
8RB
jB
��B
�dB
�B
��B	7BPB&�BF�B<jBm�B�1B�PB�\B��B��B�B��B��B��B��B��B��B�uB�%Bs�BjBT�BH�B;dB$�BPB
��B
�B
�3B
�bB
~�B
e`B
<jB
0!B
VB	�B	��B	�B	��B	}�B	m�B	aHB	?}B	'�B	VB	B�TB�B��BƨB�qB�RB�!B��B�B�jB�FB��B�B�B�B�B	1B	�B	(�B	>wB	J�B	Q�B	ZB	jB	�\B	��B	��B	�'B	�qB	ƨB	��B	��B	�BB	�B
JB
�B
!�B
,B
1'B
:^B
B�B
L�B
R�B
ZB
`BB
e`B
jB
o�B
t�B
{�B
� B
�B
�+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 BjBo�B��BɺB��B	�B	8RB	R�B	��B
8RB
jB
��B
�dB
�B
��B	7BPB&�BF�B<jBm�B�1B�PB�\B��B��B�B��B��B��B��B��B��B�uB�%Bs�BjBT�BH�B;dB$�BPB
��B
�B
�3B
�bB
~�B
e`B
<jB
0!B
VB	�B	��B	�B	��B	}�B	m�B	aHB	?}B	'�B	VB	B�TB�B��BƨB�qB�RB�!B��B�B�jB�FB��B�B�B�B�B	1B	�B	(�B	>wB	J�B	Q�B	ZB	jB	�\B	��B	��B	�'B	�qB	ƨB	��B	��B	�BB	�B
JB
�B
!�B
,B
1'B
:^B
B�B
L�B
R�B
ZB
`BB
e`B
jB
o�B
t�B
{�B
� B
�B
�+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090826035615  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090826035617  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090826035617  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090826035618  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090826035618  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090826035619  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090826035619  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090826035619  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090826035619  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090826035619  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090826040244                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090829155631  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090829155904  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090829155904  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090829155904  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090829155904  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090829155905  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090829155905  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090829155906  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090829155906  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090829155906  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090829160317                      G�O�G�O�G�O�                