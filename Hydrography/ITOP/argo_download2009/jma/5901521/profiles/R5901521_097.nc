CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   f   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  44   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  64   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  84   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  9�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :4   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ;�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <4   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ?d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  Ad   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  Cd   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    C�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    F�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    I�   CALIBRATION_DATE            	             
_FillValue                  ,  L�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    L�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    L�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    L�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    L�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  L�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M    HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M$   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M4   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M8   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M<   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M@Argo profile    2.2 1.2 19500101000000  5901521 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               aA   JA  20091019005828  20091208062819                                  2B  A   APEX-SBE 2807                                                   846 @�S+n�c^1   @�S+��I�@3ffffff@`��\(��1   ARGOS   B   B   B   A�ffA���B��B33B2ffG�O�B�  B�33B���B���B�ffB�  B�ffB���B�  B���B�33C�C� C� C  C�fC�fC  C$ffC)ffC.L�C333C8��C=L�CB�CG��CQ33C[�CeffCoffCyffC�� C��fC��fC��fC�s3C��3C��3C���C��3C���C�� C���C��fC¦fCǙ�Č�Cь�C֙�Cۀ C���C��C�fC�ffC���C���DٚD�fD��D�3DٚD�fD�3D$ٚD)�fD.ٚD3�fD8�3D=� DB� DG�fDL�fDj�3Do�fDt� Dy�fD�  D�l�D��3D�ٚD�#3D�i�D��fD�� D�  D�i�D���D��fD��D�l�Dڠ D��3D�  D�ffD� D��f111114111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�ffA���B��B33B2ffG�O�B�  B�33B���B���B�ffB�  B�ffB���B�  B���B�33C�C� C� C  C�fC�fC  C$ffC)ffC.L�C333C8��C=L�CB�CG��CQ33C[�CeffCoffCyffC�� C��fC��fC��fC�s3C��3C��3C���C��3C���C�� C���C��fC¦fCǙ�Č�Cь�C֙�Cۀ C���C��C�fC�ffC���C���DٚD�fD��D�3DٚD�fD�3D$ٚD)�fD.ٚD3�fD8�3D=� DB� DG�fDL�fDj�3Do�fDt� Dy�fD�  D�l�D��3D�ٚD�#3D�i�D��fD�� D�  D�i�D���D��fD��D�l�Dڠ D��3D�  D�ffD� D��f111114111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�wA�+A�hsA�n�G�O�A���A�K�A�ffA��A���A£�A�S�A�
=A�jA��TA��hA��A�A�r�A���A�ȴA�n�A��yA��!A��7A���A�n�A���A���A�t�A�ffA�VA�r�A���A��A��DA�VA�7LA��mA��
A�z�A�XA�;AydZAp�uAl�`Aj�AeƨAap�A]��AZ��AU?}AN�!AK��AD�A?
=A9�FA4�jA-?}A%&�A!�A�TAE�AjAl�@�F@���@�ƨ@�V@��
@��@��+@�O�@�r�@�bN@�1@�@�E�@�~�@���@|�/@wK�@ko@]��@SS�@K�F@F@?��@;@5V@/l�@)��@%`B@!hs@@�#@�@`B@�@��@ƨ@��111194111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�wA�+A�hsA�n�G�O�A���A�K�A�ffA��A���A£�A�S�A�
=A�jA��TA��hA��A�A�r�A���A�ȴA�n�A��yA��!A��7A���A�n�A���A���A�t�A�ffA�VA�r�A���A��A��DA�VA�7LA��mA��
A�z�A�XA�;AydZAp�uAl�`Aj�AeƨAap�A]��AZ��AU?}AN�!AK��AD�A?
=A9�FA4�jA-?}A%&�A!�A�TAE�AjAl�@�F@���@�ƨ@�V@��
@��@��+@�O�@�r�@�bN@�1@�@�E�@�~�@���@|�/@wK�@ko@]��@SS�@K�F@F@?��@;@5V@/l�@)��@%`B@!hs@@�#@�@`B@�@��@ƨ@��111194111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	'�B	/B	P�B	�VB	�G�O�BBBdZB�uBǮB�dB�3B�LB�}BȴBŢB�9B��B��B��B��B�{B�JB�1B�B|�Bx�Bs�Bn�BjBhsBaHBT�B9XB,B$�BVB
��B
�B
�B
�jB
��B
v�B
T�B
F�B
8RB
�B
	7B	�B	�`B	��B	�B	��B	� B	gmB	VB	?}B	"�B	+B��B�B��B�^B�!B�RB�9B�9B�^BƨB�yB	B	uB	"�B	6FB	H�B	XB	k�B	�^B	ǮB	��B	�/B	�B
B
DB
�B
(�B
(�B
0!B
8RB
B�B
J�B
Q�B
YB
^5B
dZB
iyB
n�B
s�B
w�B
{�B
� 111114111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B	'�B	/B	P�B	�VB	�G�O�BBBdZB�uBǮB�dB�3B�LB�}BȴBŢB�9B��B��B��B��B�{B�JB�1B�B|�Bx�Bs�Bn�BjBhsBaHBT�B9XB,B$�BVB
��B
�B
�B
�jB
��B
v�B
T�B
F�B
8RB
�B
	7B	�B	�`B	��B	�B	��B	� B	gmB	VB	?}B	"�B	+B��B�B��B�^B�!B�RB�9B�9B�^BƨB�yB	B	uB	"�B	6FB	H�B	XB	k�B	�^B	ǮB	��B	�/B	�B
B
DB
�B
(�B
(�B
0!B
8RB
B�B
J�B
Q�B
YB
^5B
dZB
iyB
n�B
s�B
w�B
{�B
� 111114111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091018155601  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091019005828  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091019005829  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091019005829  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091019005829  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091019005830  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091019005830  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091019005830  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091019005830  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091019005830  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20091019005830  QCF$                G�O�G�O�G�O�            4100JA  ARGQaqcp2.8b                                                                20091019005830  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091019005830  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20091019005830  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091019010126                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208054428  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208054611  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208054611  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091208054611  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208054611  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208054613  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091208054613  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208054613  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208054613  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208054613  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208054613  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208054613  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208062819                      G�O�G�O�G�O�                