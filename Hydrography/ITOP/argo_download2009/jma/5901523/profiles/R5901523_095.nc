CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901523 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               _A   JA  20090925215615  20090929100249                                  2B  A   APEX-SBE 2808                                                   846 @�NU��[�1   @�NU�%��@3�j~��#@`{�E���1   ARGOS   A   A   A   @���A��A`  A�33A�  A���B
��B��B0��BDffBY33BnffB�33B���B�33B���B�  B�33B�33Bƙ�B�  B�  B���B�ffB�ffC ��CL�CffC33CffCffC��C$ffC)L�C.ffC3ffC8�C=33CB�CG33CQ33CZ��CeffCoL�Cy��C�� C��3C���C���C��3C�s3C���C���C�� C���C���C��3C�� C�Cǌ�C�� Cр C�� C۳3C�fC�fCꙚC�� C��C���D��D�fD� D��D�fD��D�fD$�3D)� D.�3D3�3D8�fD=ٚDB��DG��DL��DQ��DV�3D[�3D`�fDe� DjٚDo� Dt�fDy��D�33D�c3D�� D��3D�#3D�l�D���D��fD��D�c3D��fD��3D�0 D�p Dک�D�ٚD��D�` D�fD�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  @���AQ��A�  A���A噚B33B33B-33B@��BU��Bj��B~��B���B�ffB���B�33B�ffB�ffB���B�33B�33B�  B왙B���B���CfgC
� CL�C� C� C�4C#� C(fgC-� C2� C734C<L�CA34CFL�CPL�CY�gCd� CnfgCx�4C�L�C�@ C�&gC��C�@ C�  C��C�Y�C�L�C�&gC��C�@ C�L�C��C��C�L�C��C�L�C�@ C�33C�33C�&gC�L�C��C�&gD�3D��D�fD�3D��D�3D��D$��D)�fD.��D3��D8��D=� DB�3DG� DLs3DQ�3DV��D[��D`��De�fDj� Do�fDt��Dy�3D�fD�FfD��3D��fD�fD�P D�� D�əD�  D�FfD���D��fD�3D�S3Dڌ�D��D���D�C3D�y�D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�%A�
=A�
=A�%A���A��yA�7LA�5?A�"�A�p�A��A�33A��`A��^A�t�A���A�Q�A�|�A�bA��A�G�A��/A�&�A��PA�A�A��!A�  A���A��A�`BA�z�A��A��yA���A�G�A�1'A��A�VA�jA�K�A��uA���A�A�
=A���A��A�+A�;dA�A{�wAv9XAsO�Al=qAe�^Ac;dA\�uAW/AP$�AJ �ADv�A?t�A9;dA3�FA,�A((�A&z�A#hsAG�A��A5?A��A��@�v�@�v�@��@�M�@�^5@�33@���@��D@�"�@�@��j@���@��!@�S�@���@�{@���@���@��@��@��@��@{ƨ@jn�@`1'@U�@J�@@Ĝ@9��@4z�@/K�@+�F@%p�@�;@��@  @O�@�#@�y@
�H@A�@`B@^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�%A�
=A�
=A�%A���A��yA�7LA�5?A�"�A�p�A��A�33A��`A��^A�t�A���A�Q�A�|�A�bA��A�G�A��/A�&�A��PA�A�A��!A�  A���A��A�`BA�z�A��A��yA���A�G�A�1'A��A�VA�jA�K�A��uA���A�A�
=A���A��A�+A�;dA�A{�wAv9XAsO�Al=qAe�^Ac;dA\�uAW/AP$�AJ �ADv�A?t�A9;dA3�FA,�A((�A&z�A#hsAG�A��A5?A��A��@�v�@�v�@��@�M�@�^5@�33@���@��D@�"�@�@��j@���@��!@�S�@���@�{@���@���@��@��@��@��@{ƨ@jn�@`1'@U�@J�@@Ĝ@9��@4z�@/K�@+�F@%p�@�;@��@  @O�@�#@�y@
�H@A�@`B@^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	,B	-B	-B	-B	.B	.B	@�B	��B	�;B	�B
�B
�^B
�}B
ǮB
��B
�NB
�B
�B
�B
��BbB�B]/BK�B?}B7LB7LB33B33B7LBffBaHB[#BbNBbNBe`Be`BdZBaHB[#BQ�B?}B5?B �B
��B
�B
ǮB
�LB
��B
�DB
p�B
bNB
@�B
"�B
uB	��B	�)B	�wB	��B	�B	n�B	T�B	;dB	�B	oB	
=B	  B�B�HB��B�qB�3B��B��B��B��B�BÖB�BB��B	JB	$�B	8RB	C�B	\)B	s�B	y�B	�B	�uB	��B	�'B	�qB	ɺB	��B	�B	�yB	��B
JB
�B
'�B
2-B
;dB
D�B
I�B
S�B
]/B
dZB
iyB
l�B
p�B
s�B
x�B
|�B
�B
�%1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	,B	-B	-B	-B	.B	.B	@�B	��B	�;B	�B
�B
�^B
�}B
ǮB
��B
�NB
�B
�B
�B
��BbB�B]/BK�B?}B7LB7LB33B33B7LBffBaHB[#BbNBbNBe`Be`BdZBaHB[#BQ�B?}B5?B �B
��B
�B
ǮB
�LB
��B
�DB
p�B
bNB
@�B
"�B
uB	��B	�)B	�wB	��B	�B	n�B	T�B	;dB	�B	oB	
=B	  B�B�HB��B�qB�3B��B��B��B��B�BÖB�BB��B	JB	$�B	8RB	C�B	\)B	s�B	y�B	�B	�uB	��B	�'B	�qB	ɺB	��B	�B	�yB	��B
JB
�B
'�B
2-B
;dB
D�B
I�B
S�B
]/B
dZB
iyB
l�B
p�B
s�B
x�B
|�B
�B
�%1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090925215614  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090925215615  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090925215615  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090925215616  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090925215617  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090925215617  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090925215617  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090925215617  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090925215617  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090925220218                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090929095601  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090929095851  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090929095852  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090929095852  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090929095853  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090929095853  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090929095853  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090929095853  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090929095853  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090929100249                      G�O�G�O�G�O�                