CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901523 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               [A   JA  20090816215512  20090820102043                                  2B  A   APEX-SBE 2808                                                   846 @�DT%*�61   @�DW>�t�@3�ě��T@`����o1   ARGOS   A   A   A   @�  A��A`  A�ffA���A홚B	��B33B1��BG33BZffBm��B���B�  B�ffB���B���B�ffB�ffB�ffB�33B���B���B���B���CffC�C�C�fCffC��CffC$L�C)33C.ffC3�C8�C=ffCBL�CG�CQL�C[ffCe�Co33Cy� C���C���C��fC���C��fC���C���C�� C�ffC��3C�� C���C��fC�� C�� C̳3Cѳ3C֙�C۳3C���C�fCꙚC�3C��fC�� D� D� DٚD�fD�fD�3D� D$ٚD)��D.�3D3��D8�3D=�fDB� DG� DL��DQ��DV� D[� D`�fDe��Dj�fDo�3Dt��Dy��D�)�D�ffD�� D��fD�0 D�s3D���D��fD�fD�ffD��fD���D��D�i�Dک�D�� D��D�c3D��D�	�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33AVffA���A�  A���B34B��B/34BD��BX  Bk34B~��B���B�33B���B�fgB�33B�33B�33B�  Bٙ�B㙚B홚B�fgC ��C� C
� CL�C��C  C��C#�3C(��C-��C2� C7� C<��CA�3CF� CP�3CZ��Cd� Cn��Cx�fC�L�C�L�C�Y�C�L�C�Y�C�@ C�L�C�33C��C�ffC�s3C�L�C�Y�C�s3C�s3C�ffC�ffC�L�C�ffC�� C�Y�C�L�C�ffC�Y�C�33D��D��D�4D� D� D��D��D$�4D)�gD.��D3�gD8��D=� DB��DG��DL�4DQ�gDV��D[��D`� De�gDj� Do��Dt�gDy�4D�gD�S3D���D��3D��D�` D���D��3D�3D�S3D��3D�ٚD�	�D�VgDږgD���D�	�D�P D�D��g1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A��A�\A�PA�A�5?A�33A�hA��A�bAӓuA�r�A�Aɡ�Ať�A���A���A�M�A�`BA���A�~�A��A��`A�x�A�A�A�A�-A��`A���A��hA��A�(�A�oA�+A�\)A�ĜA�-A�+A��9A�VA��+A���A�5?A���A�  A�7LA�\)A��#A�#A|��At5?AnZAh�AbbNAXȴAUAQ&�AI+A@�HA:^5A533A.��A+��A%��A!�FA��AoA�;A �A	�A�T@��F@�D@ܣ�@�
=@�I�@���@�-@��9@�V@�Ĝ@��@��@�E�@���@���@��R@�Z@�M�@�1@�$�@���@�1'@��@pĜ@g+@^V@R�@Ihs@>��@8  @2�@-�@)��@%?}@ �9@�/@�`@/@%@�T@
-@l�@j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A��A�\A�PA�A�5?A�33A�hA��A�bAӓuA�r�A�Aɡ�Ať�A���A���A�M�A�`BA���A�~�A��A��`A�x�A�A�A�A�-A��`A���A��hA��A�(�A�oA�+A�\)A�ĜA�-A�+A��9A�VA��+A���A�5?A���A�  A�7LA�\)A��#A�#A|��At5?AnZAh�AbbNAXȴAUAQ&�AI+A@�HA:^5A533A.��A+��A%��A!�FA��AoA�;A �A	�A�T@��F@�D@ܣ�@�
=@�I�@���@�-@��9@�V@�Ĝ@��@��@�E�@���@���@��R@�Z@�M�@�1@�$�@���@�1'@��@pĜ@g+@^V@R�@Ihs@>��@8  @2�@-�@)��@%?}@ �9@�/@�`@/@%@�T@
-@l�@j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�B�1B�DB�bB�uB��B	\B	=qB	E�B	��B
��B
��B
�B
�)B
�fB
�B
��B+B	7BDB�B�B?}BN�B\)Bl�Bx�B|�Bw�Bw�B�1B��B��B�bB�Bq�BbNBZBR�BG�B:^B,B"�B�BJB
��B
�B
�B
�jB
��B
�\B
cTB
F�B
%�B
B	�#B	ȴB	�^B	��B	r�B	T�B	?}B	,B	�B		7B��B�fB�B��B��B�jB�9B�LB��B��B��B��B�BB��B�B	1B	uB	$�B	=qB	G�B	`BB	n�B	{�B	�oB	��B	�XB	ƨB	��B	�B	�B	��B
%B
hB
�B
&�B
33B
=qB
D�B
L�B
W
B
^5B
dZB
jB
o�B
u�B
z�B
}�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�B�1B�DB�bB�uB��B	\B	=qB	E�B	��B
��B
��B
�B
�)B
�fB
�B
��B+B	7BDB�B�B?}BN�B\)Bl�Bx�B|�Bw�Bw�B�1B��B��B�bB�Bq�BbNBZBR�BG�B:^B,B"�B�BJB
��B
�B
�B
�jB
��B
�\B
cTB
F�B
%�B
B	�#B	ȴB	�^B	��B	r�B	T�B	?}B	,B	�B		7B��B�fB�B��B��B�jB�9B�LB��B��B��B��B�BB��B�B	1B	uB	$�B	=qB	G�B	`BB	n�B	{�B	�oB	��B	�XB	ƨB	��B	�B	�B	��B
%B
hB
�B
&�B
33B
=qB
D�B
L�B
W
B
^5B
dZB
jB
o�B
u�B
z�B
}�B
�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090816215510  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090816215512  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090816215512  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090816215512  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090816215513  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090816215513  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090816215513  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090816215513  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090816215514  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090816220157                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090820095959  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090820100418  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090820100419  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090820100419  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090820100420  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090820100420  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090820100420  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090820100420  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090820100421  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090820102043                      G�O�G�O�G�O�                