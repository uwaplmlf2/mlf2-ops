CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900688 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               VA   JA  20091002095610  20091005220100                                  2B  A   APEX-SBE 3032                                                   846 @�O�����1   @�O��>��@<��z�H@a�����1   ARGOS   A   A   A   @���A��Ad��A�ffA���A홚B
ffB��B2ffBF  B[��Bm��B�  B�ffB�33B���B�  B�33B�ffBƙ�B�  Bڙ�B�  B�33B���CffC  C33CL�CffC� C�C$� C)33C.�C3  C8L�C=33CBL�CG�CQ  C[� CeL�CoffCy� C��3C��fC��3C�� C���C�� C��fC��fC��3C�Y�C�s3C���C���C�Cǳ3C̙�CѦfC֌�Cۙ�C�3C�3C� C�fC� C�� D�3D�fD�3D� D��D� D��D$�fD)�3D.�fD3��D8��D=�fDB�fDG�fDL�3DQ�fDV��D[�3D`� De� Dj�3DoٚDt��DyٚD�,�D�i�D���D�� D�)�D�i�D��fD��D�)�D�c3D��3D�� D�  D�l�Dڜ�D��3D�#3D�ffD�fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A  A[33A���A�  A���B  BfgB0  BC��BY34Bk34B��B�33B�  B�fgB���B�  B�33B�fgB���B�fgB���B�  B���C ��CffC
��C�3C��C�fC� C#�fC(��C-� C2ffC7�3C<��CA�3CF� CPffCZ�fCd�3Cn��Cx�fC�ffC�Y�C�ffC�s3C�@ C�33C�Y�C�Y�C�ffC��C�&fC�� C�� C�@ C�ffC�L�C�Y�C�@ C�L�C�ffC�ffC�33C�Y�C�33C�33D��D� D��D��D�4D��D�gD$� D)��D.� D3�gD8�gD=� DB� DG� DL��DQ� DV�gD[��D`��De��Dj��Do�4Dt�gDy�4D��D�VgD���D���D�gD�VgD��3D��gD�gD�P D�� D���D��D�Y�Dډ�D�� D� D�S3D�3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�K�A�K�A�A�t�A�-A�ĜA�$�A�I�A�+A���A���A�;dA�l�A�ȴA�33A�1'A��TA��
A�&�A�?}A��uA�  A�1'A���A�G�A���A�\)A���A�t�A���A�-A�bNA�5?A�-A�$�A�{A�1A�7LA���A�r�A��A��mA�/A�ƨA�E�A�oA�\)A��
A�/A��!A~~�A{�-Ay��AuS�Ar1ApbAnVAe�^A`��A]��A]C�A\�AQ�AC�AA�hA9%A3G�A/K�A,ĜA'p�A-A$�A�Ahs@��!@��y@֧�@�ȴ@�X@���@�%@��\@��P@�|�@��F@��w@���@���@��@��u@{�@r�@lz�@j��@co@Y��@O+@J~�@Dj@8�`@3"�@,j@&E�@#o@�@�\@V@��@��@�@��@	G�@��@�H?�"�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�K�A�K�A�A�t�A�-A�ĜA�$�A�I�A�+A���A���A�;dA�l�A�ȴA�33A�1'A��TA��
A�&�A�?}A��uA�  A�1'A���A�G�A���A�\)A���A�t�A���A�-A�bNA�5?A�-A�$�A�{A�1A�7LA���A�r�A��A��mA�/A�ƨA�E�A�oA�\)A��
A�/A��!A~~�A{�-Ay��AuS�Ar1ApbAnVAe�^A`��A]��A]C�A\�AQ�AC�AA�hA9%A3G�A/K�A,ĜA'p�A-A$�A�Ahs@��!@��y@֧�@�ȴ@�X@���@�%@��\@��P@�|�@��F@��w@���@���@��@��u@{�@r�@lz�@j��@co@Y��@O+@J~�@Dj@8�`@3"�@,j@&E�@#o@�@�\@V@��@��@�@��@	G�@��@�H?�"�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	R�B	Q�B	Q�B	r�B	y�B	�B	�+B	��B	��B	��B
5?B
��B
��B
�}B
��B
�
B
�B
�B
�B
�)B
�B
�#B
�/B
�)B
�#B
�#B
�)B
�5B
�5B
�5B
�)B
�BB
�TB
�TB
�ZB
�`B
�TB
�mB
�B
�yB
�mB
�ZB
�sB
�B
�B
�mB
��B
B
�XB
��B
��B
�oB
�7B
v�B
jB
`BB
XB
33B
�B
uB
bB

=B	��B	��B	��B	u�B	`BB	P�B	C�B	+B	  B��BǮB��B��B�{Bt�B~�B�{B��B�RB�qB��B��B�BB�sB�fB	B	�B	49B	?}B	O�B	\)B	aHB	p�B	�B	��B	�B	�XB	��B	�;B	�B	��B
B
DB
�B
"�B
+B
2-B
6FB
<jB
F�B
N�B
W
B
e`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	R�B	Q�B	Q�B	r�B	y�B	�B	�+B	��B	��B	��B
5?B
��B
��B
�}B
��B
�
B
�B
�B
�B
�)B
�B
�#B
�/B
�)B
�#B
�#B
�)B
�5B
�5B
�5B
�)B
�BB
�TB
�TB
�ZB
�`B
�TB
�mB
�B
�yB
�mB
�ZB
�sB
�B
�B
�mB
��B
B
�XB
��B
��B
�oB
�7B
v�B
jB
`BB
XB
33B
�B
uB
bB

=B	��B	��B	��B	u�B	`BB	P�B	C�B	+B	  B��BǮB��B��B�{Bt�B~�B�{B��B�RB�qB��B��B�BB�sB�fB	B	�B	49B	?}B	O�B	\)B	aHB	p�B	�B	��B	�B	�XB	��B	�;B	�B	��B
B
DB
�B
"�B
+B
2-B
6FB
<jB
F�B
N�B
W
B
e`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091002095608  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091002095610  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091002095610  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091002095610  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091002095611  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091002095612  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091002095612  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091002095612  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091002095612  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091002095612  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091002100144                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091005215529  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091005215726  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091005215726  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091005215726  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091005215726  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091005215727  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091005215727  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091005215728  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091005215728  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091005215728  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091005220100                      G�O�G�O�G�O�                