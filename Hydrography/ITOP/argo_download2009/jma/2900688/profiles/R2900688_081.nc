CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900688 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               QA   JA  20090813065841  20090901010415                                  2B  A   APEX-SBE 3032                                                   846 @�Ct��c�1   @�Ct���@=>�Q�@a����F1   ARGOS   A   A   A   @�33A33Ac33A�33A���A홚B
  B  B2��BE33BY33Bn  B���B�33B���B���B�ffB���B�33B�33B�ffB�  B���B�33B���C��C� C��CL�CffC�CffC$� C)33C.�C3L�C8�C=ffCB�CF�fCP�3C[33Ce33Cn��CyffC�� C�� C��fC�� C�� C�s3C��3C���C�s3C���C�s3C�s3C���C���C���C̙�C�� Cֳ3C۳3C�fC噚C�s3C��C��fC��fD�3D�fD�3D�3D�3D�fD� D$�3D)�3D.� D3�3D8� D=�fDB� DG� DL�fDQ�fDV��D[�3D`� De�fDj��Do�fDt� Dy�3D�#3D�c3D��3D���D�#3D�i�D���D���D�)�D�S3D���D��3D�0 D�ffDڬ�D�� D�&fD�ffD�fD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A33A[33A�33A���A陚B  B  B0��BC33BW33Bl  B��B�33B���B���B�ffB���B�33B�33B�ffB�  B���B�33B���C�C  C�C��C�fC��C�fC$  C(�3C-��C2��C7��C<�fCA��CFffCP33CZ�3Cd�3Cn�Cx�fC�@ C�� C�ffC�� C�� C�33C�s3C�Y�C�33C�Y�C�33C�33C�L�C�Cǌ�C�Y�Cр C�s3C�s3C�ffC�Y�C�33C�L�C�ffC�ffD�3D�fD�3D�3D�3D�fD� D$�3D)�3D.� D3�3D8� D=�fDB� DG� DL�fDQ�fDV��D[�3D`� De�fDj��Do�fDt� Dy�3D�3D�S3D��3D���D�3D�Y�D���D���D��D�C3D���D��3D�  D�VfDڜ�D�� D�fD�VfD�fD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ffA�jA�ffA�hsA��TA��A�1'A��A�"�A��/A��#A�
=A��A��A��!A��hA���A��/A��9A��A��+A�n�A�(�A���A���A�v�A�jA��A��A��FA�O�A��A�I�A��A��-A��A��A���A��uA�M�A�p�A��\A�ƨA�33A�dZA���A�n�A�JA��jA�K�A�p�A~9XAz=qAv��As�TAp��AlZAg��Ac��A\bAY��AUVAR��AN�uAJ��AE�#AC�hA<ZA6jA0r�A(�A�A�HA	`BAG�@��9@��/@�C�@��@Ɵ�@�hs@�-@�O�@�O�@�hs@�-@�33@�1'@�S�@��7@��@��
@�=q@�{@�bN@k��@b��@U�-@K"�@A�#@:n�@4(�@(�9@#�
@!��@$�@��@33@�@�y@O�@�
@
�H@�@ƨ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�ffA�jA�ffA�hsA��TA��A�1'A��A�"�A��/A��#A�
=A��A��A��!A��hA���A��/A��9A��A��+A�n�A�(�A���A���A�v�A�jA��A��A��FA�O�A��A�I�A��A��-A��A��A���A��uA�M�A�p�A��\A�ƨA�33A�dZA���A�n�A�JA��jA�K�A�p�A~9XAz=qAv��As�TAp��AlZAg��Ac��A\bAY��AUVAR��AN�uAJ��AE�#AC�hA<ZA6jA0r�A(�A�A�HA	`BAG�@��9@��/@�C�@��@Ɵ�@�hs@�-@�O�@�O�@�hs@�-@�33@�1'@�S�@��7@��@��
@�=q@�{@�bN@k��@b��@U�-@K"�@A�#@:n�@4(�@(�9@#�
@!��@$�@��@33@�@�y@O�@�
@
�H@�@ƨ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�HB	�NB	�TB	�HB	��B
�B
+B
YB
YB
ZB
hsB
s�B
�+B
��B
��B
�B
�!B
�!B
�LB
�}B
ÖB
ÖB
�/B
�B
�B
�B
�B
�sB
�B
�BJBoBhB�B
=B
��B
��B
��B  B
��B
�B
�B
��B  B
��B
��B
�B
�)B
ɺB
�jB
�B
��B
�=B
z�B
n�B
^5B
K�B
7LB
%�B
+B	��B	�mB	�/B	ɺB	�RB	��B	��B	|�B	`BB	C�B	�B�HB�wB��B�1Be`BZBI�BD�BE�BK�B_;Bu�B�B�uB��B��B�'B�wB�mB��B	+B	5?B	<jB	@�B	aHB	r�B	�PB	��B	�jB	��B	�)B	��B
B
+B
bB
 �B
,B
1'B
7LB
<jB
?}B
B�B
H�B
V1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�HB	�NB	�TB	�HB	��B
�B
+B
YB
YB
ZB
hsB
s�B
�+B
��B
��B
�B
�!B
�!B
�LB
�}B
ÖB
ÖB
�/B
�B
�B
�B
�B
�sB
�B
�BJBoBhB�B
=B
��B
��B
��B  B
��B
�B
�B
��B  B
��B
��B
�B
�)B
ɺB
�jB
�B
��B
�=B
z�B
n�B
^5B
K�B
7LB
%�B
+B	��B	�mB	�/B	ɺB	�RB	��B	��B	|�B	`BB	C�B	�B�HB�wB��B�1Be`BZBI�BD�BE�BK�B_;Bu�B�B�uB��B��B�'B�wB�mB��B	+B	5?B	<jB	@�B	aHB	r�B	�PB	��B	�jB	��B	�)B	��B
B
+B
bB
 �B
,B
1'B
7LB
<jB
?}B
B�B
H�B
V1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090813065840  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090813065841  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090813065841  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090813065842  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090813065843  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090813065843  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090813065843  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090813065843  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090813065843  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090813071024                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090816215526  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090816215736  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090816215736  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090816215737  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090816215738  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090816215738  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090816215738  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090816215738  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090816215738  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090816220124                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090901003808  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090901003808  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090901003809  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090901003809  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090901003809  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090901003809  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090901003810  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090901010415                      G�O�G�O�G�O�                