CDF   /   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   o   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K   CALIBRATION_DATE            	             
_FillValue                  ,  N   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N4   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N8   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N<   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N@   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  ND   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  2900688 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               PA   JA  20090804095514  20091208030146                                  2B  A   APEX-SBE 3032                                                   846 @�@�
���1   @�@��N�@=6�+J@a�bM��1   ARGOS   B   B   B   @�  AffAk33A���A���A���B��B  B0��G�O�B���B���B���B�  B�  B�33B���B���B���B�ffB�ffC  C  C�CffC  C��C�3C$ffC)ffC.L�C333C8�C=  CBffCG�CQffCZ��Ce  Co� Cy33C��fC�� C��fC���C�� C�� C��3C�� C���C�s3C�s3C�Y�C��3C³3CǦfČ�Cь�C�� Cۀ C�3C�s3C�ffC�fC���C���D��D� D� D�fD��DٚD�fD$� D)�fD.�fD3ٚD8� D=ٚDB� DG� DL��DQ��DV�3D[�fD`�3DeٚDj�3Do� Dt�3Dy� D�0 D�c3D�� D�� D�#3D�ffD��3D�� D��D�i�D���D��D�)�D�i�Dڰ D��3D�#3D�Y�D�fD�Vf111111111411111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  AffAc33A���A���A���B	��B  B.��G�O�B���B���B���B�  B�  B�33B���B���B���B�ffB�ffC � C� C
��C�fC� CL�C33C#�fC(�fC-��C2�3C7��C<� CA�fCF��CP�fCZL�Cd� Co  Cx�3C�ffC�� C�ffC�L�C�@ C�� C�s3C�� C�Y�C�33C�33C��C�s3C�s3C�ffC�L�C�L�Cր C�@ C�s3C�33C�&fC�ffC�Y�C�Y�D��D� D� D�fD��D��D�fD$� D)�fD.�fD3��D8� D=��DB� DG� DL��DQ��DV�3D[�fD`�3De��Dj�3Do� Dt�3Dy� D�  D�S3D�� D�� D�3D�VfD��3D�� D��D�Y�D���D�ٚD��D�Y�Dڠ D��3D�3D�I�D�fD�Ff111111111411111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�r�A߶FA�$�AƬA�ffA�oA�+G�O�A���A���A��A�Q�A��A���A���A�+A�C�A��A���A��A��uA���A��+A�9XA�
=A�~�A�33A��A��A���A�hsA�{A�A�A���A�A��A�Q�A�ĜA�5?A��\A���A�ƨA�
=A��A��
A��#A��`A��A�l�A}�Az5?Aw��AvA�Ar��Am��AjE�AidZAfjAb�A_dZAZ�yAU��AO�#AJ5?AH�+A7?}A,bA"E�AĜA5?At�@�J@ۅ@�\)@��m@��^@��@���@��`@���@��h@��@��#@�Z@�t�@�ff@���@z��@x��@y�@l��@d��@XQ�@Nv�@E`B@<��@7|�@/l�@'\)@!�7@$�@M�@�+@��@ƨ@��@��@G�?��h?�r�111111119411111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A�r�A߶FA�$�AƬA�ffA�oA�+G�O�A���A���A��A�Q�A��A���A���A�+A�C�A��A���A��A��uA���A��+A�9XA�
=A�~�A�33A��A��A���A�hsA�{A�A�A���A�A��A�Q�A�ĜA�5?A��\A���A�ƨA�
=A��A��
A��#A��`A��A�l�A}�Az5?Aw��AvA�Ar��Am��AjE�AidZAfjAb�A_dZAZ�yAU��AO�#AJ5?AH�+A7?}A,bA"E�AĜA5?At�@�J@ۅ@�\)@��m@��^@��@���@��`@���@��h@��@��#@�Z@�t�@�ff@���@z��@x��@y�@l��@d��@XQ�@Nv�@E`B@<��@7|�@/l�@'\)@!�7@$�@M�@�+@��@ƨ@��@��@G�?��h?�r�111111119411111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�BB	��B	�)B
�B
-B
.B
�B
��B
�G�O�B
��B
ȴB
��B
ɺB
ƨB
ƨB
ɺB
ɺB
��B
��B
��B
��B
��B
��B
��B
��B
��B
��B
��B
�B
�5B
�HB
�/B
��B
��B
��B
��B
��B
��B
�HB
�B
�B
�sB
�fB
�HB
�)B
��B
ÖB
�RB
��B
��B
�%B
|�B
u�B
hsB
R�B
E�B
@�B
33B
 �B
oB	��B	�fB	��B	�?B	�B	^5B	/B	B�B�Bv�BL�B49B-B'�B.B7LBJ�B[#B{�B�7B��BÖB�B�5B�TB�B	�B	 �B	%�B	H�B	gmB	� B	��B	�3B	ǮB	��B	�mB	��B
1B
hB
�B
#�B
49B
>wB
F�B
O�B
YB
`BB
gm111111111411111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�BB	��B	�)B
�B
-B
.B
�B
��B
�G�O�B
��B
ȴB
��B
ɺB
ƨB
ƨB
ɺB
ɺB
��B
��B
��B
��B
��B
��B
��B
��B
��B
��B
��B
�B
�5B
�HB
�/B
��B
��B
��B
��B
��B
��B
�HB
�B
�B
�sB
�fB
�HB
�)B
��B
ÖB
�RB
��B
��B
�%B
|�B
u�B
hsB
R�B
E�B
@�B
33B
 �B
oB	��B	�fB	��B	�?B	�B	^5B	/B	B�B�Bv�BL�B49B-B'�B.B7LBJ�B[#B{�B�7B��BÖB�B�5B�TB�B	�B	 �B	%�B	H�B	gmB	� B	��B	�3B	ǮB	��B	�mB	��B
1B
hB
�B
#�B
49B
>wB
F�B
O�B
YB
`BB
gm111111111411111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090804095513  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090804095514  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090804095514  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090804095515  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090804095516  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090804095516  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090804095516  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090804095516  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090804095516  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090804095516  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090804095516  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090804100143                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090806215600  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090806215809  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090806215810  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090806215810  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090806215811  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090806215811  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090806215811  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090806215811  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090806215811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090806215811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090806215812  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090806220206                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090901003807  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090901003807  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090901003808  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090901003808  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090901003808  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090901003808  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090901003808  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090901003808  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090901003808  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090901010622                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208024032  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208024231  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208024232  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091208024232  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208024232  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208024234  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091208024234  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208024234  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208024234  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208024234  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208024234  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208024234  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208030146                      G�O�G�O�G�O�                