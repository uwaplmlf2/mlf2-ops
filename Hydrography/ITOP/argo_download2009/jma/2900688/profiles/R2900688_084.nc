CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900688 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               TA   JA  20090912065556  20090915220149                                  2B  A   APEX-SBE 3032                                                   846 @�J�z�11   @�J�io{@=S33333@a���l�D1   ARGOS   A   A   A   @���A  AfffA���A���A�33B
��BffB2ffBE��BZ  BnffB�  B���B�33B���B�  B�  B�  B�33B�  B���B�33BB�ffCffC33C
�fCffC33C� C33C$  C)ffC.ffC2�fC8L�C=�CB�CG33CQ33C[L�Ce�Co� CyL�C���C�� C���C�� C�� C�s3C���C���C�s3C��3C��fC��3C��3C¦fC�� C̙�C�� C֙�C�s3C�� C�s3CꙚC�s3C�ffC��fD�fD� D�3D� D� D�fD��D$� D)��D.�3D3�3D8�3D=�3DB�fDGٚDL��DQ�fDV��D[�fD`�fDe��Dj��DoٚDtٚDy�fD�&fD�i�D���D��3D�  D�ffD���D�� D��D�l�D�� D���D��D�\�Dڣ3D��fD�  D�i�D��D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA��A[33A�  A�33A噙B  B��B/��BB��BW33Bk��B33B�34B���B�34B���B���B���B���Bϙ�B�fgB���B�34B�  C �3C� C
33C�3C� C��C� C#L�C(�3C-�3C233C7��C<fgCAfgCF� CP� CZ��CdfgCn��Cx��C�33C�ffC�33C�&fC�&fC��C�@ C�@ C��C�Y�C�L�C�Y�C�Y�C�L�C�ffC�@ C�ffC�@ C��C�&fC��C�@ C��C��C�L�D��D�3D�fD�3D�3D��D��D$�3D)� D.�fD3�fD8�fD=�fDB��DG��DL� DQ��DV� D[��D`��De� Dj� Do��Dt��Dy��D� D�S4D��gD���D�	�D�P D��4D�ٚD�4D�VgD���D��gD�4D�FgDڌ�D�� D�	�D�S4D�gD�p 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ZA�S�A�^5Aڙ�A�Q�A�v�Aѥ�A�K�A�jA��+A��/A��+A��FA�r�A��/A��uA���A���A���A�ZA���A�
=A��#A���A�C�A�^5A�+A�^5A���A�M�A��HA�M�A���A��^A��9A�r�A�1'A�A���A��uA�JA�VA��-A�dZA�ƨA���A�A�A��mA�ȴA��/A�E�A�M�A�{A~��Az�/Av=qAt1'Ap��An9XAi�wAc�A` �A^n�A[AS�ANAJ�/AD�DA@A0�/A(�A�+A�TA
��A�@���@��@�Z@�=q@��#@���@���@��y@�A�@�p�@���@��@�v�@�1@��@�$�@���@}@uV@d�D@_�@U�h@KC�@A�7@8��@3�F@,�j@'�P@"=q@�@��@�@&�@��@	��@��@I�@ ��?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�ZA�S�A�^5Aڙ�A�Q�A�v�Aѥ�A�K�A�jA��+A��/A��+A��FA�r�A��/A��uA���A���A���A�ZA���A�
=A��#A���A�C�A�^5A�+A�^5A���A�M�A��HA�M�A���A��^A��9A�r�A�1'A�A���A��uA�JA�VA��-A�dZA�ƨA���A�A�A��mA�ȴA��/A�E�A�M�A�{A~��Az�/Av=qAt1'Ap��An9XAi�wAc�A` �A^n�A[AS�ANAJ�/AD�DA@A0�/A(�A�+A�TA
��A�@���@��@�Z@�=q@��#@���@���@��y@�A�@�p�@���@��@�v�@�1@��@�$�@���@}@uV@d�D@_�@U�h@KC�@A�7@8��@3�F@,�j@'�P@"=q@�@��@�@&�@��@	��@��@I�@ ��?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	I�B	H�B	I�B	5?B	5?B	�B	ĜB
\B
-B
bNB
�+B
�VB
��B
�B
�?B
�}B
�}B
ŢB
ƨB
ƨB
��B
��B
�B
��B
��B
��B
��B
��B
��B
��B
��B
ŢB
ɺB
��B
��B
��B
��B
��B
�
B
�5B
�`B
�B
�#B
�)B
��B
�BB
�yB
�B
�HB
��B
��B
B
�?B
�B
��B
�=B
w�B
l�B
^5B
Q�B
:^B
"�B
�B
PB	��B	�ZB	��B	�wB	��B	�{B	\)B	<jB	B��B��B�Bp�Bo�BVBdZBk�B��B��B�wB��B�sB�B��B��B	B	�B	!�B	(�B	8RB	H�B	jB	~�B	�hB	��B	�}B	��B	�;B	�B	��B
+B
oB
�B
&�B
2-B
=qB
C�B
K�B
Q�B
[#B
bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	I�B	H�B	I�B	5?B	5?B	�B	ĜB
\B
-B
bNB
�+B
�VB
��B
�B
�?B
�}B
�}B
ŢB
ƨB
ƨB
��B
��B
�B
��B
��B
��B
��B
��B
��B
��B
��B
ŢB
ɺB
��B
��B
��B
��B
��B
�
B
�5B
�`B
�B
�#B
�)B
��B
�BB
�yB
�B
�HB
��B
��B
B
�?B
�B
��B
�=B
w�B
l�B
^5B
Q�B
:^B
"�B
�B
PB	��B	�ZB	��B	�wB	��B	�{B	\)B	<jB	B��B��B�Bp�Bo�BVBdZBk�B��B��B�wB��B�sB�B��B��B	B	�B	!�B	(�B	8RB	H�B	jB	~�B	�hB	��B	�}B	��B	�;B	�B	��B
+B
oB
�B
&�B
2-B
=qB
C�B
K�B
Q�B
[#B
bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090912065555  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090912065556  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090912065556  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090912065557  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090912065557  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090912065558  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090912065558  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090912065558  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090912065558  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090912065558  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090912070139                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090915215550  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090915215757  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090915215757  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090915215758  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090915215758  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090915215759  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090915215759  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090915215759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090915215759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090915215759  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090915220149                      G�O�G�O�G�O�                