CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900688 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               RA   JA  20090823065548  20090901010655                                  2B  A   APEX-SBE 3032                                                   846 @�E�I��J1   @�E��www@=��1&�@a�fffff1   ARGOS   A   A   A   @���AffAfffA���A���A�ffB
��B��B2ffBF  BZffBm��B�  B���B���B�33B�ffB���B�33B���B�ffB�  B䙚BB�33C��C33CffC�CL�C�CffC$L�C)ffC.  C2��C8L�C=  CBffCG  CQffC[�Cd�fCo�Cy��C��fC���C���C��fC���C��fC���C�s3C���C��3C�� C��fC�s3C�CǦfC�� Cр C֦fCۦfC�fC�ffC��C��C���C���D��D��D� D�fDٚD� D�fD$� D)�fD.�3D3��D8�3D=�fDB�fDG��DL� DQ��DV�fD[� D`��De�3Dj��Do��Dt� Dy� D��D�p D��3D�� D�#3D�l�D���D��3D�&fD�l�D���D���D�&fD�c3Dڠ D���D�&fD�ffD�3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffA^ffA���A���A�ffB��B��B0ffBD  BXffBk��B�  B���B���B�33B�ffB���B�33B���B�ffB�  B㙚B홚B�33C�C�3C
�fC��C��C��C�fC#��C(�fC-� C2L�C7��C<� CA�fCF� CP�fCZ��CdffCn��Cy�C�ffC�Y�C�L�C�ffC�Y�C�ffC�L�C�33C�Y�C�s3C�@ C�ffC�33C�Y�C�ffC̀ C�@ C�ffC�ffC�ffC�&fC�L�C�L�C�Y�C�Y�D��D��D� D�fD��D� D�fD$� D)�fD.�3D3��D8�3D=�fDB�fDG��DL� DQ��DV�fD[� D`��De�3Dj��Do��Dt� Dy� D�	�D�` D��3D�� D�3D�\�D���D��3D�fD�\�D���D���D�fD�S3Dڐ D���D�fD�VfD�3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��;A��#A�DA��A�DAߓuA�+A�G�A�;dA�l�A�A��
A�~�A��A��`A��;A�5?A��`A� �A��PA���A�~�A�&�A��A�/A�v�A���A�K�A���A�O�A�$�A��jA��A��wA�n�A��A�E�A��A��/A��-A��\A�K�A�bA��+A��A�VA��A�
=A�~�A�&�A���A�ƨA��DA�^5A�(�A���A�A{Au"�AnffAfv�Ac��A`ffA_33AZ�AWG�AT1AQ��AK�AFJA7�FA-oA)ƨA"VA%AZA&�@��@��@��@�(�@��9@�bN@���@�$�@�I�@���@���@�^5@��@�ff@�  @z��@u�@p�@g�@^@SC�@K"�@DI�@:-@2�!@-/@)X@#S�@�@=q@dZ@Q�@@	��@��@��@ �9?�|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��;A��#A�DA��A�DAߓuA�+A�G�A�;dA�l�A�A��
A�~�A��A��`A��;A�5?A��`A� �A��PA���A�~�A�&�A��A�/A�v�A���A�K�A���A�O�A�$�A��jA��A��wA�n�A��A�E�A��A��/A��-A��\A�K�A�bA��+A��A�VA��A�
=A�~�A�&�A���A�ƨA��DA�^5A�(�A���A�A{Au"�AnffAfv�Ac��A`ffA_33AZ�AWG�AT1AQ��AK�AFJA7�FA-oA)ƨA"VA%AZA&�@��@��@��@�(�@��9@�bN@���@�$�@�I�@���@���@�^5@��@�ff@�  @z��@u�@p�@g�@^@SC�@K"�@DI�@:-@2�!@-/@)X@#S�@�@=q@dZ@Q�@@	��@��@��@ �9?�|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	ÖB	B	�}B	�?B	�3B	�B
33B
p�B
�B
�B
�B
��B
��B
�B
��B
��B
��B
��B
��B
��B
�
B
�/B
�/B
�)B
�;B
�/B
�)B
�)B
�#B
�)B
�)B
�)B
�B
�/B
�/B
�B
��B
��B
��B
��B
�B
�mB
�B
�BBB
��B
��B
��B
��B
�B
�mB
�
B
��B
��B
�3B
��B
�oB
s�B
VB
5?B
'�B
�B
oB	��B	�B	�NB	��B	�dB	��B	e`B	<jB	)�B	bB�B�mB�#B��B�}B�qB��BɺB��BBŢB�3B��B�NB�B	DB	�B	%�B	0!B	C�B	T�B	jB	�+B	��B	��B	�LB	��B	�;B	�B	��B
B
PB
�B
)�B
2-B
:^B
C�B
N�B
S�B
[#B
^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	ÖB	B	�}B	�?B	�3B	�B
33B
p�B
�B
�B
�B
��B
��B
�B
��B
��B
��B
��B
��B
��B
�
B
�/B
�/B
�)B
�;B
�/B
�)B
�)B
�#B
�)B
�)B
�)B
�B
�/B
�/B
�B
��B
��B
��B
��B
�B
�mB
�B
�BBB
��B
��B
��B
��B
�B
�mB
�
B
��B
��B
�3B
��B
�oB
s�B
VB
5?B
'�B
�B
oB	��B	�B	�NB	��B	�dB	��B	e`B	<jB	)�B	bB�B�mB�#B��B�}B�qB��BɺB��BBŢB�3B��B�NB�B	DB	�B	%�B	0!B	C�B	T�B	jB	�+B	��B	��B	�LB	��B	�;B	�B	��B
B
PB
�B
)�B
2-B
:^B
C�B
N�B
S�B
[#B
^51111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090823065547  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090823065548  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090823065548  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090823065549  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090823065550  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090823065550  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090823065550  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20090823065550  QCF$                G�O�G�O�G�O�             840JA  ARGQaqcp2.8b                                                                20090823065550  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090823065550  QCF$                G�O�G�O�G�O�             840JA  ARGQrqcpt16b                                                                20090823065550  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090823070217                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090826215540  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090826215740  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090826215741  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090826215741  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090826215742  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090826215742  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090826215742  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090826215742  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090826215743  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090826220130                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090901003810  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090901003810  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090901003811  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090901003811  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090901003811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090901003811  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090901003811  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090901010655                      G�O�G�O�G�O�                