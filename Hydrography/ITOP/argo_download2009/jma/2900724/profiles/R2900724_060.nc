CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   k   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4H   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6`   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8x   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :$   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <<   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >T   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  @    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @l   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  B   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D0   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D`   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G`   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J`   CALIBRATION_DATE            	             
_FillValue                  ,  M`   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    M�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    M�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    M�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    M�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  M�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N    HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    NArgo profile    2.2 1.2 19500101000000  2900724 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               <A   JA  20090920125633  20091208012301                                  2B  A   APEX-SBE 3463                                                   846 @�L��Ƞ�1   @�L����@;#�
=p�@`���$�1   ARGOS   B   B   B   @�ffAffAa��A���A�  A�  B	��B  B133BFffBX��BlffB�ffB�  B���B�  B�ffB�  B���B�33B�33B�ffB�  B�ffB���C  C� C� C��CffCffC�C$� C)�C.ffC3� C8ffC=ffCBffCG��CQffC[L�CeL�Co  Cy33C�s3C���C���C��3C���C���C�s3C�� C¦fCǦfC̙�Cљ�C�� CۦfC���C�� C�fC�fC��C���DٚDٚD��G�O�D)�3D.ٚD3��D8��D=�fDBٚDG�3DL��DQٚDVٚD[� D`� De�fDj� Do��DtٚDy�3D��D�l�D��3D���D�)�D�c3D��3D�� D�)�D�\�D�� D��fD�)�D�Y�Dڜ�D��3D�&fD�VfD�fD���D�P 11111111111111111111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111 @�ffAffAa��A���A�  A�  B	��B  B133BFffBX��BlffB�ffB�  B���B�  B�ffB�  B���B�33B�33B�ffB�  B�ffB���C  C� C� C��CffCffC�C$� C)�C.ffC3� C8ffC=ffCBffCG��CQffC[L�CeL�Co  Cy33C�s3C���C���C��3C���C���C�s3C�� C¦fCǦfC̙�Cљ�C�� CۦfC���C�� C�fC�fC��C���DٚDٚD��G�O�D)�3D.ٚD3��D8��D=�fDBٚDG�3DL��DQٚDVٚD[� D`� De�fDj� Do��DtٚDy�3D��D�l�D��3D���D�)�D�c3D��3D�� D�)�D�\�D�� D��fD�)�D�Y�Dڜ�D��3D�&fD�VfD�fD���D�P 11111111111111111111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�VA�bNA�ffA�hsA�hsA�l�A�n�A�n�A�jA�l�A�n�A�dZA�t�A�VAҁAϬA�(�Aɏ\A��HA��yA�1A�=qA�{A��yA���A�jA�Q�A�ZA��PA��uA�?}A�1A���A�r�A���A��A��9A�{A�
=A�z�A�VA�(�A�O�A��PA�ĜA��A� �A���A��`A���A��uA�VA��A��A{O�AyVAt1'Aqp�AljAi
=Ab  A^r�AXZAU\)AOl�ABr�A;"�G�O�@��/@�{@�x�@�$�@�|�@�9X@���@�o@���@�Q�@��j@�o@�/@��9@��^@��\@�ȴ@�X@y��@j�@_�@R�!@JM�@@��@9�^@1�#@+33@%?}@�w@��@�@o@��@
��@ȴ@j@��?�;d?��11111111111111111111111111111111111111111111111111111111111111111119411111111111111111111111111111111111111 A�VA�bNA�ffA�hsA�hsA�l�A�n�A�n�A�jA�l�A�n�A�dZA�t�A�VAҁAϬA�(�Aɏ\A��HA��yA�1A�=qA�{A��yA���A�jA�Q�A�ZA��PA��uA�?}A�1A���A�r�A���A��A��9A�{A�
=A�z�A�VA�(�A�O�A��PA�ĜA��A� �A���A��`A���A��uA�VA��A��A{O�AyVAt1'Aqp�AljAi
=Ab  A^r�AXZAU\)AOl�ABr�A;"�G�O�@��/@�{@�x�@�$�@�|�@�9X@���@�o@���@�Q�@��j@�o@�/@��9@��^@��\@�ȴ@�X@y��@j�@_�@R�!@JM�@@��@9�^@1�#@+33@%?}@�w@��@�@o@��@
��@ȴ@j@��?�;d?��11111111111111111111111111111111111111111111111111111111111111111119411111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	T�B	VB	T�B	T�B	T�B	T�B	T�B	VB	W
B	VB	VB	XB	�uB	jB	�XB	��B	�yB
�B
9XB
P�B
x�B
�DB
��B
�?B
�FB
ǮB
��B
��B
��B
�B
�B
�B
�HB
�fB
�mB
�ZB
�B
�BBB
��B�BuB�B�B�B�B�B�BbBVB+B%B
��B
�VB
�B
n�B
aHB
L�B
>wB
%�B
�B
B	��B	�fB	��B	�+G�O�B�-B��B��B��B��B�B�qBȴB�
B�`B��B	 �B	0!B	)�B	6FB	H�B	ZB	dZB	}�B	�uB	��B	�}B	��B	�NB	�B	��B
DB
�B
&�B
33B
>wB
C�B
M�B
W
B
_;B
e`B
k�B
p�B
p�11111111111111111111111111111111111111111111111111111111111111111119411111111111111111111111111111111111111 B	T�B	VB	T�B	T�B	T�B	T�B	T�B	VB	W
B	VB	VB	XB	�uB	jB	�XB	��B	�yB
�B
9XB
P�B
x�B
�DB
��B
�?B
�FB
ǮB
��B
��B
��B
�B
�B
�B
�HB
�fB
�mB
�ZB
�B
�BBB
��B�BuB�B�B�B�B�B�BbBVB+B%B
��B
�VB
�B
n�B
aHB
L�B
>wB
%�B
�B
B	��B	�fB	��B	�+G�O�B�-B��B��B��B��B�B�qBȴB�
B�`B��B	 �B	0!B	)�B	6FB	H�B	ZB	dZB	}�B	�uB	��B	�}B	��B	�NB	�B	��B
DB
�B
&�B
33B
>wB
C�B
M�B
W
B
_;B
e`B
k�B
p�B
p�11111111111111111111111111111111111111111111111111111111111111111119411111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090920125633  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090920125633  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090920125634  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090920125634  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090920125635  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090920125635  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090920125635  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090920125635  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090920125635  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090920125635  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090920125636  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090920130056                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090922215640  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090922215831  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090922215832  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090922215832  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090922215833  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090922215833  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090922215833  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090922215833  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090922215833  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090922215833  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090922215833  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090922220218                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207234127  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207234253  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207234254  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207234255  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207234257  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207234257  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207234257  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207234257  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207234257  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207234257  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091207234257  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208012301                      G�O�G�O�G�O�                