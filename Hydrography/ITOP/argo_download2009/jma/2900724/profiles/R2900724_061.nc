CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   ]   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     t  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  `  4   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     t  4p   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  `  5�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     t  6D   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     t  7�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  `  9,   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     t  9�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  `  ;    TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     t  ;`   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     t  <�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  `  >H   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     t  >�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  `  @   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     t  @|   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  A�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    B    SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    E    SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    H    CALIBRATION_DATE            	             
_FillValue                  ,  K    HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    KL   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    KP   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    KT   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    KX   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  K\   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    K�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    K�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    K�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         K�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         K�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        K�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    K�Argo profile    2.2 1.2 19500101000000  2900724 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               =A   JA  20091005005754  20091208012308                                  2B  A   APEX-SBE 3463                                                   846 @�O7���s1   @�O7��/@;      @`�V�1   ARGOS   B   B   B   G�O�BY��Bm33B�33B�33B���B�33B�33B���B�33B�ffB�33Bڙ�B�  BB�ffC��CffCL�C��CffCffC�C$  C)ffC.ffC3L�C8ffC=ffCBL�CGL�CQ� C[ffCe�Co33Cy33C��3C��3C��3C��3C���C�� C���C��fC��3C��3C�s3C���C��3C۳3C���C�� C�fC� G�O�D� D�fD��D��D$�fD)�3D.ٚD3��D8� D=�fDB��DG��DLٚDQ� Do� DtٚDy� D�&fD�ffD��3D�� D�)�D�l�D�� D��fD��D�\�D�� D��3D�&fD�` Dک�D��fD�)�D�c3D��D��fD�0 411111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111   G�O�BY��Bm33B�33B�33B���B�33B�33B���B�33B�ffB�33Bڙ�B�  BB�ffC��CffCL�C��CffCffC�C$  C)ffC.ffC3L�C8ffC=ffCBL�CGL�CQ� C[ffCe�Co33Cy33C��3C��3C��3C��3C���C�� C���C��fC��3C��3C�s3C���C��3C۳3C���C�� C�fC� G�O�D� D�fD��D��D$�fD)�3D.ٚD3��D8� D=�fDB��DG��DLٚDQ� Do� DtٚDy� D�&fD�ffD��3D�� D�)�D�l�D�� D��fD��D�\�D�� D��3D�&fD�` Dک�D��fD�)�D�c3D��D��fD�0 411111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�O�A�XA�^5A�FA׏\A�G�A΃A�^5A���AÁA��A�t�A�"�A��/A�E�A��RA�`BA��A���A��#A��A�=qA���A�33A��A�33A�C�A���A��`A�ZA�l�A�bA��mA��A��DA��PA� �A�ĜA��`A��A�K�A�jA���A�?}A��A��A��
A�E�A���Ah-AfE�AdVAa��G�O�A5l�A'�wA�hA1A�A�+@�bN@蛦@�G�@�p�@�O�@�@�O�@�Q�@�%@��@�b@���@�A�@sS�@c��@Q�#@G
=@?�w@6��@/�;@*=q@&E�@!��@��@��@G�@�j@	�#@\)@��@ Ĝ?�ƨ?���411111111111111111111111111111111111111111111111111119411111111111111111111111111111111111111   A�O�A�XA�^5A�FA׏\A�G�A΃A�^5A���AÁA��A�t�A�"�A��/A�E�A��RA�`BA��A���A��#A��A�=qA���A�33A��A�33A�C�A���A��`A�ZA�l�A�bA��mA��A��DA��PA� �A�ĜA��`A��A�K�A�jA���A�?}A��A��A��
A�E�A���Ah-AfE�AdVAa��G�O�A5l�A'�wA�hA1A�A�+@�bN@蛦@�G�@�p�@�O�@�@�O�@�Q�@�%@��@�b@���@�A�@sS�@c��@Q�#@G
=@?�w@6��@/�;@*=q@&E�@!��@��@��@G�@�j@	�#@\)@��@ Ĝ?�ƨ?���411111111111111111111111111111111111111111111111111119411111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	r�B	t�B	w�B	�B	�B	�ZB
VB
:^B
XB
t�B
�\B
�B
�FB
ÖB
ǮB
��B
��B
�B
�)B
�#B
�;B
�yB
�B
�B
�B
�B
��B
��B
�B
��B
��B
��B
��BB
=B�B�B$�B�B�BPB1B
��B
�B
�;B
�B
�wB
��B
>wB
8RB
0!B
%�B
�G�O�B	>wB��B�mB�5B�B�wB�9B�B�B�LB��B��B�B	%B	N�B	Q�B	]/B	x�B	�PB	��B	ÖB	�B	�fB	��B
B
PB
�B
 �B
)�B
:^B
F�B
P�B
YB
^5B
dZB
n�B
t�B
u�411111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111   G�O�B	r�B	t�B	w�B	�B	�B	�ZB
VB
:^B
XB
t�B
�\B
�B
�FB
ÖB
ǮB
��B
��B
�B
�)B
�#B
�;B
�yB
�B
�B
�B
�B
��B
��B
�B
��B
��B
��B
��BB
=B�B�B$�B�B�BPB1B
��B
�B
�;B
�B
�wB
��B
>wB
8RB
0!B
%�B
�G�O�B	>wB��B�mB�5B�B�wB�9B�B�B�LB��B��B�B	%B	N�B	Q�B	]/B	x�B	�PB	��B	ÖB	�B	�fB	��B
B
PB
�B
 �B
)�B
:^B
F�B
P�B
YB
^5B
dZB
n�B
t�B
u�411111111111111111111111111111111111111111111111111111411111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091002215617  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091005005754  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091005005755  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091005005755  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091005005756  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091005005756  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091005005756  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091005005756  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091005005756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20091005005756  QCF$                G�O�G�O�G�O�            4100JA  ARGQaqcp2.8b                                                                20091005005756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091005005756  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20091005005757  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091005010123                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091207234128  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091207234609  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091207234610  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091207234610  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091207234612  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091207234612  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207234612  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091207234612  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091207234612  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8c                                                                20091207234612  QCF$                G�O�G�O�G�O�            4100JA  ARGQaqcp2.8c                                                                20091207234612  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091207234612  QCF$                G�O�G�O�G�O�            4100JA  ARGQrqcpt16b                                                                20091207234612  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091208012308                      G�O�G�O�G�O�                