CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901533 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               _A   JA  20091014095452  20091017220219                                  2B  A   APEX-SBE 2882                                                   846 @�R�13�1   @�R�{�?@;�$�/@a`�\)1   ARGOS   A   A   A   @���A  AfffA���A�33A�33B	33B33B2��BE33BX��Bm��B�ffB���B�  B�33B�  B�33B�ffB���B���Bڙ�B�  B���B���C��C� CffC�C�fC� C  C$�C)L�C.  C3L�C7�fC=33CBL�CGL�CQ33C[ffCeffCo��Cy�C�� C��fC��3C�s3C���C�ffC�s3C��3C�� C���C���C���C�� C³3Cǌ�C̳3Cѳ3C�� Cۙ�C���C��C��C� C���C�� D�fD��D��D�fD�3D�fD� D$� D)��D.��D3�3D8��D=�3DBٚDGٚDL��DQ�fDVٚD[� D`ٚDe�3Dj�fDo�3Dt��Dy� D�33D�` D���D�� D��D�VfD��3D��fD�#3D�i�D�� D��3D�&fD�i�Dڣ3D�� D�fD�c3D�3D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�34A��A[33A�  A���A噙BffBffB0  BBffBV  Bj��B��B�fgB���B���B���B���B�  B�fgB�fgB�34B㙚B�fgB�34C �gC��C
�3CfgC33C��CL�C#fgC(��C-L�C2��C733C<� CA��CF��CP� CZ�3Cd�3Cn�gCxfgC�&fC�L�C�Y�C��C�33C��C��C�Y�C�ffC�@ C�33C�33C�&fC�Y�C�33C�Y�C�Y�C�ffC�@ C�33C�33C�33C�&fC�s3C�ffD��D��D� D��D�fD��D�3D$�3D)� D.� D3�fD8� D=�fDB��DG��DL� DQ��DV��D[�3D`��De�fDj��Do�fDt� Dy�3D��D�I�D��4D�ɚD�gD�@ D���D�� D��D�S4D���D���D� D�S4Dڌ�D�ɚD�  D�L�D��D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�oA�oA�VA�  A��A�n�A�A�A�A�$�A�dZA��A�%A�ƨA�+A��A��yA���A��yA��+A��-A��A���A���A���A�{A���A�(�A��A�(�A�ƨA�x�A�I�A�?}A���A�K�A�XA�bNA�I�A�1A�G�A�~�A��A�XA�ZA�XA�ȴA�=qA��A��
A��7A��A��A��A��A�I�A~�A|�uAv�Aq\)Am��Ag�wAfn�A_33A]"�AY�
AT�jAM�PAE��ABbA=�7A1&�A({A"��A�AQ�Av�A�@�ƨ@�&�@�-@�E�@���@�V@���@��R@��@���@�t�@���@�hs@�M�@�S�@� �@���@���@u�@i�@^��@SS�@G\)@>@2~�@-�-@(��@#��@p�@A�@C�@�u@��@	�7@��@?}@x�@   1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�oA�oA�VA�  A��A�n�A�A�A�A�$�A�dZA��A�%A�ƨA�+A��A��yA���A��yA��+A��-A��A���A���A���A�{A���A�(�A��A�(�A�ƨA�x�A�I�A�?}A���A�K�A�XA�bNA�I�A�1A�G�A�~�A��A�XA�ZA�XA�ȴA�=qA��A��
A��7A��A��A��A��A�I�A~�A|�uAv�Aq\)Am��Ag�wAfn�A_33A]"�AY�
AT�jAM�PAE��ABbA=�7A1&�A({A"��A�AQ�Av�A�@�ƨ@�&�@�-@�E�@���@�V@���@��R@��@���@�t�@���@�hs@�M�@�S�@� �@���@���@u�@i�@^��@SS�@G\)@>@2~�@-�-@(��@#��@p�@A�@C�@�u@��@	�7@��@?}@x�@   1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
?}B
@�B
@�B
E�B
H�B
VB
_;B
dZB
m�B
��B
�!B
�qB
�jB
�3B
��B
�sB
�fB
�B
�fB
�B
�B
�mB
�yB
�B
��B
��BB+B	7B
=BDB{B{B�B�B�B�BoBbB\B1BbBuB\BB
��B
�B
�mB
�fB
�TB
�5B
�)B
��B
�dB
�B
��B
��B
z�B
e`B
P�B
7LB
/B
PB
B	�B	�`B	�}B	��B	��B	�B	O�B	33B	�B	%B�B��B�FB��B��B�JB�uB��B��B��B�?BÖB��B�B�TB�B�B��B	+B	 �B	6FB	S�B	o�B	�7B	��B	�qB	��B	�B	��B
B
bB
 �B
,B
9XB
@�B
H�B
Q�B
W
B
]/B
ffB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
?}B
@�B
@�B
E�B
H�B
VB
_;B
dZB
m�B
��B
�!B
�qB
�jB
�3B
��B
�sB
�fB
�B
�fB
�B
�B
�mB
�yB
�B
��B
��BB+B	7B
=BDB{B{B�B�B�B�BoBbB\B1BbBuB\BB
��B
�B
�mB
�fB
�TB
�5B
�)B
��B
�dB
�B
��B
��B
z�B
e`B
P�B
7LB
/B
PB
B	�B	�`B	�}B	��B	��B	�B	O�B	33B	�B	%B�B��B�FB��B��B�JB�uB��B��B��B�?BÖB��B�B�TB�B�B��B	+B	 �B	6FB	S�B	o�B	�7B	��B	�qB	��B	�B	��B
B
bB
 �B
,B
9XB
@�B
H�B
Q�B
W
B
]/B
ffB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091014095451  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091014095452  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091014095452  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091014095453  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091014095453  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091014095454  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091014095454  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091014095454  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091014095454  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091014095454  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091014100135                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091017215526  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091017215759  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091017215800  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091017215800  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091017215800  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091017215801  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091017215801  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091017215801  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091017215801  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091017215801  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091017220219                      G�O�G�O�G�O�                