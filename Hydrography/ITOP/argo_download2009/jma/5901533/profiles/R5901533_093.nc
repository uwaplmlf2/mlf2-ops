CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901533 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ]A   JA  20090924095457  20090927220220                                  2B  A   APEX-SBE 2882                                                   846 @�M�����1   @�M����@;|�hs@a_���+1   ARGOS   A   A   A   @���A��Ac33A���A���A�  B  BffB2ffBFffB[33Bm33B���B�  B�33B���B���B�ffB���B�33BЙ�B�33B�  B�  B���C� CL�C�3CL�C� C33C� C$L�C)L�C.ffC3  C8ffC=ffCA��CG�CQL�C[33Cd�fCn�3Cx��C�s3C�� C�� C�� C��3C�� C���C���C��fC��3C���C�� C���C�C�s3C̀ Cр C֦fC۳3C���C�3C�3C�s3C��C���D�3D��D��DٚD� D� D� D$�fD)��D.��D3�fD8��D=� DB� DG� DL��DQ�fDV�3D[�fD`��De��Dj�3Do� Dt��Dy� D�&fD�\�D�� D�� D�)�D�i�D��3D���D�)�D�p D�� D��fD�,�D�ffDڣ3D��3D�  D�Y�D�D�P 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A  AY��A�  A�  A�33B��B  B0  BD  BX��Bj��B34B���B�  B�fgB���B�33B���B�  B�fgB�  B���B���B���C �fC�3C�C�3C�fC��C�fC#�3C(�3C-��C2ffC7��C<��CA33CF� CP�3CZ��CdL�Cn�Cx  C�&fC�s3C�s3C�s3C�ffC�s3C�L�C�L�C�Y�C�ffC�� C�s3C�@ C�@ C�&fC�33C�33C�Y�C�ffC�L�C�ffC�ffC�&fC�@ C�@ D��D�4D�gD�4D��D��D��D$� D)�4D.�gD3� D8�gD=��DB��DG��DL�gDQ� DV��D[� D`�4De�gDj��Do��Dt�gDy��D�3D�I�D���D���D�gD�VgD�� D�ٚD�gD�\�D���D��3D��D�S3Dڐ D�� D��D�FgD�gD�<�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�1'A�7LA�1'A�JA���A��A��HA�{A���A��A��/A�+A��FA��-A��mA��A��A�bNA���A��A�ƨA��-A�n�A�K�A��A�~�A�ffA�  A�v�A��A�(�A�v�A�z�A��A���A��RA���A��RA��\A��HA�jA�`BA�ZA�n�A�33A�&�A��A�VA��A��+A�1'A���A� �A���A�S�A�-A{��Ax1'AvZAs\)Aq7LAmx�Ai�^AeVA`  AW�APQ�AI�wAE33A?��A:��A3&�A,��A �9A��A��A�A~�A ��@�Q�@�^5@�@�@���@��-@���@�A�@��!@�"�@�G�@���@� �@�/@�X@���@~�y@o�@a&�@K��@F�R@=O�@6ȴ@.E�@*^5@#��@��@Q�@?}@�\@K�@��@��@J@ b?�=q1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�1'A�7LA�1'A�JA���A��A��HA�{A���A��A��/A�+A��FA��-A��mA��A��A�bNA���A��A�ƨA��-A�n�A�K�A��A�~�A�ffA�  A�v�A��A�(�A�v�A�z�A��A���A��RA���A��RA��\A��HA�jA�`BA�ZA�n�A�33A�&�A��A�VA��A��+A�1'A���A� �A���A�S�A�-A{��Ax1'AvZAs\)Aq7LAmx�Ai�^AeVA`  AW�APQ�AI�wAE33A?��A:��A3&�A,��A �9A��A��A�A~�A ��@�Q�@�^5@�@�@���@��-@���@�A�@��!@�"�@�G�@���@� �@�/@�X@���@~�y@o�@a&�@K��@F�R@=O�@6ȴ@.E�@*^5@#��@��@Q�@?}@�\@K�@��@��@J@ b?�=q1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�-B
�FB
�^B
��B
�B
�mB
�sB
��B
��B
�B�B=qB1'B
ƨB
ɺB
��B
ǮB
ŢB
��B
��B
��B
�B
�
B
�
B
�B
��B
�
B
�HB
�B
�B
�BB
=BDBVBuB�B�BuB
=BB
��B
��B
��B%B�B�BoBPB
��B
��B
�yB
�B
��B
�qB
��B
�{B
�B
x�B
jB
^5B
J�B
8RB
!�B
�B	�B	��B	�9B	��B	�B	p�B	T�B	5?B	+B�BBȴB�LB�B��B��B��B�hB�bB�oB��B��B�BB�
B�;B�B��B	1B	{B	�B	=qB	^5B	�B	�-B	�jB	��B	�HB	��B
B
hB
#�B
,B
49B
:^B
C�B
T�B
^5B
e`B
jB
q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�-B
�FB
�^B
��B
�B
�mB
�sB
��B
��B
�B�B=qB1'B
ƨB
ɺB
��B
ǮB
ŢB
��B
��B
��B
�B
�
B
�
B
�B
��B
�
B
�HB
�B
�B
�BB
=BDBVBuB�B�BuB
=BB
��B
��B
��B%B�B�BoBPB
��B
��B
�yB
�B
��B
�qB
��B
�{B
�B
x�B
jB
^5B
J�B
8RB
!�B
�B	�B	��B	�9B	��B	�B	p�B	T�B	5?B	+B�BBȴB�LB�B��B��B��B�hB�bB�oB��B��B�BB�
B�;B�B��B	1B	{B	�B	=qB	^5B	�B	�-B	�jB	��B	�HB	��B
B
hB
#�B
,B
49B
:^B
C�B
T�B
^5B
e`B
jB
q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090924095456  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090924095457  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090924095458  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090924095458  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090924095458  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090924095459  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090924095459  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090924095459  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090924095459  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090924095500  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090924100017                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090927215551  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090927215811  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090927215812  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090927215812  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090927215812  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090927215813  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090927215813  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090927215813  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090927215813  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090927215813  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090927220220                      G�O�G�O�G�O�                