CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   o   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K   CALIBRATION_DATE            	             
_FillValue                  ,  N   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N4   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N8   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N<   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N@   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  ND   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5901588 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090902185708  20091204051555                                  2B  A   APEX-SBE 4067                                                   846 @�HK��P�1   @�HL#Eg�@*��Q�@`�fffff1   ARGOS   B   B   B   @���AffAt��A�  A�  A�ffB��B ��B6  BI��B]��Bo��B�ffB���B�ffB���B���B���B�ffB�ffB�33B�33B㙚B뙚B���C33CL�C  C� CffC�C � C%ffC*�G�O�CH� CR��C\33CfffCp33Cy��C�33C�33C��C�33C��3C�Y�C�&fC��C�@ C��C��C��C�Y�C��C��3C��fC�ٚC��fC��fC�  C��fC��C�  C���C�33D  D` D  D�3D�D&fD��D%  D*  D/�D4�D8��D=��DC  DG��DM  DQ�3DV�3D\  Da�Dey�Dk�Dp�Du�Dy��D�9�D�|�D�� D� D�P D�|�D���D�  D�C3D�s3D�i�D��fD�FfDԆfD�� D�fD�C3D�y�D��3D���111111111111111111111111111111111141111111111111111111111111111111111111111111111111111111111111111111111111111 @�fgA,��A���A�33A�33A���B34B$fgB9��BM34Ba34Bs34B�33B���B�33B���B�fgB�fgB�33B�33B�  B�  B�fgB�fgB�fgC�C33C�fCffCL�C  C!ffC&L�C+  G�O�CIffCS� C]�CgL�Cq�Cz�3C��fC��fC�� C��fC�ffC���C���C���C��3C���C�� C���C���CÌ�C�ffC�Y�C�L�C�Y�C�Y�C�s3C�Y�C��C�s3C�@ C��fD9�D��D9�D,�DFgD` D 34D%9�D*Y�D/S4D4S4D9&gD>&gDCY�DH34DMY�DR,�DW,�D\Y�DaS4De�4DkFgDpFgDuFgDz34D�VgD���D���D�,�D�l�D���D�ٚD��D�` D�� D��gD�3D�c3Dԣ3D���D�#3D�` D�gD�� D���111111111111111111111111111111111141111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ĜA�ƨA���A���A�ĜA�9A��A�A闍A��A�`BA�-A�5?A���A��A╁A�1A�E�A���A��mA��A�1Aۛ�AڋDAأ�A׸RAօA�+AѾwA���A�7LA�p�A�O�G�O�A�$�A���A�A���A��A�1'A�bA��mA�(�A�bNA���A�Ar�/A^�yAQ�PAKK�AFbA?x�A8z�A/\)A'XA��A5?A�jAr�A�hA��Ab@���@�b@�A�@��@�X@���@�ff@�%@Ƨ�@�7L@�"�@�ff@�v�@��`@�ƨ@���@�O�@���@�{@���@�ff@��w@�&�@���@�G�@��@�`B@�I�@��-@|��@t1@g
=@^@P��@H��@A�7@9G�@3o@.v�@)7L@l�@G�@��@r�@�@1'@`B@C�@ Ĝ111111111111111111111111111111111941111111111111111111111111111111111111111111111111111111111111111111111111111 A�ĜA�ƨA���A���A�ĜA�9A��A�A闍A��A�`BA�-A�5?A���A��A╁A�1A�E�A���A��mA��A�1Aۛ�AڋDAأ�A׸RAօA�+AѾwA���A�7LA�p�A�O�G�O�A�$�A���A�A���A��A�1'A�bA��mA�(�A�bNA���A�Ar�/A^�yAQ�PAKK�AFbA?x�A8z�A/\)A'XA��A5?A�jAr�A�hA��Ab@���@�b@�A�@��@�X@���@�ff@�%@Ƨ�@�7L@�"�@�ff@�v�@��`@�ƨ@���@�O�@���@�{@���@�ff@��w@�&�@���@�G�@��@�`B@�I�@��-@|��@t1@g
=@^@P��@H��@A�7@9G�@3o@.v�@)7L@l�@G�@��@r�@�@1'@`B@C�@ Ĝ111111111111111111111111111111111941111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�}B	�qB	�qB	�wB	B	ǮB	ǮB
'�B
K�B
H�B
iyB
k�B
]/B
jBs�B�mB�B)�B?}BB�B<jBT�BYB]/BjBiyB_;BVBN�BJ�BG�BF�BE�BF�G�O�B)�B�B�NB��B|�B9XBB
��B
�B
��B
w�B
/B	�ZB	ÖB	�?B	��B	��B	�B	l�B	P�B	>wB	5?B	B�B	49B	uB�B�B	DB	oB	oB	�B	H�B	^5B	u�B	�VB	��B	�B	��B	B	��B	��B	�/B	�HB	�B	��B	��B
%B
VB
hB
�B
�B
�B
!�B
$�B
%�B
(�B
0!B
49B
:^B
=qB
E�B
K�B
N�B
T�B
YB
]/B
aHB
jB
p�B
u�B
z�B
~�B
�B
�1B
�DB
�P111111111111111111111111111111111141111111111111111111111111111111111111111111111111111111111111111111111111111 B	�}B	�qB	�qB	�wB	B	ǮB	ǮB
'�B
K�B
H�B
iyB
k�B
]/B
jBs�B�mB�B)�B?}BB�B<jBT�BYB]/BjBiyB_;BVBN�BJ�BG�BF�BE�BF�G�O�B)�B�B�NB��B|�B9XBB
��B
�B
��B
w�B
/B	�ZB	ÖB	�?B	��B	��B	�B	l�B	P�B	>wB	5?B	B�B	49B	uB�B�B	DB	oB	oB	�B	H�B	^5B	u�B	�VB	��B	�B	��B	B	��B	��B	�/B	�HB	�B	��B	��B
%B
VB
hB
�B
�B
�B
!�B
$�B
%�B
(�B
0!B
49B
:^B
=qB
E�B
K�B
N�B
T�B
YB
]/B
aHB
jB
p�B
u�B
z�B
~�B
�B
�1B
�DB
�P111111111111111111111111111111111141111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090902185707  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090902185708  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090902185709  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090902185709  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090902185710  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090902185710  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090902185710  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090902185710  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090902185710  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090902185710  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090902185710  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090902190135                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090905065616  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090905065704  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090905065704  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090905065704  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090905065706  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090905065706  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090905065706  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090905065706  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090905065706  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090905065706  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090905065706  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090905070026                      G�O�G�O�G�O�                JA  ARFMdecpA14d                                                                20091204051005  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204051033  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204051033  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204051034  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204051035  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091204051035  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204051035  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204051035  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204051035  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204051035  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204051035  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091204051555                      G�O�G�O�G�O�                