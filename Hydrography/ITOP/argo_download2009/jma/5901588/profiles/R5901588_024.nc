CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   o   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K   CALIBRATION_DATE            	             
_FillValue                  ,  N   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N4   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N8   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N<   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N@   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  ND   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5901588 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090912185729  20091204051553                                  2B  A   APEX-SBE 4067                                                   846 @�Jɝd��1   @�J�	J��@+��R@`���"��1   ARGOS   A   A   A   @�  A&ffAnffA�  A�33A�  B33B!33B2��BJ��B_33BrffB�  B�  B���B�  B���B���B�  B�  B�33B�  B�33B�33B�  C� C�3C��CffC�C  C��C"ffC*ffC/33C4L�C9  C=  CC�3CHL�CR33C\33CfL�Cp� CzffC��C��fC�ٚC�&fC�  C��3C�L�C�@ C��C�L�C��C��3C��C�  C�L�C��C�&fC��C��3C��3C�ٚC�33C�@ C��C�&fD��D  D&fD�D�D�D�3D$�fD*  D.�3D4fD9  D=�3DC3DHfDL�3DR�DW3Dt�fDz�D�@ D���D�� D� D�I�D���D��fD�fD�33D��fD�ɚD���D�L�D�|�D�� D�  D�6fD�y�D���D��3D�I�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A8  A�  A���A�  A���B��B%��B733BO33Bc��Bv��B�33B�33B�  B�33B���B�  B�33B�33B�ffB�33B�ffB�ffB�33C��C��C�4C� C34C�C �4C#� C+� C0L�C5fgC:�C>�CD��CIfgCSL�C]L�CgfgCq��C{� C���C�s3C�fgC��3C���C�� C�ٚC���C���C�ٚC��gC�� C���CÌ�C�ٚCͦgCҳ3CצgC܀ C� C�fgC�� C���C���C��3D@ DFfDl�D` DS3DS3D 9�D%,�D*ffD/9�D4L�D9FfD>9�DCY�DHL�DM9�DRS3DWY�Du,�DzS3D�c3D���D��3D�33D�l�D�� D��D�)�D�VfD���D���D��D�p DԠ D��3D�#3D�Y�D��D�� D��fD�l�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ĜA�hA�VA�33A�&�A�"�A�+A�-A�7LA�C�A�+A��A蕁A�C�A�ZA�M�A��`Aݺ^A�A۝�AڸRAփA�VA��mA�JA�XA�^5A�&�A�  A�VA�ffA��A�{A��RA��/A�C�A�
=A��A���A���A�A�v�A��PA���A��A���AvM�Ap��Ak�Aa��A[�AO&�AH�A<9XA5��A/�A$�9A!hsA�!A�Ar�A�AJA�;@���@���@�%@��@�r�@���@�ƨ@��@�+@��@�&�@���@��;@�@��@��@��+@��
@��+@��@��@�v�@��@�M�@�C�@�I�@~ff@q�@f��@Xr�@Qx�@H1'@Ax�@8�9@2�H@.E�@(�9@$�/@�@�@�@�u@
��@��@~�?��?�j111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�ĜA�hA�VA�33A�&�A�"�A�+A�-A�7LA�C�A�+A��A蕁A�C�A�ZA�M�A��`Aݺ^A�A۝�AڸRAփA�VA��mA�JA�XA�^5A�&�A�  A�VA�ffA��A�{A��RA��/A�C�A�
=A��A���A���A�A�v�A��PA���A��A���AvM�Ap��Ak�Aa��A[�AO&�AH�A<9XA5��A/�A$�9A!hsA�!A�Ar�A�AJA�;@���@���@�%@��@�r�@���@�ƨ@��@�+@��@�&�@���@��;@�@��@��@��+@��
@��+@��@��@�v�@��@�M�@�C�@�I�@~ff@q�@f��@Xr�@Qx�@H1'@Ax�@8�9@2�H@.E�@(�9@$�/@�@�@�@�u@
��@��@~�?��?�j111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	-B	.B	33B	:^B	G�B	bNB	��B	��B	�B	�RB	��B	�BB
m�BjB�dBĜB��BĜB��B��B�
B�}B��B�B�#B�B��B�B�B�B1B�B�B  BB�B��B�B�BB��B{�B\)B'�B
�B
�RB
s�B
B�B
0!B
�B	�B	�
B	�!B	��B	�B	ffB	N�B	!�B	�B		7B��B	�B	�B	JB	oB	%B	  B	VB	�B	6FB	?}B	B�B	bNB	� B	��B	��B	�^B	ÖB	ȴB	��B	�)B	�yB	��B
B
B
B
	7B
\B
{B
&�B
)�B
.B
49B
:^B
A�B
D�B
J�B
N�B
T�B
YB
]/B
bNB
ffB
k�B
r�B
x�B
{�B
�B
�%B
�=B
�PB
�V111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	-B	.B	33B	:^B	G�B	bNB	��B	��B	�B	�RB	��B	�BB
m�BjB�dBĜB��BĜB��B��B�
B�}B��B�B�#B�B��B�B�B�B1B�B�B  BB�B��B�B�BB��B{�B\)B'�B
�B
�RB
s�B
B�B
0!B
�B	�B	�
B	�!B	��B	�B	ffB	N�B	!�B	�B		7B��B	�B	�B	JB	oB	%B	  B	VB	�B	6FB	?}B	B�B	bNB	� B	��B	��B	�^B	ÖB	ȴB	��B	�)B	�yB	��B
B
B
B
	7B
\B
{B
&�B
)�B
.B
49B
:^B
A�B
D�B
J�B
N�B
T�B
YB
]/B
bNB
ffB
k�B
r�B
x�B
{�B
�B
�%B
�=B
�PB
�V111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090912185728  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090912185729  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090912185729  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090912185730  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090912185731  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090912185731  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090912185731  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090912185731  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090912185731  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090912185731  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090912185731  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090912190219                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090915065738  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090915065842  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090915065843  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090915065843  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090915065844  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090915065844  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090915065844  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090915065844  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090915065844  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090915065844  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090915065845  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090915070207                      G�O�G�O�G�O�                JA  ARFMdecpA14d                                                                20091204051006  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204051035  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204051036  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204051036  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204051037  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204051037  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204051037  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204051037  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204051037  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091204051553                      G�O�G�O�G�O�                