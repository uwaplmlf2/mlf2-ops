CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901588 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091011155746  20091015040057                                  2B  A   APEX-SBE 4067                                                   846 @�RK��1   @�RO���G@+�z�G�@`�dZ�1   ARGOS   A   A   A   @�ffA33Ak33A���A���Aٙ�B��B��B4  BH  B^ffBrffB���B�ffB�ffB�  B�  B�ffB�33B�  B�  B�ffB晚B�ffB�  C��C33C�fC  C� C33C 33C%� C)�C/�3C433C9��C>  CC� CH�CR��C\33Ce��CpL�CzffC��C�  C��C��fC��3C��C���C�L�C��C�33C��C�@ C�@ C�33C��C��3C�  C��C��C��fC��C��C�33C�  C��fDfD3D3D��D3D  D &fD$��D)�3D/  D4�D8�3D=�fDB�3DG��DMfDQ��DV�3D\  Da  Df�Dj��Do��Du�Dz3D�9�D���D�� D�  D�9�D�VfD��3D�	�D�,�D��fD�� D��D�C3D�l�D�ɚD�  D�P D��D�fD��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A0  A�  A�33A�  A�  B��B$��B933BM33Bc��Bw��B�34B�  B�  B���B���B�  B���Bʙ�Bԙ�B�  B�34B�  B���C�gC� C33CL�C��C� C!� C&��C*fgC1  C5� C:�gC?L�CD��CIfgCS�gC]� Cf�gCq��C{�3C��3C��fC�� C���C���C�� C�s3C��3C�� C�ٙC�� C��fC��fC�ٙC�� C͙�CҦfC�� C�� C��C�� C�� C�ٙC��fC���DY�DffDffDL�DffDs3D y�D%L�D*FfD/s3D4l�D9FfD>9�DCfDHL�DMY�DRL�DWFfD\s3Das3Dfl�Dk@ Dp@ Du` DzffD�c4D��gD��D�)�D�c4D�� D���D�34D�VgD�� D��D�6gD�l�DԖgD��4D�)�D�y�D��gD�� D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��#A�-A�{A�%A�A���A��A��A��;A��A�A��A��A�9XA���Aڥ�A�C�A�{AԼjA���A��A�dZA�O�A�VA��A�JA��A���A�ffA��^A���A��RA�33A���A�=qA��9A���A���A���A�z�A�Q�A�M�A�I�A�x�A���A�VAx�RAqC�Aj9XAeO�A^n�AVM�AGC�A?G�A0�A.M�A(�A �HA+Av�A��A�A�!A
�`A  A�u@�^5@�l�@�ff@�V@��@��D@�ȴ@�Z@� �@��y@�I�@�^5@��u@�O�@���@�&�@�x�@��y@�;d@�1'@�^5@�t�@��P@���@��F@�7L@��
@��@�$�@�r�@v5?@n@`bN@XA�@M��@DI�@?�P@9��@5��@0�@*M�@%�@�;@��@�@K�@
�\@�R@�D1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��#A�-A�{A�%A�A���A��A��A��;A��A�A��A��A�9XA���Aڥ�A�C�A�{AԼjA���A��A�dZA�O�A�VA��A�JA��A���A�ffA��^A���A��RA�33A���A�=qA��9A���A���A���A�z�A�Q�A�M�A�I�A�x�A���A�VAx�RAqC�Aj9XAeO�A^n�AVM�AGC�A?G�A0�A.M�A(�A �HA+Av�A��A�A�!A
�`A  A�u@�^5@�l�@�ff@�V@��@��D@�ȴ@�Z@� �@��y@�I�@�^5@��u@�O�@���@�&�@�x�@��y@�;d@�1'@�^5@�t�@��P@���@��F@�7L@��
@��@�$�@�r�@v5?@n@`bN@XA�@M��@DI�@?�P@9��@5��@0�@*M�@%�@�;@��@�@K�@
�\@�R@�D1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�5B�/B�)B�)B�#B�#B�)B�#B�/B�;B	n�B	�B
!�B
|�B
�
BbBG�B|�B�B�3B�LB�uB<jBP�B�B�LBǮB��B�JB��B}�Bv�Bu�Bq�BYBu�BXBO�BH�B]/BS�BM�B(�B
�fB
��B
��B
hsB
=qB
hB
B	�;B	�jB	�B	�\B	ffB	dZB	@�B	A�B	L�B	G�B	-B	%�B	!�B	2-B	�B		7B	+B	uB	{B	�B	/B	A�B	P�B	k�B	y�B	�7B	��B	�B	�jB	��B	�)B	�fB	�B	��B	��B
B
+B
JB
hB
�B
�B
 �B
#�B
&�B
(�B
/B
49B
7LB
=qB
A�B
G�B
M�B
P�B
T�B
XB
\)B
aHB
ffB
k�B
p�B
u�B
z�B
� B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�5B�/B�)B�)B�#B�#B�)B�#B�/B�;B	n�B	�B
!�B
|�B
�
BbBG�B|�B�B�3B�LB�uB<jBP�B�B�LBǮB��B�JB��B}�Bv�Bu�Bq�BYBu�BXBO�BH�B]/BS�BM�B(�B
�fB
��B
��B
hsB
=qB
hB
B	�;B	�jB	�B	�\B	ffB	dZB	@�B	A�B	L�B	G�B	-B	%�B	!�B	2-B	�B		7B	+B	uB	{B	�B	/B	A�B	P�B	k�B	y�B	�7B	��B	�B	�jB	��B	�)B	�fB	�B	��B	��B
B
+B
JB
hB
�B
�B
 �B
#�B
&�B
(�B
/B
49B
7LB
=qB
A�B
G�B
M�B
P�B
T�B
XB
\)B
aHB
ffB
k�B
p�B
u�B
z�B
� B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20091011155745  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091011155746  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091011155747  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091011155747  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091011155748  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091011155748  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091011155748  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091011155748  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091011155748  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091011160156                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091015035720  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091015035808  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091015035808  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091015035809  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091015035810  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091015035810  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091015035810  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091015035810  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091015035810  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091015040057                      G�O�G�O�G�O�                