CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901588 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091001125720  20091005040123                                  2B  A   APEX-SBE 4067                                                   846 @�O�c��u1   @�O˳333@+��-@`���l�D1   ARGOS   A   A   A   @�  A	��A[33A���A�33A�ffBffB"��B6  BJffB^ffBrffB���B���B���B���B���B���B���B�33B���B֙�B�33B�  B�33C��C��CffC�C33C�C �C%ffC)�fC.�3C2�C9L�C>��CB�fCH��CRffC\  Ce��CpffCzL�C�&fC�  C�ٚC���C��fC�L�C�  C�L�C��C��3C��3C��fC�  C��C��C�  C�  C��C�&fC�ٚC�33C�L�C�&fC��C�  DfD  D�fD��D��D3D   D%  D*3D/�D3��D8�3D>  DC  DG�3DM�DR  DWfD\3DafDe��Dj�3Dp  Du&fDy�3D�FfD��fD���D���D�<�D���D���D���D�@ D�|�D���D�	�D�6fD�|�D�� D��fD�<�D�3D�� D��3D�C311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�ffA��AnffA�fgA���A�  B33B'��B:��BO33Bc33Bw33B�33B�33B�  B�33B�33B�  B�33Bə�B�33B�  B虙B�ffB���C  C  C��CL�CffCL�C!L�C&��C+�C/�fC3L�C:� C?��CD�CI��CS��C]33Cf��Cq��C{� C�� C���C�s4C�fgC�� C��gC���C��gC��gC���C���C�� C���CægCȳ4C͙�Cҙ�CצgC�� C�s4C���C��gC�� C��gC���DS3DL�D33DFgDFgD` D l�D%L�D*` D/fgD49�D9@ D>l�DCL�DH  DMfgDRL�DWS3D\` DaS3Df9�Dk  Dpl�Dus3Dz@ D�l�D���D��3D�#3D�c3D��3D��3D�  D�ffD��3D�� D�0 D�\�Dԣ3D��fD��D�c3D���D��fD�ٙD�i�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ffA�jA�`BA�XA�XA�VA�K�A�;dA��;A�E�A�~�A��A߰!A�hsA�ZA�~�A�-A��
A݁AܓuAڗ�A�O�A�{A�5?A�ffA���A��HA��A�t�A��`A��mA�M�A�&�A�K�A��+A��#A���A�t�A���A��A��/A�dZA���A��A�`BA|ZApĜAj��Ah�RAg�A[�#AOhsAGVA=G�A:��A7&�A#t�A&�Av�A�
AjA
A��A�@��#@���@�w@�O�@�X@�@��@ו�@Ͼw@���@Ə\@���@�&�@��u@���@�x�@�-@��P@��P@�t�@��T@�M�@�33@��
@�J@���@��@�^5@��9@���@�J@�7L@u�T@l��@a��@Y�#@N{@D�j@?l�@8�@/�@)�^@#t�@;d@~�@��@^5@E�@	%@��@�!@�711111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�ffA�jA�`BA�XA�XA�VA�K�A�;dA��;A�E�A�~�A��A߰!A�hsA�ZA�~�A�-A��
A݁AܓuAڗ�A�O�A�{A�5?A�ffA���A��HA��A�t�A��`A��mA�M�A�&�A�K�A��+A��#A���A�t�A���A��A��/A�dZA���A��A�`BA|ZApĜAj��Ah�RAg�A[�#AOhsAGVA=G�A:��A7&�A#t�A&�Av�A�
AjA
A��A�@��#@���@�w@�O�@�X@�@��@ו�@Ͼw@���@Ə\@���@�&�@��u@���@�x�@�-@��P@��P@�t�@��T@�M�@�33@��
@�J@���@��@�^5@��9@���@�J@�7L@u�T@l��@a��@Y�#@N{@D�j@?l�@8�@/�@)�^@#t�@;d@~�@��@^5@E�@	%@��@�!@�711111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	%B	%B	%B	+B	1B		7B	JB	{B	[#B
�yB��BB�B2-B49B>wB=qBH�BL�BR�BdZBffBaHB_;BVBXBQ�BG�BD�B8RB�BbBVBbB1B��B�B�B�yB��B��BS�B5?B
�B
�FB
k�B
<jB
�B
\B
B	��B	��B	�=B	s�B	iyB	S�B	uB	\B	uB	#�B	�B�B��B	7LB	$�B	<jB	K�B	Q�B	L�B	W
B	r�B	x�B	�PB	��B	��B	�3B	�qB	ȴB	��B	�)B	�BB	�fB	�B	��B	��B
B

=B
hB
{B
�B
�B
�B
!�B
#�B
'�B
.B
49B
8RB
=qB
@�B
F�B
L�B
O�B
T�B
\)B
aHB
ffB
k�B
o�B
s�B
w�B
{�B
�B
�%B
�7B
�D11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B	%B	%B	%B	+B	1B		7B	JB	{B	[#B
�yB��BB�B2-B49B>wB=qBH�BL�BR�BdZBffBaHB_;BVBXBQ�BG�BD�B8RB�BbBVBbB1B��B�B�B�yB��B��BS�B5?B
�B
�FB
k�B
<jB
�B
\B
B	��B	��B	�=B	s�B	iyB	S�B	uB	\B	uB	#�B	�B�B��B	7LB	$�B	<jB	K�B	Q�B	L�B	W
B	r�B	x�B	�PB	��B	��B	�3B	�qB	ȴB	��B	�)B	�BB	�fB	�B	��B	��B
B

=B
hB
{B
�B
�B
�B
!�B
#�B
'�B
.B
49B
8RB
=qB
@�B
F�B
L�B
O�B
T�B
\)B
aHB
ffB
k�B
o�B
s�B
w�B
{�B
�B
�%B
�7B
�D11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20091001125718  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091001125720  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091001125720  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091001125720  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091001125722  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091001125722  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091001125722  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091001125722  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091001125722  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091001130121                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091005035725  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091005035820  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091005035821  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091005035821  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091005035822  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091005035822  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091005035822  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091005035822  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091005035822  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091005040123                      G�O�G�O�G�O�                