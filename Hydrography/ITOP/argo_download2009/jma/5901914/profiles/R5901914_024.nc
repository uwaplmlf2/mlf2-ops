CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901914 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091008155836  20091012070107                                  2B  A   APEX-SBE 4146                                                   846 @�Q�|�F�1   @�Q���w@4�+J@a��9Xb1   ARGOS   A   A   A   @�ffA)��At��A�33Aə�A�  B  B   B3��BG��BTffBq��B�ffB���B�ffB�ffB���B���B�ffB�  B���B�33B�33B�B�ffC��C�3C��C�C�C�fC � C%� C)��C/  C3��C8��C=�3CB�3CG��CRL�C\�3CfffCp33Cz� C�  C��3C�  C��fC��C�&fC���C�  C���C�  C�  C�&fC��3C��C�  C̳3C��C��3C�� C�ٚC�ٚC�� C�3C� C�� D��DfD�DfDfD�3D�3D$ٚD)� D.�fD4�D9&fD>  DB��DHfDM  DR  DWfD\fD`��Df�Dk3Dp�Dt� Dz  D�I�D���D�ɚD�  D�@ D�vfD��fD�fD�FfD���D�ɚD�3D�<�Dԃ3Dڳ3D��D�33D�p D�� D��3D��f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�  A>fgA���A���A�  A�ffB33B%33B8��BL��BY��Bv��B�  B�34B�  B�  B�fgB�fgB�  B˙�B�fgB���B���B�34B�  C�gC  C�gCfgCfgC33C!��C&��C+�C0L�C4�gC9�gC?  CD  CI�CS��C^  Cg�3Cq� C{��C��fC���C��fC���C�� C���C�@ C��fC�s3C��fC��fC���C���C�� CȦfC�Y�Cҳ3Cי�C�ffC� C� C�ffC�Y�C�&fC�ffD��DY�Dl�DY�DY�D&fD FfD%,�D*33D/9�D4l�D9y�D>S3DCL�DHY�DMs3DRS3DWY�D\Y�Da@ Dfl�DkffDp` Du3Dzs3D�s4D��4D��4D�)�D�i�D�� D�� D�0 D�p D��gD��4D�,�D�fgDԬ�D���D�6gD�\�D홚D��D��D� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�S�A�bNA�&�A�oA�FA�hsA���A�ȴA߇+A��A�A���AجA�7LA��A��A�I�A�VA�XAɼjA���A��mA�ĜA��A��A��\A�$�A�;dA�%A���A��A��hA��A�7LA�ffA�A�A��!A�XA�5?A�=qA�oA�hsA�C�A�l�A���A�(�A��A{"�As\)An1Ah�!AfM�AbĜA]hsAXA�AS`BAN�9AJ��AD{A>z�A7�A1��A'�
A$ffA!�A�AO�A��A�FAA�A@�1'@��#@��@�|�@˶F@���@þw@��H@��@��@��@�p�@�ȴ@���@���@�7L@�ƨ@���@�I�@���@���@��@��!@�@t1@j-@]�@UV@J-@A��@9��@2-@-/@'
=@"�@�+@=q@K�@�
@  @I�@��@@�!@��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�S�A�bNA�&�A�oA�FA�hsA���A�ȴA߇+A��A�A���AجA�7LA��A��A�I�A�VA�XAɼjA���A��mA�ĜA��A��A��\A�$�A�;dA�%A���A��A��hA��A�7LA�ffA�A�A��!A�XA�5?A�=qA�oA�hsA�C�A�l�A���A�(�A��A{"�As\)An1Ah�!AfM�AbĜA]hsAXA�AS`BAN�9AJ��AD{A>z�A7�A1��A'�
A$ffA!�A�AO�A��A�FAA�A@�1'@��#@��@�|�@˶F@���@þw@��H@��@��@��@�p�@�ȴ@���@���@�7L@�ƨ@���@�I�@���@���@��@��!@�@t1@j-@]�@UV@J-@A��@9��@2-@-/@'
=@"�@�+@=q@K�@�
@  @I�@��@@�!@��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
VB
VB
T�B
T�B
VB
W
B
`BB
e`B
k�B
t�B
z�B
�3B
ɺB
ɺB
�TB�B:^B9XB>wBJ�BbNBiyBw�B�JB�PB�{B�JB~�Bu�Bl�BiyB_;BZBR�BL�B;dB2-B(�B!�B�B
��B
�B
�B
�fB
��B
�B
��B
~�B
S�B
5?B
�B
{B
B	�ZB	ǮB	�B	��B	}�B	e`B	M�B	.B	bB�B�TB��B��B�XB�3B�B��B�%By�Bx�B�%B��B��B��B�;B�B�B��B	)�B	F�B	W
B	k�B	� B	�DB	�hB	��B	��B	�'B	�}B	��B	��B	�B	�fB	�B
B
�B
�B
$�B
-B
7LB
<jB
H�B
O�B
T�B
\)B
aHB
e`B
k�B
q�B
u�B
z�B
}�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
VB
VB
T�B
T�B
VB
W
B
`BB
e`B
k�B
t�B
z�B
�3B
ɺB
ɺB
�TB�B:^B9XB>wBJ�BbNBiyBw�B�JB�PB�{B�JB~�Bu�Bl�BiyB_;BZBR�BL�B;dB2-B(�B!�B�B
��B
�B
�B
�fB
��B
�B
��B
~�B
S�B
5?B
�B
{B
B	�ZB	ǮB	�B	��B	}�B	e`B	M�B	.B	bB�B�TB��B��B�XB�3B�B��B�%By�Bx�B�%B��B��B��B�;B�B�B��B	)�B	F�B	W
B	k�B	� B	�DB	�hB	��B	��B	�'B	�}B	��B	��B	�B	�fB	�B
B
�B
�B
$�B
-B
7LB
<jB
H�B
O�B
T�B
\)B
aHB
e`B
k�B
q�B
u�B
z�B
}�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20091008155835  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091008155836  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091008155837  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091008155837  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091008155838  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091008155838  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091008155838  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091008155838  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091008155839  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091008160350                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091012065647  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091012065723  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091012065723  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091012065723  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091012065725  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091012065725  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091012065725  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091012065725  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091012065725  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091012070107                      G�O�G�O�G�O�                