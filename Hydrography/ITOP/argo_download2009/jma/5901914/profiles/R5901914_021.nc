CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901914 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090908215840  20090912100007                                  2B  A   APEX-SBE 4146                                                   846 @�J�ʆ1   @�J�؎�@3��x���@a�hr� �1   ARGOS   A   A   A   @���A$��Ax  A�33A�33A�33B  B"��B6  BJ��B]��Bi��B���B�33B���B���B�33B���B�  B�ffBҙ�Bٙ�B���B�33B���C�C� C� CffC��C� C�fC%ffC*ffC/  C4�C9� C>ffCC� CH��CR33C\��Cf  Co�fCz� C��C�L�C��fC��C�� C��3C�� C�@ C�33C�&fC��C�  C�@ C��3C��C��fC�  C�@ C��C�33C�@ C��fC��C��3C�&fD�3D�D3D  D�D��D�3D%fD*  D/fD3��D8� D>  DC3DH�DM�DR  DV�3D[�3Da�De�3Dk�Dp  DufDz  D�@ D���D���D��D�@ D�y�D�ɚD���D�I�D��fD���D�3D�I�Dԓ3D�� D���D�@ D�s3D�3D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A6fgA���A�  A�  A�  B
ffB'33B:ffBO33Bb  Bn  B�  B�ffB�  B�  B�ffB���B�33Bʙ�B���B���B�  B�ffB�  C34C��C��C� C�4C��C!  C&� C+� C0�C534C:��C?� CD��CI�gCSL�C]�4Cg�Cq  C{��C��gC�ٚC�s3C���C�L�C�@ C�L�C���C�� C��3C��gC���C���CÀ Cș�C�s3CҌ�C���Cܙ�C�� C���C�s3C�C�� C��3D9�D` DY�DFfD` D33D 9�D%L�D*FfD/L�D433D9&fD>FfDCY�DH` DM` DRffDW9�D\9�Da` Df9�DkS3DpffDuL�DzffD�c3D�� D�� D�0 D�c3D���D���D�  D�l�D���D�� D�&fD�l�DԶfD��3D�  D�c3D�fD��fD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A�hsA��mA�`BA�I�A�;dA�7LA��A��yA�FA�?}A�JA�+Aס�A���A��A�VA�%A�5?AάA�hsA�&�A���A�5?A��A�\)A�Q�A�`BA��A���A��TA�bA���A���A��A��A��wA�7LA�G�A�1A�ȴA���A���A�?}A��9A�{A�n�A��uA�t�A��/AVA{�hAw�An5?Ai33A`��A\n�AS��ANVAF�HACt�A;�A6�A29XA-S�A*��A'�TA!�A=qA��AAE�@��P@�dZ@�&�@�n�@�/@�`B@�/@��@�"�@��R@�  @�Q�@��@�t�@���@�p�@��@��@�l�@�\)@�Ĝ@���@��y@��@q��@ct�@W�w@L9X@C"�@;�
@1�#@+��@&ff@"��@��@C�@�^@�+@ƨ@�9@{@
�\@l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A�hsA��mA�`BA�I�A�;dA�7LA��A��yA�FA�?}A�JA�+Aס�A���A��A�VA�%A�5?AάA�hsA�&�A���A�5?A��A�\)A�Q�A�`BA��A���A��TA�bA���A���A��A��A��wA�7LA�G�A�1A�ȴA���A���A�?}A��9A�{A�n�A��uA�t�A��/AVA{�hAw�An5?Ai33A`��A\n�AS��ANVAF�HACt�A;�A6�A29XA-S�A*��A'�TA!�A=qA��AAE�@��P@�dZ@�&�@�n�@�/@�`B@�/@��@�"�@��R@�  @�Q�@��@�t�@���@�p�@��@��@�l�@�\)@�Ĝ@���@��y@��@q��@ct�@W�w@L9X@C"�@;�
@1�#@+��@&ff@"��@��@C�@�^@�+@ƨ@�9@{@
�\@l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B	�hB	�PB	�VB	�VB	�\B	�PB	�\B
�BA�B\)Bz�B��B��B��B�B�FB�RB��B�hB�=B��B��B�\B�+B�1B��B��B�B�B�B�B~�Be`BR�BYBQ�BQ�BVBM�BE�B5?B!�B\B
��B
�`B
��B
��B
�!B
��B
�+B
o�B
B�B
)�B	��B	�HB	�^B	��B	� B	l�B	J�B	.B	�B	DB	B��B�sB��BȴB�-B��B��B�VB�1B�DB��BÖB��B�ZB��B	VB	�B	'�B	A�B	O�B	`BB	m�B	�B	�bB	��B	�B	�^B	ŢB	��B	�B	��B
bB
�B
%�B
+B
5?B
6FB
@�B
H�B
N�B
T�B
ZB
]/B
aHB
ffB
k�B
o�B
t�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	��B	�hB	�PB	�VB	�VB	�\B	�PB	�\B
�BA�B\)Bz�B��B��B��B�B�FB�RB��B�hB�=B��B��B�\B�+B�1B��B��B�B�B�B�B~�Be`BR�BYBQ�BQ�BVBM�BE�B5?B!�B\B
��B
�`B
��B
��B
�!B
��B
�+B
o�B
B�B
)�B	��B	�HB	�^B	��B	� B	l�B	J�B	.B	�B	DB	B��B�sB��BȴB�-B��B��B�VB�1B�DB��BÖB��B�ZB��B	VB	�B	'�B	A�B	O�B	`BB	m�B	�B	�bB	��B	�B	�^B	ŢB	��B	�B	��B
bB
�B
%�B
+B
5?B
6FB
@�B
H�B
N�B
T�B
ZB
]/B
aHB
ffB
k�B
o�B
t�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090908215839  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090908215840  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090908215840  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090908215841  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090908215842  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090908215842  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090908215842  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090908215842  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090908215842  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090908220302                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090912095654  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090912095716  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090912095717  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090912095717  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090912095718  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090912095718  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090912095718  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090912095718  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090912095719  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090912100007                      G�O�G�O�G�O�                