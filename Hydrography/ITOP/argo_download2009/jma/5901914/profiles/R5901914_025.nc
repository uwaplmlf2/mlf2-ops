CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901914 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20091018185803  20091022100033                                  2B  A   APEX-SBE 4146                                                   846 @�T��k�1   @�T�L;*@4P��
=q@a��+J1   ARGOS   A   A   A   @���A   Al��A�  A�  A���BffB��B6ffBJ  B^ffBpffB�ffB�33B���B�ffB�ffB�ffB�  B�ffBҙ�B�33B�33B�ffB�33CffC�fC��C�fC��C  C L�C%��C)L�C/� C4��C9ffC>�CCffCH33CQ�3C\�3Cf��CpL�CzL�C�&fC�L�C�33C��fC�ٚC�@ C�&fC�L�C�L�C�33C���C��C��C�33C�ٚC��3C��C�  C�33C��C��C�&fC��3C�&fC��3D�3D3D3D  D�D�D   D$�3D*&fD/�D4�D9�D>3DB��DH�DMfDR  DV��D\  Da  Df�Dj� Dp  Du  DzfD�@ D��fD��3D��D�9�D�y�D���D�	�D�33D���D�� D��D�<�D�s3Dڳ3D��D�L�D�vfD�fD���D�,�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@���A6ffA���A�33A�33A�  B  B%34B<  BO��Bd  Bv  B�33B�  B�fgB�33B�33B�33B���B�33B�fgB�  B�  B�33B�  C��CL�C33CL�C  CffC!�3C'  C*�3C0�fC6  C:��C?� CD��CI��CS�C^�Ch  Cq�3C{�3C�ٙC�  C��fC���C���C��3C�ٙC�  C�  C��fC�� C�� C���C��fCȌ�CͦfC���C׳3C��fC�� C�� C�ٙC�fC�ٙC��fDL�Dl�Dl�Dy�Ds4DfgD Y�D%L�D*� D/s4D4s4D9fgD>l�DCS4DHfgDM` DRy�DWS4D\y�DaY�Dfs4Dk9�DpY�DuY�Dz` D�l�D��3D�� D�9�D�fgD��gD���D�6gD�` D���D���D�9�D�i�DԠ D�� D�9�D�y�D��3D��3D�	�D�Y�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A�jA�RA�^A�!A�A�\)A�\A���A���A��#A��A�dZA��;AԬA�O�A�I�A�S�A��Aɴ9A���A��A�C�A�I�A�jA��A��A���A�JA�r�A���A��9A���A�C�A�$�A��A�n�A�1A��DA�XA�-A��A�VA��HA���A�G�A�1'A�5?A�33A�oA}��Axv�AqƨAjĜAc7LAa��A]7LAX=qAQ�;AO��AG�PAB�uA<�\A7�A4~�A.��A(5?A �\A�A�/A(�A1'@���@��H@�@�+@�K�@�Z@�Q�@��@�
=@��R@��@���@��F@��/@���@��D@�O�@�j@�?}@�v�@��@�hs@~{@m?}@e�@\�j@T�D@Ihs@@  @7�P@1x�@*�H@%�h@!��@�/@%@�/@J@�w@��@	�7@|�@�@V11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A���A�jA�RA�^A�!A�A�\)A�\A���A���A��#A��A�dZA��;AԬA�O�A�I�A�S�A��Aɴ9A���A��A�C�A�I�A�jA��A��A���A�JA�r�A���A��9A���A�C�A�$�A��A�n�A�1A��DA�XA�-A��A�VA��HA���A�G�A�1'A�5?A�33A�oA}��Axv�AqƨAjĜAc7LAa��A]7LAX=qAQ�;AO��AG�PAB�uA<�\A7�A4~�A.��A(5?A �\A�A�/A(�A1'@���@��H@�@�+@�K�@�Z@�Q�@��@�
=@��R@��@���@��F@��/@���@��D@�O�@�j@�?}@�v�@��@�hs@~{@m?}@e�@\�j@T�D@Ihs@@  @7�P@1x�@*�H@%�h@!��@�/@%@�/@J@�w@��@	�7@|�@�@V11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�!B
�'B
�'B
�-B
�-B
�9B
�XB
�`B
�BE�B�JB��B�B�B�B�B�B�B	7B�B�B�fB��B�qB�?B��B��B�uB�hB�uB��B�-B�3B�3B~�Bz�B�PBu�BW
BT�BL�B8RB#�B�BB
��B
�TB
��B
�FB
��B
�JB
q�B
Q�B
+B
B	��B	�NB	��B	�B	��B	x�B	_;B	D�B	/B	�B	1B�B��BĜB�9B��B�%Bx�Bu�Bv�Bx�B�=B��B�)B�
B�B	B	%�B	:^B	;dB	E�B	W
B	m�B	� B	��B	�B	�dB	ǮB	��B	�TB	��B
B
DB
{B
 �B
+B
-B
5?B
@�B
I�B
O�B
T�B
\)B
bNB
ffB
k�B
p�B
t�B
w�B
{�B
|�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
�!B
�'B
�'B
�-B
�-B
�9B
�XB
�`B
�BE�B�JB��B�B�B�B�B�B�B	7B�B�B�fB��B�qB�?B��B��B�uB�hB�uB��B�-B�3B�3B~�Bz�B�PBu�BW
BT�BL�B8RB#�B�BB
��B
�TB
��B
�FB
��B
�JB
q�B
Q�B
+B
B	��B	�NB	��B	�B	��B	x�B	_;B	D�B	/B	�B	1B�B��BĜB�9B��B�%Bx�Bu�Bv�Bx�B�=B��B�)B�
B�B	B	%�B	:^B	;dB	E�B	W
B	m�B	� B	��B	�B	�dB	ǮB	��B	�TB	��B
B
DB
{B
 �B
+B
-B
5?B
@�B
I�B
O�B
T�B
\)B
bNB
ffB
k�B
p�B
t�B
w�B
{�B
|�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20091018185802  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091018185803  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091018185803  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091018185804  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091018185805  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091018185805  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091018185805  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091018185805  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091018185805  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091018190150                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20091022095629  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091022095700  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091022095700  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091022095701  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091022095702  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091022095702  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091022095702  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091022095702  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091022095702  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091022100033                      G�O�G�O�G�O�                