CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   t   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4l   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7$   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;8   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  =   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =|   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C`   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    O    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    O   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    O   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    O   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  O   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    OP   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O`   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Od   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Ot   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Ox   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        O|   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O�Argo profile    2.2 1.2 19500101000000  5901914 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090918185819  20090922070055                                  2B  A   APEX-SBE 4146                                                   846 @�L����1   @�L�L�X^@3��$�/@a��1'1   ARGOS   A   A   A   @fffA��Ax  A���Aə�A�  BffB"  B6  BJffB^  BpffB���B�33B�  B���B�  B���B�ffB�ffB�ffB�  B�  B�  B���C�C� C  C� C��C�fC�3C%  C)�fC/�C4� C933C>�3CCffCHffCR  C\��Ce��Co�fCz�C��C�33C��C��fC�33C�&fC�&fC�@ C�@ C��3C�  C�&fC�ٚC��C��C�&fC��3C��C�33C�ٚC��C�33C��C�  C��fD�D3D�fDfD3D�D �D$� D*fD/  D4  D8��D>�DC  DGٚDMfDR�DWfD\fD`��DeٚDj�fDo��Dt��Dz�D�<�D�y�D�ɚD�fD�FfD���D�� D�3D�<�D�vfD�ɚD���D�FfD�s3D��fD�3D�0 D�s3D�ɚD�fD�C311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�33A$��A�  A���Aљ�A�  BffB&  B:  BNffBb  BtffB���B�33B�  B���B�  B���B�ffB�ffB�ffB�  B�  B�  B���C�C� C  C� C��C�fC �3C&  C*�fC0�C5� C:33C?�3CDffCIffCS  C]��Cf��Cp�fC{�C���C��3C���C�&fC��3C��fC��fC�� C�� C�s3C�� C��fC�Y�CÙ�Cș�CͦfC�s3Cי�Cܳ3C�Y�C晚C�3C���C�� C�ffDY�DS3D&fDFfDS3DY�D L�D%  D*FfD/@ D4@ D99�D>L�DC@ DH�DMFfDRY�DWFfD\FfDa9�Df�Dk&fDp,�Dt��DzL�D�\�D���D��D�&fD�ffD���D�� D�#3D�\�D��fD��D��D�ffDԓ3D��fD�#3D�P D�3D��D�&fD�c311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�/A�
=AˬAʃA��A���A���A��A��A��yA���Aɴ9A� �A�A��AŇ+A��yA��A�%A�"�A��^A�p�A�n�A� �A�+A��A�O�A���A��A�1A��/A���A�
=A��
A��PA�XA��!A���A��A���A�
=A�dZA���A��;A}�Ay��Au�FAp�Amp�AhffA^�RAXZAVn�AR�AM|�AH�`AC�wA>r�A:�jA5��A.5?A)�A&r�A#hsA;dA�A^5A
=A�AXA J@�+@܋D@�V@�Z@�/@��;@��h@�"�@��@��h@���@���@��;@�X@�5?@�(�@�?}@��m@��9@���@���@�dZ@��@�@r=q@eV@Y��@O�w@HQ�@=`B@5�@-@( �@"�@��@n�@$�@��@ȴ@(�@	hs@��@j@��@hs11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�/A�
=AˬAʃA��A���A���A��A��A��yA���Aɴ9A� �A�A��AŇ+A��yA��A�%A�"�A��^A�p�A�n�A� �A�+A��A�O�A���A��A�1A��/A���A�
=A��
A��PA�XA��!A���A��A���A�
=A�dZA���A��;A}�Ay��Au�FAp�Amp�AhffA^�RAXZAVn�AR�AM|�AH�`AC�wA>r�A:�jA5��A.5?A)�A&r�A#hsA;dA�A^5A
=A�AXA J@�+@܋D@�V@�Z@�/@��;@��h@�"�@��@��h@���@���@��;@�X@�5?@�(�@�?}@��m@��9@���@���@�dZ@��@�@r=q@eV@Y��@O�w@HQ�@=`B@5�@-@( �@"�@��@n�@$�@��@ȴ@(�@	hs@��@j@��@hs11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�BF�BG�BD�BC�BI�BJ�BK�BL�BL�BN�BQ�BR�BW
B^5B_;Be`BffBiyBl�Bk�Bk�BhsBo�Be`Bm�B_;BW
BYBI�BC�BB�B49B-B"�B�B{BVB%B
��B
��B
�ZB
��B
ÖB
��B
�VB
}�B
e`B
I�B
9XB
�B	�B	��B	��B	�LB	��B	�%B	n�B	W
B	C�B	,B	DB��B�B�ZB��BɺB�jB�B��B��B�B|�B�B�\B�'B�wB�)B��B	hB	�B	2-B	;dB	P�B	jB	u�B	�+B	�oB	��B	��B	�XB	ŢB	��B	�/B	�ZB	�B	��B
PB
�B
 �B
+B
)�B
33B
>wB
F�B
P�B
T�B
[#B
aHB
gmB
m�B
q�B
u�B
x�B
|�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111BF�BG�BD�BC�BI�BJ�BK�BL�BL�BN�BQ�BR�BW
B^5B_;Be`BffBiyBl�Bk�Bk�BhsBo�Be`Bm�B_;BW
BYBI�BC�BB�B49B-B"�B�B{BVB%B
��B
��B
�ZB
��B
ÖB
��B
�VB
}�B
e`B
I�B
9XB
�B	�B	��B	��B	�LB	��B	�%B	n�B	W
B	C�B	,B	DB��B�B�ZB��BɺB�jB�B��B��B�B|�B�B�\B�'B�wB�)B��B	hB	�B	2-B	;dB	P�B	jB	u�B	�+B	�oB	��B	��B	�XB	ŢB	��B	�/B	�ZB	�B	��B
PB
�B
 �B
+B
)�B
33B
>wB
F�B
P�B
T�B
[#B
aHB
gmB
m�B
q�B
u�B
x�B
|�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA14                                                                 20090918185818  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090918185819  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090918185820  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090918185820  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090918185821  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090918185821  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090918185821  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090918185821  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090918185821  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090918190154                      G�O�G�O�G�O�                JA  ARFMdecpA14                                                                 20090922065712  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090922065752  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090922065752  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090922065753  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090922065755  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090922065755  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090922065755  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090922065755  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090922065755  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090922070055                      G�O�G�O�G�O�                