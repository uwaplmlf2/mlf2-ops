CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               �A   JA  20091001215522  20100129073514  A9_60107_130                    2C  D   APEX-SBE 2341                                                   846 @�O���1   @�O�?��@8l�C��@a��
=p�1   ARGOS   A   A   B   @�ffA  Ai��A���A�ffA陚B��B  B1��BDffBY33Bm��B�ffB�33B�  B�ffB���B���B�ffB���BЙ�Bڙ�B�33B���B���C�3C� CffC� C� C33C33C$33C)�C.� C3L�C833C=ffCBffCGffCQ33C[ffCe�Cn�fCyL�C��3C���C�� C���C��3C���C���C���C���C�s3C���C��3C��fC�� C�ٚC̳3Cь�C֦fCی�C���C� C��C��C�� C���D�3D�fD�3D� D��D� D��D$� D)� D.��D3� D8ٚD=��DB�3DG�fDL��DQ��DV� D[�3D`��De�fDj� Do�3Dt�3DyٚD�  D�Y�D���D��D�,�D�i�D��fD��D�0 D�c3D���D�� D�)�D�Y�Dڣ3D�� D�&fD�Y�D�fD�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33AffA`  A�  A���A���BffB��B/33BB  BV��Bk33B~ffB�  B���B�33B���B���B�33Bř�B�ffB�ffB�  B홚B�ffC�C�fC
��C�fC�fC��C��C#��C(� C-�fC2�3C7��C<��CA��CF��CP��CZ��Cd� CnL�Cx�3C�ffC�� C�s3C�L�C�ffC�L�C�L�C�L�C�@ C�&fC�@ C�ffC�Y�C�s3Cǌ�C�ffC�@ C�Y�C�@ C�L�C�33C�@ C�@ C�s3C�@ D��D� D��D��D�fD��D�3D$��D)��D.�fD3��D8�3D=�3DB��DG� DL�fDQ�fDV��D[��D`�3De� Dj��Do��Dt��Dy�3D��D�FfD���D��fD��D�VfD��3D��fD��D�P D���D���D�fD�FfDڐ D���D�3D�FfD�3D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���Aװ!AӬAѥ�A�O�A͇+A�/A�+A���AǶFAǇ+Aũ�AÉ7AhA��!A���A�A��A���A���A�l�A���A���A�~�A�VA��7A�A�I�A���A��A��;A�A��A�=qA���A���A�VA��A�ƨA��9A���A��RA�%A���A���A��^A��wA��-A��\A��\A�|�A��FA�I�A���A���A���A}C�Ax�+AsG�Ak�Ag�Af�DAbr�A`v�AZn�AQ
=AM�AG/AB��A>�RA.ffA!�Ar�AVA	��A33@��@�@��`@Չ7@��@�5?@�$�@�9X@� �@��@���@���@�5?@��P@���@���@��h@|�D@w
=@i�7@_��@T��@MO�@A�@:�\@6ff@2n�@(A�@"^5@�@o@ȴ@��@E�@	x�@�-@1@�^@ 1'1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���Aװ!AӬAѥ�A�O�A͇+A�/A�+A���AǶFAǇ+Aũ�AÉ7AhA��!A���A�A��A���A���A�l�A���A���A�~�A�VA��7A�A�I�A���A��A��;A�A��A�=qA���A���A�VA��A�ƨA��9A���A��RA�%A���A���A��^A��wA��-A��\A��\A�|�A��FA�I�A���A���A���A}C�Ax�+AsG�Ak�Ag�Af�DAbr�A`v�AZn�AQ
=AM�AG/AB��A>�RA.ffA!�Ar�AVA	��A33@��@�@��`@Չ7@��@�5?@�$�@�9X@� �@��@���@���@�5?@��P@���@���@��h@|�D@w
=@i�7@_��@T��@MO�@A�@:�\@6ff@2n�@(A�@"^5@�@o@ȴ@��@E�@	x�@�-@1@�^@ 1'1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBDB
�B
�TB
�/B
�B
��B
ƨB
��B
��B
�wB
�qB
�XB
�dB
�^B
��B
��B
�HB
�sB
�/B
��B
��B
��B
�B
��B
��B
�B
�B
�fB
�BB
�#B
�B
��B
�B
�B
�`B
�B
B
�^B
�9B
�qB
��B
�B
��B
�!B
��B
�B
�B
��B
��B
��B
�B
�TB
�#B
ɺB
�}B
�B
��B
�B
hsB
F�B
6FB
.B
�B
bB	�B	ǮB	�9B	��B	�1B	u�B	-B	  B�B�qB�?B�B{�Bm�BiyBffBl�Bz�B��B��B�-BÖB�#B�B��B	DB	'�B	>wB	K�B	XB	cTB	~�B	�{B	��B	�jB	��B	�B	�B	��B
�B
&�B
.B
9XB
C�B
P�    B
cTB
l�B
q�B
v�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111411111 BhB
�B
�`B
�BB
�B
��B
ǮB
ÖB
B
�wB
�}B
�dB
�jB
�qB
��B
��B
�TB
�B
�NB
��B
��B
��B
��B
��B
��B
�B
�B
�yB
�HB
�)B
�
B
��B
�B
�#B
�fB
�B
ÖB
�dB
�?B
�qB
��B
�B
��B
�!B
��B
�B
�B
��B
��B
��B
��B
�ZB
�)B
��B
��B
�B
��B
�B
jB
G�B
6FB
/B
�B
oB	�B	ȴB	�FB	��B	�7B	w�B	/B	B�)B�wB�?B�B|�Bn�BjBgmBm�B{�B��B��B�3BĜB�#B�B��B	DB	'�B	>wB	K�B	XB	cTB	~�B	�{B	��B	�jB	��B	�B	�B	��B
�B
&�B
.B
9XB
C�B
P�G�O�B
cTB
l�B
q�B
v�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111411111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
G�O�<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.6(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200910150137232009101501372320091015013723200910150222072009101502220720091015022207201001120000002010011200000020100112000000  JA  ARFMdecpA9_b                                                                20091001215521  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091001215522  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091001215523  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091001215523  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091001215523  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091001215524  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091001215524  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091001215524  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20091001215524  QCF$                G�O�G�O�G�O�              40JA  ARGQaqcp2.8b                                                                20091001215524  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091001215524  QCF$                G�O�G�O�G�O�              40JA  ARGQrqcpt16b                                                                20091001215524  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091001220256                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091005185448  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091005185656  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091005185657  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091005185657  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091005185657  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091005185658  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091005185658  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091005185658  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.8b                                                                20091005185658  QCF$                G�O�G�O�G�O�              40JA  ARGQaqcp2.8b                                                                20091005185658  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091005185658  QCF$                G�O�G�O�G�O�              40JA  ARGQrqcpt16b                                                                20091005185658  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091005190054                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20091005104311  CV  DAT$            G�O�G�O�F�f                JM  ARGQJMQC1.0                                                                 20091005104311  CV  LON$            G�O�G�O�C�5                JM  ARCAJMQC1.0                                                                 20091015013723  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20091015013723  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20091015022207  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100112000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100129073437  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100129073514                      G�O�G�O�G�O�                