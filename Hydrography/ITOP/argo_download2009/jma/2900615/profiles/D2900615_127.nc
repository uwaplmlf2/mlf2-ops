CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   r   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4d   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =T   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  AX   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C    PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E\   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   E�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   N�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   W�   CALIBRATION_DATE      	   
                
_FillValue                  �  `�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    al   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    ap   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    at   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    ax   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a|   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        a�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    a�Argo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20090902035530  20100129073514  A9_60107_127                    2C  D   APEX-SBE 2341                                                   846 @�Hqz���1   @�Ht�� c@8"M���@a��+1   ARGOS   A   A   A   @���A��AfffA���Ař�A홚B	��B  B2ffBD��BZ  Bm33B�33B���B�33B�33B�ffB�33B�  B�ffB���B�33B�33BB�33CffCL�CL�CL�CL�C��CffC$33C)L�C-�fC3L�C8L�C<��CBL�CG  CQffC[��Ce33Co�Cy� C�� C�� C�s3C�� C���C���C�� C���C��3C���C���C�� C���C³3Cǌ�C̦fCь�Cր C�s3C���C��C� CC��3C���D� D�fD��D�fD�3D��D�3D$��D)�3D.�fD3�3D8ٚD=�fDB��DG�3DL�3DQ�3DV�3D[��D`��De�fDj��Do� Dt� DyٚD�fD�c3D�� D��3D��D�ffD�� D���D�  D�i�D��3D��D�&fD�ffDک�D��3D�  D�i�D� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�ffA33A\��A���A���A���B33B��B0  BBffBW��Bj��B~  B���B�  B�  B�33B�  B���B�33Bϙ�B�  B�  B�ffB�  C ��C�3C
�3C�3C�3C  C��C#��C(�3C-L�C2�3C7�3C<33CA�3CFffCP��C[  Cd��Cn� Cx�fC�s3C�33C�&fC�s3C�L�C�@ C�33C�� C�ffC�@ C�@ C�s3C�@ C�ffC�@ C�Y�C�@ C�33C�&fC�@ C�@ C�33C�L�C�ffC�� D��D� D�3D� D��D�fD��D$�fD)��D.� D3��D8�3D=� DB�fDG��DL��DQ��DV��D[�3D`�3De� Dj�fDo��Dt��Dy�3D�3D�P D���D�� D�fD�S3D���D�ɚD��D�VfD�� D��fD�3D�S3DږfD�� D��D�VfD���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A��yA���A���A�^A���A�/A�E�A�%A�ĜA�G�A��!A���A��A���A�=qA��PA��A��A��A���A��hA��;A�O�A���A�bA�5?A���A��A���A��^A��A�(�A� �A�$�A�|�A��A�\)A�XA�ȴA���A�-A�  A��A���A�~�A�A���A��TA��A}hsAyXAv�jAs��Ajv�Ac��A\��AV�yAQhsAL�AIp�AEAA�-A<v�A;+A7��A2��A.ĜA,�jA&ffA��A��A
�9A bN@�+@۾w@�$�@�Z@�n�@��D@�n�@�Q�@�bN@�O�@��@�x�@�M�@� �@��7@���@��@���@zM�@v�y@p�@i%@Xr�@Mp�@Ct�@>V@6{@+dZ@$�/@ �`@t�@|�@��@33@��@�m@��@G�?�l�?��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A��A��yA���A���A�^A���A�/A�E�A�%A�ĜA�G�A��!A���A��A���A�=qA��PA��A��A��A���A��hA��;A�O�A���A�bA�5?A���A��A���A��^A��A�(�A� �A�$�A�|�A��A�\)A�XA�ȴA���A�-A�  A��A���A�~�A�A���A��TA��A}hsAyXAv�jAs��Ajv�Ac��A\��AV�yAQhsAL�AIp�AEAA�-A<v�A;+A7��A2��A.ĜA,�jA&ffA��A��A
�9A bN@�+@۾w@�$�@�Z@�n�@��D@�n�@�Q�@�bN@�O�@��@�x�@�M�@� �@��7@���@��@���@zM�@v�y@p�@i%@Xr�@Mp�@Ct�@>V@6{@+dZ@$�/@ �`@t�@|�@��@33@��@�m@��@G�?�l�?��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�B
�B
�B
��B
��B
�B  B
=B�B�B"�B)�B0!BF�BK�BJ�B6FB>wBD�B7LB'�BPB#�BVB�B'�B7LB�B�BVB�B�BB
�B
�B
�B
�B
��B
��B
��B
��B  BBB
��B
�B
�NB
��B
�jB
�!B
��B
�7B
z�B
gmB
;dB
�B	��B	�5B	ÖB	�B	��B	�=B	y�B	dZB	]/B	J�B	6FB	%�B	�B	B��B��B�{Bs�B\)BVBO�BS�BYBo�B� B�JB�oB�B�^B��B�)B�`B�B��B	�B	)�B	=qB	E�B	ZB	u�B	��B	�B	��B	��B	�HB	��B
DB
{B
!�B
,B
2-B
7LB
=qB
H�B
VB
dZB
q�B
y�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B
�B
�B
�B
��B
��B
�`BB{B�B�B%�B,B1'BF�BL�BL�B9XBB�BF�B:^B)�BVB$�B\B�B(�B8RB�B�B\B�B�B+B
�B
�B
�B
��B
��B  B
��B
��BBBB  B
�B
�TB
��B
�qB
�'B
��B
�=B
{�B
iyB
=qB
�B	��B	�;B	ĜB	�B	��B	�DB	z�B	dZB	^5B	K�B	7LB	&�B	�B	B��B��B��Bt�B]/BW
BP�BT�BZBp�B�B�JB�uB�B�^B��B�)B�`B�B��B	�B	)�B	=qB	E�B	ZB	u�B	��B	�B	��B	��B	�HB	��B
DB
{B
!�B
,B
2-B
7LB
=qB
H�B
VB
dZB
q�B
y�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  <#�
<#�
<#�
<#�
<#�
<D��<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.6(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909150221002009091502210020090915022100200909150433142009091504331420090915043314201001120000002010011200000020100112000000  JA  ARFMdecpA9_b                                                                20090902035529  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090902035530  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090902035531  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090902035531  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090902035531  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090902035532  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090902035532  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090902035532  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090902035532  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090902035532  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090902040423                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090905185500  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090905185728  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090905185728  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090905185728  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090905185729  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090905185730  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090905185730  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090905185730  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090905185730  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090905185730  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090905190142                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090917003940  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090917003940  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090917003941  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090917003941  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090917003941  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090917003941  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090917003941  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090917011410                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090905103440  CV  DAT$            G�O�G�O�F�C�                JM  ARCAJMQC1.0                                                                 20090915022100  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090915022100  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090915043314  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100112000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100129073438  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100129073514                      G�O�G�O�G�O�                