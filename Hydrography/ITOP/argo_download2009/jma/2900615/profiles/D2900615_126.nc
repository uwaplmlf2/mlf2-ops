CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ~A   JA  20090823095440  20100129073514  A9_60107_126                    2C  D   APEX-SBE 2341                                                   846 @�E�u0�x1   @�E��U�b@8["��`B@a��C��1   ARGOS   A   A   A   @�ffA  Ah  A���A���A�33B
��B  B2��BF  BY33Bm��B��B�33B�ffB�ffB�  B���B�  B�  B�  B�  B�33B�ffB�  CffC33C� C�C�C  C�fC$L�C)�C.�C3�C8  C=L�CB��CGffCQL�C[� Ce33Co  CyffC��3C���C�� C��fC���C���C��3C��3C��3C��fC�� C��fC���C¦fCǙ�C̦fCѦfC�s3C�s3C�� C��C�s3C�� C��3C��fD�3D� D��DٚD�3DٚDٚD$��D)��D.��D3��D8� D=��DB� DG�fDL�3DQ��DV�fD[� D`�fDe�3Dj��DoٚDt�fDy� D��D�\�D�� D�� D�)�D�` D���D��D�)�D�` D�� D��fD�,�D�p DڦfD���D��D�c3D��D�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33AffA^ffA���A�  A�ffBffB��B0ffBC��BV��Bk33B}33B�  B�33B�33B���B�ffB���B���B���B���B�  B�33B���C ��C��C
�fC� C� CffCL�C#�3C(� C-� C2� C7ffC<�3CB  CF��CP�3CZ�fCd��CnffCx��C�ffC�L�C�s3C�Y�C�@ C�L�C�ffC�ffC�ffC�Y�C�s3C�Y�C�L�C�Y�C�L�C�Y�C�Y�C�&fC�&fC�33C�@ C�&fC�s3C�ffC�Y�D��D��D�fD�3D��D�3D�3D$�fD)�fD.�3D3�fD8��D=�fDB��DG� DL��DQ�fDV� D[��D`� De��Dj�fDo�3Dt� Dy��D�	�D�I�D���D���D�fD�L�D��fD��fD�fD�L�D���D��3D��D�\�Dړ3D�ٚD�fD�P D�D�6f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A� �A�&�A�A�33A�E�A�A�C�A�?}A�VA��-A��#A�ƨA���A���A��A�ĜA���A��A�r�A�\)A��HA���A�A�n�A��A�=qA�%A�5?A�JA��mA�n�A�O�A��9A��DA�n�A�&�A�Q�A���A���A��^A�$�A���A���A��A��/A�A�A��A��uA�9XA�`BA��jAzQ�Ax=qAsG�Apn�AkG�Ah��Ah �AbE�A^�A[
=AS�7AP�\AM"�AH�RAEO�A@�RA;�A6�A'7LA�A �AoA%@�9X@�@�M�@Ͳ-@ź^@��y@��@��7@� �@���@�ȴ@���@��w@��^@��@�7L@|�@t��@p��@j-@_��@\�D@MO�@C��@=�@9��@3�@-@%?}@V@r�@��@�@�P@��@x�?�\)?�?��?�A�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A� �A�&�A�A�33A�E�A�A�C�A�?}A�VA��-A��#A�ƨA���A���A��A�ĜA���A��A�r�A�\)A��HA���A�A�n�A��A�=qA�%A�5?A�JA��mA�n�A�O�A��9A��DA�n�A�&�A�Q�A���A���A��^A�$�A���A���A��A��/A�A�A��A��uA�9XA�`BA��jAzQ�Ax=qAsG�Apn�AkG�Ah��Ah �AbE�A^�A[
=AS�7AP�\AM"�AH�RAEO�A@�RA;�A6�A'7LA�A �AoA%@�9X@�@�M�@Ͳ-@ź^@��y@��@��7@� �@���@�ȴ@���@��w@��^@��@�7L@|�@t��@p��@j-@_��@\�D@MO�@C��@=�@9��@3�@-@%?}@V@r�@��@�@�P@��@x�?�\)?�?��?�A�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBPBVBVB	7BBDBoB7LB?}BVBW
BS�BP�BH�BF�BD�B@�BA�B:^B7LB'�B#�B%�B"�B �B"�B�B�B(�B33B9XB;dB;dB@�BA�BE�BD�B@�B6FB0!B%B
��B
��B
��B
��B
��B
�B
�NB
�B
ɺB
��B
�B
�JB
�B
gmB
YB
B�B
7LB
1'B
\B	��B	��B	�B	ȴB	�RB	��B	��B	�B	m�B	VB	�B�B��B�!B�hBy�Bm�BgmBffBgmBm�Bo�B�B��B��B�dB��B�B	B	�B	(�B	7LB	K�B	W
B	hsB	�%B	��B	�B	�qB	��B	�B	�ZB	�B
	7B
�B
+B
8RB
G�B
P�B
[#B
aHB
gmB
m�B
v�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 BPBVB\BDBBVB�B>wBI�BXBYBT�BQ�BJ�BH�BH�BA�BB�B=qB;dB(�B$�B%�B$�B!�B$�B�B�B)�B49B9XB;dB<jB@�BB�BG�BF�BB�B7LB2-B+B
��B
��B
��B
��B
��B
�B
�TB
�
B
ɺB
��B
�B
�PB
�B
hsB
ZB
C�B
7LB
2-B
bB
  B	��B	�B	ɺB	�XB	��B	��B	�B	n�B	XB	�B�B��B�'B�oBz�Bn�BhsBgmBhsBn�Bp�B�B��B��B�dB��B�B	B	�B	(�B	7LB	K�B	W
B	hsB	�%B	��B	�B	�qB	��B	�B	�ZB	�B
	7B
�B
+B
8RB
G�B
P�B
[#B
aHB
gmB
m�B
v�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.6(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909050134442009090501344420090905013444200909050408122009090504081220090905040812201001120000002010011200000020100112000000  JA  ARFMdecpA9_b                                                                20090823095439  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090823095440  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090823095441  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090823095441  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090823095441  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090823095442  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090823095442  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090823095442  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090823095442  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090823095443  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090823100322                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090826185453  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090826185728  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090826185729  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090826185729  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090826185729  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090826185730  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090826185730  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090826185730  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090826185730  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090826185731  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090826190227                      G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090917003938  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090917003938  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090917003939  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090917003939  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090917003940  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090917003940  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090917003940  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090917011409                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090826104226  CV  DAT$            G�O�G�O�F�/l                JM  ARCAJMQC1.0                                                                 20090905013444  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090905013444  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090905040812  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20100112000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20100129073436  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100129073514                      G�O�G�O�G�O�                