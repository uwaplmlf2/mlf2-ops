CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901571 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ;A   JA  20091006125531  20091010070607                                  2B  A   APEX-SBE 3470                                                   846 @�Q�So�1   @�Q�V�@+�=p��
@a��
=p�1   ARGOS   A   A   A   @�  A��Ac33A�ffA�33A�ffB��B  B1��BE��BY33Bo33B�ffB�33B���B���B�33B�  B�  B�  B���B�  B�  B�  B�  C�fCL�C��CffC� C� C33C$  C)ffC-�fC3L�C7��C=  CBL�CG33CQL�C[� Ce��CoffCyffC�� C�ffC���C�� C�� C��fC���C��fC���C��fC��3C��3C���C�s3CǙ�C̙�Cь�C�ffCۦfC���C��C�3CC���C���D�3D�fDٚD�3D�3D�3DٚD$�3D)�fD.��D3�fD8ٚD=� DB�3DG� DL��DQ� DV� D[�3D`��De�3Dj�fDo�fDt�3Dy�3D�&fD�p D��fD��fD�&fD�ffD��3D�� D�&fD�i�D��fD�� D�)�D�i�Dڣ3D��3D�#3D�Y�D�3D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AfgA`  A���A���A���B  B33B0��BD��BXffBnffB�  B���B�34B�fgB���B���B���Bř�B�fgBڙ�B䙚BB���C�3C�C��C33CL�CL�C  C#��C)33C-�3C3�C7��C<��CB�CG  CQ�C[L�CefgCo33Cy33C�ffC�L�C�� C�ffC��fC���C�� C���C�s3C���C���C���C�� C�Y�Cǀ C̀ C�s3C�L�Cی�C�� C�s3CꙙC� C��3C��3D�fD��D��D�fD�fD�fD��D$�fD)��D.� D3��D8��D=�3DB�fDG�3DL� DQ�3DV�3D[�fD`��De�fDj��Do��Dt�fDy�fD�  D�i�D�� D�� D�  D�` D���D�ٚD�  D�c4D�� D��D�#4D�c4Dڜ�D���D��D�S4D��D� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�/A띲A�?}A�9XA�9XA�1'A�33A�1'A�-A�+A�+A�"�A�A�`BA�wA��A���A�7A�jA�l�Aܟ�A�A��TAڶFA��A��A̴9Aǲ-A�r�A�VA�/A���A��`A��mA�E�A�bNA���A�`BA���A�bA�v�A�p�A�(�A�&�A�At~�Aj��Ac�AV�AM�PAG�A?K�A=��A;XA5��A37LA/�A'��A ĜA�A�A��A��AA�A��A�FA �/@��H@��@���@��@ۥ�@�I�@��@� �@ȃ@ÍP@�K�@�^5@�ff@��@�J@��H@�$�@��j@�?}@���@��\@�z�@�5?@���@��^@�r�@�l�@�?}@��@zn�@t��@h�u@_+@St�@FV@B��@<��@5�@2�@+S�@$�/@�P@@@�@v�@�@/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�/A띲A�?}A�9XA�9XA�1'A�33A�1'A�-A�+A�+A�"�A�A�`BA�wA��A���A�7A�jA�l�Aܟ�A�A��TAڶFA��A��A̴9Aǲ-A�r�A�VA�/A���A��`A��mA�E�A�bNA���A�`BA���A�bA�v�A�p�A�(�A�&�A�At~�Aj��Ac�AV�AM�PAG�A?K�A=��A;XA5��A37LA/�A'��A ĜA�A�A��A��AA�A��A�FA �/@��H@��@���@��@ۥ�@�I�@��@� �@ȃ@ÍP@�K�@�^5@�ff@��@�J@��H@�$�@��j@�?}@���@��\@�z�@�5?@���@��^@�r�@�l�@�?}@��@zn�@t��@h�u@_+@St�@FV@B��@<��@5�@2�@+S�@$�/@�P@@@�@v�@�@/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
m�B
o�B
p�B
p�B
q�B
q�B
q�B
q�B
s�B
v�B
v�B
x�B
�B
��B�B�-B�/B��BB{B)�B/B.B33BZBW
B_;BL�B6FB$�BbBB��B�yB�BƨB�jB�'B�B��B�BG�B
�sB
�B
]/B
"�B	��B	�BB	��B	�=B	v�B	^5B	^5B	cTB	W
B	T�B	H�B	:^B	49B	"�B	  B		7B	�B	�B	(�B	(�B	33B	9XB	7LB	:^B	8RB	B�B	[#B	x�B	�B	��B	�B	�RB	ĜB	��B	�
B	�NB	�B	�B	��B
+B
uB
�B
�B
�B
$�B
,B
-B
.B
1'B
5?B
9XB
<jB
A�B
E�B
K�B
R�B
VB
ZB
^5B
aHB
hsB
m�B
n�B
r�B
v�B
{�B
}�B
�B
�%1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
m�B
o�B
p�B
p�B
q�B
q�B
q�B
q�B
s�B
v�B
v�B
x�B
�B
��B�B�-B�/B��BB{B)�B/B.B33BZBW
B_;BL�B6FB$�BbBB��B�yB�BƨB�jB�'B�B��B�BG�B
�sB
�B
]/B
"�B	��B	�BB	��B	�=B	v�B	^5B	^5B	cTB	W
B	T�B	H�B	:^B	49B	"�B	  B		7B	�B	�B	(�B	(�B	33B	9XB	7LB	:^B	8RB	B�B	[#B	x�B	�B	��B	�B	�RB	ĜB	��B	�
B	�NB	�B	�B	��B
+B
uB
�B
�B
�B
$�B
,B
-B
.B
1'B
5?B
9XB
<jB
A�B
E�B
K�B
R�B
VB
ZB
^5B
aHB
hsB
m�B
n�B
r�B
v�B
{�B
}�B
�B
�%1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20091006125530  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091006125531  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091006125532  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091006125532  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091006125533  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091006125533  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091006125533  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091006125533  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091006125534  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091006130035                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20091010065940  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20091010070128  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091010070128  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091010070129  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091010070130  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091010070130  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20091010070130  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20091010070130  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091010070130  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091010070607                      G�O�G�O�G�O�                