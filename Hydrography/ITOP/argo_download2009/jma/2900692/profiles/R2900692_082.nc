CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   f   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  44   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  64   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  84   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  9�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :4   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ;�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <4   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  ?d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  h  Ad   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  Cd   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    C�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    F�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    I�   CALIBRATION_DATE            	             
_FillValue                  ,  L�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    L�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    L�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    L�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    L�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  L�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    M   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    M    HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    M$   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         M4   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         M8   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        M<   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    M@Argo profile    2.2 1.2 19500101000000  2900692 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               RA   JA  20090914095548  20090916190228                                  2B  A   APEX-SBE 3037                                                   846 @�K2�"�1   @�K2�(��@=�����@bE�-V1   ARGOS   A   A   A   @�  A33AfffA���A�  A�  B
  B��B333BG33BZffBnffB���B�33B�  B�ffB�ffB���B���B�  B���B���B䙚B�33B���CffC� C33C33CffC�C  C#�fC)��C.L�C3L�C8� C=��CB� CG33CQL�C[ffCe�Co33Cy� C�� C�� C��3C��fC³3C�� C̳3C�s3Cֳ3Cۙ�C�s3C�3CꙚC�3C���C��fDٚD�fD�3D��D�3D�3D�3D$ٚD)�3D.��D3�fD8�3D=�fDBٚDG� DLٚDQ��DV�fD[�3D`� De��Dj�3Do� Dt� Dy�3D�&fD�Y�D��fD���D��D�ffD���D��D�&fD�c3D���D�33D�&fD�c3D��D� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�ffAffAa��A�fgA���A陚B��B��B2  BF  BY33Bm33B�33B���B�ffB���B���B�  B�33B�ffB�33B�33B�  BB�33C�C33C
�fC�fC�C��C�3C#��C)L�C.  C3  C833C=L�CB33CF�fCQ  C[�Cd��Cn�fCy33C�Y�C�Y�C���C�� C�CǙ�Č�C�L�C֌�C�s4C�L�C��C�s4C��C�s4C�� D�gD�3D� D��D� D� D� D$�gD)� D.��D3�3D8� D=�3DB�gDG��DL�gDQ��DV�3D[� D`��De�gDj� Do��Dt��Dy� D��D�P D���D��3D� D�\�D��3D�� D��D�Y�D�� D�)�D��D�Y�D�3D�f111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A���A��#A��#A��;A��;A�ƨAڕ�AǙ�A�z�A��A��HA�K�A��;A�M�A�t�A�ffA�  A��HA��yA�hsA�A�v�A��hA�E�A�9XA��A���A���A�dZA���A�9XA�~�A��^A�z�A�"�A���A���A��A�r�A���A���A��A��A��A�bNA��7G�O�Aq"�Ak�FAg�Ab��A]x�A[��AW��AR~�AOXAJ�yAF�ABQ�A=�
A1�A%��AA��A	��@�K�@�ƨ@�Z@ʏ\@Ĵ9@�E�@�O�@�^5@���@�  @�V@�bN@�ƨ@�@��@���@�C�@��@��^@\)@l(�@aG�@X�u@Nv�@Dj@?
=@8��@1&�@*=q@%�hG�O�@�@	&�@O�@��?�X111111111111111111111111111111111111111111111119111111111111111111111111111111111111111111111111911111  A���A���A��#A��#A��;A��;A�ƨAڕ�AǙ�A�z�A��A��HA�K�A��;A�M�A�t�A�ffA�  A��HA��yA�hsA�A�v�A��hA�E�A�9XA��A���A���A�dZA���A�9XA�~�A��^A�z�A�"�A���A���A��A�r�A���A���A��A��A��A�bNA��7G�O�Aq"�Ak�FAg�Ab��A]x�A[��AW��AR~�AOXAJ�yAF�ABQ�A=�
A1�A%��AA��A	��@�K�@�ƨ@�Z@ʏ\@Ĵ9@�E�@�O�@�^5@���@�  @�V@�bN@�ƨ@�@��@���@�C�@��@��^@\)@l(�@aG�@X�u@Nv�@Dj@?
=@8��@1&�@*=q@%�hG�O�@�@	&�@O�@��?�X111111111111111111111111111111111111111111111119111111111111111111111111111111111111111111111111911111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	��B	��B	��B	��B	��B	��B	�
B
B
N�B
�PB
��B
�qB
��B
�LB
��B
��B
ƨB
��B
�B
�B
�TB
�sB
�B
�B
�B
��BVBB
��B
��B
��B  B
��B
��B\BhB�B�B�BuB+BBB
��B
��B
��B
�G�O�B
[#B
D�B
-B
�B
B	��B	�sB	��B	ƨB	�3B	��B	�oB	}�B	E�B	PB�NB�wB�oBcTB=qB%�B-B2-B7LB=qBP�B[#Bo�B� B�bB��B�!BB��B�5B�yB��B	\B	<jB	\)B	r�B	�DB	��B	�3B	ĜB	�B	�yB	��G�O�B
=qB
G�B
R�B
^5B
gm111111111111111111111111111111111111111111111119111111111111111111111111111111111111111111111111911111  B	��B	��B	��B	��B	��B	��B	�
B
B
N�B
�PB
��B
�qB
��B
�LB
��B
��B
ƨB
��B
�B
�B
�TB
�sB
�B
�B
�B
��BVBB
��B
��B
��B  B
��B
��B\BhB�B�B�BuB+BBB
��B
��B
��B
�G�O�B
[#B
D�B
-B
�B
B	��B	�sB	��B	ƨB	�3B	��B	�oB	}�B	E�B	PB�NB�wB�oBcTB=qB%�B-B2-B7LB=qBP�B[#Bo�B� B�bB��B�!BB��B�5B�yB��B	\B	<jB	\)B	r�B	�DB	��B	�3B	ĜB	�B	�yB	��G�O�B
=qB
G�B
R�B
^5B
gm111111111111111111111111111111111111111111111119111111111111111111111111111111111111111111111111911111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090914095547  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090914095548  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090914095548  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090914095548  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090914095549  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090914095550  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090914095550  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090914095550  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090914095550  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090914095550  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090914100205                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090916185531  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090916185754  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090916185754  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090916185754  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090916185754  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090916185755  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090916185755  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090916185756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090916185756  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090916185756  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090916190228                      G�O�G�O�G�O�                