CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900692 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               QA   JA  20090903005521  20090906160252                                  2B  A   APEX-SBE 3037                                                   846 @�H��R��1   @�H����@=�Ƨ@bC333331   ARGOS   A   A   A   @�33AffAa��A�  A�  A���B	��B��B1��BG33BZ  BlffB���B���B�ffB���B�33B�33B�  B�33B�  Bڙ�B�ffB�ffB�ffC�C33CL�C  CffCL�CffC$  C)L�C.�C3  C7�fC<��CA�fCG  CQ  C[�Ce��CoffCy��C��fC�� C�s3C�ffC�� C��3C���C�� C��fC��fC���C���C��fC CǙ�Č�Cѳ3Cֳ3Cۀ C�� C�s3C�3C�fC��C���DٚD� D�3D��D��DٚDٚD$� D)�fD.� D3�3D8��D=�fDB� DGٚDLٚDQ��DV�fD[�fD`��De��Dj��Do�3DtٚDy��D�#3D�l�D���D��fD�  D�ffD���D��3D�&fD�c3D��fD���D�)�D�ffDڬ�D��D�,�D�\�D�fD�P 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33AffAY��A�  A�  A���B��B��B/��BE33BX  BjffB34B���B�ffB���B�33B�33B�  B�33B�  Bٙ�B�ffB�ffB�ffC ��C�3C
��C� C�fC��C�fC#� C(��C-��C2� C7ffC<L�CAffCF� CP� CZ��Ce�Cn�fCy�C�ffC�@ C�33C�&fC�@ C�s3C�Y�C�@ C�ffC�ffC���C���C�ffC�@ C�Y�C�L�C�s3C�s3C�@ C�� C�33C�s3C�ffC�L�C���D��D� D�3D��D��D��D��D$� D)�fD.� D3�3D8��D=�fDB� DG��DL��DQ��DV�fD[�fD`��De��Dj��Do�3Dt��Dy��D�3D�\�D���D��fD� D�VfD���D��3D�fD�S3D��fD���D��D�VfDڜ�D�ٚD��D�L�D�fD�@ 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��mA��A��A��`A��TA��7A��A�K�A���A�ZA���A��+A�z�A���A�A��A�VA��HA�$�A�v�A��#A�x�A�n�A���A�v�A���A��DA���A�n�A��A�$�A�ƨA��DA�9XA�|�A�E�A�ȴA�33A��A�oA�"�A���A�33A�A�A���A��FA��-A��-A��-A��yA�$�A~~�Az��Aw�^AtA�An�`Ah��Af�AbA�A_hsAZE�AV$�AR�+AM��AIVAE|�A?�;A9��A4��A*r�A#C�A�9A�!A
=A�@��@�"�@��@�Ĝ@���@��7@�A�@���@���@�A�@�;d@�Q�@���@�-@��@���@�w@x�9@s��@i%@]p�@TI�@L1@D�D@<Z@4��@-�-@)%@#�@ȴ@G�@��@�@��@�@�@33@hs?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��mA��A��A��`A��TA��7A��A�K�A���A�ZA���A��+A�z�A���A�A��A�VA��HA�$�A�v�A��#A�x�A�n�A���A�v�A���A��DA���A�n�A��A�$�A�ƨA��DA�9XA�|�A�E�A�ȴA�33A��A�oA�"�A���A�33A�A�A���A��FA��-A��-A��-A��yA�$�A~~�Az��Aw�^AtA�An�`Ah��Af�AbA�A_hsAZE�AV$�AR�+AM��AIVAE|�A?�;A9��A4��A*r�A#C�A�9A�!A
=A�@��@�"�@��@�Ĝ@���@��7@�A�@���@���@�A�@�;d@�Q�@���@�-@��@���@�w@x�9@s��@i%@]p�@TI�@L1@D�D@<Z@4��@-�-@)%@#�@ȴ@G�@��@�@��@�@�@33@hs?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
z�B
z�B
z�B
y�B
y�B
u�B
ffB
\)B
bNB
o�B
�B
�XB
ƨB
��B
��B
��B
�TB
�NB
�B
�HB
�B
��B
��B
��B
��BhB\B
��B
��B
��B
��B
�B
�B
�B
��B
�B
�B
�B
�B
�mB
�B
�fB
��B
��B
��B
�B
�yB
�BB
�B
��B
�wB
�B
��B
�VB
�B
p�B
W
B
<jB
0!B
!�B
uB	��B	�B	�B	B	�B	��B	�+B	jB	Q�B	%�B	B��B��B�DBk�BQ�B1'B/B �B+B;dBI�BQ�B`BBt�B�hB��B�'B�wB�/B�B��B	VB	�B	=qB	cTB	y�B	�hB	��B	�qB	��B	�HB	�B	��B
	7B
�B
%�B
5?B
?}B
F�B
N�B
VB
]/B
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
z�B
z�B
z�B
y�B
y�B
u�B
ffB
\)B
bNB
o�B
�B
�XB
ƨB
��B
��B
��B
�TB
�NB
�B
�HB
�B
��B
��B
��B
��BhB\B
��B
��B
��B
��B
�B
�B
�B
��B
�B
�B
�B
�B
�mB
�B
�fB
��B
��B
��B
�B
�yB
�BB
�B
��B
�wB
�B
��B
�VB
�B
p�B
W
B
<jB
0!B
!�B
uB	��B	�B	�B	B	�B	��B	�+B	jB	Q�B	%�B	B��B��B�DBk�BQ�B1'B/B �B+B;dBI�BQ�B`BBt�B�hB��B�'B�wB�/B�B��B	VB	�B	=qB	cTB	y�B	�hB	��B	�qB	��B	�HB	�B	��B
	7B
�B
%�B
5?B
?}B
F�B
N�B
VB
]/B
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20090903005520  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090903005521  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090903005522  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090903005522  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090903005522  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090903005523  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090903005523  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090903005523  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090903005523  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090903005523  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090903010347                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090906155532  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090906155757  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090906155758  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090906155758  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090906155758  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090906155759  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090906155759  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090906155759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090906155759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090906155800  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090906160252                      G�O�G�O�G�O�                