% Argos float data ITOP
clear all
% for fi=1:5
%     if fi==1;fold0='aoml';
%     elseif fi==2; fold0='coriolis';
%     elseif fi==3; fold0='gts';
%     elseif fi==4; fold0='bodc';
%     elseif fi==5; fold0='meds';
%     end
%     D=dir(['Data07/' fold0]);
%     k=0;kk=0;
%     for i=3:1:length(D)
%         k=k+1;
% %         if k>16
% %             k=1;
% %             drawnow
% %             %print
% %             clf
% %         end
% %         subplot(4,4,k);
%
%         folder=['Data07/' fold0 '/' D(i).name]
%         id=str2num(D(i).name);
%         F=dir([folder '/profiles']);
%         for j=3:length(F)
%
%             file=[folder '/profiles/' F(j).name];
k=0;kk=0;
D0=dir('argo*');
for i0=1:length(D0)
    D1=dir(D0(i0).name);
    for i1=1:length(D1)
        D2=dir([D0(i0).name '/' D1(i1).name]);
        for i2=1:length(D2)
            D3=dir([D0(i0).name '/' D1(i1).name '/' D2(i2).name '/profiles/*.nc']);
            for i3=1:length(D3)
                file=[D0(i0).name '/' D1(i1).name '/' D2(i2).name '/profiles/' D3(i3).name];
                id=str2num(D2(i2).name);
                warning off MATLAB:nonIntegerTruncatedInConversionToChar
                S=getnc(file,'STATION_PARAMETERS');  % matrix (n,16);
                warning on MATLAB:nonIntegerTruncatedInConversionToChar
                if size(S,1)>2  % includes salinity
                    kk=kk+1;
                    P=getnc(file,'PRES');
                    T=getnc(file,'TEMP');
                    Tcorr=getnc(file,'TEMP_ADJUSTED');
                    TQ=str2num(getnc(file,'TEMP_QC'));
                    S=getnc(file,'PSAL');
                    Scorr=getnc(file,'PSAL_ADJUSTED');
                    SQ=str2num(getnc(file,'PSAL_QC'));
                    jd=getnc(file,'JULD');  % days since 00Z 1 Jan 1950
                    lat=getnc(file,'LATITUDE');
                    lon=getnc(file,'LONGITUDE');
                    yr0=jd/365.25+1950;  % Rough conversion - need better
                    daystr0=datenum('01-jan-1950');
                    mtime=jd+daystr0; % matlab time
                    yrs= datestr(mtime,10);
                    yr=str2num(yrs);  % year
                    yd=mtime-datenum(['01-jan-' yrs]);  % yearday
                    mo=floor(yd/365*12)+1;  % equal spaced month
                    fprintf(1,'%6.2f %4.0f %4.1f  %2.0f %d\n',yr0,yr,yd,mo,id)
%                     %plot(T,-P);
%                     hold on
%                     set(gcf,'Color','w');
                    %title(D(i).name);
                    prof(kk).P=P;
                    prof(kk).T=T;
                    prof(kk).Tcorr=Tcorr;
                    prof(kk).TQ=TQ;
                    prof(kk).S=S;
                    prof(kk).SQ=SQ;
                    prof(kk).Scorr=Scorr;
                    prof(kk).lat=lat;
                    prof(kk).lon=lon;
                    prof(kk).jd=jd;
                    prof(kk).yr=yr;
                    prof(kk).mo=mo;
                    prof(kk).id=id;
                    prof(kk).yd=yd;
                    %plot(lon,lat,'o');
                end
            end
        end
    end
end
save 'ArgoITOP.mat' prof
