CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   D   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   units         decibar    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   	valid_min                	valid_max         F;�    
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   units         decibar    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   	valid_min                	valid_max         F;�    
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  5    PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   
resolution        =���       5D   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min         �      	valid_max         B      
resolution        :�o       6T   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  7d   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min         �      	valid_max         B      
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   
resolution        :�o       8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     units         psu    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min                	valid_max         B(     
resolution        :�o       :   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  ;   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     units         psu    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min                	valid_max         B(     
resolution        :�o       ;`   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  <p   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   
resolution        :�o       <�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  =�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C�   CALIBRATION_DATE            	             
_FillValue                  ,  F�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G    HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G$   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G(   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G,   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G0   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Gp   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   units         decibar    
_FillValue        G�O�        G�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    units         decibar    
_FillValue        G�O�        G�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G�Argo profile    2.2 1.2 19500101000000  2900427 Argo METRI/KMA                                                  Kim, Jeong-Sik                                                  PRES            TEMP            PSAL               hA   KM  20070912060138  20090702102000  A0_01864_006                    2C  D   APEX-SBE 1584                                                   846 @Ԕ8�m�1   @ԔMk��J@/��hr�!@_�~��"�1   ARGOS   B   B   B   A  A�ffA陚B33BE��BnffB���B���B�ffB���B���BC��CffC  C�fC(��C3L�C=ffCGffC[��CoffC�� C��fC��fC��fC��3C�� C���CǙ�C�s3C�s3C��C�3C���D�fD�fDٚD��D�fD� D�3D$�fD)�fD.��D3�3D8�3D=�3DB� DG� DN�DTY�DZ��D`�3Dg  DmFfDs�fDy�3D�&fD�ffD��fD�c3D��fD�i�D��fD�p D��D�` 11114111111111111111111111111111111111111111111111111111111111111111A  A�ffA陚B33G�O�BnffB���B���B�ffB���B���BC��CffC  C�fC(��C3L�C=ffCGffC[��CoffC�� C��fC��fC��fC��3C�� C���CǙ�C�s3C�s3C��C�3C���D�fD�fDٚD��D�fD� D�3D$�fD)�fD.��D3�3D8�3D=�3DB� DG� DN�DTY�DZ��D`�3Dg  DmFfDs�fDy�3D�&fD�ffD��fD�c3D��fD�i�D��fD�p D��D�` 11114111111111111111111111111111111111111111111111111111111111111111@��@��@��@��G�O�@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�+A�ȴA���A왚A�A�ƨA�VA�G�A�ZA�O�A��A�ffA�&�A��DA�ĜA��A� �A�/A��
A��yA��A���A�oA��!A�ffArJAa��AQ�TAG\)A8��A3x�A)��A" �Ax�Av�@�(�@��`@�=q@�&�@�1@��@˝�@�
=@���@��@���@���@�Z@�;d@��@�=q@�J@��R@�(�@��h@�"�@�b@�=q@�z�@}�@fff@T�@Dj@5@)�@"��@��@�y11114111111111111111111111111111111111111111111111111111111111111111A�+A�ȴA���A왚G�O�A�ƨA�VA�G�A�ZA�O�A��A�ffA�&�A��DA�ĜA��A� �A�/A��
A��yA��A���A�oA��!A�ffArJAa��AQ�TAG\)A8��A3x�A)��A" �Ax�Av�@�(�@��`@�=q@�&�@�1@��@˝�@�
=@���@��@���@���@�Z@�;d@��@�=q@�J@��R@�(�@��h@�"�@�b@�=q@�z�@}�@fff@T�@Dj@5@)�@"��@��@�y11114111111111111111111111111111111111111111111111111111111111111111;o;o;o;oG�O�;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�B	�B	�RB	�yB	�B
�B
49B
R�B
�B
�FB/B
��B^5Bo�Bv�B� B�=B��B�\B�JBdZBI�B9XB�B
�jB
^5B
\B	��B	��B	gmB	S�B	/B	�B�B�sB��B	6FB	>wB	J�B	XB	iyB	�B	�bB	��B	�B	�dB	ǮB	��B	�;B	�sB	��B	��B
B
DB
\B
uB
�B
�B
#�B
/B
;dB
F�B
P�B
ZB
gmB
n�B
z�B
�11114111111111111111111111111111111111111111111111111111111111111111B	��B	�sB	�B	�BG�O�B
�B
+B
I�B
z�B
�B%�B
�BT�BffBm�Bv�B�B�bB�%B�B[#B@�B0!B�B
�3B
T�B
%B	ŢB	��B	^5B	J�B	%�B	\B�`B�;B�B	-B	5?B	A�B	N�B	`BB	z�B	�+B	��B	��B	�-B	�wB	ɺB	�B	�;B	�B	��B	��B
B
%B

=B
VB
hB
�B
%�B
2-B
=qB
G�B
P�B
^5B
e`B
q�B
z�11114111111111111111111111111111111111111111111111111111111111111111:�o:�o:�o:�oG�O�:�o:�o:�o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�o:�oPRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            No sifnificant pressure drift detected                                                                                                                                                                                                                          No significant temprature drift detected                                                                                                                                                                                                                        Significant salinity drift present; OW weighted least squares fit is adopted;       Map Scales:[x:8/4,y:4/2]; max_breaks=4;  T<5.0DegC                                                                                                                          200907021005002009070210050020090702100500  KM      ARGQ1.0                                                                 20070913021002  QCP$PSAL            G�O�G�O�G�O�  11110110111111KD  ARSQOW  V1.1SeHyD and ARGO_for_DMQC Climatology Version 2008V04             20090702100500  IP  PSAL            G�O�G�O�G�O�                