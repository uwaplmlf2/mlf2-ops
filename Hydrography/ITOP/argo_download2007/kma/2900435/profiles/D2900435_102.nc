CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   E   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   units         decibar    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   	valid_min                	valid_max         F;�    
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   units         decibar    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   	valid_min                	valid_max         F;�    
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
_FillValue        G�O�   
resolution        =���       5T   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min         �      	valid_max         B      
resolution        :�o       6h   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7|   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min         �      	valid_max         B      
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   
resolution        :�o       9    PSAL         
      	   	long_name         PRACTICAL SALINITY     units         psu    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min                	valid_max         B(     
resolution        :�o       :4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;H   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     units         psu    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   	valid_min                	valid_max         B(     
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
_FillValue        G�O�   
resolution        :�o       <�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >    SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >0   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A0   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D0   CALIBRATION_DATE            	             
_FillValue                  ,  G0   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G\   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G`   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Gd   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Gh   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Gl   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   units         decibar    
_FillValue        G�O�        G�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    units         decibar    
_FillValue        G�O�        G�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G�Argo profile    2.2 1.2 19500101000000  2900435 Argo METRI/KMA                                                  Jang-Won Seo                                                    PRES            TEMP            PSAL               fA   KM  20070823003516  20081015153400  A0_01864_006                    2C  D   APEX-SBE 1881                                                   846 @ԏ)�A��1   @ԏ>""""@0>��"��@_���Q�1   ARGOS   A   A   A   A33A�33A���B��BF  Bl��B�  B�33B�33B�  B�ffB�33C� C  CffCL�C)33C333C=�CG33C[L�CoffC���C�� C�� C�ffC��fC���C�� C�s3C�� Cی�C噚CC��fD��DٚD�3D�3D�3D��DٚD$��D)��D.ٚD3ٚD8� D=ٚDB�3DG� DN�DT9�DZ�fD`� Dg�DmL�Dsy�Dy��D�,�D�i�D��D�` D�ٚD�\�D��fD�i�D�� D�c3D�� 111111111111111111111111111111111111111111111111111111111111111111111   A33A�33A���B��BF  Bl��B�  B�33B�33B�  B�ffB�33C� C  CffCL�C)33C333C=�CG33C[L�CoffC���C�� C�� C�ffC��fC���C�� C�s3C�� Cی�C噚CC��fD��DٚD�3D�3D�3D��DٚD$��D)��D.ٚD3ٚD8� D=ٚDB�3DG� DN�DT9�DZ�fD`� Dg�DmL�Dsy�Dy��D�,�D�i�D��D�` D�ٚD�\�D��fD�i�D�� D�c3D�� 111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��RA���A�XA�JA�=qA�t�Aڝ�A�G�AҲ-Aʧ�A�  A�S�A�A�jA�x�A�O�A��hA���A��\A��/A�E�A�oA��wA�\)A���Av��Ak�
A^�AIoA;�A*�yA!+A�/A7LA��AV@�x�@�r�@�&�@��@��m@�V@ԓu@Η�@�@�@���@���@���@���@��@�
=@���@���@���@�`B@��`@�~�@�Ĝ@���@k�@Y�7@KC�@;"�@.�y@#33@��@J@
�111111111111111111111111111111111111111111111111111111111111111111111   A��RA���A�XA�JA�=qA�t�Aڝ�A�G�AҲ-Aʧ�A�  A�S�A�A�jA�x�A�O�A��hA���A��\A��/A�E�A�oA��wA�\)A���Av��Ak�
A^�AIoA;�A*�yA!+A�/A7LA��AV@�x�@�r�@�&�@��@��m@�V@ԓu@Η�@�@�@���@���@���@���@��@�
=@���@���@���@�`B@��`@�~�@�Ĝ@���@k�@Y�7@KC�@;"�@.�y@#33@��@J@
�111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�B	�B	��B	�uB	��B
n�B
ƨB
�mBE�Bn�B�LB��B��B�/B��BÖB�}B�B�JBo�BN�B7LBbB
��B
ÖB
�B
A�B
B	��B	l�B	B�B	49B	&�B	�B	#�B	>wB	;dB	.B	+B	9XB	I�B	`BB	|�B	��B	��B	�jB	ƨB	�B	�/B	�fB	�B	�B	��B	��B
  B
1B
bB
�B
�B
%�B
7LB
A�B
K�B
W
B
aHB
k�B
u�B
� B
�7111111111111111111111111111111111111111111111111111111111111111111111   B	��B	��B	�B	�B	�jB
]/B
�?B
�B49B]/B��B�jBÖB��BB�-B�B��Bz�B^5B=qB%�B
��B
�ZB
�-B
p�B
1'B	�B	�DB	\)B	2-B	#�B	�B	VB	uB	.B	+B	�B	�B	(�B	9XB	O�B	l�B	�B	��B	�B	�FB	ǮB	��B	�B	�B	�;B	�fB	�B	�B	��B
  B
1B
VB
�B
&�B
1'B
;dB
F�B
P�B
[#B
e`B
o�B
x�111111111111111111111111111111111111111111111111111111111111111111111   ;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            WJO(2003) salinity adjustment is adopted with SeHyD1.0; large scale 8/4, small scale 4/2 ; Use interpolate_float_valuesnov2003.m                                                                                                                                200810151443002008101514430020081015144300  KM      ARGQ1.0                                                                 20070823211001  QCP$PSAL            G�O�G�O�G�O�  11110110111111KD  ARSQWJO 2.0 SeHyD                                                           20081015144300  IP  PSAL            G�O�G�O�G�O�                