CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900656 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               cA   JA  20070817045408  20090318072829  A5_28373_099                    2C  D   APEX-SBE 1332                                                   846 @ԍ��N �1   @ԍ�V�&[@/X�t�j@b�t�j~�1   ARGOS   A   A   A   @�33A  Ac33A���A���A�33BffB33B133BD��BX  Bo33B���B���B���B���B���B���B�33Bƙ�B���B�  B���B�33B�33C�3CL�CL�CffC�C33C��C$�3C)L�C.L�C3  C833C=� CB��CG� CQffC[ffCd�fCo33Cy� C�� C���C�� C���C��fC���C���C���C��3C��3C��3C���C��3C�CǦfC̙�C�s3C֦fCۦfC�3C噚C� C�� C��3C���D� D�fD��D��D� D�3D��D$�3D)ٚD.�3D3�fD8��D=� DB� DG��DL�3DQ�fDV� D[��D`� De�3DjٚDo��Dt��Dy�3D��D�c3D���D��3D�)�D�i�D���D��3D�&fD�l�D���D��3D�#3D�Y�DڦfD���D�)�D�Y�D�3D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A  Ac33A���A���A�33BffB33B133BD��BX  Bo33B���B���B���B���B���B���B�33Bƙ�B���B�  B���B�33B�33C�3CL�CL�CffC�C33C��C$�3C)L�C.L�C3  C833C=� CB��CG� CQffC[ffCd�fCo33Cy� C�� C���C�� C���C��fC���C���C���C��3C��3C��3C���C��3C�CǦfC̙�C�s3C֦fCۦfC�3C噚C� C�� C��3C���D� D�fD��D��D� D�3D��D$�3D)ٚD.�3D3�fD8��D=� DB� DG��DL�3DQ�fDV� D[��D`� De�3DjٚDo��Dt��Dy�3D��D�c3D���D��3D�)�D�i�D���D��3D�&fD�l�D���D��3D�#3D�Y�DڦfD���D�)�D�Y�D�3D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�A�x�A�v�A�x�A�v�A�A�7A�DA�\A퟾A�bA�=qA�A�A�^5A��A�1'A܋DA��A۶FA�7LA�C�A׃AՁAӃAѝ�A���A�9XA�
=A���A�{A�\)Aç�A�\)A��9A�p�A�ffA�  A�$�A���A�=qA�l�A�G�A�I�A�  A�VA�{A�$�A���A��A��\A|{At�AhZA`��A^�+AY33AT�HAMp�AF�`A<��A8bA4�A,�`A*^5A'O�A!��A�+A��A�9A�A/@���@�x�@�$�@�v�@���@�|�@��m@�G�@��h@�b@�t�@��@���@�p�@���@���@��!@���@�@��h@��@��`@�=q@�ƨ@�Q�@u@f�y@^E�@Vv�@O\)@I�#@@��@9&�@4�/@1X@.{@)%@#S�@z�@�@��@��@
�H1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A�x�A�v�A�x�A�v�A�A�7A�DA�\A퟾A�bA�=qA�A�A�^5A��A�1'A܋DA��A۶FA�7LA�C�A׃AՁAӃAѝ�A���A�9XA�
=A���A�{A�\)Aç�A�\)A��9A�p�A�ffA�  A�$�A���A�=qA�l�A�G�A�I�A�  A�VA�{A�$�A���A��A��\A|{At�AhZA`��A^�+AY33AT�HAMp�AF�`A<��A8bA4�A,�`A*^5A'O�A!��A�+A��A�9A�A/@���@�x�@�$�@�v�@���@�|�@��m@�G�@��h@�b@�t�@��@���@�p�@���@���@��!@���@�@��h@��@��`@�=q@�ƨ@�Q�@u@f�y@^E�@Vv�@O\)@I�#@@��@9&�@4�/@1X@.{@)%@#S�@z�@�@��@��@
�H1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�TB	�`B	�ZB	�ZB	�`B	�yB	�B	�B	�B	�B
/B
�VB
��B
�;B
��B
�BhB5?B>wBC�Bo�B� B�B�=B��BǮB�BB�mB�B�mB��B�BoB��B��B�-B��BoB#�B�B �B�)B�RB�BR�B>wB2-B�BB
�B
�3B
��B
hsB
$�B	��B	�B	�B	�}B	��B	x�B	ZB	J�B	;dB	(�B	&�B	%�B	"�B	�B	�B	?}B	D�B	<jB	E�B	2-B	2-B	=qB	VB	iyB	u�B	}�B	�7B	��B	�'B	�dB	ɺB	��B	�5B	�HB	�B	�B	��B	��B	��B
+B
DB
�B
�B
(�B
7LB
<jB
A�B
E�B
I�B
O�B
T�B
XB
\)B
]/B
aHB
ffB
k�B
t�B
x�B
z�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�TB	�`B	�ZB	�ZB	�`B	�yB	�B	�B	�B	�B
/B
�VB
��B
�TB
��B
�BoB5?B>wBE�Bp�B�B�B�JB��BɺB�HB�yB�B�sB��B�B�B��B�B�9BB{B&�B"�B#�B�5B�dB�%BS�B?}B33B�BB
�B
�9B
��B
k�B
&�B
  B	�B	�B	��B	��B	{�B	[#B	K�B	=qB	)�B	'�B	&�B	#�B	�B	 �B	@�B	F�B	=qB	F�B	33B	33B	>wB	VB	iyB	u�B	}�B	�7B	��B	�'B	�dB	ɺB	��B	�5B	�HB	�B	�B	��B	��B	��B
+B
DB
�B
�B
(�B
7LB
<jB
A�B
E�B
I�B
O�B
T�B
XB
\)B
]/B
aHB
ffB
k�B
t�B
x�B
z�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��$;ѷ;��|;��;��;��
;��
;��|;��;��;��|;��|;��|;��|;��;��|;��|;��;��;��;ѷ;��;��;��|;��|;��|;��;��;��;��|;��;��;��;��;��;��;��|;��|;��;��|;��;��|;��;��;��;��|;��|;��;��;��;��|;��;��;��;��;��;��;��;��|;��;��;��;��;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES - SP, where SP is SURFACE PRESSURE (minus 5 dbar for Apf-6,7,8) from next cycle.                                                                                                                                                           none                                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                  none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            SP(NextCycle) = 0.0 dbar                                                                                                                                                                                                                                        none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            Pressures adjusted by using reported SURFACE PRESSURE. The quoted error is max [2.4, size of pressure adjustment] in dbar.                                                                                                                                      The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                 none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                                                                                               20070830081748                            20080811000000  JA  ARFMdecpA5_a                                                                20070817045405  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070817045408  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070817045409  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070817045409  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070817045413  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070817045413  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070817045413  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070817045413  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070817045413  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070817045413  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070817050556                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070821034414  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070821034418  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070821034418  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070821034419  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070821034423  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070821034423  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070821034423  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070821034423  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070821034423  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070821034423  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070821042458                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070830080732  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070830080732  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070830081748  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080811000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081001073953  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081001101631                      G�O�G�O�G�O�                JM  AREVjmrvp1.2                                                                20090312120939  IP  PRES            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090318072301  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090318072829                      G�O�G�O�G�O�                