CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900656 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               bA   JA  20070807025231  20090318072759  A5_28373_098                    2C  D   APEX-SBE 1332                                                   846 @ԋ5�j�p1   @ԋ7��e=@/���E�@b�;dZ�1   ARGOS   A   A   A   @�  A  A`  A�  A�33A陚B	��BffB2  BF��BX��BlffB�ffB�  B�  B�  B�33B���B���B���B�ffB�  B�ffBB���C33C33CL�C��C��C�3C��C$� C)� C.ffC3  C8ffC=�3CBL�CG33CQ� CZ�fCeL�Co  CyffC���C�� C��fC�� C��3C�� C�s3C���C��fC��3C�� C��3C���C¦fCǙ�C̳3C�� C�ٚCۦfC�3C��C�3C�� C���C���D�fD�fD��D� D��D� D��D$��D)��D.� D3�fD8�fD=� DBٚDG��DL��DQ�fDV�3D[�fD`�fDe��DjٚDo� DtٚDyٚD�)�D�l�D���D��D�#3D�ffD�� D�� D��D�c3D��3D��fD��D�l�Dڣ3D��3D�,�D�Y�D�3D�#31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A  A`  A�  A�33A陚B	��BffB2  BF��BX��BlffB�ffB�  B�  B�  B�33B���B���B���B�ffB�  B�ffBB���C33C33CL�C��C��C�3C��C$� C)� C.ffC3  C8ffC=�3CBL�CG33CQ� CZ�fCeL�Co  CyffC���C�� C��fC�� C��3C�� C�s3C���C��fC��3C�� C��3C���C¦fCǙ�C̳3C�� C�ٚCۦfC�3C��C�3C�� C���C���D�fD�fD��D� D��D� D��D$��D)��D.� D3�fD8�fD=� DBٚDG��DL��DQ�fDV�3D[�fD`�fDe��DjٚDo� DtٚDyٚD�)�D�l�D���D��D�#3D�ffD�� D�� D��D�c3D��3D��fD��D�l�Dڣ3D��3D�,�D�Y�D�3D�#31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�E�A�G�A�O�A�jA�n�A�x�A�r�A�jA�O�A�A�DA���AߋDA�bNA�+A�7LA�Q�A�5?Aٟ�A�"�A�7LA�(�A�K�AϾwA�r�A��yAʏ\A�C�A�VA�C�A��A���A��#A�v�A�XA�A�A���A��A��!A��A���A�JA�G�A�9XA�A�-A�$�A���A�p�A�r�A{+AqhsAm7LAdZAa��AZ�ATffAOS�AFĜAB  A=��A57LA/S�A($�A#ƨA!��AS�A�#AA�A	t�A�w@���@�7@�M�@�X@�o@��@�x�@�@�dZ@���@���@��@���@��7@��P@�dZ@�O�@���@��;@���@���@�-@�/@��@�-@�%@sƨ@g�;@]�h@SS�@M�h@G�@?�;@8�@2M�@+��@&��@!hs@��@/@&�@�y@o@O�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�E�A�G�A�O�A�jA�n�A�x�A�r�A�jA�O�A�A�DA���AߋDA�bNA�+A�7LA�Q�A�5?Aٟ�A�"�A�7LA�(�A�K�AϾwA�r�A��yAʏ\A�C�A�VA�C�A��A���A��#A�v�A�XA�A�A���A��A��!A��A���A�JA�G�A�9XA�A�-A�$�A���A�p�A�r�A{+AqhsAm7LAdZAa��AZ�ATffAOS�AFĜAB  A=��A57LA/S�A($�A#ƨA!��AS�A�#AA�A	t�A�w@���@�7@�M�@�X@�o@��@�x�@�@�dZ@���@���@��@���@��7@��P@�dZ@�O�@���@��;@���@���@�-@�/@��@�-@�%@sƨ@g�;@]�h@SS�@M�h@G�@?�;@8�@2M�@+��@&��@!hs@��@/@&�@�y@o@O�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�
B
�B
�B
�#B
�)B
�fB
��B
��B
��B$�BB
�B
�B1B!�B/B=qBO�BXBl�B�B��B�-B�#B�B�`B�
BBhB=qB8RB5?B49BE�BG�BH�BQ�BD�B.BoB��B�JBm�BdZBQ�BB�B+B\B
�B
ƨB
�uB
VB
9XB
bB
  B	�#B	�jB	��B	�B	l�B	YB	:^B	5?B	,B	'�B	�B	�B	�B	�B	�B	VB	�B	 �B	9XB	;dB	J�B	VB	o�B	r�B	�+B	�oB	��B	�XB	��B	��B	�#B	�sB	�B	�B	��B	��B
B
DB
VB
oB
�B
"�B
,B
6FB
<jB
B�B
F�B
K�B
P�B
VB
ZB
`BB
cTB
iyB
o�B
t�B
x�B
z�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�
B
�B
�B
�#B
�)B
�fB
��B
��B  B)�B	7B
�B
�B	7B"�B0!B>wBP�BYBn�B�%B��B�9B�/B�B�mB�#BBuB@�B9XB8RB7LBE�BG�BJ�BS�BH�B1'B�B��B�VBn�Be`BR�BC�B-BbB
�B
ȴB
��B
W
B
;dB
hB
B	�/B	�qB	��B	�B	m�B	[#B	<jB	7LB	-B	(�B	 �B	�B	�B	�B	�B	\B	�B	!�B	9XB	;dB	K�B	W
B	p�B	s�B	�+B	�oB	��B	�XB	��B	��B	�#B	�sB	�B	�B	��B	��B
B
DB
VB
oB
�B
"�B
,B
6FB
<jB
B�B
F�B
K�B
P�B
VB
ZB
`BB
cTB
iyB
o�B
t�B
x�B
z�B
~�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;��
;��
;��
;��
;��
;��
;��
;��
;��;�;ѷ;��;��;��;��;��;��;��;��;��|;��|;��|;��|;��|;��;��|;ѷ;��
;��|;��;��;��;��;��
;��
;��|;��|;ѷ;��;ѷ;ѷ;��|;��;��;��;��;��|;��;��|;��|;��|;��;��|;��;��|;��|;��;��|;��;��;��|;��|;��|;��;��;��|;��|;��|;��;��;��;��
;��;��
;��
;��;��;��;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES - SP, where SP is SURFACE PRESSURE (minus 5 dbar for Apf-6,7,8) from next cycle.                                                                                                                                                           none                                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                  none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            SP(NextCycle) = 0.0 dbar                                                                                                                                                                                                                                        none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            Pressures adjusted by using reported SURFACE PRESSURE. The quoted error is max [2.4, size of pressure adjustment] in dbar.                                                                                                                                      The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                 none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                                                                                               20070820084113                            20080811000000  JA  ARFMdecpA5_a                                                                20070807025228  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070807025231  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070807025231  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070807025232  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070807025236  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070807025236  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070807025236  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070807025236  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070807025236  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070807025236  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070807030441                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070811034452  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070811034456  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070811034456  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070811034457  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070811034501  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070811034501  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070811034501  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070811034501  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070811034501  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070811034501  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070811042147                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070820082735  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070820082735  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070820084113  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080811000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081001073954  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081001101810                      G�O�G�O�G�O�                JM  AREVjmrvp1.2                                                                20090312120939  IP  PRES            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090318072225  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090318072759                      G�O�G�O�G�O�                