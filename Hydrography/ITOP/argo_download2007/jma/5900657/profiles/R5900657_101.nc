CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   C   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  4�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5<   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6H   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  7T   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       8�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  ;    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;D   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  D  <P   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       <�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  =�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    =�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    @�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    C�   CALIBRATION_DATE            	             
_FillValue                  ,  F�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    F�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G    HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    GL   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G\   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G`   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Gp   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Gt   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Gx   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    G|Argo profile    2.2 1.2 19500101000000  5900657 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               eA   JA  20070907125119  20090520062318                                  2B  A   937                                                             846 @ԓ	�F)�1   @ԓ	�y\�@*��vȴ9@`�|�hs1   ARGOS   A   A   A   @���A��A�ffA�33BffBE33Bn  B���B�33B���Bƙ�B�33B�  C�3C� C��C� C)33C3� C=��CG� C[��Co�C��3C��fC��fC�ٚC��3C��fC�� Cǀ C�� Cۙ�C�� CC�� D��D��D��D�3D�3D�3D��D$ٚD)�3D.�3D3�fD8��D=�fDBٚDG� DNfDTY�DZ��D`��Dg3DmY�Ds��Dy� D�&fD�l�D��3D��fD�)�D�i�D���D�@ 1111111111111111111111111111111111111111111111111111111111111111111 @���A  A���A�ffB  BD��Bm��B���B�  B���B�fgB�  B���C��CffC� CffC)�C3ffC=� CGffC[� Co  C��fC���C���C���C��fC���C��3C�s3Cѳ3Cی�C�3C��C�s3D�4D�gD�4D��D��D��D�gD$�4D)��D.��D3� D8�gD=� DB�4DGٚDN  DTS4DZ�gD`�gDg�DmS4Ds�4DyٚD�#3D�i�D�� D��3D�&gD�fgD�ٚD�<�1111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ȴA�PA��+A��mA�1A��A��A�wA���A�|�A�A���A�M�A�S�A���A�dZAÛ�A��PA���A��PA���A���A���Ap1'AW��A?�wA/XA'G�A!?}AC�A5?A
jAffA   @�I�@�x�@��@�-@�+@�\)@և+@�$�@Ϯ@��@�bN@��#@��j@���@�1'@��j@��T@���@���@��H@��@��@��@�@�"�@�%@��7@�|�@z=q@p  @hb@W
=@J�H1111111111111111111111111111111111111111111111111111111111111111111 A�ȴA�PA��+A��mA�1A��A��A�wA���A�|�A�A���A�M�A�S�A���A�dZAÛ�A��PA���A��PA���A���A���Ap1'AW��A?�wA/XA'G�A!?}AC�A5?A
jAffA   @�I�@�x�@��@�-@�+@�\)@և+@�$�@Ϯ@��@�bN@��#@��j@���@�1'@��j@��T@���@���@��H@��@��@��@�@�"�@�%@��7@�|�@z=q@p  @hb@W
=@J�H1111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�}B��B�qB�FBB�fB�`B�B	%�B	�}B
I�B
�sBx�B�!B�B�B�B��B��B��B��B
�B
��B
;dB	�B	�7B	gmB	t�B	��B	��B	��B	��B	��B	ƨB	ŢB	��B	��B	��B	�5B	�B	�B
%B	��B
B
%B
B
B
1B
1B

=B
PB
hB
{B
�B
�B
�B
�B
"�B
'�B
-B
1'B
8RB
;dB
@�B
G�B
O�B
X1111111111111111111111111111111111111111111111111111111111111111111 B�}B��B�qB�FBB�fB�`B�B	%�B	�}B
I�B
�sBx�B�!B�B�B�B��B��B��B��B
�B
��B
;dB	�B	�7B	gmB	t�B	��B	��B	��B	��B	��B	ƨB	ŢB	��B	��B	��B	�5B	�B	�B
%B	��B
B
%B
B
B
1B
1B

=B
PB
hB
{B
�B
�B
�B
�B
"�B
'�B
-B
1'B
8RB
;dB
@�B
G�B
O�B
X1111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA4_a                                                                20070907125117  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070907125119  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070907125120  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070907125120  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070907125124  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070907125124  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070907125124  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070907125124  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070907125125  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070907125125  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070907130216                      G�O�G�O�G�O�                JA  ARFMdecpA4_a                                                                20070911160319  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070911160324  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070911160324  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070911160325  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070911160329  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070911160329  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070911160329  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070911160329  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070911160329  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070911160329  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070911160329  CV  TIME            G�O�G�O�F��M                JA  ARGQrelo2.1                                                                 20070911160329  CV  LAT$            G�O�G�O�AU                JA  ARGQrelo2.1                                                                 20070911160329  CV  LON$            G�O�G�O�C�                JA  ARUP                                                                        20070911161708                      G�O�G�O�G�O�                JA  ARFMdecpA4_a                                                                20070911160319  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090520061919  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090520061920  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090520061920  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090520061920  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090520061921  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090520061921  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090520061921  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090520061921  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090520061922  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090520062318                      G�O�G�O�G�O�                