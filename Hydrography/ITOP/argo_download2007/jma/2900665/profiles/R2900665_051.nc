CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   H   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >l   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D�   CALIBRATION_DATE            	             
_FillValue                  ,  G�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    H   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    H(   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    H,   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         H<   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         H@   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        HD   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    HHArgo profile    2.2 1.2 19500101000000  2900665 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               3A   JA  20071020013526  20090518061818                                  2B  A   APEX-SBE 2983                                                   846 @ԝ�/�c1   @ԝ�L;*@9�$�/�@`~-V1   ARGOS   A   A   A   @���A  A�33A�ffBffBDffBm��B�ffB�33B�33B�ffB���B�33CL�CffC�C33C)33C3�3C=��CGL�C[L�CoL�C�� C�� C���C���C�� C��3C�� Cǳ3Cљ�Cۙ�C�� C�fC�� D�3D��D��DٚD� D�3D� D$�fD)ٚD.��D3ٚD8� D=��DB��DGٚDM��DTL�DZ��D`�3Dg3DmFfDs��Dy� D�,�D�ffD���D�ٚD�fD�c3D�� D�i�D�� D�p D���D�c3D��f111111111111111111111111111111111111111111111111111111111111111111111111@�fgA��A���A���B��BA��Bj��B�  B���B���B�  B�fgB���C ��C
�3CfgC� C(� C3  C<�gCF��CZ��Cn��C�&fC�&fC�33C�33C�ffC�Y�C�ffC�Y�C�@ C�@ C�ffC�L�C�ffD�fD� D��D��D�3D�fD�3D$��D)��D.��D3��D8�3D=� DB� DG��DM��DT  DZ` D`�fDf�fDm�Dsl�Dy�3D�gD�P D��4D��4D�  D�L�D�ɚD�S4D�ɚD�Y�D��gD�L�D�� 111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�ȴA�A��A��TA��TA��;A��HAɥ�A�/A��yA�O�A�&�A���A�A�9XA���A��A�+A��A���A��A�9XA�5?A�{A��A�oA��A��9At�Av��An�/Ah��Aa�AW�AN�AC��A6~�A+��A"��A 9XA(�A�RAI�A33@�V@��@�h@�^5@��
@��@�I�@�9X@�?}@��j@���@�
=@�X@��@�(�@~V@o;d@`A�@U@M��@E?}@6��@/
=@�@��@�@	�@��111111111111111111111111111111111111111111111111111111111111111111111111A�ȴA�A��A��TA��TA��;A��HAɥ�A�/A��yA�O�A�&�A���A�A�9XA���A��A�+A��A���A��A�9XA�5?A�{A��A�oA��A��9At�Av��An�/Ah��Aa�AW�AN�AC��A6~�A+��A"��A 9XA(�A�RAI�A33@�V@��@�h@�^5@��
@��@�I�@�9X@�?}@��j@���@�
=@�X@��@�(�@~V@o;d@`A�@U@M��@E?}@6��@/
=@�@��@�@	�@��111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
��B
��B
�}B
��B
��B
��B
ǮB
��B�B0!BK�BW
BZBr�Bx�By�B~�B�Br�Bn�BcTBO�B=qB/B$�BJB
�B
��B
�B
�B
W
B
5?B
bB	�NB	�?B	�%B	O�B	,B	+B	B�;B��B�FB�!B�!B�B��B��B��B��B�B�wB��B�5B�B	B	�B	.B	7LB	gmB	�B	��B	�LB	ƨB	�
B	��B
%B
(�B
<jB
J�B
YB
dZ111111111111111111111111111111111111111111111111111111111111111111111111B
��B
��B
�}B
��B
��B
��B
ǮB
��B�B0!BK�BW
BZBr�Bx�By�B~�B�Br�Bn�BcTBO�B=qB/B$�BJB
�B
��B
�B
�B
W
B
5?B
bB	�NB	�?B	�%B	O�B	,B	+B	B�;B��B�FB�!B�!B�B��B��B��B��B�B�wB��B�5B�B	B	�B	.B	7LB	gmB	�B	��B	�LB	ƨB	�
B	��B
%B
(�B
<jB
J�B
YB
dZ111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20071020013523  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071020013526  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071020013527  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071020013531  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071020013531  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071020013532  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071020014329                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071024050750  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071024050756  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071024050757  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071024050802  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071024050802  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071024050802  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071024052914                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071024050750  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518061057  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518061057  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518061058  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518061059  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518061059  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518061059  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518061059  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518061059  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518061818                      G�O�G�O�G�O�                