CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   H   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >l   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D�   CALIBRATION_DATE            	             
_FillValue                  ,  G�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    H   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    H(   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    H,   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         H<   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         H@   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        HD   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    HHArgo profile    2.2 1.2 19500101000000  2900667 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               
A   JA  20070930062044  20090518063131                                  2B  A   APEX-SBE 2985                                                   846 @Ԙ�Vx��1   @Ԙ���}(@;�O�;dZ@`Q&�x��1   ARGOS   A   A   A   @�ffA  A���A�  B��BF  Bm33B�33B�  B�ffB�  B�  B�ffC33CL�CffCffC)L�C3L�C=ffCG33C[  Co� C�� C�� C��fC�� C�s3C��3C��3Cǳ3Cљ�C�s3C�s3C��C��3DٚD��DٚD� D��D�3D�fD$�fD)� D.ٚD3��D8��D=��DB��DG��DN3DTY�DZ�fD`��Df�3DmFfDs��Dy� D�,�D�l�D��3D��3D�,�D�ffD�� D�l�D���D�ffD��3D�` D�|�111111111111111111111111111111111111111111111111111111111111111111111111@���A��A�fgA���B33BDffBk��B�ffB�33B���B�33B�33B홙C ��C
�gC  C  C(�gC2�gC=  CF��CZ��Co�C���C���C�s3C�L�C�@ C�� C�� Cǀ C�fgC�@ C�@ C�Y�C�� D� D�3D� D�fD� D��D��D$��D)�fD.� D3� D8�3D=�3DB�3DG�3DM��DT@ DZl�D`� DfٙDm,�Dss3Dy�fD�  D�` D��fD��fD�  D�Y�D��3D�` D�� D�Y�D��fD�S3D�p 111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�dZA�l�A�-A���A���A�(�A�r�A��;A��A�A�bA�l�A��^A��yA��`A��A�{A���A���A�A�E�A��A��A��7A��A���A�A�A�M�A���A�K�AzĜAs�^AkoAa��AYG�AP��AH��A;
=A1��A,v�A%t�AJA9XA	x�A��A�w@��u@��@�A�@պ^@�ȴ@�`B@���@���@���@�ff@���@�1'@��^@|�@pĜ@eO�@W�;@KS�@B=q@3��@)��@��@O�@V@
=@=q111111111111111111111111111111111111111111111111111111111111111111111111A�dZA�l�A�-A���A���A�(�A�r�A��;A��A�A�bA�l�A��^A��yA��`A��A�{A���A���A�A�E�A��A��A��7A��A���A�A�A�M�A���A�K�AzĜAs�^AkoAa��AYG�AP��AH��A;
=A1��A,v�A%t�AJA9XA	x�A��A�w@��u@��@�A�@պ^@�ȴ@�`B@���@���@���@�ff@���@�1'@��^@|�@pĜ@eO�@W�;@KS�@B=q@3��@)��@��@O�@V@
=@=q111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	ĜB	ĜB	�NB
#�B
H�B
C�B
jB
�7B
�B
��B.B8RB>wBF�BG�BK�BN�BK�BL�BL�BH�BJ�B?}B0!B!�BVBB
�5B
�jB
�B
�B
bNB
8RB
1B	�HB	�XB	��B	aHB	;dB	%�B	DB�BƨB�^B�B�jB�RB�9B�^B�qBÖB��B�BB�B��B	\B	#�B	1'B	A�B	`BB	}�B	�{B	�B	ĜB	��B	�B
B
�B
0!B
>wB
N�B
Z111111111111111111111111111111111111111111111111111111111111111111111111B	ĜB	ĜB	�NB
#�B
H�B
C�B
jB
�7B
�B
��B.B8RB>wBF�BG�BK�BN�BK�BL�BL�BH�BJ�B?}B0!B!�BVBB
�5B
�jB
�B
�B
bNB
8RB
1B	�HB	�XB	��B	aHB	;dB	%�B	DB�BƨB�^B�B�jB�RB�9B�^B�qBÖB��B�BB�B��B	\B	#�B	1'B	A�B	`BB	}�B	�{B	�B	ĜB	��B	�B
B
�B
0!B
>wB
N�B
Z111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070930062029  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070930062044  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070930062047  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070930062054  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070930062054  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.7c                                                                20070930062054  QCF$                G�O�G�O�G�O�            8000JA  ARGQrqcpt16b                                                                20070930062055  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070930064546                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071004051405  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071004051414  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071004051415  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071004051420  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071004051420  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.7c                                                                20071004051420  QCF$                G�O�G�O�G�O�            8000JA  ARGQrqcpt16b                                                                20071004051420  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071004052626                      G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071122052751  QCP$                G�O�G�O�G�O�              1CJA  ARGQrqcpt19b                                                                20071122052755  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071122052755  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071122052755  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071122071015                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071004051405  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518062530  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518062531  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518062531  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518062532  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518062532  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518062532  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518062532  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518062532  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518063131                      G�O�G�O�G�O�                