CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900683 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070915050147  20090518064404                                  2B  A   APEX-SBE 2821                                                   846 @Ԕ�W��1   @Ԕ�F��@:�ě��T@b�1&�1   ARGOS   A   A   A   @�  A��Al��A���A���A�  B
ffB��B1��BE33BZ��Bm��B�  B�33B�ffB���B�33B�  B�33B���B�  B�33B䙚B�33B���CffC�C� CffC�fC�CffC$33C)�C-�fC3ffC8�C=� CB� CGffCQ��C[� Ce� CoL�CyffC���C���C���C�� C���C��3C��3C���C��fC�Y�C�Y�C���C���C�s3CǦfC�� Cѳ3C֦fCی�C�ffC�Y�C� CC��3C���D� D��DٚDٚDٚD��D� D$�3D)� D.�fD3��D8��D=� DB�3DG�fDL��DQ��DV��D[�3D`��De�fDjٚDo� Dt��Dy�fD�)�D�\�D��fD���D�)�D�l�D���D��3D�  D�p D�� D�� D�)�D�ffDڰ D�� D��D�c3D� D�ɚ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33Ak33A�  A���A�33B
  BfgB134BD��BZfgBm34B���B�  B�33B���B�  B���B�  Bƙ�B���B�  B�fgB�  B���CL�C  CffCL�C��C  CL�C$�C)  C-��C3L�C8  C=ffCBffCGL�CQ� C[ffCeffCo33CyL�C�� C���C���C��3C�� C��fC��fC���C���C�L�C�L�C���C�� C�ffCǙ�C̳3CѦfC֙�Cۀ C�Y�C�L�C�s3C��C��fC�� DٚD�4D�4D�4D�4D�4D��D$��D)ٚD.� D3�gD8�gD=ٚDB��DG� DL�gDQ�gDV�gD[��D`�gDe� Dj�4Do��Dt�gDy� D�&gD�Y�D��3D��D�&gD�i�D��gD�� D��D�l�D���D���D�&gD�c3Dڬ�D���D�gD�` D��D��g1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A���A�p�AٶFA�n�AʮA�$�A�G�A�  A�ffA�A�A��mA��jA�bNA���A���A���A�bNA�A��A���A��+A���A��PA�%A�C�A��A��A��hA�oA�I�A��A��A���A���A���A�z�A��A�ĜA�VA�S�A��/A��\A��^A���A�`BA��A��;A�\)A��A��A��`A�$�A~  A{?}AwK�As�Ao�mAn�jAj�Af1'A`~�AX��AS�AP1'AKC�AG�
A>��A8 �A1��A.��A#�mA1'AA��@�V@�Q�@�{@��h@��@�5?@���@��D@���@��@�?}@�G�@���@�5?@��@�@xbN@s��@o
=@j��@d��@[S�@Q�@L1@B��@7�w@1�@+�F@&@ff@X@o@�@p�@;d@Z@-@ �u?��?�K�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A�p�AٶFA�n�AʮA�$�A�G�A�  A�ffA�A�A��mA��jA�bNA���A���A���A�bNA�A��A���A��+A���A��PA�%A�C�A��A��A��hA�oA�I�A��A��A���A���A���A�z�A��A�ĜA�VA�S�A��/A��\A��^A���A�`BA��A��;A�\)A��A��A��`A�$�A~  A{?}AwK�As�Ao�mAn�jAj�Af1'A`~�AX��AS�AP1'AKC�AG�
A>��A8 �A1��A.��A#�mA1'AA��@�V@�Q�@�{@��h@��@�5?@���@��D@���@��@�?}@�G�@���@�5?@��@�@xbN@s��@o
=@j��@d��@[S�@Q�@L1@B��@7�w@1�@+�F@&@ff@X@o@�@p�@;d@Z@-@ �u?��?�K�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�bB
�bB
aHB
K�B
aHB
��B
��B
�TB
�`B
��B
�BB�B�B2-B=qBC�BM�BT�BR�BT�BS�BXB^5B`BBaHB`BB]/BVBR�BS�BQ�BL�BK�BE�BC�BA�B8RB1'B-B(�B �B�BPBB
��B
�B
�TB
�
B
ǮB
�FB
��B
��B
�DB
y�B
hsB
XB
P�B
?}B
(�B
bB	�B	�
B	��B	�FB	��B	� B	bNB	H�B	7LB	B�jB��Bl�BI�B:^B"�B�B#�B7LBE�BbNBr�B�B��B�'B��B�)B��B	B	�B	#�B	2-B	?}B	T�B	o�B	�%B	��B	�3B	��B	�NB	�B	��B
PB
�B
'�B
1'B
7LB
E�B
L�B
R�B
W
B
[#B
bNB
gm1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�bB
�bB
aHB
K�B
aHB
��B
��B
�TB
�`B
��B
�BB�B�B2-B=qBC�BM�BT�BR�BT�BS�BXB^5B`BBaHB`BB]/BVBR�BS�BQ�BL�BK�BE�BC�BA�B8RB1'B-B(�B �B�BPBB
��B
�B
�TB
�
B
ǮB
�FB
��B
��B
�DB
y�B
hsB
XB
P�B
?}B
(�B
bB	�B	�
B	��B	�FB	��B	� B	bNB	H�B	7LB	B�jB��Bl�BI�B:^B"�B�B#�B7LBE�BbNBr�B�B��B�'B��B�)B��B	B	�B	#�B	2-B	?}B	T�B	o�B	�%B	��B	�3B	��B	�NB	�B	��B
PB
�B
'�B
1'B
7LB
E�B
L�B
R�B
W
B
[#B
bNB
gm1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070915050144  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070915050147  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070915050148  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070915050152  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070915050152  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070915050152  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070915050858                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070919042417  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070919042421  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070919042422  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070919042425  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070919042426  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070919042426  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070919044237                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070919042417  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518063908  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518063908  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518063909  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518063910  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518063910  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518063910  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518063910  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518063910  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518064404                      G�O�G�O�G�O�                