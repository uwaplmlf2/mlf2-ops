CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   n   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4T   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6|   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :\   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J�   CALIBRATION_DATE            	             
_FillValue                  ,  M�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N    HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N`   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Np   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Nt   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  2900683 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070817085752  20091209003002                                  2B  A   APEX-SBE 2821                                                   846 @ԍp�\1   @ԍr��P�@;V�@b�z�H1   ARGOS   A   A   A   @���AffAi��A�33A���A陚BffB��B1��BF��BY33Bn��B�ffB�  B�33B���B���B�ffB���B�  B�  B�33B���B���B���C ��C  C33C��C33C�C�fC$ffC)��C.33C3��C8�C=33CB�CG� CQ33C[� CeffCo� CyffC��fC��3C��fC��fC���C���C���C�ٚC�� C���C���C�� C�s3C�ffC�s3C�Y�C�L�C֌�Cۀ C�s3C�3C�3C�3C��fC�s3D��D�3D�3D��D� DٚD�3D$�fD)� D.��D3�3DQ�3DV�3D[��D`� De� Dj�3Do��Dt� Dy��D�#3D�ffD���D���D�)�D�c3D���D��fD��D�i�D�� D��D�)�D�l�Dڜ�D��3D��D�\�D�3D�Ff11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�34A33AffgA���A�33A�  B��B��B0��BF  BXffBn  B�  B���B���B�fgB�fgB�  B�34Bř�Bϙ�B���B�fgB�fgB�fgC ��C��C  C��C  C�gC�3C$33C)fgC.  C3fgC7�gC=  CA�gCGL�CQ  C[L�Ce33CoL�Cy33C���C���C���C���C�� C�s3C�s3C�� C��fC��3C��3C��fC�Y�C�L�C�Y�C�@ C�33C�s3C�ffC�Y�C噙CꙙCC��C�Y�D��D�fD�fD��D�3D��D�fD$��D)�3D.� D3�fDQ�fDV�fD[��D`�3De�3Dj�fDo� Dt�3Dy��D��D�` D��4D��gD�#4D�\�D��gD�� D�gD�c4D���D��4D�#4D�fgDږgD���D�gD�VgD��D�@ 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�ĜA���A՝�AάA� �A�/A��DA���A��wA�ȴA�G�A�A�A���A���A�&�A�(�A��yA�~�A�t�A�7LA��^A�7LA���A��A��-A�G�A�JA��A��7A�l�A��yA��A��`A���A�&�A��A�Q�A��TA��hA��!A�1A�n�A��DA�+A�"�A��A�VA��A�JA~  Az�uAu
=Ap�Ah~�Ac��A`VA]hsAX�AUC�AR�!AM��AKG�AC�#A?��A:  A4I�A.��A+A#��A�A{Al�@�bN@�\@�z�@�
=@���@�O�@�  @�$�@��@�v�@�ff@{dZ@w
=@r^5@m��@h1'@b��@T�j@K@D�@?�w@:��@4�@*M�@%/@�@��@�D@�@C�@��@ff@��?��-?�?�%?��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�A�ĜA���A՝�AάA� �A�/A��DA���A��wA�ȴA�G�A�A�A���A���A�&�A�(�A��yA�~�A�t�A�7LA��^A�7LA���A��A��-A�G�A�JA��A��7A�l�A��yA��A��`A���A�&�A��A�Q�A��TA��hA��!A�1A�n�A��DA�+A�"�A��A�VA��A�JA~  Az�uAu
=Ap�Ah~�Ac��A`VA]hsAX�AUC�AR�!AM��AKG�AC�#A?��A:  A4I�A.��A+A#��A�A{Al�@�bN@�\@�z�@�
=@���@�O�@�  @�$�@��@�v�@�ff@{dZ@w
=@r^5@m��@h1'@b��@T�j@K@D�@?�w@:��@4�@*M�@%/@�@��@�D@�@C�@��@ff@��?��-?�?�%?��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�{B
�{B
�hB
�qB
��B
�TB	7BH�BT�B�B|�B��B�By�B[#BXBN�BI�BB�B>wB6FB33B/B!�B �B!�B�B�B�B�B#�B#�B&�B%�B#�B �B�B�B�B{B	7BBB
��B
�B
�HB
��B
ÖB
�LB
��B
��B
�=B
u�B
_;B
8RB
"�B
�B
+B	�B	�TB	�B	��B	�3B	��B	�B	m�B	T�B	A�B	6FB	�B	%B��Bp�BG�B:^B&�B!�B"�B�B'�B;dBɺB�B�B	JB	�B	'�B	;dB	B�B	S�B	x�B	��B	��B	�XB	ȴB	�B	�B
  B
JB
�B
%�B
33B
<jB
A�B
H�B
S�B
\)B
gmB
jB
m�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B
�{B
�{B
�hB
�qB
��B
�TB	7BH�BT�B�B|�B��B�By�B[#BXBN�BI�BB�B>wB6FB33B/B!�B �B!�B�B�B�B�B#�B#�B&�B%�B#�B �B�B�B�B{B	7BBB
��B
�B
�HB
��B
ÖB
�LB
��B
��B
�=B
u�B
_;B
8RB
"�B
�B
+B	�B	�TB	�B	��B	�3B	��B	�B	m�B	T�B	A�B	6FB	�B	%B��Bp�BG�B:^B&�B!�B"�B�B'�B;dBɺB�B�B	JB	�B	'�B	;dB	B�B	S�B	x�B	��B	��B	�XB	ȴB	�B	�B
  B
JB
�B
%�B
33B
<jB
A�B
H�B
S�B
\)B
gmB
jB
m�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070817085750  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070817085752  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070817085753  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070817085757  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070817085757  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070817085758  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070817090349                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070820041610  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070820041614  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070820041615  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070820041618  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070820041619  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070820041619  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070820043044                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070820041610  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518063900  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518063901  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518063901  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518063902  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518063902  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518063902  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518063902  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518063902  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518064406                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091208075007  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091208075126  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091208075126  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091208075127  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091208075128  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091208075128  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091208075128  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091208075128  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091208075128  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091209003002                      G�O�G�O�G�O�                