CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900683 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070806025841  20090518064405                                  2B  A   APEX-SBE 2821                                                   846 @Ԋ��X�1   @Ԋ�P6�@;��\(��@b5?|�1   ARGOS   A   A   A   @�33AffAfffA���Ař�A�  B
ffB  B133BE33BY33Bm33B���B�  B�33B���B�ffB�33B�ffB���B���Bڙ�B���B�33B�  C  CffCffC  C��C�fC33C#��C)L�C.�C3  C7�fC<�3CB33CF�fCQ  C[33Ce�CoffCyL�C�� C��3C��3C���C��fC�� C��3C�s3C���C�ffC��3C��3C���C¦fC�� C̀ Cѳ3C֦fCۙ�C���C�ffC�ffCC��3C�� D�fD� D� D�fD��DٚD� D$��D)ٚD.�3D3�fD8��D=��DB�fDG��DL�3DQٚDV�fD[�fD`�3De�fDj� Do��Dt� Dy��D�,�D�VfD�� D��3D�#3D�i�D�� D��D�#3D�p D���D��fD�)�D�` DڦfD��3D�  D�VfD�3D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A��Ad��A���A���A�33B
  B��B0��BD��BX��Bl��B�fgB���B�  B�fgB�33B�  B�33Bƙ�BЙ�B�fgB䙚B�  B���C �fCL�CL�C�fC�3C��C�C#�3C)33C.  C2�fC7��C<��CB�CF��CP�fC[�Ce  CoL�Cy33C��3C��fC��fC�� C���C�s3C��fC�ffC�� C�Y�C��fC��fC�� C�Cǳ3C�s3CѦfC֙�Cی�C�� C�Y�C�Y�C��C��fC��3D� D��D��D� D�4D�4D��D$�gD)�4D.��D3� D8�4D=�4DB� DG�gDL��DQ�4DV� D[� D`��De� DjٚDo�gDt��Dy�4D�)�D�S3D���D�� D�  D�fgD���D��gD�  D�l�D��gD��3D�&gD�\�Dڣ3D�� D��D�S3D� D� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�hsA�n�Aމ7A�dZAڋDA��
A�VA�S�A���A��A���A���A���A�A�n�A���A�VA�I�A��!A�A�+A�l�A�JA�1'A�bNA���A�5?A��\A��A�XA���A���A�r�A�S�A�9XA�{A��A��^A�M�A��#A��A�oA��A���A�$�A��\A��mA�~�A�VA�^5A{Aw��At��ArĜAn��Ak7LAj~�Aa�TA]AZz�AV��AS�-AO�AK�AH^5AC�hA>��A;;dA1`BA*�HAdZAE�AS�@��F@�o@��m@ɑh@�;d@�^5@��@�S�@��`@��
@�V@���@���@��w@�I�@�z�@�I�@z�\@r��@m?}@j�\@f��@Y��@Ol�@H �@?;d@9��@0��@,z�@%@;d@��@ �@"�@��@
��@A�@�@��?�|�?��#?�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�hsA�n�Aމ7A�dZAڋDA��
A�VA�S�A���A��A���A���A���A�A�n�A���A�VA�I�A��!A�A�+A�l�A�JA�1'A�bNA���A�5?A��\A��A�XA���A���A�r�A�S�A�9XA�{A��A��^A�M�A��#A��A�oA��A���A�$�A��\A��mA�~�A�VA�^5A{Aw��At��ArĜAn��Ak7LAj~�Aa�TA]AZz�AV��AS�-AO�AK�AH^5AC�hA>��A;;dA1`BA*�HAdZAE�AS�@��F@�o@��m@ɑh@�;d@�^5@��@�S�@��`@��
@�V@���@���@��w@�I�@�z�@�I�@z�\@r��@m?}@j�\@f��@Y��@Ol�@H �@?;d@9��@0��@,z�@%@;d@��@ �@"�@��@
��@A�@�@��?�|�?��#?�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
m�B
v�B
�'B
�?B
�B
�)B8RB:^B��B�B��B�^B��B�qB��B�qB�B�uBk�BT�BA�B?}B;dB9XB5?B2-B/B-B'�B#�B �B�B�B�B�B�B�B�B�B�B�B�BbBB
��B
�fB
�B
ȴB
�XB
��B
�VB
�B
v�B
p�B
aHB
L�B
E�B
�B

=B	��B	�yB	�#B	ȴB	�XB	��B	�uB	�B	s�B	O�B	;dB	oB�B�9BM�B1'B�B1'B'�B"�B1'B:^B\)Br�B�B�VB�B�3BƨB�#B��B		7B	�B	-B	9XB	B�B	jB	�7B	��B	�^B	ɺB	�HB	�B	��B
PB
�B
 �B
,B
6FB
>wB
E�B
L�B
S�B
ZB
bNB
ff1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
m�B
v�B
�'B
�?B
�B
�)B8RB:^B��B�B��B�^B��B�qB��B�qB�B�uBk�BT�BA�B?}B;dB9XB5?B2-B/B-B'�B#�B �B�B�B�B�B�B�B�B�B�B�B�BbBB
��B
�fB
�B
ȴB
�XB
��B
�VB
�B
v�B
p�B
aHB
L�B
E�B
�B

=B	��B	�yB	�#B	ȴB	�XB	��B	�uB	�B	s�B	O�B	;dB	oB�B�9BM�B1'B�B1'B'�B"�B1'B:^B\)Br�B�B�VB�B�3BƨB�#B��B		7B	�B	-B	9XB	B�B	jB	�7B	��B	�^B	ɺB	�HB	�B	��B
PB
�B
 �B
,B
6FB
>wB
E�B
L�B
S�B
ZB
bNB
ff1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070806025838  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070806025841  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070806025842  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070806025846  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070806025846  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070806025846  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070806030505                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070810041639  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070810041642  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070810041643  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070810041647  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070810041647  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070810041648  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070810043155                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070810041639  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518063858  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518063858  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518063859  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518063900  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518063900  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518063900  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518063900  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518063900  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518064405                      G�O�G�O�G�O�                