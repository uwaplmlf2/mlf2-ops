CDF   	   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900808 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ]A   JA  20070814085228  20090916044907  A5_23710_093                    2C  D   APEX-SBE 1565                                                   846 @Ԍ�d��s1   @Ԍ����]@72-V@^���vȴ1   ARGOS   A   A   A   @���AffAh  A�33A�33A�33B
  B��B1��BF  BZ  Bm��B�ffB�  B�  B�  B�  B���B�  B�33B���B�33B�33B�ffB�33G�O�G�O�G�O�G�O�G�O�C�C�fC$��C)��C.� C333C8L�C=33CBL�CGL�CQ��C[33Ce� Co�Cy� C���C��fC���C��fC���C��3C�� C���C��fC�� C�� C��fC���C�� CǙ�C̦fCљ�C�� C���C���C��C�3C�3C���C�s3D� DٚD��D��D�3D�fDٚD$��D)�3D.�3D3� D8��D=��DB��DG�3DL�3DQ�fDVٚD[ٚD`�fDe�fDjٚDo�3Dt�3Dy�fD�)�D�l�D���D���D�,�D�i�D��fD��D��D�ffD��fD��fD�,�D�i�Dڬ�D���D�#3D�Y�D�3D�ɚ1111111111111111111111111999991111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA33Ad��A���A���A陚B	33B  B0��BE33BY33Bl��B�  B���B���B���B���B�ffB���B���B�ffB���B���B�  B���G�O�G�O�G�O�G�O�G�O�C�fC�3C$ffC)ffC.L�C3  C8�C=  CB�CG�CQffC[  CeL�Cn�fCyL�C�� C���C�� C���C�� C���C�ffC�s3C���C��fC��fC���C�s3C¦fCǀ Č�Cр C֦fC۳3C�3C�s3CꙚCC� C�Y�D�3D��D� D� D�fD��D��D$��D)�fD.�fD3�3D8� D=� DB��DG�fDL�fDQ��DV��D[��D`��De��Dj��Do�fDt�fDy��D�#3D�ffD��fD��fD�&fD�c3D�� D��3D�fD�` D�� D�� D�&fD�c3DڦfD��fD��D�S3D��D��31111111111111111111111111999991111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��G�O�G�O�G�O�G�O�G�O�@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A� �A�$�A�+A�/A�/A�33A���A��A���A�\A�A��A�{A�  A�G�A�7LA�n�AōPAĲ-A��#A+A�A�A�A�G�O�G�O�G�O�G�O�G�O�A��^A�~�A�{A��wA�O�A��hA��!A�VA�C�A���A�bA��A���A�33A��A��A�`BA�33A���A���A��#A���A{�TAt=qAsK�Anr�AmƨAlffAf��A[�TAP�/AHbA=
=A5+A4�!A0E�A,v�A%"�A�`A?}A��A��AK�@���@��@�9@�@�G�@�"�@�Z@�\)@ʟ�@�x�@��@���@�z�@�M�@��@�@�x�@�&�@��-@��P@��@�V@�t�@w
=@e��@XA�@O�;@H��@A7L@;dZ@65?@3@,��@)7L@&$�@$(�@"��@!�@+@�@M�@5?1111111111111111111111111999991111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A� �A�$�A�+A�/A�/A�33A���A��A���A�\A�A��A�{A�  A�G�A�7LA�n�AōPAĲ-A��#A+A�A�A�A�G�O�G�O�G�O�G�O�G�O�A��^A�~�A�{A��wA�O�A��hA��!A�VA�C�A���A�bA��A���A�33A��A��A�`BA�33A���A���A��#A���A{�TAt=qAsK�Anr�AmƨAlffAf��A[�TAP�/AHbA=
=A5+A4�!A0E�A,v�A%"�A�`A?}A��A��AK�@���@��@�9@�@�G�@�"�@�Z@�\)@ʟ�@�x�@��@���@�z�@�M�@��@�@�x�@�&�@��-@��P@��@�V@�t�@w
=@e��@XA�@O�;@H��@A7L@;dZ@65?@3@,��@)7L@&$�@$(�@"��@!�@+@�@M�@5?1111111111111111111111111999991111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oG�O�G�O�G�O�G�O�G�O�;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�=B	�7B	�=B	�7B	�7B	�7B	�7B	�VB	�DB	�PB	�'B	�B
I�B
�B
�B
�BB�B"�B%�B'�B+B.B.B=qG�O�G�O�G�O�G�O�G�O�BcTBhsB~�B}�Bk�BhsBm�Bn�Bp�BgmBffBbNB^5BZBP�BJ�B<jB-B�B
�B
�)B
�^B
�oB
m�B
hsB
P�B
L�B
D�B
%�B	�B	��B	��B	k�B	S�B	P�B	ZB	ZB	O�B	@�B	A�B	?}B	8RB	49B	8RB	:^B	>wB	;dB	A�B	n�B	hsB	iyB	n�B	H�B	D�B	>wB	J�B	N�B	~�B	}�B	��B	��B	�uB	�
B	�NB	�`B	��B
B
bB
�B
'�B
.B
49B
H�B
S�B
XB
[#B
]/B
`BB
bNB
cTB
ffB
ffB
ffB
hsB
iy1111111111111111111111111999991111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�=B	�7B	�=B	�7B	�7B	�7B	�7B	�VB	�DB	�VB	�3B	��B
Q�B
�'B
�B
�B+B�B#�B&�B(�B.B0!B/B>wG�O�G�O�G�O�G�O�G�O�BffBn�B�B� Bn�BjBm�Bp�Br�BiyBffBcTB^5B[#BP�BK�B=qB.B�B
�B
�5B
�jB
�{B
m�B
iyB
P�B
L�B
E�B
(�B	�B	B	��B	m�B	S�B	Q�B	[#B	\)B	Q�B	A�B	B�B	@�B	9XB	5?B	8RB	:^B	>wB	<jB	A�B	n�B	hsB	iyB	o�B	I�B	E�B	>wB	J�B	N�B	� B	}�B	��B	��B	�uB	�
B	�NB	�`B	��B
B
bB
�B
'�B
.B
49B
H�B
S�B
XB
[#B
]/B
`BB
bNB
cTB
ffB
ffB
ffB
hsB
iy1111111111111111111111111999991111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
G�O�G�O�G�O�G�O�G�O�<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200708260255522007082602555220070826025552200708260303172007082603031720070826030317200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20070814085226  IP                  G�O�G�O�G�O�                JM  ARFMDECPA5                                                                  20070816115456  IP                  G�O�G�O�G�O�                JM  ARGQRQCP1                                                                   20070816115456  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20070826025552  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070826025552  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070826030317  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916044725  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916044907                      G�O�G�O�G�O�                