CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900808 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               \A   JA  20070803045349  20090916044908  A5_23710_092                    2C  D   APEX-SBE 1565                                                   846 @Ԋ-W1   @Ԋ.��s�@6�\(�@^�?|�h1   ARGOS   A   A   A   @���A��Ad��A�  A���A�ffB
  B33B333BE33B[33Bm33B���B�ffB�ffB�ffB�  B���B�  B���BЙ�B���B���B���B�ffC��C� CL�CL�C  C� CffC$33C)�C.� C3� C833C=��CB33CG� CQ�C[L�Ce33Cn�fCx��C��3C��3C���C���C��fC�� C���C�� C���C��3C�� C�� C���C¦fCǙ�C�s3C�s3Cֳ3Cۙ�C�s3C�fC��C���C��fC���D�3D�fD� D�3D� D��D�3D$� D)�fD.��D3��D8�fD=�fDB�3DG��DL�3DQ�fDV��D[ٚD`�fDeٚDj� Do�fDt��Dy�3D�,�D�i�D���D��D�#3D�` D���D��fD��D�c3D��fD���D�&fD�Y�Dڙ�D���D��D�i�D�D�c31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A  A`  A���A�33A�  B��B  B2  BD  BZ  Bl  B�33B���B���B���B�ffB�33B�ffB�33B�  B�33B�33B�33B���CL�C33C  C  C�3C33C�C#�fC(��C.33C333C7�fC=L�CA�fCG33CP��C[  Cd�fCn��Cx� C���C���C�s3C��fC�� C�Y�C�s3C�Y�C�s3C���C���C���C�ffC C�s3C�L�C�L�C֌�C�s3C�L�C� C�ffC�fC� C�s3D� D�3D��D� D��D��D� D$��D)�3D.�fD3�fD8�3D=�3DB� DG��DL� DQ�3DV��D[�fD`�3De�fDj��Do�3Dt��Dy� D�#3D�` D�� D�� D��D�VfD��3D���D�3D�Y�D���D��3D��D�P Dڐ D��3D�3D�` D� D�Y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A���A��;A��`A��#A�v�A�A�z�A�K�A�FA��A��HA�r�A��#AΏ\A���A�bNA��
A�"�Aŕ�AËDA��A�ffA��A���A��A�?}A�;dA�+A�(�A���A�  A�z�A���A��A��A�
=A�1'A� �A�S�A���A���A�\)A��+A��;A��7A��/A�bA��PA��A�|�A��hA���AK�A{�TAv��AjjA[t�AZ~�AY�ATv�AP~�AIAGAA��A>bNA;��A1�7A-�#A(��A ��A{A1A
��A�F@�v�@�@��@�K�@�5?@� �@˾w@ɲ-@���@��D@�G�@���@��@��@��@�n�@�\)@��@��!@�(�@��@x��@j��@W�;@RM�@E�h@?�@<�@1hs@/K�@/\)@)�^@$�@#C�@ A�@�@bN@��@;d@
M�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A���A��;A��`A��#A�v�A�A�z�A�K�A�FA��A��HA�r�A��#AΏ\A���A�bNA��
A�"�Aŕ�AËDA��A�ffA��A���A��A�?}A�;dA�+A�(�A���A�  A�z�A���A��A��A�
=A�1'A� �A�S�A���A���A�\)A��+A��;A��7A��/A�bA��PA��A�|�A��hA���AK�A{�TAv��AjjA[t�AZ~�AY�ATv�AP~�AIAGAA��A>bNA;��A1�7A-�#A(��A ��A{A1A
��A�F@�v�@�@��@�K�@�5?@� �@˾w@ɲ-@���@��D@�G�@���@��@��@��@�n�@�\)@��@��!@�(�@��@x��@j��@W�;@RM�@E�h@?�@<�@1hs@/K�@/\)@)�^@$�@#C�@ A�@�@bN@��@;d@
M�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	hB	!�B	'�B	)�B	)�B	-B	A�B	��B	�B
>wB
ffB
�1B
�B
�B
��B.B49B49B5?B5?B@�BH�B`BBcTB;dBA�BN�BVB^5BZBVBXBO�BQ�BO�BJ�BI�BE�BB�B8RB/B/B)�B!�BhBB  B
��B
�ZB
�yB
�`B
��B
�^B
��B
�VB
v�B
E�B
�B
uB
PB	��B	��B	�NB	��B	�!B	�^B	�B	�B	��B	��B	�=B	�PB	�JB	I�B	^5B	N�B�ZB�B��B��B	s�B	�B	�B	l�B	�{B	�B	�RB	�dB	B	ƨB	��B	��B	�#B	��B	�B	�B	��B
bB
�B
49B
:^B
:^B
@�B
>wB
O�B
YB
T�B
S�B
W
B
ZB
]/B
bNB
gmB
k�B
q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	oB	"�B	'�B	)�B	)�B	/B	D�B	��B	��B
A�B
k�B
�VB
�B
��BB0!B49B5?B6FB7LBA�BK�BbNBffB<jBB�BP�BZB`BB[#BXBYBQ�BS�BR�BK�BJ�BF�BD�B9XB0!B0!B+B#�BoBB  B
��B
�ZB
�B
�fB
��B
�jB
��B
�\B
y�B
I�B
�B
uB
VB	��B	��B	�TB	�B	�'B	�dB	�'B	�B	��B	��B	�DB	�VB	�PB	I�B	`BB	P�B�`B�#B��B��B	s�B	�B	�B	l�B	�{B	�B	�RB	�dB	B	ǮB	��B	��B	�)B	��B	�B	�B	��B
bB
�B
49B
:^B
:^B
@�B
>wB
O�B
YB
T�B
S�B
W
B
ZB
]/B
bNB
gmB
k�B
q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.3(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200708160255022007081602550220070816025502200708160306332007081603063320070816030633200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20070803045344  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070803045349  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070803045350  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070803045353  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070803045354  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070803045354  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070803051016                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070807035048  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070807035052  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070807035053  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070807035057  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070807035057  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070807035058  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070807050203                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20090330065657  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090330074839  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090330074839  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090330074839  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090330074840  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090330074840  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090330074841  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090330074841  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090330074841  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090330075136                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070806115927  CV  DAT$            G�O�G�O�F�Qq                JM  ARCAJMQC1.0                                                                 20070816025502  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070816025502  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070816030633  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916044727  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916044908                      G�O�G�O�G�O�                