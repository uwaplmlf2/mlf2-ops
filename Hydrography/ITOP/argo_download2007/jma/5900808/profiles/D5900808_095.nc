CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   a   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  4    PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  6   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  7�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  9t   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  9�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  ;\   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =D   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  >�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?,   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  d  @�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  B�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   C(   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   L(   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   U(   CALIBRATION_DATE      	   
                
_FillValue                  �  ^(   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    ^�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    ^�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    ^�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    ^�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  ^�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    ^�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    _   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    _   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         _   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         _    HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        _$   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    _(Argo profile    2.2 1.2 19500101000000  5900808 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               _A   JA  20070902010958  20090916044914  A5_23710_095                    2C  D   APEX-SBE 1565                                                   846 @ԑ�8�1   @ԑ��l@8�vȴ9@^����l�1   ARGOS   A   A   A   @�33A  Aa��A�  A�  A�ffB	33B��B2  BF  BY��Bl��B�ffB�33B���B���B�33B���B���B�ffB�ffB�  B㙚B���B�ffC  CL�C�C� C��C��C� C$  C(�fC-�fC2��C833C=�CB�CG�CQ33C[33Ce  CoL�Cy33C��3C���C���C�� C��3C���C�� C���C��fC��fC���C��fC���C�Cǀ C̙�C�� C֦fC�ffC�� C�L�C�s3C�fC��fC�� DٚD��D��D�3D�fDٚD� D$� D)� D.ٚD3��D8��D=�3DBٚDG� DL�3DQ� DV�fD[�fD`��De��Dj�3Do��Dt�fDy�fD�0 D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @���A33A\��A���A���A�  B  B��B0��BD��BXffBk��B���B���B�33B�  B���B�33B�  B���B���B�ffB�  B�33B���C �3C  C
��C33CL�CL�C33C#�3C(��C-��C2L�C7�fC<��CA��CF��CP�fCZ�fCd�3Co  Cx�fC���C�ffC��fC���C���C�ffC���C�ffC�� C�� C�s3C�� C�ffC�ffC�Y�C�s3Cљ�Cր C�@ C�Y�C�&fC�L�C� C� C���D�fD�fD��D� D�3D�fD��D$��D)��D.�fD3��D8��D=� DB�fDG��DL� DQ��DV�3D[�3D`��De�fDj� Do�fDt�3Dy�3D�&fD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�l�A��yA�;dA�(�A��A���AՁA���A��AёhAЮA�/Aǝ�A�%A�(�A�9XA���A��A��A��A�+A�E�A�ȴA��PA��-A���A���A��yA�VA�$�A�ƨA��A�E�A��A�XA�|�A��mA��A���A���A���A��A�l�A�z�A��7A�"�A��A�7A{��Aw�As�Ap  Ak7LAfVAc?}A]x�AZ  AT�AN�uADbNAA�wA>Q�A:�HA5�#A2z�A1�mA0I�A+/A%��A!�;A|�A��AM�AO�AhsA ��@�J@�O�@�j@�1@ݩ�@ٺ^@�Q�@ă@��+@���@�7L@���@�1'@�@��`@���@�ff@�S�@��`@i��@`Q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�l�A��yA�;dA�(�A��A���AՁA���A��AёhAЮA�/Aǝ�A�%A�(�A�9XA���A��A��A��A�+A�E�A�ȴA��PA��-A���A���A��yA�VA�$�A�ƨA��A�E�A��A�XA�|�A��mA��A���A���A���A��A�l�A�z�A��7A�"�A��A�7A{��Aw�As�Ap  Ak7LAfVAc?}A]x�AZ  AT�AN�uADbNAA�wA>Q�A:�HA5�#A2z�A1�mA0I�A+/A%��A!�;A|�A��AM�AO�AhsA ��@�J@�O�@�j@�1@ݩ�@ٺ^@�Q�@ă@��+@���@�7L@���@�1'@�@��`@���@�ff@�S�@��`@i��@`Q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�=B
�{B
�oB
�PB
�B
�=B
�hB
�VB
�{B
��B
��B
�dBB2-B?}B.B@�BP�BP�BgmBffBcTBZBXBYBXB\)B[#BT�B\)BVBP�BK�BH�BA�B<jB9XB7LB0!B)�B!�B{BVB
��B
�B
��B
�dB
�B
��B
�B
n�B
\)B
C�B
,B
�B
B	�B	�#B	�XB	�uB	�+B	u�B	ffB	P�B	D�B	A�B	8RB	,B	{B	�B	'�B�B�B�B	"�B	C�B	=qB	I�B	^5B	P�B	F�B	\)B	bNB	q�B	r�B	|�B	�B	��B	��B	��B	�B	�B	�B	�B	��B
hB
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B
�\B
��B
�uB
�VB
�B
�DB
�hB
�bB
��B
��B
��B
��BbB7LBD�B0!BC�BQ�BR�BhsBgmBdZBZBYBZBZB]/B]/BYB_;BW
BR�BK�BJ�BB�B=qB9XB8RB1'B)�B"�B{B\B
��B
�B
��B
�jB
�B
��B
�B
o�B
]/B
D�B
-B
�B
B	�B	�/B	�jB	�{B	�1B	v�B	gmB	Q�B	D�B	A�B	9XB	-B	�B	�B	(�B�B�B�B	$�B	D�B	>wB	I�B	^5B	Q�B	F�B	]/B	cTB	q�B	s�B	|�B	�B	��B	��B	��B	�B	�B	�B	�B	��B
hB
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.3(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709130759042007091307590420070913075904200709130815442007091308154420070913081544200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20070902010941  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070902010958  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070902011002  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070902011015  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070902011015  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070902011017  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070902024858                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070906034938  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070906034942  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070906034943  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070906034947  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070906034947  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070906034948  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070906045138                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20090330065700  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090330074846  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090330074846  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090330074847  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090330074848  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090330074848  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090330074848  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090330074848  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090330074848  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090330075125                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070905112315  CV  DAT$            G�O�G�O�F��q                JM  ARCAJMQC1.0                                                                 20070913075904  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070913075904  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070913081544  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916044740  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916044914                      G�O�G�O�G�O�                