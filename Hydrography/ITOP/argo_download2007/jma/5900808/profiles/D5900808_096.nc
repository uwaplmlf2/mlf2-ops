CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   V   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     X  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     X  4L   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  5�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     X  5�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  7T   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  8�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  9   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  :\   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  :�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  <   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  =d   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  =�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  ?   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  ?l   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  @�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   AT   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   JT   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   ST   CALIBRATION_DATE      	   
                
_FillValue                  �  \T   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    \�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    \�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    \�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    \�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  \�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    ]$   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    ]4   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    ]8   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         ]H   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         ]L   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        ]P   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    ]TArgo profile    2.2 1.2 19500101000000  5900808 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               `A   JA  20070912025412  20090916044918  A5_23710_096                    2C  D   APEX-SBE 1565                                                   846 @Ԕ'��Ɗ1   @Ԕ-�e�:@8]p��
=@^���l�D1   ARGOS   A   A   A   @�33A��AfffA�ffA�  A���B	33B33B2��BE33BX��Bn  B�  B�  B���B���B�  B�ffB�ffBƙ�BЙ�B���B�33B���B�  CL�C�fC
�3C�fC�CL�C�fC$ffC)ffC.ffC3ffC8� C=33CB33CGL�CQL�C[��Ce� CoffCyL�C�� C���C�s3C�� C��3C���C�� C�� C�s3C���C���C�ffC��fC³3Cǳ3Č�Cр C�ffCۙ�C�ffC��C�33C� C� C�� D� D� D��D�fD� D�3D��D$� D)��D.�3D3� D8� D=�fDB��DGٚDL�f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���A  Aa��A�  A���A�ffB  B  B1��BD  BW��Bl��B�ffB�ffB�33B�  B�ffB���B���B�  B�  B�33B䙚B�33B�ffC  C��C
ffC��C��C  C��C$�C)�C.�C3�C833C<�fCA�fCG  CQ  C[L�Ce33Co�Cy  C�Y�C�s3C�L�C���C���C�s3C���C���C�L�C�s3C�s3C�@ C�� C�Cǌ�C�ffC�Y�C�@ C�s3C�@ C�ffC��C�Y�C�Y�C���D��D��D�fD�3D��D� D��D$��D)�fD.� D3��D8��D=�3DB��DG�fDL�311111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�\)A�\)A�^5A�7LA�ZA��A���A�~�A�5?A�`BAڝ�A�v�A�\)A�;dA���A��/AɸRA���A�O�A��A��PA��9A��A��A���A���A�n�A�hsA��A�  A�VA��A���A�{A�-A�  A��RA�r�A�1A���A���A��A��;A��7A�t�A��HA��hA�t�A��A��A��+A�-At�/Aq�hAi�A`��A[�
AQl�AL��AH��AH1AGVAE&�AC��A@-A6��A0��A.1'A+�7A'hsAA�AĜA+A�+@��-@�|�@��@噚@�dZ@ݑh@�t�@�`B@���@�1@�x�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�\)A�\)A�^5A�7LA�ZA��A���A�~�A�5?A�`BAڝ�A�v�A�\)A�;dA���A��/AɸRA���A�O�A��A��PA��9A��A��A���A���A�n�A�hsA��A�  A�VA��A���A�{A�-A�  A��RA�r�A�1A���A���A��A��;A��7A�t�A��HA��hA�t�A��A��A��+A�-At�/Aq�hAi�A`��A[�
AQl�AL��AH��AH1AGVAE&�AC��A@-A6��A0��A.1'A+�7A'hsAA�AĜA+A�+@��-@�|�@��@噚@�dZ@ݑh@�t�@�`B@���@�1@�x�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	7B	7B1B�B]/B��B	hB	t�B	�mB
bB
�B
&�B
D�B
	7B
9XB
jB
hsB
�B
ɺB
��B
��B
��B
��BO�Bp�Bu�Bx�Bw�B� B�B�B�B�B�B|�Bu�Bo�Bm�BgmB[#BJ�B@�B8RB.B(�B\BPBDBB
�B
��B
�9B
q�B
]/B
8RB
VB	��B	B	�-B	��B	��B	��B	��B	��B	�JB	m�B	ZB	N�B	F�B	;dB	&�B	hB	uB	\B	B	�B	1'B	<jB	8RB	B�B	J�B	Q�B	bNB	iyB	m�B	z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B	7B	7B1B�B^5B��B	oB	w�B	�yB
hB
�B
(�B
G�B
PB
<jB
l�B
o�B
�B
��B
��B
��B
��BBS�Bs�Bu�Bx�By�B�B�B�%B�B�B�B� Bx�Bo�Bn�BhsB]/BK�B@�B9XB/B+B\BPBDBB
�B
��B
�LB
r�B
_;B
:^B
\B	��B	ÖB	�3B	��B	��B	��B	��B	��B	�VB	o�B	[#B	O�B	G�B	<jB	'�B	hB	uB	\B	+B	�B	2-B	<jB	8RB	B�B	J�B	Q�B	bNB	iyB	n�B	z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111  <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(ThisCycle), where SP is SURFACE PRESSURE from this cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(ThisCycle)=0.3(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709200013302007101501565920071015015659200803081241332008030812413320080308124133200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20070912025410  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070912025412  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070912025413  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070912025417  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070912025417  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070912025417  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070912031132                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070916041111  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070916041126  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070916041129  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070916041137  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070916041138  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070916041139  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070916043516                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20090330065701  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090330074848  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090330074849  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090330074849  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090330074850  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090330074850  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090330074850  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090330074850  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090330074850  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090330075129                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070920001330  CV  DAT$            G�O�G�O�F��q                JM  ARCAJMQC1.0                                                                 20071015015659  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071015015659  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080308124133  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916044744  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916044918                      G�O�G�O�G�O�                