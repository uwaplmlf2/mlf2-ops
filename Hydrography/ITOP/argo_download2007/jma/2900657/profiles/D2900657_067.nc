CDF   0   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900657 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               CA   JA  20071007033639  20090831075627  A9_67027_067                    2C  D   APEX-SBE 2975                                                   846 @Ԛk�pB�1   @Ԛnz�H@8z��vȴ@a�5?|�1   ARGOS   A   A   A   @�  A33A�33A�33BffBFffBl��B���B�ffB���B�ffB���B�  CffCffC�C  C)  C3L�C=�CF��C[33Cn�fC���C�� C���C���C���C���C�ffC�L�CѦfC�s3C�� CC�Y�D�fD�3D�3D� D�3D��D�3D$��D)�3D.�fD3ٚD8ٚD=�3DB�fDG��DN  DTY�DZ�3D`� Dg�Dm@ Ds��Dy� D�  D�i�D���D�� D��D�l�D�� D�ffD��D�ffD���D�Y�D��f111111111111111111111111111111111111111111111111111111111111111111111111@�33A��A�  A�  B��BD��Bk33B�  B���B���Bř�B�  B�33C  C  C�3C��C(��C2�fC<�3CFffCZ��Cn� C�Y�C���C�ffC�ffC�ffC�ffC�33C��C�s3C�@ C��C�ffC�&fD��D��D��D�fD��D�3D��D$�3D)��D.��D3� D8� D=��DB��DG�3DNfDT@ DZy�D`�fDf�3Dm&fDs� Dy�fD�3D�\�D�� D��3D� D�` D��3D�Y�D���D�Y�D�� D�L�D�ٚ111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A޼jA޸RA޶FAެA��/A��A�hsA��DA�1'A�ffA�r�A�1A���A�A�&�A��A��A�+A��DA�A��A�E�A�hsA�dZA��A��wA��A�ƨA��TA�=qA�ƨA|��As�Ab  AW��AJQ�A>��A6  A+�TA�;A�9A �+@���@�Z@�J@�V@�V@��@�z�@�9X@�1'@��@�z�@��\@��R@��@~��@q��@lj@aG�@W
=@N�+@D9X@;t�@4��@*=q@ȴ@��@V@�m@ �`?�x�111111111111111111111111111111111111111111111111111111111111111111111111A޼jA޸RA޶FAެA��/A��A�hsA��DA�1'A�ffA�r�A�1A���A�A�&�A��A��A�+A��DA�A��A�E�A�hsA�dZA��A��wA��A�ƨA��TA�=qA�ƨA|��As�Ab  AW��AJQ�A>��A6  A+�TA�;A�9A �+@���@�Z@�J@�V@�V@��@�z�@�9X@�1'@��@�z�@��\@��R@��@~��@q��@lj@aG�@W
=@N�+@D9X@;t�@4��@*=q@ȴ@��@V@�m@ �`?�x�111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
��B
��B
��B
��B
�B
�B"�B,BA�BE�BA�BA�B>wB<jB:^B8RB6FB33B2-B0!B/B)�B$�B �B�BuBDBB
�B
��B
�LB
��B
m�B
"�B	�B	�!B	� B	ZB	,B��B��B�VB~�BcTBZB[#BcTBp�Bx�B�{B��B�!B��B�B	B	VB	�B	;dB	K�B	n�B	�=B	��B	��B	�
B	�`B	��B
�B
6FB
G�B
YB
`BB
iy111111111111111111111111111111111111111111111111111111111111111111111111B
��B
��B
��B  B
�B
��B$�B1'BB�BE�BC�BB�B>wB<jB;dB9XB7LB33B2-B0!B/B)�B$�B �B�BuBDBB
�B
��B
�RB
��B
o�B
#�B	�B	�'B	�B	[#B	/BB��B�\B�BdZBZB\)BdZBp�By�B�{B��B�'B��B�B	B	VB	�B	;dB	K�B	n�B	�=B	��B	��B	�
B	�`B	��B
�B
6FB
G�B
YB
`BB
iy111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.4(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710150158352007101501583520071015015835200710150213092007101502130920071015021309200908190000002009081900000020090819000000  JA  ARFMdecpA9_b                                                                20071007033606  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071007033639  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071007033644  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071007033655  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071007033655  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071007033657  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071007041457                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071011043508  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071011043512  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071011043513  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071011043517  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071011043517  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071011043518  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071011050857                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071017003804  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071017003804  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071017003808  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071017003808  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071017003808  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071017003808  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071017003809  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071018014231                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828045938  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828045940  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828045949  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828045949  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828045950  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828045950  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828045950  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828063134                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071011043508  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090424013051  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090424013051  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090424013051  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090424013051  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090424013052  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090424013052  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090424013052  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090424013052  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090424013052  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090424014106                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071010105042  CV  DAT$            G�O�G�O�F��q                JM  ARCAJMQC1.0                                                                 20071015015835  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071015015835  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071015021309  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090819000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075558  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075627                      G�O�G�O�G�O�                