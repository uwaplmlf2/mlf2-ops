CDF   0   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900657 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               :A   JA  20070823013636  20090831075634  A9_67027_058                    2C  D   APEX-SBE 2975                                                   846 @ԏ&<M^|1   @ԏ)���@;ffffff@b��-V1   ARGOS   A   A   A   @�33A��A�  A�ffB33BD��Bm��B�ffB���B�  B�ffB���B���CffC�C� C33C)ffC2�fC=ffCG�C[L�Co� C�� C���C���C���C���C���C�� CǙ�Cљ�C�ffC�3C� C�L�D��D�fDٚD��D�3D�3D� D$��D)� D.ٚD3�fD8��D=� DB��DG�fDN  DTFfDZ��D`�fDg3DmffDs��Dy�3D�)�D�ffD��fD��3D�&fD�l�D��3D�c3D�� D�i�D�� D�VfD���D��1111111111111111111111111111111111111111111111111111111111111111111111111   @�ffA33A���A�33B��BC33Bl  B���B���B�33Bř�B�  B�  C  C
�3C�C��C)  C2� C=  CF�3CZ�fCo�C�L�C�ffC�ffC�Y�C���C���C���C�ffC�ffC�33C� C�L�C��D� D��D� D�3D��D��D�fD$�3D)�fD.� D3��D8�3D=�fDB�3DG��DNfDT,�DZs3D`��Df��DmL�Dss3Dy��D��D�Y�D���D��fD��D�` D��fD�VfD��3D�\�D��3D�I�D�� D� 1111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��TA�{A�wA�/A�=qA��A�z�A��DA��A��9A�O�A���A��A��jA�$�A�VA�O�A��
A��\A���A��`A�{A���A���A���A�?}A}S�AuG�Ao|�Ak�FA`�AS�;AJĜA?�;A7��A*�yA�jAt�A�A��@�`B@���@�u@�D@�ƨ@���@���@�"�@��@��F@��@���@�S�@���@���@|�@t�@n�@k��@_+@N5?@D�D@0�@'�@ ��@��@�j@Ĝ@V@z�@	&�@�\@=q1111111111111111111111111111111111111111111111111111111111111111111111111   A��TA�{A�wA�/A�=qA��A�z�A��DA��A��9A�O�A���A��A��jA�$�A�VA�O�A��
A��\A���A��`A�{A���A���A���A�?}A}S�AuG�Ao|�Ak�FA`�AS�;AJĜA?�;A7��A*�yA�jAt�A�A��@�`B@���@�u@�D@�ƨ@���@���@�"�@��@��F@��@���@�S�@���@���@|�@t�@n�@k��@_+@N5?@D�D@0�@'�@ ��@��@�j@Ĝ@V@z�@	&�@�\@=q1111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
YB
gmB
cTB
��B
�5B\BB�B]/BR�B7LBB�B7LB5?B1'B.B,B)�B(�B-B+B'�B�BVB
��B
�sB
��B
��B
z�B
_;B
L�B
�B	�fB	�}B	�{B	jB	 �B�BɺB��B{�Bn�B�1B~�B� BW
BYBs�B�PB��B�uB��BȴB�HB��B	1B	�B	-B	B�B	T�B	w�B	��B	�RB	��B	�)B	�mB	�B
1B
�B
"�B
,B
:^B
T�B
W
1111111111111111111111111111111111111111111111111111111111111111111111111   B
[#B
hsB
m�B
ƨB
�`BhBC�BaHBVB8RBC�B8RB6FB1'B.B-B)�B)�B-B+B(�B�B\B
��B
�yB
��B
��B
{�B
`BB
M�B
�B	�mB	��B	��B	l�B	"�B�B��B��B|�Bn�B�7B~�B�BYBZBt�B�VB��B�{B��BȴB�HB��B	1B	�B	-B	B�B	T�B	w�B	��B	�RB	��B	�)B	�mB	�B
1B
�B
"�B
,B
:^B
T�B
W
1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.4(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200708310234212007083102342120070831023421200708310237382007083102373820070831023738200908190000002009081900000020090819000000  JA  ARFMdecpA9_b                                                                20070823013632  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070823013636  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070823013637  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070823013641  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070823013641  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070823013641  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070823014338                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070827044815  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070827044823  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070827044825  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070827044832  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070827044833  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070827044833  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070827050654                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071017003715  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071017003715  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071017003719  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071017003719  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071017003719  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071017003719  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071017003720  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071018014313                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828045745  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828045747  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828045755  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828045755  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828045755  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828045755  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828045757  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828062839                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070827044815  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090424013030  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090424013030  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090424013030  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090424013031  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090424013032  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090424013032  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090424013032  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090424013032  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090424013032  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090424014057                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070826115640  CV  DAT$            G�O�G�O�F�yM                JM  ARCAJMQC1.0                                                                 20070831023421  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070831023421  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070831023738  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090819000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075603  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075634                      G�O�G�O�G�O�                