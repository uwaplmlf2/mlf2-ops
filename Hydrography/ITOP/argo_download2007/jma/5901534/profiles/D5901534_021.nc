CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901534 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20071001130245  20090908021642  A9_66116_021                    2C  D   APEX-SBE 2825                                                   846 @ԙ2@y�1   @ԙ=p��@4�ȴ9X@a�9XbN1   ARGOS   A   A   A   @���A33Aa��A���A�33A�  BffB��B2  BE33BY��Bn  B���B�  B�  B�  B���B�  B�  B���B���Bڙ�B���B�ffB�  C�3CffC33C�C  CL�C�C$33C)33C.ffC3��C8L�C=� CBffCGL�CQffC[�Cd�fCo33Cx�fC��fC��fC��3C���C�ffC�s3C���C���C���C���C��3C�ffC�� C³3Cǌ�C̀ C�� C���CۦfC�fC�� C� C�fC��C�s3D�3D�3D��D�fDٚD��D� D$ٚD)ٚD.� D3� D8ٚD=��DB��DG�3DL� DQ�3DV�3D[�fD`��De��Dj�fDo�fDt��Dy�fD�#3D�c3D�� D��D�#3D�c3D��fD��fD�,�D�l�D���D��3D��D�p Dڬ�D��3D��D�i�D� D�Vf1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  AffA\��A�33A���A陚B33BffB0��BD  BXffBl��B�33B�ffB�ffB�ffB�  B�ffB�ffB�33B�33B�  B�33B���B�ffCffC�C
�fC��C�3C  C��C#�fC(�fC.�C3L�C8  C=33CB�CG  CQ�CZ��Cd��Cn�fCx��C�� C�� C���C�ffC�@ C�L�C�s3C�ffC�ffC�s3C���C�@ C�Y�C�C�ffC�Y�Cљ�C֦fCۀ C�� C噚C�Y�C� C�ffC�L�D� D� D��D�3D�fD��D��D$�fD)�fD.��D3��D8�fD=��DB��DG� DL��DQ� DV� D[�3D`�fDe��Dj�3Do�3Dt��Dy�3D��D�Y�D��fD�� D��D�Y�D���D���D�#3D�c3D��3D�ٚD�3D�ffDڣ3D�ٚD� D�` D�fD�L�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��#A�z�A�-A��HA��A���A�A��A��A��A�bA�uA���AԼjA�+A�n�A��yA�~�Ȧ+A�33Aǥ�AžwAøRA��A�A�G�A�x�A��A�n�A���A��A��mA��A�ZA� �A�C�A�1A��A�(�A���A�`BA��A�A�=qA���A��
A��9A�C�A��wA�M�A��A��A��9A}�;Az^5AvjAqdZAl1Aj=qAgO�A`  AYAM�AC�A@5?A77LA2ffA.��A*�A$VA�jAx�A �@��@���@�@�ƨ@�|�@���@��-@���@�?}@�t�@��/@�v�@�5?@�ff@�X@�hs@�=q@��D@��@���@���@�v�@l�@o�w@d(�@Vff@Lj@D1@>��@8r�@2-@+t�@'�@!hs@V@G�@�y@��@X@�+@C�@+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��#A�z�A�-A��HA��A���A�A��A��A��A�bA�uA���AԼjA�+A�n�A��yA�~�Ȧ+A�33Aǥ�AžwAøRA��A�A�G�A�x�A��A�n�A���A��A��mA��A�ZA� �A�C�A�1A��A�(�A���A�`BA��A�A�=qA���A��
A��9A�C�A��wA�M�A��A��A��9A}�;Az^5AvjAqdZAl1Aj=qAgO�A`  AYAM�AC�A@5?A77LA2ffA.��A*�A$VA�jAx�A �@��@���@�@�ƨ@�|�@���@��-@���@�?}@�t�@��/@�v�@�5?@�ff@�X@�hs@�=q@��D@��@���@���@�v�@l�@o�w@d(�@Vff@Lj@D1@>��@8r�@2-@+t�@'�@!hs@V@G�@�y@��@X@�+@C�@+1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
o�B
p�B
n�B
jB
n�B
o�B
p�B
��B6FBiyB�XB�5BK�BJ�BK�BbNBdZBffBffBZBYBXB\)BXBO�BE�B@�B<jB,B&�B�B{BVB��B�B�NB�BŢB�^B��Bm�BbNB[#BM�B?}B49B)�B�BDB
��B
�mB
ĜB
��B
�\B
x�B
bNB
I�B
-B
$�B
�B	�B	��B	��B	jB	W
B	0!B	�B	JB��B�HB�jB��B{�BjBn�Bu�B�B�bB�?B��B�/B�B	B	bB	�B	,B	D�B	F�B	ffB	t�B	�B	�{B	��B	�3B	��B	�B	�B	��B	��B	��B
\B
!�B
,B
8RB
;dB
C�B
K�B
O�B
Q�B
XB
\)B
_;B
cTB
gmB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
o�B
p�B
n�B
jB
n�B
o�B
p�B
��B7LBl�B�jB�mBL�BL�BM�BbNBe`BhsBhsB\)B[#BZB^5BZBQ�BF�BB�B=qB/B(�B!�B�BbB��B�B�TB�BƨB�jB��Bo�BcTB\)BO�B@�B5?B+B�BJB
��B
�sB
ŢB
��B
�bB
y�B
cTB
J�B
-B
%�B
�B	�B	��B	��B	k�B	YB	1'B	�B	PB��B�NB�wB��B|�Bk�Bo�Bv�B�B�hB�FB��B�5B�B	B	bB	�B	,B	D�B	F�B	ffB	t�B	�B	�{B	��B	�3B	��B	�B	�B	��B	��B	��B
\B
!�B
,B
8RB
;dB
C�B
K�B
O�B
Q�B
XB
\)B
_;B
cTB
gmB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.3(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710141956192007101419561920071014195619200710142012032007101420120320071014201203200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20071001130243  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071001130245  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071001130246  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071001130250  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071001130250  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071001130250  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071001130935                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071005161643  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071005161647  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071005161647  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071005161651  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071005161652  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071005161652  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071005190701                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071005161643  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423045259  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423045259  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423045259  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423045300  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423045300  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423045301  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423045301  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423045301  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423045650                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071004224900  CV  DAT$            G�O�G�O�F��a                JM  ARCAJMQC1.0                                                                 20071014195619  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071014195619  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071014201203  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021610  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021642                      G�O�G�O�G�O�                