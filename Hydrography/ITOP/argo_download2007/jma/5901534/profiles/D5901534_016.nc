CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901534 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070812190405  20090908021652  A9_66116_016                    2C  D   APEX-SBE 2825                                                   846 @Ԍ����1   @Ԍ��+�]@4&$�/�@b"��`B1   ARGOS   A   A   A   @���A��Ai��A�33A�  A�33B
ffB33B0��BF  B[33BnffB�ffB�  B�ffB�  B�  B�ffB�  B�33B���B�  B�  B���B�ffC�C33C��C� CL�CL�CL�C$�C)�C.L�C3ffC8L�C=ffCB� CG��CQ� C[�Ce� CoL�Cy33C��fC��3C���C�s3C��3C���C�ffC�ffC���C���C��3C���C��fC¦fCǳ3C�s3Cь�C֦fCۀ C�fC��C���C�� C���C���D��D��D� DٚD��D�3D��D$� D)��D.��D3�3D8�fD=� DB�fDG��DL�3DQٚDV��D[��D`ٚDe�fDjٚDo� Dt�3Dy�3D�#3D�ffD��3D���D�  D�c3D���D��D�,�D�ffD�� D��D��D�i�Dڜ�D�ٚD�&fD�\�D�D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA��AfffA���A�ffA陚B	��BffB0  BE33BZffBm��B�  B���B�  B���B���B�  B���B���B�ffBٙ�B䙚B�ffB�  C �fC  CffCL�C�C�C�C#�fC(�fC.�C333C8�C=33CBL�CGffCQL�CZ�fCeL�Co�Cy  C���C���C�s3C�Y�C���C�s3C�L�C�L�C�s3C��3C���C�� C���C�CǙ�C�Y�C�s3C֌�C�ffC���C�s3C�3C�fC� C�s3D� D� D�3D��D� D�fD��D$�3D)� D.��D3�fD8��D=�3DB��DG� DL�fDQ��DV� D[� D`��De��Dj��Do�3Dt�fDy�fD��D�` D���D��fD��D�\�D��fD��3D�&fD�` D���D��3D�3D�c3DږfD��3D�  D�VfD�3D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�XA��
A��HA�ȴA�A�jA�ffA�"�A�dZA���Aӏ\A�G�A��mA�%Aɧ�Aȇ+A�?}AŋDA��HA��A�ƨA�l�A�C�A�%A�S�A���A���A�E�A�/A��A���A��;A�C�A��A�ffA��`A��A��`A���A���A�I�A��PA�hsA�z�A�ƨA�ZA�  A���A��
A{�wAx  Av��Aq��Al�\Ae\)A_O�AY��ASAE�#AB��A;��A;O�A8�9A3C�A.�A+t�A&-A ��A�A��AA�H@��u@� �@ޗ�@��`@�&�@��7@���@���@���@�1'@��H@�(�@�O�@�V@�{@��@�(�@�@�S�@�V@��/@�o@��9@{"�@j-@_|�@Q�#@L9X@E�@<�D@5O�@/�@+"�@&V@"�H@��@�y@z�@��@�+@33@\)@`B1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�XA��
A��HA�ȴA�A�jA�ffA�"�A�dZA���Aӏ\A�G�A��mA�%Aɧ�Aȇ+A�?}AŋDA��HA��A�ƨA�l�A�C�A�%A�S�A���A���A�E�A�/A��A���A��;A�C�A��A�ffA��`A��A��`A���A���A�I�A��PA�hsA�z�A�ƨA�ZA�  A���A��
A{�wAx  Av��Aq��Al�\Ae\)A_O�AY��ASAE�#AB��A;��A;O�A8�9A3C�A.�A+t�A&-A ��A�A��AA�H@��u@� �@ޗ�@��`@�&�@��7@���@���@���@�1'@��H@�(�@�O�@�V@�{@��@�(�@�@�S�@�V@��/@�o@��9@{"�@j-@_|�@Q�#@L9X@E�@<�D@5O�@/�@+"�@&V@"�H@��@�y@z�@��@�+@33@\)@`B1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�VB�DB�B{B(�B#�B!�B{B�B�B�B,B,B-B/B/B-B0!B1'B/B/B/B/B+B+B)�B!�B�B	7B��B�fB��BĜB�RB��B��B��B�hB�Bs�BffBM�B.B�BB  B
�mB
��B
�B
�\B
z�B
q�B
W
B
;dB
�B	��B	�#B	�FB	x�B	gmB	K�B	G�B	9XB	#�B	oB	%B�B�BŢB�-B��B�DB� B|�B~�B�bB��B�wB��B�B	B	�B	%�B	?}B	VB	bNB	p�B	z�B	�DB	��B	��B	�'B	ƨB	��B	�B	�/B	��B
+B

=B
oB
�B
(�B
0!B
9XB
=qB
B�B
G�B
N�B
XB
[#B
`BB
dZB
gmB
k�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B�VB�DB�B�B+B'�B#�B�B�B�B!�B,B-B/B0!B0!B/B1'B2-B/B0!B0!B/B.B/B,B%�B�BJB��B�B��BƨB�dB�B��B��B�uB�Bt�BgmBO�B/B�BBB
�sB
��B
�!B
�bB
z�B
r�B
XB
=qB
�B	��B	�/B	�^B	y�B	iyB	K�B	H�B	:^B	$�B	uB	+B�B�#BǮB�3B��B�JB�B}�B� B�bB��B�wBB�B	B	�B	%�B	?}B	VB	bNB	p�B	z�B	�DB	��B	��B	�'B	ƨB	��B	�B	�/B	��B
+B

=B
oB
�B
(�B
0!B
9XB
=qB
B�B
G�B
N�B
XB
[#B
`BB
dZB
gmB
k�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200708251439532007082514395320070825143953200708251445302007082514453020070825144530200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070812190403  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070812190405  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070812190406  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070812190410  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070812190410  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070812190410  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070812191135                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070816161107  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070816161110  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070816161111  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070816161115  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070816161116  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070816161116  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070816161759                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070816161107  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423045247  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423045248  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423045248  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423045249  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423045249  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423045249  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423045249  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423045249  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423045640                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070816055438  CV  DAT$            G�O�G�O�F�df                JM  ARCAJMQC1.0                                                                 20070825143953  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070825143953  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070825144530  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021623  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021652                      G�O�G�O�G�O�                