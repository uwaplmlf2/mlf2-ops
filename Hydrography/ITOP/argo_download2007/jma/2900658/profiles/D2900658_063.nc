CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900658 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               ?A   JA  20070913014245  20081024070830  A9_67028_063                    2C  D   APEX-SBE 2976                                                   846 @Ԕk�o1   @Ԕm��@=z�G�@b"n��O�1   ARGOS   A   A   A   @�33A  A���A�ffB��BE��Bn  B�ffB�  B���B���B�ffB�ffCffCL�C�C�3C)33C3ffC=� CGL�C[ffCo�C�s3C�� C��3C���C�� C���C���CǦfCѦfC�s3C��CC�Y�D�3D�3D��D��D��D�3D�3D$ٚD)� D.�fD3� D8�3D=��DB� DG�3DN�DTL�DZs3D`��Dg  DmFfDs��DyٚD�  D�i�D��3D��fD�,�D�l�D��3D�ffD�� D�i�D�ٚD�` D��111111111111111111111111111111111111111111111111111111111111111111111111@�33A  A���A�ffB��BE��Bn  B�ffB�  B���B���B�ffB�ffCffCL�C�C�3C)33C3ffC=� CGL�C[ffCo�C�s3C�� C��3C���C�� C���C���CǦfCѦfC�s3C��CC�Y�D�3D�3D��D��D��D�3D�3D$ٚD)� D.�fD3� D8�3D=��DB� DG�3DN�DTL�DZs3D`��Dg  DmFfDs��DyٚD�  D�i�D��3D��fD�,�D�l�D��3D�ffD�� D�i�D�ٚD�` D��111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�  A�A�%A�I�A�/A�~�A���A��+A���A�`BA���A���A�bNA��;A�?}A�I�A��hA��A�Q�A��wA�33A�v�A���A�ĜA���A�A���A�K�A���A�9XA���Az��As�AoXAiC�Aa�mAW��ANA�ABjA6��A,(�A ��A�AA
=@��H@�x�@�o@���@���@�5?@��@�I�@�A�@�E�@�n�@��@���@�v�@tI�@f�R@]p�@V��@L1@Ax�@5�-@%�@dZ@�-@�T@�u@ƨ111111111111111111111111111111111111111111111111111111111111111111111111A�  A�A�%A�I�A�/A�~�A���A��+A���A�`BA���A���A�bNA��;A�?}A�I�A��hA��A�Q�A��wA�33A�v�A���A�ĜA���A�A���A�K�A���A�9XA���Az��As�AoXAiC�Aa�mAW��ANA�ABjA6��A,(�A ��A�AA
=@��H@�x�@�o@���@���@�5?@��@�I�@�A�@�E�@�n�@��@���@�v�@tI�@f�R@]p�@V��@L1@Ax�@5�-@%�@dZ@�-@�T@�u@ƨ111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
ȴB
ɺB
ȴB
�qB
�?B
��B
�B	7B(�B�B�B)�B1'B49B49B6FB6FB/B,B)�B'�B&�B"�B�B�BuBDB
��B
�B
�`B
�3B
�uB
w�B
gmB
H�B
%�B	��B	ǮB	�{B	cTB	.B��B��B�'B~�BW
B<jB5?B6FB9XB@�BT�Be`Bl�B��B�FB�B�TB��B	&�B	H�B	cTB	w�B	�hB	�B	��B	��B
 �B
)�B
<jB
M�B
^5111111111111111111111111111111111111111111111111111111111111111111111111B
ȴB
ɺB
ɺB
�}B
�dB
��B
�RB
=B)�B�B�B+B2-B5?B49B6FB7LB/B,B)�B'�B&�B"�B�B�B{BJB
��B
�B
�fB
�9B
�{B
x�B
hsB
I�B
&�B	��B	ɺB	��B	dZB	0!B��B��B�3B�BYB=qB6FB7LB9XBA�BVBe`Bm�B��B�FB�B�TB��B	&�B	H�B	cTB	w�B	�hB	�B	��B	��B
 �B
)�B
<jB
M�B
^5111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                  None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                           None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            ThisCycleSSP=0.0, NextCycleSSP=0.0                                                                                                                                                                                                                              None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : max(CTM adjustment , 0.01(PSS-78))                                                                                                                                                                   None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709210237212007092102372120070921023721200709210253132007092102531320070921025313200810150000002008101500000020081015000000  JA  ARFMdecpA9_b                                                                20070913014242  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070913014245  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070913014246  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070913014250  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070913014250  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070913014250  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070913015300                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070917042815  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070917042818  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070917042819  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070917042823  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070917042823  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070917042823  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070917042823  CV  TIME            G�O�G�O�F��l                JA  ARGQrelo2.1                                                                 20070917042823  CV  LAT$            G�O�G�O�A��                JA  ARGQrelo2.1                                                                 20070917042823  CV  LON$            G�O�G�O�Cu                JA  ARUP                                                                        20070917044126                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828073218  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828073218  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828073222  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828073222  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828073223  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828073223  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828073223  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828091430                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070919132621  CV  DAT$            G�O�G�O�F��l                JM  ARCAJMQC1.0                                                                 20070921023721  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070921023721  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070921025313  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20081015000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081024051735  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081024070830                      G�O�G�O�G�O�                