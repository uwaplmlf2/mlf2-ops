CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900658 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               CA   JA  20071003013708  20081024071051  A9_67028_067                    2C  D   APEX-SBE 2976                                                   846 @ԙk����1   @ԙm5y�@=�$�/@a�A�7K�1   ARGOS   A   A   A   @���A  A�  A�  B��BF  BlffB�33B���B�ffB���B���B�33C� C� C33CL�C)L�C3� C=�CG� C[33CoffC��3C��fC�s3C�ffC��3C���C���CǙ�Cѳ3C�s3C�Y�C�fC�s3D�3D�fD��D��D� D��D��D$�fD)��D.� D3�fD8�fD=��DB��DG�3DN3DT` DZy�D`�3Dg�DmY�Ds��Dy�fD�,�D�l�D���D�� D�)�D�l�D���D�p D�� D�ffD�� D�ffD�3111111111111111111111111111111111111111111111111111111111111111111111111@���A  A�  A�  B��BF  BlffB�33B���B�ffB���B���B�33C� C� C33CL�C)L�C3� C=�CG� C[33CoffC��3C��fC�s3C�ffC��3C���C���CǙ�Cѳ3C�s3C�Y�C�fC�s3D�3D�fD��D��D� D��D��D$�fD)��D.� D3�fD8�fD=��DB��DG�3DN3DT` DZy�D`�3Dg�DmY�Ds��Dy�fD�,�D�l�D���D�� D�)�D�l�D���D�p D�� D�ffD�� D�ffD�3111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A㝲AۅAٰ!Aى7A؉7AǺ^A���A���A�1A���A��A� �A���A�x�A��+A��yA�+A��mA���A�ZA��A���A��A��A��^A���A�A���A���A�r�A�=qA�ĜA���A~9XAzJAxbAw��Aw/AvQ�Ak�PAY��AJ��AAK�A4�A($�A�9A�#AC�@��@��;@��@���@��@�33@�C�@�hs@�{@�7L@���@��@w\)@j=q@b��@W�P@M�-@:�@-?}@"J@-@�@
~�@�m111111111111111111111111111111111111111111111111111111111111111111111111A㝲AۅAٰ!Aى7A؉7AǺ^A���A���A�1A���A��A� �A���A�x�A��+A��yA�+A��mA���A�ZA��A���A��A��A��^A���A�A���A���A�r�A�=qA�ĜA���A~9XAzJAxbAw��Aw/AvQ�Ak�PAY��AJ��AAK�A4�A($�A�9A�#AC�@��@��;@��@���@��@�33@�C�@�hs@�{@�7L@���@��@w\)@j=q@b��@W�P@M�-@:�@-?}@"J@-@�@
~�@�m111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
:^B
�LB
��B
ƨB
ĜB
ŢB
�fB�B-B5?B8RB7LB49B/B+B'�B$�B#�B"�B�B�B �B�B�BoB1BB%B
=BDBDBB
�TB
��B
ƨB
��B
��B
��B
ĜB
iyB
B	�XB	�=B	R�B	{B�#B��B� BgmBS�BE�B7LB9XBJ�BdZB}�B��B�^B��B��B	�B	D�B	XB	v�B	�hB	��B	�B
+B
�B
7LB
L�B
^5111111111111111111111111111111111111111111111111111111111111111111111111B
A�B
�RB
��B
ƨB
��B
��B
�B �B/B6FB9XB8RB5?B/B+B'�B$�B#�B"�B�B�B �B�B�BoB	7BB%B
=BDBDB%B
�ZB
�B
ƨB
��B
��B
��B
ŢB
k�B
B	�^B	�JB	T�B	�B�/B��B�BhsBT�BF�B8RB:^BK�BdZB~�B��B�^B��B��B	�B	D�B	XB	v�B	�hB	��B	�B
+B
�B
7LB
L�B
^5111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                  None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                           None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            ThisCycleSSP=0.0, NextCycleSSP=0.0                                                                                                                                                                                                                              None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : max(CTM adjustment , 0.01(PSS-78))                                                                                                                                                                   None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710110204122007101102041220071011020412200710110217312007101102173120071011021731200810150000002008101500000020081015000000  JA  ARFMdecpA9_b                                                                20071003013704  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071003013708  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071003013709  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071003013713  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071003013714  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071003013714  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071003014456                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071007055801  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071007055814  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071007055818  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071007055825  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071007055825  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071007055826  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071007064023                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828073239  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828073239  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828073243  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828073243  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828073243  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828073243  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828073244  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828091604                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071011020412  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071011020412  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071011021731  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20081015000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081024051737  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081024071051                      G�O�G�O�G�O�                