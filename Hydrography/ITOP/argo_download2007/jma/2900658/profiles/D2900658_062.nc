CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900658 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               >A   JA  20070908013343  20081024070756  A9_67028_062                    2C  D   APEX-SBE 2976                                                   846 @ԓ'�!_�1   @ԓ/�rx@=p��
=@b9x���1   ARGOS   A   A   A   @�  A33A���A陚B  BE33Bm��B���B�  B�33Bƙ�B�  B���C33C  CffCL�C)33C3ffC=ffCGL�C[�CoffC���C�� C�ffC��fC��3C�� C�ffC�L�C�L�C�� C��C��C�� D� D�fD�3D��D�fD� D�3D$��D)�3D.� D3� D8�fD=��DB� DG�3DN�DTY�DZ��D`�3Df�fDm@ Ds� Dy��D�,�D�ffD�� D�� D�&fD�p D��fD�i�D�� D�i�D��D�l�D���D�31111111111111111111111111111111111111111111111111111111111111111111111111   @�  A33A���A陚B  BE33Bm��B���B�  B�33Bƙ�B�  B���C33C  CffCL�C)33C3ffC=ffCGL�C[�CoffC���C�� C�ffC��fC��3C�� C�ffC�L�C�L�C�� C��C��C�� D� D�fD�3D��D�fD� D�3D$��D)�3D.� D3� D8�fD=��DB� DG�3DN�DTY�DZ��D`�3Df�fDm@ Ds� Dy��D�,�D�ffD�� D�� D�&fD�p D��fD�i�D�� D�i�D��D�l�D���D�31111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���Aէ�A��A���AҼjA�~�A���A�`BA���A��A��A�-A���A�VA��7A�`BA��7A��PA�ffA���A��^A���A��A�dZA���A��A�|�A� �A�A���A�33A~�jAu�-Ak��Ab��A\ZAU/AP�`AH^5A@5?A0�/A&  A�RAJA/@�"�@�M�@�t�@�bN@�S�@��u@���@�&�@�hs@��@�Ĝ@��@�1@�"�@}��@st�@ep�@Wl�@K��@Ax�@8��@*-@V@@�+@K�@ �9@ Ĝ1111111111111111111111111111111111111111111111111111111111111111111111111   A���Aէ�A��A���AҼjA�~�A���A�`BA���A��A��A�-A���A�VA��7A�`BA��7A��PA�ffA���A��^A���A��A�dZA���A��A�|�A� �A�A���A�33A~�jAu�-Ak��Ab��A\ZAU/AP�`AH^5A@5?A0�/A&  A�RAJA/@�"�@�M�@�t�@�bN@�S�@��u@���@�&�@�hs@��@�Ĝ@��@�1@�"�@}��@st�@ep�@Wl�@K��@Ax�@8��@*-@V@@�+@K�@ �9@ Ĝ1111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
��B
��B
��B
��B
�=B
{�B
w�B
�BbB�B+B,B5?B6FB5?B9XB49B5?B2-B/B+B$�B�B �B�B�B�BPB  B
�ZB
��B
��B
�B
F�B
"�B
+B	�yB	��B	�B	�%B	B�B	B�B�qB��BK�B33BL�B8RB��B7LBN�BbNB|�B�B�VB��BƨB�BB	DB	(�B	O�B	o�B	�uB	�B	��B	�B

=B
�B
:^B
R�B
e`B
e`1111111111111111111111111111111111111111111111111111111111111111111111111   B
��B
��B
��B
��B
�VB
�B
{�B
�5BhB�B+B-B6FB7LB6FB:^B49B6FB2-B/B+B$�B�B �B�B�B�BVBB
�`B
��B
��B
�B
G�B
#�B
1B	�B	��B	�B	�1B	C�B	B�B�wB��BL�B49BM�B:^B��B8RBN�BcTB|�B�B�VB��BƨB�BB	DB	(�B	O�B	o�B	�uB	�B	��B	�B

=B
�B
:^B
R�B
e`B
e`1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                  None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                           None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            ThisCycleSSP=0.0, NextCycleSSP=0.0                                                                                                                                                                                                                              None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : max(CTM adjustment , 0.01(PSS-78))                                                                                                                                                                   None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709190426262007091904262620070919042626200709190432102007091904321020070919043210200810150000002008101500000020081015000000  JA  ARFMdecpA9_b                                                                20070908013340  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070908013343  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070908013344  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070908013348  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070908013348  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070908013348  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070908013949                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070912043938  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070912043942  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070912043943  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070912043947  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070912043947  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070912043947  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070912050057                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828073213  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828073213  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828073217  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828073217  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828073217  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828073217  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828073218  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828091403                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070911114155  CV  DAT$            G�O�G�O�F��~                JM  ARGQJMQC1.0                                                                 20070911114155  CV  LAT$            G�O�G�O�A�R                JM  ARGQJMQC1.0                                                                 20070911114155  CV  LON$            G�O�G�O�C��                JM  ARCAJMQC1.0                                                                 20070919042626  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070919042626  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070919043210  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20081015000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081024051734  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081024070756                      G�O�G�O�G�O�                