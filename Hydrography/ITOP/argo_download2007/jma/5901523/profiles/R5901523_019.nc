CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901523 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070828012923  20090519073008                                  2B  A   APEX-SBE 2808                                                   846 @Ԑ_k��1   @Ԑ_��Q�@44z�G�@aA�����1   ARGOS   A   A   A   @���A��Ad��A���A�33A���B33B  B1��BF  B[33Bn  B���B�33B���B�33B���B�33B���B�33B���B���B�ffBB���C� C  C� C�3CffCffC� C$ffC)� C.� C3�C8L�C=33CB33CGL�CP�fC[� Ce��CoffCy  C��3C���C�� C���C�� C��fC�� C�s3C���C�� C�� C���C���C¦fC�s3C̳3C�s3Cր Cۀ C�fC噚C�3C�fC�� C�� D��DٚD��DٚD��DٚDٚD$� D)��D.ٚD3��D8��D=� DBٚDG��DL� DQ�3DV��D[ٚD`�fDeٚDj�fDoٚDt�3DyٚD�)�D�ffD��fD��fD�)�D�ffD�� D�� D�&fD�l�D���D���D�  D�p Dڰ D��D�)�D�\�D��D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��A\��A���A�33A���B	33B  B/��BD  BY33Bl  B34B�33B���B�33B���B�33B���B�33B���B���B�ffB홚B���C  C� C  C33C�fC�fC  C#�fC)  C.  C2��C7��C<�3CA�3CF��CPffC[  Ce�Cn�fCx� C�s3C�Y�C�� C�Y�C�� C�ffC�� C�33C���C�� C�� C�L�C�Y�C�ffC�33C�s3C�33C�@ C�@ C�ffC�Y�C�s3C�ffC� C�� D��D��D��D��D��D��D��D$� D)��D.��D3��D8��D=� DB��DG��DL� DQ�3DV��D[��D`�fDe��Dj�fDo��Dt�3Dy��D��D�VfD��fD��fD��D�VfD�� D�� D�fD�\�D���D���D� D�` Dڠ D�ٚD��D�L�D��D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�\A�PA��A�p�A�bNA�S�A�9A韾A�hsA��A�;dA��A�p�A�/A�VA���A���A���A�(�A��TA��A�/A�&�A�bA�t�A���A��A��9A��!A��jA���A�VA��A��HA��uA�K�A�C�A��uA���A���A��#A�1A�\)A�p�A��wA�$�A�dZA���A�%A�^5A| �Avz�Aq�Ak7LAg"�AaVA[%AV=qAR�yAP�jAG�PADffA@�DA9��A4ffA0ZA.{A(^5A"�A�#A�;@��7@��@���@���@�%@�^5@���@�E�@�O�@�p�@�I�@���@�@�r�@�33@��@��@��@�z�@xr�@u/@pb@i��@e��@_\)@V$�@Nȴ@C33@;��@5��@0��@,��@)X@#��@ ��@j@M�@
=@o@��@�m@�@�D@dZ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�\A�PA��A�p�A�bNA�S�A�9A韾A�hsA��A�;dA��A�p�A�/A�VA���A���A���A�(�A��TA��A�/A�&�A�bA�t�A���A��A��9A��!A��jA���A�VA��A��HA��uA�K�A�C�A��uA���A���A��#A�1A�\)A�p�A��wA�$�A�dZA���A�%A�^5A| �Avz�Aq�Ak7LAg"�AaVA[%AV=qAR�yAP�jAG�PADffA@�DA9��A4ffA0ZA.{A(^5A"�A�#A�;@��7@��@���@���@�%@�^5@���@�E�@�O�@�p�@�I�@���@�@�r�@�33@��@��@��@�z�@xr�@u/@pb@i��@e��@_\)@V$�@Nȴ@C33@;��@5��@0��@,��@)X@#��@ ��@j@M�@
=@o@��@�m@�@�D@dZ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
_;B
]/B
YB
T�B
VB
\)B
��B8RB49BuB
=BhBaHB�B��B��B��B�'B��B�B�!B�9B�3B�wB�^B�9B�B��B��B�\B� Be`B^5Bq�Bn�BP�B\)BhsBffB_;BM�B>wB-B%�B\B
��B
�B
�/B
��B
�FB
�{B
v�B
XB
:^B
$�B
%B	�B	��B	ŢB	�9B	�+B	v�B	cTB	E�B	/B	�B	{B��B�sB��B�By�B� B~�B�B�VB��B��B�3BȴB�fB�B��B	PB	�B	49B	B�B	M�B	ZB	cTB	r�B	�B	�hB	��B	��B	ÖB	��B	��B
+B
oB
�B
&�B
0!B
5?B
?}B
E�B
N�B
S�B
YB
`BB
hsB
l�B
r�B
v�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
_;B
]/B
YB
T�B
VB
\)B
��B8RB49BuB
=BhBaHB�B��B��B��B�'B��B�B�!B�9B�3B�wB�^B�9B�B��B��B�\B� Be`B^5Bq�Bn�BP�B\)BhsBffB_;BM�B>wB-B%�B\B
��B
�B
�/B
��B
�FB
�{B
v�B
XB
:^B
$�B
%B	�B	��B	ŢB	�9B	�+B	v�B	cTB	E�B	/B	�B	{B��B�sB��B�By�B� B~�B�B�VB��B��B�3BȴB�fB�B��B	PB	�B	49B	B�B	M�B	ZB	cTB	r�B	�B	�hB	��B	��B	ÖB	��B	��B
+B
oB
�B
&�B
0!B
5?B
?}B
E�B
N�B
S�B
YB
`BB
hsB
l�B
r�B
v�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070828012918  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070828012923  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070828012924  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070828012928  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070828012928  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070828012928  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070828013609                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070831155456  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070831155500  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070831155500  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070831155504  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070831155505  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070831155505  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070831160050                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070831155456  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519072623  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519072623  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519072624  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519072625  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519072625  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519072625  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519072625  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519072625  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519073008                      G�O�G�O�G�O�                