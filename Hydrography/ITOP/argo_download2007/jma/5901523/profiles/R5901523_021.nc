CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5901523 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070917013558  20090519073007                                  2B  A   APEX-SBE 2808                                                   846 @ԕUEr�1   @ԕUx9��@3�hr� �@aOƧ1   ARGOS   A   A   A   @�  A  Ai��A�  A���A�  B33B  B2  BE��BY��BnffB�ffB�ffB�  B�ffB�ffB�ffB�  B�  B�33B�  B�  B�33B�  CffCffCffCL�C33C  C� C$  C(�fC-��C3L�C833C=33CBL�CGL�CP�fC[��CeffCo33Cy�C���C���C���C�� C��3C���C���C���C��3C��3C���C��3C�� C���C�� C̦fCѳ3C֦fC���C���C�fCꙚC�3C��3C���D�fD��D�fD��D�fD��D� D$��D)� D.� D3� D8ٚD=� DB��DG� DLٚDQ��DV� D[�3D`��De�fDj��Do��Dt�fDyٚD�&fD�i�D���D���D�  D�ffD�� D��fD�  D�VfD��3D�� D�,�D�c3Dڠ D��3D��D�c3D�3D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A  Aa��A�  A���A�  B	33B  B0  BC��BW��BlffB~��B�ffB�  B�ffB�ffB�ffB�  B�  B�33B�  B�  B�33B�  C �fC�fC
�fC��C�3C� C  C#� C(ffC-L�C2��C7�3C<�3CA��CF��CPffC[�Cd�fCn�3Cx��C�L�C�Y�C�L�C�@ C�s3C�L�C�L�C�Y�C�s3C�s3C�L�C�s3C�� C�Cǀ C�ffC�s3C�ffCی�C�L�C�ffC�Y�C�s3C�s3C�L�D�fD��D�fD��D�fD��D� D$��D)� D.� D3� D8��D=� DB��DG� DL��DQ��DV� D[�3D`��De�fDj��Do��Dt�fDy��D�fD�Y�D���D���D� D�VfD�� D��fD� D�FfD��3D�� D��D�S3Dڐ D��3D��D�S3D�3D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��HA��A��yA��A���A�wA陚A�I�A�ĜAպ^Aҙ�A�I�A�jA�=qA�|�A���A��PA���A�x�A�A�A��^A��A��A�
=A�%A��jA�1A�O�A���A�{A�$�A���A��A���A��\A��mA��mA��-A�JA�VA���A���A�%A���A�?}A�JA��HA~^5A|VAy��At9XAl�Af��Ae�A^ȴAX1'AP��AH�HA@5?A;+A3�^A,{A%��A"-A��A�A��AjA  A/A�`@��@��@�S�@��
@�K�@��!@�x�@�t�@�M�@��@���@��!@��@���@�~�@�1@�O�@��T@�`B@���@~V@x��@t��@o\)@_K�@V$�@N$�@CC�@<��@7��@2J@-�T@*^5@&ff@!%@I�@;d@�
@��@��@�@�@~�?�v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��HA��A��yA��A���A�wA陚A�I�A�ĜAպ^Aҙ�A�I�A�jA�=qA�|�A���A��PA���A�x�A�A�A��^A��A��A�
=A�%A��jA�1A�O�A���A�{A�$�A���A��A���A��\A��mA��mA��-A�JA�VA���A���A�%A���A�?}A�JA��HA~^5A|VAy��At9XAl�Af��Ae�A^ȴAX1'AP��AH�HA@5?A;+A3�^A,{A%��A"-A��A�A��AjA  A/A�`@��@��@�S�@��
@�K�@��!@�x�@�t�@�M�@��@���@��!@��@���@�~�@�1@�O�@��T@�`B@���@~V@x��@t��@o\)@_K�@V$�@N$�@CC�@<��@7��@2J@-�T@*^5@&ff@!%@I�@;d@�
@��@��@�@�@~�?�v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�DB
�=B
�JB
�DB
�VB
�oB
�B/B�B�B�fB�B��B�BB��B�B�B�BhBhB	7BB��B�mB�B��B�VB�Bw�Be`BZBS�BN�BL�BG�B>wB5?B+B%�B�BVB
��B
�fB
�B
�wB
�'B
��B
�uB
�B
gmB
C�B
$�B
�B	��B	��B	�B	�B	^5B	H�B	1'B	B�B�TB��BƨB��B�jB�3B�B�bBt�Bt�Bp�Bm�Br�B�VB��B��B��B�`B	B	�B	$�B	0!B	@�B	Q�B	bNB	�%B	��B	��B	�XB	��B	ǮB	��B	�ZB	�B	��B
\B
 �B
/B
9XB
A�B
K�B
P�B
YB
`BB
e`B
iyB
k�B
n�B
v�B
x�B
|�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�DB
�=B
�JB
�DB
�VB
�oB
�B/B�B�B�fB�B��B�BB��B�B�B�BhBhB	7BB��B�mB�B��B�VB�Bw�Be`BZBS�BN�BL�BG�B>wB5?B+B%�B�BVB
��B
�fB
�B
�wB
�'B
��B
�uB
�B
gmB
C�B
$�B
�B	��B	��B	�B	�B	^5B	H�B	1'B	B�B�TB��BƨB��B�jB�3B�B�bBt�Bt�Bp�Bm�Br�B�VB��B��B��B�`B	B	�B	$�B	0!B	@�B	Q�B	bNB	�%B	��B	��B	�XB	��B	ǮB	��B	�ZB	�B	��B
\B
 �B
/B
9XB
A�B
K�B
P�B
YB
`BB
e`B
iyB
k�B
n�B
v�B
x�B
|�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070917013556  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070917013558  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070917013559  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070917013604  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070917013604  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070917013604  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070917014315                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070920155601  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070920155605  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070920155606  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070920155610  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070920155610  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070920155610  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070920160145                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070920155601  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519072628  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519072628  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519072629  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519072630  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519072630  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519072630  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519072630  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519072630  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519073007                      G�O�G�O�G�O�                