CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900686 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070806130048  20090416071340                                  2B  A   APEX-SBE 2971                                                   846 @ԋ���S1   @ԋ�X^@9����F@aÅ�Q�1   ARGOS   A   A   A   @�33A  Ai��A�  A�  A�33B
��B��B2ffBF��BZ  Bm��B���B���B�33B�ffB�ffB�ffB�ffBƙ�B���B�  B���B�ffB���C33CffCL�CffC�C��CL�C$L�C)33C.33C3L�C8L�C=L�CBffCF�3CQ� C[33CeffCoffCy� C�� C���C���C���C��3C���C�ffC��fC��3C�ffC�ffC���C��fC���Cǳ3C̳3Cѳ3Cր C�ffC�� C�Y�CꙚC�fC��C��3D�3D��D��D�fD��D��D��D$�3D)� D.ٚD3� D8�3D=ٚDB��DGٚDL�fDQ� DVٚD[� D`ٚDe� Dj��DoٚDt�3Dy�3D�&fD�l�D�� D�ٚD�&fD�l�D���D��3D�#3D�i�D�� D��fD�33D�i�Dڙ�D��fD�  D�ffD� D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33AT��A���A���A���B��B��B-33BA��BT��BhfgB| B�  B���B���B���B���B���B�  B�33B�ffB�33B���B�33B���C�C
  C�C��C� C  C#  C'�fC,�fC2  C7  C<  CA�CEffCP33CY�fCd�Cn�Cx33C��C��4C��4C��gC��C��gC�� C�  C��C�� C�� C��4C�  C�&gC��C��C��C�ٚC�� C�ٚC�4C��4C�  C��gC��D� Dy�Dy�Ds3DfgDY�Dy�D$� D)��D.�gD3l�D8� D=�gDBy�DG�gDLs3DQ��DV�gD[l�D`�gDe��Djy�Do�gDt� Dy� D���D�C3D�vfD�� D���D�C3D�� D���D���D�@ D�vfDǼ�D�	�D�@ D�p D��D��fD�<�D�vfD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�DA�jA���Aң�A��A���Aɕ�AǬA�1'A�
=A�ȴA���A���A�?}A�%A��A���A�dZA�v�A��
A���A��
A�7LA��A�jA���A�XA�ĜA���A�t�A��#A��A�A�A�+A��FA� �A��A�(�A�hsA�JA�1'A�;dA�VA��FA���A���A�hsA�  A�C�A��9A�~�A�ȴA�A�A��HA�K�A�XA~VA|�/Au�#Aq��Ajz�Ac�A`ZA[��AX�AV��AO
=AL��AH�AC�A9O�A/XA,5?A1Ax�A��A��@�\)@���@�dZ@ź^@��R@��
@�E�@�o@���@�  @��@� �@~@v@sƨ@pbN@m/@j�@\�@P��@Gl�@<�j@4�@.ff@( �@#o@`B@-@��@�@dZ@�@��@ƨ@&�?�"�?��
?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�DA�jA���Aң�A��A���Aɕ�AǬA�1'A�
=A�ȴA���A���A�?}A�%A��A���A�dZA�v�A��
A���A��
A�7LA��A�jA���A�XA�ĜA���A�t�A��#A��A�A�A�+A��FA� �A��A�(�A�hsA�JA�1'A�;dA�VA��FA���A���A�hsA�  A�C�A��9A�~�A�ȴA�A�A��HA�K�A�XA~VA|�/Au�#Aq��Ajz�Ac�A`ZA[��AX�AV��AO
=AL��AH�AC�A9O�A/XA,5?A1Ax�A��A��@�\)@���@�dZ@ź^@��R@��
@�E�@�o@���@�  @��@� �@~@v@sƨ@pbN@m/@j�@\�@P��@Gl�@<�j@4�@.ff@( �@#o@`B@-@��@�@dZ@�@��@ƨ@&�?�"�?��
?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
o�B
^5B
l�B
�B
�oB
��B
�B
��B
�sB
��BJBJBbB�B#�B/B-B9XB<jB;dB;dB;dB:^B9XB;dBB�B@�B=qB9XB6FB33B1'B0!B/B.B(�B%�B$�B#�B#�B&�B'�B%�B$�B �B�B�BoBB
��B
�B
�TB
��B
ƨB
�wB
�B
��B
��B
r�B
^5B
:^B
�B
	7B	��B	�fB	�/B	�jB	�B	��B	�VB	dZB	6FB	'�B�BȴB�3B��B�+Bv�Bu�Br�Bx�B{�B�hB��B�!BǮB�BB��B	�B	,B	33B	;dB	B�B	I�B	iyB	�=B	��B	�jB	��B	�BB	�B	��B
JB
�B
'�B
0!B
<jB
G�B
L�B
T�B
\)B
bNB
jB
l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
o�B
^5B
l�B
�B
�oB
��B
�B
��B
�sB
��BJBJBbB�B#�B/B-B9XB<jB;dB;dB;dB:^B9XB;dBB�B@�B=qB9XB6FB33B1'B0!B/B.B(�B%�B$�B#�B#�B&�B'�B%�B$�B �B�B�BoBB
��B
�B
�TB
��B
ƨB
�wB
�B
��B
��B
r�B
^5B
:^B
�B
	7B	��B	�fB	�/B	�jB	�B	��B	�VB	dZB	6FB	'�B�BȴB�3B��B�+Bv�Bu�Br�Bx�B{�B�hB��B�!BǮB�BB��B	�B	,B	33B	;dB	B�B	I�B	iyB	�=B	��B	�jB	��B	�BB	�B	��B
JB
�B
'�B
0!B
<jB
G�B
L�B
T�B
\)B
bNB
jB
l�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070806130045  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070806130048  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070806130049  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070806130053  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070806130053  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070806130053  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070806130652                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070810042319  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070810042322  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070810042323  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070810042327  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070810042327  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070810042328  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070810043203                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070810042319  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416070620  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416070620  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416070621  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416070622  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416070622  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416070622  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416070622  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416070622  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416071340                      G�O�G�O�G�O�                