CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  2900686 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070816130004  20090416071350                                  2B  A   APEX-SBE 2971                                                   846 @ԍ~�B��1   @ԍ:7_2@9��hr�@a���R1   ARGOS   A   A   A   @�33A��Ah  A���A�  A홚B
  B33B2  BFffBY��Bn  B���B�ffB���B���B�  B���B���B���B�33Bٙ�B䙚B홚B�33C��CffC� C�C  C33C�fC$ffC)33C.�C3L�C8  C<��CA�3CF��CQ�C[ffCe�Cn��Cx�fC��3C��fC�� C���C��fC��fC�s3C�ffC�ffC��3C�s3C��fC��fC�� CǦfC̦fCь�Cր Cی�C���C� C���C���C��fC���D� D� D�3DٚD��D� D��D$��D)��D.ٚD3��D8�3D=�3DB�3DG�3DL� DQ� DV�fD[ٚD`� De�3Dj�3Do� DtٚDy��D�&fD�i�D�� D��3D�#3D�l�D���D��3D��D�ffD���D��fD�  D�c3Dڣ3D���D�#3D�c3D�fD��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33@���AP  A���A�  AᙚB  B33B,  B@ffBS��Bh  B{��B�ffB���B���B�  B���B���B���B�33B֙�BᙚBꙚB�33C �C�fC
  C��C� C�3CffC"�fC'�3C,��C1��C6� C;L�C@33CEL�CO��CY�fCc��CmL�CwffC��3C��fC�  C�ٚC��fC��fC��3C��fC��fC��3C��3C��fC��fC�  C��fC��fC���C�� C���C���C�� C��C��C��fC�ٚD` D� Ds3Dy�Dl�D` D��D$l�D)l�D.y�D3L�D8S3D=s3DBs3DGs3DL` DQ` DVffD[y�D`` Des3DjS3Do` Dty�Dyl�D��fD�9�D�� D��3D��3D�<�D�|�D��3D��D�6fD�y�DǶfD�� D�33D�s3D��D��3D�33D�ffD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�Q�A�\)A�dZA�l�A��A�ĜA�M�A�7LA�7LA�E�A�$�A��A�{A���A�~�A�$�A���A��+A�1A�bNA��A��DA�7LA���A��7A��hA��;A�l�A�p�A�|�A���A���A�
=A���A�A�O�A�hsA��A��A�;dA��A�?}A��A��wA���A��mA�bA�/A��A��wA�r�A��
A�v�A���A�%A{/Au�7Ap��Am|�AhM�A`=qAX-AVȴAR=qAM�AG&�A@�jA:�+A6�\A3��A,��A!�mAhsA��A	��@���@�o@�l�@�&�@��9@���@��#@�Ĝ@�hs@�o@��@��w@�p�@��h@�;@w�;@q�@n@j^5@ahs@W+@Pr�@D9X@A%@7;d@.��@,1@&ff@�@�!@{@/@n�@�@
�!@  @^5@ 1'?��m?��T1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�Q�A�\)A�dZA�l�A��A�ĜA�M�A�7LA�7LA�E�A�$�A��A�{A���A�~�A�$�A���A��+A�1A�bNA��A��DA�7LA���A��7A��hA��;A�l�A�p�A�|�A���A���A�
=A���A�A�O�A�hsA��A��A�;dA��A�?}A��A��wA���A��mA�bA�/A��A��wA�r�A��
A�v�A���A�%A{/Au�7Ap��Am|�AhM�A`=qAX-AVȴAR=qAM�AG&�A@�jA:�+A6�\A3��A,��A!�mAhsA��A	��@���@�o@�l�@�&�@��9@���@��#@�Ĝ@�hs@�o@��@��w@�p�@��h@�;@w�;@q�@n@j^5@ahs@W+@Pr�@D9X@A%@7;d@.��@,1@&ff@�@�!@{@/@n�@�@
�!@  @^5@ 1'?��m?��T1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
&�B
)�B
/B
0!B
33B
F�B
ffB
z�B
��B
��B
��B
�^B
�B{B�B"�B&�B(�B.B-B=qB>wB=qB>wB<jB=qB>wB?}B@�B?}B>wB<jB:^B8RB6FB49B5?B,B)�B'�B%�B&�B&�B'�B#�B!�B�B\B%B
��B
�B
�;B
��B
��B
��B
�\B
s�B
\)B
K�B
/B
	7B	�mB	�;B	��B	�3B	��B	}�B	dZB	T�B	F�B	)�B	B�BĜB�9B��B�+Bs�Bn�Bv�By�B� B�7B��B��B�FB��B�`B��B	\B	"�B	33B	@�B	K�B	^5B	x�B	�DB	�B	�9B	��B	�BB	�sB	��B
+B
uB
�B
"�B
+B
2-B
@�B
G�B
W
B
\)B
aHB
iy1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
&�B
)�B
/B
0!B
33B
F�B
ffB
z�B
��B
��B
��B
�^B
�B{B�B"�B&�B(�B.B-B=qB>wB=qB>wB<jB=qB>wB?}B@�B?}B>wB<jB:^B8RB6FB49B5?B,B)�B'�B%�B&�B&�B'�B#�B!�B�B\B%B
��B
�B
�;B
��B
��B
��B
�\B
s�B
\)B
K�B
/B
	7B	�mB	�;B	��B	�3B	��B	}�B	dZB	T�B	F�B	)�B	B�BĜB�9B��B�+Bs�Bn�Bv�By�B� B�7B��B��B�FB��B�`B��B	\B	"�B	33B	@�B	K�B	^5B	x�B	�DB	�B	�9B	��B	�BB	�sB	��B
+B
uB
�B
"�B
+B
2-B
@�B
G�B
W
B
\)B
aHB
iy1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070816130002  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070816130004  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070816130005  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070816130009  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070816130009  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070816130010  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070816130507                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070820042300  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070820042303  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070820042304  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070820042308  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070820042308  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070820042308  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070820043051                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070820042300  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416070622  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416070623  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416070623  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416070624  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416070624  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416070624  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416070624  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416070624  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416071350                      G�O�G�O�G�O�                