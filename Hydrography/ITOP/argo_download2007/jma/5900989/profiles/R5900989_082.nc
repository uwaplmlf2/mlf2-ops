CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   n   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4T   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6|   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :\   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J�   CALIBRATION_DATE            	             
_FillValue                  ,  M�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N    HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N`   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Np   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Nt   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5900989 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               RA   JA  20070913011420  20090423044659                                  2B  A   1575                                                            846 @Ԕc�P1   @Ԕ�r(@7^5?|�@b1&�y1   ARGOS   A   A   A   @���A33AfffA�33A���A�33B
ffB33B1��BF  BZffBlffB���B�ffB���B�ffB�33B���B�33B�ffBЙ�B�33B噚B���B�  C33C� C$L�C)��C.33C333C8� C=� CB�CG33CQffC[ffCe�Co��CyffC���C��3C���C�� C�� C�� C��3C��fC���C��3C��fC��fC��fC³3Cǳ3C̳3Cљ�C֌�C�s3C�Y�C�fC�L�C�ffC�s3C�ffD��D�3D��D��D�fD� D�3D$��D)� D.��D3��D8�3D=�fDB�3DGٚDL��DQ� DVٚD[��D`ٚDeٚDjٚDo��Dt�fDy� D�&fD�l�D�� D��fD��D�i�D��3D���D��D�i�D���D��D�,�D�i�Dڰ D���D��D�c3D�3D�,�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�34A  Ac33A���A�  A陙B	��BffB0��BE33BY��Bk��B�fgB�  B�fgB�  B���B�fgB���B�  B�34B���B�34B�fgB���C  CL�C$�C)fgC.  C3  C8L�C=L�CA�gCG  CQ33C[33Cd�gCofgCy33C�� C���C�� C��fC�ffC�ffC���C���C�s3C���C���C���C���C�CǙ�C̙�Cр C�s3C�Y�C�@ C��C�33C�L�C�Y�C�L�D� D�fD� D��D��D�3D�fD$� D)�3D.� D3��D8�fD=��DB�fDG��DL� DQ�3DV��D[� D`��De��Dj��Do� Dt��Dy�3D�  D�fgD���D�� D�4D�c4D���D��gD�gD�c4D��gD��4D�&gD�c4Dک�D��gD�gD�\�D��D�&g11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�-A�1'A�?}A�C�A�C�A�K�A�O�A�M�A�`BA왚A� �A� �A�ƨAذ!A�^5A�;dA�oA�A�A��
A�ffAȶFA�9XA�G�A�33A�G�A�K�A���A���A��A�$�A�A�
=A��A��+A�%A�~�A�Q�A�"�A�%A��hA��HA�|�A�r�A�ĜA��A���A� �A�A�\)A���A���A}��AxbAs��Ap��Af�/Ab��A\�AU�AP�/AKS�AF�RAE�hAA�^A?;dA3�7A$�A M�AjA-A
=A�\@��j@��^@�O�@�9X@ˮ@�n�@��D@�5?@�~�@���@�O�@�ff@���@�E�@��@���@���@��u@��@�G�@u�-@j�!@a�@So@KS�@BM�@8Q�@0�`@)&�@"-@�D@�P@9X@��@�y@
^5@5?@t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�-A�1'A�?}A�C�A�C�A�K�A�O�A�M�A�`BA왚A� �A� �A�ƨAذ!A�^5A�;dA�oA�A�A��
A�ffAȶFA�9XA�G�A�33A�G�A�K�A���A���A��A�$�A�A�
=A��A��+A�%A�~�A�Q�A�"�A�%A��hA��HA�|�A�r�A�ĜA��A���A� �A�A�\)A���A���A}��AxbAs��Ap��Af�/Ab��A\�AU�AP�/AKS�AF�RAE�hAA�^A?;dA3�7A$�A M�AjA-A
=A�\@��j@��^@�O�@�9X@ˮ@�n�@��D@�5?@�~�@���@�O�@�ff@���@�E�@��@���@���@��u@��@�G�@u�-@j�!@a�@So@KS�@BM�@8Q�@0�`@)&�@"-@�D@�P@9X@��@�y@
^5@5?@t�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�BȴBȴBǮBȴBȴBȴB��BȴB��B�B)�B1'BA�B<jBO�BVBW
BZB\)B^5B^5B]/B[#BXBS�B(�B1'B/B�B�BoB%B��B�B�sB��B�B��B� Bn�B\)BL�B>wB1'B'�B�B%B
��B
�5B
�B
B
��B
�=B
q�B
]/B
+B
{B	�B	��B	�!B	��B	�B	}�B	l�B	`BB	.B��B�sB��B��B�-B��B�DB�B�B�B�%B�\B��B�dB�B�B	B	)�B	B�B	XB	jB	w�B	�=B	��B	�wB	�)B	�B
B
+B
1B
JB
�B
&�B
,B
8RB
C�B
K�B
S�B
[#B
cTB
gmB
o�B
v�B
z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  BȴBȴBǮBȴBȴBȴB��BȴB��B�B)�B1'BA�B<jBO�BVBW
BZB\)B^5B^5B]/B[#BXBS�B(�B1'B/B�B�BoB%B��B�B�sB��B�B��B� Bn�B\)BL�B>wB1'B'�B�B%B
��B
�5B
�B
B
��B
�=B
q�B
]/B
+B
{B	�B	��B	�!B	��B	�B	}�B	l�B	`BB	.B��B�sB��B��B�-B��B�DB�B�B�B�%B�\B��B�dB�B�B	B	)�B	B�B	XB	jB	w�B	�=B	��B	�wB	�)B	�B
B
+B
1B
JB
�B
&�B
,B
8RB
C�B
K�B
S�B
[#B
cTB
gmB
o�B
v�B
z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20070913011403  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070913011420  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070913011422  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070913011425  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070913011433  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070913011433  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070913011433  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070913011433  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070913011434  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070913011434  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070913015247                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070915160335  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070915160349  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070915160351  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070915160353  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070915160401  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070915160401  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070915160401  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070915160401  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070915160403  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070915160403  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070915165123                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070915160335  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423044022  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423044022  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090423044023  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423044023  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423044024  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423044024  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423044024  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423044024  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423044024  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423044659                      G�O�G�O�G�O�                