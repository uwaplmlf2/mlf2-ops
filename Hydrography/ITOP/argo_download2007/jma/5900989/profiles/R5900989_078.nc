CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900989 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               NA   JA  20070802185424  20090423044717                                  2B  A   1575                                                            846 @Ԋd�	�1   @Ԋ��{�@5�vȴ9X@b��O�;1   ARGOS   A   A   A   @�ffA��A`  A���A�33A�33B	��B��B2  BFffBZ  Bl��B�ffB���B�33B�  B���B�ffB�  B�  B���B�ffB�33B�33B���C�CL�C�C�C�C��C��C$� C)L�C.� C333C8ffC=L�CB33CGL�CQ�C[  Ce33Cn�fCy� C��fC���C��3C���C�Y�C�� C���C��3C��fC���C��fC��fC���C�CǦfC�� Cѳ3C֦fCۀ C�ffC噚CꙚC�3C��fC��3D��D�3D��D�fDٚD��D� D$�3D)� D.� D3ٚD8�3D=�fDB� DG�3DL��DQ��DV�3D[��D`�fDe��Dj� DoٚDtٚDy� D�)�D�i�D��3D�� D�  D�l�D���D�� D�fD�i�D���D��fD�  D�Y�Dڬ�D���D�#3D�` D�D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  AfgA\��A�  A���A陙B��B  B133BE��BY33Bl  B��B�34B���B���B�34B�  B���Bƙ�B�fgB�  B���B���B�fgC �gC�C
�gC�gC�gCfgC��C$L�C)�C.L�C3  C833C=�CB  CG�CP�gCZ��Ce  Cn�3CyL�C���C�s3C���C�� C�@ C�ffC�� C���C���C�� C���C���C��3C Cǌ�C̦fCљ�C֌�C�ffC�L�C� C� CC��C���D� D�fD� D��D��D� D�3D$�fD)�3D.�3D3��D8�fD=ٙDB�3DG�fDL� DQ��DV�fD[� D`��De� Dj�3Do��Dt��Dy�3D�#4D�c4D���D�ٚD��D�fgD��gD�ٚD� D�c4D��4D�� D��D�S4DڦgD��gD��D�Y�D�4D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�A�VA��A���A��A�O�A�E�A�9XA�`BA�v�A���A�A�v�A���A�l�A�"�A���A�JA�x�A��A�VA��hA��A�bA�ffA��`A��A��mA�;dA��PA��DA���A���A��TA�bA�"�A��A�oA��
A��^A���A�ƨA��A�VA��\A��RA��+A�
=A��#A��FA�7LA|r�Ax5?As��An1'AhffAcp�A^1AZ�9AT^5AL��AH$�AC�A?`BA<$�A9�^A0�9A,�9A'�;A$�jA�;A��AXA~�@�p�@���@���@�-@мj@�ƨ@��@��@�dZ@�$�@�%@���@�~�@���@�Z@���@��+@�E�@�|�@�&�@��u@���@z=q@p��@e?}@Yhs@M/@A��@9��@5p�@.�y@*��@!��@��@-@\)@ƨ@��@	�7@�+@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�A�VA��A���A��A�O�A�E�A�9XA�`BA�v�A���A�A�v�A���A�l�A�"�A���A�JA�x�A��A�VA��hA��A�bA�ffA��`A��A��mA�;dA��PA��DA���A���A��TA�bA�"�A��A�oA��
A��^A���A�ƨA��A�VA��\A��RA��+A�
=A��#A��FA�7LA|r�Ax5?As��An1'AhffAcp�A^1AZ�9AT^5AL��AH$�AC�A?`BA<$�A9�^A0�9A,�9A'�;A$�jA�;A��AXA~�@�p�@���@���@�-@мj@�ƨ@��@��@�dZ@�$�@�%@���@�~�@���@�Z@���@��+@�E�@�|�@�&�@��u@���@z=q@p��@e?}@Yhs@M/@A��@9��@5p�@.�y@*��@!��@��@-@\)@ƨ@��@	�7@�+@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�Bx�By�Bx�Bw�Bw�Bv�Bv�Bs�BR�B(�B9XBB�BD�BK�BJ�B5?B)�B9XBB�BO�BG�BH�BG�B?}B;dB7LB6FB'�B�BuB
=B��B�ZB�B�B��B��B�RB�dBƨB�-B�bBk�BN�B8RB�BbB
��B
�/B
ĜB
�-B
��B
�B
jB
K�B
,B

=B	�B	�B	�LB	��B	� B	l�B	^5B	T�B	G�B	$�B	bB��B�BĜB�B��B�\B�PB~�By�B}�B�B��B��B�jB�
B�B	B	\B	#�B	<jB	R�B	cTB	v�B	�PB	��B	�9B	ǮB	�ZB	�B	��B
+B
VB
�B
!�B
1'B
5?B
?}B
M�B
L�B
O�B
R�B
ZB
_;B
hsB
o�B
s�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 Bx�By�Bx�Bw�Bw�Bv�Bv�Bs�BR�B(�B9XBB�BD�BK�BJ�B5?B)�B9XBB�BO�BG�BH�BG�B?}B;dB7LB6FB'�B�BuB
=B��B�ZB�B�B��B��B�RB�dBƨB�-B�bBk�BN�B8RB�BbB
��B
�/B
ĜB
�-B
��B
�B
jB
K�B
,B

=B	�B	�B	�LB	��B	� B	l�B	^5B	T�B	G�B	$�B	bB��B�BĜB�B��B�\B�PB~�By�B}�B�B��B��B�jB�
B�B	B	\B	#�B	<jB	R�B	cTB	v�B	�PB	��B	�9B	ǮB	�ZB	�B	��B
+B
VB
�B
!�B
1'B
5?B
?}B
M�B
L�B
O�B
R�B
ZB
_;B
hsB
o�B
s�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20070802185422  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070802185424  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070802185425  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070802185425  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070802185429  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070802185429  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070802185429  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070802185429  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070802185430  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070802185430  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070802191049                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070806154401  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070806154405  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070806154405  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070806154405  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070806154409  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070806154409  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070806154410  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070806154410  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070806154410  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070806154410  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070806160201                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070806154401  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423044013  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423044013  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090423044013  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423044013  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423044014  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423044014  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423044014  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423044014  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423044015  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423044717                      G�O�G�O�G�O�                