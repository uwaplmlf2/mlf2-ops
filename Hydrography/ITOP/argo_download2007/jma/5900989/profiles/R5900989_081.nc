CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900989 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               QA   JA  20070901191134  20090423044703                                  2B  A   1575                                                            846 @ԑ��'qf1   @ԑ���-"@7st�j~�@b���+1   ARGOS   A   A   A   @�  AffA[33A�33A�  A홚B
  B  B1��BF  BY33Bn  B���B�  B���B�ffB�33B�33B���B�33B�  B�ffB�  B�  B�  C��CffCffC� C� C�C�C$ffC)33C.�C3� C8L�C=� CBffCG� CQffC[L�CeL�CoffCyL�C��fC���C���C��3C�� C�s3C�� C���C�ffC�Y�C���C���C���C³3C�� C�� Cр C�� C�� C�� C��C�� C�fC�s3C��3D� D�3D�fD� DٚDٚDٚD$�fD)� D.� D3ٚD8�3D=�3DB�3DGٚDL�3DQ�3DV� D[��D`� De� Dj��Do�3Dt�fDy�3D�&fD�i�D��3D��D��D�ffD�� D�� D�#3D�` D���D��D�  D�ffDڠ D�� D�&fD�S3D�fD��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A  AT��A�  A���A�fgBffBffB0  BDffBW��BlffB�  B�33B�  B���B�ffB�ffB���B�ffB�33Bٙ�B�33B�33B�33C34C  C  C�C�C�4C�4C$  C(��C-�4C3�C7�gC=�CB  CG�CQ  CZ�gCd�gCo  Cx�gC�s3C�Y�C�fgC�� C���C�@ C�L�C�fgC�33C�&gC�Y�C�Y�C���C Cǌ�Č�C�L�C֌�Cی�C�L�C�Y�C��C�s3C�@ C�� D�fD��D��D�fD� D� D� D$��D)�fD.�fD3� D8��D=��DB��DG� DL��DQ��DV�fD[� D`�fDe�fDj�3Do��Dt��Dy��D��D�\�D��fD���D� D�Y�D��3D��3D�fD�S3D���D���D�3D�Y�Dړ3D��3D��D�FfD�D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��;A��TA��;A���A�A흲A�r�A�r�A���A���A�7LA�bNA�bA���A���A�x�AЬA���A�M�A�A���AŰ!A���A��FA��A�ȴA��uA��#A��PA��yA�t�A�-A�
=A��;A��hA�E�A�|�A�ȴA���A��HA���A��uA��FA� �A�+A��^A�
=A�$�A���A��7A���A�Q�A��`A�x�A�33A�&�Az-Aw��As"�Al��Ad��Aa/AZ^5AWl�AQ��AI�mAG��A?C�A<n�A7�hA.ȴA!�A�DA�A	|�@��@�M�@��H@��m@�+@��@��#@�E�@��
@�33@��#@�1@�$�@���@�+@���@�ff@�ƨ@�&�@���@���@��@sdZ@hr�@^�+@O�w@GK�@>�y@9�7@5@/|�@)��@!�#@S�@&�@I�@Q�@�
@
�@|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��;A��TA��;A���A�A흲A�r�A�r�A���A���A�7LA�bNA�bA���A���A�x�AЬA���A�M�A�A���AŰ!A���A��FA��A�ȴA��uA��#A��PA��yA�t�A�-A�
=A��;A��hA�E�A�|�A�ȴA���A��HA���A��uA��FA� �A�+A��^A�
=A�$�A���A��7A���A�Q�A��`A�x�A�33A�&�Az-Aw��As"�Al��Ad��Aa/AZ^5AWl�AQ��AI�mAG��A?C�A<n�A7�hA.ȴA!�A�DA�A	|�@��@�M�@��H@��m@�+@��@��#@�E�@��
@�33@��#@�1@�$�@���@�+@���@�ff@�ƨ@�&�@���@���@��@sdZ@hr�@^�+@O�w@GK�@>�y@9�7@5@/|�@)��@!�#@S�@&�@I�@Q�@�
@
�@|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�Bw�Bw�Bv�Bu�Bt�Bt�B}�B}�B.BF�B9XBA�BE�BK�BT�BW
B[#Be`BhsBffBYBVB_;B\)BR�BQ�BO�BL�BH�BJ�BA�B49B+B#�B�B�B�BB��B�yB��B��B��Bv�BbNBXBK�B=qB33B%�B�B+B
��B
�;B
��B
�LB
��B
�+B
n�B
H�B
�B
%B	�5B	��B	�XB	�uB	�B	bNB	VB	A�B	�B�yBĜB�LB��B��B�DB�%B�%B�1B�VB��B�BŢB�
B�B	1B	%�B	@�B	P�B	cTB	r�B	�B	�uB	��B	�XB	�)B	��B
B
DB
PB
�B
�B
!�B
(�B
/B
8RB
D�B
M�B
T�B
^5B
e`B
k�B
m�B
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 Bw�Bw�Bv�Bu�Bt�Bt�B}�B}�B.BF�B9XBA�BE�BK�BT�BW
B[#Be`BhsBffBYBVB_;B\)BR�BQ�BO�BL�BH�BJ�BA�B49B+B#�B�B�B�BB��B�yB��B��B��Bv�BbNBXBK�B=qB33B%�B�B+B
��B
�;B
��B
�LB
��B
�+B
n�B
H�B
�B
%B	�5B	��B	�XB	�uB	�B	bNB	VB	A�B	�B�yBĜB�LB��B��B�DB�%B�%B�1B�VB��B�BŢB�
B�B	1B	%�B	@�B	P�B	cTB	r�B	�B	�uB	��B	�XB	�)B	��B
B
DB
PB
�B
�B
!�B
(�B
/B
8RB
D�B
M�B
T�B
^5B
e`B
k�B
m�B
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20070901191114  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070901191134  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070901191136  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070901191139  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070901191146  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070901191146  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070901191147  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070901191147  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070901191148  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070901191148  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070901205827                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070905160045  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070905160049  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070905160049  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070905160050  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070905160054  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070905160054  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070905160054  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070905160054  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070905160054  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070905160054  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070905161824                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070905160045  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423044020  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423044020  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090423044020  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423044020  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423044021  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423044021  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423044021  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423044021  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423044022  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423044703                      G�O�G�O�G�O�                