CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900989 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               OA   JA  20070813010930  20090423044721                                  2B  A   1575                                                            846 @Ԍ����1   @Ԍ�X-��@6����o@b�1&�1   ARGOS   A   A   A   @�33A33AfffA�33A���A�ffB��B  B0��BFffBZ��Bm��B�33B�ffB���B���B�33B���B���B�  B�33B�ffB�33BB���C� CffCffC�C  C33C��C$�C)ffC.33C3L�C8� C=L�CB33CF�fCQL�C[ffCeL�Co33Cy  C��fC�� C��3C�� C�� C��fC���C���C�� C�� C��fC��fC���C�C�s3Č�C�Y�Cր Cۀ C�� C�s3C�fC�� C���C�� D��DٚD�fD�3D�fD��D�3D$��D)��D.�fD3ٚD8�fD=�3DB�fDG� DL� DQ� DV� D[�3D`� De�3Dj�3Do��Dt��Dy��D�)�D�i�D�� D���D�)�D�l�D�� D��fD�)�D�ffD��3D��fD�)�D�l�Dڙ�D���D�  D�Y�D�fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffAa��A���A�fgA�  B��B��B/��BE33BY��BlfgB33B���B�33B�33B���B�  B�33B�ffBϙ�B���B䙙B�  B�33C33C�C�C��C�3C�fCL�C#��C)�C-�fC3  C833C=  CA�fCF��CQ  C[�Ce  Cn�fCx�3C�� C���C���C���C���C�� C�fgC�fgC���C���C�� C�� C�fgC�fgC�L�C�fgC�34C�Y�C�Y�C�Y�C�L�C� CC��gC���D��D�gD�3D� D�3D�gD� D$�gD)��D.�3D3�gD8�3D=� DB�3DG��DL��DQ��DV��D[� D`��De� Dj� Do�gDt�gDy��D�  D�` D��fD��3D�  D�c3D��fD���D�  D�\�D���D���D�  D�c3Dڐ D��3D�fD�P D��D�vf1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�p�A��A�7A�\)A�K�A�33A� �A�%A��A�G�A��;A�r�A�{AՇ+A�r�A�x�A�E�A�JA�z�A���Aŧ�Aĕ�A�bNA���A�
=A��hA�-A���A��PA�1'A��HA���A�p�A��FA��A��A��RA��yA�p�A���A�~�A��^A�ȴA�9XA��-A��PA�"�A���A�I�A� �A�~�A��^A�r�A{?}Au��Aq�FAmƨAgG�AbjA\�\AX1'AT��AQ|�AM�PAI�
AG\)AA�mA>$�A<1A0��A't�A!l�A��AbNA	�FA�^A ��@�"�@�A�@�hs@��`@��y@�
=@�
=@��@��@��D@�M�@�$�@���@��^@�C�@��`@��y@�K�@�9X@z~�@r�@fV@]?}@T�j@HĜ@A7L@9&�@3dZ@-�@'l�@#�F@j@�w@$�@-@l�@�F@
�H1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�p�A��A�7A�\)A�K�A�33A� �A�%A��A�G�A��;A�r�A�{AՇ+A�r�A�x�A�E�A�JA�z�A���Aŧ�Aĕ�A�bNA���A�
=A��hA�-A���A��PA�1'A��HA���A�p�A��FA��A��A��RA��yA�p�A���A�~�A��^A�ȴA�9XA��-A��PA�"�A���A�I�A� �A�~�A��^A�r�A{?}Au��Aq�FAmƨAgG�AbjA\�\AX1'AT��AQ|�AM�PAI�
AG\)AA�mA>$�A<1A0��A't�A!l�A��AbNA	�FA�^A ��@�"�@�A�@�hs@��`@��y@�
=@�
=@��@��@��D@�M�@�$�@���@��^@�C�@��`@��y@�K�@�9X@z~�@r�@fV@]?}@T�j@HĜ@A7L@9&�@3dZ@-�@'l�@#�F@j@�w@$�@-@l�@�F@
�H1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�Bo�BhsBaHBbNBe`BgmBiyBhsBhsBdZBK�B,B33B-B2-B?}BA�B;dBC�BE�BVBN�BP�B_;B]/BW
BO�BH�BA�B:^B0!B+B(�B"�B�B�B��B�sB��B��B�?B�\Bm�BdZBQ�B=qB49B(�B�B\B
�B
�BB
�dB
��B
y�B
bNB
G�B
+B
bB	�B	�#B	ɺB	�XB	��B	�uB	�%B	n�B	]/B	O�B	!�B	B�`BɺB�9B��B��B�VB~�B� B�hB��B�BȴB�B�B��B	�B	$�B	?}B	W
B	_;B	p�B	�\B	��B	��B	��B	�B	��B
B
DB
�B
�B
33B
5?B
@�B
I�B
J�B
T�B
R�B
XB
\)B
bNB
gmB
m�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 Bo�BhsBaHBbNBe`BgmBiyBhsBhsBdZBK�B,B33B-B2-B?}BA�B;dBC�BE�BVBN�BP�B_;B]/BW
BO�BH�BA�B:^B0!B+B(�B"�B�B�B��B�sB��B��B�?B�\Bm�BdZBQ�B=qB49B(�B�B\B
�B
�BB
�dB
��B
y�B
bNB
G�B
+B
bB	�B	�#B	ɺB	�XB	��B	�uB	�%B	n�B	]/B	O�B	!�B	B�`BɺB�9B��B��B�VB~�B� B�hB��B�BȴB�B�B��B	�B	$�B	?}B	W
B	_;B	p�B	�\B	��B	��B	��B	�B	��B
B
DB
�B
�B
33B
5?B
@�B
I�B
J�B
T�B
R�B
XB
\)B
bNB
gmB
m�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20070813010916  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070813010930  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070813010932  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070813010934  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070813010940  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070813010940  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070813010941  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070813010941  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070813010942  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070813010942  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070813014457                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070816155725  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070816155729  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070816155730  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070816155730  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070816155734  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070816155734  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070816155734  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070816155734  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070816155735  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070816155735  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070816161740                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070816155725  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423044015  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423044015  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090423044015  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423044016  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423044017  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423044017  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423044017  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423044017  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423044017  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423044721                      G�O�G�O�G�O�                