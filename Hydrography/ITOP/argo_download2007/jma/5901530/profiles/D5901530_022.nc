CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901530 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20071010130212  20090908021430  A9_66111_022                    2C  D   APEX-SBE 2820                                                   846 @ԛH�4�1   @ԛL�\�@/bM��@b
=p��1   ARGOS   A   A   A   @�  A  Aa��A�ffA�  A���B
��BffB2��BF��BX��Bn  B�ffB�  B�ffB�ffB���B�ffB�  Bƙ�B���B�  B�ffB�  B�  C� C33CL�C� CffC33C��C$L�C)ffC.� C3�C8L�C=L�CB33CG33CQ  C[33CeffCo�Cy� C���C�� C�s3C���C���C�ffC���C���C��fC��3C�� C��3C�� C³3Cǀ C�ffCѳ3C�s3Cۙ�C�� C噚C�3C� C� C�� D�3D��D� D��D�3D�fD� D$�fD)��D.��D3�fD8ٚD=� DBٚDG�fDL�3DQ�3DV�3D[ٚD`�3DeٚDj�3Do�3Dt�3Dy�fD�#3D�i�D���D��fD�,�D�c3D�� D��3D�,�D�\�D��3D��D�)�D�p Dک�D���D�&fD�c3D�D�6f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��A^ffA���A�ffA�33B
  B��B2  BF  BX  Bm33B�  B���B�  B�  B�33B�  B���B�33B�ffBڙ�B�  BB���CL�C  C�CL�C33C  CffC$�C)33C.L�C2�fC8�C=�CB  CG  CP��C[  Ce33Cn�fCyL�C��3C��fC�Y�C�� C�s3C�L�C�� C�s3C���C���C��fC���C��fC�C�ffC�L�Cљ�C�Y�Cۀ C�fC� CꙚC�ffC�ffC�ffD�fD� D�3D� D�fD��D�3D$��D)��D.� D3ٚD8��D=�3DB��DG��DL�fDQ�fDV�fD[��D`�fDe��Dj�fDo�fDt�fDy��D��D�c3D��fD�� D�&fD�\�D���D���D�&fD�VfD���D��3D�#3D�i�Dڣ3D��fD�  D�\�D�3D�0 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��wA��A�{A�{A��A� �A�S�A�^5A��A��A�;dA��mA�`BA߮A݃A�dZA�&�AڅA�/Aԉ7AѾwA�S�A�ĜA�;dA�n�A�-A�5?A�bNA��mA�dZA��\A�jA�M�A�K�A�C�A���A�`BA��A�A��PA�?}A�?}A�oA��A�A��PA��A|I�Ar��Aj��AgO�AaXA[?}AO�AJ�uAD��A>�A7�
A3%A*�A$VA �9A��A��A�PA
 �A=qA r�@���@�S�@�V@�7L@��@�+@���@̴9@�dZ@��@���@���@�~�@�V@��@�z�@�J@���@�v�@�C�@��F@�@�+@�j@��-@�  @��@���@�?}@w+@h��@^@U�T@R�@G��@>��@7�P@.��@(Ĝ@%V@ Q�@��@��@t�@��@z�@A�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��wA��A�{A�{A��A� �A�S�A�^5A��A��A�;dA��mA�`BA߮A݃A�dZA�&�AڅA�/Aԉ7AѾwA�S�A�ĜA�;dA�n�A�-A�5?A�bNA��mA�dZA��\A�jA�M�A�K�A�C�A���A�`BA��A�A��PA�?}A�?}A�oA��A�A��PA��A|I�Ar��Aj��AgO�AaXA[?}AO�AJ�uAD��A>�A7�
A3%A*�A$VA �9A��A��A�PA
 �A=qA r�@���@�S�@�V@�7L@��@�+@���@̴9@�dZ@��@���@���@�~�@�V@��@�z�@�J@���@�v�@�C�@��F@�@�+@�j@��-@�  @��@���@�?}@w+@h��@^@U�T@R�@G��@>��@7�P@.��@(Ĝ@%V@ Q�@��@��@t�@��@z�@A�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��B�B�B	  B	B	\B	9XB	\)B	�yB
��BB�B%�B&�B?}BZB^5Bo�B��B�FB��B�5B�BB �B33B@�BE�BI�BJ�BI�BN�BC�B7LB�B+BJB�B��B�BcTBG�B6FB�BB
�B
��B
{�B
J�B
%�B
PB	�B	��B	��B	��B	�B	p�B	VB	;dB	)�B	{B	
=B�B�BB��B�RB�wB�qB��BĜB��B�B	-B	R�B	M�B	~�B	�uB	��B	��B	��B	�RB	��B	��B	�BB	�ZB	�B	�B	��B	��B
  B
B

=B
\B
hB
�B
 �B
%�B
,B
5?B
=qB
B�B
E�B
K�B
P�B
VB
\)B
`BB
cTB
e`B
iyB
l�B
p�B
q�B
u�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B�B�B	  B	B	\B	9XB	]/B	�B
��BB�B&�B(�B@�BZB_;Bs�B��B�RB��B�BB��BB!�B5?BB�BE�BI�BK�BL�BP�BH�B;dB�B/BbB�B��B�-Be`BI�B7LB �BB
�B
��B
}�B
L�B
&�B
VB	�B	�
B	��B	��B	�B	r�B	W
B	=qB	,B	�B	JB��B�HB��B�^B�}B�wBBŢB��B�B	-B	S�B	N�B	~�B	�uB	��B	��B	��B	�RB	��B	��B	�BB	�ZB	�B	�B	��B	��B
  B
B

=B
\B
hB
�B
 �B
%�B
,B
5?B
=qB
B�B
E�B
K�B
P�B
VB
\)B
`BB
cTB
e`B
iyB
l�B
p�B
q�B
u�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710231407592007102314075920071023140759200710231411242007102314112420071023141124200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20071010130209  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071010130212  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071010130213  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071010130217  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071010130217  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071010130218  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071010130850                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071014160845  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071014160848  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071014160849  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071014160853  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071014160853  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071014160853  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071014190857                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071014160845  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422064221  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422064222  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422064222  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422064223  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422064223  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422064223  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422064223  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422064223  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422064659                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071013224157  CV  DAT$            G�O�G�O�F��a                JM  ARCAJMQC1.0                                                                 20071023140759  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071023140759  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071023141124  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021355  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021430                      G�O�G�O�G�O�                