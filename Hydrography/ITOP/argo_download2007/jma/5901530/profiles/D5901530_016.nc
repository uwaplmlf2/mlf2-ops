CDF   	   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901530 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070811201634  20090908021438  A9_66111_016                    2C  D   APEX-SBE 2820                                                   846 @ԌFƊ�1   @ԌH��9@1n��+@a��E���1   ARGOS   A   A   A   @���A  A^ffA�33A�  A�ffB  B33B0��BFffBY33Bn  B�  B���B�  B���B���B�  B�33B���B�33B���B���B�33B�ffC��C� C  C�C� CffC��C$33C)� C.� C3��C8ffC=��CB��CGffCQL�C[ffCe� CoffCy33C��fC�� C��3C���C��fC��fC�s3C��fC�� C��3C���C��3C���C�� C�� C̙�CѦfC֌�C۳3C�3C���CꙚC�3C�� C��fD� D��D�3D� D� D�3DٚD$�fD)� D.�3D3��D8� D=� DB�3DG��DL� DQ� DV� D[�3D`ٚDeٚDjٚDo��Dt� Dy�fD�)�D�c3D���D��D�)�D�\�D��fD���D�0 D�i�D�� D�� D�  D�i�Dک�D���D�&fD�\�D�3D�C31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffA\��A�ffA�33A陚B��B��B0ffBF  BX��Bm��B���B�ffB���B�ffB���B���B�  Bƙ�B�  Bڙ�B䙚B�  B�33C� CffC
�fC  CffCL�C� C$�C)ffC.ffC3� C8L�C=� CB� CGL�CQ33C[L�CeffCoL�Cy�C���C�s3C��fC���C���C���C�ffC���C��3C��fC�� C��fC���C³3Cǳ3Č�Cљ�Cր CۦfC�fC�� C��C�fC��3C���D��D�fD��DٚD��D��D�3D$� D)��D.��D3�fD8ٚD=ٚDB��DG�3DL��DQ��DV��D[��D`�3De�3Dj�3Do�fDt��Dy� D�&fD�` D��fD��fD�&fD�Y�D��3D�ٚD�,�D�ffD���D���D��D�ffDڦfD�ٚD�#3D�Y�D� D�@ 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�oA���A�{A��A�wA��A��A�jA�~�A�jA�;dA��A�RA��A�;dA�`BA�jA��yA�A��A�p�Aݧ�A��HA�JA��A�=qA�1A�1A�-Aϩ�A���A�%A�  A�;dAǉ7A�-A� �A��`A��;A�JA���A��HA��7A�A�A��^A�%A���A��A��A{�AqG�Am��Af�RA[��APĜAG\)AA\)A>VA6�A1��A-33A)�PA#�A"ffA!x�AĜA{A�yA
^5An�@���@�7L@�"�@թ�@�t�@�;d@��@�$�@�~�@��@��T@�|�@�{@�p�@�ȴ@���@�5?@�K�@�Z@�?}@��!@���@�Ĝ@�E�@��D@�ff@�&�@w��@m�h@ct�@\�D@W�@R~�@L�D@CdZ@8�u@0��@'��@ �u@�@~�@  @�j@��@1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�oA���A�{A��A�wA��A��A�jA�~�A�jA�;dA��A�RA��A�;dA�`BA�jA��yA�A��A�p�Aݧ�A��HA�JA��A�=qA�1A�1A�-Aϩ�A���A�%A�  A�;dAǉ7A�-A� �A��`A��;A�JA���A��HA��7A�A�A��^A�%A���A��A��A{�AqG�Am��Af�RA[��APĜAG\)AA\)A>VA6�A1��A-33A)�PA#�A"ffA!x�AĜA{A�yA
^5An�@���@�7L@�"�@թ�@�t�@�;d@��@�$�@�~�@��@��T@�|�@�{@�p�@�ȴ@���@�5?@�K�@�Z@�?}@��!@���@�Ĝ@�E�@��D@�ff@�&�@w��@m�h@ct�@\�D@W�@R~�@L�D@CdZ@8�u@0��@'��@ �u@�@~�@  @�j@��@1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	��B	��B	�FB	�B	�fB	�B	�B	��B
%B
DB
\B
PB
	7B
Q�B
�B
�^B
�qB
�B
��BB	7BVBQ�B`BBx�B�\B��B�wB�B�B��BPBoB49B@�BZBhsBjBffB\)B7LB�B��B�\Bs�BF�BbB
B
t�B
T�B
)�B
�B	��B	ƨB	��B	|�B	jB	u�B	gmB	\)B	O�B	<jB	33B	33B	/B	(�B	�B	+B�B�B�yB��B�dB�sB	�B	<jB	K�B	aHB	q�B	z�B	�VB	��B	�?B	ƨB	��B	�NB	�B	�B	��B	��B	��B
B
B
PB
hB
�B
!�B
�B
(�B
0!B
49B
7LB
:^B
>wB
D�B
L�B
O�B
VB
[#B
`BB
dZB
ffB
jB
m�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	��B	�FB	�B	�fB	�B	�B	��B
%B
DB
\B
PB
JB
R�B
�+B
�dB
��B
�B
��B%B
=BbBQ�BaHBy�B�bB��B��B�B�B��BVBuB5?BC�B]/BiyBk�BiyB^5B:^B �B�B�hBu�BJ�BuB
ŢB
v�B
W
B
+B
�B	��B	ɺB	��B	~�B	k�B	w�B	hsB	]/B	P�B	>wB	33B	33B	0!B	+B	�B		7B��B�B�B��B�dB�yB	�B	<jB	L�B	aHB	q�B	z�B	�VB	��B	�?B	ƨB	��B	�NB	�B	�B	��B	��B	��B
B
B
PB
hB
�B
!�B
�B
(�B
0!B
49B
7LB
:^B
>wB
D�B
L�B
O�B
VB
[#B
`BB
dZB
ffB
jB
m�B
o�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200708241405202007082414052020070824140520200708241422172007082414221720070824142217200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070811201619  IP                  G�O�G�O�G�O�                JM  ARFMDECPA9                                                                  20070815001126  IP                  G�O�G�O�G�O�                JM  ARGQRQCP1                                                                   20070815001126  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20070824140520  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070824140520  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070824142217  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021406  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021438                      G�O�G�O�G�O�                