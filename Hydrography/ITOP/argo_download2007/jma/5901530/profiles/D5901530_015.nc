CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901530 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070801190325  20090908021436  A9_66111_015                    2C  D   APEX-SBE 2820                                                   846 @ԉ�9u0�1   @ԉ�Ǯ{@1t�j~��@a�C��%1   ARGOS   A   A   A   @�ffAffAc33A���A���A噚B��B33B0ffBE��BZffBm33B�33B�  B�  B�33B���B���B���B�  B�  Bڙ�B�ffB�33B���C� C� CL�CL�CL�C� CL�C$ffC)ffC.��C3L�C8��C=� CB�CG�3CQL�C[  Ce�Co� CyffC���C���C�� C���C���C���C���C���C���C�� C���C��fC��3C�Cǳ3C̳3C�� Cֳ3Cۀ C�3C�3C��C� C��C���D� D�fD��D��D��D� D�fD$��D)��D.��D3�fD8��D=��DB��DG��DL�fDQ�3DV�fD[��D`� De�fDj�fDo��Dt� Dy�3D�  D�\�D�� D���D�)�D�` D���D���D�  D�ffD��3D��fD�  D�c3Dڬ�D��fD�  D�VfD�D�ff1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��Aa��A�  A���A���BffB��B0  BE33BZ  Bl��B�  B���B���B�  B���B�ffB�ffB���B���B�ffB�33B�  B���CffCffC33C33C33CffC33C$L�C)L�C.� C333C8� C=ffCB  CG��CQ33CZ�fCe  CoffCyL�C���C���C��3C���C���C���C�� C�� C���C��3C�� C���C��fC�CǦfC̦fCѳ3C֦fC�s3C�fC�fC� C�s3C� C�� DٚD� D�fD�fD�fD��D� D$�fD)�fD.�3D3� D8�fD=�fDB�fDG�3DL� DQ��DV� D[�fD`ٚDe� Dj� Do�fDt��Dy��D��D�Y�D���D�ٚD�&fD�\�D���D��D��D�c3D�� D��3D��D�` Dک�D��3D��D�S3D�fD�c31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�Q�A��A�ƨA��AA�PA�n�A�K�A�/A�(�A��`A�x�A�ĜA�t�A�$�A�7LA�A�A�ffA٬Aإ�A�"�A��`A��A�O�A�C�A�O�A��/A�O�A�x�A͸RA�p�A��A�-Aǥ�A��yA��A�?}A���A��!A�7LA�A���A�%A�=qA�{A�A��HA�I�A�l�A/Ar�uAk��Af��AZ�DAT�AO%AG��AAS�A7|�A/A.A�A'�7A$z�A!x�AdZA7LA�#A��A�PA�7A��@�S�@�!@ޏ\@��/@̋D@�Z@�@�@�M�@���@�dZ@��j@�Q�@�Ĝ@���@���@��\@���@���@�O�@�K�@��@�v�@��m@��7@|�j@s��@h �@^v�@W\)@Q7L@HbN@A��@=@6��@/�@)�@%p�@ Q�@I�@Q�@��@�y@Z1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�Q�A��A�ƨA��AA�PA�n�A�K�A�/A�(�A��`A�x�A�ĜA�t�A�$�A�7LA�A�A�ffA٬Aإ�A�"�A��`A��A�O�A�C�A�O�A��/A�O�A�x�A͸RA�p�A��A�-Aǥ�A��yA��A�?}A���A��!A�7LA�A���A�%A�=qA�{A�A��HA�I�A�l�A/Ar�uAk��Af��AZ�DAT�AO%AG��AAS�A7|�A/A.A�A'�7A$z�A!x�AdZA7LA�#A��A�PA�7A��@�S�@�!@ޏ\@��/@̋D@�Z@�@�@�M�@���@�dZ@��j@�Q�@�Ĝ@���@���@��\@���@���@�O�@�K�@��@�v�@��m@��7@|�j@s��@h �@^v�@W\)@Q7L@HbN@A��@=@6��@/�@)�@%p�@ Q�@I�@Q�@��@�y@Z1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	m�B	ffB	k�B	m�B	w�B	~�B	�DB	�oB	��B	��B	��B	�B
'�B
��B.BXBbNBl�Bv�B�7B�{B��B�'B��B�;B�BB�TB�#B�HB�B��BbB�B�B=qBN�BYB_;B^5BYB�fB�RB��BH�B
�TB
ÖB
�'B
�B
l�B
ffB
.B
oB	�B	B	��B	��B	� B	k�B	L�B	<jB	8RB	=qB	8RB	!�B	�B	'�B	'�B	�B��B�B�BĜB�
B�)B�B	1B	+B	K�B	_;B	u�B	��B	��B	�9B	��B	��B	��B	�B	�NB	�sB	�B	��B	��B
B
B
+B
bB
�B
$�B
.B
33B
8RB
;dB
@�B
E�B
H�B
L�B
O�B
VB
W
B
ZB
`BB
e`B
hsB
n�B
q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	m�B	ffB	k�B	m�B	w�B	~�B	�DB	�oB	��B	��B	��B	�#B
.B
��B0!BYBcTBm�Bw�B�7B��B��B�-B��B�BB�HB�ZB�)B�TB�B��BhB�B�B?}BO�B[#B`BBbNB_;B�B�XB��BL�B
�`B
ĜB
�9B
�%B
l�B
iyB
0!B
uB	��B	ĜB	�B	��B	�B	n�B	N�B	<jB	:^B	>wB	9XB	"�B	�B	'�B	(�B	 �B��B�B�BĜB�B�/B�B	1B	+B	K�B	_;B	u�B	��B	��B	�9B	��B	��B	��B	�B	�NB	�sB	�B	��B	��B
B
B
+B
bB
�B
$�B
.B
33B
8RB
;dB
@�B
E�B
H�B
L�B
O�B
VB
W
B
ZB
`BB
e`B
hsB
n�B
q�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200708141511342007081415113420070814151134200708141515202007081415152020070814151520200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070801190323  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070801190325  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070801190326  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070801190330  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070801190330  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070801190331  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070801191018                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20090423023416  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090423023603  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090423023603  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090423023604  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090423023605  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090423023605  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090423023605  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090423023605  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090423023605  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090423024219                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070804231448  CV  DAT$            G�O�G�O�F�N{                JM  ARCAJMQC1.0                                                                 20070814151134  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070814151134  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070814151520  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021404  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021436                      G�O�G�O�G�O�                