CDF   	   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901530 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20071005161502  20090908021429  A9_66111_021                    2C  D   APEX-SBE 2820                                                   846 @Ԙ�S�1   @ԘϔJU�@//��v�@bKƧ�1   ARGOS   A   A   A   @�  A��AfffA���A�ffA�ffB��B33B2  BF  BY��Bn  B�  B���B�33B���B�33B���B�33B�33BЙ�B�33B�  B�ffB�  C� C��C�3C��CL�CffC�C$�C)� C.�3C3ffC8��C=� CB� CG� CQ33C[33CeffCo33CyffC�� C���C���C���C�� C��fC�� C�s3C���C��fC�� C���C���C�C�ffC�s3C�Y�C֌�C�� C���C�3C�� C� C��3C���D��D� D� D�fD�3DٚDٚD$�3D)�3D.�fD3� D8��D=��DB��DG�3DL�3DQ�fDV��D[��D`� DeٚDjٚDo��Dt�3Dy��D��D�l�D���D�ٚD�)�D�\�D�� D�� D��D�ffD���D��3D�,�D�p Dک�D��D�&fD�S3D�fD�ɚ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33Ad��A�  A���A陚BffB��B1��BE��BY33Bm��B���B���B�  B���B�  B�ffB�  B�  B�ffB�  B���B�33B���CffC�3C��C� C33CL�C  C$  C)ffC.��C3L�C8� C=ffCBffCGffCQ�C[�CeL�Co�CyL�C��3C�� C�� C�� C�s3C���C�s3C�ffC�� C���C��3C�� C�� C C�Y�C�ffC�L�Cր C۳3C���C�fC�3C�s3C��fC���D�fD��DٚD� D��D�3D�3D$��D)��D.� D3��D8�fD=�fDB�3DG��DL��DQ� DV�fD[�fD`ٚDe�3Dj�3Do�fDt��Dy�3D�fD�i�D���D��fD�&fD�Y�D���D���D��D�c3D���D�� D�)�D�l�DڦfD��fD�#3D�P D�3D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A�-A��A쟾A왚A웦A왚A웦A웦A웦A쟾A��A��A��A���A�I�A�+A��
A��TAՋDA�hsAԟ�A��A�E�A��A��TA�A�$�A��/A��hA�p�A���A���A�~�A�ffA�ȴA��+A�1A��mA���A�\)A�bA�hsA���A�x�A��A�"�A|E�Ar�uAh�AbI�A[7LAT1'AO�AI�#A@~�A<~�A2 �A0��A&  A-A��A��Ar�AQ�A�A��@�Q�@�n�@�j@�@�
=@��@υ@͙�@�n�@�%@�Z@�C�@�G�@�O�@�7L@���@�Z@��@���@��
@�ff@�1@�E�@��@�V@�Q�@�n�@�
=@���@}O�@o�@g;d@`��@Z^5@RJ@L�j@E@<�@8��@2n�@(r�@"-@��@/@b@��@��@
�\1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A�-A��A쟾A왚A웦A왚A웦A웦A웦A쟾A��A��A��A���A�I�A�+A��
A��TAՋDA�hsAԟ�A��A�E�A��A��TA�A�$�A��/A��hA�p�A���A���A�~�A�ffA�ȴA��+A�1A��mA���A�\)A�bA�hsA���A�x�A��A�"�A|E�Ar�uAh�AbI�A[7LAT1'AO�AI�#A@~�A<~�A2 �A0��A&  A-A��A��Ar�AQ�A�A��@�Q�@�n�@�j@�@�
=@��@υ@͙�@�n�@�%@�Z@�C�@�G�@�O�@�7L@���@�Z@��@���@��
@�ff@�1@�E�@��@�V@�Q�@�n�@�
=@���@}O�@o�@g;d@`��@Z^5@RJ@L�j@E@<�@8��@2n�@(r�@"-@��@/@b@��@��@
�\1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	v�B	v�B	w�B	v�B	w�B	v�B	x�B	x�B	w�B	x�B	x�B	y�B	z�B	�`B
�B
��B
��BN�BXB�hB�DB��B�B�B��BJBhB�B$�B49BI�BM�BK�B@�B33B�B+B��B�B�NBŢB�\BL�BhB
�B
��B
��B
x�B
;dB
bB	�B	�B	�qB	�B	�hB	t�B	jB	H�B	@�B	'�B	�B	uB	oB		7B�B��B��B��B�B�`B	B	VB	D�B	J�B	z�B	�\B	�PB	��B	��B	�dB	ȴB	��B	��B	�B	�#B	�TB	�B	�B	��B	��B	��B	��B
B
B
JB
�B
%�B
1'B
6FB
;dB
?}B
D�B
H�B
L�B
R�B
T�B
YB
_;B
bNB
e`B
gmB
m�B
o�B
s�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	v�B	v�B	w�B	v�B	w�B	v�B	x�B	x�B	w�B	x�B	x�B	y�B	{�B	�B
�!B
��B
��BP�BYB�hB�JB��B�B�#BB\B{B �B(�B7LBK�BN�BN�BC�B6FB�B	7B��B�B�TBȴB�oBO�BuB
��B
��B
��B
z�B
=qB
oB	��B	�B	�wB	�B	�uB	u�B	m�B	H�B	C�B	)�B	 �B	{B	uB	
=B�B��B��B��B�B�`B	%B	VB	E�B	J�B	z�B	�\B	�PB	��B	��B	�dB	ȴB	��B	��B	�B	�#B	�TB	�B	�B	��B	��B	��B	��B
B
B
JB
�B
%�B
1'B
6FB
;dB
?}B
D�B
H�B
L�B
R�B
T�B
YB
_;B
bNB
e`B
gmB
m�B
o�B
s�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200903261410212009032614102120090326141021200904090438362009040904383620090409043836200909040000002009090400000020090904000000  JA  ARFMdecpA9_b                                                                20071004070014  IP                  G�O�G�O�G�O�                JM  ARFMDECPA9                                                                  20090324191752  IP                  G�O�G�O�G�O�                JM  ARGQRQCP1                                                                   20090324191752  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20090326141021  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090326141021  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090409043836  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090904000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021353  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021429                      G�O�G�O�G�O�                