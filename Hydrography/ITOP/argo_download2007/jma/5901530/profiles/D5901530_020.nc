CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901530 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070920190356  20090908021427  A9_66111_020                    2C  D   APEX-SBE 2820                                                   846 @ԖG�0�}1   @ԖHR}'�@/���O�;@b�^5?}1   ARGOS   A   A   A   @�  A  Ad��A�  A���A�  B	��B��B2ffBE33BY��Bm��B�ffB���B���B�ffB���B�33B�  Bř�B�33B���B�33B�  B�  C33C�3C� CffC33C� C�C$ffC)�C.33C3ffC8� C=33CBffCG� CQ  C[  Ce33Co33Cy33C���C���C��fC��fC�s3C���C�ffC���C�� C�� C���C�� C�s3C³3CǙ�C̳3C�ٚC�� CۦfC�� C噚CꙚC�� C��C�� D�fD��D��D��D��DٚD�fD$�3D)�3D.ٚD3ٚD8�fD=��DB� DG� DL�fDQ��DV�3D[��D`� DeٚDj� DoٚDtٚDy��D��D�l�D�� D��fD�#3D�ffD�� D���D��D�c3D��fD�� D�#3D�p Dڣ3D���D�#3D�c3D�D��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @s33A��Aa��A�ffA�33A�ffB��B  B1��BDffBX��Bl��B�  B�33B�ffB�  B�33B���B���B�33B���B�ffB���BB���C  C� CL�C33C  CL�C�fC$33C(�fC.  C333C8L�C=  CB33CGL�CP��CZ��Ce  Co  Cy  C��3C�� C���C���C�Y�C�s3C�L�C�� C��fC��fC�s3C��fC�Y�C�Cǀ C̙�C�� C֦fCی�C�ffC� C� C�fC�s3C��fD��D� D� D� D� D��D��D$�fD)�fD.��D3��D8��D=� DB�3DG�3DL��DQ��DV�fD[� D`�3De��Dj�3Do��Dt��Dy� D�3D�ffD���D�� D��D�` D���D��fD�fD�\�D�� D�ٚD��D�i�Dڜ�D��fD��D�\�D�3D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A�\)A�M�A�G�A�G�A�E�A�?}A�v�A�Q�A�K�A�p�A�A�A�ffAܑhAڶFA�|�A�A���A�I�A�l�A�7LA� �A�|�A��AƋDA�
=A��;A�;dA�~�A�r�A��A�M�A�hsA��wA��!A�"�A�%A�1A�Q�A��A�1A��hA��7A��A��9A�ffA��DA��9A�AxM�ArVAlI�A`=qAW��AN��AC�TA:��A2(�A*=qA$��A7LA�\A�`A"�A
bA��AĜ@�b@�(�@�h@�v�@�S�@�{@�t�@�/@���@�@�X@��F@�X@��!@���@��/@�E�@��y@��D@��\@�|�@�+@��H@��m@�@��P@�G�@�Q�@��@�K�@yhs@lj@dI�@[t�@St�@P �@J�@BJ@8bN@0b@+�
@"�\@ff@o@�;@&�@��@	7L1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A�\)A�M�A�G�A�G�A�E�A�?}A�v�A�Q�A�K�A�p�A�A�A�ffAܑhAڶFA�|�A�A���A�I�A�l�A�7LA� �A�|�A��AƋDA�
=A��;A�;dA�~�A�r�A��A�M�A�hsA��wA��!A�"�A�%A�1A�Q�A��A�1A��hA��7A��A��9A�ffA��DA��9A�AxM�ArVAlI�A`=qAW��AN��AC�TA:��A2(�A*=qA$��A7LA�\A�`A"�A
bA��AĜ@�b@�(�@�h@�v�@�S�@�{@�t�@�/@���@�@�X@��F@�X@��!@���@��/@�E�@��y@��D@��\@�|�@�+@��H@��m@�@��P@�G�@�Q�@��@�K�@yhs@lj@dI�@[t�@St�@P �@J�@BJ@8bN@0b@+�
@"�\@ff@o@�;@&�@��@	7L1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	`BB	^5B	_;B	`BB	aHB	bNB	q�B	��B	�B
J�B
�sB
��B2-BJ�B[#B��B��B�FB��B�yB��BJB$�B49B]/B`BBdZBffBcTB[#BS�BK�BB�B:^B,B�BJB�BÖB��B�%BaHBW
BD�BA�B{B
�/B
�-B
�JB
\)B
?}B
�B	�fB	ÖB	��B	x�B	\)B	D�B	5?B	+B	�B	�B	
=B	  B�
B�B�BB�B	JB	�B	O�B	dZB	hsB	x�B	� B	�B	�{B	��B	�3B	�dB	��B	ǮB	��B	�B	�;B	�fB	�yB	�B	�B	��B	��B
B
B

=B
DB
�B
"�B
)�B
2-B
8RB
>wB
D�B
E�B
I�B
N�B
T�B
[#B
^5B
dZB
dZB
hsB
jB
q�B
t�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	`BB	^5B	_;B	`BB	aHB	bNB	q�B	��B	�B
O�B
�yB
��B33BL�B_;B��B��B�RB��B�B��BPB%�B6FB]/BbNBffBiyBgmB^5BW
BM�BC�B=qB.B�B\B�BƨB�B�7BbNBXBE�BC�B�B
�;B
�3B
�VB
]/B
A�B
�B	�sB	ŢB	��B	z�B	^5B	F�B	7LB	-B	�B	�B	DB	B�B�#B�NB�B	PB	�B	P�B	e`B	iyB	x�B	�B	�B	�{B	��B	�3B	�dB	��B	ǮB	��B	�B	�;B	�fB	�yB	�B	�B	��B	��B
B
B

=B
DB
�B
"�B
)�B
2-B
8RB
>wB
D�B
E�B
I�B
N�B
T�B
[#B
^5B
dZB
dZB
hsB
jB
q�B
t�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200909031305062009090313050620090903130506200909031313142009090313131420090903131314200909040000002009090400000020090904000000  JA  ARFMdecpA9_b                                                                20070920190352  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070920190356  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070920190357  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070920190401  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070920190401  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070920190401  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070920191109                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070924045055  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070924045105  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070924045108  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070924045115  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070924045116  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070924045116  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070924045116  CV  TIME            G�O�G�O�                    JA  ARGQrelo2.1                                                                 20070924045116  CV  LAT$            G�O�G�O�A}t�                JA  ARGQrelo2.1                                                                 20070924045116  CV  LON$            G�O�G�O�C͑                JA  ARUP                                                                        20070924051018                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070924045055  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090422064216  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090422064216  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090422064217  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090422064218  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090422064218  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090422064218  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090422064218  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090422064218  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090422064654                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070924000218  CV  DAT$            G�O�G�O�F��C                JM  ARCAJMQC1.0                                                                 20090903130506  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090903130506  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090903131314  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090904000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908021350  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908021427                      G�O�G�O�G�O�                