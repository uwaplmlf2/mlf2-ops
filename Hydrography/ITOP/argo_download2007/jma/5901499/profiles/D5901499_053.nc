CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901499 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               5A   JA  20071008045029  20080826081058  A5_24108_053                    2C  D   APEX-SBE 1714                                                   846 @Ԛ����1   @Ԛ��ci@8b�\(��@b{dZ�1   ARGOS   A   A   A   @���A33Ai��A�ffA�33A�ffB
ffBffB2ffBDffBX��Bm33B���B���B�  B�33B���B�ffB�  B�33B���Bڙ�B�ffB�33B���C  C33C�C�fC  C�C33C$� C)�3C.��C3��C8L�C=��CB� CGffCQ33CZ�fCe� Co  CyffC���C���C���C�s3C�� C�ffC��fC�L�C�Y�C�� C���C��3C�� C�� CǦfC�� CѦfCֳ3C�s3C�Y�C� CꙚC� C� C��fD� D��D�3D� D�3D�3D� D$�fD)��D.�fD3� D8�3D=�3DB�3DG�fDLٚDQ�3DV��D[��D`��De� Dj��DoٚDtٚDy�3D�)�D�l�D��3D��3D�  D�ffD���D�� D�,�D�` D���D���D�0 D�l�Dڣ3D�� D�&fD�\�D�fD�,�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33Ai��A�ffA�33A�ffB
ffBffB2ffBDffBX��Bm33B���B���B�  B�33B���B�ffB�  B�33B���Bڙ�B�ffB�33B���C  C33C�C�fC  C�C33C$� C)�3C.��C3��C8L�C=��CB� CGffCQ33CZ�fCe� Co  CyffC���C���C���C�s3C�� C�ffC��fC�L�C�Y�C�� C���C��3C�� C�� CǦfC�� CѦfCֳ3C�s3C�Y�C� CꙚC� C� C��fD� D��D�3D� D�3D�3D� D$�fD)��D.�fD3� D8�3D=�3DB�3DG�fDLٚDQ�3DV��D[��D`��De� Dj��DoٚDtٚDy�3D�)�D�l�D��3D��3D�  D�ffD���D�� D�,�D�` D���D���D�0 D�l�Dڣ3D�� D�&fD�\�D�fD�,�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A��HA��TA��;A��;A��;A��;A�ȴA�
=A�{A��A���A��yA�z�A���A���A�K�A�A���A��;A�x�A�E�A�&�A��A�ƨA�|�A�ZA�"�A�ffA�`BA���A�  A�Q�A���A��wA��+A�{A���A�ZA���A��+A���A�+A���A�A�bA�A�?}A�"�A~z�A|�jAxbAtbNAq�Am�FAh�AbVA]�hAY�AR-ANv�AJ�uAE%AAK�A<�jA7�A4�A.�9A,��A(5?A�uA?}AK�@�K�@�S�@��y@�
=@�j@��/@�33@���@�^5@��@��j@��@�v�@��D@��T@��-@�M�@~@t1@l(�@e@_�@V�R@N�y@Fȴ@<��@5�@,��@)X@#ƨ@�D@�@��@9X@�9@{@��@&�?�p�?�E�?��?� �1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A��HA��TA��;A��;A��;A��;A�ȴA�
=A�{A��A���A��yA�z�A���A���A�K�A�A���A��;A�x�A�E�A�&�A��A�ƨA�|�A�ZA�"�A�ffA�`BA���A�  A�Q�A���A��wA��+A�{A���A�ZA���A��+A���A�+A���A�A�bA�A�?}A�"�A~z�A|�jAxbAtbNAq�Am�FAh�AbVA]�hAY�AR-ANv�AJ�uAE%AAK�A<�jA7�A4�A.�9A,��A(5?A�uA?}AK�@�K�@�S�@��y@�
=@�j@��/@�33@���@�^5@��@��j@��@�v�@��D@��T@��-@�M�@~@t1@l(�@e@_�@V�R@N�y@Fȴ@<��@5�@,��@)X@#ƨ@�D@�@��@9X@�9@{@��@&�?�p�?�E�?��?� �1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�fB
�fB
�mB
�fB
�fB
�fB
�B&�BF�BcTB��B��B��B��B�B��B��B�hB�+B~�B{�Bx�Bq�Bk�BjBgmBaHB[#BVBO�BJ�BF�B:^B6FB1'B-B+B)�B%�B"�B!�B�BbBDBB
�B
�B
ȴB
�9B
��B
��B
�+B
t�B
gmB
S�B
:^B
�B
B	�B	��B	�jB	��B	�PB	|�B	gmB	Q�B	@�B	)�B	"�B	\B�B�PBiyBH�B)�B"�B�B�B'�B1'B?}BYBk�B�+B�3B�qB��B�/B��B	VB	$�B	C�B	VB	n�B	�1B	��B	�?B	��B	�B	�B
B
PB
�B
)�B
49B
D�B
R�B
[#B
`BB
gmB
jB
r�B
z�B
|�B
~�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�fB
�fB
�mB
�fB
�fB
�fB
�HB-BL�BiyB��B��B�B�!B�B��B��B�uB�7B~�B{�By�Br�Bk�BjBhsBbNB\)BW
BP�BL�BH�B<jB7LB2-B-B+B)�B&�B"�B!�B�BbBDBB
�B
�#B
ɺB
�9B
��B
��B
�1B
u�B
hsB
T�B
<jB
�B
%B	�B	��B	�qB	��B	�VB	}�B	hsB	R�B	B�B	+B	#�B	hB�#B�VBjBJ�B+B#�B�B�B'�B1'B@�BYBk�B�1B�3B�qB��B�/B��B	VB	$�B	C�B	VB	n�B	�1B	��B	�?B	��B	�B	�B
B
PB
�B
)�B
49B
D�B
R�B
[#B
`BB
gmB
jB
r�B
z�B
|�B
~�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;��
;��
;��
;��
;��
;��
<u;��$;��$;��$;��
;��|;��<��';��;��;��|;��|;��|;��
;��
;��;��;��
;��
;��;��;��;��;��;��|;��|;��|;��;��;��
;��
;��
;��;��
;��
;��;��
;��
;��;��;��;��;��
;��
;��;��;��;��;��;��|;��;��;��|;��;��;��;��;��;��;��;��|;��;��;��|;��|;��;��;��|;��;��;��;��;��
;��
;��;��
;��
;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.0, NextCycleSSP=0.0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20071021092742              20071021092742                            20071021094252                            20080811000000  JA  ARFMdecpA5_a                                                                20071008045026  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071008045029  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071008045030  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071008045033  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071008045034  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071008045034  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071008050438                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20071012035242  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071012035246  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071012035247  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071012035251  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071012035251  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071012035251  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071012051921                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071021092742  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071021092742  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071021094252  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080811000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20080826063055  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20080826081058                      G�O�G�O�G�O�                