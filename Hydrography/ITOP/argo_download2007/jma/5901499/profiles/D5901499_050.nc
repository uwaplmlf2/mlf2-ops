CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901499 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               2A   JA  20070908045041  20080826080950  A5_24108_050                    2C  D   APEX-SBE 1714                                                   846 @ԓ3�qf1   @ԓ4OW�@9�fffff@b�XbM�1   ARGOS   A   A   A   @�ffA33Ad��A���A�  A陚B
  B33B3��BE��BXffBm33B���B���B���B�  B���B�  B���B�ffBЙ�Bڙ�B���B���B�33C33C�CffC� CL�C�CffC$L�C)ffC.33C3L�C7��C=L�CA�3CG  CQ33CZ�fCe�Co33CyL�C�� C��fC���C���C�s3C�� C��3C��fC���C��fC�s3C���C���C�� CǦfČ�CѦfCֳ3Cۀ C�3C�3C� CC� C���D�3D� DٚD�3D� D� D�3D$�3D)�3D.� D3�fD8��D=�3DBٚDG��DL�3DQ�3DV��D[�3D`� De��Dj� Do�fDt�fDy�fD�&fD�i�D���D��D�#3D�p D��3D��fD�  D�i�D���D���D��D�i�Dڜ�D�� D��D�l�D�D�&f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA33Ad��A���A�  A陚B
  B33B3��BE��BXffBm33B���B���B���B�  B���B�  B���B�ffBЙ�Bڙ�B���B���B�33C33C�CffC� CL�C�CffC$L�C)ffC.33C3L�C7��C=L�CA�3CG  CQ33CZ�fCe�Co33CyL�C�� C��fC���C���C�s3C�� C��3C��fC���C��fC�s3C���C���C�� CǦfČ�CѦfCֳ3Cۀ C�3C�3C� CC� C���D�3D� DٚD�3D� D� D�3D$�3D)�3D.� D3�fD8��D=�3DBٚDG��DL�3DQ�3DV��D[�3D`� De��Dj� Do�fDt�fDy�fD�&fD�i�D���D��D�#3D�p D��3D��fD�  D�i�D���D���D��D�i�Dڜ�D�� D��D�l�D�D�&f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�$�Aݝ�A܃A�bA���AۑhA�A�A�bA�;dA�^5A���A�\)A�  A��A��7A�?}A���A���A��A�n�A��DA��A��A��+A��A���A�oA���A�-A��^A�XA��/A�^5A��`A�Q�A���A�^5A��#A�~�A��`A��\A��A�  A�\)A�A�A��A�v�A��A�ĜA�VA�A�p�A��A|�Ax�Asx�AnffAkt�Ag`BA`bAZ��AW�PAQ��AM�mAJ�AFz�A@��A:�+A3��A,�!A �RA�yAA�A��@��@�\@�9X@͡�@Ų-@��@��h@�ff@�v�@��P@�E�@�(�@��@���@�"�@�b@��@~E�@y��@s��@n��@f�R@V��@M?}@E`B@@��@;��@5V@.��@(A�@�@�R@=q@�@
�@ff@��@   ?�j?�r�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�$�Aݝ�A܃A�bA���AۑhA�A�A�bA�;dA�^5A���A�\)A�  A��A��7A�?}A���A���A��A�n�A��DA��A��A��+A��A���A�oA���A�-A��^A�XA��/A�^5A��`A�Q�A���A�^5A��#A�~�A��`A��\A��A�  A�\)A�A�A��A�v�A��A�ĜA�VA�A�p�A��A|�Ax�Asx�AnffAkt�Ag`BA`bAZ��AW�PAQ��AM�mAJ�AFz�A@��A:�+A3��A,�!A �RA�yAA�A��@��@�\@�9X@͡�@Ų-@��@��h@�ff@�v�@��P@�E�@�(�@��@���@�"�@�b@��@~E�@y��@s��@n��@f�R@V��@M?}@E`B@@��@;��@5V@.��@(A�@�@�R@=q@�@
�@ff@��@   ?�j?�r�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�;B
�/B
�)B
�#B
�/B
�;B
�BB
�/B
�BjB�B~�B� B�oB��B��B�oB�Bz�Bl�BbNB[#BVBN�BI�BC�B@�B;dB:^B8RB5?B49B0!B.B/B.B(�B'�B&�B!�B�B�B�B�BDBB
�B
�ZB
�B
��B
��B
�XB
��B
��B
�1B
o�B
YB
L�B
6FB
�B	��B	�B	��B	�}B	�'B	��B	� B	bNB	E�B	"�B�B��B��B{�B[#BC�B8RB7LB33B<jBH�BW
BaHBw�B�bB�BÖB�B�B	B	bB	%�B	0!B	B�B	O�B	w�B	��B	�wB	��B	�ZB	�B
B
bB
�B
'�B
7LB
=qB
G�B
R�B
\)B
dZB
k�B
p�B
t�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�BB
�5B
�)B
�#B
�/B
�;B
�BB
�5B
�sBr�B�B�B�B��B�B��B��B�B|�Bo�BcTB\)BW
BN�BJ�BD�BA�B;dB:^B8RB5?B49B0!B/B/B/B(�B'�B'�B!�B�B�B�B�BJBB
�B
�`B
�#B
��B
��B
�^B
�B
��B
�7B
p�B
ZB
M�B
8RB
�B	��B	�B	��B	��B	�-B	��B	�B	dZB	G�B	$�B�B��B��B|�B\)BD�B9XB8RB49B=qBI�BW
BbNBw�B�hB�BÖB�B�B	B	bB	%�B	0!B	B�B	O�B	w�B	��B	�wB	��B	�ZB	�B
B
bB
�B
'�B
7LB
=qB
G�B
R�B
\)B
dZB
k�B
p�B
t�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;��;��;��
;��
;��
;��
;��
;��<t!<u;��|;��$;��;ѷ;�;��;ѷ;��;��|;��;��;��;��;��
;��;��;��;��
;��
;��
;��
;��
;��
;��;��
;��;��
;��
;��;��
;��
;��
;��
;��;��;��;��;��;��;��;��
;��;��;��;��;��;��;��;��|;��;��;��;��;��;��;��|;��|;��|;��|;��|;��;��;��;��;��;��;��;��;��;��;��;��
;��;��
;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.2, NextCycleSSP=0.0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20070921104805              20070921104805                            20070921105526                            20080811000000  JA  ARFMdecpA5_a                                                                20070908045038  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070908045041  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070908045042  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070908045046  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070908045046  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070908045046  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070908050318                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070912034949  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070912034953  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070912034954  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070912034958  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070912034958  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070912034959  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070912034959  CV  TIME            G�O�G�O�F���                JA  ARGQrelo2.1                                                                 20070912034959  CV  LAT$            G�O�G�O�A�33                JA  ARGQrelo2.1                                                                 20070912034959  CV  LON$            G�O�G�O�C��                JA  ARUP                                                                        20070912045909                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070913175744  CV  LAT$            G�O�G�O�A�9X                JM  ARGQJMQC1.0                                                                 20070913175744  CV  LON$            G�O�G�O�C�                JM  ARCAJMQC1.0                                                                 20070921104805  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070921104805  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070921105526  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080811000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20080826063053  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20080826080950                      G�O�G�O�G�O�                