CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900809 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               _A   JA  20070902010733  20090916045202  A5_23633_095                    2C  D   APEX-SBE 1558                                                   846 @ԑ�(�6�1   @ԑ���	w@6ݲ-V@_�^5?|�1   ARGOS   A   A   A   @�ffA  Ah  A�33A�  A�ffB33B��B1��BF��BY��Bn��B�33B���B�  B�33B�  B�ffB�ffB�  B�  B�ffB�  B�  B���C33CL�C� CL�C� CL�C�C$�C)L�C.��C3L�C8� C=L�CB33CG  CQ� C[L�CeffCoffCyL�C�� C��fC��fC��fC�� C��3C�s3C�� C��fC���C�ffC��3C��fC�� Cǀ C̳3Cр C֦fCۙ�C�� C噚C�3C�fC���C��3D� D� DٚD�3D�3D�fDٚD$ٚD)��D.�3D3�3D8�fD=��DB��DG�fDL�3DQ��DV��D[��D`�3DeٚDj�fDo�fDt�fDy��D�&fD�l�D��3D�� D�)�D�ffD�� D��D�)�D�` D���D��3D�)�D�i�Dڣ3D��fD�  D�i�D��D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA  Ah  A�33A�  A�ffB33B��B1��BF��BY��Bn��B�33B���B�  B�33B�  B�ffB�ffB�  B�  B�ffB�  B�  B���C33CL�C� CL�C� CL�C�C$�C)L�C.��C3L�C8� C=L�CB33CG  CQ� C[L�CeffCoffCyL�C�� C��fC��fC��fC�� C��3C�s3C�� C��fC���C�ffC��3C��fC�� Cǀ C̳3Cр C֦fCۙ�C�� C噚C�3C�fC���C��3D� D� DٚD�3D�3D�fDٚD$ٚD)��D.�3D3�3D8�fD=��DB��DG�fDL�3DQ��DV��D[��D`�3DeٚDj�fDo�fDt�fDy��D�&fD�l�D��3D�� D�)�D�ffD�� D��D�)�D�` D���D��3D�)�D�i�Dڣ3D��fD�  D�i�D��D�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�ĜA�9XA��A��A��A�%A�p�A��
A���A�I�A���A͝�A���A�C�A�n�A��A�Q�A��A�5?A�z�A��yA�oA�(�A�t�A�7LA���A�`BA��A�ƨA��PA��A���A���A��A�M�A�z�A���A�bNA��RA�jA�M�A�M�A�Q�A���A���A�7LA�VA�/A��A�=qA�5?A��yA��A�ȴA��A�/A}XAx�Ar��Ao�Ai?}Ae\)A^��AY�7AU&�AN��AL�HAE33A?�^A;\)A3�A+%A �/A�`A�A��@��R@��@޸R@�7L@�J@ÍP@���@�{@��h@��@���@�
=@��@�I�@�C�@�G�@�/@�@~{@o�w@a��@W�@M�@G�P@?�@9��@3"�@.V@(Ĝ@"��@S�@��@��@  @��@	�#@��@�/@�71111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�ĜA�9XA��A��A��A�%A�p�A��
A���A�I�A���A͝�A���A�C�A�n�A��A�Q�A��A�5?A�z�A��yA�oA�(�A�t�A�7LA���A�`BA��A�ƨA��PA��A���A���A��A�M�A�z�A���A�bNA��RA�jA�M�A�M�A�Q�A���A���A�7LA�VA�/A��A�=qA�5?A��yA��A�ȴA��A�/A}XAx�Ar��Ao�Ai?}Ae\)A^��AY�7AU&�AN��AL�HAE33A?�^A;\)A3�A+%A �/A�`A�A��@��R@��@޸R@�7L@�J@ÍP@���@�{@��h@��@���@�
=@��@�I�@�C�@�G�@�/@�@~{@o�w@a��@W�@M�@G�P@?�@9��@3"�@.V@(Ĝ@"��@S�@��@��@  @��@	�#@��@�/@�71111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	��B	�jB	�XB	�XB	��B	��B	�RB
6FB
W
B
�hB
�?B
�sBB�B+B>wBI�BG�BW
BZBVBW
BS�BW
B[#B_;B`BBffBaHBbNB]/BW
B[#B^5BbNB^5B`BB^5BZBXBe`B_;BXBK�BG�B?}B7LB)�B�BhB+B
��B
�B
�`B
��B
�}B
��B
�PB
q�B
^5B
>wB
)�B
JB	�B	�;B	�}B	�9B	��B	�B	n�B	O�B	,B	B�#B�XB��B��B�=B�B�7B��B��B�^BB�B�B��B	PB	�B	#�B	5?B	G�B	[#B	q�B	�B	��B	ŢB	�B	�`B	�B	��B
PB
�B
#�B
/B
8RB
D�B
J�B
Q�B
W
B
]/B
dZB
m�B
r�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	�jB	�XB	�^B	��B	��B	�jB
9XB
]/B
��B
�^B
�B1B�B,B@�BI�BI�BYB[#BXB[#BVBYB]/BbNBbNBgmBbNBcTB^5BXB\)B_;BcTB_;BaHB_;BZBYBffB`BBYBK�BH�B@�B8RB+B �BoB1B
��B
�B
�fB
��B
��B
��B
�VB
r�B
_;B
?}B
,B
PB	��B	�HB	��B	�FB	��B	�B	o�B	P�B	-B	B�/B�^B��B��B�DB�B�=B��B��B�dBÖB�B�B��B	PB	�B	#�B	5?B	G�B	[#B	q�B	�B	��B	ŢB	�B	�`B	�B	��B
PB
�B
#�B
/B
8RB
D�B
J�B
Q�B
W
B
]/B
dZB
m�B
r�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709130801172007091308011720070913080117200709130816132007091308161320070913081613200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20070902010712  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070902010733  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070902010737  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070902010746  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070902010747  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.7c                                                                20070902010747  QCF$                G�O�G�O�G�O�             140JA  ARGQrqcpt16a                                                                20070902010748  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070902024820                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070906034750  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070906034755  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070906034756  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070906034800  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070906034800  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070906034800  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070906045133                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070906034750  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090413010122  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090413010123  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090413010123  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090413010124  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090413010124  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090413010124  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090413010124  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090413010125  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090413011412                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070905112350  CV  DAT$            G�O�G�O�F��q                JM  ARCAJMQC1.0                                                                 20070913080117  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070913080117  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070913081613  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045105  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045202                      G�O�G�O�G�O�                