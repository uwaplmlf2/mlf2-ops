CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900809 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               `A   JA  20070912065422  20090916045208  A5_23633_096                    2C  D   APEX-SBE 1558                                                   846 @Ԕ/:�1   @Ԕ2QY�q@6��l�C�@_�bM��1   ARGOS   A   A   A   @���A33Aa��A�ffA�33A���B
  B  B2  BF��BZffBnffB�  B���B�  B�ffB�  B�33B�33B���B���B���B�ffB�  B���C�C�fCL�CffCffC��C33C$� C)ffC.�C3L�C833C=33CB��CG��CQ33C[��Ce33Co33Cy  C���C��fC��fC�� C�� C�ffC���C��3C��3C��fC���C��fC���C¦fCǳ3Č�C�s3Cֳ3Cۀ C���C�� C��CC��3C��3DٚD��DٚD�fD� D�3DٚD$��D)ٚD.� D3��D8� D=�fDB�3DG� DL� DQ��DV�fD[�3D`�fDe� Dj��Do��Dt��Dy�3D�&fD�l�D���D���D�,�D�l�D�� D�� D�&fD�i�D���D��D�)�D�i�Dڬ�D��D��D�ffD�fD�S31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33Aa��A�ffA�33A���B
  B  B2  BF��BZffBnffB�  B���B�  B�ffB�  B�33B�33B���B���B���B�ffB�  B���C�C�fCL�CffCffC��C33C$� C)ffC.�C3L�C833C=33CB��CG��CQ33C[��Ce33Co33Cy  C���C��fC��fC�� C�� C�ffC���C��3C��3C��fC���C��fC���C¦fCǳ3Č�C�s3Cֳ3Cۀ C���C�� C��CC��3C��3DٚD��DٚD�fD� D�3DٚD$��D)ٚD.� D3��D8� D=�fDB�3DG� DL� DQ��DV�fD[�3D`�fDe� Dj��Do��Dt��Dy�3D�&fD�l�D���D���D�,�D�l�D�� D�� D�&fD�i�D���D��D�)�D�i�Dڬ�D��D��D�ffD�fD�S31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A靲A雦A靲A陚A�-A��#A�v�A�5?A�E�A�(�A��A�=qA�bA�A�A��A���A���A��#A�VA��!A�(�A�t�A���A��A�A�A�t�A�jA�{A��!A��A��HA�O�A�v�A��A���A�v�A�;dA�n�A�G�A�C�A�~�A��A�O�A��A�n�A��mA�jA��\A�/A�~�A�$�A���A�5?A�;dA�C�A���A���A�33Ay�Au
=AnM�Ah�yAfr�A`�A]+AW|�AS�
ANjADffA>JA/x�A&��A ��AAK�A	�#AO�@�&�@�|�@���@�^5@̬@�x�@�S�@��F@�@��
@�?}@��P@�Z@��\@�{@��T@��@�t�@{��@p�u@a��@Y�@N�R@D��@>��@8bN@0A�@(  @!��@t�@��@�@ �@�@	��@�+@�@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A靲A雦A靲A陚A�-A��#A�v�A�5?A�E�A�(�A��A�=qA�bA�A�A��A���A���A��#A�VA��!A�(�A�t�A���A��A�A�A�t�A�jA�{A��!A��A��HA�O�A�v�A��A���A�v�A�;dA�n�A�G�A�C�A�~�A��A�O�A��A�n�A��mA�jA��\A�/A�~�A�$�A���A�5?A�;dA�C�A���A���A�33Ay�Au
=AnM�Ah�yAfr�A`�A]+AW|�AS�
ANjADffA>JA/x�A&��A ��AAK�A	�#AO�@�&�@�|�@���@�^5@̬@�x�@�S�@��F@�@��
@�?}@��P@�Z@��\@�{@��T@��@�t�@{��@p�u@a��@Y�@N�R@D��@>��@8bN@0A�@(  @!��@t�@��@�@ �@�@	��@�+@�@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�=B	�DB	�DB	�=B	�%B	��B	ȴB
B
 �B
�VB
�}B
��BBhBF�BF�BO�BXB\)B\)B]/B^5B_;B_;BaHB`BBcTBdZBhsBm�Bn�Bl�Bo�Bp�Bo�Bm�BgmBe`BdZBhsB]/B]/B^5B[#BW
BO�BJ�B@�B9XB49B)�B�BhB+B
��B
�TB
��B
�?B
�\B
x�B
XB
>wB
0!B
�B
B	�yB	�B	�qB	�oB	t�B	=qB	�B		7B��B��B�-B��B��B�=B�B�B�DB��B�dBȴB�`B�B	B	uB	!�B	/B	9XB	E�B	W
B	ffB	�=B	��B	�qB	�
B	�`B	��B
B
VB
�B
+B
49B
A�B
I�B
O�B
W
B
]/B
dZB
l�B
r�B
v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�=B	�DB	�DB	�DB	�%B	��B	ɺB
B
(�B
�uB
ĜB
��B+B�BI�BJ�BS�BZB\)B\)B^5B`BB`BB`BBbNBbNBcTBdZBjBn�Bp�Bn�Bq�Bq�Bp�Bo�BhsBffBe`BiyB^5B]/B_;B\)BXBP�BK�BA�B9XB5?B+B�BhB1B
��B
�ZB
��B
�LB
�bB
z�B
YB
?}B
1'B
�B
B	�B	�B	��B	�{B	v�B	>wB	�B	
=B��B��B�3B��B��B�DB�B�B�JB��B�jBȴB�`B�B	B	uB	!�B	/B	9XB	E�B	W
B	ffB	�=B	��B	�qB	�
B	�`B	��B
B
VB
�B
+B
49B
A�B
I�B
O�B
W
B
]/B
dZB
l�B
r�B
v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709250249542007092502495420070925024954200709250300132007092503001320070925030013200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20070912065419  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070912065422  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070912065423  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070912065427  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070912065427  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070912065428  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070912071209                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070916040657  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070916040713  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070916040718  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070916040726  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070916040727  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070916040728  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070916040728  CV  TIME            G�O�G�O�                    JA  ARGQrelo2.1                                                                 20070916040728  CV  LAT$            G�O�G�O�A��/                JA  ARGQrelo2.1                                                                 20070916040728  CV  LON$            G�O�G�O�B���                JA  ARUP                                                                        20070916043452                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070916040657  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090413010125  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090413010125  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090413010126  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090413010127  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090413010127  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090413010127  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090413010127  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090413010127  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090413011417                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070920001525  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20070925024954  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070925024954  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070925030013  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045115  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045208                      G�O�G�O�G�O�                