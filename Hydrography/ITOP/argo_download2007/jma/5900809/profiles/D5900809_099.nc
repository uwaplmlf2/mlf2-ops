CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900809 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               cA   JA  20071012010417  20090916045146  A5_23633_099                    2C  D   APEX-SBE 1558                                                   846 @ԛ�4��1   @ԛ���m�@7q���l�@_޸Q�1   ARGOS   A   A   A   @�33A��Ad��A�  A���A�ffB	33B��B133BE33BZ��Bm��B���B���B�33B�  B�ffB�  B�ffB�ffB�ffB�33B�33B�ffB�33C33CffC33CffC33C  CffC$�C)  C.�C3ffC8�C<�fCB�CG� CQ� C[ffCe� Co33CyL�C�� C���C�s3C���C�s3C�s3C��fC�ffC���C���C��fC�� C���C�C�s3Č�Cр Cր CۦfC�ffC噚C�fC�� C�� C��3D�fD� DٚD� D� DٚD� D$ٚD)ٚD.�3D3��D8ٚD=� DB�3DG��DL�fDQ��DVٚD[� D`ٚDe�3Dj��Do�fDt�fDy� D�)�D�c3D���D��3D�,�D�i�D���D���D�)�D�c3D���D��D�&fD�p Dڣ3D���D�#3D�l�D� D�s31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��Ad��A�  A���A�ffB	33B��B133BE33BZ��Bm��B���B���B�33B�  B�ffB�  B�ffB�ffB�ffB�33B�33B�ffB�33C33CffC33CffC33C  CffC$�C)  C.�C3ffC8�C<�fCB�CG� CQ� C[ffCe� Co33CyL�C�� C���C�s3C���C�s3C�s3C��fC�ffC���C���C��fC�� C���C�C�s3Č�Cр Cր CۦfC�ffC噚C�fC�� C�� C��3D�fD� DٚD� D� DٚD� D$ٚD)ٚD.�3D3��D8ٚD=� DB�3DG��DL�fDQ��DVٚD[� D`ٚDe�3Dj��Do�fDt�fDy� D�)�D�c3D���D��3D�,�D�i�D���D���D�)�D�c3D���D��D�&fD�p Dڣ3D���D�#3D�l�D� D�s31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�+A�bNAՓuA�=qA���AԾwA�I�A�"�A��#Aӧ�A��mA���A��TAăA�-A��A��A�+A��A��A���A�r�A�I�A�G�A��PA��+A�=qA�VA��A�
=A�hsA�x�A���A��hA�bA���A���A��uA���A��;A�jA��A�A���A���A�JA�$�A��HA�=qA��A�n�A���A��A{�hAx1AuC�Ap�Aj�/Af��AcVA\A�AXv�AS��AN��AFn�ABM�A=�hA8��A2 �A+��A#��A33A^5A
ȴA�\@���@���@�dZ@�K�@�X@�7L@��@�S�@���@���@�S�@�X@���@��@�G�@��R@�9X@��j@�@��w@w\)@n$�@b^5@Y�^@P �@H�u@@Ĝ@7�@.$�@)�#@%�-@!��@�@�@?}@�^@�T@o@��@o1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�+A�bNAՓuA�=qA���AԾwA�I�A�"�A��#Aӧ�A��mA���A��TAăA�-A��A��A�+A��A��A���A�r�A�I�A�G�A��PA��+A�=qA�VA��A�
=A�hsA�x�A���A��hA�bA���A���A��uA���A��;A�jA��A�A���A���A�JA�$�A��HA�=qA��A�n�A���A��A{�hAx1AuC�Ap�Aj�/Af��AcVA\A�AXv�AS��AN��AFn�ABM�A=�hA8��A2 �A+��A#��A33A^5A
ȴA�\@���@���@�dZ@�K�@�X@�7L@��@�S�@���@���@�S�@�X@���@��@�G�@��R@�9X@��j@�@��w@w\)@n$�@b^5@Y�^@P �@H�u@@Ĝ@7�@.$�@)�#@%�-@!��@�@�@?}@�^@�T@o@��@o1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
{B
'�B
&�B
&�B
'�B
&�B
&�B
%�B
'�B
+B
33B
J�B
�B
ĜBP�BW
B^5Bo�Bp�Bt�Bu�Bv�Bw�Bw�B}�B�B�B��B�7B�B~�B~�Bz�By�Bw�Bs�Bo�Bk�BiyBffB`BBXBQ�BD�B7LB)�B�BJB
��B
�sB
�B
ǮB
�-B
��B
�7B
x�B
`BB
D�B
0!B
�B	��B	�mB	��B	�LB	�hB	� B	jB	VB	:^B	 �B	B�yB��B�qB�B��B�JB�B�%B�DB��B�B�jB��B�`B��B	DB	�B	#�B	-B	<jB	F�B	YB	k�B	t�B	��B	�B	ƨB	�
B	�sB	��B	��B
bB
�B
)�B
49B
<jB
B�B
H�B
P�B
[#B
e`B
k�B
u�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�B
(�B
&�B
&�B
'�B
&�B
&�B
%�B
'�B
,B
5?B
N�B
�DB
��BQ�BXBaHBp�Bq�Bu�Bw�Bx�Bx�By�B~�B�B�B��B�=B�+B� B� B{�Bz�Bx�Bt�Bp�Bl�BjBgmBaHBXBR�BE�B8RB+B�BPB
��B
�yB
�B
ȴB
�3B
��B
�=B
y�B
bNB
E�B
1'B
�B	��B	�sB	��B	�XB	�oB	�B	k�B	XB	<jB	!�B	B�B��B�wB�!B��B�PB�B�+B�JB��B�B�jB��B�`B��B	DB	�B	#�B	-B	<jB	F�B	YB	k�B	t�B	��B	�B	ƨB	�
B	�sB	��B	��B
bB
�B
)�B
49B
<jB
B�B
H�B
P�B
[#B
e`B
k�B
u�B
{�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710250224002007102502240020071025022400200710250234292007102502342920071025023429200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20071012010401  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071012010417  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071012010421  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071012010428  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071012010429  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071012010430  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071012014636                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20071016034748  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071016034753  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071016034753  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071016034757  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071016034757  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071016034758  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071016050718                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20071016034748  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090413010133  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090413010133  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090413010134  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090413010135  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090413010135  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090413010135  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090413010135  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090413010135  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090413011424                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071015105709  CV  DAT$            G�O�G�O�F��H                JM  ARCAJMQC1.0                                                                 20071025022400  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071025022400  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071025023429  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045036  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045146                      G�O�G�O�G�O�                