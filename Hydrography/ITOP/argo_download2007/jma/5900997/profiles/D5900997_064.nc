CDF   %   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900997 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               @A   JA  20071016185346  20090916045602  A9_60126_064                    2C  D   APEX-SBE 2360                                                   846 @Ԝ�K��1   @Ԝ�bj�p@:�ȴ9X@a�+I�1   ARGOS   A   A   A   @�33A33Ac33A���A�ffA���B
ffB��B2ffBF��BZ  Bo33B�33B���B�33B�33B���B�33B�  B�ffB���Bڙ�B䙚B�ffB���C�C  C  CL�C33C�fC�fC$33C(��C.L�C3�C8  C<�3CB33CGL�CP�fC[�3Ce� CoL�CyL�C���C�s3C���C�� C��3C�� C��fC�� C���C�� C�� C�� C��fC���Cǳ3C̀ Cѳ3C֌�Cی�C�fC� CꙚC�ffC��fC���D�3DٚD� D�3D� D�3D� D$�3D)�fD.� D3��D8�3D=��DB�fDGٚDL� DQ�fDV� D[�3D`�3DeٚDj�3Do�3Dt� DyٚD�#3D�l�D��3D��fD�,�D�l�D��fD�� D�,�D�p D���D��fD�)�D�ffDڦfD��3D��D�Y�D��D� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A33Ac33A���A�ffA���B
ffB��B2ffBF��BZ  Bo33B�33B���B�33B�33B���B�33B�  B�ffB���Bڙ�B䙚B�ffB���C�C  C  CL�C33C�fC�fC$33C(��C.L�C3�C8  C<�3CB33CGL�CP�fC[�3Ce� CoL�CyL�C���C�s3C���C�� C��3C�� C��fC�� C���C�� C�� C�� C��fC���Cǳ3C̀ Cѳ3C֌�Cی�C�fC� CꙚC�ffC��fC���D�3DٚD� D�3D� D�3D� D$�3D)�fD.� D3��D8�3D=��DB�fDGٚDL� DQ�fDV� D[�3D`�3DeٚDj�3Do�3Dt� DyٚD�#3D�l�D��3D��fD�,�D�l�D��fD�� D�,�D�p D���D��fD�)�D�ffDڦfD��3D��D�Y�D��D� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�r�A��Aڏ\A�\)A؁A�M�A׶FA�7LA�S�A���A�bA�t�A�\)A�;dA���A��A���A�v�A�5?A��^A�dZA�l�A��A�C�A��FA�|�A�A�A�ƨA���A���A��-A��A�VA�(�A�$�A��yA��A���A��+A�jA���A�1A��A��mA��9A�5?A�
=A�=qA�"�A�A���A���A~^5A}�Azz�Aw`BAsoAm"�AfI�A^��AXE�AQ��AJAFA�A?�A<  A533A.I�A)�A$r�A%A�A��A�#@�@�{@ͺ^@š�@�ff@�O�@�X@��@�@�o@���@�;d@�Q�@�/@�=q@�|�@y��@sS�@k��@i�#@e��@W�;@S��@G+@B��@<z�@1��@'+@!��@�@��@�P@r�@Z@�P@�D@=q?���?�x�?�9X?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�r�A��Aڏ\A�\)A؁A�M�A׶FA�7LA�S�A���A�bA�t�A�\)A�;dA���A��A���A�v�A�5?A��^A�dZA�l�A��A�C�A��FA�|�A�A�A�ƨA���A���A��-A��A�VA�(�A�$�A��yA��A���A��+A�jA���A�1A��A��mA��9A�5?A�
=A�=qA�"�A�A���A���A~^5A}�Azz�Aw`BAsoAm"�AfI�A^��AXE�AQ��AJAFA�A?�A<  A533A.I�A)�A$r�A%A�A��A�#@�@�{@ͺ^@š�@�ff@�O�@�X@��@�@�o@���@�;d@�Q�@�/@�=q@�|�@y��@sS�@k��@i�#@e��@W�;@S��@G+@B��@<z�@1��@'+@!��@�@��@�P@r�@Z@�P@�D@=q?���?�x�?�9X?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�B
�NB
�B
�TB
�
B
�B
��B
ŢB
�wB
��B
��B
�yB
��BB(�B&�B33B.B1'B49B5?B.B+B0!B49B5?B33B)�B/B7LB:^B7LB5?B33B+B-B(�B'�B$�B!�B�B"�B"�BbB%B
�B
�yB
�HB
�
B
��B
��B
�?B
��B
��B
�uB
�B
o�B
S�B
33B
hB	�B	�B	�FB	��B	�7B	u�B	]/B	A�B	0!B	�B�BŢB��B��Bz�Br�BVBW
B`BBhsB�=B�\B�B�B�qB��B�B�ZB�B	uB	1'B	@�B	Q�B	T�B	^5B	�B	�VB	�3B	�qB	��B	�fB	��B
B
bB
�B
�B
33B
=qB
I�B
Q�B
YB
`BB
ffB
l�B
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�B
�TB
�B
�ZB
�
B
�B
��B
��B
ĜB
��B
�
B
�B
��BuB)�B(�B49B1'B2-B5?B6FB/B,B1'B49B5?B49B+B/B8RB;dB8RB5?B49B+B-B(�B'�B$�B"�B�B#�B#�BhB+B
��B
�yB
�NB
�B
��B
��B
�FB
��B
��B
�{B
�B
p�B
VB
5?B
uB	��B	�B	�LB	��B	�=B	w�B	_;B	B�B	1'B	�B�BƨB��B��B{�Bs�BW
BXBaHBiyB�=B�bB�B�B�qB��B�B�ZB�B	{B	1'B	@�B	Q�B	T�B	^5B	�B	�VB	�3B	�qB	��B	�fB	��B
B
bB
�B
�B
33B
=qB
I�B
Q�B
YB
`BB
ffB
l�B
p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<u<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710292023252007102920232520071029202325200710292026572007102920265720071029202657200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20071016185344  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071016185346  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071016185346  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071016185347  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071016185351  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071016185351  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071016185351  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071016185351  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071016185352  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071016190624                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071020161954  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071020162009  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071020162012  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071020162014  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071020162023  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071020162023  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071020162023  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071020162023  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071020162024  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071020205400                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071020161954  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519065411  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519065412  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519065412  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519065412  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519065413  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519065413  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519065414  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519065414  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519065414  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071005                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071029202325  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071029202325  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071029202657  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045520  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045602                      G�O�G�O�G�O�                