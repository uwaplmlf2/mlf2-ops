CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900997 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               >A   JA  20070926185426  20090916045608  A9_60126_062                    2C  D   APEX-SBE 2360                                                   846 @ԗ����1   @ԗ�`��@:�=p��
@a���l�1   ARGOS   A   A   A   @�33A33AfffA�33A�ffA�  B��B33B2��BFffBY33BnffB���B�  B���B�  B�ffB�  B�  B�33BЙ�Bڙ�B�  B�  B���CffC33C  CL�C  CL�C33C$L�C)  C.  C3�C8L�C<��CB33CF�fCQ�C[�Ce  CoffCyL�C�ffC���C��3C��3C��3C��fC�s3C��fC�� C��fC�s3C���C��fC�� CǙ�C̦fCь�C֌�Cۀ C�3C���C�3C���C��3C��fDٚD�fD�fD�3D��D��D� D$��D)� D.� D3��D8ٚD=ٚDBٚDGٚDL� DQٚDV� D[� D`��De� Dj� Do� Dt��Dy�fD�  D�c3D���D���D�&fD�ffD��fD���D�&fD�i�D��fD�� D�&fD�c3Dڠ D�� D�  D�ffD�fD�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A33AfffA�33A�ffA�  B��B33B2��BFffBY33BnffB���B�  B���B�  B�ffB�  B�  B�33BЙ�Bڙ�B�  B�  B���CffC33C  CL�C  CL�C33C$L�C)  C.  C3�C8L�C<��CB33CF�fCQ�C[�Ce  CoffCyL�C�ffC���C��3C��3C��3C��fC�s3C��fC�� C��fC�s3C���C��fC�� CǙ�C̦fCь�C֌�Cۀ C�3C���C�3C���C��3C��fDٚD�fD�fD�3D��D��D� D$��D)� D.� D3��D8ٚD=ٚDBٚDGٚDL� DQٚDV� D[� D`��De� Dj� Do� Dt��Dy�fD�  D�c3D���D���D�&fD�ffD��fD���D�&fD�i�D��fD�� D�&fD�c3Dڠ D�� D�  D�ffD�fD�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�+A�A߼jA��A���Aߣ�A��A�{AɃA�r�A��yA���A��A���A�  A��9A�;dA�S�A��A�7LA���A�ffA��A��wA���A�"�A���A�XA���A�x�A�O�A�7LA���A���A�|�A��uA�^5A��A�I�A���A���A��A�|�A��uA���A�9XA�l�A���A��;A�O�A�"�A�33A�A|��Ay�wAu�As�Ao��Ai�
Agx�Ab��A_oAY33AX~�AO��AJ$�AES�A>$�A7\)A6��A*�uA"v�AƨA	�-A b@�-@ܣ�@ӶF@�V@��j@�K�@���@��@�G�@���@��@�@���@�|�@��@�z�@y%@tz�@m�h@h��@\(�@L�j@C��@?��@8��@2J@,�@%��@ b@�@�@o@&�@C�@ƨ@ Ĝ?��H?�1'?�o?�v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�+A�A߼jA��A���Aߣ�A��A�{AɃA�r�A��yA���A��A���A�  A��9A�;dA�S�A��A�7LA���A�ffA��A��wA���A�"�A���A�XA���A�x�A�O�A�7LA���A���A�|�A��uA�^5A��A�I�A���A���A��A�|�A��uA���A�9XA�l�A���A��;A�O�A�"�A�33A�A|��Ay�wAu�As�Ao��Ai�
Agx�Ab��A_oAY33AX~�AO��AJ$�AES�A>$�A7\)A6��A*�uA"v�AƨA	�-A b@�-@ܣ�@ӶF@�V@��j@�K�@���@��@�G�@���@��@�@���@�|�@��@�z�@y%@tz�@m�h@h��@\(�@L�j@C��@?��@8��@2J@,�@%��@ b@�@�@o@&�@C�@ƨ@ Ĝ?��H?�1'?�o?�v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�)B
z�B
��B�B �B �B&�B�B
�yB
�B
�;B
�mB
�yB
��B
��BB%BBoB{B�B�B�B�B�B'�B$�B$�B"�B#�B#�B!�B �B!�B"�B!�B�B!�B!�B#�B�B�B �B�B�BuB1BB
�B
�NB
��B
B
��B
��B
�PB
z�B
o�B
]/B
C�B
8RB
#�B
hB	��B	�B	��B	�RB	��B	� B	e`B	]/B	)�B	%B�HB��B~�Bk�BbNBZB\)B_;BgmBq�B�B�{B��B�B��B�sB��B	oB	�B	8RB	E�B	P�B	YB	q�B	��B	�B	�XB	ɺB	�)B	�yB	��B

=B
oB
�B
+B
1'B
?}B
Q�B
ZB
bNB
ffB
m�B
s�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�)B
y�B
��B�B �B!�B(�B/B
�B
�;B
�HB
�B
�B
��BB%B+B%BuB�B �B�B�B�B �B(�B%�B%�B#�B$�B#�B"�B!�B!�B#�B!�B�B!�B"�B$�B�B�B �B�B�B{B1BB
�B
�TB
��B
ÖB
�B
��B
�VB
{�B
p�B
^5B
D�B
9XB
$�B
oB	��B	��B	��B	�XB	��B	�B	e`B	_;B	+B	+B�TB��B�Bl�BcTB[#B]/B`BBgmBq�B�B�{B��B�B��B�sB��B	oB	�B	8RB	E�B	P�B	YB	q�B	��B	�B	�XB	ɺB	�)B	�yB	��B

=B
oB
�B
+B
1'B
?}B
Q�B
ZB
bNB
ffB
m�B
s�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<�o<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710091948162007100919481620071009194816200710091957372007100919573720071009195737200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20070926185423  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070926185426  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070926185427  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070926185431  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070926185431  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070926185431  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070926190554                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070930155518  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070930155522  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070930155523  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070930155527  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070930155527  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20070930155528  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070930161621                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070930155518  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519065405  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519065406  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519065406  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519065406  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519065408  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519065408  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519065408  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519065408  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519065408  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519070957                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070930044645  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20071009194816  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071009194816  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071009195737  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045529  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045608                      G�O�G�O�G�O�                