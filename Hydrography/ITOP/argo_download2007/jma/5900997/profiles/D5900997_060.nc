CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   p   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4\   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :|   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  A   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C<   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   E�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   N�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   W�   CALIBRATION_DATE      	   
                
_FillValue                  �  `�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a\   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    al   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    ap   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        a�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    a�Argo profile    2.2 1.2 19500101000000  5900997 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               <A   JA  20070906185348  20090916045609  A9_60126_060                    2C  D   APEX-SBE 2360                                                   846 @Ԓ�7��v1   @Ԓ�g���@:��+@a�t�j~�1   ARGOS   A   A   A   @�  A��Ai��A�  A�ffA���B
  B��B2ffBFffBZffBn��B�ffB�33B�ffB�  B�  B�  B�ffB�  B�33B���B�  BB�33C��CL�CL�C� C� CffCL�C$ffC)L�C.L�C3  C8� C=  CBffCG33CP��CZ�3CeL�CoL�Cy  C���C��fC���C���C���C���C���C�s3C���C��fC��fC���C��3C�� C�� C�� Cь�C֙�Cۀ C���C� C�3CC��3C��3D� D��D� DٚDٚD��D�3D$� D)�fD.� D3�fD8��D=�3DB�fDG�fDLٚDQ� DV� D[��D`ٚDe� Dj��Do�fDt� DyٚD��D�i�D���D���D�#3D�l�D���D�� D�,�D�p D��fD���D�  D�ffDڬ�D��fD�\�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@�  A��Ai��A�  A�ffA���B
  B��B2ffBFffBZffBn��B�ffB�33B�ffB�  B�  B�  B�ffB�  B�33B���B�  BB�33C��CL�CL�C� C� CffCL�C$ffC)L�C.L�C3  C8� C=  CBffCG33CP��CZ�3CeL�CoL�Cy  C���C��fC���C���C���C���C���C�s3C���C��fC��fC���C��3C�� C�� C�� Cь�C֙�Cۀ C���C� C�3CC��3C��3D� D��D� DٚDٚD��D�3D$� D)�fD.� D3�fD8��D=�3DB�fDG�fDLٚDQ� DV� D[��D`ٚDe� Dj��Do�fDt� DyٚD��D�i�D���D���D�#3D�l�D���D�� D�,�D�p D��fD���D�  D�ffDڬ�D��fD�\�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�(�A�-A�ĜA�
=A�A�G�A��HA�O�A��#A�/A��;A��TA�1A�1'A���A�p�A��hA���A��+A���A�VA��mA���A�?}A��A�$�A��A�dZA�;dA�|�A��A���A��!A���A��^A�=qA��RA��A�dZA���A�33A��A��yA��9A��wA��TA�(�A�x�A��A�ȴA�
=A���A��
A�A�A���A}��AzA�Av1'Ar�Ak�AhI�AehsAc�^Ac�A`��AV��AS�AMAFr�AAVA6r�A1A#oA�A�
AI�@�&�@�ƨ@�`B@�1@��H@��D@��9@���@��@���@��u@���@�9X@�$�@�S�@���@{��@l��@e`B@bn�@[�m@Ko@<�D@8�`@.��@%�@!G�@hs@/@^5@��@
�\@|�@ b?�ƨ?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111A�(�A�-A�ĜA�
=A�A�G�A��HA�O�A��#A�/A��;A��TA�1A�1'A���A�p�A��hA���A��+A���A�VA��mA���A�?}A��A�$�A��A�dZA�;dA�|�A��A���A��!A���A��^A�=qA��RA��A�dZA���A�33A��A��yA��9A��wA��TA�(�A�x�A��A�ȴA�
=A���A��
A�A�A���A}��AzA�Av1'Ar�Ak�AhI�AehsAc�^Ac�A`��AV��AS�AMAFr�AAVA6r�A1A#oA�A�
AI�@�&�@�ƨ@�`B@�1@��H@��D@��9@���@��@���@��u@���@�9X@�$�@�S�@���@{��@l��@e`B@bn�@[�m@Ko@<�D@8�`@.��@%�@!G�@hs@/@^5@��@
�\@|�@ b?�ƨ?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
��B
��B
��B
�qB
�wB
�wB
ǮB
��B
�/BB"�BB�BM�B_;B]/Bo�Bn�BgmBdZBYBdZB^5BJ�BL�BA�B@�B>wB?}B<jB;dB:^B9XB7LB7LB6FB49B2-B0!B.B.B-B)�B'�B+B+B�B�BoB
��B
�B
�NB
��B
��B
�qB
�XB
��B
�hB
|�B
iyB
J�B
9XB
)�B
$�B
#�B
uB	�yB	�B	�wB	��B	�1B	XB	A�B	DB�BĜB�!B�PB�B�Bp�B�B|�B�B�B�!BȴB�
B�yB��B	1B	�B	�B	.B	O�B	dZB	l�B	|�B	��B	��B	��B	�mB	��B
%B
�B
$�B
,B
33B
A�B
H�B
\)B
bNB
dZ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111B
��B
��B
��B
��B
��B
ĜB
��B
��B
�NB	7B'�BF�BN�B`BB_;Bp�Bo�BhsBffB[#BdZB`BBK�BN�BC�BA�B@�B@�B=qB<jB;dB9XB8RB8RB6FB49B33B0!B/B.B-B+B(�B+B+B�B �BuB  B
��B
�TB
��B
��B
�qB
�^B
��B
�oB
}�B
k�B
K�B
:^B
)�B
$�B
$�B
�B	�B	�)B	��B	��B	�7B	YB	C�B	PB�
BŢB�-B�VB�%B�Bq�B�B}�B�B�B�'BȴB�B�yB��B	1B	�B	�B	/B	O�B	dZB	l�B	|�B	��B	��B	��B	�mB	��B
%B
�B
$�B
,B
33B
A�B
H�B
\)B
bNB
dZ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709192039312007091920393120070919203931200709192055562007091920555620070919205556200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20070906185345  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070906185348  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070906185348  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070906185353  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070906185353  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070906185353  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070906190436                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070910161343  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070910161347  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070910161348  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070910161352  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070910161352  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070910161352  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070910162225                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070910161343  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519065400  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519065400  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519065400  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519065401  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519065402  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519065402  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519065402  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519065402  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519065402  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519070958                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070910055029  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20070919203931  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070919203931  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070919205556  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045530  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045609                      G�O�G�O�G�O�                