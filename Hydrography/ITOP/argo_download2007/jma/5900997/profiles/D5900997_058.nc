CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5900997 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               :A   JA  20070817185427  20090916045610  A9_60126_058                    2C  D   APEX-SBE 2360                                                   846 @ԍ���|1   @ԍ�(�@9�$�/�@a�XbM�1   ARGOS   A   A   A   @�33A��Ac33A�33A���A�ffB33B��B2ffBE��BY��Bl  B���B���B���B�  B���B�ffB���B���B�33B�ffB�  B�33B�  C� C� CffC33C� C�C�fC#�fC)�C-�fC3  C833C<�3CB�CF�3CQ� C[L�CeL�CoL�Cy��C�� C���C�� C��3C��3C��fC���C�� C�� C�� C��3C��3C���C�CǙ�C̳3CѦfC֦fCی�C���C噚C�fC�� C���C���D�fD�3D��D�3D�3D� DٚD$� D)�3D.�3D3� D8��D=�3DB� DG� DLٚDQ� DV�fD[�fD`�fDe��Dj� DoٚDt�fDy�3D�&fD�` D���D�� D�33D�p D��3D��3D�&fD�i�D�� D��D�&fD�l�Dڬ�D��fD�#3D�l�D�fD�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��Ac33A�33A���A�ffB33B��B2ffBE��BY��Bl  B���B���B���B�  B���B�ffB���B���B�33B�ffB�  B�33B�  C� C� CffC33C� C�C�fC#�fC)�C-�fC3  C833C<�3CB�CF�3CQ� C[L�CeL�CoL�Cy��C�� C���C�� C��3C��3C��fC���C�� C�� C�� C��3C��3C���C�CǙ�C̳3CѦfC֦fCی�C���C噚C�fC�� C���C���D�fD�3D��D�3D�3D� DٚD$� D)�3D.�3D3� D8��D=�3DB� DG� DLٚDQ� DV�fD[�fD`�fDe��Dj� DoٚDt�fDy�3D�&fD�` D���D�� D�33D�p D��3D��3D�&fD�i�D�� D��D�&fD�l�Dڬ�D��fD�#3D�l�D�fD�f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�+A�-A��A��A��A�XA�{A�XA���A�O�A�{A���A���A�I�A�XA�XA���A�A�A�XA���A�K�A�M�A��A�G�A��A�=qA���A��RA�ffA��jA���A��wA��9A��hA�jA�  A�I�A�
=A��\A���A�Q�A�Q�A��`A��A�S�A���A���A��hA�VA��A���A��mA~A{��AxM�Ar�Am�7Ai��Af(�A_\)A[+ATZAK��AI�TAB��A?��A:ZA4(�A+�PA*n�A9XAM�A��A��@�5?@�|�@Չ7@�l�@å�@��F@�Z@���@�Ĝ@��j@�V@���@��+@���@���@�ff@���@�J@�@y%@h �@\��@P1'@Hb@A��@:�!@2��@.$�@%�h@V@�!@��@1'@@
�@�R@��@ �u?���?�ff1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�+A�-A��A��A��A�XA�{A�XA���A�O�A�{A���A���A�I�A�XA�XA���A�A�A�XA���A�K�A�M�A��A�G�A��A�=qA���A��RA�ffA��jA���A��wA��9A��hA�jA�  A�I�A�
=A��\A���A�Q�A�Q�A��`A��A�S�A���A���A��hA�VA��A���A��mA~A{��AxM�Ar�Am�7Ai��Af(�A_\)A[+ATZAK��AI�TAB��A?��A:ZA4(�A+�PA*n�A9XAM�A��A��@�5?@�|�@Չ7@�l�@å�@��F@�Z@���@�Ĝ@��j@�V@���@��+@���@���@�ff@���@�J@�@y%@h �@\��@P1'@Hb@A��@:�!@2��@.$�@%�h@V@�!@��@1'@@
�@�R@��@ �u?���?�ff1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
��B
��B
��B
��B
��B
��B
ÖB
��B8RB:^BC�BQ�BO�BT�BL�BR�BO�BS�BP�BB�BA�BB�B=qBA�BB�BA�B>wB>wB=qB<jB8RB6FB5?B5?B49B33B1'B0!B0!B0!B/B-B-B-B)�B �B�BuBB
�B
�
B
ĜB
�XB
��B
��B
�7B
n�B
S�B
B�B
.B
bB	��B	�BB	�jB	�-B	��B	�DB	p�B	Q�B	/B	#�B�B�;B�qB�JB{�Bk�BcTBL�BP�BYBhsBx�B�+B�oB��BǮB�B�BB�B��B	VB	!�B	.B	=qB	`BB	}�B	��B	�XB	ŢB	�B	�B	��B
+B
�B
 �B
/B
8RB
?}B
H�B
O�B
W
B
_;B
ffB
n�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
��B
��B
��B
��B
��B
�-B
��B1B9XB<jBF�BR�BP�BVBO�BS�BQ�BT�BR�BD�BB�BC�B?}BA�BB�BB�B>wB>wB=qB=qB9XB6FB5?B5?B49B33B2-B0!B0!B0!B/B-B.B-B+B �B�B{BB
�B
�B
ŢB
�^B
��B
��B
�=B
o�B
T�B
C�B
0!B
hB	��B	�NB	�qB	�9B	��B	�JB	r�B	S�B	/B	%�B�B�BB�}B�PB}�Bl�BdZBM�BQ�BZBiyBx�B�+B�uB��BǮB�B�BB�B��B	VB	!�B	.B	=qB	`BB	}�B	��B	�XB	ŢB	�B	�B	��B
+B
�B
 �B
/B
8RB
?}B
H�B
O�B
W
B
_;B
ffB
n�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<T��<49X<e`B<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200708302033172007083020331720070830203317200708302035142007083020351420070830203514200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20070817185424  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070817185427  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070817185428  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070817185432  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070817185432  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070817185433  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070817190548                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070821155001  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070821155005  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070821155006  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070821155010  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070821155010  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070821155010  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070821160002                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070821155001  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519065354  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519065355  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090519065355  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519065355  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519065356  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519065356  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519065356  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519065356  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519065357  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519070959                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070821055827  CV  DAT$            G�O�G�O�F�n�                JM  ARCAJMQC1.0                                                                 20070830203317  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070830203317  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070830203514  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916045530  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916045610                      G�O�G�O�G�O�                