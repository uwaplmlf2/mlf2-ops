CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   n   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4T   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6|   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :\   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J�   CALIBRATION_DATE            	             
_FillValue                  ,  M�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N    HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N`   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Np   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Nt   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5900923 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               ^A   JA  20071006125107  20090415042512                                  2B  A   1335                                                            846 @Ԛ��,`1   @Ԛ�|�@5
��n�@`�����1   ARGOS   A   A   A   @���A  Ai��A�  A���A���B
��B��B133BFffBY33Bm��B�  B�  B�  B�ffB���B�ffB�33B�ffB�33B�  B���BB�  CL�C  C  CffC�fC333C8� C=� CBL�CGffCQ�C[33CeL�Cn��Cy33C��fC�� C���C���C���C��fC��3C��fC���C�� C��fC�ffC�Y�C¦fCǦfC̦fCь�C֦fCۦfC�ffC�ffC�fCC��fC�ffD�3D� D�3D��D�3D�3D�3D$��D)��D.��D3ٚD8��D=�3DB�3DG� DL��DQ�3DV��D[�fD`��De�3Dj�3DoٚDt��DyٚD��D�` D��3D��fD��D�Y�D���D���D�#3D�ffD��3D�� D�)�D�ffDڦfD��fD�&fD�|�D��D�i�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���A  Ai��A�  A���A���B
��B��B133BFffBY33Bm��B�  B�  B�  B�ffB���B�ffB�33B�ffB�33B�  B���BB�  CL�C  C  CffC�fC333C8� C=� CBL�CGffCQ�C[33CeL�Cn��Cy33C��fC�� C���C���C���C��fC��3C��fC���C�� C��fC�ffC�Y�C¦fCǦfC̦fCь�C֦fCۦfC�ffC�ffC�fCC��fC�ffD�3D� D�3D��D�3D�3D�3D$��D)��D.��D3ٚD8��D=�3DB�3DG� DL��DQ�3DV��D[�fD`��De�3Dj�3DoٚDt��DyٚD��D�` D��3D��fD��D�Y�D���D���D�#3D�ffD��3D�� D�)�D�ffDڦfD��fD�&fD�|�D��D�i�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�Q�A�O�A�G�A�M�A�XA�?}A�;dA�;dA�(�A�A�O�A���A�A��AɶFAŬA�ZA�XA��PA�O�A��A�x�A�v�A�/A���A��A�bA��HA�G�A�Q�A��A���A��wA� �A�G�A�ffA�&�A�{A� �A�XA���A��-A�-A��A��A��
A��jA�&�A��uA��+A��A~��Az��Ar�9An�`Ai��Agx�A\�DAU�#AN��AK&�AD��A=x�A8v�A0-A�^A��A
�9A ��@��j@�R@�ff@���@�`B@��;@�?}@�`B@�Ĝ@�E�@���@��h@���@��/@�=q@�V@���@�hs@K�@z^5@w�P@g;d@X�9@M�T@F�y@A&�@>{@9x�@3S�@-`B@(A�@#�m@ 1'@O�@�@�u@�h@J@��@��@�911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�Q�A�O�A�G�A�M�A�XA�?}A�;dA�;dA�(�A�A�O�A���A�A��AɶFAŬA�ZA�XA��PA�O�A��A�x�A�v�A�/A���A��A�bA��HA�G�A�Q�A��A���A��wA� �A�G�A�ffA�&�A�{A� �A�XA���A��-A�-A��A��A��
A��jA�&�A��uA��+A��A~��Az��Ar�9An�`Ai��Agx�A\�DAU�#AN��AK&�AD��A=x�A8v�A0-A�^A��A
�9A ��@��j@�R@�ff@���@�`B@��;@�?}@�`B@�Ĝ@�E�@���@��h@���@��/@�=q@�V@���@�hs@K�@z^5@w�P@g;d@X�9@M�T@F�y@A&�@>{@9x�@3S�@-`B@(A�@#�m@ 1'@O�@�@�u@�h@J@��@��@�911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
�B
�B
�B
�B
�B
�B
�B
 �B
"�B
49B
s�B�B}�B��B��B��B��B��B��B�}B�dB�wB�NB�sB�yB�yB�NB�B��B��B�Bv�Bl�BcTB^5BQ�BL�BF�BF�BN�BM�BC�B;dB0!B'�B�BPB
��B
�;B
ƨB
��B
�oB
iyB
VB
;dB
)�B	��B	�B	�3B	��B	�B	hsB	P�B	2-B�B��B��B��B��B��B�{B�hB��B�oB�!B�B�B��B	oB	�B	%�B	49B	O�B	]/B	l�B	�B	�VB	��B	��B	�3B	��B	�B	��B
\B
$�B
0!B
=qB
G�B
P�B
ZB
_;B
dZB
gmB
k�B
o�B
t�B
x�B
{�B
� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B
�B
�B
�B
�B
�B
�B
�B
�B
 �B
"�B
49B
s�B�B}�B��B��B��B��B��B��B�}B�dB�wB�NB�sB�yB�yB�NB�B��B��B�Bv�Bl�BcTB^5BQ�BL�BF�BF�BN�BM�BC�B;dB0!B'�B�BPB
��B
�;B
ƨB
��B
�oB
iyB
VB
;dB
)�B	��B	�B	�3B	��B	�B	hsB	P�B	2-B�B��B��B��B��B��B�{B�hB��B�oB�!B�B�B��B	oB	�B	%�B	49B	O�B	]/B	l�B	�B	�VB	��B	��B	�3B	��B	�B	��B
\B
$�B
0!B
=qB
G�B
P�B
ZB
_;B
dZB
gmB
k�B
o�B
t�B
x�B
{�B
� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20071006125104  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071006125107  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071006125108  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071006125111  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071006125112  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071006125112  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071006130243                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20071009035617  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071009035621  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071009035622  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071009035626  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071009035626  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071009035626  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071009050446                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20071009035617  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090415042122  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090415042122  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090415042123  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090415042124  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090415042124  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090415042124  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090415042124  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090415042124  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090415042512                      G�O�G�O�G�O�                