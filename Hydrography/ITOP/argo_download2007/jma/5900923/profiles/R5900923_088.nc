CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900923 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               XA   JA  20070806185344  20090415042522                                  2B  A   1335                                                            846 @Ԋ�{r�b1   @Ԋ���@5�5?|�@`^��`A�1   ARGOS   A   A   A   @���A��Ai��A�ffA�  A�33B��B��B2ffBE33BY��Bn  B���B�  B���B�33B�  B�ffB�  B���B�  Bڙ�B���B���B�  C�CL�CffCffC��CffC33C$L�C)L�C.33C3ffC8L�C=� CB� CG��CQffC[��CeffCo33CyffC��3C���C��fC���C��3C�s3C�� C���C��fC��3C�� C��3C��fC³3Cǌ�C̀ CѦfCր Cۙ�C�� C�s3C��C�3C��3C��3D��D�fDٚD� D��DٚD��D$�fD)� D.��D3�3D8��D=ٚDB� DGٚDL�fDQ��DV� D[��D`��De� DjٚDo� Dt� Dy��D�&fD�i�D���D���D�)�D�i�D���D��3D�&fD�p D��3D��fD�&fD�ffDڠ D�� D�)�D�\�D�fD��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��Ai��A�ffA�  A�33B��B��B2ffBE33BY��Bn  B���B�  B���B�33B�  B�ffB�  B���B�  Bڙ�B���B���B�  C�CL�CffCffC��CffC33C$L�C)L�C.33C3ffC8L�C=� CB� CG��CQffC[��CeffCo33CyffC��3C���C��fC���C��3C�s3C�� C���C��fC��3C�� C��3C��fC³3Cǌ�C̀ CѦfCր Cۙ�C�� C�s3C��C�3C��3C��3D��D�fDٚD� D��DٚD��D$�fD)� D.��D3�3D8��D=ٚDB� DGٚDL�fDQ��DV� D[��D`��De� DjٚDo� Dt� Dy��D�&fD�i�D���D���D�)�D�i�D���D��3D�&fD�p D��3D��fD�&fD�ffDڠ D�� D�)�D�\�D�fD��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�=qA�?}A�;dA��A��A�JA�?}A�uA痍A�JA��A�VA��;A��A���A�K�A��HAԇ+A�Q�A��Aͩ�A�{Aʧ�A�p�A��A�r�A��mA�ĜA��A�l�A��FA�t�A�O�A�ffA�JA��FA�VA��
A�?}A���A���A��PA�A�A�-A���A���A�
=A�dZA�
=A�jA�G�A��
A�jA��DA��HA�ffA{�Ax�Ar��Am%Ag�hAcA^�!A[�AT�AM�AJv�ABffA:��A9�wA/|�A'hsA�;A7LAVAK�@�E�@�9@� �@Гu@Ə\@�ȴ@�{@�@���@���@�1@��`@�C�@���@��
@�9X@�@��j@��@rJ@dz�@Y��@N�y@H�`@Ax�@<I�@6ȴ@/�@)&�@"^5@�+@�/@ �@�@7L@�/@
~�@�@j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�=qA�?}A�;dA��A��A�JA�?}A�uA痍A�JA��A�VA��;A��A���A�K�A��HAԇ+A�Q�A��Aͩ�A�{Aʧ�A�p�A��A�r�A��mA�ĜA��A�l�A��FA�t�A�O�A�ffA�JA��FA�VA��
A�?}A���A���A��PA�A�A�-A���A���A�
=A�dZA�
=A�jA�G�A��
A�jA��DA��HA�ffA{�Ax�Ar��Am%Ag�hAcA^�!A[�AT�AM�AJv�ABffA:��A9�wA/|�A'hsA�;A7LAVAK�@�E�@�9@� �@Гu@Ə\@�ȴ@�{@�@���@���@�1@��`@�C�@���@��
@�9X@�@��j@��@rJ@dz�@Y��@N�y@H�`@Ax�@<I�@6ȴ@/�@)&�@"^5@�+@�/@ �@�@7L@�/@
~�@�@j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
��B
��B
��B
��B
�DB
�PB
�JB
�7B
�B
�+B
��B
�RB
�FB
��B
�NB
��BVB�B@�B{�B}�B�=B��B��By�B�B~�Bv�B~�B�{B�{B�uB��BǮBɺB��B�B��B��B�qB��B��B�\B�1B|�Bm�BS�BB�B2-B#�BbBB
��B
�BB
ŢB
�9B
��B
�B
cTB
G�B
)�B
uB	��B	�mB	��B	�?B	��B	|�B	_;B	XB	2-B	DB�TB��B�FB��B��B��B��B��B�9BĜBƨB��B�B�`B��B��B	oB	&�B	>wB	S�B	gmB	o�B	v�B	��B	�FB	��B	�B	��B
JB
�B
+B
33B
>wB
E�B
M�B
XB
YB
`BB
dZB
iyB
p�B
v�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
��B
��B
��B
��B
�DB
�PB
�JB
�7B
�B
�+B
��B
�RB
�FB
��B
�NB
��BVB�B@�B{�B}�B�=B��B��By�B�B~�Bv�B~�B�{B�{B�uB��BǮBɺB��B�B��B��B�qB��B��B�\B�1B|�Bm�BS�BB�B2-B#�BbBB
��B
�BB
ŢB
�9B
��B
�B
cTB
G�B
)�B
uB	��B	�mB	��B	�?B	��B	|�B	_;B	XB	2-B	DB�TB��B�FB��B��B��B��B��B�9BĜBƨB��B�B�`B��B��B	oB	&�B	>wB	S�B	gmB	o�B	v�B	��B	�FB	��B	�B	��B
JB
�B
+B
33B
>wB
E�B
M�B
XB
YB
`BB
dZB
iyB
p�B
v�B
}�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20070806185341  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070806185344  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070806185345  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070806185349  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070806185349  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070806185349  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070806190628                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070810035447  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070810035451  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070810035452  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070810035456  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070810035456  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070810035457  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070810043129                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070810035447  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090415042107  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090415042108  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090415042108  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090415042109  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090415042109  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090415042109  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090415042109  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090415042109  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090415042522                      G�O�G�O�G�O�                