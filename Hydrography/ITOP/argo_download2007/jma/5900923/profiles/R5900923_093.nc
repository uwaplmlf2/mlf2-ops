CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   n   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4T   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6|   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :\   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  B�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    D�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    G�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    J�   CALIBRATION_DATE            	             
_FillValue                  ,  M�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N    HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N`   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Np   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Nt   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5900923 J-ARGO                                                          kensuke Takeuchi                                                PRES            TEMP            PSAL               ]A   JA  20070926185304  20090415042503                                  2B  A   1335                                                            846 @ԗ���?�1   @ԗ��r�b@5��+@` z�G�1   ARGOS   A   A   A   @�ffAffA^ffA���A�ffA���B	��B33B0��BE��BX��Bm33B�  B�33B�ffB�33B���B�ffB���B�33B�  B���B�33B�  B���CL�C33C�C33C�3C� CffC$� C)ffC.ffC3� C8L�C=ffCB33CG  CQffC[L�CeffCo33CyL�C�s3C��3C�� C��3C��3C�� C�s3C�ffC���C�s3C�ffC�� C�� C���Cǳ3C�� Cь�C֌�C۳3C�s3C噚C��C�s3C���C�� DٚD� D��D��D��D�fDٚD$ٚD)��D.� D3��D8� D=�fDBٚDG�3DL� DQ��DV��D[ٚD`�3D��D�Y�D���D�� D�#3D�l�D�� D���D�)�D�ffD���D�� D�,�D�c3Dک�D��3D�fD�c3D�D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�ffAffA^ffA���A�ffA���B	��B33B0��BE��BX��Bm33B�  B�33B�ffB�33B���B�ffB���B�33B�  B���B�33B�  B���CL�C33C�C33C�3C� CffC$� C)ffC.ffC3� C8L�C=ffCB33CG  CQffC[L�CeffCo33CyL�C�s3C��3C�� C��3C��3C�� C�s3C�ffC���C�s3C�ffC�� C�� C���Cǳ3C�� Cь�C֌�C۳3C�s3C噚C��C�s3C���C�� DٚD� D��D��D��D�fDٚD$ٚD)��D.� D3��D8� D=�fDBٚDG�3DL� DQ��DV��D[ٚD`�3D��D�Y�D���D�� D�#3D�l�D�� D���D�)�D�ffD���D�� D�,�D�c3Dک�D��3D�fD�c3D�D���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A埾A�~�A�^A��A���A���A�hA��A�A�-A�ȴA�ƨAޡ�A�%A�1A�;dA�9XAмjA�A�VA�A�x�A��9A���A�1A�  A���A��TA�ĜA��+A���A�G�A��A�C�A��-A�O�A���A�l�A���A�`BA��A���A���A�1'A���A�v�A���A�K�A���A��
A�bA�XA�jA�A�A���A}�Ax-At��AooAh{Ab�RA]�wAR�jAO`BAJQ�AF��AB��A?7LA9&�A2��A*�A�jA�TA�FA ��@�^5@�9X@�
=@̛�@�V@�$�@��R@���@�`B@��\@��@�\)@�5?@���@���@u�T@h �@Z~�@K��@D9X@>E�@9�@2�H@-`B@(A�@$1@ �@��@�F@\)@Z@hs@�R@�
@Ĝ11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A埾A�~�A�^A��A���A���A�hA��A�A�-A�ȴA�ƨAޡ�A�%A�1A�;dA�9XAмjA�A�VA�A�x�A��9A���A�1A�  A���A��TA�ĜA��+A���A�G�A��A�C�A��-A�O�A���A�l�A���A�`BA��A���A���A�1'A���A�v�A���A�K�A���A��
A�bA�XA�jA�A�A���A}�Ax-At��AooAh{Ab�RA]�wAR�jAO`BAJQ�AF��AB��A?7LA9&�A2��A*�A�jA�TA�FA ��@�^5@�9X@�
=@̛�@�V@�$�@��R@���@�`B@��\@��@�\)@�5?@���@���@u�T@h �@Z~�@K��@D9X@>E�@9�@2�H@-`B@(A�@$1@ �@��@�F@\)@Z@hs@�R@�
@Ĝ11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
iyB
gmB
[#B
J�B
J�B
L�B
T�B
H�B
F�B
gmB
{�B
��B
�TB
�BB�B �B#�BA�B_;BS�BA�B>wBI�BS�BQ�BXBaHBaHBu�BjB� B��B��B�?B��B��B��B��B�bB�7Bo�BS�BL�BI�BB�B:^B+B�BPB
��B
�B
�BB
��B
�}B
��B
�+B
q�B
S�B
/B
uB	��B	ȴB	�-B	��B	�oB	�B	s�B	\)B	B�B	�B��B�/B��B�1Bx�Br�Bx�B�B��B�wB��B��B�B	DB	�B	7LB	D�B	K�B	C�B	��B	��B	�)B	��B
\B
�B
+B
:^B
E�B
O�B
XB
^5B
bNB
dZB
k�B
n�B
r�B
v�B
z�B
~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B
iyB
gmB
[#B
J�B
J�B
L�B
T�B
H�B
F�B
gmB
{�B
��B
�TB
�BB�B �B#�BA�B_;BS�BA�B>wBI�BS�BQ�BXBaHBaHBu�BjB� B��B��B�?B��B��B��B��B�bB�7Bo�BS�BL�BI�BB�B:^B+B�BPB
��B
�B
�BB
��B
�}B
��B
�+B
q�B
S�B
/B
uB	��B	ȴB	�-B	��B	�oB	�B	s�B	\)B	B�B	�B��B�/B��B�1Bx�Br�Bx�B�B��B�wB��B��B�B	DB	�B	7LB	D�B	K�B	C�B	��B	��B	�)B	��B
\B
�B
+B
:^B
E�B
O�B
XB
^5B
bNB
dZB
k�B
n�B
r�B
v�B
z�B
~�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA5_a                                                                20070926185301  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070926185304  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070926185305  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070926185309  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070926185309  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070926185309  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070926190548                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070929035622  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070929035626  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070929035627  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070929035631  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070929035631  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20070929035631  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070929043525                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070929035622  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090415042120  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090415042120  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090415042120  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090415042121  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090415042121  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090415042121  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090415042121  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090415042122  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090415042503                      G�O�G�O�G�O�                