CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               5A   JA  20070824025723  20090916024449  A9_60107_053                    2C  D   APEX-SBE 2341                                                   846 @ԏk�W;1   @ԏm�)��@8���"��@bC���+1   ARGOS   A   A   A   @���A33Ad��A�  A�  A���B
ffBffB0ffBG��BY��Bm��B�ffB�ffB�ffB���B���B�ffB�  Bƙ�B���B�  B�33B�ffB�ffC�3CffCffCffCffC��C�C$L�C)�C.  C3�C7��C=L�CB33CGL�CQ33C[� Ce� Co� CyL�C��fC���C���C�� C���C�s3C�� C��fC��3C�ffC�s3C�� C���C³3C�� C�� Cь�C֙�Cۙ�C�� C�� C�3C�fC��3C��fD� D��DٚD��D��D�3D� D$��D)ٚD.ٚD3��D8��D=��DB�3DG��DL�3DQ��DV��D[� D`��De�fDj�fDo� Dt�fDy� D�,�D�p D���D���D�)�D�ffD�� D��3D�)�D�` D���D���D�#3D�ffDڣ3D�� D�  D�ffD� D�y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33A\��A�  A�  A���BffBffB.ffBE��BW��Bk��B�ffB�ffB�ffB���B���B�ffB�  Bř�B���B�  B�33B�ffB�ffC33C�fC
�fC�fC�fC�C��C#��C(��C-� C2��C7L�C<��CA�3CF��CP�3C[  Ce  Co  Cx��C�ffC�L�C�L�C�� C�L�C�33C�@ C�ffC�s3C�&fC�33C�@ C�Y�C�s3Cǀ C̀ C�L�C�Y�C�Y�C�@ C� C�s3C�ffC�s3C�ffD� D��D��D��D��D�3D� D$��D)��D.��D3��D8��D=��DB�3DG��DL�3DQ��DV��D[� D`��De�fDj�fDo� Dt�fDy� D��D�` D���D���D��D�VfD�� D��3D��D�P D���D���D�3D�VfDړ3D�� D� D�VfD� D�i�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A��A���A�  A���A��A�9A���A�oA��A� �A��yA���A˶FA�oA���A�{AčPA�z�A�  A��A�VA���A�oA��FA���A�ZA��\A�XA�M�A�S�A��A�^5A�
=A� �A�x�A��A�n�A�x�A���A��A�JA��A�n�A�=qA��uA��yA���A�Q�A���A���A��jA�$�A��A�1'A~5?AyS�At$�Ap��Al��Ai��Afv�A`ffA[�AVffAQl�AKAEdZA=��A5��A+��A&�A�9A=qA 1'@�-@��/@��@��7@�ȴ@�b@��y@�$�@��/@���@�@�7L@�9X@�G�@�\)@��@��j@|�D@u?}@m��@`�u@S@I��@B�!@:n�@4�@/+@( �@"��@�D@�w@��@5?@
�H@l�@p�@��@ �9?�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A���A�  A���A��A�9A���A�oA��A� �A��yA���A˶FA�oA���A�{AčPA�z�A�  A��A�VA���A�oA��FA���A�ZA��\A�XA�M�A�S�A��A�^5A�
=A� �A�x�A��A�n�A�x�A���A��A�JA��A�n�A�=qA��uA��yA���A�Q�A���A���A��jA�$�A��A�1'A~5?AyS�At$�Ap��Al��Ai��Afv�A`ffA[�AVffAQl�AKAEdZA=��A5��A+��A&�A�9A=qA 1'@�-@��/@��@��7@�ȴ@�b@��y@�$�@��/@���@�@�7L@�9X@�G�@�\)@��@��j@|�D@u?}@m��@`�u@S@I��@B�!@:n�@4�@/+@( �@"��@�D@�w@��@5?@
�H@l�@p�@��@ �9?�?���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBÖBÖBÖBBB��B��BƨBoB�B!�B�BVBVBbB#�B!�B�B(�B$�B�B�B	7B	7B��B�hB|�Bt�Bm�BiyBXBI�B6FB9XB9XB6FB33B/B.B1'B-B"�B!�B�B+B-B%�B�B�BPB
��B
�B
�;B
��B
�RB
��B
�DB
t�B
`BB
J�B
?}B
,B
oB	��B	�TB	��B	�'B	�uB	m�B	O�B	1'B	!�B�B�Bm�BR�BC�B>wBD�BR�B_;Bk�Bw�B�DB��B�BǮB�5B�B��B	JB	�B	(�B	@�B	W
B	v�B	��B	�9B	ÖB	��B	�TB	�B	��B
PB
�B
#�B
0!B
=qB
D�B
M�B
T�B
ZB
_;B
ffB
m�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 BÖBÖBÖBBB��B��B��B{B�B%�B%�BuB\BoB$�B"�B �B)�B(�B �B�BPB\B�B�{B~�Bu�Bo�Bl�BZBK�B7LB:^B:^B7LB49B0!B.B1'B-B"�B!�B �B+B.B%�B�B�BVB
��B
�B
�BB
��B
�XB
��B
�JB
u�B
aHB
K�B
@�B
.B
uB	��B	�ZB	��B	�3B	��B	o�B	P�B	2-B	#�B�B�!Bn�BS�BD�B?}BE�BS�B_;Bk�Bw�B�DB��B�BǮB�5B�B��B	JB	�B	(�B	@�B	W
B	v�B	��B	�9B	ÖB	��B	�TB	�B	��B
PB
�B
#�B
0!B
=qB
D�B
M�B
T�B
ZB
_;B
ffB
m�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.5(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709060209462007090602094620070906020946200709060216092007090602160920070906021609200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20070824025721  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070824025723  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070824025724  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070824025728  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070824025729  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070824025729  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070824030951                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070828035750  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070828035754  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070828035755  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070828035759  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070828035759  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070828035759  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070828042919                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070828035750  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090501065330  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090501065330  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090501065331  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090501065331  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090501065332  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090501065332  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090501065332  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090501065332  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090501065332  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090501065712                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070827110154  CV  DAT$            G�O�G�O�F�{l                JM  ARCAJMQC1.0                                                                 20070906020946  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070906020946  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070906021609  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916024234  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916024449                      G�O�G�O�G�O�                