CDF   	   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               6A   JA  20070904085457  20090916024455  A9_60107_054                    2C  D   APEX-SBE 2341                                                   846 @ԑ�ja�x1   @ԑ���@9      @bBV�u1   ARGOS   A   A   A   @���AffAfffA�  Ař�A�ffB
��B��B333G�O�G�O�G�O�G�O�G�O�B���B���B�  B���B���B�33B���B���B�ffB�33B�33C�C33CL�C  C��CffCffC$L�C)�C.L�C3� C8L�C=ffCB� CG� CQL�C[ffCe  Co33CyL�C��fC�� C�s3C�s3C�� C��3C�s3C��fC�� C��fC��fC���C��3C C�� C̀ C�s3C֦fC�ffC���C�fCꙚCC���C���D�fD�3DٚD�3D��D��DٚD$�3D)�fD.� D3��D8ٚD=ٚDB�fDG�fDL�fDQ��DV��D[� D`��De��Dj� Do�3Dt� Dy�3D�&fD�p D�� D��D�#3D�i�D���D��fD�0 D�i�D��fD�� D�)�D�l�Dڣ3D�ٚD�  D�` D�fD�S31111111119999911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffA^ffA�  A���A�ffB��B��G�O�G�O�G�O�G�O�G�O�G�O�B���B���B�  B���B���B�33B���B���B�ffB�33B�33C ��C�3C
��C� CL�C�fC�fC#��C(��C-��C3  C7��C<�fCB  CG  CP��CZ�fCd� Cn�3Cx��C�ffC�@ C�33C�33C�@ C�s3C�33C�ffC�@ C�ffC�ffC���C�s3C�@ Cǀ C�@ C�33C�ffC�&fC�Y�C�ffC�Y�C�Y�C�Y�C�L�D�fD�3D��D�3D��D��D��D$�3D)�fD.� D3��D8��D=��DB�fDG�fDL�fDQ��DV��D[� D`��De��Dj� Do�3Dt� Dy�3D�fD�` D�� D�ٚD�3D�Y�D���D��fD�  D�Y�D��fD�� D��D�\�Dړ3D�ɚD� D�P D�fD�C31111111199999911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��G�O�G�O�G�O�G�O�G�O�G�O�@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��yA���A���A�A�I�AܑhA��
AǁG�O�G�O�G�O�G�O�G�O�A��DA��A�7LA�bNA�?}A��A��
A��A��A�/A�v�A�  A�n�A�1'A�\)A�bNA�r�A���A���A�jA�;dA���A�ȴA���A���A��/A��PA��/A��A���A���A��A��A�A�A��A��TA���A�VA�=qA�hsA�M�A~JAy�ApQ�Ai�wAgG�Ae33A_S�AY�-AT{AO��AK�#AD�9A@5?A<z�A7p�A1�A++A�+A&�A
ZAp�@��`@�7L@��@��
@��@��m@�X@�&�@�r�@��@��H@�33@��@�I�@�Z@�Z@��9@{dZ@v�+@r=q@e��@\�/@N{@E�@>$�@65?@0��@*��@'�@#�m@!��@��@@M�@v�@
�@b@I�@G�?���1111111199999111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��yA���A���A�A�I�AܑhA��
AǁG�O�G�O�G�O�G�O�G�O�G�O�A��A�7LA�bNA�?}A��A��
A��A��A�/A�v�A�  A�n�A�1'A�\)A�bNA�r�A���A���A�jA�;dA���A�ȴA���A���A��/A��PA��/A��A���A���A��A��A�A�A��A��TA���A�VA�=qA�hsA�M�A~JAy�ApQ�Ai�wAgG�Ae33A_S�AY�-AT{AO��AK�#AD�9A@5?A<z�A7p�A1�A++A�+A&�A
ZAp�@��`@�7L@��@��
@��@��m@�X@�&�@�r�@��@��H@�33@��@�I�@�Z@�Z@��9@{dZ@v�+@r=q@e��@\�/@N{@E�@>$�@65?@0��@*��@'�@#�m@!��@��@@M�@v�@
�@b@I�@G�?���1111111199999911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;oG�O�G�O�G�O�G�O�G�O�G�O�;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB~�B~�B~�B{�Be`B"�B
�LB
��B=qG�O�G�O�G�O�G�O�G�O�B5?B8RB@�B?}B<jB:^B<jB:^B:^B:^B9XB7LB6FB5?B49B2-B1'B0!B0!B/B.B-B)�B(�B'�B$�B�B�BoBVBDBB
��B
��B
�B
�TB
��B
��B
ĜB
�!B
��B
�DB
_;B
B�B
6FB
'�B
DB	�B	�B	ƨB	�-B	�{B	~�B	hsB	P�B	5?B	�B�B��B�\B}�Be`BR�BI�BB�BI�BW
BaHBu�B�B��B�B�wB��B�5B�B		7B	�B	-B	>wB	H�B	�B	��B	�RB	��B	�;B	�mB	�B	��B

=B
�B
!�B
,B
+B
7LB
@�B
H�B
N�B
W
B
_;B
e`1111111119999911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B~�B~�B~�B|�BjB0!B
�jB
��G�O�G�O�G�O�G�O�G�O�G�O�B8RB:^BB�B@�B=qB=qB=qB;dB;dB:^B:^B7LB7LB6FB5?B33B1'B0!B0!B/B.B.B+B)�B'�B%�B �B�BoBVBDB%B
��B
��B
�B
�ZB
��B
��B
ŢB
�'B
��B
�PB
aHB
C�B
7LB
)�B
JB	�B	�B	ǮB	�9B	��B	� B	iyB	R�B	6FB	 �B�B��B�bB~�BffBS�BJ�BD�BJ�BXBbNBu�B�B��B�B�wB��B�5B�B		7B	�B	-B	>wB	H�B	�B	��B	�RB	��B	�;B	�mB	�B	��B

=B
�B
!�B
,B
+B
7LB
@�B
H�B
N�B
W
B
_;B
e`1111111199999911111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<T��<#�
<#�
G�O�G�O�G�O�G�O�G�O�G�O�<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.5(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709191610592007091916105920070919161059200709191615482007091916154820070919161548200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20070904085455  IP                  G�O�G�O�G�O�                JM  ARFMDECPA9                                                                  20070906110940  IP                  G�O�G�O�G�O�                JM  ARGQRQCP1                                                                   20070906110940  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20070919161059  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070919161059  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070919161548  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916024240  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916024455                      G�O�G�O�G�O�                