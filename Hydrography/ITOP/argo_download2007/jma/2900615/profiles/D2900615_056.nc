CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               8A   JA  20070923054451  20090916024451  A9_60107_056                    2C  D   APEX-SBE 2341                                                   846 @Ԗ�x1   @Ԗ�^>��@8�^5?|�@b7dZ�1   ARGOS   A   A   A   @�  A33Ah  A���A���A홚B
��B33B2ffBE��BZffBnffB�33B�ffB���B�  B�ffB�ffB���B���BЙ�B�  B���B���B�ffC� C33C�CL�CffCffC��C$� C)L�C.� C3�C8ffC=L�CBffCG�CQ�C[  Ce33Co� Cy� C�� C�s3C���C�� C���C���C���C���C���C��fC���C�� C�s3C³3Cǀ C̦fCр C֌�C�� C�� C�3CꙚC���C���C�� D�fD� D�3D� D�3DٚD�fD$�3D)� D.ٚD3��D8�3D=� DB�3DGٚDL�3DQ��DV��D[�3D`� De�3Dj�fDo�3DtٚDy�fD�&fD�Y�D���D��fD�fD�c3D�� D��D�&fD�l�D���D�ٚD�  D�p Dڠ D��fD��D�VfD�3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A	��A^ffA�  A�  A���BffB��B0  BC33BX  Bl  B�  B�33B���B���B�33B�33B���Bř�B�ffB���B㙚B홚B�33C �fC��C
� C�3C��C��C  C#�fC(�3C-�fC2� C7��C<�3CA��CF� CP� CZffCd��Cn�fCx�fC�33C�&fC�@ C�33C�@ C�L�C�@ C�� C�@ C�Y�C�L�C�s3C�&fC�ffC�33C�Y�C�33C�@ C�s3C�s3C�ffC�L�C� C�L�C�s3D� D��D��D��D��D�3D� D$��D)��D.�3D3�fD8��D=��DB��DG�3DL��DQ�3DV�3D[��D`��De��Dj� Do��Dt�3Dy� D�3D�FfD��fD��3D�3D�P D���D��fD�3D�Y�D���D��fD��D�\�Dڌ�D��3D�	�D�C3D� D�vf1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A���A���A���A���A�ȴAԴ9A�ffA�ffA���A���A��yA��A��A��A�1A�z�A��RA��^A�G�A�JA��^A��yA�VA��7A��A���A��+A�K�A��TA�bNA�-A�A��A�`BA���A��DA��!A�JA��A�l�A���A��A��A�=qA�r�A�;dA�ffA��/A���A� �A�mAz��Au�mAqXAp�Al1Ag�Ad{A]
=AS�hAM��AI��A?�7A8ȴA5p�A2  A0�RA,5?A(ZA�TAbA	�h@��@��/@��#@۝�@�l�@�M�@�S�@��u@�V@���@�ff@�&�@���@�X@�@��!@�"�@}`B@y�#@sdZ@m`B@i�7@^@TI�@H  @@1'@:��@6ff@0�u@*��@#ƨ@�@�#@?}@��@��@
�@��@��@ bN?��?��m1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A���A���A���A���A�ȴAԴ9A�ffA�ffA���A���A��yA��A��A��A�1A�z�A��RA��^A�G�A�JA��^A��yA�VA��7A��A���A��+A�K�A��TA�bNA�-A�A��A�`BA���A��DA��!A�JA��A�l�A���A��A��A�=qA�r�A�;dA�ffA��/A���A� �A�mAz��Au�mAqXAp�Al1Ag�Ad{A]
=AS�hAM��AI��A?�7A8ȴA5p�A2  A0�RA,5?A(ZA�TAbA	�h@��@��/@��#@۝�@�l�@�M�@�S�@��u@�V@���@�ff@�&�@���@�X@�@��!@�"�@}`B@y�#@sdZ@m`B@i�7@^@TI�@H  @@1'@:��@6ff@0�u@*��@#ƨ@�@�#@?}@��@��@
�@��@��@ bN?��?��m1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB1'B1'B1'B0!B-B#�B
�-B
ĜB
��B
�B
��B
��BBhB{B�B�B+B@�BB�BB�B?}B>wB;dB5?B33B2-B0!B-B,B)�B)�B)�B(�B(�B&�B$�B$�B!�B!�B �B�BoB
=BB
��B
�B
�B
�/B
��B
B
�B
�uB
z�B
hsB
cTB
L�B
:^B
'�B
%B	�;B	ɺB	�9B	�%B	l�B	]/B	N�B	I�B	A�B	.B�sB�LB��Bu�BjB^5BQ�BC�BI�BQ�B\)Bq�B�%B��B�FB��B�ZB��B	\B	�B	(�B	>wB	M�B	aHB	hsB	�B	��B	�}B	��B	�#B	�fB	��B
B
\B
�B
&�B
/B
;dB
A�B
H�B
P�B
[#B
aHB
gmB
k�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B1'B1'B1'B0!B.B0!B
�RB
ȴB
�B
�B
��BB
=BoB�B�B �B1'B@�BB�BB�BA�B?}B>wB8RB49B2-B1'B-B,B)�B)�B)�B)�B)�B&�B%�B%�B!�B!�B!�B�BoB
=BB
��B
�B
�B
�5B
��B
ÖB
�B
�{B
{�B
hsB
dZB
M�B
;dB
)�B
1B	�HB	��B	�FB	�1B	m�B	^5B	N�B	J�B	B�B	0!B�B�RB��Bv�Bk�B_;BR�BD�BJ�BR�B]/Br�B�+B��B�FB��B�ZB��B	\B	�B	(�B	>wB	M�B	aHB	hsB	�B	��B	�}B	��B	�#B	�fB	��B
B
\B
�B
&�B
/B
;dB
A�B
H�B
P�B
[#B
aHB
gmB
k�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<D��<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.6(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200901071300192009010713001920090107130019200901071306572009010713065720090107130657200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20070923054426  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070923054451  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070923054455  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070923054507  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070923054507  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070923054509  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070923062108                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070927035922  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070927035926  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070927035927  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070927035930  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070927035931  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070927035931  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070927043314                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070927035922  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090501065337  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090501065338  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090501065338  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090501065338  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090501065339  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090501065339  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090501065339  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090501065339  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090501065339  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090501065704                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20090107130014  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20090107130019  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090107130019  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090107130657  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916024237  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916024451                      G�O�G�O�G�O�                