CDF   $   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900615 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               7A   JA  20070913085950  20090916024455  A9_60107_055                    2C  D   APEX-SBE 2341                                                   846 @ԔoCQ��1   @Ԕq���@8ѩ��l�@b:��"��1   ARGOS   A   A   A   @���A  Ad��A�  A�ffA�  B
ffB33B133BE��BXffBm��B���B�33B���B���B���B���B���B���B�  B�ffB䙚B�33B�  C33CffCffCffC� C�fC  C$33C)ffC.�C2��C8L�C=�CB33CG��CQ33C[�CeL�CoffCy33C���C��fC�� C�s3C�s3C���C�s3C���C��3C��fC��3C��3C��fC�Cǌ�C̦fCѳ3Cֳ3C�s3C���C��C�L�C�L�C��fC��fD�3D��D�fD�fD�fD� D�fD$� D)�fD.ٚD3�3D8��D=��DB��DG� DL�3DQ��DV�fD[�3D`�3De�3Dj� Do� Dt�fDy� D�#3D�l�D��fD���D�#3D�p D�� D�� D��D�Y�D�� D��3D�#3D�ffDڣ3D���D�  D�` D�D�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA��AY��A�ffA���A�ffB��BffB.ffBB��BU��Bj��B~��B���B�ffB�ffB�33B�ffB�ffB�ffBϙ�B�  B�33B���B���C � C�3C
�3C�3C��C33CL�C#� C(�3C-ffC2�C7��C<ffCA� CF�fCP� CZffCd��Cn�3Cx� C�@ C�L�C�&fC��C��C�@ C��C�33C�Y�C�L�C�Y�C�Y�C�L�C�33C�33C�L�C�Y�C�Y�C��C�33C�33C��3C��3C�L�C�L�D�fD� D��D��D��D�3D��D$�3D)��D.��D3�fD8� D=� DB��DG�3DL�fDQ� DV��D[�fD`�fDe�fDj�3Do�3Dt��Dy�3D��D�VfD�� D��fD��D�Y�D���D�ɚD�fD�C3D���D���D��D�P Dڌ�D��fD�	�D�I�D�3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A��A��A��A���A��TA��
A�A��wA���A�G�A�;dA�$�A�7LA��TA�&�A��A�
=A�bNA��^A��#A�E�A��yA��+A��\A���A��^A��!A�jA��A�x�A�XA���A�A���A�bNA�G�A�(�A��A�A��A�VA�O�A�$�A��\A��jA�;dA�{A��PA�5?A�33A��-A�1A�7A{��Aw��Ar5?Al�jAi�Ac��A_+AYhsAXJAS�AN��AI%ADQ�A?S�A9��A/�A'l�AG�A�7Aff@��w@��@�\)@�/@�z�@��!@���@�b@�z�@�I�@�=q@��m@�M�@�9X@�@�M�@���@}��@xbN@p��@i&�@_|�@S@Hb@A�7@9�@5�h@/�P@,�D@%�@��@33@�-@r�@o@b@�/@�^?�O�?��u?�`B1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A��A��A���A��TA��
A�A��wA���A�G�A�;dA�$�A�7LA��TA�&�A��A�
=A�bNA��^A��#A�E�A��yA��+A��\A���A��^A��!A�jA��A�x�A�XA���A�A���A�bNA�G�A�(�A��A�A��A�VA�O�A�$�A��\A��jA�;dA�{A��PA�5?A�33A��-A�1A�7A{��Aw��Ar5?Al�jAi�Ac��A_+AYhsAXJAS�AN��AI%ADQ�A?S�A9��A/�A'l�AG�A�7Aff@��w@��@�\)@�/@�z�@��!@���@�b@�z�@�I�@�=q@��m@�M�@�9X@�@�M�@���@}��@xbN@p��@i&�@_|�@S@Hb@A�7@9�@5�h@/�P@,�D@%�@��@33@�-@r�@o@b@�/@�^?�O�?��u?�`B1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�FB
�LB
�RB
�9B
�LB
�'B
�BVB-B?}B�B��B�B�B�B�fB�)B��B��B�dB��B�Bt�Bo�BR�B;dB7LB49B49B2-B0!B/B.B,B(�B'�B&�B%�B&�B%�B�B�B$�B�B�BhB1BB
��B
�B
�5B
��B
�FB
�B
��B
�B
hsB
O�B
@�B
&�B
\B	��B	�B	�5B	ȴB	�B	��B	|�B	e`B	6FB	\B��B��B�PB�%Bq�BZBD�BJ�BP�B_;Bo�B{�B�hB��B��B�HB��B	DB	�B	'�B	@�B	G�B	VB	[#B	{�B	��B	�jB	��B	�`B	�B	��B
B
JB
�B
(�B
49B
<jB
E�B
M�B
W
B
^5B
e`B
m�B
s�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�FB
�LB
�RB
�FB
�}B
ÖB
�B�B/B@�B��B��B�B�B�B�sB�/B�
B��B�wB��B�Bu�Bp�BT�B<jB8RB49B49B2-B0!B/B.B-B(�B'�B&�B&�B&�B%�B�B�B%�B�B�BoB	7BB
��B
�B
�;B
��B
�LB
�B
��B
�B
iyB
P�B
B�B
'�B
bB	��B	�B	�;B	ɺB	�!B	��B	}�B	hsB	7LB	hB�B�B�VB�+Br�B\)BE�BJ�BQ�B`BBo�B{�B�hB��B��B�HB��B	DB	�B	'�B	@�B	G�B	VB	[#B	{�B	��B	�jB	��B	�`B	�B	��B
B
JB
�B
(�B
49B
<jB
E�B
M�B
W
B
^5B
e`B
m�B
s�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<�t�<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.7(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709260242402007092602424020070926024240200709260252382007092602523820070926025238200909110000002009091100000020090911000000  JA  ARFMdecpA9_b                                                                20070913085948  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070913085950  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070913085951  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070913085956  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070913085956  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070913085956  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070913091346                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070917035636  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070917035640  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070917035641  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070917035645  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070917035645  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070917035645  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070917035645  CV  TIME            G�O�G�O�                    JA  ARGQrelo2.1                                                                 20070917035645  CV  LAT$            G�O�G�O�AƍP                JA  ARGQrelo2.1                                                                 20070917035645  CV  LON$            G�O�G�O�C��                JA  ARUP                                                                        20070917044049                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070917035636  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090501065335  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090501065335  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090501065335  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090501065335  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090501065337  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090501065337  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090501065337  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090501065337  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090501065337  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090501065707                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070920011053  CV  DAT$            G�O�G�O�F���                JM  ARGQJMQC1.0                                                                 20070920011053  CV  LAT$            G�O�G�O�AƏ\                JM  ARCAJMQC1.0                                                                 20070926024240  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070926024240  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070926025238  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916024240  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916024455                      G�O�G�O�G�O�                