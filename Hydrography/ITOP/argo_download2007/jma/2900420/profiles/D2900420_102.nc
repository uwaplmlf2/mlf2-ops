CDF   (   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   o   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Eh   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Nh   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Wh   CALIBRATION_DATE      	   
                
_FillValue                  �  `h   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a8   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    aH   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    aL   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a\   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a`   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        ad   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    ahArgo profile    2.2 1.2 19500101000000  2900420 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               fA   JA  20070911185419  20090916023821  A5_21330_102                    2C  D   APEX-SBE 1093                                                   846 @ԔA��{1   @Ԕi�@9T9XbN@b7;dZ�1   ARGOS   A   A   A   @�33A  Ad��A�ffAř�A홚B33B��B2  BFffBX  Bm33B�ffB�33B���B���B�ffB���B�  B���B�  Bڙ�B�ffB�33B�  C  C  C  C33CffC�CffC$  C)ffC.�C2��C833C<��CBL�CG�CP�fC[L�Cd�fCoffCyffC��3C�� C��3C�� C�� C�s3C��fC�� C��3C���C��3C���C�� C¦fC�s3C̳3C�� C֦fC�s3C�3C�ffC�fC�fC��fC���DٚD� D��D�fD��D3D"FfD(�3D.�3D5�D;FfDAy�DG�3DN�DTFfDZ��D`�fDg  DmFfDs� DyٚD�0 D�ffD��3D�� D�  D�p D���D�� D�,�D�c3D���D���D�)�D�ffDڦfD�ٚD�&fD�VfD�3D��3111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A  Ad��A�ffAř�A홚B33B��B2  BFffBX  Bm33B�ffB�33B���B���B�ffB���B�  B���B�  Bڙ�B�ffB�33B�  C  C  C  C33CffC�CffC$  C)ffC.�C2��C833C<��CBL�CG�CP�fC[L�Cd�fCoffCyffC��3C�� C��3C�� C�� C�s3C��fC�� C��3C���C��3C���C�� C¦fC�s3C̳3C�� C֦fC�s3C�3C�ffC�fC�fC��fC���DٚD� D��D�fD��D3D"FfD(�3D.�3D5�D;FfDAy�DG�3DN�DTFfDZ��D`�fDg  DmFfDs� DyٚD�0 D�ffD��3D�� D�  D�p D���D�� D�,�D�c3D���D���D�)�D�ffDڦfD�ٚD�&fD�VfD�3D��3111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A��A��#A�n�AӺ^AĲ-A�9XA�E�A�  A��A�9XA���A�Q�A�jA�A�33A���A�VA��A���A�bA��hA�ƨA�9XA��yA��+A�K�A���A�|�A��A�`BA�33A�O�A��9A��A�S�A�A��A�Q�A��A���A��jA���A�A��jA�x�A��A���A�1A}��Ax��Au/Aq�Ao|�Ah��Ac�7A_��A[VAXE�AUC�AQoAL1'AI��ABZA=��A;ƨA3�A,A(jA%�#A�An�A�@�%@���@��/@ϕ�@��@��@�&�@�-@�l�@��@�ƨ@�1'@��@�~�@��@z��@rJ@m`B@`�9@V{@N$�@F5?@=O�@5�T@/K�@(1'@!��@33@l�@�!@E�@
-@l�@C�@   ?�?�r�?�t�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A��#A�n�AӺ^AĲ-A�9XA�E�A�  A��A�9XA���A�Q�A�jA�A�33A���A�VA��A���A�bA��hA�ƨA�9XA��yA��+A�K�A���A�|�A��A�`BA�33A�O�A��9A��A�S�A�A��A�Q�A��A���A��jA���A�A��jA�x�A��A���A�1A}��Ax��Au/Aq�Ao|�Ah��Ac�7A_��A[VAXE�AUC�AQoAL1'AI��ABZA=��A;ƨA3�A,A(jA%�#A�An�A�@�%@���@��/@ϕ�@��@��@�&�@�-@�l�@��@�ƨ@�1'@��@�~�@��@z��@rJ@m`B@`�9@V{@N$�@F5?@=O�@5�T@/K�@(1'@!��@33@l�@�!@E�@
-@l�@C�@   ?�?�r�?�t�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
ɺB
ɺB
ȴB
�B
��B
��B�BB�B&�B(�B/B5?B>wB=qBI�BI�BJ�BK�BA�B>wB;dB:^B9XB9XB7LB5?B49B33B33B33B1'B0!B.B.B0!B,B&�B"�B!�B�B�B�BoBB
��B
�;B
��B
�RB
�B
�{B
�B
t�B
e`B
F�B
0!B
�B

=B	��B	�B	�NB	��B	�wB	��B	�bB	�B	W
B	49B	#�B	{B��B��B��B�hBn�BaHB`BB^5BaHBr�B�=B�bB�LB�B�mB	B	�B	+B	7LB	P�B	bNB	�B	��B	�9B	��B	�;B	�B	��B
DB
�B
&�B
33B
<jB
F�B
P�B
VB
`BB
iyB
o�B
r�B
x�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
ɺB
ɺB
��B
�5B
�/B
�BB�B	7B�B+B,B0!B8RB?}B@�BI�BJ�BL�BL�BB�B>wB<jB;dB9XB9XB7LB5?B49B49B49B33B2-B1'B.B.B0!B-B'�B"�B!�B�B�B�BuBB
��B
�BB
��B
�XB
�B
��B
�B
u�B
gmB
G�B
1'B
 �B
DB	��B	�B	�TB	��B	��B	��B	�hB	�B	YB	5?B	$�B	�B��B��B��B�oBo�BbNBaHB_;BbNBr�B�=B�hB�LB�B�mB	B	�B	+B	7LB	P�B	bNB	�B	��B	�9B	��B	�;B	�B	��B
DB
�B
&�B
33B
<jB
F�B
P�B
VB
`BB
iyB
o�B
r�B
x�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<T��<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709241438542007092414385420070924143854200709241442572007092414425720070924144257200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20070911185416  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070911185419  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070911185419  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070911185419  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070911185423  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070911185423  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070911185424  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070911185424  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070911185424  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070911185424  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070911191205                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070915034648  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070915034653  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070915034653  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070915034654  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070915034658  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070915034658  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070915034658  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070915034658  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070915034658  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070915034658  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070915044443                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070915034648  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090414022153  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090414022153  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090414022153  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090414022153  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090414022154  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090414022154  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090414022154  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090414022154  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090414022154  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090414022428                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070919233419  CV  DAT$            G�O�G�O�F��=                JM  ARCAJMQC1.0                                                                 20070924143854  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070924143854  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070924144257  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916023503  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916023821                      G�O�G�O�G�O�                