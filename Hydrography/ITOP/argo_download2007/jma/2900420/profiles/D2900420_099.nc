CDF   (   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   o   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Eh   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Nh   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Wh   CALIBRATION_DATE      	   
                
_FillValue                  �  `h   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a8   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    aH   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    aL   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a\   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a`   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        ad   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    ahArgo profile    2.2 1.2 19500101000000  2900420 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               cA   JA  20070812185304  20090916023812  A5_21330_099                    2C  D   APEX-SBE 1093                                                   846 @Ԍ��Xv1   @Ԍ���a@8�;dZ�@b!�hr�!1   ARGOS   A   A   A   @���AffAc33A���A�33A�ffBffB��B0  BFffBX��Bm33B���B���B�33B�ffB�33B�  B�  B�ffB���B���B䙚B�ffB�33C�C  C�CL�CL�CffC��C$L�C)33C.L�C2�fC7�fC=  CBL�CG  CQ33CZ�3Ce�CoL�CyffC�� C��fC���C��3C���C���C���C���C���C�� C��fC��fC���C³3CǦfC̙�Cр C֌�Cۙ�C���C噚CꙚC�� C��fC���D� DٚD�3D�3DٚD3D"FfD(��D.�fD4�3D;@ DA� DG� DN�DTFfDZs3D`��Dg3DmFfDs�3Dy��D�#3D�l�D��fD�ٚD��D�i�D���D��D��D�Y�D���D�� D�#3D�l�DڦfD�� D�  D�` D�D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffAc33A���A�33A�ffBffB��B0  BFffBX��Bm33B���B���B�33B�ffB�33B�  B�  B�ffB���B���B䙚B�ffB�33C�C  C�CL�CL�CffC��C$L�C)33C.L�C2�fC7�fC=  CBL�CG  CQ33CZ�3Ce�CoL�CyffC�� C��fC���C��3C���C���C���C���C���C�� C��fC��fC���C³3CǦfC̙�Cр C֌�Cۙ�C���C噚CꙚC�� C��fC���D� DٚD�3D�3DٚD3D"FfD(��D.�fD4�3D;@ DA� DG� DN�DTFfDZs3D`��Dg3DmFfDs�3Dy��D�#3D�l�D��fD�ٚD��D�i�D���D��D��D�Y�D���D�� D�#3D�l�DڦfD�� D�  D�` D�D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A�\A�x�A�dZA�XA�M�A�E�A�$�Aڏ\AӋDA�1'A���A��
A�A�v�A�A�ĜA��uA�z�A�$�A���A��A��DA�ƨA�v�A�ĜA�"�A�bNA��+A�ĜA�{A��yA�A�C�A�{A�"�A�^5A���A���A��9A��-A�v�A���A��A���A���A���A��RA�ƨA��#A���A��9A���A��A|�HAxn�As��ApE�Ak��Aj�+Ae�A`-A^bNAV�/AL�AH��AD�/A?�mA:^5A6{A/%A�^A�
A33A�A33@��
@�@��@�K�@�S�@�33@��u@�Ĝ@�p�@��@�ƨ@��h@x�`@qhs@mO�@d�@\�@Q7L@JM�@AG�@:J@3o@+��@'l�@#S�@9X@5?@=q@ff@	7L@�T@��?��w?���?��+111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A�\A�x�A�dZA�XA�M�A�E�A�$�Aڏ\AӋDA�1'A���A��
A�A�v�A�A�ĜA��uA�z�A�$�A���A��A��DA�ƨA�v�A�ĜA�"�A�bNA��+A�ĜA�{A��yA�A�C�A�{A�"�A�^5A���A���A��9A��-A�v�A���A��A���A���A���A��RA�ƨA��#A���A��9A���A��A|�HAxn�As��ApE�Ak��Aj�+Ae�A`-A^bNAV�/AL�AH��AD�/A?�mA:^5A6{A/%A�^A�
A33A�A33@��
@�@��@�K�@�S�@�33@��u@�Ĝ@�p�@��@�ƨ@��h@x�`@qhs@mO�@d�@\�@Q7L@JM�@AG�@:J@3o@+��@'l�@#S�@9X@5?@=q@ff@	7L@�T@��?��w?���?��+111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB+B+B%B+B+B1B
=BhBB49B33B2-B33B=qB=qB:^B<jB49B7LB49B.B%�B�BuBbB+B��B�B�yB�fB�BB��B��B�LB��B��B��B�bB}�Br�B_;BA�B7LB49B/B'�B�B\B
��B
�yB
�;B
��B
ȴB
�qB
��B
�VB
x�B
ffB
P�B
J�B
1'B
�B
{B	�B	ƨB	�?B	��B	�VB	u�B	cTB	E�B��B��B�B�oB~�BgmB`BB\)B\)BhsB�%B�B��B�;B��B	bB	�B	9XB	N�B	\)B	t�B	�JB	��B	�^B	��B	�ZB	�B
B
VB
�B
$�B
33B
<jB
E�B
S�B
]/B
dZB
l�B
r�B
w�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B+B+B%B+B+B1BDB�B
=B5?B49B5?B7LB>wB@�B<jB?}B7LB8RB5?B0!B'�B�B{BoB	7B��B�B�B�mB�TB��BÖB�^B��B��B��B�oB~�Bs�B`BBB�B7LB5?B0!B(�B�BbB
��B
�B
�BB
��B
ȴB
�wB
��B
�\B
y�B
gmB
P�B
K�B
2-B
�B
�B	�B	ǮB	�FB	��B	�\B	v�B	dZB	G�B��B��B�B�uB�BhsBaHB]/B]/BiyB�+B�B��B�;B��B	bB	�B	9XB	N�B	\)B	t�B	�JB	��B	�^B	��B	�ZB	�B
B
VB
�B
$�B
33B
<jB
E�B
S�B
]/B
dZB
l�B
r�B
w�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<T��<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200708251435532007082514355320070825143553200708251444252007082514442520070825144425200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20070812185302  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070812185304  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070812185305  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070812185305  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070812185309  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070812185309  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070812185309  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070812185309  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070812185310  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070812185310  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070812191055                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070816155007  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070816155023  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070816155025  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070816155026  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070816155041  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070816155041  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070816155041  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070816155041  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070816155042  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070816155042  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070816161654                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070816155007  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090414022145  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090414022146  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090414022146  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090414022146  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090414022147  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090414022147  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090414022147  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090414022147  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090414022147  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090414022437                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070815234441  CV  DAT$            G�O�G�O�F�dq                JM  ARCAJMQC1.0                                                                 20070825143553  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070825143553  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070825144425  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916023448  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916023812                      G�O�G�O�G�O�                