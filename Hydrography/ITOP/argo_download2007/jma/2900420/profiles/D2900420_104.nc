CDF   '   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   o   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Eh   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Nh   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Wh   CALIBRATION_DATE      	   
                
_FillValue                  �  `h   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a8   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    aH   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    aL   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a\   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a`   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        ad   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    ahArgo profile    2.2 1.2 19500101000000  2900420 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               hA   JA  20071001185230  20090916023816  A5_21330_104                    2C  D   APEX-SBE 1093                                                   846 @ԙ�!�1   @ԙ��Է@9��+J@b$1&�y1   ARGOS   A   A   A   @���A��A`  A�ffAř�A�  B
  B33B2  BC33BXffBi��B�  B�33B�  B�ffB���B�33B�33BǙ�BЙ�B�33B�  B�ffB�ffC �3C�fC33C�3C�fC��CffC$L�C)L�C.L�C3ffC7��C<�fCA�fCGL�CQ�C[�3CeL�Co  Cy�C�� C��fC�L�C�33C�� C�s3C�� C��fC���C���C���C��fC��fC�Y�C�ffC�s3Cѳ3C֦fC���C�� C�fC� C��C��fC��3D� D� DٚD�3D��D  D"L�D(��D.��D4�fD;9�DA� DG� DN  DT@ DZ�fD`�3Dg�DmY�Ds� Dy�3D��D�S3D��fD��fD�)�D�` D��fD���D�&fD�VfD�� D���D��D�c3Dڬ�D��D�3D�VfD��D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��A`  A�ffAř�A�  B
  B33B2  BC33BXffBi��B�  B�33B�  B�ffB���B�33B�33BǙ�BЙ�B�33B�  B�ffB�ffC �3C�fC33C�3C�fC��CffC$L�C)L�C.L�C3ffC7��C<�fCA�fCGL�CQ�C[�3CeL�Co  Cy�C�� C��fC�L�C�33C�� C�s3C�� C��fC���C���C���C��fC��fC�Y�C�ffC�s3Cѳ3C֦fC���C�� C�fC� C��C��fC��3D� D� DٚD�3D��D  D"L�D(��D.��D4�fD;9�DA� DG� DN  DT@ DZ�fD`�3Dg�DmY�Ds� Dy�3D��D�S3D��fD��fD�)�D�` D��fD���D�&fD�VfD�� D���D��D�c3Dڬ�D��D�3D�VfD��D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��AݍPA�A�A� �A�JA�n�A���A�G�A���A��RA�A��+A��A���A�Q�A�A�A��A�1'A���A�{A���A�
=A��^A��+A�33A���A�XA��mA��RA��mA�9XA��wA�E�A�%A��HA�A�p�A��!A��A���A���A�  A�bA��A�A��TA�ZA��!A}�;AxZAw�hAt5?Ar��Aq;dAl�\Ai�7A`�A]�A[
=AT�/AOhsAKS�AG�7ADE�A=�wA6^5A3O�A.E�A+?}A'`BA%XAv�A�A��@�v�@��@�+@ԋD@���@�5?@���@�G�@��@��T@�o@�A�@��
@�~�@���@~$�@s�F@n5?@gK�@^V@Z�@J��@A7L@9��@41@+��@��@I�@�@�@��@
-@+@dZ?�{?�C�?�
=?�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 AݍPA�A�A� �A�JA�n�A���A�G�A���A��RA�A��+A��A���A�Q�A�A�A��A�1'A���A�{A���A�
=A��^A��+A�33A���A�XA��mA��RA��mA�9XA��wA�E�A�%A��HA�A�p�A��!A��A���A���A�  A�bA��A�A��TA�ZA��!A}�;AxZAw�hAt5?Ar��Aq;dAl�\Ai�7A`�A]�A[
=AT�/AOhsAKS�AG�7ADE�A=�wA6^5A3O�A.E�A+?}A'`BA%XAv�A�A��@�v�@��@�+@ԋD@���@�5?@���@�G�@��@��T@�o@�A�@��
@�~�@���@~$�@s�F@n5?@gK�@^V@Z�@J��@A7L@9��@41@+��@��@I�@�@�@��@
-@+@dZ?�{?�C�?�
=?�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�B
�B
�B
�yB
�/B
�ZB
�TB
��B33B49B33B5?B/BG�BO�BM�BG�BI�BK�BB�BA�B@�B?}B=qB<jB:^B9XB7LB6FB49B33B2-B2-B1'B0!B.B,B)�B'�B$�B �BuBB
�B
��B
ĜB
�XB
��B
�oB
�JB
}�B
w�B
o�B
ZB
K�B
$�B
�B
	7B	�B	�B	ƨB	�LB	��B	�VB	jB	\)B	H�B	<jB	-B	�B	  B��B�oB�%BbNBS�BQ�BQ�BcTBr�B�\B��B�BŢB��B�B�B	%B	+B	J�B	ZB	m�B	�+B	�bB	�XB	��B	�NB	�B
B
�B
%�B
0!B
9XB
I�B
O�B
W
B
`BB
jB
n�B
s�B
u�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�B
�B
�B
��B
�BB
�fB
�BB49B49B5?B6FB6FBI�BP�BM�BJ�BL�BO�BD�BA�B@�B?}B=qB=qB:^B9XB8RB7LB49B33B2-B2-B1'B0!B/B-B)�B(�B$�B!�B{BB
�B
��B
ĜB
�^B
��B
�oB
�PB
}�B
w�B
p�B
[#B
M�B
%�B
�B
DB	�B	�B	ǮB	�RB	��B	�bB	k�B	]/B	I�B	=qB	.B	�B	B��B�uB�1BcTBS�BR�BR�BdZBs�B�\B��B�BŢB��B�B�B	%B	+B	J�B	ZB	m�B	�+B	�bB	�XB	��B	�NB	�B
B
�B
%�B
0!B
9XB
I�B
O�B
W
B
`BB
jB
n�B
s�B
u�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<T��<#�
<#�
<#�
<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710141353442007101413534420071014135344200710141354592007101413545920071014135459200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20071001185227  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071001185230  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071001185230  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071001185231  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071001185235  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071001185235  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071001185235  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071001185235  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071001185235  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071001190938                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20071005035104  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071005035109  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071005035109  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071005035110  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071005035113  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071005035113  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071005035114  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071005035114  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071005035114  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071005050652                      G�O�G�O�G�O�                JA  ARUP                                                                        20071005052352                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20071005035104  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090414022157  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090414022158  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090414022158  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090414022158  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090414022159  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090414022159  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090414022159  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090414022159  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090414022159  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090414022434                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071004224840  CV  DAT$            G�O�G�O�F��.                JM  ARCAJMQC1.0                                                                 20071014135344  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071014135344  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071014135459  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916023454  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916023816                      G�O�G�O�G�O�                