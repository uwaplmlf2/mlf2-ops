CDF   &   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   o   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Eh   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Nh   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Wh   CALIBRATION_DATE      	   
                
_FillValue                  �  `h   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a8   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    aH   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    aL   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a\   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a`   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        ad   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    ahArgo profile    2.2 1.2 19500101000000  2900420 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               iA   JA  20071011185240  20090916023816  A5_21330_105                    2C  D   APEX-SBE 1093                                                   846 @ԛ�W�1   @ԛ��8!S@:W�O�;d@b����1   ARGOS   A   A   A   @�  A  Aa��A�33A�  A陚B	��B33B2��BE��B[33Bn  B���B���B�ffB���B�33B�ffB�  B�  B�  B���B���B���B�ffCffCffC  C  C�C33CL�C#��C)�C.��C3ffC8� C=� CB��CG� CQ� CZ�fCeffCoL�CyffC���C��fC���C���C���C�ffC��fC��fC�ffC�� C��3C��fC��3C¦fCǙ�C�ffCр C֌�C�Y�C�@ C��C�fC�Y�C��3C���DٚD� D�fD��D��D3D"FfD(y�D.�3D5�D;S3DA��DG�fDN  DTL�DZ�3D`�fDg  Dm@ Ds�3DyٚD�  D�ffD���D�� D��D�i�D��3D���D�&fD�i�D��3D�ٚD�0 D�l�Dڬ�D�� D��D�c3D�3D�9�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A  Aa��A�33A�  A陚B	��B33B2��BE��B[33Bn  B���B���B�ffB���B�33B�ffB�  B�  B�  B���B���B���B�ffCffCffC  C  C�C33CL�C#��C)�C.��C3ffC8� C=� CB��CG� CQ� CZ�fCeffCoL�CyffC���C��fC���C���C���C�ffC��fC��fC�ffC�� C��3C��fC��3C¦fCǙ�C�ffCр C֌�C�Y�C�@ C��C�fC�Y�C��3C���DٚD� D�fD��D��D3D"FfD(y�D.�3D5�D;S3DA��DG�fDN  DTL�DZ�3D`�fDg  Dm@ Ds�3DyٚD�  D�ffD���D�� D��D�i�D��3D���D�&fD�i�D��3D�ٚD�0 D�l�Dڬ�D�� D��D�c3D�3D�9�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�~�A�E�A�(�A�$�A� �A�{A�A���AۼjA��A��mA�p�A�bA���A�A�l�A�?}A��A�x�A�A��A��A��HA��#A���A���A��mA���A��PA��mA�ĜA���A�;dA��mA� �A��9A�ĜA�|�A�E�A��FA���A���A��yA��^A��A� �A�;dA�/A�bA�-A}��AwK�At��Ap=qAn�DAe��Ac
=A]�AY�AQ+AMS�AK�AH  AAC�A=�#A933A6��A0�+A*�yA%��AVA�+A��@�Q�@���@�@�X@��@���@�C�@�  @�ff@�@��D@���@��@|z�@w|�@o\)@n@i&�@Y�#@N��@IG�@D�D@>��@9�@5�h@.$�@'
=@!�#@�7@1@b@�@bN@��@�H@��?�Q�?��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�~�A�E�A�(�A�$�A� �A�{A�A���AۼjA��A��mA�p�A�bA���A�A�l�A�?}A��A�x�A�A��A��A��HA��#A���A���A��mA���A��PA��mA�ĜA���A�;dA��mA� �A��9A�ĜA�|�A�E�A��FA���A���A��yA��^A��A� �A�;dA�/A�bA�-A}��AwK�At��Ap=qAn�DAe��Ac
=A]�AY�AQ+AMS�AK�AH  AAC�A=�#A933A6��A0�+A*�yA%��AVA�+A��@�Q�@���@�@�X@��@���@�C�@�  @�ff@�@��D@���@��@|z�@w|�@o\)@n@i&�@Y�#@N��@IG�@D�D@>��@9�@5�h@.$�@'
=@!�#@�7@1@b@�@bN@��@�H@��?�Q�?��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBBBBB%B%BB
��B
�)B
ŢB
�RB
ǮB
��B
�HB
��BuB�BF�BS�BL�BI�BD�BB�BB�BH�BH�BE�BB�B@�B<jB:^B6FB33B49B33B2-B0!B.B-B+B(�B(�B�B�BVBB
��B
�B
�5B
ǮB
��B
�PB
|�B
k�B
aHB
;dB
.B
�B
B	�HB	��B	ɺB	�LB	��B	�JB	y�B	k�B	R�B	8RB	�B�5B��B��B�Bx�BiyB`BB\)B^5Bs�B��B��BǮB�fB�B	{B	0!B	=qB	T�B	YB	ffB	�hB	�B	�jB	ǮB	�B	�TB	�B	��B
VB
�B
,B
9XB
B�B
G�B
T�B
\)B
bNB
e`B
r�B
y�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 BBBBB%B%B%B
��B
�BB
��B
�qB
��B
��B
�`B
��B�B"�BJ�BT�BM�BK�BG�BD�BE�BJ�BJ�BF�BC�BA�B=qB;dB7LB33B5?B33B33B0!B.B.B+B(�B(�B�B�B\BB
��B
�B
�;B
ɺB
��B
�VB
}�B
k�B
cTB
<jB
/B
�B
%B	�NB	��B	��B	�XB	��B	�PB	z�B	m�B	S�B	9XB	!�B�BB��B��B�%By�BjBaHB]/B_;Bt�B��B��BǮB�fB�B	{B	0!B	=qB	T�B	YB	ffB	�hB	�B	�jB	ǮB	�B	�TB	�B	��B
VB
�B
,B
9XB
B�B
G�B
T�B
\)B
bNB
e`B
r�B
y�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710142253422007102601394020071026013940200710260156052007102601560520071026015605200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20071011185238  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071011185240  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071011185241  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071011185241  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071011185245  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071011185245  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071011185245  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071011185245  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071011185246  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071011191109                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20071015154116  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071015154121  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071015154121  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071015154122  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071015154126  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071015154126  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071015154126  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071015154126  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071015154127  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071015190600                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20071015154116  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090414022159  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090414022200  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090414022200  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090414022200  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090414022201  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090414022201  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090414022201  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090414022201  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090414022201  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090414022433                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071014225342  CV  DAT$            G�O�G�O�F��q                JM  ARCAJMQC1.0                                                                 20071026013940  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071026013940  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071026015605  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916023453  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916023816                      G�O�G�O�G�O�                