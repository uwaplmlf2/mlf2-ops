CDF   (   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   o   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Eh   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Nh   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Wh   CALIBRATION_DATE      	   
                
_FillValue                  �  `h   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a8   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    aH   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    aL   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         a\   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         a`   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        ad   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    ahArgo profile    2.2 1.2 19500101000000  2900420 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               eA   JA  20070901190349  20090916023819  A5_21330_101                    2C  D   APEX-SBE 1093                                                   846 @ԑ�&��1   @ԑ�P�?�@94z�G�@b8�j~��1   ARGOS   A   A   A   @�33A��A^ffA�  A�  A홚B
ffB��B2��BF��BZffBm��B�  B�  B�ffB���B���B���B�  B�33BЙ�B���B䙚BB���C  CL�C33C33CffC�fC  C$33C)  C-��C3ffC8ffC<��CB�CF�3CP�fC[ffCe� Co� CyL�C��3C���C���C�ffC�Y�C��fC�s3C��3C��3C���C��fC�� C��fC�Cǳ3C̀ C�ffC֦fC�s3C�s3C� C�fC��C��3C���D�3D� DٚD� D� DfD"Y�D(��D.��D4��D;S3DA��DG� DN�DTS3DZs3D`�fDg&fDmS3Ds�3DyٚD��D�ffD�� D���D�,�D�i�D��fD��fD�#3D�ffD���D���D�&fD�l�Dڬ�D��3D�  D�c3D��D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��A^ffA�  A�  A홚B
ffB��B2��BF��BZffBm��B�  B�  B�ffB���B���B���B�  B�33BЙ�B���B䙚BB���C  CL�C33C33CffC�fC  C$33C)  C-��C3ffC8ffC<��CB�CF�3CP�fC[ffCe� Co� CyL�C��3C���C���C�ffC�Y�C��fC�s3C��3C��3C���C��fC�� C��fC�Cǳ3C̀ C�ffC֦fC�s3C�s3C� C�fC��C��3C���D�3D� DٚD� D� DfD"Y�D(��D.��D4��D;S3DA��DG� DN�DTS3DZs3D`�fDg&fDmS3Ds�3DyٚD��D�ffD�� D���D�,�D�i�D��fD��fD�#3D�ffD���D���D�&fD�l�Dڬ�D��3D�  D�c3D��D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�?}A� �A��A�JA��A�A�(�A�+A���A���A�G�A�bNA���A��A��FA��uA�%A��+A�A�$�A��;A�JA�O�A��A��A�7LA�v�A�bNA���A���A�jA�n�A���A�=qA���A�z�A�"�A��hA�p�A��FA��A���A�^5A��A�Q�A�K�A�
=A��+A��A��jA���A��A�7LA��HA|~�At9XAj�jAg�wAeA`=qA^A�A]��AX~�ASx�AI�TAC��A8I�A2�A&I�A|�A��A	%A�/@�(�@�I�@�A�@�@��P@�$�@�1'@��7@�V@�?}@�j@�?}@�?}@�`B@�M�@}O�@u�-@nv�@[dZ@P�9@G�@?|�@9hs@5�@.v�@)&�@#"�@�@��@z�@;d@@��@33@�7?�v�?�Q�?�9X111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�?}A� �A��A�JA��A�A�(�A�+A���A���A�G�A�bNA���A��A��FA��uA�%A��+A�A�$�A��;A�JA�O�A��A��A�7LA�v�A�bNA���A���A�jA�n�A���A�=qA���A�z�A�"�A��hA�p�A��FA��A���A�^5A��A�Q�A�K�A�
=A��+A��A��jA���A��A�7LA��HA|~�At9XAj�jAg�wAeA`=qA^A�A]��AX~�ASx�AI�TAC��A8I�A2�A&I�A|�A��A	%A�/@�(�@�I�@�A�@�@��P@�$�@�1'@��7@�V@�?}@�j@�?}@�?}@�`B@�M�@}O�@u�-@nv�@[dZ@P�9@G�@?|�@9hs@5�@.v�@)&�@#"�@�@��@z�@;d@@��@33@�7?�v�?�Q�?�9X111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��B��B��B��B��B�B
��B
ȴB
�/B
�/B
��B  B�B$�B �B;dBD�BG�BE�BG�BJ�BG�BG�BH�BE�BE�BE�BC�BA�BA�B@�B?}B<jB9XB9XB7LB5?B5?B49B2-B1'B0!B.B+B!�B�B{B	7BB
��B
�B
�TB
��B
�jB
��B
|�B
R�B
E�B
7LB
#�B
�B
�B	��B	�TB	�wB	��B	q�B	T�B	�B�B�FB��B�+Bu�BbNBbNB^5B^5BcTBq�B�=B��B�RB��B�NB��B	JB	�B	2-B	J�B	`BB	�\B	�!B	B	�B	�NB	�B	��B
1B
{B
�B
,B
:^B
D�B
O�B
YB
aHB
e`B
k�B
r�B
w�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B��B��B��B��B��B&�B
�B
��B
�HB
�NB
��BB�B%�B&�B=qBD�BH�BF�BH�BK�BI�BH�BI�BG�BF�BF�BD�BB�BA�BA�B@�B=qB:^B9XB7LB6FB5?B5?B2-B1'B0!B/B,B!�B�B�B	7B%B
��B
�B
�ZB
�B
�qB
��B
~�B
S�B
F�B
8RB
$�B
�B
�B	��B	�`B	��B	��B	s�B	XB	�B�B�LB��B�1Bv�BcTBcTB_;B_;BdZBr�B�=B��B�RB��B�NB��B	JB	�B	2-B	J�B	`BB	�\B	�!B	B	�B	�NB	�B	��B
1B
{B
�B
,B
:^B
D�B
O�B
YB
aHB
e`B
k�B
r�B
w�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<D��<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709130810302007091308103020070913081030200709130816312007091308163120070913081631200909110000002009091100000020090911000000  JA  ARFMdecpA5_a                                                                20070901190332  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070901190349  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070901190351  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070901190354  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070901190402  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070901190402  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070901190402  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070901190402  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070901190404  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070901190404  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070901205803                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070905155052  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070905155101  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20070905155101  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070905155102  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070905155109  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20070905155109  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070905155109  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20070905155109  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070905155110  QCP$                G�O�G�O�G�O�           10000JA  ARGQaqcpt16a                                                                20070905155110  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070905161734                      G�O�G�O�G�O�                JA  ARFMdecpA5_a                                                                20070905155052  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090414022150  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090414022151  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090414022151  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090414022151  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090414022152  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090414022152  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090414022152  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090414022152  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090414022152  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090414022425                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070904231052  CV  DAT$            G�O�G�O�F��{                JM  ARCAJMQC1.0                                                                 20070913081030  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070913081030  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070913081631  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090911000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090916023500  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090916023819                      G�O�G�O�G�O�                