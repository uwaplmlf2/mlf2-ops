CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901521 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070806185922  20081017093536  A9_66098_017                    2C  D   APEX-SBE 2807                                                   846 @ԋ�	�[1   @ԋ���@1��t�j@b����l�1   ARGOS   A   A   A   @���A��AfffA�33A�33A���B	33B  B133BE��BY��Bm��B�  B���B���B�33B���B���B�ffB�33B�33B�ffB�  B�33B���C� CffCffCffC33CffC  C$ffC)33C.L�C3�C8  C=ffCB� CGffCQffC[L�CeffCn��Cy��C���C���C���C�� C�� C��fC���C���C��3C���C��fC��3C�s3C Cǀ C̀ CѦfC֙�Cۙ�C���C���C�� CC��fC�� D� DٚD� D�fD� D��D� D$�3D)ٚD.ٚD3��D8� D=�fDBٚDG�3DL� DQ�3DV�3D[�fD`ٚDe��Dj�fDo�3Dt�3Dy��D��D�i�D���D��3D�0 D�ffD���D�� D�&fD�ffD���D�� D�)�D�i�Dڬ�D���D�)�D�ffD��D�9�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA33Ad��A�ffA�ffA�  B��B��B0��BE33BY33Bm33B���B�ffB���B�  B�ffB���B�33B�  B�  B�33B���B�  B�ffCffCL�CL�CL�C�CL�C�fC$L�C)�C.33C3  C7�fC=L�CBffCGL�CQL�C[33CeL�Cn�3Cy� C�� C���C���C�s3C��3C���C�� C�� C��fC���C���C��fC�ffC�s3C�s3C�s3Cљ�C֌�Cی�C�� C�� C�3C��C���C�s3DٚD�3DٚD� DٚD�fD��D$��D)�3D.�3D3�3D8ٚD=� DB�3DG��DL��DQ��DV��D[� D`�3De�fDj� Do��Dt��Dy�3D��D�ffD��fD�� D�,�D�c3D��fD���D�#3D�c3D��fD���D�&fD�ffDک�D��D�&fD�c3D�D�6f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A�$�A�/A�33A�33A���A��A��A�7LA�AAAA�ZA��A�\)A؇+A�z�AЇ+A�"�A�z�A�jA�z�A�^5A���A���A�E�A�~�A��A�  A��A�v�A�n�A��A��!A�x�A��A�A��A���A���A�33A��A���A�E�A�C�A���A~�/Aw��AmS�AghsAd�uAY�FAU��AM�TAI"�AG�A>��A6-A1S�A,�yA*A�A%7LA �jA|�A�Ar�A
�`A��AJ@��@�x�@�%@���@�|�@�z�@�$�@��@�7L@���@�C�@�9X@���@��/@��\@�G�@�O�@���@�Q�@�V@��`@���@��@}�@x��@q��@bM�@W�w@M�h@E�@>�y@6��@17L@+��@'l�@ �`@~�@
=@j@�^@�T@�j@	%@5?@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A�$�A�/A�33A�33A���A��A��A�7LA�AAAA�ZA��A�\)A؇+A�z�AЇ+A�"�A�z�A�jA�z�A�^5A���A���A�E�A�~�A��A�  A��A�v�A�n�A��A��!A�x�A��A�A��A���A���A�33A��A���A�E�A�C�A���A~�/Aw��AmS�AghsAd�uAY�FAU��AM�TAI"�AG�A>��A6-A1S�A,�yA*A�A%7LA �jA|�A�Ar�A
�`A��AJ@��@�x�@�%@���@�|�@�z�@�$�@��@�7L@���@�C�@�9X@���@��/@��\@�G�@�O�@���@�Q�@�V@��`@���@��@}�@x��@q��@bM�@W�w@M�h@E�@>�y@6��@17L@+��@'l�@ �`@~�@
=@j@�^@�T@�j@	%@5?@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
  B
B
1B

=B

=B
�B
�B
#�B
.B
D�B
dZB
dZB
cTB
�uB
ÖBR�Bt�B� B�DB��B��B�-B��B�FBÖBȴB��B�)B�;BBuB
=BB	7B��B�B�
B��B�wB�3B��Bv�B:^B
��B
��B
�3B
��B
r�B
I�B
!�B
%B	��B	��B	�RB	��B	�B	x�B	\)B	<jB	+B	!�B	�B	B�B��B�}B�-B��B��B�{B��B�oB�uB��B�'B��B�5B	+B	�B	'�B	0!B	=qB	J�B	\)B	e`B	m�B	}�B	�PB	��B	�RB	ȴB	��B	�;B	�mB	�B
	7B
JB
�B
$�B
.B
49B
>wB
D�B
H�B
L�B
R�B
ZB
]/B
aHB
ffB
m�B
q�B
t�B
|�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
  B
B
1B

=B

=B
�B
�B
#�B
.B
D�B
dZB
dZB
e`B
��B
��BVBw�B�B�JB��B�B�LB�B�RBŢB��B�B�)B�NB%B{BDBBPB��B�B�B�B��B�9B��Bx�B=qB
��B
��B
�9B
��B
t�B
K�B
#�B
+B	��B	��B	�^B	��B	�%B	z�B	^5B	=qB	,B	"�B	�B	B�B��B��B�9B��B��B��B��B�uB�{B��B�-B��B�;B	+B	�B	'�B	0!B	=qB	J�B	\)B	e`B	m�B	}�B	�PB	��B	�RB	ȴB	��B	�;B	�mB	�B
	7B
JB
�B
$�B
.B
49B
>wB
D�B
H�B
L�B
R�B
ZB
]/B
aHB
ffB
m�B
q�B
t�B
|�B
�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<49X<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.0, NextCycleSSP=0.1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : max(CTM adjustment , 0.01(PSS-78))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20070819202036              20070819202036                            20070819202446                            20080825000000  JA  ARFMdecpA9_b                                                                20070806185920  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070806185922  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070806185923  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070806185927  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070806185927  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070806185928  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070806190633                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070810155827  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070810155830  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070810155831  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070810155835  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070810155835  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070810155835  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070810155835  CV  TIME            G�O�G�O�F�X�                JA  ARGQrelo2.1                                                                 20070810155835  CV  LAT$            G�O�G�O�A��                JA  ARGQrelo2.1                                                                 20070810155835  CV  LON$            G�O�G�O�CD                JA  ARUP                                                                        20070810160639                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070819202036  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070819202036  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070819202446  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080825000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081017073849  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081017093536                      G�O�G�O�G�O�                