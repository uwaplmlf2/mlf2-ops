CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901521 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070827013235  20081017093647  A9_66098_019                    2C  D   APEX-SBE 2807                                                   846 @Ԑ<�)1   @Ԑ �=@2��-V@b�z�H1   ARGOS   A   A   A   @���A  Ac33A�  A�33A陚B
  B33B1��BF��BY33BlffB���B�  B���B�33B���B�  B�  B�  B���B���B�  B���B�  C�C� C�3CffC� CL�CffC$ffC)ffC.L�C3L�C8L�C=� CB  CG� CQ33C[��Ce� Co�Cy33C��fC�� C��fC��3C��3C��3C���C���C���C���C��fC�� C��fC�� CǦfC̙�Cр Cր Cی�C�3C�� C�fCC��fC���D� D��D� D��D��D�3D��D$�3D)�3D.�3D3ٚD8ٚD=��DB�3DG��DL��DQ��DV� D[ٚD`� DeٚDj�fDo� Dt�fDy�fD�#3D�i�D���D��fD�#3D�ffD��fD��D�)�D�c3D���D���D��D�i�Dک�D��3D�  D�\�D�fD�31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffAa��A�33A�ffA���B	��B��B133BFffBX��Bl  B���B���B���B�  B���B���B���B���BЙ�Bڙ�B���BB���C  CffC��CL�CffC33CL�C$L�C)L�C.33C333C833C=ffCA�fCGffCQ�C[� CeffCo  Cy�C���C��3C���C��fC��fC��fC�� C�� C���C���C���C��3C���C³3CǙ�Č�C�s3C�s3Cۀ C�fC�3CꙚC��C���C�� D��D�fD��D�fD�fD��D�fD$��D)��D.��D3�3D8�3D=�3DB��DG�fDL�3DQ�3DV��D[�3D`ٚDe�3Dj� Do��Dt� Dy� D�  D�ffD���D��3D�  D�c3D��3D��fD�&fD�` D��fD�ٚD�fD�ffDڦfD�� D��D�Y�D�3D� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�?}A�C�A��A�t�A�jA�bNA�S�A�O�A�A�A���A���A�t�A�ȴA�A�~�A�9XA�ƨA�VA��#A��mAǅA�Aŝ�A�~�A�33A��hA�^5A�A���A�(�A�VA�ZA�;dA��HA��A��7A�Q�A���A��A���A��!A�\)A���A�A�A�t�A�r�A���A��#A�XAyG�Au�Aq�Aj�Ad�A[�wAU��AMhsAF�A?/A8 �A1�TA)XA$9XAVA;dA\)A;dA��A ��@�@��/@�/@�%@أ�@��#@ȃ@�7L@�S�@��@�@��@�ƨ@�K�@��@���@���@��@��@�@�+@��7@�1'@���@|��@w�@nE�@e��@Z�\@P��@JM�@B-@:�\@4�D@-V@'\)@ �u@�!@{@��@�@
n�@l�@��@��@ bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�?}A�C�A��A�t�A�jA�bNA�S�A�O�A�A�A���A���A�t�A�ȴA�A�~�A�9XA�ƨA�VA��#A��mAǅA�Aŝ�A�~�A�33A��hA�^5A�A���A�(�A�VA�ZA�;dA��HA��A��7A�Q�A���A��A���A��!A�\)A���A�A�A�t�A�r�A���A��#A�XAyG�Au�Aq�Aj�Ad�A[�wAU��AMhsAF�A?/A8 �A1�TA)XA$9XAVA;dA\)A;dA��A ��@�@��/@�/@�%@أ�@��#@ȃ@�7L@�S�@��@�@��@�ƨ@�K�@��@���@���@��@��@�@�+@��7@�1'@���@|��@w�@nE�@e��@Z�\@P��@JM�@B-@:�\@4�D@-V@'\)@ �u@�!@{@��@�@
n�@l�@��@��@ bN1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
��B
��B
�dB
�^B
�qB
��B
ÖB
ÖB
ÖB
�sB8RBL�B_;Bo�B�oB��B��B��B	7BoB{BhBbBbB{B�B�B)�B{B�B�B��B�BŢBǮBÖB�B�{B�B|�Be`B2-B�B%B
��B
�B
ĜB
��B
�DB
gmB
P�B
5?B
oB	�B	ǮB	�B	�\B	u�B	\)B	D�B	.B��B�B�5B�)B�'B�FB��B��B�JB�bB��B�bB��B�?BŢB�B�TB��B	�B	,B	=qB	^5B	p�B	}�B	�JB	��B	�B	�LB	ÖB	��B	��B	�/B	�ZB	�yB	��B
JB
�B
#�B
-B
8RB
@�B
C�B
I�B
N�B
VB
]/B
aHB
hsB
m�B
p�B
t�B
x�B
z�B
~�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
��B
��B
�dB
�^B
�qB
��B
ÖB
ÖB
ƨB
�B;dBO�BaHBr�B��B��B  B��B
=BuB�BhBoBhB�B�B�B-B�B��B��B��B�)BǮBȴBǮB�'B��B�B~�BhsB33B�B+B
��B
�
B
ŢB
��B
�PB
hsB
Q�B
7LB
{B	��B	ɺB	�'B	�hB	w�B	^5B	F�B	0!B	  B�B�;B�;B�-B�LB��B��B�PB�bB��B�hB��B�?BƨB�B�ZB��B	�B	,B	=qB	^5B	p�B	}�B	�JB	��B	�B	�LB	ÖB	��B	��B	�/B	�ZB	�yB	��B
JB
�B
#�B
-B
8RB
@�B
C�B
I�B
N�B
VB
]/B
aHB
hsB
m�B
p�B
t�B
x�B
z�B
~�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.1, NextCycleSSP=0.1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : max(CTM adjustment , 0.01(PSS-78))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20070909021813              20070909021813                            20070909022940                            20080825000000  JA  ARFMdecpA9_b                                                                20070827013232  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070827013235  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070827013236  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070827013240  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070827013240  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070827013240  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070827014000                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070830154956  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070830154959  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070830155000  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070830155004  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070830155004  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070830155004  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070830160008                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070909021813  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070909021813  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070909022940  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080825000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081017073850  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081017093647                      G�O�G�O�G�O�                