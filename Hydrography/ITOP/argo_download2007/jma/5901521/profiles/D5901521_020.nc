CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901521 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070906025715  20081017093721  A9_66098_020                    2C  D   APEX-SBE 2807                                                   846 @Ԓ���1   @Ԓ����@2��7Kƨ@bj~��"�1   ARGOS   A   A   A   @�  A33Ai��A���A�33A�33B
  B��B2  BE33BZ��Bn  B���B�  B�  B���B�33B�33B�  B�  B�  B�  B�  B�33B���C� C� C33CL�C�3CL�CL�C$33C)33C.ffC2�fC7�fC=�CB�CGffCQL�C[L�Ce33Co33CyffC�� C���C�� C��fC��fC���C���C��fC���C��fC���C���C�� C³3C�s3C�ffCљ�C֙�C�ffC�ffC� C�fC�fC���C�� D�fD��D�3D�3D�3D� D� D$��D)��D.�3D3��D8ٚD=��DB� DG� DLٚDQ��DVٚD[�fD`��De� Dj��Do� Dt��Dy��D��D�i�D�� D��fD�)�D�c3D�� D���D�&fD�l�D���D��fD�  D�s3Dڬ�D��D�#3D�Y�D�D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��Ah  A�  A�ffA�ffB	��BffB1��BD��BZffBm��B�ffB���B���B�ffB�  B�  B���B���B���B���B���B�  B���CffCffC�C33C��C33C33C$�C)�C.L�C2��C7��C=  CB  CGL�CQ33C[33Ce�Co�CyL�C�s3C�� C��3C���C���C���C�� C���C�� C���C���C�� C��3C¦fC�ffC�Y�Cь�C֌�C�Y�C�Y�C�s3CꙚCC��C��3D� D�fD��D��D��DٚD��D$�fD)�fD.��D3�fD8�3D=�fDBٚDG��DL�3DQ�fDV�3D[� D`�3DeٚDj�fDoٚDt�fDy�fD��D�ffD���D��3D�&fD�` D���D��D�#3D�i�D��fD��3D��D�p Dک�D��fD�  D�VfD�fD�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���A�/A�/A�33AA�ĜA�jA��yA�^A�z�A�r�A�r�A�r�A�VAծAӛ�A�1'A�p�Aͺ^A��TA�=qA���A��hA�1'A���A�M�A�Q�A��A�z�A�G�A���A��hA��A���A�$�A��wA��^A��A�l�A��A��7A�ZA�JA�O�A��A��A� �A��#A|�AvQ�Ap�+Ai�;Aet�A^�AZn�AV��AS��AMO�AHE�ACK�A>A�A:=qA5�7A/t�A)%A$ĜAS�A��A�An�A��Ax�@�@��@�1'@�33@Ǖ�@��\@�z�@���@�C�@�l�@�x�@�x�@�z�@�I�@�r�@�v�@��@�\)@���@�E�@��@z-@vV@j�\@_�w@XQ�@Nv�@Hb@>{@4(�@/�;@*�\@'�@"�H@ b@�@ff@t�@Q�@�m@1'@@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���A�/A�/A�33AA�ĜA�jA��yA�^A�z�A�r�A�r�A�r�A�VAծAӛ�A�1'A�p�Aͺ^A��TA�=qA���A��hA�1'A���A�M�A�Q�A��A�z�A�G�A���A��hA��A���A�$�A��wA��^A��A�l�A��A��7A�ZA�JA�O�A��A��A� �A��#A|�AvQ�Ap�+Ai�;Aet�A^�AZn�AV��AS��AMO�AHE�ACK�A>A�A:=qA5�7A/t�A)%A$ĜAS�A��A�An�A��Ax�@�@��@�1'@�33@Ǖ�@��\@�z�@���@�C�@�l�@�x�@�x�@�z�@�I�@�r�@�v�@��@�\)@���@�E�@��@z-@vV@j�\@_�w@XQ�@Nv�@Hb@>{@4(�@/�;@*�\@'�@"�H@ b@�@ff@t�@Q�@�m@1'@@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�#B	�)B	�ZB
B
9XB
^5B
�
B%�BK�BffB��B�NB�B!�BC�BN�Be`BffBR�B"�B,B6FB7LB49B49B-B�BDB��B�B��B�}B��B��B��B�1B{�Bt�Bm�BXB<jB"�B�BuB+B
��B
�yB
��B
��B
gmB
D�B
�B
1B	�B	��B	��B	�FB	��B	~�B	iyB	S�B	?}B	-B	PB��B�mB��B�ZB�B��B��B�jB��B��B��B�RBÖB�;B�sB	  B	�B	/B	5?B	O�B	iyB	|�B	�oB	��B	�FB	B	��B	�
B	�BB	�B	�B
B
bB
�B
'�B
/B
7LB
8RB
@�B
G�B
K�B
T�B
\)B
_;B
cTB
gmB
k�B
q�B
s�B
v�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	�)B	�)B	�ZB
B
9XB
_;B
�B&�BN�BiyB��B�fB�B%�BE�BQ�BffBgmBXB%�B-B9XB;dB7LB6FB0!B"�BVB��B��B��BĜB��B��B��B�=B|�Bu�Bo�BYB>wB#�B�B{B1B
��B
�B
ÖB
��B
hsB
F�B
 �B

=B	�B	��B	B	�RB	��B	� B	jB	T�B	@�B	/B	\B��B�yB��B�ZB��B��B��B�wB��B��B��B�XBĜB�;B�sB	  B	�B	/B	5?B	O�B	iyB	|�B	�oB	��B	�FB	B	��B	�
B	�BB	�B	�B
B
bB
�B
'�B
/B
7LB
8RB
@�B
G�B
K�B
T�B
\)B
_;B
cTB
gmB
k�B
q�B
s�B
v�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.1, NextCycleSSP=0.1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : max(CTM adjustment , 0.01(PSS-78))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20070919162550              20070919162550                            20070919163708                            20080825000000  JA  ARFMdecpA9_b                                                                20070906025713  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070906025715  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070906025716  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070906025720  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070906025720  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070906025720  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070906030411                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070910041058  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070910041101  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070910041102  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070910041106  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070910041106  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070910041107  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070910042903                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070919162550  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070919162550  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070919163708  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080825000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081017073850  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081017093721                      G�O�G�O�G�O�                