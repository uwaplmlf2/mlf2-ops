CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   j   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   D�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   M�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   V�   CALIBRATION_DATE      	   
                
_FillValue                  �  _�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `    HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `$   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `(   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `,   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    `l   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    `|   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    `�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         `�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         `�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        `�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    `�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               :A   JA  20070921125743  20081008070853  A9_60134_058                    2C  D   APEX-SBE 2404                                                   846 @Ԗ>�(3{1   @Ԗ?����@.�?|�h@a�Q��1   ARGOS   A   A   A   @�ffA  Ai��A�33A���A�ffB	��B��B333BE��BY33Bl��B�33B�ffB���B�  B�  B���B���B���B�  B�  B�ffB���B���CffCffC  C33C�C� C�3C$ffC)� C.�3C3� C8� C=��CBL�CGL�CQL�C[ffCe��CoL�Cy  C���C���C���C�� C�� C���C�s3C���C��3C��fC��fC�� C���C¦fCǦfC̳3Cљ�C֌�Cۀ C�fC�3C��C�fC��C��3DٚD�3DٚD�3D� DٚD� D$�3D)�fD.ٚD3ٚD8�3D=�fDBٚDGٚDL� DQ�3DV�3D[� D`��De��Dj��Do�fDt��Dy��D�#3D�i�D��fD�� D�&fD�l�D��fD��3D�3D�l�D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�33AffAh  A�ffA�  A陚B	33B33B2��BE33BX��BlffB�  B�33B���B���B���B�ffB���Bƙ�B���B���B�33BB���CL�CL�C
�fC�C  CffC��C$L�C)ffC.��C3ffC8ffC=� CB33CG33CQ33C[L�Ce� Co33Cx�fC�� C���C�� C��3C��3C���C�ffC���C��fC���C���C��3C���C�CǙ�C̦fCь�Cր C�s3C���C�fC� CC� C��fD�3D��D�3D��D��D�3DٚD$��D)� D.�3D3�3D8��D=� DB�3DG�3DLٚDQ��DV��D[��D`�fDe�fDj�3Do� Dt�fDy�fD�  D�ffD��3D���D�#3D�i�D��3D�� D� D�i�D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�VA�A���A�~�A�jA��A�A�RAA���A�RA���A�+A㗍A��A���A��AށA��A��mA�+A�
=A��/A��yA�ȴA�=qAЕ�Aβ-A̟�A���Aɰ!A�S�A�7LA� �A�A�^5A��A���A�hsA��TA���A���A�A�A��A��9A���A}p�Am��AdQ�A^��AQp�AKC�AG%A=�A7�hA3�A-&�A&�A!�-A�+A+A�A�A/Av�A	x�A  A9XAx�@�b@�@��@�=q@۶F@�9X@��@�v�@Ĵ9@��@��/@��u@�9X@�(�@�1@�M�@��\@�Z@�hs@��H@���@�V@���@��@�~�@�  @���@��@{�@q�7@g
=@\�@T9X@M��@F�@@bN@@A�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�VA�A���A�~�A�jA��A�A�RAA���A�RA���A�+A㗍A��A���A��AށA��A��mA�+A�
=A��/A��yA�ȴA�=qAЕ�Aβ-A̟�A���Aɰ!A�S�A�7LA� �A�A�^5A��A���A�hsA��TA���A���A�A�A��A��9A���A}p�Am��AdQ�A^��AQp�AKC�AG%A=�A7�hA3�A-&�A&�A!�-A�+A+A�A�A/Av�A	x�A  A9XAx�@�b@�@��@�=q@۶F@�9X@��@�v�@Ĵ9@��@��/@��u@�9X@�(�@�1@�M�@��\@�Z@�hs@��H@���@�V@���@��@�~�@�  @���@��@{�@q�7@g
=@\�@T9X@M��@F�@@bN@@A�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	�B	�B	�B	[#B	x�B	��B	��B	��B	�BB
)�B
r�B
B
��B
w�B
��B
�BD�B\)B��B�1B�%B�oB��B�B�BB��B��B\B$�B!�B)�B.B �B'�B+B�BoB  B�TB��B_;B.BB
�XB
��B
�B
jB
(�B
B	�sB	�3B	��B	�1B	k�B	O�B	K�B	:^B	2-B	+B	'�B	&�B	=qB	?}B	[#B	T�B	L�B	>wB	A�B	G�B	VB	VB	`BB	v�B	~�B	�oB	��B	�B	�RB	��B	ɺB	��B	�/B	�mB	�B	�B	��B	��B
B
	7B
\B
hB
�B
�B
�B
�B
#�B
(�B
2-B
49B
:^B
>wB
D�B
H�B
L�B
P�B
P�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B	�B	�B	�B	[#B	x�B	��B	��B	��B	�HB
-B
s�B
ÖB
��B
y�B
��B
�BE�B]/B��B�DB�+B�uBB�
B�HB��B��BhB%�B#�B+B2-B%�B-B.B �B�BB�fBƨBbNB0!B%B
�dB
��B
�B
n�B
+B
B	�B	�?B	��B	�=B	m�B	P�B	M�B	<jB	33B	,B	(�B	'�B	>wB	?}B	\)B	VB	M�B	?}B	B�B	H�B	W
B	W
B	aHB	v�B	� B	�oB	��B	�B	�RB	��B	ɺB	��B	�/B	�mB	�B	�B	��B	��B
B
	7B
\B
hB
�B
�B
�B
�B
#�B
(�B
2-B
49B
:^B
>wB
D�B
H�B
L�B
P�B
P�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;��
;��
;��
;��
;��
;��
;��
;��
;��;��;��;��;��;��|;��;��;��;��;��;��;��;��;��;��;��;��;��|;��|;��;��|;��;ѷ;�;�;��;��;��;��;��;��$;��;��|;ѷ;��|;��;��;ѷ;��|;��;��;��|;��;��|;��|;��;��|;��|;��;��;��;��;��;��
;��;��;��;��;��;��;��;��;��;��
;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.1, NextCycleSSP=0.1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20071003133758              20071003133758                            20071003134413                            20080929000000  JA  ARFMdecpA9_b                                                                20070921125741  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070921125743  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070921125744  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070921125748  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070921125748  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070921125749  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070921130842                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070924042544  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070924042548  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070924042548  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070924042552  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070924042552  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070924042553  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070924050935                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070924000031  CV  DAT$            G�O�G�O�F���                JM  ARGQJMQC1.0                                                                 20070924000031  CV  LAT$            G�O�G�O�Aw��                JM  ARGQJMQC1.0                                                                 20070924000031  CV  LON$            G�O�G�O�C"                JM  ARCAJMQC1.0                                                                 20071003133758  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071003133758  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071003134413  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080929000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081008053306  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081008070853                      G�O�G�O�G�O�                