CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   i   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  C�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Dx   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Mx   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Vx   CALIBRATION_DATE      	   
                
_FillValue                  �  _x   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    _�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    _�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `    HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    `H   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    `X   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    `\   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         `l   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         `p   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        `t   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    `xArgo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               8A   JA  20070831125728  20081008070746  A9_60134_056                    2C  D   APEX-SBE 2404                                                   846 @ԑ@�B^�1   @ԑCY <�@-d�/��@a��t�1   ARGOS   A   A   A   @�  A��Ac33A�  A�33A�ffB��B33B1��BF  BXffBlffB���B�ffB�ffB�  B�  B�ffB���B�  B�33Bڙ�B�ffB���B�33C33C  CL�C��C� C� C�C$ffC)�3C.� C3ffC8ffC=� CBL�CG� CQ� C[�3Ce��Co��CyL�C�� C��3C���C���C��3C��fC��3C��fC�s3C���C���C���C���C�� Cǌ�C̳3C�� C�s3Cۙ�C���C噚CꙚC�3C��C���D�fDٚD�3DٚDٚD�3D� D$�3D)ٚD.ٚD3�fD8��D=�3DB�fDG��DL� DQٚDV��D[ٚD`� De�3Dj��Do� Dt�fDy��D�0 D�c3D��3D�� D�&fD�ffD��fD��3D�  D�� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @���A33Aa��A�33A�ffA陚BffB��B133BE��BX  Bl  B���B�33B�33B���B���B�33B���B���B�  B�ffB�33BB�  C�C�fC33C� CffCffC  C$L�C)��C.ffC3L�C8L�C=ffCB33CGffCQffC[��Ce� Co� Cy33C��3C��fC���C�� C��fC���C��fC���C�ffC���C�� C���C�� C³3Cǀ C̦fCѳ3C�ffCی�C�� C��C��C�fC� C�� D� D�3D��D�3D�3D��DٚD$��D)�3D.�3D3� D8�3D=��DB� DG�fDLٚDQ�3DV�fD[�3D`��De��Dj�fDo��Dt� Dy�fD�,�D�` D�� D���D�#3D�c3D��3D�� D��D�|�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�ĜAA���A���A��`A���A�;dA�~�AA�!A�jA�n�A�z�A�wA�"�A�O�A�v�A��HA�
=A�/A�/A���A��/A؉7A���A�^5A��;A���A�A�A̰!A�M�A��A�^5A���A��A�5?A���A�(�A��RA��A�p�A���A���A�-A�9XAz�yAg�wA_;dA[33AK�A=
=A9\)A/�TA(E�A$��A VA7LA��A1AI�A	�
A�;A�yA�@�&�@�"�@���@�7@�@�ff@���@��/@�@�G�@�r�@�dZ@�  @�S�@��9@��@�p�@��9@�@�Ĝ@�$�@��@�V@�X@��!@��@�Q�@���@�C�@��@�33@���@�?}@w��@jn�@d�@Y�@PĜ@G�@A�@7�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�ĜAA���A���A��`A���A�;dA�~�AA�!A�jA�n�A�z�A�wA�"�A�O�A�v�A��HA�
=A�/A�/A���A��/A؉7A���A�^5A��;A���A�A�A̰!A�M�A��A�^5A���A��A�5?A���A�(�A��RA��A�p�A���A���A�-A�9XAz�yAg�wA_;dA[33AK�A=
=A9\)A/�TA(E�A$��A VA7LA��A1AI�A	�
A�;A�yA�@�&�@�"�@���@�7@�@�ff@���@��/@�@�G�@�r�@�dZ@�  @�S�@��9@��@�p�@��9@�@�Ĝ@�$�@��@�V@�X@��!@��@�Q�@���@�C�@��@�33@���@�?}@w��@jn�@d�@Y�@PĜ@G�@A�@7�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	B	B	PB	!�B	"�B	 �B	�B	uB	 �B	,B	!�B	.B	<jB	�BB
 �B
k�B
��B
�}B
��BbB{B �B@�BB�BZB`BBw�B�DB��B��B�;B�`B%BJBJBB�B�}B��B�7B
��B
�B
��B
�?B
�1B
S�B
PB	�yB	��B	�DB	]/B	L�B	-B	�B	oB	B��B	B	�B	1'B	=qB	E�B	E�B	H�B	J�B	O�B	ZB	`BB	aHB	aHB	m�B	}�B	�7B	��B	��B	�-B	�XB	ĜB	ȴB	��B	�B	�`B	�B	��B	��B
B
B
JB
hB
�B
�B
�B
�B
 �B
$�B
'�B
.B
33B
8RB
:^B
?}B
D�B
J�B
O�B
V111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	B	B	PB	!�B	"�B	 �B	�B	uB	 �B	,B	!�B	.B	<jB	�NB
#�B
n�B
��B
��B
��BoB�B"�B@�BC�B[#BaHBx�B�VB��B��B�HB�B1BVBhB
=B�#BÖB��B�bB
��B
�B
��B
�RB
�DB
YB
\B	�B	��B	�\B	^5B	N�B	/B	�B	uB	%B��B	B	�B	2-B	>wB	F�B	F�B	I�B	K�B	O�B	ZB	aHB	bNB	aHB	n�B	~�B	�7B	��B	��B	�-B	�XB	ĜB	ȴB	��B	�B	�`B	�B	��B	��B
B
B
JB
hB
�B
�B
�B
�B
 �B
$�B
'�B
.B
33B
8RB
:^B
?}B
D�B
J�B
O�B
V111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;��
;��
;��
;��
;��
;��
;��;��
;��
;��
;��
;��
;��
;��|;��;��;��;��;��;��|;��;��|;��
;��;��;��;��;��;��;��|;��|;�;��|;��|;�;�;��|;ѷ;��|<�;��;ѷ;��;��;��;�;��|;��;ѷ;ѷ;��;��|;��|;��;��;��;��;��;��;��;��;��;��;��;��;��
;��
;��;��;��
;��;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.1, NextCycleSSP=0.1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20070913085215              20070913085215                            20070913090923                            20080929000000  JA  ARFMdecpA9_b                                                                20070831125726  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070831125728  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070831125729  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070831125733  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070831125733  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070831125734  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070831130839                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070904042340  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070904042343  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070904042344  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070904042348  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070904042349  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070904042349  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070904051137                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070913085215  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070913085215  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070913090923  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080929000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081008053305  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081008070746                      G�O�G�O�G�O�                