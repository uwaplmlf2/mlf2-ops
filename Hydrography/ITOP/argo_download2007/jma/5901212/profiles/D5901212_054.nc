CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   j   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   D�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   M�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   V�   CALIBRATION_DATE      	   
                
_FillValue                  �  _�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `    HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `$   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `(   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `,   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    `l   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    `|   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    `�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         `�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         `�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        `�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    `�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               6A   JA  20070811193622  20081008070639  A9_60134_054                    2C  D   APEX-SBE 2404                                                   846 @ԌB�x��1   @ԌC�r�\@,cS���@a|�t�j1   ARGOS   A   A   A   @�33AffAi��A�  A�  A�ffB��B��B0��BE33BX��Bm��B�ffB�  B���B�ffB�  B�  B���B�ffB�33B�ffB�  B�33B�33C��C�3CffC33CffC33CffC$�3C)�3C.�C3�3C8�3C=��CB��CG��CP�fC[�3Ce��Co� Cy33C���C�ٚC��fC��3C���C��fC��fC�� C��fC��3C�s3C�ffC�s3C���C�� C̙�Cь�C֦fC۳3C�fC�3C�3C��C��3C�� D��D��D�3D�3D��D�fD��D$� D)ٚD.�fD3�3D8�fD=�3DB�3DGٚDL��DQ�fDV� D[�fD`�fDe��DjٚDo�3Dt�fDy��D�)�D�c3D��fD���D�#3D�` D�� D��3D�&fD�vfD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @���A33AfffA�ffA�ffA���B  B  B0  BDffBX  Bl��B�  B���B�ffB�  B���B���B�ffB�  B���B�  B䙚B���B���CffC� C33C  C33C  C33C$� C)� C-�fC3� C8� C=ffCBffCGffCP�3C[� CeffCoL�Cy  C�� C�� C���C���C�s3C���C���C�ffC���C���C�Y�C�L�C�Y�C³3CǦfC̀ C�s3C֌�Cۙ�C���C噚CꙚC�s3C���C�ffD��D� D�fD�fD��DٚD� D$�3D)��D.��D3�fD8��D=�fDB�fDG��DL� DQٚDV�3D[��D`��De� Dj��Do�fDt��Dy� D�#3D�\�D�� D��fD��D�Y�D���D���D�  D�p D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�A�r�A�ffA�G�A�^A�VA�VA�A���A��A���A��A�"�A�t�A�hA���A�bA�!A㙚A�I�A�A݅A�
=A�33A�-AҁA�C�A��A�ZAȗ�A���A�ĜA�|�A�+A���A�ffA�;dA�/A��A��jA�-A��-A��FA�n�A��+Au|�AkdZAY��AP�DAA�mA8��A5��A0��A+�A"�/A!�^A��A{AffA��A
jA��A�T@� �@��7@�^5@땁@��y@�  @���@Ӿw@�V@��@őh@���@�ȴ@�S�@��9@��P@�=q@���@��F@���@�+@��!@� �@���@��F@��`@���@�o@��j@���@���@�^5@��@w�@nv�@d�j@]��@R~�@L�j@D9X@>��@6ȴ@6�R1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�A�r�A�ffA�G�A�^A�VA�VA�A���A��A���A��A�"�A�t�A�hA���A�bA�!A㙚A�I�A�A݅A�
=A�33A�-AҁA�C�A��A�ZAȗ�A���A�ĜA�|�A�+A���A�ffA�;dA�/A��A��jA�-A��-A��FA�n�A��+Au|�AkdZAY��AP�DAA�mA8��A5��A0��A+�A"�/A!�^A��A{AffA��A
jA��A�T@� �@��7@�^5@땁@��y@�  @���@Ӿw@�V@��@őh@���@�ȴ@�S�@��9@��P@�=q@���@��F@���@�+@��!@� �@���@��F@��`@���@�o@��j@���@���@�^5@��@w�@nv�@d�j@]��@R~�@L�j@D9X@>��@6ȴ@6�R1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��BÖBĜBŢB�B	,B	0!B	0!B	0!B	.B	-B	F�B	]/B	S�B	W
B	hsB	�B	�
B
JB
�B
dZB
�qB�B6FBC�BYBaHBhsBo�Bq�Bp�Bq�Bu�BdZB\)BVBZBq�Bm�B^5B!�B
��B
�NB
�B
�+B
H�B
�B	��B	��B	l�B	M�B	B�B	33B	"�B	DB	B��B�B�B	VB	.B	L�B	L�B	M�B	R�B	R�B	Q�B	`BB	iyB	q�B	z�B	�B	��B	�PB	��B	�XB	��B	��B	��B	�HB	�B	�B	��B	��B
B
+B
DB
\B
VB
oB
�B
�B
�B
"�B
$�B
)�B
49B
7LB
=qB
A�B
F�B
J�B
Q�B
T�B
ZB
Y1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B��BÖBĜBƨB�B	-B	0!B	0!B	0!B	/B	/B	G�B	]/B	T�B	XB	iyB	�B	�B
PB
�B
ffB
�}B�B7LBH�B[#BcTBjBr�Br�Br�Bs�Bw�Bn�B_;BYBbNBs�Bn�BaHB&�B
��B
�fB
�B
�=B
J�B
�B	��B	��B	n�B	N�B	C�B	49B	$�B	DB	%B��B��B�B	VB	/B	M�B	M�B	N�B	S�B	R�B	R�B	`BB	jB	r�B	z�B	�B	��B	�VB	��B	�XB	��B	��B	��B	�HB	�B	�B	��B	��B
B
+B
DB
\B
VB
oB
�B
�B
�B
"�B
$�B
)�B
49B
7LB
=qB
A�B
F�B
J�B
Q�B
T�B
ZB
Y1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;��
;��
;��
;��;��
;��;��
;��
;��
;��;��|;��;��
;��;��;��;��|;��|;��;��|;��|;��|;��;��;�;��|;��|;��|;��;��;��|;��|;��|<7�4;��;��<u;��|;��;��;�;��|;ѷ;��|;��;��|;�;��|;ѷ;��|;��;��;��;��|;��
;��|;��|;��;��;��
;��;��;��;��;��;��
;��;��
;��;��;��
;��
;��;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.2, NextCycleSSP=0.2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20070824140606              20070824140606                            20070824142239                            20080929000000  JA  ARFMdecpA9_b                                                                20070811193607  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070811193622  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070811193626  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070811193634  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070811193634  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070811193635  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070811205337                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070815041838  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070815041842  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070815041843  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070815041847  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070815041847  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070815041847  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070815050052                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070824140606  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070824140606  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070824142239  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080929000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081008053304  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081008070639                      G�O�G�O�G�O�                