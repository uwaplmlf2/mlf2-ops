CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   i   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4@   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6P   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8`   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :p   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >$   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @4   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  BD   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  C�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   Dx   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   Mx   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Vx   CALIBRATION_DATE      	   
                
_FillValue                  �  _x   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    _�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    _�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `    HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    `H   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    `X   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    `\   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         `l   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         `p   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        `t   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    `xArgo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               5A   JA  20070801105722  20081008070606  A9_60134_053                    2C  D   APEX-SBE 2404                                                   846 @ԉ�FZC�1   @ԉ��=Ѻ@+��vȴ9@an�\(��1   ARGOS   A   A   A   @�33AffA^ffA�  A�  A�  B
ffBffB2  BDffBZffBm33B�33B�  B�33B�33B���B�  B���B�ffB�ffB�33B�  B�ffB���C33C33C�3C� CffC� C��C$ffC)ffC.ffC3L�C833C=� CBL�CG� CQ� C[L�Ce�CoffCy  C�� C��3C��3C��fC��fC���C���C���C�� C���C���C���C��3C³3CǙ�Č�Cѳ3C֌�Cۀ C���C�� CꙚC��C�� C���D� D� D��D�fD�fD�3D� D$�fD)� D.ٚD3� D8� D=�3DBٚDG��DL�fDQ��DV�fD[ٚD`� De��Dj�3Do�3Dt� Dy�3D�,�D�` D��3D���D�  D�i�D��fD���D�fD��f111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @���A33A[33A�ffA�ffA�ffB	��B��B133BC��BY��BlffB���B���B���B���B�33B���B�33B�  B�  B���B䙚B�  B�ffC  C  C� CL�C33CL�CffC$33C)33C.33C3�C8  C=L�CB�CGL�CQL�C[�Cd�fCo33Cx��C��fC���C���C���C���C�� C�� C�s3C��fC��3C��3C��3C���C�Cǀ C�s3Cљ�C�s3C�ffC�3C�fC� C�s3C��fC�s3D�3D�3D� D��D��D�fD�3D$��D)�3D.��D3�3D8�3D=�fDB��DG��DLٚDQ� DV��D[��D`�3De� Dj�fDo�fDt�3Dy�fD�&fD�Y�D���D��fD��D�c3D�� D��fD� D�� 111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�\)A�jA�hA�\A�RA���A�ĜA�/A��A�PA�jA�~�A�`BA��;A�PA���A�jA�n�A�VA�9XA��#A�&�A�C�A�r�A��A�9XA�bNA��#AӓuAͶFA�~�A��A�9XA�XA��A���A�VA�hsA�I�A��9A�|�A��`A�XA��A��9A�n�A|�Ai�Ad~�AU�AR5?AOt�AB��A:v�A,��A$�DA 1A`BA��A=qA
=AG�A	��Av�AƨA��@�o@��j@�t�@�"�@�h@ݙ�@֗�@�hs@У�@�~�@�^5@�Ĝ@�bN@��@��u@��P@���@�S�@�Q�@�hs@�~�@���@�\)@�Q�@�E�@�&�@�1'@��@��F@�I�@z~�@ol�@e�h@[t�@SS�@N��@G�w@AG�@<��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�\)A�jA�hA�\A�RA���A�ĜA�/A��A�PA�jA�~�A�`BA��;A�PA���A�jA�n�A�VA�9XA��#A�&�A�C�A�r�A��A�9XA�bNA��#AӓuAͶFA�~�A��A�9XA�XA��A���A�VA�hsA�I�A��9A�|�A��`A�XA��A��9A�n�A|�Ai�Ad~�AU�AR5?AOt�AB��A:v�A,��A$�DA 1A`BA��A=qA
=AG�A	��Av�AƨA��@�o@��j@�t�@�"�@�h@ݙ�@֗�@�hs@У�@�~�@�^5@�Ĝ@�bN@��@��u@��P@���@�S�@�Q�@�hs@�~�@���@�\)@�Q�@�E�@�&�@�1'@��@��F@�I�@z~�@ol�@e�h@[t�@SS�@N��@G�w@AG�@<��111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��B		7B	DB	�B	,B	8RB	:^B	0!B	:^B	I�B	K�B	A�B	A�B	B�B	�1B	��B	�'B	�B
�B
>wB
dZB
�RBuB<jBQ�B�DB�1B��B��B�TB�sB�B�RB�BT�BVBB\B&�BdZB$�B�B
��B
��B
��B
z�B
gmB
{B	��B	�}B	�!B	��B	o�B	M�B	�B	�B	bB��B	#�B	5?B	.B	8RB	=qB	B�B	C�B	S�B	VB	ZB	]/B	\)B	^5B	^5B	|�B	�B	��B	�B	�dB	ƨB	��B	�)B	�ZB	�B	��B	��B
B

=B
\B
{B
�B
�B
 �B
!�B
"�B
%�B
'�B
-B
33B
;dB
;dB
@�B
G�B
K�B
P�B
R�B
S�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B��B		7B	DB	�B	,B	8RB	;dB	1'B	:^B	J�B	L�B	B�B	B�B	B�B	�7B	��B	�-B	�#B
�B
?}B
ffB
�^B�B=qBR�B�JB�7B��B��B�mB�B�B�jB�=B[#BbBBbB,BiyB'�B�B
��B
�B
��B
{�B
l�B
�B
B	��B	�'B	��B	q�B	Q�B	�B	�B	oB��B	#�B	6FB	0!B	8RB	=qB	C�B	D�B	T�B	VB	[#B	^5B	]/B	_;B	_;B	|�B	�B	��B	�B	�dB	ƨB	��B	�)B	�ZB	�B	��B	��B
B

=B
\B
{B
�B
�B
 �B
!�B
"�B
%�B
'�B
-B
33B
;dB
;dB
@�B
G�B
K�B
P�B
R�B
S�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   ;��;��
;��
;��
;��
;��
;��;��;��
;��;��;��;��;��
;��;��;��;��|;��;��;��|;��|;��|;��;��;��;��;��;�;ѷ;��;��$;ѷ;�;��$;��|;��;��;�;�;��;��;��|;ѷ;��|;��;�;��;ѷ;��;��;��;��|;ѷ;��|;��;��|;��
;��
;��;��|;��
;��
;��;��;��;��
;��;��;��;��;��;��
;��
;��
;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.1, NextCycleSSP=0.2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20070814151153              20070814151153                            20070814151528                            20080929000000  JA  ARFMdecpA9_b                                                                20070801105720  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070801105722  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070801105723  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070801105727  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070801105727  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070801105727  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070801110827                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070814151153  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070814151153  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070814151528  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080929000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081008053304  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081008070606                      G�O�G�O�G�O�                