CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   j   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  4D   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  6X   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8l   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  :   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  <(   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  <�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ><   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  ?�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @P   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  l  A�   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  Bd   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  D   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   D�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   M�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   V�   CALIBRATION_DATE      	   
                
_FillValue                  �  _�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    `   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    `    HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    `$   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    `(   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  `,   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    `l   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    `|   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    `�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         `�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         `�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        `�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    `�Argo profile    2.2 1.2 19500101000000  5901212 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               =A   JA  20071020193336  20081008071034  A9_60134_061                    2C  D   APEX-SBE 2404                                                   846 @ԝ����1   @ԝ����@@/�I�^5@aź^5?}1   ARGOS   A   A   A   @�33A��AfffA�ffA���A���B	33B��B1��BDffBZ��Bn  B���B�33B�  B�  B�33B���B���B���B���B�  B�33B�33B�  C��CL�C33C  C�fCffC��C$��C)� C.L�C3�3C8L�C=� CBffCG33CQ��C[ffCe  Cn�fCx�fC���C�� C��fC��fC���C�� C���C�� C�ffC���C���C��3C���C�CǦfC̀ Cь�Cֳ3Cی�C���C� CꙚC�3C��3C��fD�3D��D��DٚD�3DٚD��D$� D)�fD.��D3ٚD8��D=��DB�fDGٚDL�3DQ� DV� D[� D`� De��Dj� Do� Dt��Dy�3D�,�D�l�D���D��D�#3D�\�D���D��fD�  D�vfD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @�  A33Ad��A���A�  A�  B��B33B133BD  BZffBm��B���B�  B���B���B�  B�ffB�ffBř�Bϙ�B���B�  B�  B���C� C33C�C�fC��CL�C� C$� C)ffC.33C3��C833C=ffCBL�CG�CQ� C[L�Cd�fCn��Cx��C���C��3C���C���C�� C��3C�� C�s3C�Y�C���C���C��fC�� C�CǙ�C�s3Cр C֦fCۀ C�� C�s3C��C�fC��fC���D��D�fD�fD�3D��D�3D�fD$ٚD)� D.�fD3�3D8�fD=�3DB� DG�3DL��DQٚDV��D[��D`ٚDe�fDj��Do��Dt�fDy��D�)�D�i�D��fD��fD�  D�Y�D���D��3D��D�s3D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�ZA���A�A� �A���A�9A��A쟾A���A�\)A���A�=qA�DA坲A�VA�DA��A��`A�ƨA�ZAغ^A�A���A�;dA���AЅA���A�9XA�&�Ȧ+A�ZA��A�5?A��uA��HA��DA�VA���A���A�A��uA�ZA�ffA�1'A�Au�Aq�#Ag�mAb�AW��AKAHJA?�A8�/A3"�A0��A*�jA$A v�A��Al�A��A��A9XA��A	��A~�A�AX@�p�@�
=@�S�@���@�=q@�ff@�z�@�hs@��
@�9X@�p�@�^5@�K�@���@�1@���@�$�@���@���@�V@�~�@��@���@�=q@�r�@�%@��@�/@�%@x �@n@c�F@]O�@U�h@MV@C�F@C��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  A�ZA���A�A� �A���A�9A��A쟾A���A�\)A���A�=qA�DA坲A�VA�DA��A��`A�ƨA�ZAغ^A�A���A�;dA���AЅA���A�9XA�&�Ȧ+A�ZA��A�5?A��uA��HA��DA�VA���A���A�A��uA�ZA�ffA�1'A�Au�Aq�#Ag�mAb�AW��AKAHJA?�A8�/A3"�A0��A*�jA$A v�A��Al�A��A��A9XA��A	��A~�A�AX@�p�@�
=@�S�@���@�=q@�ff@�z�@�hs@��
@�9X@�p�@�^5@�K�@���@�1@���@�$�@���@���@�V@�~�@��@���@�=q@�r�@�%@��@�/@�%@x �@n@c�F@]O�@U�h@MV@C�F@C��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB�BN�BaHBq�B�DB��B��B��B�B	7LB	�dB
S�B
�B1'BZBl�B�\B��B��B�B�bB��B��BB��B�B��BBbB�BE�B:^B�B$�B �BbBB��B��B�FBcTB�B
�5B
��B
u�B
J�B
8RB
VB	��B	��B	��B	�JB	o�B	ZB	N�B	E�B	.B	�B	 �B	$�B	.B	0!B	6FB	@�B	D�B	A�B	R�B	S�B	VB	\)B	^5B	hsB	u�B	�=B	�{B	��B	�B	�dB	ÖB	ȴB	��B	�
B	�BB	�mB	�B	��B	��B
B
B
DB
JB

=B
JB
VB
�B
�B
$�B
)�B
/B
6FB
9XB
=qB
@�B
E�B
L�B
L�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  B�BN�BaHBq�B�DB��B��B��B�B	7LB	�jB
T�B
�=B33B[#Bn�B�bB��B��B�%B�hB��B��BĜB��B�B��BBhB�BH�B=qB!�B'�B#�BoB+B��B��B�^BffB�B
�HB
�B
w�B
K�B
:^B
bB	��B	��B	��B	�VB	q�B	[#B	O�B	G�B	0!B	�B	!�B	%�B	/B	1'B	7LB	A�B	E�B	B�B	S�B	T�B	W
B	]/B	_;B	iyB	u�B	�DB	�{B	��B	�B	�dB	ÖB	ȴB	��B	�
B	�BB	�mB	�B	��B	��B
B
B
DB
JB

=B
JB
VB
�B
�B
$�B
)�B
/B
6FB
9XB
=qB
@�B
E�B
L�B
L�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111  ;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��;��;�;��|;��;��|;��;��|;��|;��;��;��;��;��|;��;��;��;��;��;��;��;��;ѷ;��;��;��|;ѷ;��$;��;ѷ;��;��;��;��;��|;��;��|;��|;��;��;��;��|;��|;��;��;��|;��|;��;��;��;��;��;��;��;��;��;��;��;��;��;��;��;��
;��;��
;��;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
;��
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJUSTED = PRES-NextCycleSSP                                                                                                                                                                                                                               TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJUSTED = RecalS = PSAL(PRES_ADJUSTED,TEMP,Conductivity)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  PSAL_ADJUSTED = celltm_sbe41(RecalS,TEMP,PRES_ADJUSTED,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P= dbar since the start of the profile for each samples.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ThisCycleSSP=0.1, NextCycleSSP=0.1                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Pressure Correction using SeaSurfacePressure of this and next cycle in Technical Data. PRES_ADJ_ERR : Manufacturer sensor accuracy                                                                                                                              TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJUSTED. PSAL_ADJ_ERR : SBE sensor accuracy & CTM adjustment                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         20071102141424              20071102141424                            20071102142210                            20080929000000  JA  ARFMdecpA9_b                                                                20071020193318  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071020193336  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071020193340  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071020193347  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071020193348  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071020193349  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071020205201                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071024042929  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071024042933  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071024042934  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071024042938  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071024042938  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071024042939  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20071024042939  CV  TIME            G�O�G�O�F��                JA  ARGQrelo2.1                                                                 20071024042939  CV  LAT$            G�O�G�O�A|J                JA  ARGQrelo2.1                                                                 20071024042939  CV  LON$            G�O�G�O�C-�                JA  ARUP                                                                        20071024052842                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071102141424  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071102141424  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071102142210  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080929000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20081008053308  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20081008071034                      G�O�G�O�G�O�                