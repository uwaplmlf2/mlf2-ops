CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900413 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               yA   JA  20071012185647  20100129073511  A5_28340_121                    2C  D   APEX-SBE 1313                                                   846 @ԛ��7K�1   @ԛ���#@<��`A�7@b��+1   ARGOS   A   A   A   @���A��A`  A�ffA���A�  BffBffB2ffBFffBY��Bo33B���B���B�33B���B�  B�33B�ffB�33B�  B�33B䙚B�  B���C33CL�C
��C  C33CffC�fC$�C)�C.  C3  C8  C=�CBL�CG��CQ33C[L�CeffCoL�CyffC��fC���C���C���C��fC�Y�C�Y�C���C���C��fC��3C���C�� C³3Cǳ3C̳3Cљ�C֌�CۦfC�fC�fC�3C�� C��fC���D�3D�3DٚDٚD� D��DٚD$� D)� D.�3D3��D8�3D=��DB��DGٚDL��DQ�3DVٚD[�3D`ٚDeٚDj�fDo��Dt� Dy� D�&fD�l�D���D���D�)�D�c3D��fD�ٚD�)�D�i�D�� D�� D��D�c3Dک�D��D�  D�VfD�D�  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A  A^ffA���A�  A�33B  B  B2  BF  BY33Bn��B���B���B�  B���B���B�  B�33B�  B���B�  B�ffB���B���C�C33C
�3C�fC�CL�C��C$  C)  C-�fC2�fC7�fC=  CB33CG� CQ�C[33CeL�Co33CyL�C���C�� C���C�� C���C�L�C�L�C���C���C���C��fC�� C��3C¦fCǦfC̦fCь�Cր Cۙ�C���C噚C�fC�3C���C���D��D��D�3D�3D��D�3D�3D$ٚD)��D.��D3�3D8��D=�3DB�fDG�3DL�fDQ��DV�3D[��D`�3De�3Dj� Do�fDt��Dy��D�#3D�i�D���D��D�&fD�` D��3D��fD�&fD�ffD���D���D��D�` DڦfD��fD��D�S3D�fD��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A���AۃA�p�A�ffA�\)A�Q�A�I�A���A�$�A��A�  A�K�A�?}A�C�A���A�S�A�%A�p�A���A�l�A���A�(�A��uA���A�M�A��uA��#A�`BA���A��/A�ffA���A��
A�z�A���A���A�O�A�/A��DA�-A���A���A���A���A���A��A�7LA�z�A�;dA�dZA�+A~�AzbAwx�ArJAoO�Al-Ag�
Ae�7A_��A[33AVȴAUS�AP�AN�+AG�AB�A<$�A8ZA4E�A-�-A#x�A33A+A�FA\)@�o@�@�=q@�x�@�j@���@�@��/@�X@�t�@��@��/@�n�@�p�@���@��@���@}�-@w�@l��@_�;@XbN@J�!@E?}@>�+@8A�@2�H@-@'|�@ A�@M�@�R@��@�9@z�@Ĝ@��@"�@7L1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A���AۃA�p�A�ffA�\)A�Q�A�I�A���A�$�A��A�  A�K�A�?}A�C�A���A�S�A�%A�p�A���A�l�A���A�(�A��uA���A�M�A��uA��#A�`BA���A��/A�ffA���A��
A�z�A���A���A�O�A�/A��DA�-A���A���A���A���A���A��A�7LA�z�A�;dA�dZA�+A~�AzbAwx�ArJAoO�Al-Ag�
Ae�7A_��A[33AVȴAUS�AP�AN�+AG�AB�A<$�A8ZA4E�A-�-A#x�A33A+A�FA\)@�o@�@�=q@�x�@�j@���@�@��/@�X@�t�@��@��/@�n�@�p�@���@��@���@}�-@w�@l��@_�;@XbN@J�!@E?}@>�+@8A�@2�H@-@'|�@ A�@M�@�R@��@�9@z�@Ĝ@��@"�@7L1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�JB
�=B
�JB
�DB
�=B
�=B
�7B
�PB
�7B
ÖB
�BDB#�B=qBM�BT�BZBk�BffBdZB[#BS�BR�BP�BO�BK�BI�BH�BE�BA�B>wB5?B1'B/B.B,B.B(�B%�B#�B"�B �B�B{BDB
��B
�sB
�BB
��B
��B
�3B
��B
�hB
�B
jB
ZB
G�B
7LB
+B
bB	��B	�fB	�/B	ƨB	ĜB	��B	�=B	n�B	XB	K�B	&�B��B�B�'B�oBjB;dB#�B�B�B(�B33B:^BL�BiyBw�B�JB��B��BƨB�mB��B	JB	�B	'�B	G�B	o�B	�7B	�'B	B	��B	�fB	�B
B
JB
�B
%�B
/B
6FB
=qB
F�B
P�B
XB
^5B
cT1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�JB
�=B
�JB
�DB
�=B
�=B
�7B
�\B
�oB
��B
��B{B)�B@�BO�BW
B\)Bn�BgmBffB\)BT�BS�BP�BP�BL�BJ�BH�BF�BA�B?}B6FB1'B/B.B,B/B)�B%�B#�B"�B!�B�B�BJB
��B
�sB
�HB
��B
��B
�9B
��B
�oB
�B
k�B
[#B
H�B
8RB
,B
hB	��B	�fB	�5B	ǮB	ƨB	��B	�JB	o�B	YB	L�B	'�B��B�B�-B�uBl�B<jB$�B�B�B(�B33B;dBL�BiyBw�B�JB��B��BƨB�mB��B	JB	�B	'�B	G�B	o�B	�7B	�'B	B	��B	�fB	�B
B
JB
�B
%�B
/B
6FB
=qB
F�B
P�B
XB
^5B
cT1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next                                                                                                                                                                                           TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710151059272007101510592720071015105927200809101320382008091013203820080910132038200808110000002008081100000020080811000000  JA  ARFMdecpA5_a                                                                20071012185644  IP                  G�O�G�O�G�O�                JM  ARFMDECPA5                                                                  20071015105927  IP                  G�O�G�O�G�O�                JM  ARFMBITP1                                                                   20071015105927  SV                  @���A���G�O�                JM  ARGQRQCP1                                                                   20071015105927  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20080910130141  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080910130141  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080910132038  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080811000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20080926065635  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20080926093736                      G�O�G�O�G�O�                JM  AREVjmrvp1.2                                                                20090312110822  IP  PRES            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090317084141  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090317084229                      G�O�G�O�G�O�                JM  ARSQJMQC1.0                                                                 20091009010000  IP  PSAL_ADJ        @���A���G�O�                JA  RFMTcnvd2.1                                                                 20100129073444  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100129073511                      G�O�G�O�G�O�                