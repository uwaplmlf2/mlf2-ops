CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900413 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               vA   JA  20070912185838  20100129073511  A5_28340_118                    2C  D   APEX-SBE 1313                                                   846 @Ԕ	̢�1   @Ԕg� T@<�����@b��^5?}1   ARGOS   A   A   A   @�ffA  Ah  A�  A�33A���B	��B��B2ffBG33BZffBnffB���B���B�  B�33B�33B�33B���B���B�ffB�33B�  B�  B�33CL�C33C33C� CffC33C33C$33C)� C.L�C3L�C8ffC=33CB�CG33CQ� C[L�CeL�Co�Cy  C��fC��3C���C�� C�s3C���C���C��3C���C�� C��fC��3C��3C�Cǌ�C̙�Cр Cֳ3Cۀ C�� C�3C�3C�3C��C���DٚD��D�fD�3D� D�3D�3D$��D)�3D.�fD3��D8��D=�3DBٚDG� DL�3DQ� DV��D[� D`��De�3Dj��Do�fDt�3Dy��D�,�D�ffD���D���D�#3D�l�D���D�ٚD�0 D�i�D��fD��D�&fD�i�DڦfD�ٚD��D�c3D� D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A33Ac33A���A���A�ffBffB��B133BF  BY33Bm33B�  B�  B�ffB���B���B���B�33B�33B���Bٙ�B�ffB�ffB���C  C�fC
�fC33C�C�fC�fC#�fC)33C.  C3  C8�C<�fCA��CF�fCQ33C[  Ce  Cn��Cx�3C�� C���C�ffC�Y�C�L�C�s3C��fC���C�ffC�Y�C�� C���C���C�ffC�ffC�s3C�Y�C֌�C�Y�C���C��C��C��C�ffC�s3D�fD��D�3D� D��D� D� D$��D)� D.�3D3�fD8�fD=� DB�fDG��DL� DQ��DV�fD[��D`�fDe� Dj��Do�3Dt� Dy��D�#3D�\�D�� D��3D��D�c3D��3D�� D�&fD�` D���D�� D��D�` Dڜ�D�� D� D�Y�D�fD��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�/A�VAŃA�M�AþwA�|�A¡�A��jA��-A�&�A���A�z�A�7LA�E�A�z�A��7A�S�A�&�A���A�E�A��HA��!A��A���A�XA���A�5?A���A�^5A�5?A���A�=qA���A�M�A���A�~�A�dZA�33A��A��\A��/A��HA�9XA��A�5?A�  A�jA�AA}G�Ay�AuƨArE�Al=qAi�Ag"�AdE�A_�^A\jAY7LAWoAQdZAM�TAJ��AC�-A>VA;l�A4��A-��A(  AhsA�A33A�D@���@�O�@�@�&�@��9@��
@�\)@�K�@��@�r�@�ȴ@�t�@��;@���@�&�@��@��w@~��@t1@m/@g�;@`1'@Xb@N��@E�@;�
@49X@.V@%@!�7@1@�@�7@
=@��@
~�@�;@��@-?��?�/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�/A�VAŃA�M�AþwA�|�A¡�A��jA��-A�&�A���A�z�A�7LA�E�A�z�A��7A�S�A�&�A���A�E�A��HA��!A��A���A�XA���A�5?A���A�^5A�5?A���A�=qA���A�M�A���A�~�A�dZA�33A��A��\A��/A��HA�9XA��A�5?A�  A�jA�AA}G�Ay�AuƨArE�Al=qAi�Ag"�AdE�A_�^A\jAY7LAWoAQdZAM�TAJ��AC�-A>VA;l�A4��A-��A(  AhsA�A33A�D@���@�O�@�@�&�@��9@��
@�\)@�K�@��@�r�@�ȴ@�t�@��;@���@�&�@��@��w@~��@t1@m/@g�;@`1'@Xb@N��@E�@;�
@49X@.V@%@!�7@1@�@�7@
=@��@
~�@�;@��@-?��?�/1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB:^B5?B0!B:^B=qB>wB>wB;dB7LB9XB:^B`BB`BBhsB_;BM�B=qB5?B5?B33B0!B/B.B'�B(�B%�B!�B�B�B�B{BoBhBbB\BVBPBVBDB	7BB
��B
��B
�sB
�BB
��B
ÖB
�?B
��B
��B
�B
s�B
dZB
G�B
;dB
2-B
%�B
{B
1B	��B	�B	�
B	ƨB	�LB	��B	�%B	w�B	YB	;dB	"�B�B�oBp�B_;B@�B8RB,BhB�B-B6FBC�B_;Bt�B�DB��B�BŢB�
B�TB��B	DB	'�B	;dB	G�B	cTB	z�B	�hB	��B	�}B	��B	�5B	��B
B
PB
�B
(�B
49B
=qB
G�B
R�B
ZB
aHB
iyB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 BC�B;dB1'B:^B=qB?}B?}B=qB=qBC�BA�BcTBhsBjBaHBN�B>wB5?B5?B33B0!B/B/B'�B)�B&�B!�B�B�B�B{BoBhBbB\BVBPBVBDB	7BB
��B
��B
�sB
�HB
��B
ĜB
�FB
��B
��B
�B
t�B
ffB
H�B
<jB
33B
&�B
�B
	7B	��B	�B	�B	ǮB	�XB	��B	�+B	y�B	[#B	=qB	#�B�B�uBq�B`BBA�B9XB.BoB�B-B6FBD�B_;Bt�B�DB��B�BŢB�
B�TB��B	DB	'�B	;dB	G�B	cTB	z�B	�hB	��B	�}B	��B	�5B	��B
B
PB
�B
(�B
49B
=qB
G�B
R�B
ZB
aHB
iyB
j1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.3(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709200032552007092000325520070920003255200809101320272008091013202720080910132027200808110000002008081100000020080811000000  JA  ARFMdecpA5_a                                                                20070912185835  IP                  G�O�G�O�G�O�                JM  ARFMDECPA5                                                                  20070920003255  IP                  G�O�G�O�G�O�                JM  ARFMBITP1                                                                   20070920003255  SV                  @�ffA�33G�O�                JM  ARGQRQCP1                                                                   20070920003255  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20080910130120  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20080910130120  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20080910132027  CV  PSAL            G�O�G�O�G�O�                JM  ARSQWJO 2.0 SeHyD1.0                                                        20080811000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20080926065637  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20080926094010                      G�O�G�O�G�O�                JM  AREVjmrvp1.2                                                                20090312110821  IP  PRES            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090317084140  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090317084228                      G�O�G�O�G�O�                JM  ARSQJMQC1.0                                                                 20091009010000  IP  PSAL_ADJ        @�ffA�33G�O�                JA  RFMTcnvd2.1                                                                 20100129073443  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20100129073511                      G�O�G�O�G�O�                