CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   o   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  4X   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  6�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  :l   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  :�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  >�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  @�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  @�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  p  B�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  D�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K   CALIBRATION_DATE            	             
_FillValue                  ,  N   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N4   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N8   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N<   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N@   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  ND   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    N�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    N�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    N�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         N�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         N�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        N�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    N�Argo profile    2.2 1.2 19500101000000  5900996 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               <A   JA  20070827185259  20091211042201                                  2B  A   2296                                                            846 @Ԑ���B1   @Ԑ��u@7&�x���@`��1'1   ARGOS   B   B   B   @�  A��Ai��A���A�ffA�33B
ffB33B0��BF  BY33Bn��B�ffB�ffB�  B�  B�33B�33B���B�  BЙ�Bڙ�B�  B���B���C� C33C33CffC  CL�C�fC$�C)33C.ffC2�fC8  C=��CB��CG33CQL�C[33Ce�Co� CyL�C���C�� C�� C��fC��fC��3C�s3C�� C�� C��3C��3C�� C���C¦fC�� C̦fCь�C֙�Cی�C�ffC�ffC�Y�CC�ffC�ffD��DٚD� D��DٚD�fD� D$� D)�fD.ٚD3� D8�3D=��DB� DG�3DL� DQ�3DV�3D[� D`� De��Dj��Do�fDt�3DyٚD�,�G�O�D�� D��D�,�D�l�D���D���D��D�l�DڦfD��fD�)�D�ffD�fD�f111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111411111111111111 @�33A34Ac34A�fgA�33A�  B��B��B/33BDffBW��Bm33B���B���B�33B�33B�ffB�ffB�  B�33B���B���B�33B�  B�  C�C��C
��C  C��C�gC� C#�4C(��C.  C2� C7��C=34CB34CF��CP�gCZ��Cd�4Co�Cx�gC�fgC���C���C�s3C�s3C�� C�@ C���C���C�� C�� C�L�C�Y�C�s3Cǌ�C�s3C�Y�C�fgC�Y�C�33C�33C�&gC�fgC�33C�33D�3D� D�fD� D� D��D�fD$�fD)��D.� D3�fD8��D=�3DB�fDG��DL�fDQ��DV��D[�fD`�fDe�3Dj�3Do��Dt��Dy� D�  G�O�D��3D���D�  D�` D���D�� D� D�` Dڙ�D�ٙD��D�Y�D�D���111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111411111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�AځA��A�n�Aԇ+A�G�A��A��A��A�hsA��A��A���A��9A��A�`BA�~�A��A���A�1'A��-A�x�A���A��7A���A���A���A�~�A���A��uA�%A���A��A���A���A�bA���A�A��A��A��hA��A��A�`BA�A�A�M�A��TA���A�dZA��jA~ffAz(�AtM�Ao|�Al�jAh��Ag?}AfZAbZA_`BAW�mAT��AM�wAEt�AC�A?S�A<^5A7�7A5�#A1hsA,  A!S�A�-A�-A
��A/@���@���@�(�@�G�@��@�p�@��P@��@��u@�ƨ@�E�@�5?@�
=@�ff@��@��@��@�$�@��@z��G�O�@>�@6ȴ@.v�@'�@"�!@V@^5@ff@�@%@�@	�^@$�@S�@&�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111119411111111111111 AځA��A�n�Aԇ+A�G�A��A��A��A�hsA��A��A���A��9A��A�`BA�~�A��A���A�1'A��-A�x�A���A��7A���A���A���A�~�A���A��uA�%A���A��A���A���A�bA���A�A��A��A��hA��A��A�`BA�A�A�M�A��TA���A�dZA��jA~ffAz(�AtM�Ao|�Al�jAh��Ag?}AfZAbZA_`BAW�mAT��AM�wAEt�AC�A?S�A<^5A7�7A5�#A1hsA,  A!S�A�-A�-A
��A/@���@���@�(�@�G�@��@�p�@��P@��@��u@�ƨ@�E�@�5?@�
=@�ff@��@��@��@�$�@��@z��G�O�@>�@6ȴ@.v�@'�@"�!@V@^5@ff@�@%@�@	�^@$�@S�@&�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111119411111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�TB
�B
�B
�B1B�B)�B49B7LBC�BA�BA�B_;BZBe`B`BB]/B`BB`BBdZBS�BN�BL�BH�BH�BI�BD�B9XB1'B33B2-B1'B1'B5?B7LB9XB6FB2-B1'B1'B/B#�B�BhB
��B
�B
�B
�
B
��B
�B
��B
y�B
aHB
Q�B
A�B
8RB
33B
 �B
VB	�sB	�
B	�B	�+B	x�B	iyB	]/B	K�B	G�B	;dB	#�B�B�
B�FB��B�=Bx�Bs�Br�B|�B|�B�1B�\B��B�wB��B�NB�B	B	�B	)�B	;dB	J�B	W
B	`BB	l�B	�PG�O�B
B
uB
�B
/B
8RB
A�B
I�B
Q�B
ZB
dZB
iyB
r�B
x�B
|�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111411111111111111 B
�TB
�B
�B
�B1B�B)�B49B7LBC�BA�BA�B_;BZBe`B`BB]/B`BB`BBdZBS�BN�BL�BH�BH�BI�BD�B9XB1'B33B2-B1'B1'B5?B7LB9XB6FB2-B1'B1'B/B#�B�BhB
��B
�B
�B
�
B
��B
�B
��B
y�B
aHB
Q�B
A�B
8RB
33B
 �B
VB	�sB	�
B	�B	�+B	x�B	iyB	]/B	K�B	G�B	;dB	#�B�B�
B�FB��B�=Bx�Bs�Br�B|�B|�B�1B�\B��B�wB��B�NB�B	B	�B	)�B	;dB	J�B	W
B	`BB	l�B	�PG�O�B
B
uB
�B
/B
8RB
A�B
I�B
Q�B
ZB
dZB
iyB
r�B
x�B
|�111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111411111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070827185257  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070827185259  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070827185300  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070827185304  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20070827185304  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070827185304  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.7c                                                                20070827185304  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16a                                                                20070827185304  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070827190443                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070830035808  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070830035812  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070830035813  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070830035817  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20070830035817  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070830035818  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcp2.7c                                                                20070830035818  QCF$                G�O�G�O�G�O�            4000JA  ARGQrqcpt16a                                                                20070830035818  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070830043610                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070830035808  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518084946  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518084947  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090518084947  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518084947  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518084948  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090518084948  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518084948  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518084948  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518084948  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518084948  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518084948  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518085344                      G�O�G�O�G�O�                JA  ARFMdecpA9_d                                                                20091211023723  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091211024251  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091211024251  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20091211024252  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091211024253  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091211024255  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091211024255  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091211024255  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091211024255  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091211024255  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091211024255  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091211024255  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20091211042201                      G�O�G�O�G�O�                