CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   s   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  E�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    E�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    H�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    K�   CALIBRATION_DATE            	             
_FillValue                  ,  N�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    N�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    N�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    N�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    N�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  N�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    O,   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    O<   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    O@   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         OP   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         OT   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        OX   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    O\Argo profile    2.2 1.2 19500101000000  5900996 J-ARGO                                                          Nobuyuki Shikama                                                PRES            TEMP            PSAL               =A   JA  20070905185415  20090518085342                                  2B  A   2296                                                            846 @Ԓ��+�e1   @Ԓ�� <�@7�t�@`�7Kƨ1   ARGOS   A   A   A   @���A��Ak33A���A���A�33B
��BffB1��BD��BY33BnffB���B���B�ffB�ffB�33B�  B���B���B���B�  B䙚B���B�  C� CL�C33C33CffC�C�C$33C(�fC-��C2�fC8�C<��CB�CF�3CP�fC[  Cd�fCo��CyffC��fC���C��fC���C�� C�s3C�� C�� C�Y�C�� C�s3C��fC�� C³3CǦfC̦fCр Cր CۦfC�ffC� C�� C�� C���C�� D� D�3D��D��D� D� D� D$� D)�fD.ٚD3��D8� D=�fDB�fDG��DL� DQ��DV�3D[�3D`�fDe��Dj��Do��DtٚDy��D�)�D�i�D�� D��3D�&fD�c3D���D��fD�33D�i�D�� D��fD��D�c3Dڣ3D��fD�  D�Y�D�fD��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A34Ad��A���A���A�  B	33B��B0  BC33BW��Bl��B��B�  B���B���B�ffB�33B���B�  B�  B�33B���B�  B�33C�C�gC
��C��C  C�4C�4C#��C(� C-fgC2� C7�4C<fgCA�4CFL�CP� CZ��Cd� Co34Cy  C�s3C�fgC�s3C�fgC���C�@ C�L�C�L�C�&gC�L�C�@ C�s3C���C C�s3C�s3C�L�C�L�C�s3C�33C�L�C��C��C�fgC���D�fD��D� D�3D�fD�fD�fD$�fD)��D.� D3� D8�fD=��DB��DG�3DL�fDQ� DV��D[��D`��De� Dj�3Do� Dt� Dy�3D��D�\�D��3D��fD��D�VfD���D�ٙD�&fD�\�D��3D�ٙD��D�VfDږfD�əD�3D�L�D�D�ٙ1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�%A�VA��
A�I�A�oA�"�A�JA�|�A�5?A��A��A���A�K�A��jA���A��A�1'A�t�A��TA��
A���A��HA�x�A��A�A��!A�t�A�+A���A�7LA�?}A�ȴA� �A�A�=qA��PA�  A��uA�A�VA�\)A�ƨA�l�A�7LA�z�A��A��uA��A|A�Ax^5AtjAl1Aj1AdjAaS�A^A�AZ�DAP�`AJAD�/A@�A<�A;��A7
=A0�jA,��A)�A&�jA$�A�PA�AM�Ap�@��@��`@�1'@��H@�%@�$�@�`B@�1@�  @���@�Q�@��@��@��@�dZ@���@���@��u@��j@}p�@s��@lj@`  @V{@N@FE�@?+@9%@3�@-/@&v�@"�!@@��@l�@��@�`@�@	�@ȴ@9X@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�%A�VA��
A�I�A�oA�"�A�JA�|�A�5?A��A��A���A�K�A��jA���A��A�1'A�t�A��TA��
A���A��HA�x�A��A�A��!A�t�A�+A���A�7LA�?}A�ȴA� �A�A�=qA��PA�  A��uA�A�VA�\)A�ƨA�l�A�7LA�z�A��A��uA��A|A�Ax^5AtjAl1Aj1AdjAaS�A^A�AZ�DAP�`AJAD�/A@�A<�A;��A7
=A0�jA,��A)�A&�jA$�A�PA�AM�Ap�@��@��`@�1'@��H@�%@�$�@�`B@�1@�  @���@�Q�@��@��@��@�dZ@���@���@��u@��j@}p�@s��@lj@`  @V{@N@FE�@?+@9%@3�@-/@&v�@"�!@@��@l�@��@�`@�@	�@ȴ@9X@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�Bs�Br�BjB1B
�B{BhB�uB��B��B�B�B�3B�LB�'B�dB��B��B��B��B�B��B�B��B��B�oB�JB~�Bv�Bl�BgmB^5BW
BO�BJ�BF�BC�B@�B:^B49B-B �B{B1B
�B
�#B
��B
�'B
��B
�B
n�B
L�B
B�B
$�B
�B
%B	�B	��B	��B	�%B	r�B	`BB	W
B	J�B	,B	�B	VB	B��B�TB�RB��B�VB�Bv�Bs�Bx�B}�B�PB��B��B�}B��B�B�/B��B	1B	{B	"�B	33B	K�B	ZB	dZB	z�B	�JB	��B	�jB	��B	�HB	�B
B
DB
�B
+B
7LB
>wB
D�B
M�B
ZB
`BB
gmB
m�B
r�B
w�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 Bs�Br�BjB1B
�B{BhB�uB��B��B�B�B�3B�LB�'B�dB��B��B��B��B�B��B�B��B��B�oB�JB~�Bv�Bl�BgmB^5BW
BO�BJ�BF�BC�B@�B:^B49B-B �B{B1B
�B
�#B
��B
�'B
��B
�B
n�B
L�B
B�B
$�B
�B
%B	�B	��B	��B	�%B	r�B	`BB	W
B	J�B	,B	�B	VB	B��B�TB�RB��B�VB�Bv�Bs�Bx�B}�B�PB��B��B�}B��B�B�/B��B	1B	{B	"�B	33B	K�B	ZB	dZB	z�B	�JB	��B	�jB	��B	�HB	�B
B
DB
�B
+B
7LB
>wB
D�B
M�B
ZB
`BB
gmB
m�B
r�B
w�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070905185413  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070905185415  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070905185416  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070905185420  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070905185420  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070905185421  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070905190533                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070909154430  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070909154433  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070909154434  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070909154438  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070909154439  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070909154439  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070909155427                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070909154430  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090518084949  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090518084949  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090518084950  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090518084950  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090518084951  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090518084951  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090518084951  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090518084951  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090518084951  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090518085342                      G�O�G�O�G�O�                