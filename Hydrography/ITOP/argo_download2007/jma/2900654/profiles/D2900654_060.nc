CDF   (   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900654 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               <A   JA  20070903025954  20090907082841  A9_67024_060                    2C  D   APEX-SBE 2972                                                   846 @ԑ�h|e�1   @ԑ�3�_@<��O�;d@a� ě��1   ARGOS   A   A   A   @���A33A�  A�ffBffBF��BlffB�ffB�  B���B�  B�  B�33C�C�C33C�C(� C3�C<��CG��C[33Co�C�s3C�ffC�ffC���C��fC���C�� CǦfCѦfCی�C噚C�fC��3D�3D��D�3D�3D� DٚD�3D$�3D)��D.��D3�fD8��D=� DB� DG� DN3DTY�DZ��D`��Dg  Dm` Ds�fDy� D��D�i�D���D���D�)�D�` D��D�l�D�� D�c3D���D�\�D�L�111111111111111111111111111111111111111111111111111111111111111111111111@���A��A�33A陚B  BFffBl  B�33B���B�ffB���B���B�  C  C  C�C  C(ffC3  C<�3CG� C[�Co  C�ffC�Y�C�Y�C�� C���C���C��3CǙ�Cљ�Cۀ C��CC��fD��D�fD��D��DٚD�3D��D$��D)�fD.�fD3� D8�fD=ٚDB��DG��DN�DTS3DZ�fD`�fDg�DmY�Ds� Dy��D��D�ffD���D�ٚD�&fD�\�D��fD�i�D���D�` D��D�Y�D�I�111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�x�A�l�A�S�AԸRA�VA�XA�^5A�VA��`A�r�A�v�A���A�jA���A�bA��7A��A�A�bNA�K�A���A�jA�G�A��9A�K�A}��Ax��At�HAk�PAa�
AYdZAQS�AD�!A7A.ĜA%�AdZA��A33@�5?@��@ް!@�7L@�  @���@���@�l�@��T@�V@���@��@�-@�^5@��@���@��@zM�@sdZ@k33@]��@Wl�@M�@D�@=p�@3ƨ@&v�@��@-@��@��@   ?�
=111111111111111111111111111111111111111111111111111111111111111111111111A�x�A�l�A�S�AԸRA�VA�XA�^5A�VA��`A�r�A�v�A���A�jA���A�bA��7A��A�A�bNA�K�A���A�jA�G�A��9A�K�A}��Ax��At�HAk�PAa�
AYdZAQS�AD�!A7A.ĜA%�AdZA��A33@�5?@��@ް!@�7L@�  @���@���@�l�@��T@�V@���@��@�-@�^5@��@���@��@zM�@sdZ@k33@]��@Wl�@M�@D�@=p�@3ƨ@&v�@��@-@��@��@   ?�
=111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB  B
��B
��BK�B|�B�B�PB�uB�Bw�Bp�BM�BK�B8RB7LB+B"�B#�B0!B'�B$�BhB
��B
�sB
��B
��B
�1B
v�B
G�B
�B	��B	��B	��B	e`B	D�B	�B	B��B�{Bo�B`BBI�B33B&�B33BF�BP�B\)Bo�By�B�VB��B��B�/B�B	B	�B	33B	J�B	hsB	w�B	�bB	��B	�jB	��B	��B
�B
-B
=qB
L�B
\)B
gm111111111111111111111111111111111111111111111111111111111111111111111111B  B
��B%BN�B�B�+B�\B��B�By�Br�BO�BL�B9XB8RB,B"�B#�B1'B(�B%�BoB
��B
�yB
��B
��B
�7B
w�B
H�B
�B	��B	�B	��B	ffB	E�B	 �B	B��B��Bp�BaHBK�B49B'�B49BF�BP�B\)Bo�By�B�VB��B��B�/B�B	B	�B	33B	J�B	hsB	w�B	�bB	��B	�jB	��B	��B
�B
-B
=qB
L�B
\)B
gm111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709110242352007091102423520070911024235200709110251132007091102511320070911025113200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070903025951  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070903025954  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070903025954  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070903025959  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070903025959  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070903025959  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070903030657                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070907041641  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070907041645  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070907041646  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070907041650  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070907041650  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070907041650  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070907042937                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828002601  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828002601  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828002605  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828002605  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828002605  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828002605  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828002606  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828014245                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070906185838  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416051303  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416051303  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416051303  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416051303  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416051304  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416051304  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416051304  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416051304  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416051304  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416052344                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070906121219  CV  DAT$            G�O�G�O�F��l                JM  ARCAJMQC1.0                                                                 20070911024235  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070911024235  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070911025113  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090907082719  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090907082841                      G�O�G�O�G�O�                