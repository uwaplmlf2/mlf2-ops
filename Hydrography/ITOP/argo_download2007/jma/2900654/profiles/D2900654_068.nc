CDF   .   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900654 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               DA   JA  20071013013955  20090907082830  A9_67024_068                    2C  D   APEX-SBE 2972                                                   846 @ԛ�~�	1   @ԛ��&@<�E���@a������1   ARGOS   A   A   A   @�ffAffA�  A홚B��BD��Bo33B�33B�33B�ffB�  B���B�  CL�CL�C33C33C)L�C3�C<��CG  CZ�fCoL�C�� C���C�� C�L�C�ffC���C���CǦfCр C�ffC�Y�CC�� D� D��D��D�3D�3D��D� D$�3D)�fD.� D3ٚD8��D=�3DBٚDG�3DM��DTS3DZ�3D`��Dg3Dm@ Ds�fDyٚD�  D�ffD��3D���D�,�D�ffD���D�i�D��3D�i�D��fD�` D���D��1111111111111111111111111111111111111111111111111111111111111111111111111   @�  A33A�ffA�  B  BD  BnffB���B���B�  Bƙ�B�ffBC�C�C  C  C)�C2�fC<��CF��CZ�3Co�C��fC�� C�ffC�33C�L�C��3C�� Cǌ�C�ffC�L�C�@ C� C��fD�3D� D��D�fD�fD��D�3D$�fD)��D.�3D3��D8��D=�fDB��DG�fDM��DTFfDZ�fD`� DgfDm33Dsy�Dy��D��D�` D���D��fD�&fD�` D��fD�c3D���D�c3D�� D�Y�D��fD�31111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�hsA�l�A���A�;dA̝�A��A�Q�A�+A��A��^A���A�A���A��`A���A���A�ffA��+A���A��wA�dZA�r�A�r�A���A���A�oA�G�A���AzM�Ar{Ah-Aa��AU�^AQ33AGoA;�A-��A$�!AA�AE�A�A5?@��m@�%@�|�@�K�@���@��+@��@���@���@�@��R@��@�l�@��@���@}@x�u@p��@e�@Z�\@RM�@H�9@B��@1x�@)&�@9X@z�@?}@O�?�7L?��1111111111111111111111111111111111111111111111111111111111111111111111111   A�hsA�l�A���A�;dA̝�A��A�Q�A�+A��A��^A���A�A���A��`A���A���A�ffA��+A���A��wA�dZA�r�A�r�A���A���A�oA�G�A���AzM�Ar{Ah-Aa��AU�^AQ33AGoA;�A-��A$�!AA�AE�A�A5?@��m@�%@�|�@�K�@���@��+@��@���@���@�@��R@��@�l�@��@���@}@x�u@p��@e�@Z�\@RM�@H�9@B��@1x�@)&�@9X@z�@?}@O�?�7L?��1111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oBBB\B/B
��B
�^B
�B
�mB�B&�B8RBE�BVBVBT�BS�BK�BF�B@�B=qB:^B1'B �BJB
��B
�B
�NB
�wB
�hB
iyB
<jB
�B	�yB	��B	��B	t�B	5?B	JB�B��B��Br�BK�B-B-B+B5?B@�BL�BdZBw�B�hB��B�FB�B��B	B	hB	�B	1'B	XB	s�B	�DB	��B	�?B	�HB	��B
�B
)�B
;dB
N�B
dZB
dZ1111111111111111111111111111111111111111111111111111111111111111111111111   BBBbB49B
��B
�wB
�B
�B�B'�B;dBG�BVBVBVBT�BK�BF�BA�B=qB:^B2-B!�BPB
��B
�B
�TB
�}B
�oB
jB
=qB
�B	�B	�B	��B	v�B	6FB	PB�B��B��Bs�BM�B.B-B,B6FB@�BM�Be`Bw�B�hB��B�FB�B��B	B	hB	�B	1'B	XB	s�B	�DB	��B	�?B	�HB	��B
�B
)�B
;dB
N�B
dZB
dZ1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710210226312007102102263120071021022631200710210241502007102102415020071021024150200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20071013013951  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071013013955  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071013013955  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071013013956  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071013013959  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071013013959  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071013014000  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071013014000  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071013014000  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071013014645                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071017042737  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071017042742  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071017042742  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071017042742  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071017042746  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071017042746  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071017042747  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071017042747  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071017042747  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071017050413                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828002642  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828002643  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828002647  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828002647  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828002647  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828002647  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828002647  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828014554                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071016185836  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416051321  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416051321  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416051321  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416051321  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416051323  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416051323  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416051323  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416051323  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416051323  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416052347                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071016111637  CV  DAT$            G�O�G�O�F��C                JM  ARCAJMQC1.0                                                                 20071021022631  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071021022631  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071021024150  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090907082702  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090907082830                      G�O�G�O�G�O�                