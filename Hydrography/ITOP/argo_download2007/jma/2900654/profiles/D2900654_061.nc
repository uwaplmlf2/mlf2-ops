CDF   .   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900654 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               =A   JA  20070908013324  20090907082838  A9_67024_061                    2C  D   APEX-SBE 2972                                                   846 @ԓ%��Sx1   @ԓ&כ�@<}�E��@a�I�^1   ARGOS   A   A   A   @�33A��A�  A�  B  BE��Bm��B���B���B�  Bƙ�B�  B���CffCL�CL�CffC)ffC3L�C=ffCG�C[�CoffC��3C�� C���C�Y�C��3C�� C�� CǦfC�s3Cی�C�3C���C�� D� D� D��DٚD��D� D�fD$� D)� D.��D3�3D8��D=��DB� DGٚDNfDTL�DZs3D`��DgfDmFfDs�3Dy�fD�&fD�l�D���D�� D��D�` D��fD�` D��fD�p D�� D�\�D���D�I�1111111111111111111111111111111111111111111111111111111111111111111111111   @�  A  A�33A�33B��BE33Bm33B�ffB���B���B�ffB���BCL�C33C33CL�C)L�C333C=L�CG  C[  CoL�C��fC�s3C�� C�L�C��fC��3C�s3CǙ�C�ffCۀ C�fC�� C��3D��D��D�fD�3D�fDٚD� D$ٚD)��D.�3D3��D8�fD=�3DBٚDG�3DN  DTFfDZl�D`�fDg  Dm@ Ds��Dy� D�#3D�i�D��fD���D��D�\�D��3D�\�D��3D�l�D���D�Y�D��D�Ff1111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�ȴA�ZA��`A��A��A��A�;dA�XA��#A��A�1A�"�A�XA��A�-A�z�A�I�A��hA�7LA�XA��A�I�A��PA�r�A�%A�z�A}hsAt�+AkƨAd-AZ�HAP��AG�A=;dA.�`A)"�A �`A��At�A�@��@�$�@ԣ�@Ƈ+@�t�@�S�@�bN@���@�o@���@���@��7@�b@��@�9X@�x�@zJ@t�@pr�@Z��@Sƨ@N�y@Gl�@?��@4��@)��@�@n�@�/@�D?��?�%?�%1111111111111111111111111111111111111111111111111111111111111111111111111   A�ȴA�ZA��`A��A��A��A�;dA�XA��#A��A�1A�"�A�XA��A�-A�z�A�I�A��hA�7LA�XA��A�I�A��PA�r�A�%A�z�A}hsAt�+AkƨAd-AZ�HAP��AG�A=;dA.�`A)"�A �`A��At�A�@��@�$�@ԣ�@Ƈ+@�t�@�S�@�bN@���@�o@���@���@��7@�b@��@�9X@�x�@zJ@t�@pr�@Z��@Sƨ@N�y@Gl�@?��@4��@)��@�@n�@�/@�D?��?�%?�%1111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�B
�sB
�;B
�HB
�HB
�BB.B33BD�BK�B;dB:^B7LB2-B0!B,B+B&�B(�B!�B�BoBB
�HB
ƨB
��B
t�B
I�B
&�B	��B	��B	��B	y�B	=qB	�B	B��B��B�=BL�BA�B?}B0!B(�B0!B33BM�BZBl�B�7B�!B��B�;B��B	PB	"�B	.B	:^B	o�B	�B	��B	�B	��B	��B	�B
hB
0!B
=qB
R�B
aHB
o�B
n�1111111111111111111111111111111111111111111111111111111111111111111111111   B
�B
�yB
�;B
�HB
�NB
�B+B0!B9XBF�BM�B<jB;dB8RB2-B1'B,B,B&�B)�B!�B�BuBB
�NB
ǮB
��B
u�B
J�B
'�B	��B	��B	�B	{�B	>wB	�B	B��B��B�JBM�BB�B@�B1'B)�B1'B49BM�BZBl�B�7B�!B��B�;B��B	PB	"�B	.B	:^B	o�B	�B	��B	�B	��B	��B	�B
hB
0!B
=qB
R�B
aHB
o�B
n�1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709190419392007091904193920070919041939200709190431582007091904315820070919043158200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070908013321  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070908013324  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070908013325  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070908013329  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070908013329  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070908013330  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070908013947                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070912043559  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070912043603  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070912043604  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070912043608  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070912043608  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070912043608  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070912043608  CV  TIME            G�O�G�O�                    JA  ARGQrelo2.1                                                                 20070912043608  CV  LAT$            G�O�G�O�A��                JA  ARGQrelo2.1                                                                 20070912043608  CV  LON$            G�O�G�O�Cx                JA  ARUP                                                                        20070912050049                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828002606  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828002606  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828002610  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828002610  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828002610  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828002610  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828002611  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828014309                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070911190525  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416051305  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416051305  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416051305  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416051306  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416051307  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416051307  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416051307  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416051307  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416051307  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20090416051307  CV  TIME            G�O�G�O�                    JA  ARGQrelo2.1                                                                 20090416051307  CV  LAT$            G�O�G�O�A��                JA  ARGQrelo2.1                                                                 20090416051307  CV  LON$            G�O�G�O�Cx                JA  ARUP                                                                        20090416052342                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070911114230  CV  DAT$            G�O�G�O�F��3                JM  ARCAJMQC1.0                                                                 20070919041939  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070919041939  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070919043158  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090907082715  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090907082838                      G�O�G�O�G�O�                