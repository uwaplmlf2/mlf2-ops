CDF   (   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900654 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               >A   JA  20070913030506  20090907082835  A9_67024_062                    2C  D   APEX-SBE 2972                                                   846 @Ԕgl��1   @Ԕi�,�@<����o@a�$�/�1   ARGOS   A   A   A   @�ffA  A���A�ffBffBF  BnffB�  B���B�ffB�  Bڙ�B�33C  C�C33C��C(��C333C=��CGL�C[  CoL�C�� C��3C���C���C�� C��3C���C�� Cљ�Cۀ C���C�fC���D�3D��D��D��DٚD� D�3D$ٚD)�3D.� D3�3D8��D=� DB� DG�fDN  DT9�DZ� D`��DgfDmffDs��Dy�3D�  D�Y�D��3D�ٚD�,�D�c3D��3D�p D���D�ffD�ٚD�c3D���D�I�1111111111111111111111111111111111111111111111111111111111111111111111111   @�  A��A�33A���B��BE33Bm��B���B�33B�  Bƙ�B�33B���C ��C
�fC  C��C(��C3  C=ffCG�CZ��Co�C�ffC���C�s3C�� C��fC���C�s3CǦfCр C�ffC�3C��C�� D�fD� D� D� D��D�3D�fD$��D)�fD.�3D3�fD8��D=�3DB�3DG��DM�3DT,�DZs3D`� Df��DmY�Ds��Dy�fD��D�S3D���D��3D�&fD�\�D���D�i�D��fD�` D��3D�\�D��fD�C31111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�$�A�/A�5?A��#AÉ7A�x�A�ZA�jA�K�A��9A��hA��`A�  A�?}A�G�A��yA���A���A�\)A�K�A���A�C�A���A�v�A���A�Av��Apv�Am��Ai�A\=qAT�AJ�A=hsA1�A"~�A��A�DA�y@�`B@��@�dZ@�"�@��9@�O�@�dZ@�z�@�/@��@��\@�Z@�ƨ@�p�@�V@�/@��@{@rn�@k@\I�@T�@Kt�@?�w@:=q@7;d@'+@^5@�y@�;@ bN?�$�?�?�j1111111111111111111111111111111111111111111111111111111111111111111111111   A�$�A�/A�5?A��#AÉ7A�x�A�ZA�jA�K�A��9A��hA��`A�  A�?}A�G�A��yA���A���A�\)A�K�A���A�C�A���A�v�A���A�Av��Apv�Am��Ai�A\=qAT�AJ�A=hsA1�A"~�A��A�DA�y@�`B@��@�dZ@�"�@��9@�O�@�dZ@�z�@�/@��@��\@�Z@�ƨ@�p�@�V@�/@��@{@rn�@k@\I�@T�@Kt�@?�w@:=q@7;d@'+@^5@�y@�;@ bN?�$�?�?�j1111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�5B
�
B
�/B
�B
��B
��B
�BhB<jBI�BC�BK�B>wB<jB9XB7LB2-B/B-B)�B&�B�B
��B
�B
�B
��B
�%B
e`B
ZB
C�B
	7B	�B	�dB	�B	A�B	B�B��B��Bq�BR�BC�B;dB/BD�B>wBE�BhsB�%B�oB��B��B��B�sB�B	  B	�B	5?B	H�B	l�B	�B	��B	�qB	ȴB	��B	��B
�B
49B
E�B
XB
ffB
r�B
r�1111111111111111111111111111111111111111111111111111111111111111111111111   B
�ZB
�B
�5B
�B
��B
��B
��B�B?}BM�BF�BL�B?}B<jB:^B8RB2-B/B-B)�B'�B�B  B
�B
�B
�B
�+B
e`B
[#B
E�B

=B	�B	�qB	�%B	C�B	B�B��B��Br�BS�BD�B<jB0!BD�B?}BF�BhsB�%B�uB��B��B��B�sB�B	  B	�B	5?B	H�B	l�B	�B	��B	�qB	ȴB	��B	��B
�B
49B
E�B
XB
ffB
r�B
r�1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709210237482007092102374820070921023748200709210253212007092102532120070921025321200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070913030503  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070913030506  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070913030506  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070913030511  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070913030511  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070913030511  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070913031305                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070917042437  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070917042441  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070917042441  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070917042445  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070917042445  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070917042446  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070917044120                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828002611  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828002612  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828002616  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828002616  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828002616  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828002616  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828002616  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828014331                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070916185852  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416051307  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416051307  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416051308  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416051308  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416051309  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416051309  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416051309  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416051309  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416051309  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416052338                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070919131929  CV  DAT$            G�O�G�O�F��H                JM  ARCAJMQC1.0                                                                 20070921023748  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070921023748  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070921025321  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090907082710  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090907082835                      G�O�G�O�G�O�                