CDF   (   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   I   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  50   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     $  5|   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  8   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  94   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  9�   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  :�   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  <   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  L  =8   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     $  =�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   ?8   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   H8   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   Q8   CALIBRATION_DATE      	   
                
_FillValue                  �  Z8   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    [   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    [   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    [   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         [,   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         [0   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        [4   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    [8Argo profile    2.2 1.2 19500101000000  2900654 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               AA   JA  20070928025818  20090907082844  A9_67024_065                    2C  D   APEX-SBE 2972                                                   846 @Ԙ&9D�1   @Ԙ+��^@<��l�C�@a��+1   ARGOS   A   A   A   @���A��A���A�ffB��BF  Bn  B���B�ffB�ffB�ffB���B홚C�C33CL�C�fC)ffC3ffC=��CG��C[33Co�C�� C���C���C�ffC��3C��fC���Cǌ�CѦfCۙ�C���CC���DٚDٚD�3DٚD� D� D�3D$ٚD)ٚD.ٚD3� D8��D=�3DBٚDG�fDN3DT` DZ��D`�3Df��DmS3DsffDy� D�3D�ffD��3D�� D�,�D�i�D��3D�i�D���D�` D��3D�\�D�� D�6f1111111111111111111111111111111111111111111111111111111111111111111111111   @�ffA��A�33A���B  BE33Bm33B�33B�  B�  B�  B�ffB�33C �fC  C�C�3C)33C333C=ffCGffC[  Cn�fC�ffC�s3C�� C�L�C���C���C�s3C�s3Cь�Cۀ C�3C� C�� D��D��D�fD��D�3D�3D�fD$��D)��D.��D3�3D8� D=�fDB��DGٚDNfDTS3DZ� D`�fDf��DmFfDsY�Dy�3D��D�` D���D�ٚD�&fD�c3D���D�c3D��fD�Y�D���D�VfD��D�0 1111111111111111111111111111111111111111111111111111111111111111111111111   @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��AڸRAٺ^A��A�ȴAĥ�A��HA�r�A�\)A�M�A��7A��hA���A�+A�l�A�ƨA�?}A�%A��\A���A��DA��9A�v�A�jA��-A�M�A��Aw�Aq�Ag��A_��AX �AN��AGO�AAp�A9|�A&�A��AA	�
Ar�@�G�@�&�@١�@�G�@��m@��@���@��/@�(�@���@��-@�K�@�G�@�=q@�M�@��
@�z�@��@x�9@lj@[��@J�!@<I�@8Q�@2J@#dZ@o@b@?}@;d?��?�33?�331111111111111111111111111111111111111111111111111111111111111111111111111   AڸRAٺ^A��A�ȴAĥ�A��HA�r�A�\)A�M�A��7A��hA���A�+A�l�A�ƨA�?}A�%A��\A���A��DA��9A�v�A�jA��-A�M�A��Aw�Aq�Ag��A_��AX �AN��AGO�AAp�A9|�A&�A��AA	�
Ar�@�G�@�&�@١�@�G�@��m@��@���@��/@�(�@���@��-@�K�@�G�@�=q@�M�@��
@�z�@��@x�9@lj@[��@J�!@<I�@8Q�@2J@#dZ@o@b@?}@;d?��?�33?�331111111111111111111111111111111111111111111111111111111111111111111111111   ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
��B
��B
��B
ƨB
�BBVB%�BM�BM�BO�BM�BI�BD�B?}B:^B2-B-B)�B&�B �B�BVB
��B
�B
�B
�B
�%B
hsB
:^B
�B	��B	��B	�B	�hB	gmB	�B�B�^B�\Bn�BQ�B>wB+B&�B(�B33B;dBB�BL�B^5Bp�B��B�B�^BɺB��B�BB��B	%�B	F�B	q�B	��B	B	��B	�#B
B
�B
�B
:^B
H�B
hsB
}�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111   B
�B
��B
��B
��B
�`BoB,BO�BO�BP�BN�BJ�BE�B?}B;dB33B.B)�B'�B �B�BVB
��B
�B
�B
�!B
�+B
iyB
;dB
�B	��B	��B	�B	�oB	iyB	�B�B�dB�bBo�BR�B?}B,B'�B(�B33B<jBB�BL�B^5Bp�B��B�B�^BɺB��B�BB��B	%�B	F�B	q�B	��B	B	��B	�#B
B
�B
�B
:^B
H�B
hsB
}�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111   <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710060209542007100602095420071006020954200710060213572007100602135720071006021357200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070928025815  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070928025818  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070928025819  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070928025823  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070928025823  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20070928025823  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070928030440                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071002042757  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071002042801  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071002042802  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071002042806  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071002042806  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071002042806  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071002044715                      G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20080828002627  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20080828002627  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20080828002631  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20080828002631  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8a                                                                20080828002631  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8a                                                                20080828002631  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20080828002632  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20080828014443                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071001190257  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416051314  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416051314  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416051314  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416051315  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416051316  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416051316  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416051316  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416051316  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416051316  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416052333                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071001110042  CV  DAT$            G�O�G�O�F��3                JM  ARCAJMQC1.0                                                                 20071006020954  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071006020954  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071006021357  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090907082723  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090907082844                      G�O�G�O�G�O�                