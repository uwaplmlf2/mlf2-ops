CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900582 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               A   JA  20070911050349  20090918073745  A9_67038_025                    2C  D   APEX-SBE 2986                                                   846 @ԓ��Ӫ1   @ԓ����@;�E����@`�S���1   ARGOS   A   A   A   @�33AffA���A���B��BE33Bn��B���B�ffB�33Bř�B���B���C� CffC�C�3C)� C3� C=  CGL�C[ffCo� C���C���C�� C�ffC�s3C��fC��3C�Y�C�@ Cی�C��CC��fD� DٚDٚD��D�fD�3D��D$� D)�3D.�3D3� D8�3D=�fDBٚDG�3DNfDTL�DZ�fD`��Dg3DmL�Ds� Dy��D�,�D�\�D��fD�� D�,�D�ffD���D�l�D�ٚD�ffD��3D�` D���111111111111111111111111111111111111111111111111111111111111111111111111@���A33A�  A�33B��BDffBn  B�33B�  B���B�33B�ffB�ffCL�C33C�fC� C)L�C3L�C<��CG�C[33CoL�C�� C�� C�ffC�L�C�Y�C���C���C�@ C�&fC�s3C�s3C� C���D�3D��D��D��D��D�fD� D$�3D)�fD.�fD3�3D8�fD=��DB��DG�fDM��DT@ DZy�D`� DgfDm@ Dss3Dy��D�&fD�VfD�� D�ٚD�&fD�` D��fD�ffD��3D�` D���D�Y�D��f111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�ƨA���A�%A�%A�O�A�E�A���A�I�A���A�z�A���A��#A��A���A�{A�bNA��7A��\A��A���A���A��FA�ƨA��DA��FA��A�z�A�r�A�hsAtAk�hAf�uAa`BA[ƨAT�RAH�!A=��A4��A1
=A*1A#��A&�A�yA��A ��@�`B@�@�@��@�ȴ@�K�@��R@���@�b@�x�@���@�?}@�9X@�dZ@�j@s��@n��@a��@V�@NV@=V@1�@"��@1'@�y@�9@�111111111111111111111111111111111111111111111111111111111111111111111111A�ƨA���A�%A�%A�O�A�E�A���A�I�A���A�z�A���A��#A��A���A�{A�bNA��7A��\A��A���A���A��FA�ƨA��DA��FA��A�z�A�r�A�hsAtAk�hAf�uAa`BA[ƨAT�RAH�!A=��A4��A1
=A*1A#��A&�A�yA��A ��@�`B@�@�@��@�ȴ@�K�@��R@���@�b@�x�@���@�?}@�9X@�dZ@�j@s��@n��@a��@V�@NV@=V@1�@"��@1'@�y@�9@�111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
L�B
L�B
B�B
T�B
p�B
p�B
}�B
�B0!BD�BJ�BF�BF�BH�BH�BF�BG�BI�BG�BG�BE�B?}B33B�B1B
��B
�B
��B
�3B
p�B
F�B
/B
�B	��B	�5B	��B	v�B	Q�B	?}B	(�B	bB�B��B�9B�B��B��B��B��B��B��B��B�qB��B�sB��B	B	�B	,B	G�B	e`B	o�B	�VB	��B	�}B	�mB
B
�B
49B
H�B
W
B
_;111111111111111111111111111111111111111111111111111111111111111111111111B
L�B
M�B
J�B
W
B
s�B
q�B
�B
�-B1'BF�BK�BG�BG�BI�BI�BG�BG�BI�BH�BG�BF�B@�B49B�B	7B
��B
�B
��B
�?B
q�B
G�B
0!B
�B	��B	�BB	��B	w�B	R�B	@�B	)�B	hB�B��B�?B�B��B��B��B��B��B��B��B�qB��B�sB��B	B	�B	,B	G�B	e`B	o�B	�VB	��B	�}B	�mB
B
�B
49B
H�B
W
B
_;111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709190850242007091908502420070919085024200709190903382007091909033820070919090338200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070911050345  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070911050349  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070911050349  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070911050353  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070911050353  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070911050354  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070911050947                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070915043957  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070915044000  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070915044002  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070915044006  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070915044006  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070915044006  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070915044006  CV  TIME            G�O�G�O�                    JA  ARGQrelo2.1                                                                 20070915044006  CV  LAT$            G�O�G�O�Aݲ-                JA  ARGQrelo2.1                                                                 20070915044006  CV  LON$            G�O�G�O�Cz�                JA  ARUP                                                                        20070915044651                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070915043957  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519014846  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519014847  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519014847  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519014848  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519014848  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519014848  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519014848  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519014848  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519015505                      G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070919085024  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070919085024  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070919090338  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090918073656  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090918073745                      G�O�G�O�G�O�                