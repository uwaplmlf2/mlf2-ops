CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   H   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  >l   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   >�   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   G�   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   P�   CALIBRATION_DATE      	   
                
_FillValue                  �  Y�   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Z|   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Z�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Z�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Z�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Z�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    Z�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Z�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Z�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Z�   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Z�   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Z�   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Z�Argo profile    2.2 1.2 19500101000000  2900582 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               A   JA  20070926050725  20090918073732  A9_67038_028                    2C  D   APEX-SBE 2986                                                   846 @ԗ�x1   @ԗ��io@;��/��@`���R1   ARGOS   A   A   A   @�33A  A�33A���B��BE33BnffB���B�ffB���B�  B�  B�ffCffCffC33C�fC)ffC3ffC=��CG��C[  CoffC��3C���C��fC�� C���C���C�� C�� Cљ�C�s3C� C�s3C���DٚD��D�3DٚD��DٚD�3D$�3D)ٚD.�3D3�3D8�fD=��DB�fDG��DN�DTFfDZ��D`� Dg&fDmY�Ds�fDy�fD�)�D�` D��3D��D�&fD�p D��3D�ffD�� D�ffD��3D�ffD�Y�111111111111111111111111111111111111111111111111111111111111111111111111@���A33A���A�ffB��BD  Bm33B�33B���B�33B�ffB�ffB���C�C�C�fC��C)�C3�C=L�CGL�CZ�3Co�C���C�ffC�� C�Y�C�s3C��fC���CǙ�C�s3C�L�C�Y�C�L�C�s3D�fD��D� D�fD�fD�fD� D$� D)�fD.� D3� D8�3D=�fDB�3DG��DM��DT33DZ�fD`��Dg3DmFfDss3Dy�3D�  D�VfD���D�� D��D�ffD�ٚD�\�D��fD�\�D�ٚD�\�D�P 111111111111111111111111111111111111111111111111111111111111111111111111@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A��A��A�A��mAБhA��TA��A�JA�7LA���A�  A�\)A��A�JA�%A�oA��A��9A��/A�ffA��A�r�A��wA��\A��A�C�A�O�A}��Ax��Ao��Akl�Ac�#AY�;AR�\AMVA?�A5�A+��A!�A%A%A�mAJ@���@���@�^5@ۍP@�;d@�1@���@�n�@��;@��@�&�@�l�@�33@���@�l�@��`@o�;@c"�@Y��@Q��@H �@9�#@*n�@\)@
=@�@�u@(�111111111111111111111111111111111111111111111111111111111111111111111111A��A��A��A�A��mAБhA��TA��A�JA�7LA���A�  A�\)A��A�JA�%A�oA��A��9A��/A�ffA��A�r�A��wA��\A��A�C�A�O�A}��Ax��Ao��Akl�Ac�#AY�;AR�\AMVA?�A5�A+��A!�A%A%A�mAJ@���@���@�^5@ۍP@�;d@�1@���@�n�@��;@��@�&�@�l�@�33@���@�l�@��`@o�;@c"�@Y��@Q��@H �@9�#@*n�@\)@
=@�@�u@(�111111111111111111111111111111111111111111111111111111111111111111111111;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
[#B
[#B
[#B
W
B
O�B
��B
�3B
�BA�BD�B^5B\)B_;B`BBT�BdZB_;B\)B]/BZBVBC�B:^B)�B�BB
�sB
��B
��B
�\B
bNB
L�B
&�B	��B	�
B	�jB	�B	ZB	0!B	VB�B�BB��BB�LB��B��B��B��B��B�RBǮB��B�B�B��B	DB	�B	)�B	E�B	k�B	�7B	��B	�XB	��B	�B
DB
$�B
6FB
F�B
VB
aH111111111111111111111111111111111111111111111111111111111111111111111111B
[#B
[#B
[#B
W
B
YB
��B
�RB
��BC�BF�B`BB]/B`BBaHBVBe`B`BB\)B^5B[#BW
BC�B;dB+B�BB
�sB
��B
��B
�bB
cTB
M�B
'�B	��B	�B	�wB	�%B	[#B	1'B	\B�B�HB��BÖB�RB�B��B��B��B��B�XBȴB��B�#B�B��B	DB	�B	)�B	E�B	k�B	�7B	��B	�XB	��B	�B
DB
$�B
6FB
F�B
VB
aH111111111111111111111111111111111111111111111111111111111111111111111111<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.3(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710040846232007100408462320071004084623200710040848302007100408483020071004084830200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070926050722  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070926050725  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070926050725  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070926050730  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070926050730  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070926050730  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070926051250                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070930051737  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070930051752  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070930051755  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070930051804  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070930051804  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20070930051806  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070930054802                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070930051737  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519014854  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519014854  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519014855  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519014856  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519014856  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519014856  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519014856  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519014856  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519015520                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070929174306  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20071004084623  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071004084623  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071004084830  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090918073637  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090918073732                      G�O�G�O�G�O�                