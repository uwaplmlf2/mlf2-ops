CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   H   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        4   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5$   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���        5l   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        6�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        9\   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        :|   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  =   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o        =L   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >l   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    A�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    D�   CALIBRATION_DATE            	             
_FillValue                  ,  G�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    H   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    H(   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    H,   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         H<   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         H@   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        HD   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    HHArgo profile    2.2 1.2 19500101000000  2900663 Routine-JMA                                                     JMA                                                             PRES            TEMP            PSAL               EA   JA  20070930081034  20090416042720                                  2B  A   APEX-SBE 2981                                                   846 @Ԙ���1   @Ԙ�6lw�@<Q���l�@`���l�D1   ARGOS   A   A   A   @�33A��A���A���BffBF  Bo33B���B���B�  BǙ�B���B�ffCL�C� CffC� C)  C3ffC=  CGffC[� Co�C�� C�s3C���C���C�� C��3C�� C�� Cљ�CۦfC�3C�3C��3D� D� DٚDٚD�3D� D�fD$� D)ٚD.� D3��D8��D=�3DB�fDG��DN�DTY�DZ��D`�3Dg�DmS3Ds� Dy� D�)�D�ffD��3D���D�)�D�s3D���D�i�D���D�c3D�� D�ffD�� 111111111111111111111111111111111111111111111111111111111111111111111111@���@���A�34A�fgB33B@��Bj  B�33B�33B�ffB�  B�33B���C   C
33C�C33C'�3C2�C;�3CF�CZ33Cm��C�ٚC���C��gC��gC�ٚC��C��C��C��4C�  C��C��C��D��D��D�gD�gD� Dl�Ds3D$��D)�gD.l�D3fgD8y�D=� DBs3DGy�DM�gDTgDZFgD`� Df��Dm  DsL�Dy��D�  D�<�D�y�D��3D�  D�I�D��3D�@ Dǳ3D�9�D�fD�<�D�Vf111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A�DA�n�A��TA�A��/Aʛ�A��A��#A��A�bA�/A��A��A��A��\A�/A�VA�A��A�^5A���A�JA�9XA��mA�|�A���A���A�M�A{&�AuXAj��AdQ�AYp�AR�AM�hAG%A@�!A8�A-XA!ƨA�9A~�A~�@��@�~�@��
@�p�@ɲ-@�Z@��P@�Ĝ@�^5@�G�@��@�
=@���@�;d@��`@w�w@j-@\��@XA�@K�
@EV@4j@'�@��@I�@��@��@o111111111111111111111111111111111111111111111111111111111111111111111111A��A�DA�n�A��TA�A��/Aʛ�A��A��#A��A�bA�/A��A��A��A��\A�/A�VA�A��A�^5A���A�JA�9XA��mA�|�A���A���A�M�A{&�AuXAj��AdQ�AYp�AR�AM�hAG%A@�!A8�A-XA!ƨA�9A~�A~�@��@�~�@��
@�p�@ɲ-@�Z@��P@�Ĝ@�^5@�G�@��@�
=@���@�;d@��`@w�w@j-@\��@XA�@K�
@EV@4j@'�@��@I�@��@��@o111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
E�B
A�B
N�B
XB
l�B
�FB
�wBDBJB&�B7LB8RBL�BK�BJ�BG�BE�BG�BF�BC�B@�B<jB2-B'�BhB%B
��B
�5B
�qB
��B
z�B
H�B
'�B	��B	�B	ÖB	��B	�JB	hsB	9XB	�B	VB��B�B��B��B�}B�dBƨB�}BB�B��B��B�B	B	�B	+B	6FB	W
B	|�B	��B	�?B	ƨB	�BB	�B
DB
&�B
:^B
N�B
ZB
dZ111111111111111111111111111111111111111111111111111111111111111111111111B
E�B
A�B
N�B
XB
l�B
�FB
�wBDBJB&�B7LB8RBL�BK�BJ�BG�BE�BG�BF�BC�B@�B<jB2-B'�BhB%B
��B
�5B
�qB
��B
z�B
H�B
'�B	��B	�B	ÖB	��B	�JB	hsB	9XB	�B	VB��B�B��B��B�}B�dBƨB�}BB�B��B��B�B	B	�B	+B	6FB	W
B	|�B	��B	�?B	ƨB	�BB	�B
DB
&�B
:^B
N�B
ZB
dZ111111111111111111111111111111111111111111111111111111111111111111111111G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            JA  ARFMdecpA9_b                                                                20070930081010  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070930081034  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070930081039  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070930081048  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070930081048  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20070930081050  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070930084012                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071004050440  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071004050449  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071004050451  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071004050456  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071004050456  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071004050456  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071004052608                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071004050440  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416041817  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416041817  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416041817  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416041819  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416041819  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416041819  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416041819  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416041819  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416042720                      G�O�G�O�G�O�                