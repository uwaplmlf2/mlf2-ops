CDF   4   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   V   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     X  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  9�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     X  :L   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  ;�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     X  ;�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  =T   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  >�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  ?   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  @\   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  @�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  B   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  Cd   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  C�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  E   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  El   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  F�   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  H   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  Ht   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  I�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  J$   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  K|   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    K�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    O�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    S�   CALIBRATION_DATE            	             
_FillValue                  8  W�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    W�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    W�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    W�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    X    HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  X   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    XD   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    XT   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    XX   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Xh   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Xl   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Xp   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    XtArgo profile    2.2 1.2 19500101000000  2900616 Argo eq. TU                                                     Toshio Suga                                                     PRES            TEMP            PSAL            DOXY               kA   JA  20070811065858  20091204025611                                  2B  A   APEX-SBE 2460                                                   846 @ԋ��#�1   @ԋﴷ��@<������@aQ`A�7L1   ARGOS   B   B   B   F   @���A33Aa��A�  A�33A�33BffB��B0ffBD��BW��Bl��B~��B���B���B�  B�ffB���B���Bę�B���B���B�33B���B�33B�  C�fC
  C� C��C  C  C"��C'��C,� C1ffC6ffC;�3C@ffCEL�COffCZ�CdL�Cn  Cx  C���C��3C�� C��fC��fC��fC��fC���C���C��fC���C��3G�O�C�ٚC���C䙚C�3C�s3DFfD,�D9�DS3DFfDFfD@ D$@ D),�D.3D2� D7� D=9�DB�DG�DL�DQfDUٚDZ�fD_� Dd��Di�3Do�311111111111111111111111111111111111111111111111111111111141111111111111111111111111111  @y��A33AY��A�  A�33A�33BffB��B.ffBB��BU��Bj��B|��B���B���B�  B�ffB���B���BÙ�B���B���B�33B���B�33B�  CffC	� C  CL�C� C� C"�C'L�C,  C0�fC5�fC;33C?�fCD��CN�fCY��Cc��Cm� Cw� C���C�s3C�� C��fC��fC��fC��fC�Y�C�L�C�ffC�L�C��3G�O�CЙ�Cڌ�C�Y�C�s3C�33D&fD�D�D33D&fD&fD  D$  D)�D-�3D2� D7� D=�DA��DF��DK��DP�fDU��DZ�fD_� Ddy�Dis3Do�311111111111111111111111111111111111111111111111111111111141111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�DA��A��A�`BA��/Aذ!AՉ7A�?}A�1A���A�ZA��jA��A��A���A��
A�1'A��jA���A�33A���A�VA�"�A�/A���A�ffA�"�A���A�ĜA��A��+A�"�A�z�A��jA��HA�VA�~�A�oA���A�1A�\)A�VA��RA��9A�M�A��A��FA��HA���A�/A��A��A��A�C�A�A��A\)Av��Ap�Aj�A`��AYAU`BAHjA=�mA6M�A/�
A(�/A
=AA-A �@�S�@�u@��@�Z@�O�@�ȴ@�(�@�{@�ƨ@��#@��
@���@�9X@��T11111111111111111111111111111111111111111111111111111111141111111111111111111111111111  A�DA��A��A�`BA��/Aذ!AՉ7A�?}A�1A���A�ZA��jA��A��A���A��
A�1'A��jA���A�33A���A�VA�"�A�/A���A�ffA�"�A���A�ĜA��A��+A�"�A�z�A��jA��HA�VA�~�A�oA���A�1A�\)A�VA��RA��9A�M�A��A��FA��HA���A�/A��A��A��A�C�A�A��A\)Av��Ap�Aj�A`��AYAU`BAHjA=�mA6M�A/�
A(�/A
=AA-A �@�S�@�u@��@�Z@�O�@�ȴ@�(�@�{@�ƨ@��#@��
@���@�9X@��T11111111111111111111111111111111111111111111111111111111141111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
��B
��B
��B
�B
�wB
��B
�BB
�BB
�B
��B�B �B$�B(�B(�B9XB=qB@�BK�BN�BO�BK�BI�BR�BM�BL�BK�BJ�BO�BN�BJ�BK�BG�BD�BD�BE�BA�BA�BG�BD�BE�B?}B<jB9XB49B0!B+B%�B�BhBB
�B
�BB
�B
��B
ÖB
�?G�O�B
n�B
N�B
$�B
B	�B	�jB	�hB	x�B	_;B	D�B	�B��B�)BȴB��BɺB�
B�)B�yB��B��B	B	�B	�B	-B	D�B	K�B	aH11111111111111111111111111111111111111111111111111111111141111111111111111111111111111  B
��B
��B
��B
�B
�wB
��B
�BB
�BB
�B
��B�B �B$�B(�B(�B9XB=qB@�BK�BN�BO�BK�BI�BR�BM�BL�BK�BJ�BO�BN�BJ�BK�BG�BD�BD�BE�BA�BA�BG�BD�BE�B?}B<jB9XB49B0!B+B%�B�BhBB
�B
�BB
�B
��B
ÖB
�?G�O�B
n�B
N�B
$�B
B	�B	�jB	�hB	x�B	_;B	D�B	�B��B�)BȴB��BɺB�
B�)B�yB��B��B	B	�B	�B	-B	D�B	K�B	aH11111111111111111111111111111111111111111111111111111111141111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�}qB��7B�R�B�&fB���B��B��PB���B�B���B~�1B{>wBzm�Bwq�Bu�VBp��Bm}�BjC�Be�)Ba��B`�BY��BV�^BOk�BLq�BJ/BI
=BHoBD{�BC@�BB�BA��B@ĜB?�PB>��B=��B=%B<!�B91B8>wB6YB2YB2�B0�%B,�B*:^B'�RB%ɺB#C�B H�Bu�B�HB��B��B|�B+B�+G�O�B ,A�VA�$�Aݰ!A�oA���A��!A�A�A��HA�/Ak�AD��A��@�/@��G�O�G�O�G�O�D
�C�S�C�A�C���C�Q�C�VfC��uC���C�)�D�d00000000000000000000000000000000000000000000000000000000040000000000000009990000000000  B�}qB��7B�R�B�&fB���B��B��PB���B�B���B~�1B{>wBzm�Bwq�Bu�VBp��Bm}�BjC�Be�)Ba��B`�BY��BV�^BOk�BLq�BJ/BI
=BHoBD{�BC@�BB�BA��B@ĜB?�PB>��B=��B=%B<!�B91B8>wB6YB2YB2�B0�%B,�B*:^B'�RB%ɺB#C�B H�Bu�B�HB��B��B|�B+B�+G�O�B ,A�VA�$�Aݰ!A�oA���A��!A�A�A��HA�/Ak�AD��A��@�/@��G�O�G�O�G�O�D
�C�S�C�A�C���C�Q�C�VfC��uC���C�)�D�d00000000000000000000000000000000000000000000000000000000040000000000000009990000000000  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA10a                                                                20070811065855  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070811065858  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070811065859  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070811065902  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20070811065902  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070811065902  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070811065903  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070811070919                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20070814042512  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070814042517  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070814042518  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070814042522  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20070814042522  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070814042522  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070814042522  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070814044516                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20090331024554  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090331030653  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090331030653  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090331030654  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090331030655  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090331030655  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030655  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030655  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090331030655  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090331030655  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090331030655  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090331031241                      G�O�G�O�G�O�                JA  ARFMdecpA10b                                                                20090706075931  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090706083212  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090706083212  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090706083213  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090706083214  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20090706083214  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083214  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083214  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090706083214  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090706083214  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090706083214  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090706084252                      G�O�G�O�G�O�                JA  ARFMdecpA10c                                                                20091204022726  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204023105  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204023106  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204023106  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204023107  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcpt19b                                                                20091204023107  QCF$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023107  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023107  QCF$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204023107  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204023107  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204023108  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204025611                      G�O�G�O�G�O�                