CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   Y   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  :    PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  :\   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ;�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  <   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  =�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  >�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  ?@   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  @�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  A    PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  Bd   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  C�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  D$   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  E�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  E�   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  GH   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  H�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  I   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  Jl   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  J�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  L,   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    Ll   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Pl   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Tl   CALIBRATION_DATE            	             
_FillValue                  8  Xl   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    X�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    X�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    X�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    X�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  X�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    X�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Y   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Y   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Y   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Y   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Y    HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Y$Argo profile    2.2 1.2 19500101000000  2900616 Argo eq. TU                                                     Toshio Suga                                                     PRES            TEMP            PSAL            DOXY               lA   JA  20070815185719  20091204025621                                  2B  A   APEX-SBE 2460                                                   846 @ԍ-����1   @ԍ.���@<߾vȴ9@aQ�S���1   ARGOS   A   A   A       @���A33A[33A�ffA���A���B33B33B/��BD  BW��BlffB�33B���B�ffB���B�33B�ffB�ffB�33B�33B���BᙚB�33B�33B���C��C
  C�C��C� C�3C#ffC(� C-L�C2L�C7ffC<  CA�CFL�CO�fCZ33Cd33Cm��Cw�fC�  C��3C�� C��3C��3C��fC�� C���C��fC�33C��C�ٚC�ٚC�  C�ٚC���Cڳ3C�� C�s3C�� D&fDS3D` DffD@ DL�D,�D$�D)�D-��D2��D7� D=L�DB  DF�3DK�3DP�fDU�fDZ�fD_��Dd� Di� Dn� Ds� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @���A33AS33A�ffA���A���B33B33B-��BB  BU��BjffB~ffB���B�ffB���B�33B�ffB�ffB�33B�33B���B���B�33B�33B���CL�C	� C��CL�C  C33C"�fC(  C,��C1��C6�fC;� C@��CE��COffCY�3Cc�3CmL�CwffC�� C��3C�� C��3C�s3C�ffC�� C�Y�C��fC��3C�ٚC���C���C�� Cƙ�CЌ�C�s3C� C�33C�@ DfD33D@ DFfD  D,�D�D#��D(��D-��D2��D7� D=,�DB  DF�3DK�3DP�fDU�fDZ�fD_ٚDd� Di� Dn� Ds` 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�~�A��AᕁA�^A�1'A���A�jA�VA�9XA�  A���A���A��
A��A��A�O�A���A�p�A�x�A��A��+A�Q�A�  A��A���A�+A�dZA�v�A�hsA�bNA�M�A��jA�hsA��#A��-A�9XA�1A���A�l�A�C�A�E�A��
A��!A�v�A�VA��9A���A�ĜA���A���A���A��A��A�VA�O�A�mA|VAx�Av9XAt=qAl9XAc7LA[hsAT�AKl�AA/A7S�A*{A!VA�AVAZA ~�@�~�@�Ĝ@ݲ-@Ϯ@�&�@�G�@�V@��@��F@��9@�~�@�ff@�&�@��P@�r�@�z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�~�A��AᕁA�^A�1'A���A�jA�VA�9XA�  A���A���A��
A��A��A�O�A���A�p�A�x�A��A��+A�Q�A�  A��A���A�+A�dZA�v�A�hsA�bNA�M�A��jA�hsA��#A��-A�9XA�1A���A�l�A�C�A�E�A��
A��!A�v�A�VA��9A���A�ĜA���A���A���A��A��A�VA�O�A�mA|VAx�Av9XAt=qAl9XAc7LA[hsAT�AKl�AA/A7S�A*{A!VA�AVAZA ~�@�~�@�Ĝ@ݲ-@Ϯ@�&�@�G�@�V@��@��F@��9@�~�@�ff@�&�@��P@�r�@�z�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
u�B
P�B
W
B
m�B
��B
�wB
��B
�ZB
�HB{B%B�B�B.B0!B>wBD�BK�BP�BM�BN�BP�BO�BN�BN�BM�BI�BH�BF�BF�BG�BG�BG�BC�BD�BB�BA�BA�B@�B?}B@�B?}B>wB:^B8RB33B49B+B�BPBB
��B
�B
�B
ŢB
�RB
��B
��B
�=B
� B
ZB
1'B
VB	�B	ǮB	��B	{�B	H�B	)�B	DB��B�B��BĜB��B��B�B�BB�B�B�B	%B	DB	�B	(�B	?}B	L�B	VB	q�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B
u�B
P�B
W
B
m�B
��B
�wB
��B
�ZB
�HB{B%B�B�B.B0!B>wBD�BK�BP�BM�BN�BP�BO�BN�BN�BM�BI�BH�BF�BF�BG�BG�BG�BC�BD�BB�BA�BA�B@�B?}B@�B?}B>wB:^B8RB33B49B+B�BPBB
��B
�B
�B
ŢB
�RB
��B
��B
�=B
� B
ZB
1'B
VB	�B	ǮB	��B	{�B	H�B	)�B	DB��B�B��BĜB��B��B�B�BB�B�B�B	%B	DB	�B	(�B	?}B	L�B	VB	q�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B�EB�B�"�B�QhB�z�B��B���B��B��B���B��B~W
Bz�BwBt�BpcTBm��Bh%�Be�B^v�BZl�BV� BPK�BLx�BJ�BH�BF�BC�{BA��B@>wB?�B>dZB=t�B=�B<��B;s�B:�B9v�B9�qB9s�B7p�B5 �B3�HB12-B-��B)�?B(z�B%�9B!L�B�BB��B��B49B7LB�B��B	�B\B��B�A�?}A��AܮA� �A¾wA��TA���A�/ArZAb5?A7�A�@���@�+G�O�G�O�G�O�D!�wD��C��uC�l�C�}PC�܋C�jC���C���C�e�C���C�:=00000000000000000000000000000000000000000000000000000000000000000000000000999000000000000   B�EB�B�"�B�QhB�z�B��B���B��B��B���B��B~W
Bz�BwBt�BpcTBm��Bh%�Be�B^v�BZl�BV� BPK�BLx�BJ�BH�BF�BC�{BA��B@>wB?�B>dZB=t�B=�B<��B;s�B:�B9v�B9�qB9s�B7p�B5 �B3�HB12-B-��B)�?B(z�B%�9B!L�B�BB��B��B49B7LB�B��B	�B\B��B�A�?}A��AܮA� �A¾wA��TA���A�/ArZAb5?A7�A�@���@�+G�O�G�O�G�O�D!�wD��C��uC�l�C�}PC�܋C�jC���C���C�e�C���C�:=00000000000000000000000000000000000000000000000000000000000000000000000000999000000000000   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA10a                                                                20070815185716  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070815185719  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070815185720  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070815185723  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070815185723  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070815185724  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070815190710                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20070819051508  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070819051524  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070819051528  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070819051535  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070819051536  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070819051537  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070819060454                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20090331024555  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090331030655  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090331030656  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090331030656  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090331030657  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030657  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090331030657  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090331030657  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090331030657  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090331031253                      G�O�G�O�G�O�                JA  ARFMdecpA10b                                                                20090706075932  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090706083214  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090706083215  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090706083215  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090706083216  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083216  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090706083217  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090706083217  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090706083217  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090706084304                      G�O�G�O�G�O�                JA  ARFMdecpA10c                                                                20091204022727  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204023108  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204023108  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204023109  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204023110  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023110  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204023110  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204023110  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204023110  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204025621                      G�O�G�O�G�O�                