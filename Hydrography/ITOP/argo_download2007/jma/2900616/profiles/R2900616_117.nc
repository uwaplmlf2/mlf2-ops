CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   Y   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  :    PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  :\   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ;�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  <   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  =�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  >�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  ?@   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  @�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  A    PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  Bd   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  C�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  D$   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  E�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  E�   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  GH   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  H�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  I   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  Jl   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  J�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  L,   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    Ll   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Pl   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Tl   CALIBRATION_DATE            	             
_FillValue                  8  Xl   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    X�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    X�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    X�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    X�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  X�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    X�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Y   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Y   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Y   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Y   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Y    HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Y$Argo profile    2.2 1.2 19500101000000  2900616 Argo eq. TU                                                     Toshio Suga                                                     PRES            TEMP            PSAL            DOXY               uA   JA  20070929085620  20091204025618                                  2B  A   APEX-SBE 2460                                                   846 @Ԙmja�Q1   @Ԙn�r�b@<Nz�G�@a5$�/1   ARGOS   A   A   A       @�33AffA`  A�  A�ffA�ffB��B33B0  BC��BV��Bk33B���B���B�  B�  B���B�ffB�ffB���B�33Bٙ�B♚B�ffB���B�  C��C	��C� CffC�3CffC#ffC(��C-� C2�C733C;��CA  CE�fCO� CYffCc�3Cm�3Cw�fC�ٚC���C�� C�ٚC�33C�&fC��C��fC��3C��3C���C��3C��fC�� C��fC�� C���C�fC� C�s3Dl�Dl�D` DFfD,�D9�D&fD$�D)FfD.S3D3@ D833D=9�DB  DFٚDK� DP�3DU�fDZٚD_� Dd��Di�3Dn��DtL�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�33AffAX  A�  A�ffA�ffB��B33B.  BA��BT��Bi33B34B���B�  B�  B���B�ffB�ffB���B�33Bؙ�BᙚB�ffB���B�  C�C	�C  C�fC33C�fC"�fC(�C-  C1��C6�3C;L�C@� CEffCO  CX�fCc33Cm33CwffC���C�Y�C�� C���C��3C��fC�ٚC��fC��3C��3C���C��3C��fC�� CƦfCЀ Cڌ�C�ffC�@ C�33DL�DL�D@ D&fD�D�DfD#��D)&fD.33D3  D83D=�DA� DF��DK� DP�3DU�fDZ��D_� Dd��Dis3Dnl�Dt,�11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�{A�{A�JA��DA��
A���A�A�Aݥ�A��A�l�A�I�A�bA��TA�{A�dZA��
A���A��A�S�A��A�&�A���A��A�33A�|�A�bA�ȴA�S�A��wA���A��#A�&�A�33A�=qA�"�A�1A��mA�(�A��A�jA�E�A�E�A���A�x�A��A�7LA��A�K�A��!A�hsA�"�A��9A��+A� �A�x�A�oA��A~-A{hsAw��An�Aj-A_�-A[K�AR�`AF��A=��A7?}A/C�A'�7A�PA�A�/A��AM�@�33@�ƨ@��
@϶F@ă@�+@�33@��@�33@�t�@�+@���@���@�5?11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�{A�{A�JA��DA��
A���A�A�Aݥ�A��A�l�A�I�A�bA��TA�{A�dZA��
A���A��A�S�A��A�&�A���A��A�33A�|�A�bA�ȴA�S�A��wA���A��#A�&�A�33A�=qA�"�A�1A��mA�(�A��A�jA�E�A�E�A���A�x�A��A�7LA��A�K�A��!A�hsA�"�A��9A��+A� �A�x�A�oA��A~-A{hsAw��An�Aj-A_�-A[K�AR�`AF��A=��A7?}A/C�A'�7A�PA�A�/A��AM�@�33@�ƨ@��
@϶F@ă@�+@�33@��@�33@�t�@�+@���@���@�5?11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�B	�B	�B
+B
�B
B�B
XB
s�B
�+B
^5B
u�B
z�B
�VB
��B
�B
�)BJB �B+B33B6FB8RB8RBB�BQ�BT�B[#B[#BZBR�BW
BI�BN�BR�BL�BM�BQ�BP�BXBYBXBXBR�BS�BJ�BE�B=qB1'B$�B�BPB
��B
��B
�B
�;B
��B
��B
�3B
��B
�bB
e`B
M�B
�B
JB	�fB	�-B	�DB	r�B	R�B	33B	{B	\B	B�B�B�fB�)B�B�5B�B�B�B	+B	
=B	�B	0!B	B�B	R�B	`B11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B	�B	�B	�B
+B
�B
B�B
XB
s�B
�+B
^5B
u�B
z�B
�VB
��B
�B
�)BJB �B+B33B6FB8RB8RBB�BQ�BT�B[#B[#BZBR�BW
BI�BN�BR�BL�BM�BQ�BP�BXBYBXBXBR�BS�BJ�BE�B=qB1'B$�B�BPB
��B
��B
�B
�;B
��B
��B
�3B
��B
�bB
e`B
M�B
�B
JB	�fB	�-B	�DB	r�B	R�B	33B	{B	\B	B�B�B�fB�)B�B�5B�B�B�B	+B	
=B	�B	0!B	B�B	R�B	`B11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C<��C;,JC;@�CB�qC;&fCBsC<��C=aHC>!�C?�#C?��C?�CAaCD
=CL�CGc�C:�C5�=C6�C4�\C4�C4��C:�C>�-CBV�CA�/CFDCB�ZC=aC6�^C8C7��C81�C9}C6��C3�C<��C:��C:�^C9�;C9]/C9sC8��C8��C8�C9C�C9��C9u�C9-C8/\C7��C6��C6]�C5�3C4��C4uC3.C2$ZC0��C/OC-@ C,_}C*��C)m�C'?}C#�7C!�'CYC�#C'�C:C��CI�C X�B�S�B�Bף�B�JB�ڠB��7B���B��TB���B�8RB�B�B{ZBo+Bl1'Bk��00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000   C<��C;,JC;@�CB�qC;&fCBsC<��C=aHC>!�C?�#C?��C?�CAaCD
=CL�CGc�C:�C5�=C6�C4�\C4�C4��C:�C>�-CBV�CA�/CFDCB�ZC=aC6�^C8C7��C81�C9}C6��C3�C<��C:��C:�^C9�;C9]/C9sC8��C8��C8�C9C�C9��C9u�C9-C8/\C7��C6��C6]�C5�3C4��C4uC3.C2$ZC0��C/OC-@ C,_}C*��C)m�C'?}C#�7C!�'CYC�#C'�C:C��CI�C X�B�S�B�Bף�B�JB�ڠB��7B���B��TB���B�8RB�B�B{ZBo+Bl1'Bk��00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA10a                                                                20070929085617  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070929085620  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070929085621  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070929085624  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070929085624  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20070929085625  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070929090628                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20071003043132  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071003043140  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071003043141  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071003043144  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071003043145  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071003043145  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20071003050506                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20090331024606  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090331030714  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090331030714  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090331030714  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090331030715  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030715  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090331030715  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090331030715  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090331030715  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090331031249                      G�O�G�O�G�O�                JA  ARFMdecpA10b                                                                20090706075937  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090706083237  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090706083237  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090706083238  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090706083239  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083239  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090706083239  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090706083239  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090706083239  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090706084300                      G�O�G�O�G�O�                JA  ARFMdecpA10c                                                                20091204022732  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204023132  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204023132  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204023133  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204023135  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023135  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204023135  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204023135  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204023135  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204025618                      G�O�G�O�G�O�                