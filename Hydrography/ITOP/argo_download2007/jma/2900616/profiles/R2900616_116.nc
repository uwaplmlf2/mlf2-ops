CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   V   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     X  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  9�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     X  :L   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  ;�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     X  ;�   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  =T   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  >�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  ?   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  @\   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  @�   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  B   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  Cd   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  C�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  E   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  El   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  F�   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  H   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  Ht   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  X  I�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     X  J$   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  K|   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    K�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    O�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    S�   CALIBRATION_DATE            	             
_FillValue                  8  W�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    W�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    W�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    W�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    X    HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  X   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    XD   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    XT   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    XX   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Xh   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Xl   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Xp   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    XtArgo profile    2.2 1.2 19500101000000  2900616 Argo eq. TU                                                     Toshio Suga                                                     PRES            TEMP            PSAL            DOXY               tA   JA  20070925045649  20091204025619                                  2B  A   APEX-SBE 2460                                                   846 @ԗ%�m:1   @ԗ(d��@<�M���@a1\(�1   ARGOS   A   A   A       @���A	��A[33A���A���A���B��B33B133BC��BX  BjffB~ffB���B�  B�33B�  B�ffB�  B�  Bϙ�B�  B�33B�  B�  B�ffC�C	�fC33C�3C�fC  C"��C(  C,��C1��C733C<�3CA��CF�3CP��CZ  Cd  Cm��Cx�C��3C�� C��fC��3C���C�L�C�33C�  C�  C�� C��3C���C�� C��C��C���D&fD�D&fDs3DS3D,�D9�D$33D)&fD.` D3,�D8&fD=33DB  DF�3DK��DP��DUٚDZٚD_�3Dd�fDi�3Dn��Dsy�Dy� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111  @s34A   AQ��A�  A�  A�  B34B��B.��BA34BU��Bh  B|  B���B���B�  B���B�33B���B���B�fgB���B�  B���B���B�33C� C	L�C��C�CL�CffC"  C'ffC,33C1  C6��C<�CA  CF�CP  CYffCcffCm  Cw� C��fC�s3C���C�ffC�� C�  C��fC��3C��3C�s3CƦfCЀ C�s3C�@ C�@ C�@ D  D�4D  DL�D,�DgD4D$�D)  D.9�D3gD8  D=�DAٚDF��DK�4DP�gDU�4DZ�4D_��Dd� Dil�DnfgDsS4Dy��11111111111111111111111111111111111111111111111111111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�AߋDAߑhAߋDA�t�A�n�A�p�A�1'A�\)A��A��#A���A��#A��yA��A�ZA���A���A�=qA��PA���A�G�A�JA��A���A���A���A�{A���A�O�A�  A��A��A�1'A�\)A��A�~�A���A���A�(�A���A���A�G�A��A��A�p�A���A�r�A�JA��/G�O�A���A�ƨA�&�A��HAhsAt��Ao�mAg��A\��AU/AN�ADQ�A:�yA5��A-?}A&z�A!S�A�yA�/A�@���@�@�n�@�5?@�ƨ@��9@�E�@�(�@�V@�"�@��R@�S�@�O�@��@�9X@~�+11111111111111111111111111111111111111111111111119111111111111111111111111111111111111  AߋDAߑhAߋDA�t�A�n�A�p�A�1'A�\)A��A��#A���A��#A��yA��A�ZA���A���A�=qA��PA���A�G�A�JA��A���A���A���A�{A���A�O�A�  A��A��A�1'A�\)A��A�~�A���A���A�(�A���A���A�G�A��A��A�p�A���A�r�A�JA��/G�O�A���A�ƨA�&�A��HAhsAt��Ao�mAg��A\��AU/AN�ADQ�A:�yA5��A-?}A&z�A!S�A�yA�/A�@���@�@�n�@�5?@�ƨ@��9@�E�@�(�@�V@�"�@��R@�S�@�O�@��@�9X@~�+11111111111111111111111111111111111111111111111119111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�sB	�mB	�yB	�B
VB
\B
hB
"�B
C�B
ffB
q�B
�VB
��B
�XB
��B
�BB�B7LB6FBB�BN�BQ�BXB[#BXB]/B^5B^5B[#BZBYBXBXBZB[#BYBZBZBYBW
BQ�BL�BF�BH�B=qBB�B6FB"�G�O�B
�B
�NB
��B
ŢB
�XB
�B
jB
D�B
bB	�B	��B	��B	�B	l�B	I�B	1'B	�B	B�B�yB��B��B��B��B�ZB�TB�TB��B	�B	'�B	;dB	=qB	N�B	`BB	l�B	w�11111111111111111111111111111111111111111111111119111111111111111111111111111111111111  B	�sB	�mB	�yB	�B
VB
\B
hB
"�B
C�B
ffB
q�B
�VB
��B
�XB
��B
�BB�B7LB6FBB�BN�BQ�BXB[#BXB]/B^5B^5B[#BZBYBXBXBZB[#BYBZBZBYBW
BQ�BL�BF�BH�B=qBB�B6FB"�G�O�B
�B
�NB
��B
ŢB
�XB
�B
jB
D�B
bB	�B	��B	��B	�B	l�B	I�B	1'B	�B	B�B�yB��B��B��B��B�ZB�TB�TB��B	�B	'�B	;dB	=qB	N�B	`BB	l�B	w�11111111111111111111111111111111111111111111111119111111111111111111111111111111111111  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C:-CFF�C8�}CE��C;ZCN%`CBDZC>:C<PbC<�3CA�XCEr-CEf�CC �CT�CTP�CFW�CK�BC>]�CD�'CFX�CH��CG+CP?CF�3CM�DCBU�CV�HCB��C@[�C<5?C;VC>;C=��C>�3C=��C<�C<mC;��C5��C6z�C6	7C9�C6˅C5�{C6��C:n�C8#�C7S�G�O�C49C4"�C2�HC2|�C2+DG�O�C*_}C*��C)MPC(!C&	7C"��C�ZC�C�HC�hC�C&�C
�C��B��
B�k�BÆ%B�J=B���B�aHB�/B�O�B��;B��}Bv�uBd��B^�Bh�Bg�BfdZ00000000000000000000000000000000000000000000000009000009000000000000000000000000000000  C:-CFF�C8�}CE��C;ZCN%`CBDZC>:C<PbC<�3CA�XCEr-CEf�CC �CT�CTP�CFW�CK�BC>]�CD�'CFX�CH��CG+CP?CF�3CM�DCBU�CV�HCB��C@[�C<5?C;VC>;C=��C>�3C=��C<�C<mC;��C5��C6z�C6	7C9�C6˅C5�{C6��C:n�C8#�C7S�G�O�C49C4"�C2�HC2|�C2+DG�O�C*_}C*��C)MPC(!C&	7C"��C�ZC�C�HC�hC�C&�C
�C��B��
B�k�BÆ%B�J=B���B�aHB�/B�O�B��;B��}Bv�uBd��B^�Bh�Bg�BfdZ00000000000000000000000000000000000000000000000009000009000000000000000000000000000000  G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA10a                                                                20070925045646  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070925045649  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070925045650  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070925045653  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070925045654  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070925045654  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070925050638                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20070928042005  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070928042010  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070928042011  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070928042014  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070928042015  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20070928042015  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070928044759                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20090331024605  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090331030711  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090331030712  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090331030712  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090331030713  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030713  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090331030713  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090331030713  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090331030713  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090331031250                      G�O�G�O�G�O�                JA  ARFMdecpA10b                                                                20090706075936  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090706083235  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090706083235  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090706083235  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090706083237  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083237  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090706083237  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090706083237  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090706083237  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090706084302                      G�O�G�O�G�O�                JA  ARFMdecpA10c                                                                20091204022731  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204023129  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204023129  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204023130  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204023131  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023131  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204023131  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204023131  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204023131  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204025619                      G�O�G�O�G�O�                