CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   Y   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  :    PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  :\   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ;�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  <   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  =�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  >�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  ?@   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  @�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  A    PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  Bd   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  C�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  D$   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  E�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  E�   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  GH   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  H�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  I   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  Jl   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  J�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  L,   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    Ll   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Pl   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Tl   CALIBRATION_DATE            	             
_FillValue                  8  Xl   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    X�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    X�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    X�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    X�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  X�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    X�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Y   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Y   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Y   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Y   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Y    HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Y$Argo profile    2.2 1.2 19500101000000  2900616 Argo eq. TU                                                     Toshio Suga                                                     PRES            TEMP            PSAL            DOXY               wA   JA  20071009085549  20091204025629                                  2B  A   APEX-SBE 2460                                                   846 @Ԛ�"�9E1   @Ԛ����@<������@a9G�z�1   ARGOS   A   A   A       @s33A��AX  A���A�33A���BffB  B/33BDffBX  BnffB���B���B�33B�ffB���B�ffB�ffB�  BΙ�Bٙ�B���B���B���C ffC33C
L�CL�C  C��C��C"��C'� C,��C1ffC6�3C;L�C@�3CF  CP�3CZ� Cd33Cm��Cw� C�  C�  C�ٚC�ٚC���C��C�ٚC�� C���C��fC��3C���C�� C�ffC��C��Cڙ�C��C�fC���D� DL�D33DY�D@ D33D��D$  D(� D-�fD2�3D8FfD=9�DB9�DG,�DL&fDP��DU�fDZ�fD_y�Dd� Di@ DnY�Dt�f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @S33@���AP  A���A�33A���BffB  B-33BBffBV  BlffB34B���B�33B�ffB���B�ffB�ffB�  B͙�Bؙ�B���B���B���B���C�3C	��C��C� C�CL�C"�C'  C,L�C0�fC633C:��C@33CE� CP33CZ  Cc�3CmL�Cw  C�� C�� C���C���C���C���C���C�� C���C�ffC�s3C�Y�C�@ C�&fC���C���C�Y�C�L�C�ffC�Y�D` D,�D3D9�D  D3DٚD#� D(� D-�fD2�3D8&fD=�DB�DG�DLfDP��DU�fDZ�fD_Y�Dd` Di  Dn9�Dt�f11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�dZA�l�A�v�A�t�A�x�A�x�A�|�A�x�A�\)A���A��`A�&�A�\)A��;A�r�AƮAÍPA�VA��RA��A��A�9XA�
=A���A���A��+A�|�A���A�=qA���A���A���A���A��#A�9XA��RA�x�A�"�A���A��PA���A�$�A�C�A���A��A�^5A���A�C�A�hsA���A��!A��!A��A��/A���A�~�A���A���A}�A{�7ArȴAi`BAb��AYVAP�/AC��A8�A-p�A%33A��AA	�hA�#@�33@�^5@◍@ݡ�@ҟ�@υ@�"�@�z�@��m@��@�X@�bN@��@��@�  @���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   A�dZA�l�A�v�A�t�A�x�A�x�A�|�A�x�A�\)A���A��`A�&�A�\)A��;A�r�AƮAÍPA�VA��RA��A��A�9XA�
=A���A���A��+A�|�A���A�=qA���A���A���A���A��#A�9XA��RA�x�A�"�A���A��PA���A�$�A�C�A���A��A�^5A���A�C�A�hsA���A��!A��!A��A��/A���A�~�A���A���A}�A{�7ArȴAi`BAb��AYVAP�/AC��A8�A-p�A%33A��AA	�hA�#@�33@�^5@◍@ݡ�@ҟ�@υ@�"�@�z�@��m@��@�X@�bN@��@��@�  @���11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
hB
hB
hB
hB
oB
oB
oB
hB
-B
0!B
>wB
�B
��B
��B�B&�B(�BC�BE�BJ�B`BBI�B>wBP�BR�BW
BXBXBXBT�BS�BQ�BQ�BN�BM�BL�BL�BN�BN�BN�BO�BQ�BR�BP�BO�BK�BH�BA�B5?B1'B(�B�BPB
��B
�B
�5B
��B
��B
�'B
��B
y�B
N�B
/B
B	�5B	��B	t�B	K�B	,B	1B��B�NB�fBǮB�XB�'BȴBÖBB�mB	B	B	uB	"�B	49B	A�B	N�B	`BB	gm11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   B
hB
hB
hB
hB
oB
oB
oB
hB
-B
0!B
>wB
�B
��B
��B�B&�B(�BC�BE�BJ�B`BBI�B>wBP�BR�BW
BXBXBXBT�BS�BQ�BQ�BN�BM�BL�BL�BN�BN�BN�BO�BQ�BR�BP�BO�BK�BH�BA�B5?B1'B(�B�BPB
��B
�B
�5B
��B
��B
�'B
��B
y�B
N�B
/B
B	�5B	��B	t�B	K�B	,B	1B��B�NB�fBǮB�XB�'BȴBÖBB�mB	B	B	uB	"�B	49B	A�B	N�B	`BB	gm11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C9�!C8� C:��C:_;C7�yC:"NC:3uC;8�CF�C?Y�C@�JCA?}CBo�CD��CN��CP�CP��CQ��CRJ�CQ��CP�CN{CRt{CE�CEGmCDCAC�CCZCB�CD$CC�CB��CB��CC7�CC#CB��CB�VCBI�CA�CA��C?;C<��C;B�C;�C:^�C9�C:2oC:TC9\)C9e`C9C8C6��C6�C5D�C3��C2�VC2,JC1m�C0��C-��C+w�C*nVC'��C&<�C#QhC��C�C^C:^C	�C��B�MPB��B��B�lB�XB�oB���B�wLB�lB��ZB���B��!B~��Bx�=BsW
Bn��Bd}�00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000   C9�!C8� C:��C:_;C7�yC:"NC:3uC;8�CF�C?Y�C@�JCA?}CBo�CD��CN��CP�CP��CQ��CRJ�CQ��CP�CN{CRt{CE�CEGmCDCAC�CCZCB�CD$CC�CB��CB��CC7�CC#CB��CB�VCBI�CA�CA��C?;C<��C;B�C;�C:^�C9�C:2oC:TC9\)C9e`C9C8C6��C6�C5D�C3��C2�VC2,JC1m�C0��C-��C+w�C*nVC'��C&<�C#QhC��C�C^C:^C	�C��B�MPB��B��B�lB�XB�oB���B�wLB�lB��ZB���B��!B~��Bx�=BsW
Bn��Bd}�00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA10a                                                                20071009085546  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071009085549  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071009085550  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071009085553  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071009085554  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071009085554  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20071009090618                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20071013043242  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071013043247  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071013043248  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071013043252  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071013043252  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071013043252  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20071013051838                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20090331024609  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090331030718  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090331030718  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090331030719  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090331030720  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030720  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090331030720  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090331030720  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090331030720  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090331031302                      G�O�G�O�G�O�                JA  ARFMdecpA10b                                                                20090706075938  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090706083242  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090706083242  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090706083243  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090706083244  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083244  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090706083244  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090706083244  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090706083244  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090706084314                      G�O�G�O�G�O�                JA  ARFMdecpA10c                                                                20091204022733  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204023138  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204023138  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204023139  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204023140  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023140  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204023140  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204023140  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204023140  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204025629                      G�O�G�O�G�O�                