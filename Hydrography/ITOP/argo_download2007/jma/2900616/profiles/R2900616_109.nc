CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   [   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     l  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  :   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     l  :d   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ;�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     l  <,   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  =�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ?   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  ?`   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  @�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  A(   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  B�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  D    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  D\   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  E�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  F$   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  G�   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  H�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  IX   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  J�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  K    	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  L�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    L�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    P�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    T�   CALIBRATION_DATE            	             
_FillValue                  8  X�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Y   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Y   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Y   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Y   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Y   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    YT   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Yd   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Yh   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Yx   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Y|   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Y�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Y�Argo profile    2.2 1.2 19500101000000  2900616 Argo eq. TU                                                     Toshio Suga                                                     PRES            TEMP            PSAL            DOXY               mA   JA  20070820085539  20091204025622                                  2B  A   APEX-SBE 2460                                                   846 @Ԏf�kU1   @Ԏk/��k@=�+@aJ�\(��1   ARGOS   A   A   A       @���A��Ac33A�  A���A�  B33BffB1��BE��BW33Bj  B~  B���B���B�  B���B���B�  B�  BΙ�B�  B���B���B���B���C��C	�CffC��C33C�3C"��C(  C,ffC2  C7�3C<�3CAffCE�fCP�CY�3CcL�Cm��Cw��C�  C�ٚC�ٚC��fC�� C��fC���C��3C�L�C���C�Y�C���C�  C�  C��C�  C�ٚC�� C�fC�Y�D  D,�D,�D  DY�D` DL�D$9�D)3D.  D2��D7� D<��DB3DGL�DL&fDP��DU��DZ��D_� Dd� Di� Dn� Ds��DyfDz��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  AfgA\��A���A���A���B��B��B0  BD  BU��BhffB|ffB�  B���B�33B���B���B�33B�33B���B�33B�  B�  B�  B���C34C�4C  C34C��CL�C"fgC'��C,  C1��C7L�C<L�CA  CE� CO�4CYL�Cb�gCmfgCw34C���C��gC��gC��3C���C��3C�fgC�� C��C�Y�C�&gC�Y�C���C���C�ٚC���CڦgC��C�s3C�&gDfD3D3DfD@ DFfD33D$  D(��D-�fD2�3D7�fD<� DA��DG33DL�DP�3DU� DZ� D_�fDd�fDi�fDn�fDs� Dx��Dz� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A��A�ĜA��A�A��A�p�A�VA��TA� �A��RA��yA�`BA���A��A�33A�`BA�Q�A�C�A�ZA�bNA�t�A�hsA��TA��A���A�33A��jA�$�A��yA���A���A�&�A��DA���A�^5A�\)A��jA�bA�^5A�M�A��wA���A�?}A�$�A��-A��\A�ffA�%A��-A�p�A�$�A���A�;dA�ĜA��-A~��Azn�Av�uAn9XAe�-A]+AT-AL��ACC�A;��A4bA+t�A"v�Ax�A(�A	�@�
=@��;@�M�@��/@ܴ9@��H@��@��@�K�@�V@�/@��/@�G�@�j@�l�@�K�@��9@�J1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A��A�ĜA��A�A��A�p�A�VA��TA� �A��RA��yA�`BA���A��A�33A�`BA�Q�A�C�A�ZA�bNA�t�A�hsA��TA��A���A�33A��jA�$�A��yA���A���A�&�A��DA���A�^5A�\)A��jA�bA�^5A�M�A��wA���A�?}A�$�A��-A��\A�ffA�%A��-A�p�A�$�A���A�;dA�ĜA��-A~��Azn�Av�uAn9XAe�-A]+AT-AL��ACC�A;��A4bA+t�A"v�Ax�A(�A	�@�
=@��;@�M�@��/@ܴ9@��H@��@��@�K�@�V@�/@��/@�G�@�j@�l�@�K�@��9@�J1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
�B
�`B
�B
��B
�B
ŢB
��B
��B
�NB
�`BoB'�B@�BR�BZB`BBbNB_;B_;B^5B[#BYBT�BQ�BP�BP�BM�BL�BL�BK�BI�BH�BG�BG�BF�BF�BF�BF�BF�B9XB<jB:^B;dB:^B7LB5?B0!B(�B�B{B
=B
��B
�B
�NB
��B
�}B
�3B
��B
�DB
cTB
<jB
{B	�B	��B	��B	�DB	jB	I�B	(�B	hB�B�ZB�#B�B�TB��B�#B�)B�sB��B	
=B	�B	+B	6FB	?}B	P�B	^5B	jB	r�B	y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�B
�B
�`B
�B
��B
�B
ŢB
��B
��B
�NB
�`BoB'�B@�BR�BZB`BBbNB_;B_;B^5B[#BYBT�BQ�BP�BP�BM�BL�BL�BK�BI�BH�BG�BG�BF�BF�BF�BF�BF�B9XB<jB:^B;dB:^B7LB5?B0!B(�B�B{B
=B
��B
�B
�NB
��B
�}B
�3B
��B
�DB
cTB
<jB
{B	�B	��B	��B	�DB	jB	I�B	(�B	hB�B�ZB�#B�B�TB��B�#B�)B�sB��B	
=B	�B	+B	6FB	?}B	P�B	^5B	jB	r�B	y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C96FC9D�C9�C:oC>�C@G�CAO�CE��CF�CG�PCO,JCO�'CQ�;CY'�CT%`CS�!CQ;CN�sCN��CJ�\CMr-CGAHCD!�CEԼCB0bCA(1CC6FC@�+C@F�C?�bC?R�C?��C?��C?!C>��C=��C=fC<�C;��C;O�C?�C<�XC<T�C:��C9z�C8wLC7xC6��C5�jC4{dC2��C0�3C/�C-@�C*�uC(��C&;#C#/\C�CQ�C��C�C�C
ĜC�C�C��C_�B���B�}qB��Bݳ�BҞ5B�O�B�E�B���B�5�B�]/B��-Bz�Bh�`BZ�5BJ�qBBgmB:1'B46FB.Q�B)�B$�jB"�B!�q0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 C96FC9D�C9�C:oC>�C@G�CAO�CE��CF�CG�PCO,JCO�'CQ�;CY'�CT%`CS�!CQ;CN�sCN��CJ�\CMr-CGAHCD!�CEԼCB0bCA(1CC6FC@�+C@F�C?�bC?R�C?��C?��C?!C>��C=��C=fC<�C;��C;O�C?�C<�XC<T�C:��C9z�C8wLC7xC6��C5�jC4{dC2��C0�3C/�C-@�C*�uC(��C&;#C#/\C�CQ�C��C�C�C
ĜC�C�C��C_�B���B�}qB��Bݳ�BҞ5B�O�B�E�B���B�5�B�]/B��-Bz�Bh�`BZ�5BJ�qBBgmB:1'B46FB.Q�B)�B$�jB"�B!�q0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA10a                                                                20070820085536  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070820085539  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070820085540  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070820085544  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070820085544  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070820085544  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070820090509                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20070824042842  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070824042847  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070824042848  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070824042852  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070824042853  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070824042853  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20070824044730                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20090331024557  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090331030658  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090331030658  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090331030658  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090331030659  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030659  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090331030659  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090331030659  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090331030700  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090331031255                      G�O�G�O�G�O�                JA  ARFMdecpA10b                                                                20090706075932  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090706083217  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090706083217  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090706083218  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090706083219  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083219  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090706083219  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090706083219  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090706083219  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090706084307                      G�O�G�O�G�O�                JA  ARFMdecpA10c                                                                20091204022728  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204023111  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204023111  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204023111  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204023113  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023113  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204023113  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204023113  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204023113  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204025622                      G�O�G�O�G�O�                