CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   [   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     l  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  :   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     l  :d   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ;�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     l  <,   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  =�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ?   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  ?`   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  @�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  A(   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  B�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  D    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  D\   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  E�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  F$   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  G�   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  H�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  IX   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  J�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  K    	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  L�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    L�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    P�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    T�   CALIBRATION_DATE            	             
_FillValue                  8  X�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Y   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Y   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Y   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Y   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Y   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    YT   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Yd   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Yh   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Yx   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Y|   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Y�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Y�Argo profile    2.2 1.2 19500101000000  2900616 Argo eq. TU                                                     Toshio Suga                                                     PRES            TEMP            PSAL            DOXY               vA   JA  20071004030025  20091204025629                                  2B  A   APEX-SBE 2460                                                   846 @ԙ���ax1   @ԙ�ʆA�@<��7Kƨ@a5�hr�!1   ARGOS   A   A   A       @�ffA��A\��A���A���A�  B��B  B0ffBDffBX��Bm33B}��B�  B�ffB�ffB���B���B���B���B�ffB�33B�33B�ffB�33C 33C33C
�C�3C�C  C  C"� C'� C,� C1ffC6ffC;�fCA33CF��CPL�CZL�Cd�Cn33Cx  C��C��3C�  C�ٚC��3C�ٚC���C�� C�ٚC��3C��3C�@ C�33C�  C�ٚCг3Cڙ�C��CC���DL�D3D
�3DٚDfD&fD` D$,�D(�fD-�fD2ٚD7��D=L�DB9�DG3DK��DP��DU��DZ�fD_� Dd�fDi� Dn� Ds� Dys3Dz,�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @l��A��AT��A���A���A�  B��B  B.ffBBffBV��Bk33B{��B�  B�ffB�ffB���B���B���B���B�ffB�33B�33B�ffB�33B�ffC�3C	��C33C��C� C� C"  C'  C,  C0�fC5�fC;ffC@�3CF�CO��CY��Cc��Cm�3Cw� C�ٚC��3C�� C���C��3C���C�Y�C�� C���C�s3C��3C�  C��3C�� Cƙ�C�s3C�Y�C�L�C�Y�C�Y�D,�D�3D
�3D��D�fDfD@ D$�D(�fD-�fD2��D7��D=,�DB�DF�3DK��DP��DU��DZ�fD_� Dd�fDi� Dn� Ds� DyS3Dz�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A�33A�RA��A�|�A�VA�p�A�ȴA�v�A�ffA�;dA�1A���A�`BAЬAʗ�A��A�A���A��A���A�$�A�dZA�VA��DA�oA��PA��A�9XA��yA��RA�-A�r�A���A�+A�A�I�A�{A���A��;A�l�A�n�A��PA��A�oA�XA��A�ȴA�dZA��A�&�A�XA��RA�r�A��uA���A�9XA�G�A|��Aw��As��Ak�mAdVA\��AR  AJVAA�A9�wA.��A!�-A�HA��A
�u@�&�@�7@�@��T@ו�@��#@��;@�b@��@��@��@�\)@�dZ@�33@�t�@��@�bN@��@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�33A�RA��A�|�A�VA�p�A�ȴA�v�A�ffA�;dA�1A���A�`BAЬAʗ�A��A�A���A��A���A�$�A�dZA�VA��DA�oA��PA��A�9XA��yA��RA�-A�r�A���A�+A�A�I�A�{A���A��;A�l�A�n�A��PA��A�oA�XA��A�ȴA�dZA��A�&�A�XA��RA�r�A��uA���A�9XA�G�A|��Aw��As��Ak�mAdVA\��AR  AJVAA�A9�wA.��A!�-A�HA��A
�u@�&�@�7@�@��T@ו�@��#@��;@�b@��@��@��@�\)@�dZ@�33@�t�@��@�bN@��@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
�B
^5B
�oB
�B
�B
�'B
��B
��B
��B
��B
��B
=B/B6FBC�B=qB�B	7BDB�B>wBC�BG�BP�BQ�BQ�BR�BR�BS�BT�BT�BW
BS�BQ�BO�BM�BH�BH�BI�BL�BK�BM�BN�BO�BM�BI�BA�BA�B49B,B�BDBB
��B
�B
�ZB
ǮB
��B
�hB
~�B
YB
6FB
uB	�ZB	��B	��B	y�B	Q�B	#�B	�B	  B�B��BÖB�B�/B�5B�#B�yB��B��B	
=B	VB	!�B	33B	B�B	L�B	Q�B	W
B	l�B	m�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�B
^5B
�oB
�B
�B
�'B
��B
��B
��B
��B
��B
=B/B6FBC�B=qB�B	7BDB�B>wBC�BG�BP�BQ�BQ�BR�BR�BS�BT�BT�BW
BS�BQ�BO�BM�BH�BH�BI�BL�BK�BM�BN�BO�BM�BI�BA�BA�B49B,B�BDBB
��B
�B
�ZB
ǮB
��B
�hB
~�B
YB
6FB
uB	�ZB	��B	��B	y�B	Q�B	#�B	�B	  B�B��BÖB�B�/B�5B�#B�yB��B��B	
=B	VB	!�B	33B	B�B	L�B	Q�B	W
B	l�B	m�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C>�ZC9u?C:  C:��C;oC=�C<+DC<t�CF5C;�C;��CF'CI�}CO�CN�CN�CN��CKwCJLCM!HCKo�CG�NCE��CJ��CC�CA��C@�C?�7C?�RC@�7C@MPCB��CC��CC��CC��CC��CC��CDRCC�RC@a�C?�9C;ٚC:�!C:`BC:C9ӶC8�}C9�1C9�C8��C7�`C5�C6� C67�C5�mC4CTC1��C0-PC/#C.sC,�C*t�C)�qC&�PC%��C"KC�bC�DC��CX�C'mCv�B��B�  B�B�i�B��B�>�B�r�B�� B�(sB�!�B�ffB��B�O�B~r�BwBqQ�Bj��Bk]/Bl>w0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 C>�ZC9u?C:  C:��C;oC=�C<+DC<t�CF5C;�C;��CF'CI�}CO�CN�CN�CN��CKwCJLCM!HCKo�CG�NCE��CJ��CC�CA��C@�C?�7C?�RC@�7C@MPCB��CC��CC��CC��CC��CC��CDRCC�RC@a�C?�9C;ٚC:�!C:`BC:C9ӶC8�}C9�1C9�C8��C7�`C5�C6� C67�C5�mC4CTC1��C0-PC/#C.sC,�C*t�C)�qC&�PC%��C"KC�bC�DC��CX�C'mCv�B��B�  B�B�i�B��B�>�B�r�B�� B�(sB�!�B�ffB��B�O�B~r�BwBqQ�Bj��Bk]/Bl>w0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA10a                                                                20071004030022  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071004030025  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071004030026  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071004030030  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071004030030  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071004030030  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20071004031331                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20071008040844  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071008040850  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071008040851  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071008040854  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071008040854  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071008040855  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20071008050413                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20090331024608  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090331030716  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090331030716  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090331030716  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090331030717  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030717  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090331030717  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090331030717  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090331030718  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090331031303                      G�O�G�O�G�O�                JA  ARFMdecpA10b                                                                20090706075937  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090706083240  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090706083240  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090706083240  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090706083242  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083242  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090706083242  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090706083242  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090706083242  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090706084315                      G�O�G�O�G�O�                JA  ARFMdecpA10c                                                                20091204022732  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204023135  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204023136  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204023136  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204023137  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023137  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204023137  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204023137  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204023137  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204025629                      G�O�G�O�G�O�                