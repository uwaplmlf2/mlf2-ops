CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   Y   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  :    PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  :\   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ;�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     d  <   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  =�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  >�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  ?@   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  @�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  A    PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  Bd   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  C�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  D$   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  E�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  E�   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  GH   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  H�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  I   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  Jl   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     d  J�   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  L,   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    Ll   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Pl   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Tl   CALIBRATION_DATE            	             
_FillValue                  8  Xl   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    X�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    X�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    X�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    X�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  X�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    X�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Y   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Y   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Y   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Y   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Y    HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Y$Argo profile    2.2 1.2 19500101000000  2900616 Argo eq. TU                                                     Toshio Suga                                                     PRES            TEMP            PSAL            DOXY               yA   JA  20071020045723  20091204025628                                  2B  A   APEX-SBE 2460                                                   846 @ԝe|ƻ[1   @ԝjT�>3@<�`A�7L@aA�����1   ARGOS   A   A   A       @�  A33AT��A�ffA�ffA噚BffB33B333BDffBW��Bk33B��B�33B���B�  B�ffB�  B���B�ffB�33B�33B���C �C�C	��C��C��CL�CL�C"ffC&��C,  C1ffC5�fC;33C?��CE  CO  CYL�Cd� CnffCxL�C��3C��3C���C�ٚC�� C���C�ٚC�  C�  C��C�  C���C�ٚC�ٚCƳ3CЦfC�s3C䙚C��C��fDl�DffD` DFfD�D�D�D$�D)&fD.FfD39�D8&fD=&fDB  DF�fDK�fDP��DU�fDZ��D_�3Dd��Di�fDn� Ds�3Dy33D{  11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   @�  A33AL��A�ffA�ffAᙚBffB33B133BBffBU��Bi33B}��B�33B���B�  B�ffB�  B���B�ffB�33B�33B���B�34C��C	L�CL�CL�C��C��C!�fC&L�C+� C0�fC5ffC:�3C?L�CD� CN� CX��Cd  Cm�fCw��C��3C��3C�Y�C���C�@ C�L�C���C�� C�� C���C�� C���C���C���C�s3C�ffC�33C�Y�C���C��fDL�DFfD@ D&fD��D��D��D#��D)fD.&fD3�D8fD=fDA� DF�fDK�fDP��DU�fDZ��D_�3Dd��DiffDn` Dss3Dy3Dz� 11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��
A���A���A��;A��;A��HA��mA��`AځA�\)A��A˼jAǣ�A� �A�+A�O�A�A��mA�O�A�C�A��!G�O�A��A��A�x�A���A��A��A�;dA�ZA�33A���A�+A�S�A��/A��DA��A���A�A�C�A��A���A��A��\A�\)A���A���A���A�;dA���A���A��DA�bNA��A{��AxAs�FAp�\Ai\)Ac;dA]K�AS�mAMS�AE33A@=qA4�/A,��A#�AS�Ax�AZ@�(�@�ff@��`@�&�@�A�@�`B@���@���@��!@�ff@�%@�  @���@���@��u@��+@�M�@�X11111111111111111111191111111111111111111111111111111111111111111111111111111111111111111   A��
A���A���A��;A��;A��HA��mA��`AځA�\)A��A˼jAǣ�A� �A�+A�O�A�A��mA�O�A�C�A��!G�O�A��A��A�x�A���A��A��A�;dA�ZA�33A���A�+A�S�A��/A��DA��A���A�A�C�A��A���A��A��\A�\)A���A���A���A�;dA���A���A��DA�bNA��A{��AxAs�FAp�\Ai\)Ac;dA]K�AS�mAMS�AE33A@=qA4�/A,��A#�AS�Ax�AZ@�(�@�ff@��`@�&�@�A�@�`B@���@���@��!@�ff@�%@�  @���@���@��u@��+@�M�@�X11111111111111111111191111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
ffB
ffB
gmB
ffB
ffB
ffB
ffB
ffB
k�B
�B
�JB
��B
�B�B�B'�B.B.B6FB<jBD�G�O�BS�BVBXB[#B[#BZB]/B]/B\)B[#B[#B[#B[#BaHB`BBaHB`BB_;B_;BYBI�B49B)�B�BbBB
��B
�B
�mB
�5B
��B
�^B
��B
�{B
~�B
n�B
N�B
6FB
�B	�B	��B	�-B	��B	n�B	O�B	&�B	VB�B�ZB�B�B��B�
B�B�HB�B�B��B	1B	�B	(�B	33B	G�B	N�B	aHB	q�B	t�11111111111111111111191111111111111111111111111111111111111111111111111111111111111111111   B
ffB
ffB
gmB
ffB
ffB
ffB
ffB
ffB
k�B
�B
�JB
��B
�B�B�B'�B.B.B6FB<jBD�G�O�BS�BVBXB[#B[#BZB]/B]/B\)B[#B[#B[#B[#BaHB`BBaHB`BB_;B_;BYBI�B49B)�B�BbBB
��B
�B
�mB
�5B
��B
�^B
��B
�{B
~�B
n�B
N�B
6FB
�B	�B	��B	�-B	��B	n�B	O�B	&�B	VB�B�ZB�B�B��B�
B�B�HB�B�B��B	1B	�B	(�B	33B	G�B	N�B	aHB	q�B	t�11111111111111111111191111111111111111111111111111111111111111111111111111111111111111111   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�C<�
C;f%C;�9C;��C;�yC:��C<VC<&�C=FC@��C@�RC@R-CI� CN4�C[�DCUVFCL��CK�CF��C?CA$G�O�C;�fC=�hC>;#C>VCB�=C=o\C<$�C=�1C=?}C<��C<��C;{�C9�1C7�
C7�HC7z�C7o�C7w
C7�C7&�C7߾C5Z�C4�C4��C4S�C3kDC2�LC1	�C2ܬC2ƨC2�C09C/C.GmC+��C*�dC) �C'z^C%�uC#�+C!U�C�bC�VC�LC�wC�bCB�CC �%B�7LB�oB�.B�C�B���B�XB�[#B�5B��B� �B�B�)�B��sBw�Bs��Bl�1Bh��Bf�500000000000000000000090000000000000000000000000000000000000000000000000000000000000000000   C<�
C;f%C;�9C;��C;�yC:��C<VC<&�C=FC@��C@�RC@R-CI� CN4�C[�DCUVFCL��CK�CF��C?CA$G�O�C;�fC=�hC>;#C>VCB�=C=o\C<$�C=�1C=?}C<��C<��C;{�C9�1C7�
C7�HC7z�C7o�C7w
C7�C7&�C7߾C5Z�C4�C4��C4S�C3kDC2�LC1	�C2ܬC2ƨC2�C09C/C.GmC+��C*�dC) �C'z^C%�uC#�+C!U�C�bC�VC�LC�wC�bCB�CC �%B�7LB�oB�.B�C�B���B�XB�[#B�5B��B� �B�B�)�B��sBw�Bs��Bl�1Bh��Bf�500000000000000000000090000000000000000000000000000000000000000000000000000000000000000000   G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA10a                                                                20071020045720  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071020045723  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071020045724  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071020045727  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071020045727  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071020045728  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20071020050624                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20071023043559  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071023043604  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071023043605  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071023043609  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071023043609  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071023043609  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20071023052306                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20090331024612  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090331030723  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090331030723  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090331030723  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090331030724  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030724  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090331030724  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090331030724  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090331030724  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090331031301                      G�O�G�O�G�O�                JA  ARFMdecpA10b                                                                20090706075939  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090706083247  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090706083248  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090706083248  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090706083249  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083249  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090706083249  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090706083249  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090706083249  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090706084313                      G�O�G�O�G�O�                JA  ARFMdecpA10c                                                                20091204022734  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204023143  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204023143  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204023144  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204023145  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023145  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204023145  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204023145  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204023145  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204025628                      G�O�G�O�G�O�                