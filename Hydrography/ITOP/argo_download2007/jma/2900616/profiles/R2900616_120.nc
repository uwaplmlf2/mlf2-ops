CDF   ,   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PROF        N_PARAM       N_LEVELS   [   N_CALIB       	N_HISTORY                     B   	DATA_TYPE                  comment       	Data type      
_FillValue                    6�   FORMAT_VERSION                 comment       File format version    
_FillValue                    6�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    6�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    6�   PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    6�   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  7   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  7D   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  @  7�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        7�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    7�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    7�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    7�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     7�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    8   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    8   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  8   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    8X   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8\   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    8d   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            8h   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             8p   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             8x   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    8�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    8�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    8�   PROFILE_DOXY_QC                	long_name         #Global quality flag of DOXY profile    conventions       Argo reference table 2a    
_FillValue                    8�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     l  8�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  :   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     l  :d   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ;�   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     l  <,   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  =�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  ?   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  ?`   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  @�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  A(   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  B�   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  D    PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  D\   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  E�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  F$   DOXY         
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  G�   DOXY_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  H�   DOXY_ADJUSTED            
      	   	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   	valid_min                	valid_max         D"�    comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  IX   DOXY_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  \  J�   DOXY_ADJUSTED_ERROR          
         	long_name         DISSOLVED OXYGEN   
_FillValue        G�O�   units         micromole/kg   comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     l  K    	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  @  L�   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    L�   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    P�   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    T�   CALIBRATION_DATE            	             
_FillValue                  8  X�   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    Y   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    Y   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    Y   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    Y   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  Y   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    YT   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    Yd   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    Yh   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         Yx   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         Y|   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        Y�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    Y�Argo profile    2.2 1.2 19500101000000  2900616 Argo eq. TU                                                     Toshio Suga                                                     PRES            TEMP            PSAL            DOXY               xA   JA  20071014185712  20091204025630                                  2B  A   APEX-SBE 2460                                                   846 @Ԝ%΁��1   @Ԝ1�˪@<ٙ����@aB���m1   ARGOS   A   A   A       @s33AffAQ��A���A�33A�33BffBffB/33BC33BVffBn  B���B�  B���B�ffB���B���B�33B�ffB���B�ffB♚B�  B�ffC ffC�C	��C�C�C��C��C#  C'� C,��C1��C7  C;��C@��CE��COL�CYL�Cc��Cn��Cx� C��C�&fC��fC��fC�  C�  C�  C�ٚC��fC�ٚC�� C�� C�s3C�ffCƀ C�&fC��C�ٚCC�� DS3DL�DL�D` DL�D  D,�D$9�D)&fD.3D3  D7�3D<ٚDB9�DG  DL  DQ3DV  DZٚD_�3Dd��Di�fDn��Ds�3Dy�D{  1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @S33@���AI��A���A�33A�33BffBffB-33BA33BTffBl  B��B�  B���B�ffB���B���B�33B�ffB���B�ffBᙚB�  B�ffB���C��C	L�C��C��C�CL�C"� C'  C,L�C1�C6� C;L�C@�CEL�CN��CX��Cc�Cn�Cx  C�ٚC��fC��fC��fC�� C�� C�� C���C��fC���C�� C�@ C�33C�&fC�@ C��fC�ٚC䙚C�Y�C�@ D33D,�D,�D@ D,�D  D�D$�D)fD-�3D2� D7�3D<��DB�DG  DL  DP�3DU� DZ��D_�3Dd��Di�fDn��Ds�3Dx��Dz� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��A��A��A��A� �A�"�A��A���Aڏ\A١�Aכ�A��#A�
=Aʥ�A�
=A�?}A���A�-A��
A���A���A�t�A�VA�x�A�S�A���A�VA��HA��A�\)A��jA���A�%A�dZA�ĜA��yA�`BA�1'A��HA���A�{A��HA��A�ƨA�v�A��jA��A���A�C�A���A���A�r�A��TA��!A�&�A��RA�A|n�Ax�yAudZAp�Af��A_��A[�PAQS�AJA�AB�HA6A�A)�A!x�A�A��AdZA ��@�I�@�@��#@�p�@���@�"�@��+@��!@�j@��`@�~�@�ȴ@���@�v�@�@�z�@���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A��A��A� �A�"�A��A���Aڏ\A١�Aכ�A��#A�
=Aʥ�A�
=A�?}A���A�-A��
A���A���A�t�A�VA�x�A�S�A���A�VA��HA��A�\)A��jA���A�%A�dZA�ĜA��yA�`BA�1'A��HA���A�{A��HA��A�ƨA�v�A��jA��A���A�C�A���A���A�r�A��TA��!A�&�A��RA�A|n�Ax�yAudZAp�Af��A_��A[�PAQS�AJA�AB�HA6A�A)�A!x�A�A��AdZA ��@�I�@�@��#@�p�@���@�"�@��+@��!@�j@��`@�~�@�ȴ@���@�v�@�@�z�@���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B
oB
oB
oB
oB
oB
oB
hB
(�B
I�B
VB
[#B
_;B
�JB
��B
��B
�B�B0!BT�Bn�BB�BD�BM�BW
BT�BW
BYBYBVBT�BVBR�BP�BM�BR�BO�BO�BO�BN�BN�BL�BM�BG�BH�BJ�BA�B9XB7LB.B�B�BJB
��B
�yB
�
B
ɺB
�RB
��B
��B
�+B
l�B
@�B
!�B
\B	�HB	ÖB	��B	n�B	P�B	,B�sB�B��B�/B�
BȴB�LB�'B�9B�'B�B�BB	+B	{B	�B	�B	/B	H�B	T�B	aHB	e`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
oB
oB
oB
oB
oB
oB
hB
(�B
I�B
VB
[#B
_;B
�JB
��B
��B
�B�B0!BT�Bn�BB�BD�BM�BW
BT�BW
BYBYBVBT�BVBR�BP�BM�BR�BO�BO�BO�BN�BN�BL�BM�BG�BH�BJ�BA�B9XB7LB.B�B�BJB
��B
�yB
�
B
ɺB
�RB
��B
��B
�+B
l�B
@�B
!�B
\B	�HB	ÖB	��B	n�B	P�B	,B�sB�B��B�/B�
BȴB�LB�'B�9B�'B�B�BB	+B	{B	�B	�B	/B	H�B	T�B	aHB	e`1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�CB��C:�C:F%C;�C;>wCC&fC<0!C?n�C@�C@��CA-PCAv�CBؓC?�=C>=�CB�CNg�CG�CY��C[?CJHCI��CE�FCEvCDg�CB�CC�CD%`CD�CCv�CB�CC�CB��CA��C;j�C7<jC<��C=dZC<�VC8��C7��C8q�C7�yC8��C9�+C8�3C85�C8��C8|)C8xRC6�C5�\C5�C4\C1��C0��C/{dC.c�C.�}C.�XC,_�C*>�C(ɺC#�
C!�'C"�yC�Cf�C�7C�'C�mC.�COB��)B���B��JB�B�1B̬�B�T{B�B���B���B��HB��B�B�ܬB{ĜBs\)Bj�qBhV0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 CB��C:�C:F%C;�C;>wCC&fC<0!C?n�C@�C@��CA-PCAv�CBؓC?�=C>=�CB�CNg�CG�CY��C[?CJHCI��CE�FCEvCDg�CB�CC�CD%`CD�CCv�CB�CC�CB��CA��C;j�C7<jC<��C=dZC<�VC8��C7��C8q�C7�yC8��C9�+C8�3C85�C8��C8|)C8xRC6�C5�\C5�C4\C1��C0��C/{dC.c�C.�}C.�XC,_�C*>�C(ɺC#�
C!�'C"�yC�Cf�C�7C�'C�mC.�COB��)B���B��JB�B�1B̬�B�T{B�B���B���B��HB��B�B�ܬB{ĜBs\)Bj�qBhV0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        JA  ARFMdecpA10a                                                                20071014185709  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071014185712  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071014185713  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071014185716  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071014185716  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071014185717  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20071014190913                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20071018041703  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071018041709  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071018041709  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071018041713  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071018041713  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071018041714  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20071018050453                      G�O�G�O�G�O�                JA  ARFMdecpA10a                                                                20090331024611  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090331030720  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090331030721  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090331030721  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090331030722  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090331030722  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090331030722  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090331030722  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090331030722  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090331031304                      G�O�G�O�G�O�                JA  ARFMdecpA10b                                                                20090706075938  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090706083245  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090706083245  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090706083246  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090706083247  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090706083247  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090706083247  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090706083247  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090706083247  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20090706084315                      G�O�G�O�G�O�                JA  ARFMdecpA10c                                                                20091204022733  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.6                                                                 20091204023140  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20091204023141  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20091204023141  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20091204023142  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20091204023142  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8c                                                                20091204023142  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8c                                                                20091204023142  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20091204023143  QCP$                G�O�G�O�G�O�               0JA  ARUP                                                                        20091204025630                      G�O�G�O�G�O�                