CDF   #   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900614 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               ;A   JA  20071015025438  20090831075414  A9_60138_059                    2C  D   APEX-SBE 2408                                                   846 @ԜkoP�1   @ԜlD�[c@6*��n�@`�I�^51   ARGOS   A   A   A   @���A��AfffA���A�33A���B
  B��B2ffBE��BZ��Bo33B�  B���B�33B�33B�33B�ffB���Bƙ�B�33B�33B���BB�ffC33C�C  C�C33C33CL�C$� C)�C.33C3  C7�fC=ffCB� CG��CQffC[33Ce�Co�CyL�C��fC�� C��3C���C��fC���C��3C�ffC��3C�� C��3C�s3C���C³3Cǌ�C�s3C�s3C�s3Cۙ�C�3C�fC�� C�fC��3C���D�3D��D�fDٚDٚD��D�fD$�fD)��D.� D3��D8�fD=ٚDB��DG� DL�3DQ�fDV�3D[� D`��De�fDj�fDoٚDt�3Dy��D��D�\�D��fD���D��D�s3D���D���D�&fD�i�D��3D��3D��D�p Dڬ�D�� D�  D�i�D�D�I�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�ffA  Ad��A���A�ffA�  B	��B33B2  BE33BZffBn��B���B���B�  B�  B�  B�33B���B�ffB�  B�  B䙚B�ffB�33C�C  C
�fC  C�C�C33C$ffC)  C.�C2�fC7��C=L�CBffCG� CQL�C[�Ce  Co  Cy33C���C�s3C��fC�� C���C���C��fC�Y�C��fC�s3C��fC�ffC�� C¦fCǀ C�ffC�ffC�ffCی�C�fC噚C�3CC��fC�� D��D�fD� D�3D�3D�fD� D$� D)�fD.��D3�fD8� D=�3DB�fDGٚDL��DQ� DV��D[ٚD`�fDe� Dj� Do�3Dt��Dy�3D��D�Y�D��3D�ٚD�fD�p D��fD�ٚD�#3D�ffD�� D�� D��D�l�Dک�D���D��D�ffD�fD�Ff1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�v�A�+A�A�DA坲A��A�A���A�bA�ƨAا�A��`A���A���A�^5A��A�jA���A�r�A��DA�S�A�p�A�5?A��^A���A�p�A���A�E�A���A�x�A���A���A�x�A�A��A��7A���A��7A���A�M�A�C�A�hsA�JA�jA�7LA�ZA��!A�x�A��wA�r�A���A+A|�Ay�At�AooAjAc�A^ĜAW�FAS��AO�AL(�AFbAAG�A;�A8jA2�A.=qA)��A ��A�+A��A��@�@���@��y@�{@��@��F@�&�@��@�{@��-@�I�@�G�@���@���@�bN@���@���@�=q@��@{�@tI�@e�@Y7L@J��@B=q@<�@5��@0��@/+@+S�@#S�@�@�@v�@�\@��@"�@�+@�@ ��?�p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�v�A�+A�A�DA坲A��A�A���A�bA�ƨAا�A��`A���A���A�^5A��A�jA���A�r�A��DA�S�A�p�A�5?A��^A���A�p�A���A�E�A���A�x�A���A���A�x�A�A��A��7A���A��7A���A�M�A�C�A�hsA�JA�jA�7LA�ZA��!A�x�A��wA�r�A���A+A|�Ay�At�AooAjAc�A^ĜAW�FAS��AO�AL(�AFbAAG�A;�A8jA2�A.=qA)��A ��A�+A��A��@�@���@��y@�{@��@��F@�&�@��@�{@��-@�I�@�G�@���@���@�bN@���@���@�=q@��@{�@tI�@e�@Y7L@J��@B=q@<�@5��@0��@/+@+S�@#S�@�@�@v�@�\@��@"�@�+@�@ ��?�p�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
��B
��B
��B
��B
��B
��B
��B
�}B
��B
�mBhB&�BT�BffBl�B�B�B~�B�VB�7B�DB�=B�1B�B� Bt�Bp�Bk�Bs�Be`Be`BcTB_;BffBcTBbNB_;B\)BXBVBL�B@�B;dB.B�BJB
��B
�yB
�BB
��B
�}B
��B
��B
�JB
o�B
S�B
8RB
�B
B	�NB	��B	�RB	��B	�{B	}�B	ffB	XB	A�B	/B	�B��B�5B�qB��B�uB�Bu�Bv�Bz�B�=B��B��B�jB��B�/B��B	hB	!�B	7LB	D�B	I�B	T�B	]/B	iyB	y�B	��B	�FB	�
B	�yB	��B
%B
�B
%�B
8RB
<jB
D�B
K�B
R�B
YB
bNB
iyB
l�B
r�B
v�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
��B
��B
��B
��B
��B
��B
��B
��B
��B
�B�B.B]/Bm�Bp�B�B�%B�B�\B�DB�VB�DB�7B�B�Bu�Bq�Bl�Bt�BffBffBdZB_;BgmBcTBcTB`BB]/BYBW
BM�BA�B<jB/B�BPB
��B
�yB
�HB
��B
��B
�B
��B
�PB
p�B
T�B
:^B
�B
B	�TB	��B	�XB	��B	��B	� B	gmB	ZB	B�B	0!B	�B��B�;B�wB�B�{B�Bv�Bw�B{�B�DB��B��B�jB��B�5B��B	hB	!�B	7LB	D�B	I�B	T�B	]/B	iyB	y�B	��B	�FB	�
B	�yB	��B
%B
�B
%�B
8RB
<jB
D�B
K�B
R�B
YB
bNB
iyB
l�B
r�B
v�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710280205522007102802055220071028020552200710280218522007102802185220071028021852200908240000002009082400000020090824000000  JA  ARFMdecpA9_b                                                                20071015025435  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071015025438  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071015025439  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071015025443  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071015025443  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071015025443  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071015030628                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071019035341  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071019035344  IP                  G�O�G�O�G�O�                JA  ARCArsal2.1                                                                 20071019035345  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071019035345  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071019035349  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20071019035349  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071019035350  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.7c                                                                20071019035350  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071019035350  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071019050633                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071019035341  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416015700  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416015700  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416015700  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416015700  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416015702  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416015702  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416015702  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416015702  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416015702  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416020033                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071018103927  CV  DAT$            G�O�G�O�F��a                JM  ARCAJMQC1.0                                                                 20071028020552  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071028020552  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071028021852  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090824000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075259  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075414                      G�O�G�O�G�O�                