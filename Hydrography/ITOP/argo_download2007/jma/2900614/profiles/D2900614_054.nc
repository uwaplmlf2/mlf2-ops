CDF       
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900614 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               6A   JA  20070826031819  20090831075406  A9_60138_054                    2C  D   APEX-SBE 2408                                                   846 @ԏ�'q�1   @ԏ��T�6@6��/��@`�/��w1   ARGOS   A   A   A   @�ffA  Ac33A�ffA���A�33B
  B��B1��BFffBZ  Bm33B�  B���B�  B���B�ffB�33B�  B���B���B�  B���B�  B�33CffCL�C33CL�C33C��C��C$ffC)33C.� C3L�C833C=33CBffCGL�CQ�C[L�Ce  Co� Cy33C��3C��3C��3C��3C���C���C���C��3C�� C�s3C��fC��fC��fC�Cǳ3C̳3C�� C֌�C۳3C�s3C�s3C��C�fC� C�� D��D�fD��DٚD� D�3D�fD$ٚD)� D.��D3� D8ٚD=�3DB�fDG� DL��DQ��DV��D[ٚD`� De�3Dj�3Do��Dt��Dy� D�)�D�l�D���D��fD�&fD�c3D�� D��3D�&fD�i�D��fD���D�fD�` Dک�D���D��D�ffD�3D�Ff1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A	��A\��A�33A���A�  BffB33B0  BD��BXffBk��B�33B���B�33B�  B���B�ffB�33B�  B�  B�33B�  B�33B�ffC  C�fC
��C�fC��C33C33C$  C(��C.�C2�fC7��C<��CB  CF�fCP�3CZ�fCd��Co�Cx��C�� C�� C�� C�� C�Y�C�ffC�ffC�� C�L�C�@ C�s3C�s3C�s3C�Y�Cǀ C̀ Cь�C�Y�Cۀ C�@ C�@ C�Y�C�s3C�L�C���D�3D��D�3D� D�fD��D��D$� D)�fD.�3D3�fD8� D=��DB��DG�fDL�3DQ�3DV�3D[� D`�fDe��Dj��Do�3Dt�3Dy�fD��D�` D�� D�ٚD��D�VfD��3D��fD��D�\�D���D�� D�	�D�S3Dڜ�D�� D��D�Y�D�fD�9�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A陚A��A�A��A�!A�-A�FA�`BA�Aٙ�A�~�A�5?A�t�A�"�A�dZA��TA���A�{A�t�AA��A�~�A��\A�(�A���A��hA��wA�`BA��A�-A�ZA���A�+A��A���A��A�A���A�A�A��jA��#A��hA���A���A��jA��A��A�|�A�(�A���A��A���Az=qAu?}Asp�Ap��Ai�Ae��Aa�
A^ �AY�
AT��AO��AH�AGoAC�wAAK�A<�HA6A�A2ĜA)�A�hA�A��A�A�D@�t�@���@��;@ʸR@�%@��@��`@�\)@���@�ƨ@��P@�{@���@�@��@�Q�@z�@vȴ@s�@e?}@Z-@O�@E/@=�-@6�y@1x�@,�@&��@"�H@E�@�@1'@O�@&�@v�@
�H@��@z�@n�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A陚A��A�A��A�!A�-A�FA�`BA�Aٙ�A�~�A�5?A�t�A�"�A�dZA��TA���A�{A�t�AA��A�~�A��\A�(�A���A��hA��wA�`BA��A�-A�ZA���A�+A��A���A��A�A���A�A�A��jA��#A��hA���A���A��jA��A��A�|�A�(�A���A��A���Az=qAu?}Asp�Ap��Ai�Ae��Aa�
A^ �AY�
AT��AO��AH�AGoAC�wAAK�A<�HA6A�A2ĜA)�A�hA�A��A�A�D@�t�@���@��;@ʸR@�%@��@��`@�\)@���@�ƨ@��P@�{@���@�@��@�Q�@z�@vȴ@s�@e?}@Z-@O�@E/@=�-@6�y@1x�@,�@&��@"�H@E�@�@1'@O�@&�@v�@
�H@��@z�@n�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
�^B
�^B
�XB
�^B
�XB
�^B
�XB
�?B
��B
�/B!�BM�BK�B{B  B�B+BS�BI�BG�B�B`BBk�Bn�Bz�Bz�Bw�B� B�B� B{�B�B�%B��B��B��B�oB�JB�Bx�Bn�BQ�BF�B<jB33B+B�B\BB
�B
�B
�wB
�PB
t�B
jB
[#B
6FB
!�B
VB	��B	�fB	��B	�!B	�bB	�+B	x�B	k�B	VB	;dB	+B	  B��B�^B��B�bB�1By�Bp�Bp�Bt�B�B�uB��B�FBɺB�;B�B	DB	)�B	0!B	A�B	^5B	iyB	s�B	z�B	��B	�9B	ɺB	�5B	�B
B
\B
�B
'�B
1'B
<jB
D�B
K�B
T�B
^5B
cTB
jB
q�B
t�B
w�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
�^B
�^B
�XB
�^B
�XB
�^B
�XB
�XB
�'B
�;B#�BP�BM�B�BB �B-BT�BL�BK�B�%BbNBl�Bp�B{�B{�B{�B�B�+B�B}�B�B�+B��B��B��B�uB�PB�%By�Bp�BR�BG�B=qB49B,B �BbBB
�B
�B
��B
�VB
t�B
k�B
]/B
7LB
"�B
\B	��B	�mB	��B	�-B	�hB	�1B	y�B	l�B	XB	<jB	,B	B�B�dB��B�bB�7Bz�Bq�Bq�Bu�B�B�{B��B�FBɺB�;B�B	JB	)�B	0!B	A�B	^5B	iyB	s�B	z�B	��B	�9B	ɺB	�5B	�B
B
\B
�B
'�B
1'B
<jB
D�B
K�B
T�B
^5B
cTB
jB
q�B
t�B
w�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.4(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709080254122007090802541220070908025412200709080255112007090802551120070908025511200908240000002009082400000020090824000000  JA  ARFMdecpA9_b                                                                20070826031808  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070826031819  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070826031822  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070826031829  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070826031830  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070826031831  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070826043813                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070830035951  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070830035955  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070830035956  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070830040000  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070830040000  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070830040000  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070830043611                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070830035951  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416015649  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416015649  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416015649  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416015649  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416015650  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416015650  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416015650  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416015650  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416015650  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416020043                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070829113717  CV  DAT$            G�O�G�O�F�f                JM  ARCAJMQC1.0                                                                 20070908025412  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070908025412  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070908025511  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090824000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075311  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075406                      G�O�G�O�G�O�                