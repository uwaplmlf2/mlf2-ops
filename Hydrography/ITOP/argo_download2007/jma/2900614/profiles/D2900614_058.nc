CDF   !   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  2900614 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               :A   JA  20071005050332  20090831075414  A9_60138_058                    2C  D   APEX-SBE 2408                                                   846 @ԙ�ɓ�1   @ԙ��5�@6��S��@`��O�;d1   ARGOS   A   A   A   @�ffAffAa��A�ffA�  A홚B
ffB��B2ffBF  BY��Bn��B�  B���B�  B�33B���B�  B���B�  B���Bڙ�B�ffB�ffB�  CL�C� C� C33C  C�C  C$  C)33C-��C333C8ffC=  CBffCG33CQ33C[L�Ce33Co��CyL�C�� C��3C���C���C�� C���C�� C��fC�� C�� C�� C���C��3C C�� C̙�Cр C֌�C�ffC���C�� C�� C�fC���C���D��D�3D�3D� D��D� D� D$�3D)��D.��D3��D8�3D=�fDB� DG�3DL�3DQ�3DV�3D[ٚD`ٚDe��Dj� Do��Dt��DyٚD�&fD�l�D��fD���D��D�c3D��fD�� D�&fD�ffD���D�� D�fD�c3Dڣ3D���D��D�i�D�fD���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A��A`  A���A�33A���B
  BffB2  BE��BY33BnffB���B���B���B�  B�ffB���B�ffB���BЙ�B�ffB�33B�33B���C33CffCffC�C�fC  C�fC#�fC)�C-�3C3�C8L�C<�fCBL�CG�CQ�C[33Ce�Co� Cy33C��3C��fC���C�� C�s3C���C�s3C���C��3C��3C��3C���C��fC�s3Cǳ3Č�C�s3Cր C�Y�C���C�3C�3CC��C���D�fD��D��D��D�fDٚDٚD$��D)�fD.�fD3�3D8��D=� DB��DG��DL��DQ��DV��D[�3D`�3De�3DjٚDo�fDt�fDy�3D�#3D�i�D��3D��D�fD�` D��3D���D�#3D�c3D��fD���D�3D�` Dڠ D�ٚD��D�ffD�3D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�1'A�33A�1'A�5?A�5?A�=qA�9XA߲-A�l�A�I�A��#A���A�v�AËDA�|�A��TA��A�VA���A��RA��PA��9A��A�ffA�/A��^A��HA��HA��A��-A���A�~�A��mA��A�K�A��A��HA���A���A��hA�|�A��RA��;A���A�A�A��wA��HA���A��;A�VA��A}G�AzbAudZAp~�Aj�HAe�TAa��A\�RAWl�AT��AN�\AKS�AF��AC"�A?t�A;?}A6��A1K�A+hsA"�RA9XA��A�+A �\@��@߾w@Ԭ@�=q@�z�@��D@�X@�t�@�n�@��R@�dZ@�j@���@�`B@�$�@�@��^@|�D@u�@n{@_��@X�9@Ol�@F{@>5?@7�;@2�H@1�@,�/@&@ �9@�F@  @�m@;d@9X@	x�@O�@�^?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�1'A�33A�1'A�5?A�5?A�=qA�9XA߲-A�l�A�I�A��#A���A�v�AËDA�|�A��TA��A�VA���A��RA��PA��9A��A�ffA�/A��^A��HA��HA��A��-A���A�~�A��mA��A�K�A��A��HA���A���A��hA�|�A��RA��;A���A�A�A��wA��HA���A��;A�VA��A}G�AzbAudZAp~�Aj�HAe�TAa��A\�RAWl�AT��AN�\AKS�AF��AC"�A?t�A;?}A6��A1K�A+hsA"�RA9XA��A�+A �\@��@߾w@Ԭ@�=q@�z�@��D@�X@�t�@�n�@��R@�dZ@�j@���@�`B@�$�@�@��^@|�D@u�@n{@_��@X�9@Ol�@F{@>5?@7�;@2�H@1�@,�/@&@ �9@�F@  @�m@;d@9X@	x�@O�@�^?��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
]/B
]/B
]/B
]/B
]/B
\)B
ZBB#�BoB5?B@�Bw�B��B�XB��B�BS�BR�BVBbNBbNB�VBv�B��B��B��B�hB�By�Bu�Bp�Br�Bp�BgmBW
BVBM�BK�BC�B:^B2-B+B�B%B
��B
�B
�B
��B
ǮB
�!B
��B
�hB
x�B
`BB
E�B
)�B
uB	��B	�5B	��B	�LB	��B	�hB	� B	n�B	ZB	I�B	49B	�B��B�B�qB��B�bB�B� B}�B� B�=B��B��BÖB��B�5B�B��B	bB	�B	/B	B�B	W
B	gmB	w�B	�+B	��B	�RB	��B	�HB	�B
  B
uB
-B
7LB
@�B
E�B
M�B
T�B
[#B
_;B
cTB
jB
p�B
u�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
]/B
]/B
]/B
]/B
]/B
\)B
^5B1B'�B�B7LBC�Bz�B��B�jB��B�9BVBVBXBdZBdZB�bBy�B��B��B��B�{B�Bz�Bv�Bq�Bs�Bq�BhsBW
BW
BN�BL�BD�B;dB2-B,B�B+B
��B
�B
�B
��B
ȴB
�'B
��B
�oB
y�B
aHB
F�B
+B
{B	��B	�;B	��B	�RB	��B	�oB	�B	o�B	[#B	J�B	6FB	�B��B�#B�wB��B�hB�%B�B~�B�B�DB��B��BÖB��B�5B�B��B	bB	�B	/B	B�B	W
B	gmB	w�B	�+B	��B	�RB	��B	�HB	�B
  B
uB
-B
7LB
@�B
E�B
M�B
T�B
[#B
_;B
cTB
jB
p�B
u�B
x�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.1(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710180139302007101801393020071018013930200710180144382007101801443820071018014438200908240000002009082400000020090824000000  JA  ARFMdecpA9_b                                                                20071005050327  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071005050332  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071005050333  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071005050337  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071005050337  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071005050338  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071005050631                      G�O�G�O�G�O�                JA  ARUP                                                                        20071005052307                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071009035958  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071009040003  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071009040003  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071009040007  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071009040007  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071009040008  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071009050452                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071009035958  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090416015658  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090416015658  IP  PRES            G�O�G�O�G�O�                JA  ARCArsal2.1a                                                                20090416015658  IP  PSAL            G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20090416015658  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090416015659  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090416015659  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090416015659  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090416015659  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090416015659  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090416020033                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071008103355  CV  DAT$            G�O�G�O�F�Ϛ                JM  ARCAJMQC1.0                                                                 20071018013930  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071018013930  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071018014438  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090824000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090831075302  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090831075414                      G�O�G�O�G�O�                