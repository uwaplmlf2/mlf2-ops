CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901522 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070807025800  20090908020812  A9_66094_017                    2C  D   APEX-SBE 2803                                                   846 @ԋ;�Ӡv1   @ԋ>�Vx�@2P��
=q@a����m1   ARGOS   A   A   A   @���A��Ac33A���A�  A�ffB
  BffB2ffBF  BY33Bn  B���B�ffB�33B���B���B���B�33B�  B�ffB�ffB�  B�33B�33CffCffCL�C�CL�C  C�C$�C)�C.33C3� C8� C=L�CBffCGffCQ��C[� Ce33Co33Cy33C��3C���C���C�s3C���C���C��3C���C���C���C�� C���C���C�C�ffC�� Cѳ3Cֳ3Cی�C�fC�� C�3C�� C��3C�� D��DٚD�3D��D�fD�3DٚD$��D)�3D.��D3�3D8�fD=ٚDBٚDG��DL� DQ�3DV�3D[ٚD`��De� Dj�fDo�3Dt�fDy��D��D�ffD�� D�� D�33D�i�D���D�ٚD�#3D�l�D�� D��3D�fD�p Dڣ3D��fD�#3D�P D��D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���A��Ac33A���A�  A�ffB
  BffB2ffBF  BY33Bn  B���B�ffB�33B���B���B���B�33B�  B�ffB�ffB�  B�33B�33CffCffCL�C�CL�C  C�C$�C)�C.33C3� C8� C=L�CBffCGffCQ��C[� Ce33Co33Cy33C��3C���C���C�s3C���C���C��3C���C���C���C�� C���C���C�C�ffC�� Cѳ3Cֳ3Cی�C�fC�� C�3C�� C��3C�� D��DٚD�3D��D�fD�3DٚD$��D)�3D.��D3�3D8�fD=ٚDBٚDG��DL� DQ�3DV�3D[ٚD`��De� Dj�fDo�3Dt�fDy��D��D�ffD�� D�� D�33D�i�D���D�ٚD�#3D�l�D�� D��3D�fD�p Dڣ3D��fD�#3D�P D��D��f1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��A��A��A��A�A�A�A�A��A�=qA敁A�`BA�&�A��A�ZA�1'AӬA�VA�1A��A˛�A��`Aș�AƩ�A�ȴA�5?A�(�A���A��A��HA�n�A�ƨA��HA���A���A�E�A�%A�ĜA��A� �A�M�A�~�A��!A���A���A�|�A�ĜA�ĜA�O�A��PA�&�A|~�Ay"�At^5AqAmK�Af��A_+AP�HAJM�AC��A=oA7"�A.��A*�A&jA�A�AO�AA	��A�#@���@��^@� �@�G�@��@�33@�p�@��+@��#@���@���@��@���@��m@��@�?}@��
@�O�@�^5@�Q�@��/@|��@w�P@j�H@b�\@Y��@Ol�@G|�@?�P@7|�@3�F@/K�@+33@%O�@�w@�^@O�@�@$�@
^5@�P@?}@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A��A��A��A��A�A�A�A�A��A�=qA敁A�`BA�&�A��A�ZA�1'AӬA�VA�1A��A˛�A��`Aș�AƩ�A�ȴA�5?A�(�A���A��A��HA�n�A�ƨA��HA���A���A�E�A�%A�ĜA��A� �A�M�A�~�A��!A���A���A�|�A�ĜA�ĜA�O�A��PA�&�A|~�Ay"�At^5AqAmK�Af��A_+AP�HAJM�AC��A=oA7"�A.��A*�A&jA�A�AO�AA	��A�#@���@��^@� �@�G�@��@�33@�p�@��+@��#@���@���@��@���@��m@��@�?}@��
@�O�@�^5@�Q�@��/@|��@w�P@j�H@b�\@Y��@Ol�@G|�@?�P@7|�@3�F@/K�@+33@%O�@�w@�^@O�@�@$�@
^5@�P@?}@�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
%�B
&�B
&�B
&�B
&�B
&�B
&�B
+B
o�B
�B
�B
��B
�
B
��BB�Bm�Bk�B�B� B��B��B��B��B�B�yBB�yB�B�ZB�HB�)B�B�HB�#B��BȴB��B�'B��B|�B\)BR�BI�B2-B �B�B%B
�B
��B
�FB
�{B
�B
jB
\)B
B�B
&�B	��B	�^B	��B	v�B	[#B	C�B	�B	VB	B�B�)B��BÖB�?B�'B�?B�B�B�?BƨB�BB�B	VB	�B	/B	=qB	R�B	l�B	}�B	�=B	��B	��B	�-B	�wB	ɺB	��B	�B	�NB	��B
  B
�B
�B
(�B
0!B
9XB
A�B
H�B
M�B
W
B
]/B
_;B
cTB
iyB
m�B
r�B
u�B
y�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
%�B
&�B
&�B
&�B
&�B
&�B
&�B
+B
q�B
�B
�B
��B
�B
��B1B�Bn�Bm�B�B�B��B��BB��B�B�B%B�B�B�mB�NB�;B�B�TB�5B��BɺBÖB�9B��B� B\)BS�BJ�B33B �B�B+B
�B
��B
�LB
��B
�B
k�B
]/B
D�B
(�B	��B	�jB	��B	x�B	]/B	E�B	�B	\B	%B�B�5B��BĜB�FB�-B�FB�B�B�FBǮB�BB��B	VB	�B	0!B	=qB	R�B	l�B	}�B	�=B	��B	��B	�-B	�wB	ɺB	��B	�B	�NB	��B
  B
�B
�B
(�B
0!B
9XB
A�B
H�B
M�B
W
B
]/B
_;B
cTB
iyB
m�B
r�B
u�B
y�B
y�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200708200830452007082008304520070820083045200708200841372007082008413720070820084137200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070807025758  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070807025800  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070807025801  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070807025805  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070807025805  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070807025806  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070807030447                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070811040628  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070811040641  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070811040643  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070811040651  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070811040651  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070811040653  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070811042217                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070811040628  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519071536  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519071536  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519071537  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519071538  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519071538  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519071538  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519071538  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519071538  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071910                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070810174616  CV  DAT$            G�O�G�O�F�Y�                JM  ARCAJMQC1.0                                                                 20070820083045  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070820083045  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070820084137  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020737  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908020812                      G�O�G�O�G�O�                