CDF   
   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901522 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070828085627  20090908020801  A9_66094_019                    2C  D   APEX-SBE 2803                                                   846 @Ԑ4�i1   @Ԑ6*z@2��z�H@a� ě��1   ARGOS   A   A   A   @�  A33Ac33A�  A�  A�33BffB��B1��BF  BZ  Bn��B�  B���B���B�33B�  B�33B�  B�  B�  B���B�  BB�  C��C��C33C33C�CL�C� C$ffC)ffC.ffC3ffC8ffC=�3CB� CGffCQffC[ffCe�Co� Cy� C���C��3C��3C��fC��fC���C���C���C�� C�� C���C��3C��fC�CǦfČ�Cѳ3Cր C�Y�C�� C�Y�C�ffC��C�L�C�ffD�3D�fDٚDٚD�fD�3D�fD$�fD)�3D.� D3�fD8�fD=� DB� DG�3DL�3DQ��DV��D[�3D`� DeٚDj�fDo��DtٚDy�3D�#3D�i�D�� D��fD�0 D�c3D��3D�� D��D�p D���D��D�fD�l�Dک�D��3D�)�D�P D� D�0 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A33Ac33A�  A�  A�33BffB��B1��BF  BZ  Bn��B�  B���B���B�33B�  B�33B�  B�  B�  B���B�  BB�  C��C��C33C33C�CL�C� C$ffC)ffC.ffC3ffC8ffC=�3CB� CGffCQffC[ffCe�Co� Cy� C���C��3C��3C��fC��fC���C���C���C�� C�� C���C��3C��fC�CǦfČ�Cѳ3Cր C�Y�C�� C�Y�C�ffC��C�L�C�ffD�3D�fDٚDٚD�fD�3D�fD$�fD)�3D.� D3�fD8�fD=� DB� DG�3DL�3DQ��DV��D[�3D`� DeٚDj�fDo��DtٚDy�3D�#3D�i�D�� D��fD�0 D�c3D��3D�� D��D�p D���D��D�fD�l�Dک�D��3D�)�D�P D� D�0 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�hA�hA�uA�uA앁A�?}A�JA�v�A�bNA�A�A��A�E�A���A���A�I�A��A݃A��A�O�AծAҟ�A�A�A��yAμjA�A�bA�ȴA�ĜAĶFAPA�~�A�ĜA��DA���A��hA�A�n�A�ƨA�XA���A�%A���A��A��A�&�A�Q�A��
A��uA�t�A�v�A���A�`BA}��Aw��Ar$�Al��Ad�A]��AX1'ASK�AG��A@��A;��A3�A0�yA,�A(�/A$��A r�A1'AȴAX@�t�@�j@ߝ�@և+@�(�@�=q@�@�V@�
=@��j@�b@�K�@�G�@�A�@��T@��#@�
=@���@�M�@�\)@��;@�&�@z�@i�#@d��@[dZ@R��@J^5@E/@?l�@9��@2�\@,j@'��@#t�@;d@J@�@33@�@�/@	7L@v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�hA�hA�uA�uA앁A�?}A�JA�v�A�bNA�A�A��A�E�A���A���A�I�A��A݃A��A�O�AծAҟ�A�A�A��yAμjA�A�bA�ȴA�ĜAĶFAPA�~�A�ĜA��DA���A��hA�A�n�A�ƨA�XA���A�%A���A��A��A�&�A�Q�A��
A��uA�t�A�v�A���A�`BA}��Aw��Ar$�Al��Ad�A]��AX1'ASK�AG��A@��A;��A3�A0�yA,�A(�/A$��A r�A1'AȴAX@�t�@�j@ߝ�@և+@�(�@�=q@�@�V@�
=@��j@�b@�K�@�G�@�A�@��T@��#@�
=@���@�M�@�\)@��;@�&�@z�@i�#@d��@[dZ@R��@J^5@E/@?l�@9��@2�\@,j@'��@#t�@;d@J@�@33@�@�/@	7L@v�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	l�B	l�B	l�B	l�B	l�B	p�B	v�B	�B	�B	�hB	�B
F�B
|�B
��B
��B
�RB
��B
�BBuB$�B2-B<jBC�BVBcTBq�Br�Bw�B�B�DB��B��B�5B�/B�
B�B�#B�)B�B��BĜB�Br�BP�BD�B1'B�BJB
�B
�)B
�XB
��B
|�B
dZB
A�B
�B	��B	�/B	�}B	�1B	e`B	O�B	1'B	'�B	�B	VB	+B�B�#BB�RB��B��B�?B��B��B�B�B		7B	\B	�B	7LB	J�B	dZB	v�B	�B	�oB	��B	�B	�RB	ÖB	��B	�B	�)B	��B
�B
�B
/B
9XB
=qB
D�B
H�B
N�B
T�B
ZB
]/B
aHB
ffB
iyB
l�B
o�B
s�B
v�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	l�B	l�B	l�B	l�B	l�B	p�B	v�B	�B	�B	�hB	�!B
H�B
~�B
��B
��B
�^B
�B
�B%B�B%�B33B=qBE�BXBe`Br�Bt�By�B�B�PB��B��B�BB�BB�B�#B�/B�/B�#B�BǮB�Bt�BQ�BE�B33B�BPB
�B
�5B
�^B
��B
}�B
e`B
C�B
�B	��B	�5B	B	�=B	ffB	Q�B	2-B	(�B	�B	\B	1B��B�/BĜB�^B��B��B�FB��B��B�B�B		7B	bB	�B	7LB	J�B	dZB	v�B	�B	�oB	��B	�B	�RB	ÖB	��B	�B	�)B	��B
�B
�B
/B
9XB
=qB
D�B
H�B
N�B
T�B
ZB
]/B
aHB
ffB
iyB
l�B
o�B
s�B
v�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200907111300302009071113003020090711130030200907111530472009071115304720090711153047200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070828085624  IP                  G�O�G�O�G�O�                JM  ARFMDECPA9                                                                  20090711130021  IP                  G�O�G�O�G�O�                JM  ARFMBITP1                                                                   20070828085624  SV                  D�c3D���G�O�                JM  ARGQRQCP1                                                                   20090711130021  QCP$                G�O�G�O�G�O�            FB7CJM  ARCAJMQC1.0                                                                 20090711130030  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20090711130030  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20090711153047  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020721  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908020801                      G�O�G�O�G�O�                