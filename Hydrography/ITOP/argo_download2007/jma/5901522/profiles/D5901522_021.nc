CDF   "   
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901522 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070916061242  20090908020802  A9_66094_021                    2C  D   APEX-SBE 2803                                                   846 @ԕ5���1   @ԕ7�#B@2���E�@a�bM��1   ARGOS   A   A   A   @���AffA`  A���A�ffA�  BffBffB133BD��BZffBn  B�ffB���B�  B�  B�  B�  B�  B�33B���B�  B�ffB�  B���C33CL�CL�C� CL�CffC��C$ffC)L�C.ffC3ffC8� C=� CB��CGffCQffC[ffCeffCo�CyffC���C��3C���C���C���C�� C��fC���C���C��fC��fC���C���C�� CǦfC̳3CѦfC֌�CۦfC�s3C���C�3C�fC�� C���D� D��DٚD�fD� D�3D�fD$� D)�3D.��D3��D8��D=�fDB�3DG�fDL�fDQ�3DV� D[ٚD`��DeٚDj� Do�3Dt�fDy��D�,�D�i�D��fD��D�)�D�\�D���D��D��D�l�D���D��D�,�D�VfDڣ3D�� D�)�D�i�D�3D�� 1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�33A33A\��A�  A���A�ffB��B��B0ffBD  BY��Bm33B�  B�33B���B���B���B���B���B���B�ffBڙ�B�  BB�33C  C�C�CL�C�C33CffC$33C)�C.33C333C8L�C=L�CBffCG33CQ33C[33Ce33Cn�fCy33C��3C���C�� C��3C�s3C�ffC���C�s3C�s3C���C���C�� C�� C¦fCǌ�C̙�Cь�C�s3Cی�C�Y�C�3CꙚC��C��fC�� D�3D� D��D��D�3D�fD��D$�3D)�fD.� D3��D8� D=��DB�fDG��DL��DQ�fDV�3D[��D`��De��Dj�3Do�fDt��Dy��D�&fD�c3D�� D��3D�#3D�VfD��fD��3D�3D�ffD��fD��3D�&fD�P Dڜ�D��D�#3D�c3D��D���1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�z�A�v�A�x�A�~�A�PA�PA�\A�hA�uA앁A쟾A�A�ƨA�ƨA�G�A畁A���A��A�E�A��A��A׍PAՓuA�ȴA�{A�  A���A�v�A��;A�x�A�5?A��A��A��FA�{A��jA���A�r�A��A�O�A��mA��+A�^5A���A�n�A�^5A�~�A��RA�&�A���A�ffA|JAt�Ao�hAe�A[G�AU�AR�uAF~�A@ĜA<�yA4��A05?A'�A ȴA+Av�AVAQ�A��A�T@��T@��@��m@��@��@�$�@���@�ff@�hs@�  @���@�j@�K�@��^@��`@��j@��h@�?}@��!@��w@�Ĝ@�%@��`@}O�@t1@l1@[�F@U��@J�@EO�@>ȴ@7�w@2n�@-p�@(��@#��@5?@@��@�/@V@
J@�@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�z�A�v�A�x�A�~�A�PA�PA�\A�hA�uA앁A쟾A�A�ƨA�ƨA�G�A畁A���A��A�E�A��A��A׍PAՓuA�ȴA�{A�  A���A�v�A��;A�x�A�5?A��A��A��FA�{A��jA���A�r�A��A�O�A��mA��+A�^5A���A�n�A�^5A�~�A��RA�&�A���A�ffA|JAt�Ao�hAe�A[G�AU�AR�uAF~�A@ĜA<�yA4��A05?A'�A ȴA+Av�AVAQ�A��A�T@��T@��@��m@��@��@�$�@���@�ff@�hs@�  @���@�j@�K�@��^@��`@��j@��h@�?}@��!@��w@�Ĝ@�%@��`@}O�@t1@l1@[�F@U��@J�@EO�@>ȴ@7�w@2n�@-p�@(��@#��@5?@@��@�/@V@
J@�@��1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	��B	��B	��B	��B	��B	��B	��B	��B	��B	��B	��B	�B	�B
�LB
�5B
�B
�B
�B
��B�B(�B/BI�Bn�BVBVB�B�B�%B��B��B��B;dB>wB5?B6FB�B\B��B�B�BBȴB�9B�VBw�B]/B>wB�B
�B
��B
�qB
�VB
e`B
D�B
  B	�B	ĜB	�RB	�VB	~�B	t�B	P�B	@�B	+B	�B	�B	PB	B�B��B�9B��B��B�NB�BB��B�B�;B�/B�B��B	bB	(�B	:^B	N�B	]/B	x�B	�1B	��B	��B	�-B	�}B	ȴB	��B	�
B	��B
hB
oB
%�B
33B
?}B
D�B
K�B
O�B
S�B
YB
]/B
cTB
e`B
hsB
jB
n�B
r�B
v�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B	��B	��B	��B	��B	��B	��B	��B	��B	��B	��B	��B	�B	�B
�RB
�;B
�B
�)B
�B
��B!�B)�B1'BK�Br�BXBYB�B�B�7B��B��B��B=qBA�B8RB:^B�BoB��B�B�NBɺB�LB�\Bx�B_;B@�B�B
�B
�B
�}B
�bB
ffB
F�B
B	�#B	ŢB	�dB	�bB	� B	v�B	Q�B	B�B	-B	�B	�B	\B	B��B��B�?B��B��B�TB�HB�B�B�;B�5B�B��B	bB	(�B	:^B	N�B	]/B	x�B	�1B	��B	��B	�-B	�}B	ȴB	��B	�
B	��B
hB
oB
%�B
33B
?}B
D�B
K�B
O�B
S�B
YB
]/B
cTB
e`B
hsB
jB
n�B
r�B
v�B
z�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200709300754292007093007542920070930075429200709300757142007093007571420070930075714200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070916061222  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070916061242  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070916061246  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070916061254  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070916061255  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070916061256  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070916062355                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070920041831  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070920041834  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070920041835  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070920041839  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070920041840  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070920041840  QCP$                G�O�G�O�G�O�           10000JA  ARGQrelo2.1                                                                 20070920041840  CV  TIME            G�O�G�O�                    JA  ARGQrelo2.1                                                                 20070920041840  CV  LAT$            G�O�G�O�A���                JA  ARGQrelo2.1                                                                 20070920041840  CV  LON$            G�O�G�O�C�T                JA  ARUP                                                                        20070920043650                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070920041831  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519071546  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519071546  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519071547  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519071548  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519071548  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519071548  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519071548  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519071548  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071918                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070920012623  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20070930075429  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20070930075429  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20070930075714  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020721  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908020802                      G�O�G�O�G�O�                