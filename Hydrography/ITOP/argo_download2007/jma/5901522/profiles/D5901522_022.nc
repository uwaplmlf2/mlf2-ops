CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901522 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20070926085738  20090908020803  A9_66094_022                    2C  D   APEX-SBE 2803                                                   846 @ԗ����1   @ԗ�+l��@2g-@a�=p��
1   ARGOS   A   A   A   @�ffAffAk33A���A���A�ffB
  B  B2  BFffBZffBm��B���B���B�ffB�33B�33B���B�33Bƙ�B�33B�ffB䙚B�ffB���C33C33C� CffC��C33CL�C$ffC)�C.�C333C8L�C=33CB  CGffCQffC[��Ce�Co�Cy�C�� C�� C���C�� C�� C���C�� C��3C���C���C�� C���C���C³3Cǳ3C̀ Cљ�C�s3Cۙ�C�� C�fC�3C��C���C�� D��D�3D� D�3D�3D� D��D$�3D)�3D.�3D3� D8� D=� DB��DG��DL��DQ�3DV�fD[�3D`�fDeٚDj�3DoٚDt�fDy�fD�  D�p D���D���D��D�i�D���D�� D�  D�c3D���D��fD�fD�ffDڣ3D��fD��D�` D�fD�)�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @�  A33Ah  A�33A�33A���B	33B33B133BE��BY��Bl��B�33B�33B�  B���B���B�ffB���B�33B���B�  B�33B�  B�33C  C  CL�C33CffC  C�C$33C(�fC-�fC3  C8�C=  CA��CG33CQ33C[ffCd�fCn�fCx�fC��fC�ffC�s3C��fC�ffC��3C��fC���C�s3C�� C�ffC�s3C�s3C�CǙ�C�ffCр C�Y�Cۀ C�fC��CꙚC�s3C� C�ffD��D�fD�3D�fD�fD�3D� D$�fD)�fD.�fD3�3D8�3D=�3DB� DG� DL� DQ�fDV��D[�fD`��De��Dj�fDo��Dt��Dy��D��D�i�D��fD��fD�3D�c3D��3D�ٚD��D�\�D��fD�� D� D�` Dڜ�D�� D�fD�Y�D� D�#31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�bA�VA�hsA�PA�A��yA�hsA�l�A���A�O�A���A�9XAǙ�A��HA�;dA�A�A�x�A���A�hsA�9XA���A�ZA��A���A�
=A�~�A�E�A���A��PA���A�O�A��A�p�A�&�A��HA��^A�+A���A�O�A��\A�&�A�ƨA�%A��hA��A���A��A};dAu�AsdZAm��Ag��Ab�/Ab9XA`��AYAX�AT�DAR�AG%ABr�A:�9A4n�A-l�A%A!A��AA�AXA�A�hA��@���@�@�J@���@�@��\@�z�@��H@��h@�Q�@��h@�r�@���@�Q�@��^@��!@��@�&�@��@�33@��D@y��@s��@i�@dI�@\�/@R�\@M�T@K"�@@r�@=�@5�-@0Ĝ@'l�@#�F@�j@�y@9X@G�@�@	�7@@o1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�bA�VA�hsA�PA�A��yA�hsA�l�A���A�O�A���A�9XAǙ�A��HA�;dA�A�A�x�A���A�hsA�9XA���A�ZA��A���A�
=A�~�A�E�A���A��PA���A�O�A��A�p�A�&�A��HA��^A�+A���A�O�A��\A�&�A�ƨA�%A��hA��A���A��A};dAu�AsdZAm��Ag��Ab�/Ab9XA`��AYAX�AT�DAR�AG%ABr�A:�9A4n�A-l�A%A!A��AA�AXA�A�hA��@���@�@�J@���@�@��\@�z�@��H@��h@�Q�@��h@�r�@���@�Q�@��^@��!@��@�&�@��@�33@��D@y��@s��@i�@dI�@\�/@R�\@M�T@K"�@@r�@=�@5�-@0Ĝ@'l�@#�F@�j@�y@9X@G�@�@	�7@@o1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
=qB
��BZBm�B�{B��B+B�B,B#�B2-BdZBdZBaHBbNB`BB]/BT�BO�BN�B>wB/B\BJB�B	7B\BDB��B�B�BBȴB�XB�B��B�+Bq�BiyBe`B^5BL�B@�B,BPB
��B
�sB
��B
��B
n�B
`BB
A�B
%�B

=B
B	��B	�/B	�B	ŢB	�XB	�%B	r�B	\)B	G�B	>wB	,B	�B	{B	VB	VB	DB��B�B�B�#BɺBÖBĜB��B�B�B��B	 �B	7LB	P�B	k�B	z�B	�uB	��B	�B	��B	ĜB	��B	�
B	�/B	�yB
B
�B
&�B
1'B
5?B
9XB
C�B
G�B
M�B
Q�B
YB
\)B
aHB
ffB
iyB
l�B
p�B
u�B
x�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
>wB
��BZBn�B��B��B	7B�B,B&�B5?BiyBe`BcTBcTBaHB_;BVBP�BP�B@�B2-BbBPB�B
=BhBJBB��B�ZB��B�jB�'B��B�7Br�BjBffB_;BM�BA�B.BVB
��B
�yB
��B
��B
o�B
aHB
C�B
&�B

=B
B	��B	�/B	�B	ƨB	�jB	�+B	t�B	^5B	I�B	@�B	-B	�B	�B	\B	VB	JB��B�B�B�)B��BĜBŢB�B�B�B��B	 �B	7LB	P�B	k�B	{�B	�uB	��B	�B	��B	ĜB	��B	�
B	�/B	�yB
B
�B
&�B
1'B
5?B
9XB
C�B
G�B
M�B
Q�B
YB
\)B
aHB
ffB
iyB
l�B
p�B
u�B
x�B
|�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.2(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710090743022007100907430220071009074302200710090756002007100907560020071009075600200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20070926085736  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070926085738  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070926085739  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070926085744  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070926085744  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16a                                                                20070926085744  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070926090416                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070930050012  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20070930050030  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20070930050033  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20070930050042  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20070930050042  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20070930050043  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20070930054611                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20070930050012  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519071548  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519071549  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519071549  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519071550  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519071550  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519071550  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519071550  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519071550  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071920                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20070930165427  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20071009074302  CV  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071009074302  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071009075600  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020724  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908020803                      G�O�G�O�G�O�                