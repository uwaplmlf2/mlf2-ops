CDF      
      	DATE_TIME         	STRING256         STRING64   @   STRING32       STRING16      STRING8       STRING4       STRING2       N_PARAM       N_PROF        N_CALIB       N_LEVELS   s   	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER       	            	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME      	            comment       Name of the project    
_FillValue                  @  1   PI_NAME       	            comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS        	               	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER      	         	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION         	         	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE       	            	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE      	            	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR      	            	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE         	         	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE        	            	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE         	            	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD      	         	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC       	         	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION         	         	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE      	         	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE         	         	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC       	         	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM        	            	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC       	         	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC       	         	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC       	         	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES      	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  2�   PRES_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  4h   PRES_ADJUSTED         	         	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  4�   PRES_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  6�   PRES_ADJUSTED_ERROR       	            	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���     �  7   TEMP      	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  8�   TEMP_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  :�   TEMP_ADJUSTED         	         	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ;(   TEMP_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  <�   TEMP_ADJUSTED_ERROR       	            	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  =h   PSAL      	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  ?4   PSAL_QC       	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  A    PSAL_ADJUSTED         	         	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  At   PSAL_ADJUSTED_QC      	            	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  t  C@   PSAL_ADJUSTED_ERROR       	            	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o     �  C�   	PARAMETER         	   
               	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  �  E�   SCIENTIFIC_CALIB_EQUATION         	   
               	long_name         'Calibration equation for this parameter    
_FillValue                 	   F   SCIENTIFIC_CALIB_COEFFICIENT      	   
               	long_name         *Calibration coefficients for this equation     
_FillValue                 	   O   SCIENTIFIC_CALIB_COMMENT      	   
               	long_name         .Comment applying to this parameter calibration     
_FillValue                 	   X   CALIBRATION_DATE      	   
                
_FillValue                  �  a   HISTORY_INSTITUTION          	            	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    a�   HISTORY_STEP         	            	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    a�   HISTORY_SOFTWARE         	            	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    a�   HISTORY_SOFTWARE_RELEASE         	            	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    a�   HISTORY_REFERENCE            	            	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  a�   HISTORY_DATE         	             	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    a�   HISTORY_ACTION           	            	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    a�   HISTORY_PARAMETER            	            	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    a�   HISTORY_START_PRES           	         	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         b   HISTORY_STOP_PRES            	         	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         b   HISTORY_PREVIOUS_VALUE           	         	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        b   HISTORY_QCTEST           	            	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    bArgo profile    2.2 1.2 19500101000000  5901522 J-ARGO                                                          JAMSTEC                                                         PRES            TEMP            PSAL               A   JA  20071016045930  20090908020759  A9_66094_024                    2C  D   APEX-SBE 2803                                                   846 @Ԝ��� �1   @Ԝ��+I@1y�+@b ���F1   ARGOS   A   A   A   @���AffAfffA�33A�ffA�33B	��B  B1��BFffBZffBn  B���B�33B���B�  B���B�  B���B�  B�ffB�33B�ffB�  B���CffC  C33CffC� CffC��C$� C)L�C.33C3ffC833C=� CB33CGL�CQ  CZ�fCeL�Co33Cy�C���C�� C��fC���C�� C�ffC��fC��fC��3C�� C���C��fC�� C¦fCǌ�C̦fCљ�C֦fCۙ�C�� C�fCꙚC�3C� C�� D��D�fDٚD� D�3D� D�3D$��D)ٚD.��D3��D8��D=�3DB�fDG�3DL��DQٚDV��D[�3D`�3De��DjٚDo� Dt��Dy� D�)�D�l�D���D���D�&fD�ffD��fD��D�  D�c3D��fD��D�)�D�i�Dڬ�D�� D�fD�` D�3D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @���AffAfffA�33A�ffA�33B	��B  B1��BFffBZffBn  B���B�33B���B�  B���B�  B���B�  B�ffB�33B�ffB�  B���CffC  C33CffC� CffC��C$� C)L�C.33C3ffC833C=� CB33CGL�CQ  CZ�fCeL�Co33Cy�C���C�� C��fC���C�� C�ffC��fC��fC��3C�� C���C��fC�� C¦fCǌ�C̦fCљ�C֦fCۙ�C�� C�fCꙚC�3C� C�� D��D�fDٚD� D�3D� D�3D$��D)ٚD.��D3��D8��D=�3DB�fDG�3DL��DQٚDV��D[�3D`�3De��DjٚDo� Dt��Dy� D�)�D�l�D���D���D�&fD�ffD��fD��D�  D�c3D��fD��D�)�D�i�Dڬ�D�� D�fD�` D�3D��31111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A�PA�7A�uA�A���A��/A�JA��A��yA�A�`BA�33A���A�O�A؃A���A���A�{A�%A�~�A�%A�S�A��A��A���A��A�A��A�p�A�=qA�ȴA��/A��
A�Q�A��A�-A�x�A��PA�VA�/A�1A��#A��A�1A���A��A�A�^5A���A~ĜA|�AxZAp �Ag\)A\E�AU�
AS7LAK�PAD5?A=�A6��A3�A+��A)�A n�Az�A"�A�A�AO�AoAo@�@އ+@��H@���@�"�@ǥ�@�r�@��T@�Q�@��m@��-@��/@�X@�
=@���@�n�@���@�;d@�Q�@{t�@v�@q��@m��@e`B@^�+@SC�@O|�@L�/@F�@?|�@9�@/�@$z�@��@�7@I�@��@ȴ@��@��@�/@-@ �1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 A�PA�7A�uA�A���A��/A�JA��A��yA�A�`BA�33A���A�O�A؃A���A���A�{A�%A�~�A�%A�S�A��A��A���A��A�A��A�p�A�=qA�ȴA��/A��
A�Q�A��A�-A�x�A��PA�VA�/A�1A��#A��A�1A���A��A�A�^5A���A~ĜA|�AxZAp �Ag\)A\E�AU�
AS7LAK�PAD5?A=�A6��A3�A+��A)�A n�Az�A"�A�A�AO�AoAo@�@އ+@��H@���@�"�@ǥ�@�r�@��T@�Q�@��m@��-@��/@�X@�
=@���@�n�@���@�;d@�Q�@{t�@v�@q��@m��@e`B@^�+@SC�@O|�@L�/@F�@?|�@9�@/�@$z�@��@�7@I�@��@ȴ@��@��@�/@-@ �1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB
(�B
)�B
.B
7LB
F�B
P�B
�-B
ɺB
��B
�Bv�B��B��B��B��B�dB$�B<jB49B2-B&�BG�BG�BE�B:^B:^B5?B-B)�B&�B1B��B�mB�/B��BƨBB�dB�dB��B�BcTBT�B<jB"�BB
�`B
ÖB
�!B
��B
��B
{�B
Q�B
%�B	�yB	ǮB	�3B	�hB	l�B	m�B	=qB	6FB	0!B	,B	bB	JB	�B	PB	DB	
=B	JB	B�B��B	�B	�B	J�B	XB	bNB	r�B	�B	�B	y�B	�B	�?B	�LB	�FB	�-B	��B	��B	��B	�)B	�TB	�sB	�B	��B
hB
uB
�B
0!B
2-B
A�B
I�B
P�B
N�B
S�B
VB
_;B
ffB
hsB
k�B
s�B
y�B
|�B
~�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 B
(�B
)�B
.B
7LB
F�B
P�B
�-B
ɺB
��B
�Bx�B��B��B��B��B��B(�B=qB6FB49B'�BK�BI�BG�B<jB;dB7LB-B+B)�BDB��B�yB�BB�BǮBÖB�wB�wB��B�BdZBVB=qB#�B%B
�mB
ĜB
�'B
��B
��B
}�B
S�B
(�B	�B	ȴB	�?B	�uB	n�B	o�B	>wB	8RB	1'B	.B	oB	JB	�B	\B	JB	DB	PB	B�B��B	�B	�B	K�B	XB	cTB	r�B	�B	�B	y�B	�B	�?B	�RB	�FB	�-B	��B	��B	��B	�)B	�TB	�sB	�B	��B
hB
uB
�B
0!B
2-B
A�B
I�B
P�B
N�B
S�B
VB
_;B
ffB
hsB
k�B
s�B
y�B
|�B
~�1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111 <#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
<#�
PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES            TEMP            PSAL            PRES_ADJ = PRES-SP(NextCycle), where SP is SURFACE PRESSURE from next cycle                                                                                                                                                                                     TEMP_ADJUSTED = TEMP                                                                                                                                                                                                                                            PSAL_ADJ = RecalS= psal(PRES_ADJ,TEMP,Conductivity)                                                                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            PSAL_ADJ = celltm_sbe41(RecalS,TEMP,PRES_ADJ,elapsed_time,alpha,tau),elapsed_time=P/mean_rise_rate,P=dbar since the start of the profile for each samples                                                                                                       None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            SP(NextCycle)=0.0(dbar)                                                                                                                                                                                                                                         None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            alpha=0.0267C, tau=18.6s, mean_rise_rate = 0.09 dbar/second                                                                                                                                                                                                     None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Pressure Correction using reported SURFACE PRESSURE; PRES_ADJ_ERR : Manufacture sensor accuracy                                                                                                                                                                 TEMP_ADJ_ERR : SBE sensor accuracy                                                                                                                                                                                                                              Salinity Recalculation using PRES_ADJ; PSAL_ADJ_ERR : max(sum of RecalS & CTM errors, 0.01(PSS-78))                                                                                                                                                             None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            Correction for conductivity cell thermal mass error(CTM), Johnson et al., 2007, JAOT                                                                                                                                                                            None                                                                                                                                                                                                                                                            None                                                                                                                                                                                                                                                            No adjustment is needed                                                                                                                                                                                                                                         200710290816032007102908160320071029081603200710290824192007102908241920071029082419200909020000002009090200000020090902000000  JA  ARFMdecpA9_b                                                                20071016045927  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071016045930  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071016045931  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071016045935  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071016045935  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071016045935  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071016050832                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071020040811  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.4                                                                 20071020040815  IP                  G�O�G�O�G�O�                JA  ARGQrqcppo_a                                                                20071020040816  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20071020040819  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.7c                                                                20071020040820  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20071020040820  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20071020050815                      G�O�G�O�G�O�                JA  ARFMdecpA9_b                                                                20071020040811  IP                  G�O�G�O�G�O�                JA  ARFMfmtp2.5                                                                 20090519071553  IP                  G�O�G�O�G�O�                JA  ARCAsspa3.0                                                                 20090519071554  IP  PRES            G�O�G�O�G�O�                JA  ARGQrqcppo_b                                                                20090519071554  QCP$                G�O�G�O�G�O�              3CJA  ARGQrqcpt19b                                                                20090519071555  QCP$                G�O�G�O�G�O�           80000JA  ARGQaqcpt19b                                                                20090519071555  QCP$                G�O�G�O�G�O�           80000JA  ARGQrqcp2.8b                                                                20090519071555  QCP$                G�O�G�O�G�O�            FB40JA  ARGQaqcp2.8b                                                                20090519071555  QCP$                G�O�G�O�G�O�            FB40JA  ARGQrqcpt16b                                                                20090519071555  QCP$                G�O�G�O�G�O�           10000JA  ARUP                                                                        20090519071925                      G�O�G�O�G�O�                JM  ARGQJMQC1.0                                                                 20071019165233  CV  DAT$            G�O�G�O�F���                JM  ARCAJMQC1.0                                                                 20071029081603  IP  PRES            G�O�G�O�G�O�                JM  ARCAJMQC1.0                                                                 20071029081603  IP  PSAL            G�O�G�O�G�O�                JM  ARCAJMTM1.0                                                                 20071029082419  CV  PSAL            G�O�G�O�G�O�                JM  ARSQOW  1.1 SeHyD1.0                                                        20090902000000  IP  PSAL            G�O�G�O�G�O�                JA  RFMTcnvd2.1                                                                 20090908020717  IP                  G�O�G�O�G�O�                JA  ARDU                                                                        20090908020759                      G�O�G�O�G�O�                