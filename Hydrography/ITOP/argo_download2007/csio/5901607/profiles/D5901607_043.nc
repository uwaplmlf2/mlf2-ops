CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   G   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       4    PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5d   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9    TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9H   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       :d   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       =,   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >H   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >x   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Ax   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Dx   CALIBRATION_DATE            	             
_FillValue                  ,  Gx   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    H   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    H   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         H   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         H   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        H    HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    H$Argo profile    2.2 1.2 19500101000000  5901607 CHINA ARGO PROJECT                                              XU JIANPING                                                     PRES            TEMP            PSAL               +A   HZ  20070818054941  20070818054941  0042_28207_043                  2C  D   APEX_SBE_2471                                                   846 @ԍ��}'�1   @ԍ�$h��@0!�@   @b��   1   ARGOS   A   A   A   @���AffA���A�  B  BE33Bm��B���B�  B�33B�33B�ffB�  CffC�C��C� C)L�C333C=ffCG33CQffC[33Ce� Co�CyL�C��fC��fC�� C��3C��3C��3C�� C���C�� C���C��Cǀ C�  C�� C�33C�� DfD	Y�D� D� D�D"FfD.�fD;Y�DG� DT9�D`�3DmS3Dy�3D�)�D�c3D�� D�� D�,�D�` D��3D��D�0 D�p D���D�l�D��fD�c3D��D�f11111111111111111111111111111111111111111111111111111111111111111111111 @���AffA���A�  B  BE33Bm��B���B�  B�33B�33B�ffB�  CffC�C��C� C)L�C333C=ffCG33CQffC[33Ce� Co�CyL�C��fC��fC�� C��3C��3C��3C�� C���C�� C���C��Cǀ C�  C�� C�33C�� DfD	Y�D� D� D�D"FfD.�fD;Y�DG� DT9�D`�3DmS3Dy�3D�)�D�c3D�� D�� D�,�D�` D��3D��D�0 D�p D���D�l�D��fD�c3D��D�f11111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�A��yA���A�1'A�A���A�A�A�(�A�n�Aߙ�Aۇ+A��A�p�A�33A�ZA���A��;A���A�7LA��;A�=qA��A��wA�-A��yA���A��A��jA���A�$�A��A~(�Ax^5ApVAiA^�/AX��A@�A.�yA%��AXA��A�\@���@�w@��@��#@�V@��-@�  @��9@�ȴ@�M�@�Z@�@��`@�~�@{�
@sƨ@lj@_��@R�@J~�@B�@:n�@4��@%�-@Z@��@1@/@?}11111111111111111111111111111111111111111111111111111111111111111111111 A��yA���A�1'A�A���A�A�A�(�A�n�Aߙ�Aۇ+A��A�p�A�33A�ZA���A��;A���A�7LA��;A�=qA��A��wA�-A��yA���A��A��jA���A�$�A��A~(�Ax^5ApVAiA^�/AX��A@�A.�yA%��AXA��A�\@���@�w@��@��#@�V@��-@�  @��9@�ȴ@�M�@�Z@�@��`@�~�@{�
@sƨ@lj@_��@R�@J~�@B�@:n�@4��@%�-@Z@��@1@/@?}11111111111111111111111111111111111111111111111111111111111111111111111 G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�G�O�B	�?B	�-B	��B	��B	�?B
�B
�B.BA�Bk�B�B�B�BBhB�B0!B;dB�BPB�B�BBĜB��BjBK�B.BoB
�B
��B
��B
y�B
J�B
 �B	�yB	ƨB	e`B	5?B	 �B	�B	bB	B	bB	�B	$�B	>wB	O�B	v�B	�uB	�wB	�5B	�B
B
PB
�B
 �B
&�B
,B
1'B
9XB
A�B
E�B
L�B
P�B
W
B
]/B
hsB
q�B
|�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111 B	�mB	�vB	�#B	�B	��B
�3B
�B/�BC@BmB�B��B�@B�B�B�B2�B?7B ^BB��B�zBƒB��BlHBMLB/�B5B
�B
�yB
�)B
{�B
L�B
#lB	�B	�%B	gTB	6LB	!�B	XB	qB	�B	�B	4B	%�B	>�B	P�B	wB	��B	��B	�}B	��B
DB
{B
�B
 �B
'B
,*B
1]B
9�B
A�B
E�B
L�B
P�B
W,B
]EB
h�B
q�B
|�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111 <�i<��<��<��<�'<�#<�{j<���<��U<���<��<��<�/<���<�rH<��<���<���<���<��B<�7�<�D@<�K<�A�<�� <���<���<��\<�T�<���<��{<�-<���<���<���<���<w�t<vU<vp1<wa�<vX<v�<u��<u�=<v2�<u��<vK<u�)<uԃ<u�k<u�<u��<u�<u�[<uȡ<u��<u�B<u��<u�|<uȉ<u�g<u�Y<u��<u��<u��<uÕ<uÚ<u�'<u�%<u<uPRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; WJO: No significant salinity drift detected; r=1.000000                                                                                                                                        none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(WJO & CTM error, SBE sensor accuracy)                                                                                                                                                   200712040952142007120409521420071204095214  HZ  ARGQ                                                                        20070818054941  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20070818054941  QCF$                G�O�G�O�G�O�0               HZ  ARSQWJO 2.0 WOD2001                                                         20071204095214  IP                  G�O�G�O�G�O�                