CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   G   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       4    PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5d   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6�   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  9    TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       9H   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       :d   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;�   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       =,   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >H   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >x   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    Ax   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    Dx   CALIBRATION_DATE            	             
_FillValue                  ,  Gx   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    H   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    H   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         H   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         H   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        H    HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    H$Argo profile    2.2 1.2 19500101000000  5901605 CHINA ARGO PROJECT                                              XU JIANPING                                                     PRES            TEMP            PSAL               ,A   HZ  20070908054838  20070908054838  0040_28205_044                  2C  D   APEX_SBE_2469                                                   846 @Ԓ�k��1   @Ԓ�M��@.Z��   @a��   1   ARGOS   A   A   A   @���A��A�ffA�ffB��BE33Bl��B���B�  B�ffB�  B�  BC� CffC�fC� C)L�C3ffC=� CG��CQL�C[ffCeL�Co��Cy�C�� C�� C���C��3C�� C�� C��fC���C��fC��3C�33Cǌ�C�@ C�3C�  C�� D�D	FfD�fD�fDfD"L�D.� D;9�DG��DTY�D`��Dm9�Dy� D�)�D�i�D��3D��fD�,�D�l�D���D��3D��D�p D���D�l�D��fD�ffD�� D�,�11111111111111111111111111111111111111111111111111111111111111111111111 @���A��A�ffA�ffB��BE33Bl��B���B�  B�ffB�  B�  BC� CffC�fC� C)L�C3ffC=� CG��CQL�C[ffCeL�Co��Cy�C�� C�� C���C��3C�� C�� C��fC���C��fC��3C�33Cǌ�C�@ C�3C�  C�� D�D	FfD�fD�fDfD"L�D.� D;9�DG��DTY�D`��Dm9�Dy� D�)�D�i�D��3D��fD�,�D�l�D���D��3D��D�p D���D�l�D��fD�ffD�� D�,�11111111111111111111111111111111111111111111111111111111111111111111111 @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A��`A��A�l�A�VA�VA�5?A�A�ZA���Aݣ�A�1A�ȴA��#A���A��HA�I�A���A�ȴA���A���A��mA�7LA���A���A��jA���A�^5A��-A�jA�=qAr9XAhVAg�7A_��AW�-ANA?O�A)hsA��A{A	"�A��@�;d@��@�l�@�/@�?}@���@��#@��^@�^5@��@���@�/@�Z@�^5@�S�@��P@{33@u��@lZ@`1'@W\)@P  @H�@8A�@(A�@\)@p�@�@111111111111111111111111111111111111111111111111111111111111111111111111 A��`A��A�l�A�VA�VA�5?A�A�ZA���Aݣ�A�1A�ȴA��#A���A��HA�I�A���A�ȴA���A���A��mA�7LA���A���A��jA���A�^5A��-A�jA�=qAr9XAhVAg�7A_��AW�-ANA?O�A)hsA��A{A	"�A��@�;d@��@�l�@�/@�?}@���@��#@��^@�^5@��@���@�/@�Z@�^5@�S�@��P@{33@u��@lZ@`1'@W\)@P  @H�@8A�@(A�@\)@p�@�@111111111111111111111111111111111111111111111111111111111111111111111111 ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB��B	49B	VB	ŢB
ȴB1'Bo�B|�B�BB�)B1BuBhB\B,BI�BO�B>wBG�B"�B�B��B�BK�B(�B�B  B
�`B
�3B
`BB
,B
#�B	��B	��B	��B	ffB	,B	!�B	�B	�B	P�B	t�B	w�B	�+B	��B	��B	��B	B	�B	�`B	�B
+B
oB
�B
"�B
+B
0!B
6FB
9XB
?}B
G�B
J�B
M�B
S�B
[#B
ffB
n�B
u�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111 B��B	4�B	8B	�CB
�B2�Bp!B}�B��BãB��B	�B�BEB�B.}BL�BP�BB�BJ�B&B�>B�`B�DBNcB)�B�B B
�cB
�:B
b�B
,EB
%�B	��B	�aB	�hB	h�B	-OB	#ZB	uB	%B	Q�B	u4B	x�B	��B	�*B	�GB	�iB	��B	�BB	�B	� B
rB
�B
�B
#B
+$B
0TB
6`B
9�B
?�B
G�B
J�B
M�B
TB
[FB
f{B
n�B
u�B
�B
�11111111111111111111111111111111111111111111111111111111111111111111111 <49w<4�_<4ƽ<9�<5l�<5�g<4h�<4�}<4h�<5�<9<�<6W,<4\�<4��<5�q<8wk<;�5<4�9<?�<;.<;_�<8��<>i�<;��<8��<4zz<5�O<5e<7S<?f'<8�.<4C�<75<7-�<8�K<5�%<88><5`�<5�<4�<4je<4�<4a-<4��<4<4e<4h<4a�<4Fv<4D<4?P<4P�<4G�<4>\<4@�<4C<4<�<4@w<4;+<4><4@�<4=�<4<e<4<d<4<�<4<�<4:�<4:�<4:�<49�<49cPRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; WJO: No significant salinity drift detected; r=1.000000                                                                                                                                        The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(WJO & CTM error, SBE sensor accuracy)                                                                                                                                                   200712040114422007120401144220071204011442  HZ  ARGQ                                                                        20070908054838  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20070908054838  QCF$                G�O�G�O�G�O�0               HZ  ARSQWJO 2.0 WOD2001                                                         20071204011442  IP                  G�O�G�O�G�O�                