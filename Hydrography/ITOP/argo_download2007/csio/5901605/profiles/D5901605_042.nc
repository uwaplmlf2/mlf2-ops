CDF      
      	DATE_TIME         STRING2       STRING4       STRING8       STRING16      STRING32       STRING64   @   	STRING256         N_PROF        N_PARAM       N_LEVELS   F   N_CALIB       	N_HISTORY                     <   	DATA_TYPE                  comment       	Data type      
_FillValue                    0�   FORMAT_VERSION                 comment       File format version    
_FillValue                    0�   HANDBOOK_VERSION               comment       Data handbook version      
_FillValue                    0�   REFERENCE_DATE_TIME                 comment       !Date of reference for Julian days      conventions       YYYYMMDDHHMISS     
_FillValue                    1    PLATFORM_NUMBER                   	long_name         Float unique identifier    conventions       WMO float identifier : A9IIIII     
_FillValue                    1   PROJECT_NAME                  comment       Name of the project    
_FillValue                  @  1   PI_NAME                   comment       "Name of the principal investigator     
_FillValue                  @  1X   STATION_PARAMETERS           	            	long_name         ,List of available parameters for the station   conventions       Argo reference table 3     
_FillValue                  0  1�   CYCLE_NUMBER               	long_name         Float cycle number     conventions       <0..N, 0 : launch cycle (if exists), 1 : first complete cycle   
_FillValue         ��        1�   	DIRECTION                  	long_name         !Direction of the station profiles      conventions       -A: ascending profiles, D: descending profiles      
_FillValue                    1�   DATA_CENTRE                   	long_name         .Data centre in charge of float data processing     conventions       Argo reference table 4     
_FillValue                    1�   DATE_CREATION                   comment       Date of file creation      conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DATE_UPDATE                 	long_name         Date of update of this file    conventions       YYYYMMDDHHMISS     
_FillValue                    1�   DC_REFERENCE                  	long_name         (Station unique identifier in data centre   conventions       Data centre convention     
_FillValue                     1�   DATA_STATE_INDICATOR                  	long_name         1Degree of processing the data have passed through      conventions       Argo reference table 6     
_FillValue                    2   	DATA_MODE                  	long_name         Delayed mode or real time data     conventions       >R : real time; D : delayed mode; A : real time with adjustment     
_FillValue                    2   INST_REFERENCE                    	long_name         Instrument type    conventions       Brand, type, serial number     
_FillValue                  @  2   WMO_INST_TYPE                     	long_name         Coded instrument type      conventions       Argo reference table 8     
_FillValue                    2\   JULD               	long_name         ?Julian day (UTC) of the station relative to REFERENCE_DATE_TIME    units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2`   JULD_QC                	long_name         Quality on Date and Time   conventions       Argo reference table 2     
_FillValue                    2h   JULD_LOCATION                  	long_name         @Julian day (UTC) of the location relative to REFERENCE_DATE_TIME   units         "days since 1950-01-01 00:00:00 UTC     conventions       8Relative julian days with decimal part (as parts of day)   
_FillValue        A.�~            2l   LATITUDE               	long_name         &Latitude of the station, best estimate     units         degree_north   
_FillValue        @�i�       	valid_min         �V�        	valid_max         @V�             2t   	LONGITUDE                  	long_name         'Longitude of the station, best estimate    units         degree_east    
_FillValue        @�i�       	valid_min         �f�        	valid_max         @f�             2|   POSITION_QC                	long_name         ,Quality on position (latitude and longitude)   conventions       Argo reference table 2     
_FillValue                    2�   POSITIONING_SYSTEM                    	long_name         Positioning system     
_FillValue                    2�   PROFILE_PRES_QC                	long_name         #Global quality flag of PRES profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_TEMP_QC                	long_name         #Global quality flag of TEMP profile    conventions       Argo reference table 2a    
_FillValue                    2�   PROFILE_PSAL_QC                	long_name         #Global quality flag of PSAL profile    conventions       Argo reference table 2a    
_FillValue                    2�   PRES         
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       2�   PRES_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  3�   PRES_ADJUSTED            
      	   	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    	valid_min                	valid_max         F;�    comment       $In situ measurement, sea surface = 0   C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       3�   PRES_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  5   PRES_ADJUSTED_ERROR          
         	long_name         SEA PRESSURE   
_FillValue        G�O�   units         decibar    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %7.1f      FORTRAN_format        F7.1   
resolution        =���       5\   TEMP         
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       6t   TEMP_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  7�   TEMP_ADJUSTED            
      	   	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     	valid_min         �      	valid_max         B      comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       7�   TEMP_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  8�   TEMP_ADJUSTED_ERROR          
         	long_name         $SEA TEMPERATURE IN SITU ITS-90 SCALE   
_FillValue        G�O�   units         degree_Celsius     comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       94   PSAL         
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       :L   PSAL_QC          
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  ;d   PSAL_ADJUSTED            
      	   	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    	valid_min                	valid_max         B(     comment       In situ measurement    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       ;�   PSAL_ADJUSTED_QC         
         	long_name         quality flag   conventions       Argo reference table 2     
_FillValue                  H  <�   PSAL_ADJUSTED_ERROR          
         	long_name         PRACTICAL SALINITY     
_FillValue        G�O�   units         psu    comment       WContains the error on the adjusted values as determined by the delayed mode QC process.    C_format      %9.3f      FORTRAN_format        F9.3   
resolution        :�o       =   	PARAMETER               	            	long_name         /List of parameters with calibration information    conventions       Argo reference table 3     
_FillValue                  0  >$   SCIENTIFIC_CALIB_EQUATION               	            	long_name         'Calibration equation for this parameter    
_FillValue                    >T   SCIENTIFIC_CALIB_COEFFICIENT            	            	long_name         *Calibration coefficients for this equation     
_FillValue                    AT   SCIENTIFIC_CALIB_COMMENT            	            	long_name         .Comment applying to this parameter calibration     
_FillValue                    DT   CALIBRATION_DATE            	             
_FillValue                  ,  GT   HISTORY_INSTITUTION                      	long_name         "Institution which performed action     conventions       Argo reference table 4     
_FillValue                    G�   HISTORY_STEP                     	long_name         Step in data processing    conventions       Argo reference table 12    
_FillValue                    G�   HISTORY_SOFTWARE                     	long_name         'Name of software which performed action    conventions       Institution dependent      
_FillValue                    G�   HISTORY_SOFTWARE_RELEASE                     	long_name         2Version/release of software which performed action     conventions       Institution dependent      
_FillValue                    G�   HISTORY_REFERENCE                        	long_name         Reference of database      conventions       Institution dependent      
_FillValue                  @  G�   HISTORY_DATE                      	long_name         #Date the history record was created    conventions       YYYYMMDDHHMISS     
_FillValue                    G�   HISTORY_ACTION                       	long_name         Action performed on data   conventions       Argo reference table 7     
_FillValue                    G�   HISTORY_PARAMETER                        	long_name         (Station parameter action is performed on   conventions       Argo reference table 3     
_FillValue                    G�   HISTORY_START_PRES                    	long_name          Start pressure action applied on   
_FillValue        G�O�   units         decibar         G�   HISTORY_STOP_PRES                     	long_name         Stop pressure action applied on    
_FillValue        G�O�   units         decibar         G�   HISTORY_PREVIOUS_VALUE                    	long_name         +Parameter/Flag previous value before action    
_FillValue        G�O�        G�   HISTORY_QCTEST                       	long_name         <Documentation of tests performed, tests failed (in hex form)   conventions       EWrite tests performed when ACTION=QCP$; tests failed when ACTION=QCF$      
_FillValue                    H Argo profile    2.2 1.2 19500101000000  5901605 CHINA ARGO PROJECT                                              XU JIANPING                                                     PRES            TEMP            PSAL               *A   HZ  20070818054936  20070818054936  0040_28205_042                  2C  D   APEX_SBE_2469                                                   846 @ԍ��6;�1   @ԍ�I2q@.��   @a�Ơ   1   ARGOS   A   A   A   @���A33A���A���B33BFffBn  B���B�33B�ffB���B�33B�  CL�C� C��CL�C)� C3L�C=��CG��CQL�C[L�Cd��Co��Cy33C���C��3C���C���C���C��fC���C�� C��fC�s3C��3C�Y�C�33C�3C��C��fD�D	FfD��D�3D�3D"FfD.�3D;L�DG�3DTFfD`��Dm@ Dy�3D�)�D�ffD��3D�� D�0 D�ffD��3D���D�33D�l�D��fD�` D���D�c3D�9�1111111111111111111111111111111111111111111111111111111111111111111111  @���A33A���A���B33BFffBn  B���B�33B�ffB���B�33B�  CL�C� C��CL�C)� C3L�C=��CG��CQL�C[L�Cd��Co��Cy33C���C��3C���C���C���C��fC���C�� C��fC�s3C��3C�Y�C�33C�3C��C��fD�D	FfD��D�3D�3D"FfD.�3D;L�DG�3DTFfD`��Dm@ Dy�3D�)�D�ffD��3D�� D�0 D�ffD��3D���D�33D�l�D��fD�` D���D�c3D�9�1111111111111111111111111111111111111111111111111111111111111111111111  @��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��@��A� �A�1A�?}A�+A�-A�A�9XA�ZA�&�Aܙ�A��yA�%AփAԕ�A�5?AͮA� �A�(�A�-A�M�A���A��PA�~�A�l�A���A��+A|At  Anz�A`�9A[�FARI�AMO�AH�A>�A:�yA7%A&=qA"�Ax�A
��AĜ@���@��T@�1'@��/@��@��@�J@�O�@���@���@��m@�?}@�%@���@���@�/@l�@s��@k��@h1'@` �@T�@Kƨ@=p�@/+@!&�@`B@$�1111111111111111111111111111111111111111111111111111111111111111111111  A� �A�1A�?}A�+A�-A�A�9XA�ZA�&�Aܙ�A��yA�%AփAԕ�A�5?AͮA� �A�(�A�-A�M�A���A��PA�~�A�l�A���A��+A|At  Anz�A`�9A[�FARI�AMO�AH�A>�A:�yA7%A&=qA"�Ax�A
��AĜ@���@��T@�1'@��/@��@��@�J@�O�@���@���@��m@�?}@�%@���@���@�/@l�@s��@k��@h1'@` �@T�@Kƨ@=p�@/+@!&�@`B@$�1111111111111111111111111111111111111111111111111111111111111111111111  ;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;o;oB	@�B	�bB
B
s�B
�B
�HB�B'�B8RBt�B�B��B��B�/B��B��B&�B;dBF�B49B%�B��B�jBgmB(�B
��B
iyB
K�B
0!B	��B	�yB	��B	�wB	�B	�uB	�\B	�B	�B	gmB	aHB	O�B	iyB	~�B	}�B	�B	��B	��B	�B	ÖB	��B	�sB	��B
%B
oB
�B
�B
&�B
+B
1'B
6FB
<jB
=qB
C�B
H�B
M�B
W
B
aHB
jB
t�B
{�1111111111111111111111111111111111111111111111111111111111111111111111  B	@�B	��B
�B
s�B
��B
��BB(�B9�BuqB�zB�1B̠BޟB�eB�PB)B<�BJB5�B(�B��B��Bi�B0,B
�'B
krB
M9B
3�B	�1B	��B	�*B	��B	��B	�|B	��B	��B	�TB	hOB	bQB	P�B	jUB	�B	~�B	��B	��B	�8B	�nB	��B	�?B	�B	�6B
^B
�B
�B
�B
'B
+1B
1XB
6jB
<~B
=�B
C�B
H�B
M�B
W*B
agB
j�B
t�B
{�1111111111111111111111111111111111111111111111111111111111111111111111  <4;�<4]�<4NA<4A�<6<<��<4Sa<4�<5�Y<4��<4SD<5�s<4�`<5�e<5�V<<k�<8�<5�r<<I�<6*<:b�<9ҩ<H+�<8��<U��<@p:<6��<5��<<�2<5q�<8a <5}�<5Sw<9i�<4��<4^�<6�l<5A�<4Ƅ<4�Y<4��<4��<4qR<4wA<4�]<4\�<4J�<4O�<4H-<4D�<4F<4J�<4BI<4=Q<4=�<4@�<4?j<4?�<4?�<4<�<4:h<4<�<4?�<4=�<4<=<4<1<4<<4;[<4:O<49fPRES            TEMP            PSAL            none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            PSAL_ADJ corrects Cnd. Thermal Mass (CTM), Johnson et al., 2007, JAOT; PSAL_ADJ = CTM_ADJ_PSAL + dS, dS is calculated from a potential conductivity (ref to 0 dbar) multiplicative adjustment term r.                                                           none                                                                                                                                                                                                                                                            none                                                                                                                                                                                                                                                            CTM: alpha=0.0267, tau=18.6s with error equal to the adjustment; WJO: No significant salinity drift detected; r=1.000000                                                                                                                                        The quoted error is manufacturer specified accuracy at time of laboratory calibration.                                                                                                                                                                          The quoted error is manufacturer specified accuracy with respect to ITS-90 at time of laboratory calibration.                                                                                                                                                   No significant salinity drift and/or offset detected; PSAL_ADJ_ERR: max(WJO & CTM error, SBE sensor accuracy)                                                                                                                                                   200712040114392007120401143920071204011439  HZ  ARGQ                                                                        20070818054936  QCP$                G�O�G�O�G�O�FFBFE           HZ  ARGQ                                                                        20070818054936  QCF$                G�O�G�O�G�O�0               HZ  ARSQWJO 2.0 WOD2001                                                         20071204011439  IP                  G�O�G�O�G�O�                