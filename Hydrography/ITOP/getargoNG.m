%GET argo profiles
clear all
k=0;
D0=dir('argo*');
for i0=1:length(D0)
    D1=dir(D0(i0).name);
    for i1=1:length(D1)
        D2=dir([D0(i0).name '/' D1(i1).name]);
        for i2=1:length(D2)
            D3=dir([D0(i0).name '/' D1(i1).name '/' D2(i2).name '/profiles']);
            for i3=1:length(D3)
                file=[D0(i0).name '/' D1(i1).name '/' D2(i2).name '/profiles/' D3(i3).name];
                %disp(file)
                fp=fopen(file);
                if fp~=-1
                    fclose(fp);
                    k=k+1;
                    dat(k).P=getnc(file,'PRES');
                    dat(k).T=getnc(file,'TEMP');
                    dat(k).S=getnc(file,'PSAL');
                    dat(k).lat=getnc(file,'LATITUDE');
                    dat(k).lon=getnc(file,'LONGITUDE');
                    dat(k).juld=getnc(file,'JULD'); %days since 1950-01-01 00:00:00 UTC
                end
            end
        end
        disp(k)
    end
end
save argoall.mat
