
clear all

col='rgbkmc';
kk=0;
for k=0:5
    if k==0
        file='/Users/ericdasaro/Documents/Maudness/Historical CTD/ODVctd/ODVCTD.mat';
        load(file);
        A=B';
        tit='NODC Summer';
    elseif k==1
        file='/Users/ericdasaro/Documents/Maudness/Historical CTD/CTDAnzf+NODC/NODC_winter_stations.mat';
        load(file);
        A=A;
        tit='NODC Winter';
    elseif k==2
        file='/Users/ericdasaro/Documents/Maudness/Historical CTD/CTDAnzf+NODC/ANZFLUX.mat';
        load(file);
        A=Yd(6:end);
        tit='/Users/ericdasaro/Documents/Maudness/Historical CTD/ANZFLUX YoYo';
    elseif k==3
        file='/Users/ericdasaro/Documents/Maudness/Historical CTD/CTDAnzf+NODC/ANZFLUX.mat';
        load(file);
        A=Sd;
        tit='/Users/ericdasaro/Documents/Maudness/Historical CTD/ANZFLUX CTD';
    elseif k==4
        file='AnzfluxAll/consolidate/data/mstructure/OSU_Profiler_Maud_int.mat';
        continue;  % No good data...
    elseif k==5
        load '/Users/ericdasaro/Documents/Maudness/Historical CTD/LES/LESpack.mat'
        tit='LES Simulation';
        P=sw_pres(-z,-63*ones(size(z)));
        nskip=200;
        g=[1:nskip:12000];  % Subsample in tme
        t=t(g);
        SM=SM(g,:);
        TM=TM(g,:);  % potential temperature
        A=TM;
    end

    for i=1:size(A,1)
        if k>-1 & k<4
            if size(A(i).data,2)~=4
                continue
            end
            kk=kk+1;
            lat(kk)=A(i).hdr.lat;
            lon(kk)=A(i).hdr.long;
            yr(kk)=A(i).hdr.date(1);
            P=A(i).data(:,4);
            T=A(i).data(:,2);
            S=A(i).data(:,3);
            g=find(P<500);
            P=P(g);
            T=T(g);
            S=S(g);
            S(find(S<1))=NaN;
            Z=-sw_dpth(P,-65);
            Sig0=sw_pden(S,T,P,0);
            Th=sw_ptmp(S,T,P,0);
        elseif k==4
            kk=kk+1;
            b=find( kk-bad ==0);  % NG profile
            if length(b)>0
                continue
            end
            S=sal_int(i,:);
            Th=ptemp_int(i,:);
            yr(kk)=time(i);
            lon(kk)=NaN;
            lat(kk)=NaN;
            Z= -yi;
            g=find(~isnan(S+Th+Z));
            if length(g)==0
                continue
            end
            S=S(g);
            Th=Th(g);
            Z=Z(g);
            P=sw_pres(-Z',-65)';
            T=sw_temp(S,Th,P,0);
            Sig0=sw_pden(S,T,P,0);
        elseif k==5
            kk=kk+1;
            S=SM(i,:);
            Th=TM(i,:);
            Z= -sw_dpth(P',65)';
            T=sw_temp(S,Th,P,0);
            Sig0=sw_pden(S,T,P,0);
            yr(kk)=t(i);
            lon(kk)=NaN;
            lat(kk)=NaN;
        end
        if max(P)<490 | min(P)>20 | sum(isnan(T+S))>0  % screen for shallow & bad data
            kk=kk-1;
            continue;
        end

        ggg=find(~isnan(T+S+P));  % No NaNs allowed
        P=P(ggg);
        S=S(ggg);
        T=T(ggg);
        P(1)=0;       % Stretch out top & bottom to fill needed space
        P(end)=600;
        dz=1;
        P0=[0:dz:600];
   
        if k>1   % high quality modern data
            S0=interp1(P,S,P0,'linear','extrap');
            T0=interp1(P,T,P0,'linear','extrap');
        else    % Crummy old data - smooth over 50m
            for ii=1:length(P0)
                gp=find(abs(P-P0(ii))<25);
                if length(gp)>0
                    S0(ii)=mean(S(gp));
                    T0(ii)=mean(T(gp));
                else
                    S0(ii)=NaN;
                    T0(ii)=NaN;
                end
            end
            bb=find(isnan(S0));
            ggg=find(~isnan(S0));
            if length(bb)>0
                S0(bb)=interp1(P0(ggg),S0(ggg),P0(bb),'linear','extrap');
                T0(bb)=interp1(P0(ggg),T0(ggg),P0(bb),'linear','extrap');
            end
        end
        dd=find(P0>510);   % Extrapolated range
        T0(dd)=T0(dd)-(P0(dd)-510)/100.*0.04;  % put some fake bottom Strat.

        N=min(length(P0),600);
        Pall(kk,1:N)=P0(1:N)';
        Sall(kk,1:N)=S0(1:N)';
        Tall(kk,1:N)=T0(1:N)';
        staA.lat(kk)=lat(kk);
        staA.lon(kk)=lon(kk);
        staA.yr(kk)=yr(kk);
        staA.mo(kk)=NaN;
        staA.day(kk)=NaN;
        staA.id(kk)=k;

    end
end
S=Sall';T=Tall';P=Pall';
sta=staA;

SIG_0=sw_pden(S,T,P,0) -1000;   % potential density
SIG = sw_dens(S,T,P)-1000;         % In-situ density
TH = sw_ptmp(S,T,P,0);                % potential temperature
readme='Weddell Sea CTD';
save 'Weddell/clean.mat' sta S T P SIG_0 SIG TH readme


