% add weak stratification point to database
clear all
    
XX=[0        10.0000   28.7820
    6.1370   10.0000   28.7883
   12.7248   10.0000   28.7987
   19.3127   10.0000   28.8113
   27.4208   10.0000   28.8238
   32.4884   10.0000   28.8343
   41.6100   10.0000   28.8426
   45.6641   10.0000   28.8698
   52.2519   10.0000   28.8991
   300          10           29.2
   ];

load clean.mat
%P       S       SIG     SIG_0   T       TH      readme  sta 
N=size(P,2)+1;
g=[1:size(XX,1)];
b=[size(XX,1)+1:size(P,1)];
P(g,N)=XX(g,1); P(b,N)=NaN;
T(g,N)=XX(g,2); T(b,N)=NaN;
S(g,N)=XX(g,3); S(b,N)=NaN;
SIG=sw_dens(S,T,P)-1000;
SIG_0=sw_pden(S,T,P,0)-1000;
TH=sw_ptmp(S,T,P,0);
sta.lat(14)=NaN;
sta.lon(14)=NaN;
sta.yr(14)=NaN;
sta.mo(14)=NaN;
sta.day(14)=NaN;

plot(SIG_0,-P)
hold on
plot(SIG_0(:,14),-P(:,14),'ko');

save newclean.mat


