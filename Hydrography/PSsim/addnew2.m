% add strong stratification point to database
clear all
load newclean.mat
%P       S       SIG     SIG_0   T       TH      readme  sta

for i=1:3
    if i==1
    XX=[0        10.0000   24
        300          10          36
        ];
    elseif i==2
    XX=[0        10.0000   22
        100          10          35
        300          10          36
        ];
    elseif i==3
    XX=[0        10.0000   21
        30            10         21.1
        90            10          36
        300          10          36.4
        ];
    end


    N=size(P,2)+1;
    g=[1:size(XX,1)];
    b=[size(XX,1)+1:size(P,1)];
    P(g,N)=XX(g,1); P(b,N)=NaN;
    T(g,N)=XX(g,2); T(b,N)=NaN;
    S(g,N)=XX(g,3); S(b,N)=NaN;
    SIG=sw_dens(S,T,P)-1000;
    SIG_0=sw_pden(S,T,P,0)-1000;
    TH=sw_ptmp(S,T,P,0);
    sta.lat(N)=NaN;
    sta.lon(N)=NaN;
    sta.yr(N)=NaN;
    sta.mo(N)=NaN;
    sta.day(N)=NaN;

    plot(SIG_0,-P)
    hold on
    plot(SIG_0(:,N),-P(:,N),'ko');

end
save newclean2.mat


