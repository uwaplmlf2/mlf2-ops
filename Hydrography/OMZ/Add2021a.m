% Add 2021 profile, float 11095 launch

clear all

load('cleanOMZ_2020.mat');
%  Name         Size                Bytes  Class     Attributes
% 
%   P           94x1792            1347584  double              
%   S           94x1792            1347584  double              
%   SIG         94x1792            1347584  double              
%   SIG_0       94x1792            1347584  double              
%   T           94x1792            1347584  double              
%   TH          94x1792            1347584  double              
%   readme       1x9                    18  char                
%   sta          1x1                 66920  struct 
%
% sta = 
%   struct with fields:
%     lat: [1×1652 double]
%     lon: [1×1652 double]
%      yr: [1×1652 double]
%      mo: [1×1652 double]
%     day: [1×1652 double]

file='/Users/eric/Documents/OMZ/OMZ-II/OMZ Float/Operations Second Cruise/11095 Deployment/S28_E2_LaunchCTD.mat';
load(file,'ctd');
% CTD
%         latitude: [395601×1 double]   11.8122
%        longitude: [395601×1 double]   -94.3417
%             prDM: [395601×1 double]
%            depSM: [395601×1 double]
%        sigma_E00: [395601×1 double]
%            sal00: [395601×1 double]
%            t090C: [395601×1 double]
%     timeS_matlab: [395601×1 double]
%       timeS_date: [395601×20 char]    02-Jan-2022 11:24:25


n=length(sta.lat)+1;
sta.lat(n)=11.8122;
sta.lon(n)=-94.3417;
sta.yr(n)=2022;
sta.mo(n)=1;
sta.day(n)=02;


%   P           94x1792            1347584  double              
%   S           94x1792            1347584  double              
%   SIG         94x1792            1347584  double              
%   SIG_0       94x1792            1347584  double              
%   T           94x1792            1347584  double              
%   TH          94x1792            1347584  double              
%   readme       1x9                    18  char                
%   sta          1x1                 66920  struct 


PP=[0:5:100 110:10:500 550:50:1400]; 

P(1:length(PP),n)=PP';
[pmax,imax]=max(ctd.prDM);
kk=1:imax;
imin=1.35e4;
kk=kk(imin:end);
[~,isort]=sort(ctd.prDM(kk));
kk=kk(isort);
[~,iu]=unique(ctd.prDM(kk));
kk=kk(iu);

S(:,n)=interp1(ctd.prDM(kk),ctd.sal00(kk),P(:,n));
T(:,n)=interp1(ctd.prDM(kk),ctd.t090C(kk),P(:,n));
TH(:,n)=sw_ptmp(S(:,n),T(:,n),P(:,n),0);
SIG_0(:,n)=sw_pden(S(:,n),T(:,n),P(:,n),0)-1000;
SIG(:,n)=sw_dens(S(:,n),T(:,n),P(:,n))-1000;
readme='OMZ Argos- last profile from 11095 deploy';

for i=[1:100:n]
    plot(S(:,i),-P(:,i));
    hold on
end
plot(S(:,n),-P(:,n),'k-','LineWidth',2);
plot(ctd.sal00(kk),-ctd.prDM(kk),'.');

save('cleanOMZ_2021.mat','P','S','SIG','SIG_0','T','TH','readme','sta');


