% Add 2022 profile, float 11094 launch

clear all

load('cleanOMZ_2021.mat');
%  Name         Size                Bytes  Class     Attributes
% 
%   P           94x1792            1347584  double              
%   S           94x1792            1347584  double              
%   SIG         94x1792            1347584  double              
%   SIG_0       94x1792            1347584  double              
%   T           94x1792            1347584  double              
%   TH          94x1792            1347584  double              
%   readme       1x9                    18  char                
%   sta          1x1                 66920  struct 
%
% sta = 
%   struct with fields:
%     lat: [1×1652 double]
%     lon: [1×1652 double]
%      yr: [1×1652 double]
%      mo: [1×1652 double]
%     day: [1×1652 double]

file='/Users/eric/Documents/GIT/mlf2-ops/Hydrography/OMZ/xo11094_Launch.mat';
load(file,'ctd');
%                        timeS: [1996×1 double]
%                     latitude: [1996×1 double]
%                    longitude: [1996×1 double]
%                         prDM: [1996×1 double]
%                        depSM: [1996×1 double]
%                    sigma_E00: [1996×1 double]
%                        sal00: [1996×1 double]
%                        t090C: [1996×1 double]
%                 timeS_matlab: [1996×1 double]
%                   timeS_date: [1996×20 char]

n=length(sta.lat)+1;
sta.lat(n)=12.5397;
sta.lon(n)=-101.3794;
sta.yr(n)=2022;
sta.mo(n)=1;
sta.day(n)=08;


%   P           94x1792            1347584  double              
%   S           94x1792            1347584  double              
%   SIG         94x1792            1347584  double              
%   SIG_0       94x1792            1347584  double              
%   T           94x1792            1347584  double              
%   TH          94x1792            1347584  double              
%   readme       1x9                    18  char                
%   sta          1x1                 66920  struct 


PP=[0:5:100 110:10:500 550:50:1400]; 

P(1:length(PP),n)=PP';
[pmax,imax]=max(ctd.prDM);
kk=1:imax;
imin=1; % First good point
kk=kk(imin:end);
[~,isort]=sort(ctd.prDM(kk));
kk=kk(isort);
[~,iu]=unique(ctd.prDM(kk));
kk=kk(iu);

S(:,n)=interp1(ctd.prDM(kk),ctd.sal00(kk),P(:,n));
T(:,n)=interp1(ctd.prDM(kk),ctd.t090C(kk),P(:,n));
TH(:,n)=sw_ptmp(S(:,n),T(:,n),P(:,n),0);
SIG_0(:,n)=sw_pden(S(:,n),T(:,n),P(:,n),0)-1000;
SIG(:,n)=sw_dens(S(:,n),T(:,n),P(:,n))-1000;
readme='OMZ 2022  last profile is 11094';

for i=[n-3:n] %[1:100:n]
    plot(TH(:,i),-P(:,i));
    hold on
end
plot(TH(:,n),-P(:,n),'k-','LineWidth',2);
plot(ctd.potential_temperature(kk),-ctd.prDM(kk),'.');

save('cleanOMZ_2022.mat','P','S','SIG','SIG_0','T','TH','readme','sta');


