% Plot OMZ profiles
clear all

load 'cleanOMZ.mat' 
%sta S T P SIG_0 SIG TH readme   

cm=colormap();
j=0;
for i=[180 200 210 230 250 280 300  900  1400 1350  1380 1440 150 250 ] %1:length(sta.yr)
    if (sta.lat(i)<0 );
        continue
    end
    j=j+1;
    kk(j)=i;
    
    icol=cm(mod(j*10,64)+1,:);
    s1=subplot(2,2,1);
    plot(SIG_0(:,i),-P(:,i),'Color',icol);
    hold on
    %text(min(SIG_0(:,i)),-15,num2str(i),'FontSize',12,'FontWeight','bold');
    title('SIG_0','FontSize',12,'FontWeight','bold');

    s2=subplot(2,2,3);
    plot(TH(:,i),-P(:,i),'Color',icol);
    hold on
   % text(TH(1,i),-15,num2str(i),'FontSize',12,'FontWeight','bold');
    title('PotTEMP','FontSize',12,'FontWeight','bold');

    s3=subplot(2,2,4);
    plot(S(:,i),-P(:,i),'Color',icol);
    hold on
    %text(S(1,i),-15,num2str(i),'FontSize',12,'FontWeight','bold');
    title('SALINITY','FontSize',12,'FontWeight','bold');

    s4=subplot(2,2,2);
    plot(sta.lon(i),sta.lat(i),'*','Color',icol)
    hold on
    if j<10
        text(sta.lon(i),sta.lat(i),num2str(i),'FontSize',12,'FontWeight','bold');
    end
    if S(1,i)<30
         plot(sta.lon(i),sta.lat(i),'ro','MarkerSize',14)  
    end
    hold on
    title('MAP','FontSize',12,'FontWeight','bold');
end
a=axis();
load 'Coast.mat'
plot(lon,lat,'k');
axis(a);
set(gca,'DataAspectRatio',[1 cos(17/180*pi) 1]);
set(gcf,'Color','w');

linkaxes([s1 s2 s3],'y');
ylim(s1,[-600 0]);

%% ID fig
figure
plot(kk,TH(1,kk),'+')
hold on
plot(kk,S(25:35,kk),'r.')






