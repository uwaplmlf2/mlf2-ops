
% Add 2020 profile

clear all

load('cleanOMZ.mat');
%  Name         Size                Bytes  Class     Attributes
% 
%   P           94x1792            1347584  double              
%   S           94x1792            1347584  double              
%   SIG         94x1792            1347584  double              
%   SIG_0       94x1792            1347584  double              
%   T           94x1792            1347584  double              
%   TH          94x1792            1347584  double              
%   readme       1x9                    18  char                
%   sta          1x1                 66920  struct 
%
% sta = 
%   struct with fields:
%     lat: [1×1652 double]
%     lon: [1×1652 double]
%      yr: [1×1652 double]
%      mo: [1×1652 double]
%     day: [1×1652 double]

load('Qprofile.mat');
% Prof = struct with fields:
%        P: [1402×1 double]
%        T: [1402×1 double]
%        S: [1402×1 double]
%      rho: [1402×1 double]
%     time: 23-Dec-2020 06:57:15

n=1653;
sta.lat(n)=17+17.56/60;
sta.lon(n)=-(107 + 45.71/60);
sta.yr(n)=2020;
sta.mo(n)=12;
sta.day(n)=24;


%   P           94x1792            1347584  double              
%   S           94x1792            1347584  double              
%   SIG         94x1792            1347584  double              
%   SIG_0       94x1792            1347584  double              
%   T           94x1792            1347584  double              
%   TH          94x1792            1347584  double              
%   readme       1x9                    18  char                
%   sta          1x1                 66920  struct 


PP=[0:5:100 110:10:500 550:50:1400]; 

P(1:length(PP),n)=PP'; 

S(:,n)=interp1(Prof.P,Prof.S,P(:,n));
T(:,n)=interp1(Prof.P,Prof.T,P(:,n));
TH(:,n)=sw_ptmp(S(:,n),T(:,n),P(:,n),0);
SIG_0(:,n)=sw_pden(S(:,n),T(:,n),P(:,n),0)-1000;
SIG(:,n)=sw_dens(S(:,n),T(:,n),P(:,n))-1000;
readme='OMZ Argos- profile 1653 is from 2020 float';

for i=[1:100:1652]
    plot(S(:,i),-P(:,i));
    hold on
end
plot(S(:,n),-P(:,n),'k-','LineWidth',2);

save('cleanOMZ_2020.mat','P','S','SIG','SIG_0','T','TH','readme','sta');


