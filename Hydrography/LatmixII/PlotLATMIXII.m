% Plot LATMIX profiles
clear all

load 'cleanLATMIXII.mat' 
%sta S T P SIG_0 SIG TH readme   

col='rgbkmcrgbkmcrgbkmcrgbkmcrgbkmcrgbkmcrgbkmc';
for i=1:14
    subplot(2,2,1)
    plot(SIG_0(:,i),-P(:,i),col(i));
    hold on
    text(min(SIG_0(:,i)),-15,num2str(i),'FontSize',12,'FontWeight','bold');
    title('SIG_0','FontSize',12,'FontWeight','bold');

    
    subplot(2,2,3)
    plot(TH(:,i),-P(:,i),col(i));
    hold on
    text(TH(1,i),-15,num2str(i),'FontSize',12,'FontWeight','bold');
    title('PotTEMP','FontSize',12,'FontWeight','bold');

    
    subplot(2,2,4)
    plot(S(:,i),-P(:,i),col(i));
    hold on
    text(S(1,i),-15,num2str(i),'FontSize',12,'FontWeight','bold');
    title('SALINITY','FontSize',12,'FontWeight','bold');

    subplot(2,2,2)
    plot(sta.lon(i),sta.lat(i),'+','Color',col(i))
    text(sta.lon(i),sta.lat(i),num2str(i),'FontSize',12,'FontWeight','bold');
    hold on
    title('MAP','FontSize',12,'FontWeight','bold');
end
load 'WNatlantic.mat'
plot(lon,lat);
xlim([-79 -60]);
ylim([30 45]);
set(gca,'DataAspectRatio',[1 cos(34/180*pi) 1]);
set(gcf,'Color','w');


