for i=1:size(S,1)
plot(SIG_0(:,i),-P(:,i),'k',(SIG_0(:,i)-max(SIG_0(:,i)))*10+27.8,-P(:,i),'r');
axis([27.2 27.9 -500 0]);
title([num2str(i) '   ID ' num2str(sta.id(i))])
pause
clf
end