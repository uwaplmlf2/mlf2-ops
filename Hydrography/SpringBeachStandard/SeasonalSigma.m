% Collect all Spring Beach CTD profiles
% All prefaced by /Users/ericdasaro/Documents/
% ADV ballast
%ADVfloat/Testing/Miller1203-03/Ballast/env0001.nc
kk=0;
CB=colormap;
fp=fopen('SpringBeachCTD.txt','r');
while(1)
    name=fgetl(fp);
    while(strcmp(name(1),'%')==1)
        name=fgetl(fp);
    end
    if name==-1
        fclose(fp);
        break
    end
    file=['/Users/ericdasaro/Documents/' name];
    %disp(name)
    timef=getnc(file,'time'); % sec ref to   Jan 1 1970 00:00:00 (GMT)
    sec=(timef-timef(1));   % Seconds from start of file
    Mtime=timef/86400+datenum('01-Jan-1970');  % Matlab time days
    %yd=Mtime-datenum('01-Jan-2006')+1;  % yearday 2004
    Vtime=datevec(Mtime);  % time vector
    fprintf(1,'%s',name);

    Area=pi*(2*2.54/2)^2;

    % CONTROL
    P=getnc(file,'pressure');
    B=getnc(file,'piston')*Area;

    % CTD
    T=getnc(file,'temp');  % 2 values
    S=getnc(file,'sal');  % 2 values
    Sigma=sw_pden(S,T,P,0)-1000;
    Th=sw_ptmp(S,T,P,0);

    mask=ones(size(P));
    b=find(S(:,1)<10. | T(:,1)<5 | Sigma(:,1)<10);
    mask(b)=NaN;

    gp=find(diff(sec)>25);  % not profile
    mask(gp)=NaN;

    gg=find(~isnan(Sigma(:,1)) & abs(P-40)<10);
    if length(gg)==0
        fprintf(1,'\n');
        continue
    end
    kk=kk+1;
    fprintf(1,'     %d  %d points \n',kk,sum(~isnan(mask)));

    SigM=median(Sigma(gg,1));
    year(kk)=Vtime(1,1);
    month(kk)=Vtime(1,2);
    day(kk)=Vtime(1,3);
    
    kc=ceil(day(kk)/30*63);

%     subplot(2,1,1)
%     plot(Th(:,1).*mask,-P,'-','Color',CB(kc,:));
%     hold on;
%     xlabel('Pot. Temp');
%     axis([8 15 -50 0]);
%     grid on
    if month(kk)==12 | month(kk)==11 | month(kk)==1
        season=4;
    elseif month(kk)==2 | month(kk)==3 | month(kk)==4
        season=1;
    elseif month(kk)==5 | month(kk)==6 | month(kk)==7
        season=2;
    else
        season=3;
    end
    subplot(2,2,season)
    plot(Sigma(:,1).*mask-SigM,-P,'-','Color',CB(kc,:));
    hold on;grid on
    xlabel('Pot. Density');
    
    axis([-1 0.5 -80 0]);
    %title(sprintf('%s %3.0f  %3.0f',name,year(kk),month(kk)));
    title(sprintf('Season %d',season));
    
   %  pause
%     subplot(2,1,1);cla;
%     subplot(2,1,2);cla;
end
set(gcf,'Color','w');
