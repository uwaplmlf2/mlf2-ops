% Collect all Spring Beach CTD profiles
% All prefaced by /Users/ericdasaro/Documents/
% ADV ballast
%ADVfloat/Testing/Miller1203-03/Ballast/env0001.nc
kk=0;
CB=colormap;
fp=fopen('SpringBeachCTD.txt','r');
while(1)
    name=fgetl(fp);
    while(strcmp(name(1),'%')==1)
        name=fgetl(fp);
    end
    if name==-1
        fclose(fp);
        break
    end
    file=['/Users/ericdasaro/Documents/' name];
    disp(name)
    time=getnc(file,'time'); % sec ref to   Jan 1 1970 00:00:00 (GMT)
    sec=(time-time(1));   % Seconds from start of file

    Area=pi*(2*2.54/2)^2;

    % CONTROL
    P=getnc(file,'pressure');
    B=getnc(file,'piston')*Area;

    % CTD
    T=getnc(file,'temp');  % 2 values
    S=getnc(file,'sal');  % 2 values
    Sigma=sw_pden(S,T,P,0)-1000;
    Th=sw_ptmp(S,T,P,0);

    mask=ones(size(P));
    b=find(S(:,1)<10. | T(:,1)<5 | Sigma(:,1)<10);
    mask(b)=NaN;

    gp=find(diff(sec)>20);  % not profile
    mask(gp)=NaN;

    Pb=[5:10:150];
    for ib=1:length(Pb)
        gb=find(abs(P-Pb(ib))<10 & ~isnan(mask) );  % in bin
        if length(gb)>4
            [C,dum]=polyfit(P(gb),Sigma(gb,1).*mask(gb),1);
            N2(ib)=9.8*C(1)/1024;
        else
            N2(ib)=NaN;
        end
    end
        kk=kk+1;
    kc=mod(kk,63)+1;
 
    subplot(2,3,1)
    plot(Th(:,1).*mask,-P,'-','Color',CB(kc,:));
    hold on;
    xlabel('Pot. Temp');
    axis([8 15 -150 0]);
    grid on
    subplot(2,3,2)
    plot(S(:,1).*mask,-P,'-','Color',CB(kc,:));
    hold on;
    axis([26 31 -150 0]);
    xlabel('Salinity');
    grid on
    subplot(2,3,3)
    plot(Sigma(:,1).*mask,-P,'-','Color',CB(kc,:));
    hold on;grid on
    xlabel('Pot. Density');
    axis([20 24 -150 0]);
    
    subplot(2,1,2)
    gp=find(N2>0.);
    semilogx(N2(gp),-Pb(gp),'-','Color',CB(kc,:),'LineWidth',1);
    hold on;
    gm=find(N2<=0.);
    semilogx(-N2(gm),-Pb(gm),'*','Color',CB(kc,:),'LineWidth',1);
    title('N^2 - * are negative');
    axis([1e-6 1e-3 -150 0]);
    grid on
    
end
set(gcf,'Color','w');
