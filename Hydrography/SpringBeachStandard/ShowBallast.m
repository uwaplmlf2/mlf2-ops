% Collect all Spring Beach Ballasting records
% All prefaced by /Users/ericdasaro/Documents/
% 

kk=0;
CB=colormap;
figure
fp=fopen('SpringBeachCTD.txt','r');
while(1)
    name=fgetl(fp);
    while(strcmp(name(1),'%')==1)
        name=fgetl(fp);
    end
    if name==-1
        fclose(fp);
        break
    end
    file=['/Users/eric/Documents/' name];
    disp(name)
    time=getnc(file,'time'); % sec ref to   Jan 1 1970 00:00:00 (GMT)
    sec=(time-time(1));   % Seconds from start of file
    hr=sec/3600;

    Area=pi*(2*2.54/2)^2;

    % CONTROL
    P=getnc(file,'pressure');
    B=getnc(file,'piston')*Area;

    % CTD
    T=getnc(file,'temp');  % 2 values
    S=getnc(file,'sal');  % 2 values
    Sigma=sw_pden(S,T,P,0)-1000;
    Th=sw_ptmp(S,T,P,0);

    mask=ones(size(P));
    b=find(S(:,1)<10. | T(:,1)<5 | Sigma(:,1)<10);
    mask(b)=NaN;
    kk=kk+1;
    subplot(2,1,1)
    plot(hr,-P,'LineWidth',2)
    ylim([-160 0]);
    grid on;
    title(file,'interpreter','none','FontSize',10);
    subplot(2,1,2);
    plot(hr,B,'k.-','LineWidth',2);
    ylim([0 700]);
    grid on
    set(gcf,'Color','w');
    set(gcf,'position',[400   400   600   400]);
    
    in = strfind(name,'/')
    print('-dpdf',['Ballasts/' name(1:in-1) '_' num2str(kk) '.pdf']);
    clf
end
