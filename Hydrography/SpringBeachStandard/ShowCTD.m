% Collect all Spring Beach CTD profiles
% All prefaced by /Users/ericdasaro/Documents/
% ADV ballast
%ADVfloat/Testing/Miller1203-03/Ballast/env0001.nc
kk=0;
CB=colormap;
fp=fopen('SpringBeachCTD.txt','r');
while(1)
    name=fgetl(fp);
    while(strcmp(name(1),'%')==1)
        name=fgetl(fp);
    end
    if name==-1
        fclose(fp);
        break
    end
    file=['/Users/ericdasaro/Documents/' name];
    disp(name)
    time=getnc(file,'time'); % sec ref to   Jan 1 1970 00:00:00 (GMT)
    sec=(time-time(1));   % Seconds from start of file

    Area=pi*(2*2.54/2)^2;

    % CONTROL
    P=getnc(file,'pressure');
    B=getnc(file,'piston')*Area;

    % CTD
    T=getnc(file,'temp');  % 2 values
    S=getnc(file,'sal');  % 2 values
    Sigma=sw_pden(S,T,P,0)-1000;
    Th=sw_ptmp(S,T,P,0);

    mask=ones(size(P));
    b=find(S(:,1)<10. | T(:,1)<5 | Sigma(:,1)<10);
    mask(b)=NaN;

    gp=find(diff(sec)>20);  % not profile
    mask(gp)=NaN;

    kk=kk+1;
    kc=mod(kk,63)+1;

    subplot(2,1,1)
    plot(Th(:,1).*mask,-P,'-','Color',CB(kc,:));
    hold on;
    xlabel('Pot. Temp');
    axis([8 15 -50 0]);
    grid on
    subplot(2,1,2)
    plot(Sigma(:,1).*mask,-P,'-','Color',CB(kc,:));
    hold on;grid on
    xlabel('Pot. Density');
    axis([20 24 -50 0]);
    
    pause
    subplot(2,1,1);cla;
    subplot(2,1,2);cla;
end
set(gcf,'Color','w');
