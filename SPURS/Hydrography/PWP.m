function pwp_output = PWP(met, profile)
%  PWP(met, profile)
%  met fields:
%  time,		sample time in yearday
%  sw,			short wave radiation (W/m^2)
%  lw,			long wave radiation (W/m^2)
%  qlat,		latent heat flux (W/m^2)
%  qsens,		sensible heat flux (W/m^2)
%  tx,			east stress (N/m^2)
%  ty,			north stress (N/m^2)
%  precip,	precipitaion rate (m/s)
%
% profile fields:
%  z,		initial depth(m)
%  t,		initial temperature(C)
%  s,		initial salinity(o/oo)

%  The heat fluxes are defined positive for
%  sw: flux into the ocean
%  lw, qlat, qsens: flux out of the ocean

%  This main program is used to drive the pwp subroutine (pwpgo)
%  with a daily cycle of heating, or, an arbitrary (observed) time
%  series of surface flux data.

%  Written by Jim Price, april 27, 1989. Please direct any
%  comments or questions to Jim Price, WHOI, Woods Hole, MA
%  02543, USA, tel. 508-289-2526.

%  Version 2:  changed the gradient Ri relaxation method.
%  Version 3:  clarified the bulk Ri relaxation.
%  Version 4:  fixed an error that disabled background diffusion.

%  Last editted on 20 Jan, 1999 by JFP.

%  Translated for MATLAB 5.X by Peter Lazarevich and Scott Stoermer,
%  GSO/URI, 11 July 2001.


%  -- Option to display output during run. --

diagnostics = 0;

%  -- Initialize the user-defined variables. --

global dt dz days depth dt_save lat g cpw rb rg rkz beta1 beta2 udrag r0 ml_depth

%  dt,			time-step increment (seconds)
%  dz,			depth increment (meters)
%  days,  	the number of days to run
%  depth,		the depth to run
%  dt_save,	time-step increment for saving to file (multiples of dt)
%  lat,  		latitude (degrees)
%  g,				gravity (9.8 m/s^2)
%  cpw,			specific heat of water (4183.3 J/kgC)
%  rb,			critical bulk richardson number (0.65)
%  rg,			critical gradient richardson number (0.25)
%  rkz,			background diffusion (0)
%  beta1, 	longwave extinction coefficient (0.6 m)
%  beta2, 	shortwave extinction coefficient (20 m)

dt			= mean(diff(met.time))*24*60*60; % sec.
dz			= mean(diff(profile.z));
days 		= diff(minmax(met.time));
depth		= max(profile.z);

lat 		= profile.lat;
g			= 9.8;
cpw			= 4183.3;
le          = 2.5e6; % J/kg, latnet heat 
r0          = 1025; 
rb			= 0.65;
rg			= 0.25;
rkz			= 1e-5;
beta1 	= 0.6;
beta2 	= 20;

%  -- Initialize additional variables. ---

global f ucon

%  f,			Coriolis parameter
%  ucon,	coefficient of inertial-internal wave dissipation

f = 2*7.29E-5*sin(lat*pi/180);

%  -- Load the air/sea flux data. --

%  -- Interpolate the air/sea flux variables at dt resolution. --

%  Set nmet equal to the number of time increments using a resolution of dt.

time	= met.time;
n = length(time);

qi			= intnan(met.sw);
qo			= intnan(met.lw+met.qlat+met.qsens);
tx			= intnan(met.tx);
ty			= intnan(met.ty);
precip      = intnan(met.precip);

disp('loading complete')

%  -- Load initial t,s profile data. --

%  -- Interpolate the profile variables at dz resolution. --

global nz z t s d

%  Set nz equal to the number of depth increments + 1 using a resolution of dz.

z		= profile.z(:);
nz = length(z);

t	= profile.t(:);
s	= profile.s(:);
d	= sgt(t,s);

disp('loading complete')

%  -- Interpolate evaporation minus precipitaion at dt resolution. --

%evap 	= (0.03456/(86400*1000))*met.qlat;
evap = met.qlat/le/1e3; % m/s, evaporation rate
emp		= evap - precip;

%  -- Initialize additional profile variables at dz resolution. --

global u v absrb

%  u and v, 	east and north current
%  absrb,			absorbtion fraction

u			= zeros(nz,1);
v			= zeros(nz,1);
absrb = absorb(beta1,beta2);

%  -- Specify a simple "background" diffusion to be applied to the profiles. --

dstab = dt*rkz/dz^2;

if dstab > 0.5
	disp('Warning, this value of rkz will be unstable')
end

%  -- Define the variables to be saved. --

pwp_output.dt 			= dt;
pwp_output.dz 			= dz;
pwp_output.lat 			= lat;
pwp_output.z				= z;
pwp_output.time             = time;
pwp_output.t				= zeros(nz,n);
pwp_output.s				= zeros(nz,n);
pwp_output.d				= zeros(nz,n);
pwp_output.u				= zeros(nz,n);
pwp_output.v				= zeros(nz,n);
pwp_output.ml_depth		= zeros(1,n);

%  -- Step through the PWP model. --

for m = 1:n
    if mod(m,100)==0,
        disp(m)
    end
	pwpgo(qi(m),qo(m),emp(m),tx(m),ty(m),m);
 if any(isnan(t+s+d+u+v)),
     keyboard;
 end
%  Apply a "background" diffusion if rkz is non-zero.

	if rkz > 0
		diffus(dstab,t);
		diffus(dstab,s);
		d = sgt(t,s);
		diffus(dstab,u);
		diffus(dstab,v);
	end

%  -- Store variables. --
		pwp_output.t(:,m)			= t;
		pwp_output.s(:,m)			= s;
        pwp_output.d(:,m)			= d;
        pwp_output.u(:,m)			= u;
        pwp_output.v(:,m)			= v;
         pwp_output.ml_depth(m)			= ml_depth;
end

    %% diagnostics - heat and FW balance
    tanom = pwp_output.t-repmat(pwp_output.t(:,1),1,n);
    heat = sum(tanom)*dz*r0*cpw;
    q = cumsum(qi-qo)*dt;
    
    sanom = pwp_output.s-repmat(pwp_output.s(:,1),1,n);
    fw = -sum(sanom./pwp_output.s)*dz; % m
    qfw = -cumsum(emp)*dt;
    
% -------------------------------------------------------------------------


function pwpgo(qi,qo,emp,tx,ty,m)
%  This subroutine is an implementation of the Price, Weller,
%  Pinkel upper ocean model described in JGR 91, C7 8411-8427
%  July 15, 1986 (PWP). This version was coded and documented by
%  Jim Price in April 1989.

%  Edited on 20 September 1993 by JFP to allow for a critical
%  gradient Richardson number other than 1/4, and to implement a
%  different and a priori better means of achieving convergence
%  of gradient ri mixing (see subroutine stir for details).
%  The major difference is that the revised scheme gives a more
%  smoothly varying mixed layer depth over a diurnal cycle.
%  Edited on 14 December 1998 by JFP to clarify the bulk Ri
%  relaxation.

%  This model also implements an energy budget form of an
%  entrainment parameterization that is very similar to that
%  described in Price, Mooers, Van Leer, JPO 8, 4, 582-599 (and
%  references therein). This part of the model should be
%  treated as developmental only, as there are several features
%  that are arbitrary to this model (i.e., the depth of the ml
%  during times of heating can be the grid interval, dz). To
%  use this parameterization set the bulk Richardson number to
%  zero, and set em1, or em2, or em3 to non-zero. The gradient
%  Richardson number can be zero or not.

global dt dz days depth dt_save lat g cpw rb rg beta1 beta2 udrag ml_depth
global nz z t s d
global u v absrb
global f ucon r0

%  Apply heat and fresh water fluxes to the top most grid cell.

t(1) = t(1)+(qi*absrb(1)-qo)*dt./(dz*r0*cpw);
s(1) = s(1)/(1-emp*dt/dz);

%  Absorb solar radiation at depth.
t(2:nz) = t(2:nz)+qi*absrb(2:nz)*dt./(dz*r0*cpw);

%  Compute the density, and relieve static instability, if it occurs.
d = sgt(t,s);
remove_si;

%  At this point the density proifile should be statically stable.

%  Find the index of the surfacd mixed-layer right after the heat/salt fluxes.

ml_index = min(find(diff(d)>1E-4));

if isempty(ml_index)
	error_text = ['Error reached in PWP.m (line 346): the mixed layer is too deep!' sprintf('\n') ...
	'Possible reasons:' sprintf('\n') ...
	'1) tempearature inversion at surface is too strong' sprintf('\n') ...
	'2) wind is too strong' sprintf('\n') ...
	'3) evaporation is too great'];
	error([ sprintf('\n') error_text])
end

%  Get the depth of the surfacd mixed-layer.

ml_depth = z(ml_index+1);

%  Time step the momentum equation.

%  Rotate the current throughout the water column through an
%  angle equal to inertial rotation for half a time step.

ang = -f*dt/2;

rot(ang);

%  Apply the wind stress to the mixed layer as it now exists.

du = (tx/(ml_depth*d(1)))*dt;
dv = (ty/(ml_depth*d(1)))*dt;
u(1:ml_index) = u(1:ml_index)+du;
v(1:ml_index) = v(1:ml_index)+dv;

%  Apply drag to the current (this is a horrible parameterization of
%  inertial-internal wave dispersion).

if ucon > 1E-10
	u = u*(1-dt*ucon);
	v = v*(1-dt*ucon);
end

%  Rotate another half time step.

rot(ang);

%  Finished with the momentum equation for this time step.

%  Do the bulk Richardson number instability form of mixing (as in PWP).

if rb > 1E-5
	bulk_mix(ml_index)
end

%  Do the gradient Richardson number instability form of mixing.

if rg > 0
	grad_mix;
end


% -------------------------------------------------------------------------


function bulk_mix(ml_index)

global g rb
global nz z d
global u v

rvc = rb;
for j = ml_index+1:nz
	h 	= z(j);
	dd 	= (d(j)-d(1))/d(1);
	dv 	= (u(j)-u(1))^2+(v(j)-v(1))^2;
	if dv == 0
		rv = Inf;
	else
		rv = g*h*dd/dv;
	end
	if rv > rvc
		continue;% break
	else
		mix5(j); 
	end
end


% -------------------------------------------------------------------------


function grad_mix

%  This function performs the gradeint Richardson Number relaxation
%  by mixing adjacent cells just enough to bring them to a new
%  Richardson Number.

global dz g rg
global nz z t s d
global u v

rc 	= rg;

%  Compute the gradeint Richardson Number, taking care to avoid dividing by
%  zero in the mixed layer.  The numerical values of the minimum allowable
%  density and velocity differences are entirely arbitrary, and should not
%  effect the calculations (except that on some occasions they evidnetly have!)

j1 = 1;
j2 = nz-1;

while 1
	for j = j1:j2
		if j <= 0
			keyboard
		end
		dd = (d(j+1)-d(j))/d(j);
		dv = (u(j+1)-u(j))^2+(v(j+1)-v(j))^2;
		if dv == 0
			r(j) = Inf;
		else
			r(j) = g*dz*dd/dv;
		end
	end

	%  Find the smallest value of r in profile

	[rs,js] = min(r);
	
	%  Check to see whether the smallest r is critical or not.

	if rs > rc
		return
	end

	%  Mix the cells js and js+1 that had the smallest Richardson Number

	%% stir(rc,rs,js);
    % inlining "stir" function:
    
rcon 			= 0.02+(rc-rs)/2;
rnew 			= rc+rcon/5;
f 				= 1-rs/rnew;
dt				= (t(js+1)-t(js))*f/2;
t(js+1)		= t(js+1)-dt;
t(js) 			= t(js)+dt;
ds				= (s(js+1)-s(js))*f/2;
s(js+1)		= s(js+1)-ds;
s(js) 			= s(js)+ds;

% d(js:js+1)	= sgt(t(js:js+1),s(js:js+1)); % this is correct formulation
dd			= (d(js+1)-d(js))*f/2; % this is linearized
d(js+1)		= d(js+1)-dd;
d(js) 		= d(js)+dd;


du				= (u(js+1)-u(js))*f/2;
u(js+1)		= u(js+1)-du;
u(js) 			= u(js)+du;
dv				= (v(js+1)-v(js))*f/2;
v(js+1)		= v(js+1)-dv;
v(js) 			= v(js)+dv;
% end of "stir"
 %% 

	%  Recompute the Richardson Number over the part of the profile that has changed

	j1 = js-2;
	if j1 < 1
		 j1 = 1;
	end
	j2 = js+2;
	if j2 > nz-1
		 j2 = nz-1;
	end
end


% -------------------------------------------------------------------------


function a = stir(rc,r,j)

%  This subroutine mixes cells j and j+1 just enough so that
%  the Richardson number after the mixing is brought up to
%  the value rnew. In order to have this mixing process
%  converge, rnew must exceed the critical value of the
%  richardson number where mixing is presumed to start. If
%  r critical = rc = 0.25 (the nominal value), and r = 0.20, then
%  rnew = 0.3 would be reasonable. If r were smaller, then a
%  larger value of rnew - rc is used to hasten convergence.

%  This subroutine was modified by JFP in Sep 93 to allow for an
%  aribtrary rc and to achieve faster convergence.

global t s d u v

rcon 			= 0.02+(rc-r)/2;
rnew 			= rc+rcon/5;% rc+rcon/5;
f 				= 1-r/rnew;
dt				= (t(j+1)-t(j))*f/2;
t(j+1)		= t(j+1)-dt;
t(j) 			= t(j)+dt;
ds				= (s(j+1)-s(j))*f/2;
s(j+1)		= s(j+1)-ds;
s(j) 			= s(j)+ds;

% d(j:j+1)	= sgt(t(j:j+1),s(j:j+1)); % this is correct formulation
dd			= (d(j+1)-d(j))*f/2; % this is linearized
d(j+1)		= d(j+1)-dd;
d(j) 		= d(j)+dd;



du				= (u(j+1)-u(j))*f/2;
u(j+1)		= u(j+1)-du;
u(j) 			= u(j)+du;
dv				= (v(j+1)-v(j))*f/2;
v(j+1)		= v(j+1)-dv;
v(j) 			= v(j)+dv;


% -------------------------------------------------------------------------


function mix5(j)

%  This subroutine mixes the arrays t, s, u, v down to level j.

global t s d u v

t(1:j) = mean(t(1:j));
s(1:j) = mean(s(1:j));
d(1:j) = sgt(t(1:j),s(1:j));
u(1:j) = mean(u(1:j));
v(1:j) = mean(v(1:j));


% -------------------------------------------------------------------------


function rot(ang)

%  This subroutine rotates the vector (u,v) through an angle, ang

global u v

r = (u+i*v)*exp(i*ang);
u = real(r);
v = imag(r);


% -------------------------------------------------------------------------


function remove_si

%  Find and relieve static instability that may occur in the
%  density array d. This simulates free convection.
%  ml_index is the index of the depth of the surface mixed layer after adjustment,

global d

while 1
	ml_index = min(find(diff(d)<0));
	if isempty(ml_index)
		break
	end
	mix5(ml_index+1);
end


% -------------------------------------------------------------------------


function absrb = absorb(beta1,beta2)

%  Compute solar radiation absorption profile. This
%  subroutine assumes two wavelengths, and a double
%  exponential depth dependence for absorption.

%  Subscript 1 is for red, non-penetrating light, and
%  2 is for blue, penetrating light. rs1 is the fraction
%  assumed to be red.

global nz dz

rs1 = 0.6;
rs2 = 1.0-rs1;
absrb = zeros(nz,1);
z1 = (0:nz-1)*dz;
z2 = z1 + dz;
z1b1 = z1/beta1;
z2b1 = z2/beta1;
z1b2 = z1/beta2;
z2b2 = z2/beta2;
absrb = (rs1*(exp(-z1b1)-exp(-z2b1))+rs2*(exp(-z1b2)-exp(-z2b2)))';

% -------------------------------------------------------------------------


function a = diffus(dstab,a)

%  This subroutine applies a simple diffusion
%  operation to the array a. It leaves the endpoints
%  unchanged (assumes nothing about the
%  boundary conditions).

global nz

a(2:nz-1) = a(2:nz-1)+dstab*(a(1:nz-2)-2*a(2:nz-1)+a(3:nz));


% -------------------------------------------------------------------------


      
      
      function r = sgt(t,s)
    %  used to be sigma-t function...
     r = sw_dens0(s,t);