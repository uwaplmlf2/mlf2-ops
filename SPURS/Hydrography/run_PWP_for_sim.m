% run PWP simulation and prepare CTD structure for the simulator
clear
% DAY0 =0; NDAYS = 365;
% DAY0 = to_day(2009,2009,2,1); 
% DAY0 = to_day(2009,2009,6,18); 
DAY0 = to_day(2009,2009,9,15); 
%DAY0 = to_day(2009,2009,1,5); 
NDAYS = 5; 
fn_out = sprintf('pwp_%d',DAY0);


% get TS profile
f = ncload('h:\spurs\PIRATA\TAO\s20n38w_dy.cdf');

S0 = sq(f.S_41(1,1,:,DAY0));
S0(S0>100) = NaN;
ZS = f.depth(:);


f = ncload('h:\spurs\PIRATA\TAO\t20n38w_dy.cdf');
T0 = sq(f.T_20(1,1,:,DAY0));
T0(T0>100) = NaN;
ZT = f.depth(:);


pr.z = 0:150;
pr.t = interp1(ZT,T0,pr.z,'pchip');
pr.s = interp1(ZS,S0,pr.z,'pchip');
pr.lat = 20;
%%

% get meteo
f = ncload('h:\spurs\PIRATA\TAO\met20n38w_10m.cdf');
t = (f.time)*(1/60/24); % in days (10-min intervals)
id = find(within(t,DAY0+[0 NDAYS]));
ta = sq(f.AT_21(id));
rh = sq(f.RH_910(id));
sst = sq(f.T_25(id));
w = sq(f.WU_422(id) + i*f.WV_423(id));
t = t(id);


% fake pressure
bp = t*0+1014;


% get fluxes
f = ncload('h:\spurs\PIRATA\TAO\rad20n38w_2m.cdf');
t1 = (f.time(:))*(1/60/24); % in days (2 -min intervals)
sw = f.RD_495(:);
sw = bindata(t1,sw,t);



%%
A = hfbulktc(abs(w),3.5,ta,2.2,rh,2.2,bp,sst);
% A=[Hs Hl Hl_webb stress U_star T_star Q_star L zetu CD CT CQ RI]

Hs = A(:,1); % sensible heat INTO the ocean
Hl = A(:,2); % latent heat INTO the ocean
tau = A(:,4).*w./abs(w);
 

[Hsw,a] = swhf(t,2009,38,20,sw); % net shortwave radiation

Hlw = blwhf(sst,ta,rh,.8); % bulk longwave radiation into the ocean  (assume constant cloud cover of .8)


%% create meteo file for PWP
%  met fields:
met.time = t;%		sample time in yearday
met.sw = Hsw;%			short wave radiation (W/m^2)
met.lw = -Hlw;%			long wave radiation (W/m^2)
met.qlat = -Hl;%		latent heat flux (W/m^2)
met.qsens = -Hs;%		sensible heat flux (W/m^2)
met.tx = real(tau);%			east stress (N/m^2)
met.ty = imag(tau);%			north stress (N/m^2)
met.precip = t*0;%      precipitaion rate (m/s)
%%
P = PWP(met, pr);
fw = 6;
X.day = P.time;
X.S = black_filt(P.s.',fw).';
X.T = black_filt(P.t.',fw).';
X.P = P.z;
X.Th = sw_ptmp(X.S,X.T,X.P,0); 
X.Sigma=sw_dens(X.S,X.T,X.P)-1000.;   % in situ density
X.Sigma0=sw_pden(X.S,X.T,X.P,0)-1000.;   % potential density

% this is not quite accurate (reference should be the mid-point), but will
% do:
X.Pn2 = mpoint(X.P);
X.N2 = 9.81./(1000+mpoint(X.Sigma0)).*diff(X.Sigma0)./P.dz;
X.Pbin = P.dz;

np = length(X.day);
sta.lat    = 24.5020*ones(np,1);
sta.lon = -38.5*ones(np,1);
dv = datevec(X.day+datenum(2012,1,1)-1);
sta.yr = dv(:,1);
sta.mo = dv(:,2);
sta.day = dv(:,3);
sta.hr = dv(:,4);
sta.min = dv(:,5);
sta.id = dv(:,6);
X.sta = sta;

% 
% % % establish ML depth and entrainment zone
 Rt = diff(P.d.')'/P.dt; % time change
 Rz = diff(P.d)/P.dz; % vertical stratification change of density (marks the top of mixing)
% 
 iml = sum(abs(Rz)<1e-6);
 iml = max(iml,1);
 MLD_top = X.Pn2(iml );
 X.MLD = black_filt(P.ml_depth,fw);


% % %  Mixed layer T/S  vrs entrainment depth
% Hent=X.P;  % Entrainment depth
% Hml=Hent-ML.Zmlb;  % Mixed layer depth
% X.Sml=cumtrapz( Hent, X.S );
% X.Tml=cumtrapz( Hent, X.Th );
% X.Sml=X.Sml-Hent.*X.S;   % Heat/Salt content above 
% X.Tml=X.Tml-Hent.*X.Th;   % depth P
% 
% e=( 2:length(Hent) );
% deno=(Hml(e) +0.5*ML.Zmlb);
% b=find(deno==0.);
% if length(b)>0
% error('divide by zero coming- change ML & entrainment depths to fix');
% end
% X.Sml(e)=X.Sml(e)./deno + X.S(e);
% X.Tml(e)=X.Tml(e)./deno + X.T(e);
% X.Sml(1)=X.Sml(2); % used to be  X.S(1)
% X.Tml(1)=X.Tml(2);
% % X.Sml gives ML sal for Zent of X.P  or  H of X.P/(1+ML.ent_fact)
% % X.Tml is ML POTENTIAL TEMPERATURE
% % ( We have cheated and conserved Theta not T ..)
% 
% X.atg = sw_adtg(X.Sml,X.Tml,0.)% ATG to get Ptemp from temp
% ggg=find(~isnan(X.atg));
% X.atg=X.atg(ggg(1));
%%
clf
picolor(X.day,X.P,X.Sigma0);
flipy
hold on
plot(X.day,X.MLD,'w');

fplot(X.day,-met.sw/100,'y','clipping','off');
% plot(X.day,MLD_top,'g');
%%
save(fn_out,'-struct','X');

