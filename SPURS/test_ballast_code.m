% test ballasting code?
clear
load ./output/data1.mat time SAL TEMP P Ball FM  Mode Weight RHO

%%
Mtime = time;

bfile=expand_wildcard('bal000*.txt');
BF = [];
TYPE = 0;
for k=1:length(bfile),
    fp=fopen(bfile{k});
    if TYPE==0,
        %         first reading
        hdr = fgetl(fp);s1=fgetl(fp);
        
        switch hdr,
            case 'day_time,mode,mode_time,Settle.Target,Drift.Target,Ballast.Target,Ballast..V0,Ballast.Vdev,Mass, Bugs.weight',
                TYPE = 10;
            case 'day_time,mode,mode_time,Settle.Target,Drift.Target,Ballast.Target,Ballast.V0,Ballast.Vdev,Mass, Bugs.weight,Drift.Air,P,T',
                TYPE = 13;
            otherwise,
                error 'Unknown format of bal00001.txt'
        end
    end
    bf = [];
    if ~feof(fp),
        switch TYPE
            case 10,
                % 10-parameter
                Bal=fscanf(fp,'%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n',[10,inf]);
                t = Bal(1,:); % this is time since deployment
                bf.V0=Bal(7,:)/1e6;
                bf.Vdev=Bal(8,:)/1e6;
                bf.Mass = Bal(9,:)/1e3;
                bf.Mtime = t+Mtime(1); % this is not necessarily correct (assumes that ENV file starts at deployment)
                bf.SettleTarget = Bal(4,:);
                bf.DriftTarget = Bal(5,:);
                bf.BallastTarget = Bal(6,:);
                ic = find(diff(bf.Mass)  | diff(bf.V0))+1;
%                 for_each_field bf '*=*(ic)';
                
            case 13,
                % SPURS-F74 format ( 13-parameter )
                Bal=fscanf(fp,'%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n',[13,inf]);
                t = Bal(1,:); % this is time since deployment
                bf.V0=Bal(7,:)/1e6;
                bf.Vdev=Bal(8,:)/1e6;
                bf.Mass = Bal(9,:)/1e3;
                bf.Air = Bal(11,:)/1e6;
                bf.P = Bal(12,:);
                bf.T = Bal(13,:);
                bf.Mtime = t+Mtime(1); % this is not necessarily correct (assumes that ENV file starts at deployment)
                bf.SettleTarget = Bal(4,:);
                bf.DriftTarget = Bal(5,:);
                bf.BallastTarget = Bal(6,:);
                ic = find(diff(bf.Mass) | diff(bf.Air) | diff(bf.V0))+1;
%                                 for_each_field bf '*=*(ic)';
%                 
                
        end
    end
    fclose(fp);
    if ~isempty(bf),
        BF = merge_struct(BF,bf);
    end
end

%%
PAR  = BF;
v = fieldnames(PAR);
v(strcmp(v,'Mtime'))=[];
EOS = [];
EOS.Mtime = Mtime;
EOS.V0 = 62200e-6+Mtime*0;
EOS.Mass = FM.mass0+Mtime*0;
EOS.Air = FM.air+Mtime*0;
EOS.Alpha= FM.alpha+Mtime*0;
EOS.Compress= FM.chi+Mtime*0;
EOS0 = EOS;
for n=1:length(v)
    var = v{n};
    for k=1:length(PAR.Mtime)
        ii = find(Mtime>=PAR.Mtime(k));
        if ~isempty(ii),
          
            if ~isnan(PAR.(var)(k)),
                EOS.(var)(ii) = PAR.(var)(k);
            else
                PAR.(var)(k) = PAR.(var)(k-1); %  not changed, just update
            end
        end
    end
end
% %%

%%
dV = volume_anomaly(SAL,TEMP,P,Ball*1e6,EOS);
dV0 = volume_anomaly(SAL,TEMP,P,Ball*1e6,EOS0);

figure(1);
clf
subplot(3,1,1)
% plot(time,dV0*1e6,'k')
hold on
plot(time,dV*1e6)
plot(time,Weight./RHO*1e6,'r')
hold on
plot([1 1]'*BF.Mtime(ic),repmat(ylim',1,length(ic)),'m')
% plot(BF.Mtime,BF.Vdev*1e6,'g.');
datetick x keeplimits
ylabel('\DeltaV (cc)');
ylim([-1 1]*2)
hg
legend('Float','Weight/\rho');

subplot(3,1,2)
plot(time,Ball*1e6)
hold on
plot([1 1]'*BF.Mtime(ic),repmat(ylim',1,length(ic)),'m')
datetick x keeplimits
ylabel('B (cc)');

subplot(3,1,3)
cplot(time,P,Mode,'.')
hold on
plot([1 1]'*BF.Mtime(ic),repmat(ylim',1,length(ic)),'m')
plot(BF.Mtime,BF.P,'o');
datetick x keeplimits
ylabel('P (dbar)');
flipy
linkaxes(get(gcf,'children'),'x');