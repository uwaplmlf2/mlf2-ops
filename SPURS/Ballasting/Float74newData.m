% FLOAT 74 BALLASTING DATA
% PUGET SOUND AUGUST , 2012
% Adjusted for float modification based on lab calculations

% float 74 ballasting file
file='/Users/eric/Documents/SPURS/Floats/Testing/PugetSound/Float74 SPURS/DATA/env0004.nc';

% Use
% Air 13.5 cc    Chi 3.15e-06 db^-1  Drift.Thermal_exp=7.36e-5
% 74
% All parts as ballasted in Puget sound. 63456
% brass added  395
% FYI drogue weights 472,480,477

Ball.flag=0.;
Ball.lead=0./1000;      % Exterior lead during ballast
Ball.brass= 395./1000.;  % Exterior Brass during ballast
Mass0=63.456-Ball.lead-Ball.brass;  % float weight without above, but with drop weight

% Total weight & Volume at time of ballasting
MassW=Mass0+Ball.lead+Ball.brass;

Vol0=62023.5e-6;  % m^3  volume at P=0, Bocha=0, T=Tref, No Air
				%from GetV0.m program
                
% Things added to change weight and volume to correct values
%  Lead is automatically removed
%  NB volume is automatically computed
Ball.NB= 0.769 - 0.3;   %626./1000.;     % Exterior Naval Brass
Ball.SS= 0/1000.;	           % Exterior SS weight

Ball.Weight= 493.87/1000.; % Weight change (extra battery, camera assy, and paint)
Ball.Vol= 123.03e-6;  % Volume change 

Soffset=0.;  % Salinity offset

%*********PARAMETERS ***********
rhoair=0.0012*1000;
rhoSS=8.0272*1000;
rhoNB=8.41469*1000.;  % Naval Brass
rhow=1.021*1000;
rholead=11340;

Air =15.5e-6;%13.5e-6; 	% air trapped to float at surface m^3
Patm=10.;
Compress= 3.1e-6;%3.15e-6; %HURR08- compressibility of float  db^-1 */ 
Alpha = 7.22e-5;%7.36E-05; 	% thermal expansion of float K^-1 */
Tref=8;
%***************************