% build the simulator...
clear  mex
od = pwd;
cd SPURS_c
try
%     mex -v ballast.c sw.c
    mex  -g ballast.c sw.c ptable_mex.c
    movefile *.mexw64 ..
catch ME
    cd(od)
    ME.throw;
end
cd(od);
