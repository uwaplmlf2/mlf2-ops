function P=xml2ptable_flat(fn)
% XML2PTABLE - get parameter table(s) from params.xml
% no syntax checks at all!  
% assumes a single ptable entry (or none at all)
% P{:,1} are the names
% P{:,2} are the values
f = fopen(fn,'rt');
P = [];
k = 0; % index
if f<0,
    return
end
while ~feof(f),
    s = fgets(f);
    s(s=='''') = '"';
    v = textscan(s,'%q','delimiter','<> =/','MultipleDelimsAsOne',1);
    v = v{:};
    if isempty(v),
        continue;   
    end
    
    switch v{1}
        case 'param', % <param
            % read and append...
            k = k+1;
            P{k,1} = v{find(strcmp(v,'name'),1)+1};
            
            idd = find(strcmp(v,'value'),1)+1;
            if ~isempty(idd)
                % there is a "value=" property -- take it!
                P{k,2} = str2num(v{idd}); 
            else
                % we must have the <param>value</param> syntaxis
                idd = find(strcmp(v,'param'),1,'last')-1;
                if ~isempty(idd)
                 P{k,2} = str2num(v{idd});
                else
                    error(sprintf('Bax XML line: %s\n',s));
                end
            end
        otherwise,
            % </ptable or smth.
    end
end
fclose(f);
% sort alphabetically?
[xx,is] = sort(P(:,1));
P = P(is,:);
end