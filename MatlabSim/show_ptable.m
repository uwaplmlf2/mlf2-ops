function show_ptable(P)
% show Ptable in grid editor


%     display property grid
figure( ...
    'MenuBar', 'none', ...
    'Name', 'Ptable', ...
    'NumberTitle', 'off', ...
    'Toolbar', 'none',...
    'units','pixels');
p = get(gcf,'position');
w = 300;
h = 800;
set(gcf,'position',[p(1)+p(3)/2-w/2 p(2)+p(4)-h w h]);
%%
% grid
% Initialize JIDE's usage within Matlab
com.mathworks.mwswing.MJUtilities.initJIDE;

% Prepare the properties list
list = java.util.ArrayList();

for k=1:size(P,1)
    name = P{k,1};
    value = num2str(P{k,2});
    id = find(name=='.',1);
    if ~isempty(id),
        cat = name(1:id-1);
    else
        cat = '.';
    end
    pr(k) = com.jidesoft.grid.DefaultProperty();
    pr(k).setName(name);
    pr(k).setType(java.lang.Class.forName('java.lang.String'));
    pr(k).setValue(value);
    pr(k).setCategory(cat);
    pr(k).setDisplayName(name);
%    pr(k).setDescription(['[',value,']',10,P.description{k}]);
    pr(k).setEditable(true);
    list.add(pr(k));
end

% Prepare a properties table containing the list
model = com.jidesoft.grid.PropertyTableModel(list);
model.expandAll();
grid = com.jidesoft.grid.PropertyTable(model);
pane = com.jidesoft.grid.PropertyPane(grid);
pane.setShowDescription(0); % no description in this representation of Ptable

% Display the properties pane onscreen
panel = uipanel(gcf);
javacomponent(pane, [0 0 w h], panel);
grid.setSelectedProperty(pr(1)); %% select the first row

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function OnKeyPressed(obj, event)
    end


    
end
