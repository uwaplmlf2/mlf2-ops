function EOS = Read_ballast_file(file)
% READ_BALLAST_FILE - read LF ballst from balXXXXX.txt file
%   EOS = Read_ballast_file(file) returns the EOS structure.
% Note that some of the EOS values (the ones that don't change, e.g. Tref)
% are not in the ballast file - but we can get them from params.xml
%

% Jul 2016 AS

%%
[pth,fn,ext] = fileparts(file);
EOS = [];

% get params.xml first
fn_param = fullfile(pth,'params.xml');
PT = xml2ptable_flat(fn_param);
if isempty(PT)
    fprintf('Warning: %s not found.\n',fn_param);
else
    for k=1:size(PT,1)
        if strncmp(PT{k,1},'EOS',3),
            eval([PT{k,1},'=',num2str(PT{k,2}),';']);
        end
    end
end

% next, get mission.xml
fn_param = fullfile(pth,'mission.xml');
PT = xml2ptable_flat(fn_param);
if isempty(PT)
    fprintf('Warning: %s not found.\n',fn_param);
else
    for k=1:size(PT,1)
        if strncmp(PT{k,1},'EOS',3),
            eval([PT{k,1},'=',num2str(PT{k,2}),';']);
        end
    end
end


f = fopen(file,'rt');
% skip the header(s)
h = fgets(f);
% get field names
Names = textscan(h,'%s','delimiter',',','CollectOutput',1);
Names = Names{1};
n = length(Names);

% Format:
% RealTimeClock[s], mode, mode_time[s], EOS.mass[g], EOS.V0[cc], EOS.Air[cc], EOS.Compress, EOS.Thermal_exp, B_Neutral[cc], Target
A = textscan(f,'%f','delimiter',',','commentstyle',Names{1},'CollectOutput',1);
A = A{1};
fclose(f);

% reshape
A = reshape(A,n,[]).';

% first row of A will contain scalings - remove those
scale = A(1,:);
out = all(bsxfun(@minus,A,scale)==0,2);
A(out,:) = [];

% apply scalings to bring everything to SI

% Scaling line is not yet correct - hardwire it
scale = [1 1 1 1e3 1e6 1e6 1e6 1e6 1e6 1];
A = bsxfun(@rdivide,A,scale);

% make the structure
for k=1:n
    nm = Names{k};
    % get rid of extra stuff
    i1 = find(nm=='.');
    if ~isempty(i1)
        nm = nm(i1+1:end);
    end
    i1 = find(nm=='[');
    if ~isempty(i1)
        nm = nm(1:i1-1);
    end
    EOS.(nm) = A(:,k);
    
end
EOS.Mtime = EOS.RealTimeClock/86400+datenum(1970,1,1);
EOS.Names = Names;

if nargout==0
    % plot some
    figure(1)
    clf
    plot(EOS.Mtime, EOS.V0*1e6,'.-');
    ylabel('EOS.V0 (cc)');
    datetick x keeplimits
end


