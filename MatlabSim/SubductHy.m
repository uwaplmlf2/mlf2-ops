function [Hy,SD]=SubductHy(day_time,Hy,SD,ML)
% Change T S P profile to simulate subduction 
recompute=0;

if SD.flag==1  % initial mix on first call
    SD.flag=2;
    fprintf(1,'Subduct Mix to %4.1fm\n',SD.ML0);
    if SD.ML0>0
        [Zmix,Smix] =Entrain(Hy.P,Hy.S,SD.ML0,SD.Zmlb0);
        [Zmix,Thmix]=Entrain(Hy.P,Hy.Th,SD.ML0,SD.Zmlb0);
        Hy.S=Smix;
        Hy.Th=Thmix;
        Hy.P=Zmix;
        Zmlb=SD.Zmlb0;
        recompute=1;
    end
    
end

% "Open the door" by deepening mixed layer
if SD.flag==2 & day_time>SD.Timestart
    SD.flag=3;
    fprintf(1,'Subduct Open door: ML to %4.1fm TL to %4.1f\n',SD.MLs,SD.MLs+SD.Zmlbs);
    
    gmix=find(Hy.P<=SD.MLs+SD.Zmlbs); % redo this segment
    Hy.S(gmix)  =interp1([0 SD.MLs SD.MLs+SD.Zmlbs],[Hy.S(1)  Hy.S(1)  Hy.S(gmix(end)) ],Hy.P(gmix) );
    Hy.Th(gmix)= interp1([0 SD.MLs SD.MLs+SD.Zmlbs],[Hy.Th(1) Hy.Th(1) Hy.Th(gmix(end))], Hy.P(gmix) );  
    Zmlb=SD.Zmlbs;
    recompute=1;
end

% "Close the door" by freshening ML
if SD.flag==3 & day_time>SD.Timestop
    SD.flag=4;
    fprintf(1,'Subduct Close door: freshen by %6.3fpsu to %4.1fm TL to %4.1f\n',SD.dS,SD.ML0,SD.ML0+SD.Zmlb0);
    gmix=find(Hy.P<=SD.ML0+SD.Zmlb0); % redo this segment
    gmix=find(Hy.P<=SD.MLs); % redo this segment
    Hy.S(gmix)  =interp1([0              SD.ML0        SD.ML0+SD.Zmlb0           SD.MLs        ],...
                         [Hy.S(1)-SD.dS Hy.S(1)-SD.dS  Hy.S(gmix(end))-SD.dSml Hy.S(gmix(end))],Hy.P(gmix) );
    Zmlb=SD.Zmlb0;
    recompute=1;
end

% Fix up other quantities if necessary
if recompute
    Hy.T=sw_temp(Hy.S,Hy.Th,Hy.P,0.);
    Smix=Hy.P*NaN;Tmix=Smix;
    for i=2:length(Hy.P)
        [Zmix,Smix0]=Entrain(Hy.P,Hy.S,Hy.P(i),Zmlb);
        [Zmix,Tmix0]=Entrain(Hy.P,Hy.T,Hy.P(i),Zmlb);
        Smix(i)=Smix0(1);
        Tmix(i)=Tmix0(1);
    end
    Smix(1)=Smix(2);Tmix(1)=Tmix(2);
    Hy.Sml=Smix;
    Hy.Tml=Tmix;
end


% % This only matters for plot- keep initial value
% [Hy.N2,vort,Hy.Pn2] = sw_bfrq(Hy.S,Hy.T,Hy.P); % N^2 and Pgrid ( no Fine Str)
% Hy.N2 = max(Hy.N2,0);
