% Read output files
clear all
D=dir('Output/Data*.mat');

for iii=1:length(D)
        load(['Output/' D(iii).name]);

    plot(time,Weight); hold on
    g=find(Mode==FM.ML);
    plot(time(g),Weight(g),'ro');
    axis([0 3.5 -0.01 0.01]);
    grid on
    pause
    clf
end



% for Subduct2.pdf
return

col='rgbmck';
kkk=0;
for iii=[20 35 36  3 21 24] %1:length(D)
    kkk=kkk+1;
    load(['Output/' D(iii).name]);
    g=find(Mode==FM.ML);
    MLweight=median(Weight(g));
    g=find(~isnan(Weight));
    b=find(isnan(Weight));
    Weight(b)=0.;
    Wav=filtfilt(B,A,Weight);
   
    subplot(2,1,1)
    plot(time,-P,'Color',col(kkk)); hold on
    %plot(time,-H,'b--',time,-H-ML.Ztent,'b-.')
    subplot(2,1,2)
    %plot(time,Weight,'g.','MarkerSize',2,'Color',col(kkk) )
    hold on
    plot(time,Wav,'-','Color',col(kkk) )
    hold on
    axis([0 3.5 -0.01 0.02]);

    grid on
    
end