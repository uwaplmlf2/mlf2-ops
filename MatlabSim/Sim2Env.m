% Convert EQSim output to float output format

load 'Output/Data1.mat' P SAL TEMP Mode Day Drogue Ball
yd=Day;
B=Ball;
S=SAL;
T=TEMP;
Th=sw_ptmp(S,T,P,0.);
Sig0=sw_pden(S,T,P,0.);
drogue=Drogue;
mode=Mode;
save 'EnvSim.mat' yd S T P B Th Sig0 drogue mode
