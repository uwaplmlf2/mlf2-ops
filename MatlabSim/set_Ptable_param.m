function Ptable = set_Ptable_param(Ptable,name,v)
% SET_PTABLE_PARAM - set a parameter in PTable
%   set_Ptable_param(Ptable,name,value) sets the value of parameter "name" in the
%   Ptable
%   Ptable is a [nx2] cell array returned by ballast():
%   {'name1', value;
%    'name2', value2;...}
%   It is a mirror of the floats' Ptable.

id = find(strcmpi(Ptable(:,1),name));
if isempty(id),
    warning(sprintf('Ptable entry "%s" not found',name));
else
    Ptable{id,2} = v;
end