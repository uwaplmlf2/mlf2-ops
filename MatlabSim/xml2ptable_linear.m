function P=xml2ptable_linear(fn)
% XML2PTABLE - get parameter table(s) from params.xml
% no syntax checks at all!
% assumes a single ptable entry (or none at all)
f = fopen(fn,'rt');
P = [];
k = 0; % index
if f<0,
    return
end
while ~feof(f),
    s = fgets(f);
    s(s=='''') = '"';
    v = textscan(s,'%q','delimiter','<> =','MultipleDelimsAsOne',1);
    v = v{:};
    if isempty(v),
        continue;
    end
    
    switch v{1}
        case 'param', % <param
            % read and append...
            k = k+1;
            P.name{k} = v{find(strcmp(v,'name'),1)+1};
            
            P.val{k} = v{end-1};
            
            idd = find(strcmp(v,'hint'),1)+1;
            if ~isempty(idd),
                P.description{k} = v{idd};
            else
                P.description{k} = '';
            end
            idd = find(strcmp(v,'type'),1)+1;
            if ~isempty(idd),
                P.type{k} = v{idd};
            else
                P.type{k} = '';
            end
        otherwise,
            % </ptable or smth.
    end
end
fclose(f);
end