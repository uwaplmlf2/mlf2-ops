function W=Weight2W(Cd,Cw,N,Area,Weight)
% W=Weight2W(Cd,Cw,N,Area,Weight)
% Uses float drag law to get vertical velocity from buoyancy
% Finds roots of the equation
%  Cd rho A W^2 + Cw rho A r N W - g Weight=0
% Code from floatsim2.m with some Mods
        rho=1024;g=9.8;
        PC(1)=1;   % W^2
        PC(2)=Cw/Cd*N*sqrt(Area/pi);   % N sqrt(A) * W
        PC(3)= -g*abs(Weight)/Cd/rho/Area;
        
        Wroot=roots(PC);
        pos=find(imag(Wroot)==0 & Wroot>=0);
        
        if length(pos)==0
            error('No drag law root');
        end
        
        
        W=Wroot(pos)*sign(Weight);
        % end 
