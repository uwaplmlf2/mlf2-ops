function p = get_Ptable_param(Ptable,name)
% GET_PTABLE_PARAM - return a parameter from PTable
%   p=get_Ptable_param(Ptable,name) returns the value of parameter "name" from the
%   Ptable
%   Ptable is a [nx2] cell array returned by ballast():
%   {'name1', value;
%    'name2', value2;...}
%   It is a mirror of the floats' Ptable.

id = find(strcmpi(Ptable(:,1),name));
if isempty(id),
    p = NaN;
else
    p = Ptable{id,2};
end