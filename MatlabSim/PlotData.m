%% plot start here
disp('OBSOLETE- Use localized PlotSimData');
    Zmax=150;
    orange=[1 0.75 0]; brown=[0.8 0.4 0.4];
    darkred=[0.7 0 0];purple=[0.8 0 0.6];darkblue=[0 0 0.7];
    darkgreen=[0 0.8 0];bluegreen=[0 0.9 0.5];

    adt=axes('position',[0.20 0.55 0.78 0.4]);  % Depth time
    awt=axes('position',[0.20 0.05 0.78 0.4]);  % Weight time
    asd=axes('position',[0.01 0.55 0.13 0.4]); % Sigma depth
    awd=axes('position',[0.01 0.05 0.13 0.4]); % Weight depth    
%%    
    axes(adt)   % Depth plot
    dl=ceil(1e5/length(H));
    l=[1:dl:length(H)];
    
    hh=plot(time,-H,'b--',time,-H-ML.Turb_ent,'b-.');
    hh=hh(1);
    hold on
    plot(time,-H-ML.Zmlb,'-.','Color',darkblue);
    hz=plot(time,-Zeta-80,'k--');

    hz=plot(time,-Z,'g','LineWidth',0.5);
    hp=plot(time,-P,'r','LineWidth',2,'Color',purple);
    
    dcl=find(Drogue==0);  % Drogue
    tl=time(dcl);pl=P(dcl);
    le=find(diff(dcl)>1);
    tl(le)=NaN;
    plot(tl,-pl,'Color',orange,'LineWidth',2);
    hB=plot(time,-ZB,'Color',brown,'LineWidth',2);
    
    dcld=find(Drogue==0 & (Mode==FM.ML | Mode==FM.SEEK | Mode==FM.ISO));
    plot(time(dcld),-P(dcld),'o','Color',orange,'LineWidth',3,'MarkerSize',3);

    
    mm=find(diff(Mode)~=0 & ~isnan(diff(Mode)) );
    Pb=P; Wb=Weightav;
    Pb(mm+1)=NaN;Wb(mm+1)=NaN;
    gm=find(Mode==FM.ML);
    plot(time(gm),-Pb(gm),'k','LineWidth',2,'Color',darkred);
    gs=find(Mode==FM.ISO);
    plot(time(gs),-Pb(gs),'k','LineWidth',2,'Color','r');

    if modelabel==1        
        for i=1:length(mm)
            k=mm(i);
            s=smode(Mode(k+1)+1);
            ht=text(time(k),-P(k)+(Mode(k)-3),s);
            set(ht,'FontSize',7);
            hold on
        end
    end
    
    a=axis;
    axis([a(1) a(2) -Zmax 0]);
    
    U10=ones(size(sec))*ML.U0;
    st=find(sec/86400.>ML.storm_start &...
        sec/86400.<ML.storm_start+ML.storm_duration );
    U10(st)=ML.Ustorm;
    
    % 		hw=plot(Day,U10-120,'LineWidth',2,'Color',[0.5 0.5 0.5]);
    % 		text(ML.storm_start*86400,-115,'U10','FontWeight','Bold',...
    % 		'HorizontalAlignment','Right');
    
    if nolegend~=1
        %legend([hh hp hz hB ],'Mixed Layer','Pfloat','Zfloat','Bottom',4);
        legend([hh hp hz],'Mixed Layer','Pfloat','Zfloat');
    end
    title(['Float Simulation ' num2str(real) '   '  date])
    grid on
    set(gcf,'Color','w');
    a=axis;
    
    axes(asd)    % DENSITY VRS DEPTH
    plot(Hy.Sigma0,-Hy.P,'k--');
    hold on
    plot(SIGMA0,-P,'r');
    axis tight
    b=axis;
    axis([b(1) b(2) a(3) a(4)]);
    set(gca,'YtickLabel','');
    xlabel('Sigma');
    grid on;axis manual;
    plot(b(1)+sqrt(Hy.N2)*(b(2)-b(1))/0.04,-Hy.Pn2,'k','LineWidth',2);
    for it=0:0.25:1
        xx=b(1)+it*(b(2)-b(1));
        plot([xx xx],[-5 0],'k');
    end
    title('\sigma 0 & N2 [0 0.04]');

%%
    axes(awt)   % Ballast plot
    hw=plot(time,Weightav*1e3,'r');
    hold on
    plot(time(gm),Wb(gm)*1e3,'k','LineWidth',2,'Color',darkred);
    hww=plot(time,Weightav*1e5,'g.','MarkerSize',4);
    plot(time(gm),Wb(gm)*1e5,'g.','MarkerSize',4,'Color',darkgreen);
    plot(time(gs),Wb(gs)*1e5,'g.','MarkerSize',4,'Color',bluegreen);

    %plot(time,Weightav*1e5,'.','Color',[0. 0.4 0],'MarkerSize',6);  % Dot weight
    gnn=find(~isnan(SIGMA0));
    hs=plot(time,(SIGMA0-median(SIGMA0(gnn)) )*1000,'m');
    hb=plot(time,Ball*1.e6,'k','LineWidth',2);
    bl=Ball(dcl)*1.e6;
    plot(tl,bl,'Color',[0  0 1],'LineWidth',2);
    plot(time(dcld),Ball(dcld)*1e6,'bo','LineWidth',3,'MarkerSize',3);

    if modelabel==1
        mm=find(diff(Mode)~=0 & ~isnan(diff(Mode)) );
        for i=1:length(mm)
            k=mm(i);
            s=smode(Mode(k+1)+1);
            ht=text(time(k),(Mode(k)-3),s);
            set(ht,'FontSize',7);
            hold on
        end
    end


    
    axis([a(1) a(2) -200 400]);
    a=axis;
    grid on
    
    if nolegend~=1
        legend([hb hw hww hs],...
            'Ballast','Weight','Weight*100','(\sigma_0-Mean)*1000');
    end
    set(gcf,'Color','w');
    zoom on
    title(sprintf('CTD # %d %d/%d/%d   %6.3f N  %8.3f W'...
        ,cast,sta.mo(cast),sta.day(cast),sta.yr(cast),...
        sta.lat(cast),abs(sta.lon(cast)) )  );
    
    axes(awd)
    plot(Weightav*1000,P,'g.');
    axis([-5 5 0 150]);
    grid on;
    set(gca,'Yaxislocation','right','Ydir','reverse')
    xlabel('Weight/g');
    title('Weight vrs Depth');
