function  [Day, P, Z, Weight, Zeta, H,  RHO,TEMP,SAL, Hy,ML,Fl,FM,Ball,B0,Mode,ZB,Drogue,Home,State]...
    =mlf2sim(seed)
%      mlf2simsim.m 
% Simulates motion of Lagrangian float in convective ml
% with underlying waves, tides and eddies
%
% version for MLF2
% adapted from floatsim.m  Sept, 1999
%
% calls ballast() - actual DLF ballasting routine
%
% DATA STRUCTURE
%  FL.?  - float variables
%        P,ballast, area, mass_offset
%        S,T,sigma,sigma0, buoyancy
%
% FM.?  - float Model parameters  (see  in SimParam)
%
% ML.?  -  mixed layer properties  (see SimParam)
%
% BT.?  - bottom depth properties (see SimParam);
%
% IW.? -  internal wave and eddy properties (see SimParam)
%   Fl.zeta = Zdisp(day,IW,Fl.Z)    displacement of isopycnals by IW,eddies & upwelling
%   Fl.Z -  position of float for flat isopycnals
%         dFl.Z/dt = Wturb + Wfloat
%   Fl.P=Fl.Z+Fl.zeta  - actual float depth
% Z & P both positive down

global Pi Si Thi  SimParamName mldcast homing FM
persistent Bsave bcycle  bcycleMax  % saves last bycleMax values of Ballast

tic  % start clock
clear ballast % unload ballast() MEX function

% MISSION.XML
% mpt = xml2ptable_linear('mission.xml');
% if ~isempty(mpt),
%     fprintf(1,'*** Using Mission.XML Ptable ***\n');
%     MissionPtable = [mpt.name',cellfun(@str2num,mpt.val','UniformOutput',false)]; % reformat to a cell array
%     disp(MissionPtable);
%     fprintf(1,'*********************************\n');
% else
%     MissionPtable = {};
% end
MissionPtable = xml2ptable_flat('mission.xml');
disp(MissionPtable);
fprintf(1,'*********************************\n');

% obtain Ptable so that we could use it in SimParam
% Also push Mission.xml prameters to ballast (they will stick).
[~,~,~,~,Ptable] = ballast(0,0,0,0,0,0,0,-10,0,0,0,MissionPtable); % use mode==-10 to skip everything!

run(SimParamName)      % set up all simulation parameters

Initialize     % Do initialization computations

% output variables
nprof=(FM.end_time-FM.launchtime)/Prof_interval;
Nout=ceil(nprof*3600/FM.time_step_sec(FM.DOWN+1) + (FM.end_time-FM.launchtime)*86400/FM.time_step_sec(FM.ISO+1))+2;
Nout=Nout*2;
fprintf(1,'Guess %f Profiles, %f data points\n',nprof,Nout)
Day=zeros(1,Nout)*NaN;
P=Day;Z=Day;Weight=Day;Zeta=Day;
H=Day; RHO=zeros(1,Nout);TEMP=Day;SAL=Day;
Ball=Day; Mode=Day;ZB=Day;Drogue=Day;B0=Day;Home=Day;
State=Day;

iout=0;
ijump=0;  % time jump flag
day_plot=0;
icycle=0; % cycled CTD counter
Wbsave= NaN;%f

Fl.commode=0;  % Keeps track of communications mode actions
Fl.commstart=-1;  % start time of communications mode
Fl.Icom= -1;
FM.do_emergency=1.;

Soff=0;
day_time=FM.launchtime;
secOday=(day_time-floor(day_time))*86400;
drogue=0;   % closed at launch
mode= FM.START;     % Start mode first
last_ballast=-999;  % last ballast value in drift mode
Fl.ballast=0.;
Ptable_in = {}; % this will oabe used to simulate parameter changes (and errors)
% INITIALIZATION
% Call ballast routine to initialize
command=0;


if FM.SafeBallast==1   % SAFE BALLAST
    [ballast_goal,mode_out,drogue_out]...   % float control program
        =SafeBallast(day_time,0.,Fl.ballast,mode);  % C subrountine
else
    [ballast_goal,mode_out,drogue_out,telem_out,Ptable]...
        =ballast(day_time,0.,0.,0.,0.,0.,Fl.ballast,mode,drogue,secOday,command);  % C subrountine
end
mode=mode_out;

ML.P=mld(day_time,ML);  % Get ML depth
ML.Z=ML.P+ Zup(day_time,IW);  % assumes eddies and IW have no displacement at t=0...
[Fl.weight, Fl.Wb]=floatmodel2(day_time,Fl.Z,Fl.P,Fl,FM,Hy,ML);
[rho Fl.T Fl.S]=HyP2(Fl.Z,Fl.P,ML,Hy);
if (FM.LABSIMTEST==1)
    [Psim,Tsim,Ssim]=labsimtest(day_time,mode,Fl.T,Fl.S,Fl.P,Fl.ballast,FM.diag);
    Fl.P=Psim;Fl.S=Ssim;Fl.T=Tsim;
    rho=sw_dens(Fl.S,Fl.T,Fl.P);
    MLD0=ML.P;
end

while(day_time<FM.end_time)    			% MAIN LOOP
    % time step?
    time_step_sec = FM.time_step_sec(mode+1);
    time_step_day=time_step_sec/86400.;     % time step in days
    
    if mode==FM.ERR  % ERROR
%         time_step_sec=FM.time_step_sec(FM.COMM+1);
%         mode=FM.COMM;  % Jump to Comm
%         Fl.commode=0;
%         disp('>>ERROR Declared - surface in COMM')
        disp('>>ERROR Declared - mission code will mitigate')  
        %     elseif mode==FM.XFER  % AUX file transfer
        %          time_step_sec=FM.time_step_sec(FM.XFER+1);
    end
    if mode==FM.DONE
        disp('>>DONE!!')
        break;
    end
    if mode==FM.START
        disp('>>(Re) starting a mode')
    end
    
    
    % Surface heating/mixing
    %Hy=Flux(Hy,ML,day_time,time_step_sec);
    
    % SUBDUCTION T S profile changes
    if ismember(SD.flag,[2 3 4])
        [Hy,SD]=SubductHy(day_time,Hy,SD);
    end
    
    % CTD data actually measured
    Ztop=max(0.,Fl.Z-FM.ctdZ);Ptop=max(0.,Fl.P-FM.ctdZ);  % keep top CTD in water
    [dum Ttop Stop]=HyP2(Ztop,Ptop,ML,Hy);
    [dum Tbot Sbot]=HyP2(Fl.Z+FM.ctdZ,Fl.P+FM.ctdZ,ML,Hy);
    
    if (FM.LABSIMTEST==1)
        [Psim,Tsim,Ssim]=labsimtest(day_time,mode,Fl.T,Fl.S,Fl.P,Fl.ballast,FM.diag);
        Fl.P=Psim;
        Ttop=Tsim+0.001;Tbot=Tsim;   %perhaps not quite right
        Stop=Ssim;Sbot=Ssim;
        %disp([day_time Fl.P Fl.S Fl.T ])
    end
    
    % Correct pressure to Sensor position
    Pin=Fl.P-FM.Poffset;
    
    % Simulated CTD jump
    if day_time>IW.badCTD_time
        if IW.badCTD_which==1
            Stop=Stop+IW.badCTD_psu;
        elseif IW.badCTD_which== -1
            Sbot=Sbot+IW.badCTD_psu;
        end
    end
    
    % Simulated CTD zero
    if abs(day_time-IW.CTDZeroTime)<IW.CTDZeroDuration
        Stop=0;Ttop=0.;
        Sbot=0;Tbot=0.;
    end
    if  rand(1,1)<IW.CTDZeroProbT
        Stop=0.;Ttop=0.;
    end
    if  rand(1,1)<IW.CTDZeroProbB
        Sbot=0.;Tbot=0.;
    end

%     % Simulate Profiling CTD-  Top CTD is set to zero
    % {'Stage','Phase(icall)','Mode','Variation'};
    state = get_Ptable_param(Ptable,'State'); % obtaind State from the float's Ptable

    k=1;Sstage = floor(mod(state,256^(5-k))/(256^(4-k)));
    k=2;Sphase = floor(mod(state,256^(5-k))/(256^(4-k)));
    k=3;Smode = floor(mod(state,256^(5-k))/(256^(4-k)));
    k=4;Svar = floor(mod(state,256^(5-k))/(256^(4-k)));
%     if ~isnan(state)
%         fprintf(1,'Stage %d  Phase %d Mode %d Var %d\n',Sstage,Sphase,Smode,Svar);
%     end
    %  state for fast profile:  e.g. FM.ctdprof_state=[1 1 2 0];
    if (Sstage==FM.ctdprof_state(1) && Sphase==FM.ctdprof_state(2)... 
        && Smode==FM.ctdprof_state(3) && Svar==FM.ctdprof_state(4) )
        Stop=0;
    end

    % Air in top CTD - but not in labsimtest
    if Fl.P < FM.PsurfaceAir && FM.LABSIMTEST~=1
        Stop=Stop*rand(1,1);
    end
    
    if rand>FM.Alt_bad_fraction
        Altin=bottom(day_time,BT,IW)-Pin;  % Altitude above bottom
        if Altin<=0
            Altin=1;  % float on bottom gives small reading
        end
    else
        Altin=FM.Alt_bad_value;
    end
    
    if (day_time>IW.spike_time)    % Give it a weight jolt
        FM.mass0=FM.mass0+IW.spike_dM;
        IW.spike_time=1e20;     % no more spikes
    end
    
    % Tidal Strain noise in CTD
    % Soff=IW.Tstrain*sin(2*pi/0.5*day_time);
    
    
    % Periodic weight change due to "Bugs"
    if FM.bugweight_on>0,
        bugt=[FM.bugs_on_sec-1 FM.bugs_on_sec FM.bugs_off_sec FM.bugs_off_sec+1];
        bugw=[0. FM.bugweight_on FM.bugweight_off 0.];
        bugt=[bugt-86400 bugt bugt+86400];
        bugw=[bugw bugw bugw];
        [bugt is]=sort(bugt);
        bugw=bugw(is);
        FM.bugmass=interp1(bugt,bugw,secOday);
    else
        FM.bugmass = 0;
    end
    
    % Simulate errors
    FM.errorcode = 0;
    % clear error_code potentially present in Ptable_in
    if ~isempty(Ptable_in)
        ie = find(strcmp(Ptable_in(:,1), 'error_code'));
        Ptable_in(ie,:) = [];
    end
    
    if (ierror <= length(FM.Error_Time) && day_time>FM.Error_Time(ierror) )
        fprintf(1,'>>%4.2f  Mode %d Issue error %d\n',day_time,mode,FM.Error_code(ierror));
        Ptable_in(end+1,:) = {'error_code', FM.Error_code(ierror)}; % no need to re-send MissionPtable!
        mode=FM.ERR;
        ierror = ierror+1;
    end
    
    %     if(rand(1)<FM.Prob_emergency | ...
    %             ( day_time>FM.Time_emergency & FM.do_emergency==1) )
    %         fprintf(1,'>>%4.2f Simulated Random EMERGENCY  Mode %d -> %d\n',day_time,mode,FM.ERR);
    %         mode=FM.ERR;
    %         Fl.commode=0.;
    %         FM.do_emergency=0;
    %     end
    
    % Simulate acoustic commands
    command = 0;
    if icommand<=length(FM.Command_Time) && day_time>FM.Command_Time(icommand),
        command=FM.Command(icommand);
        icommand=icommand+1;
        fprintf(1,'>>%4.2f  Mode %d Issue command %d\n',day_time,mode,command);
    end
    
    if (mode==FM.COMM & Fl.commode<2)  % Comm mode, not done
        %fprintf(1,'%4.2f COMM %d  Ball %3.0f\n',day_time,Fl.commode,Fl.ballast*1e6);
        [ballast_goal,Fl,drogue_out]=commsim(Fl,FM,day_time,time_comm);
        drogue=drogue_out;
        if day_time>FM.timejumptime & ijump==0   % Set clock forward
            day_time=day_time+FM.timejump;
            ijump=1;
        end
      
    end
    
    
    %% Simulate Altimeter response 
    % (this is a hack for Ice float missions)
    SurfcheckRequested = get_Ptable_param(Ptable,'Ice.SurfcheckRequested');
    if ~isnan(SurfcheckRequested) && SurfcheckRequested>0 && isfield(FM,'IceCommands');
        % issue ice command and rotate the list
        command = FM.IceCommands(1);
        fprintf(1,'>> %4.2f Simulate Surfcheck response (%d)\n',day_time, command);
        
        if length(FM.IceCommands)>1
            FM.IceCommands = FM.IceCommands([2:end,1]);
        end
        % add to FM.Command for plotting
        FM.Command_Time = [FM.Command_Time,day_time];
        FM.Command = [FM.Command,command];
        icommand = length(FM.Command)+1; % to prevent execution of the "command"
    end
    %% simulate Photo taking
    % photos
    last_snapshot = get_Ptable_param(Ptable,'Snapshot.day_time_last');
    if ~isnan(last_snapshot) && last_snapshot>0 
        % add to FM.Photo for plotting
        if ~isfield(FM,'Photo_Time'),
            FM.Photo_Time = last_snapshot;
        elseif last_snapshot>FM.Photo_Time(end)
            FM.Photo_Time(end+1) = last_snapshot;
        end
    end
    % end of altimeter / camera hack
    %%
    if (mode==FM.XFER & Fl.commode<2)  % Aux file transfer mode, not done
        %fprintf(1,'%4.2f COMM %d  Ball %3.0f\n',day_time,Fl.commode,Fl.ballast*1e6);
        [ballast_goal,Fl,drogue_out]=xfersim(Fl,FM,day_time,time_xfer);
        drogue=drogue_out;
    end
    
    if homing==1   % Homing
        drogue=1;  % open drogue during home - speedbreak
        ballast_goal=0;
        if (Fl.ballast<0.1e-6)  % done
            homing=0;
            drogue=0;
            fprintf(1,'>>%6.3f  HOME end  Ball:%4.1f\n',day_time, Fl.ballast*1e6);
        end
    end
    
    if ( (mode~=FM.COMM && mode~=FM.XFER && homing~=1) || ...
            Fl.commode>=2 )  % not comm, not aux transfer, not homing, but if comm done
        %secOday=(day_time+FM.launchtime-floor(day_time+FM.launchtime))*86400;
        secOday=(day_time-floor(day_time))*86400;
        if FM.SafeBallast==1   % SAFE BALLAST
            [ballast_goal,mode_out,drogue_out]...   % float control program
                =SafeBallast(day_time,Pin,Fl.ballast,mode);  % C subrountine
        else   % NORMAL BALLAST
            %fprintf(1,'%5.1f %5.1f T %5.1f %5.1f S %5.1f %5.1f\n',day_time,Pin,Tbot,Ttop,Sbot,Stop);
            [ballast_goal,mode_out,drogue_out,telem_out,Ptable]...   % float control program
                =ballast(day_time,Pin,Tbot,Sbot,Ttop,Stop,Fl.ballast,mode,drogue,secOday,command, Ptable_in);  % C subrountine
        end
        
        down_home = get_Ptable_param(Ptable,'down_home'); % obtaind down_home from the float's Ptable
        state = get_Ptable_param(Ptable,'State'); % obtaind State from the float's Ptable
        Fl.commode=0;
        % Check if we should home next (simulate down_home in Kenney's code)
        if mode~=mode_out
            if mode==FM.DOWN & down_home==1  % end of DOWN
                homing=1;
                fprintf(1,'>>%6.3f  HOME at end of DOWN Ball:%4.1f\n',day_time, Fl.ballast*1e6);
            elseif mode==FM.COMM & down_home==0  % end of COMM
                homing=1;
                fprintf(1,'>>%6.3f  HOME at end of COMM Ball:%4.1f\n',day_time, Fl.ballast*1e6);
            end
        end
        mode=mode_out;
        drogue=drogue_out;
        if(ballast_goal>FM.ballmax)	ballast_goal=FM.ballmax;end
        if(ballast_goal<0.)ballast_goal=0.;end
        
    end
        
    next_time=day_time+time_step_day;
    center_time=(day_time+next_time)/2.;
    Pold=Fl.P;  % starting depth
    Hold=ML.P;  %starting ML depth
    
    Bold=Fl.ballast; % Calculate bocha position during next time step
    [Bnext Bav]=move_bocha(Fl.P,Bold,ballast_goal,time_step_sec,mode,FM,homing);
    
    % Allow for arbitrary delay in implementing Computed bocha position
    if length(Bsave)==0  % initialize
        bcycleMax=100;
        bcycle=0;   % circular index
        Bsave(1:bcycleMax)=NaN;
    end
    bcycle=bcycle+1;
    bcycle=mod(bcycle-1,bcycleMax)+1;  % [ 1 2 98 99 100 101]  -> [ 1 2 98 99 100 1]
    Bsave(bcycle)=Bnext;
    ib=mod(bcycle-1-FM.Blag,bcycleMax)+1;  % Go back FM.Blag samples
    if ( FM.Blag>0 & ~isnan(Bsave(ib)) )
        Fl.ballast=Bsave(ib);  % With lag
    else   % old way
        Fl.ballast=Bnext;  % Using Bnext seems to stabilize settle mode
        %Fl.ballast=Bav;  % This might be more honest
    end
    %disp([Fl.ballast Bnext]*1e6)   % Check
    
    if drogue==0   % drogue closed
        if Fl.weight<FM.closed_weights(1),
            Fl.area = FM.closed_areas(1);
        elseif Fl.weight>FM.closed_weights(end),
            Fl.area = FM.closed_areas(end);
        else
            Fl.area=interp1(FM.closed_weights',FM.closed_areas',Fl.weight);
        end
        % NOTE: I used weight from last time step above, which isn't perfect
    elseif drogue==1;              % drogue open
        Fl.area=FM.area_open;
    else
        error('BAD DROGUE VALUE');
    end
    
    % mld gives true depth of ML
    ML.P=mld(center_time,ML);
    if ML.P<ML.min;    % ML can't have negative depth.
        ML.P=ML.min;
    end
    ML.Z=ML.P- Zdisp(center_time,IW,ML.Z,SD);
    
    % Note turbulent region extends ML.Turb_ent below ML.P;
    if Fl.P<(ML.P+ML.Turb_ent)   % in mixed layer or ent zone. Get turb W and accel at float
        [Fl.Wt, Fl.At]=Wturb(day_time,next_time,ML,Fl,Hy,Fl.P);
        Fl.Wt0=Fl.Wt;  % save W in case float exits ML
    else
        Fl.Wt=0;Fl.At=0;  % no turb below mixed layer
    end
    
    % Add Subduction velocity.
    % Peak at half MLD, zero at top and MLB
    if SD.flag==3
        Fl.Wt=Fl.Wt+interp1([0 SD.MLs/2 SD.MLs 1000.],[0 SD.Wmax 0 0],Fl.Z);
    end
    
    % Integrate turbulence velocity
    Fl.Z=Fl.Z+Fl.Wt*time_step_sec+ 0.5*Fl.At*time_step_sec^2;  % Z at end
    
 
    % Update Disp/Pressure at central time
    Fl.Zeta= Zdisp(center_time,IW,Fl.Z,SD);  % IW/Eddy Displacement at float
    Fl.P=Fl.Z+ Fl.Zeta;	% float depth
    
    % Integrate float buoyancy velocity - Gear's method
    [Weight0, Wb0]=floatmodel2(next_time,Fl.Z,Fl.P,Fl,FM,Hy,ML);  % W from start
    Zpred=Fl.Z + Wb0*time_step_sec;   % Predicted depth
    Ppred=Zpred+Fl.Zeta;  % Predicted pressure
    [Weight1, Wb1]=floatmodel2(next_time,Zpred,Ppred,Fl,FM,Hy,ML);  % W from pred.
    Fl.Wb=(Wb0+Wb1)/2; 		  % Average Wb and Weight
    Fl.weight=(Weight0 + Weight1)/2;
    Fl.Z=Fl.Z + Fl.Wb*time_step_sec;  % Now step
    day_time=next_time;
    
    % Is Here better for this statement???
    Fl.Zeta= Zdisp(next_time,IW,Fl.Z,SD);  % IW/Eddy Displacement at float
    Fl.P=Fl.Z+Fl.Zeta;   % float depth
    
    Fl.ballast=Bnext;  % Update bocha position
    %Fl.ballast=Bav;  % This might be more honest
    
    
    if Fl.P<FM.Pmin 	%  Above surface
        Fl.P=min(FM.Pmin+abs(Fl.P-FM.Pmin)*ML.softbounce,   ML.P);  % don't bounce out of ML
        Fl.Wt=abs(Fl.Wt);
        x=1000;
        nit=0;
        Fl.Z=Fl.P-Fl.Zeta;
        while abs(x)>0.001  % iterate to get Fl.Z
            Fl.Zeta= Zdisp(next_time,IW,Fl.Z,SD);
            x=(Fl.P-Fl.Zeta) - Fl.Z;  % change in Fl.Z
            Fl.Z=Fl.Z+x;
            nit=nit+1;
            if (nit==150)
                warning(sprintf('NIT ERROR: %5.3f %d interations to get Z (%f) from P (%f)\n',day_time,nit,Fl.Z,Fl.P));
                Fl.Z=Fl.P-Fl.Zeta;
                break;
            end
        end
    end
    
    if Fl.P<ML.P+ML.Turb_ent & Pold>Hold+ML.Turb_ent  % float has entered ML from below
        Fl.Wt=-abs(Fl.Wt0);  % Get it up to speed for next step
    end
    
    if Fl.P>bottom(day_time,BT,IW)   % below bottom
        Fl.P=bottom(day_time,BT,IW);  % resting on bottom
        Fl.Z=Fl.P-Fl.Zeta;
    end
    
    %Update T & S at new position
    Ztop=max(0.,Fl.Z-FM.ctdZ);Ptop=max(0.,Fl.P-FM.ctdZ);  % keep top CTD in water
    Zbot=Fl.Z+FM.ctdZ;
    [dum Ttop Stop]=HyP2(Ztop,Ptop,ML,Hy);
    [dum Tbot Sbot]=HyP2(Zbot,Fl.P+FM.ctdZ,ML,Hy);
    Fl.T=(Ttop+Tbot)/2.;
    Fl.S=(Stop+Sbot)/2;
    
    rho=sw_dens(Fl.S,Fl.T,Fl.P);
    Z100=Zdisp(next_time,IW,100.,SD); % displacement at 100m
    bottomZ=bottom(day_time,BT,IW);  % bottom depth
    
    if (FM.LABSIMTEST==1)  % substitute labsimtest values
        ML.P=MLD0;ML.Z=MLD0;  % ML doesn't change
        Fl.P=Psim;
        Fl.Z=Fl.P;
        Fl.weight=0.;  % a bunch of fake values
        Z100=0.;
        bottomZ=1000;
    end
    
    %  Seems to duplicate a section above
    %     if mode==FM.ERR
    %         fprintf(1,'ERROR DECLARED -> COMM mode\n');
    %         mode=FM.COMM;
    %     end
    if Fl.P>FM.Pmax
        fprintf(1,'TOO DEEP %4.1f\n',Fl.P);
        toc
        return
    end
    
    %	Output  & save variables and plot
    smode= FM.smode;
    if ( day_plot>time_out  || day_time==time_step_day)
        sig=sw_pden(Fl.S,Fl.T,Fl.P,0)-1000.;
        % Terr=telem_out-1000-sig;  % telemetry error
        fprintf(1,...
            '>>%3.2fP %5.1f Ball/cc %5.1f %3.0f Mode %s Weight/g %7.3f  Wb/cms %5.2f Wt/cms %5.2f Sig %5.3f D%1d\n',...
            day_time,Fl.P,Fl.ballast*1.e6,ballast_goal*1.e6,smode(mode+1)...
            ,Fl.weight*1000.,Fl.Wb*100.,Fl.Wt*100.,sig,drogue);
        
        day_plot=0.;
        
        % DIAGNOSTIC PROFILE plot & contours
        if FM.doDensPlots
            igs=igs+1;
            figure(100);set(gcf,'position',[1100 400 500  500]);
            % Float measured Potential density
            flsg=sw_pden(Fl.S,Fl.T,Fl.P,0.)-1000;
            scatter(flsg,-Fl.P,50,day_time,'filled','MarkerEdgeColor','k');hold on
            
            % Potential density at center of float - different because of profile curvature
            [~,flt,fls]=HyP2(Fl.Z,Fl.P,ML,Hy);
            flsg=sw_pden(fls,flt,Fl.P,0.)-1000;
            Gtop=sw_pden(Stop,Ttop,Ztop,0.)-1000;
            Gbot=sw_pden(Sbot,Tbot,Zbot,0.)-1000;
            scatter(flsg,-Fl.P,50,day_time,'+','MarkerEdgeColor','k');
            color_line([Gtop Gbot],-[Ztop Zbot],day_time*[1 1]); % line showing interpolation
            
            % Potential density profile from stored 'Hy' profile
            for ipp=1:length(Hy.P)
                Zpp=Hy.P(ipp); % Depth in stored profile
                Ppp=Zpp+Zdisp(day_time,IW,Zpp,SD);  % real depth - want data here
                if Zpp<Hy.P(end-2) && Zpp>=0  % in profile
                    [~,flt,fls]=HyP2(Zpp,Ppp,ML,Hy);
                    sg0(ipp)=sw_pden(fls,flt,Ppp,0)-1000;
                    Ppps(ipp)=Ppp;
                elseif Zpp<0
                    sg0(ipp)=sw_pden(Hy.S(1),Hy.T(1),0.,0)-1000;
                    Ppps(ipp)=Ppp;
                else
                    sg0(ipp)=NaN;
                    Ppps(ipp)=NaN;
                end
            end
            % interpolate to uniform grid going to the surface
%             gridP=Hy.P;
%             gin=find(~isnan(sg0+Ppps));
%             sg1=interp1(Ppps(gin),sg0(gin),gridP,'linear','extrap'); 
            
            color_line(sg0,-Ppps,day_time*ones(size(Hy.P)));
            hold on;
            colormap('jet');
            colorbar
            legend('Float measured','Float Center','Stored Profile');
            title('Storing these profiles for plotting');
            FM.SGsave(igs,:)=sg0; % save for plot
            FM.Tsave(igs,:)=day_time*ones(size(sg0));
            FM.Psave(igs,:)=Ppps;
        end
        
    end
    day_plot=day_plot+time_step_day;
    
    % save data - every sample interval
    iout=iout+1;
    if iout-Nout >0 & iout-Nout<3
        warning('Files too small');
    end
    
    Day(iout)=day_time;
    P(iout)=Fl.P;
    Z(iout)=Fl.Z;
    Weight(iout)=Fl.weight;
    Zeta(iout)=Z100;  % Displacement of nominal 100 m isopyc
    H(iout)=ML.P;
    RHO(iout)=rho;
    TEMP(iout)=Fl.T;
    SAL(iout)=Fl.S;
    Ball(iout)=Fl.ballast;
    Mode(iout)=mode;
    ZB(iout)=bottomZ;
    Drogue(iout)=drogue;
    Home(iout)=homing;
    State(iout)=state;
end

toc
% END MAIN SCRIPT

%*************************************
function [Bnext, Bav]=move_bocha(P,ballast,goal,time_step_sec,mode,FM,homing)
% Gives end and average bocha position during next time step

if homing==1  % Full Speed if homing
    dist=time_step_sec*FM.bocha_speed;
else       % else limited time when sampling is occuring also
    dist=min( time_step_sec,FM.bocha_time(mode+1) )*FM.bocha_speed;
end

if goal>ballast
    Bnext=ballast+dist;
    if (Bnext>goal)
        Bnext=goal;
    end
    
elseif goal<ballast
    Bnext=ballast-dist;
    if (Bnext<goal)
        Bnext=goal;
    end
else
    Bnext=goal;
end

Bav=(ballast+Bnext)/2;

%*************************************
function H=mld(day,ML)
% Gets mixed layer depth

if ML.pwp==0    % Use prescribed ML depth
    if day<ML.time0
        H=ML.z0;
    elseif day>ML.time0 & day<ML.time1
        H=ML.z0+ML.growth*(day-ML.time0);
    else
        H=ML.z2;
    end
    H=H+ML.Zdiel*sin(2*pi*day);  % diel variation
    
else % Implement PRT Bulk Parameterization
    tstorm=day-ML.storm_start;
    error('PRT not implemented');
    if tstorm<0   % Before storm
        H=ML.z0;
    elseif (  (tstorm>0) & (tstorm<ML.storm_duration )) % during storm
        UIH=ML.Impulsemax*tstorm/ML.storm_duration;
        H=interp1(ML.gDrhoH3,ML.Zent,UIH^2*ML.pwp)/(1.+ML.ent_fact);
    else
        UIH=ML.Impulsemax;  % After storm
        H=interp1(ML.gDrhoH3,ML.Zent,UIH^2*ML.pwp)/(1.+ML.ent_fact);
    end
end

%*************************************
function [W, A]=Wturb(day0,day1,ML,Fl,Hy,Z)
% Gets turbulent velocity at day1 from values at day0, average accel over interval
% Langevin model based on epsilon
% bounce at surface
% Add bounce at MLB due to stratification
% Note that 'Z' is called with FL.P, real depth

dt=(day1-day0)*86400;
if dt==0,
    % degenerate time step
    W=Fl.Wt;
    A=0;
    return;
end

W0=Fl.Wt;  % W from last time, time day0

tstorm=day1-ML.storm_start;
if (  (tstorm>0) & (tstorm<ML.storm_duration ))
    U=ML.Ustorm;
else
    U=ML.U0;
end
Udiel=ML.Udiel*sin(2*pi*day0);
U=max(U+Udiel,ML.Umin);
W_rms=LP80(U,ML);

if Z<=ML.P     % In mixed layer - homogeneous
    dw2dz=0.;
    Arho=0.;
    
elseif Z>ML.P & Z<ML.P+ML.Turb_ent   % Entrainment zone
    
    % Turbulent param
    dw2dz= -W_rms.^2/ML.Turb_ent;  % linearly decay Wrms in EntZone
    W_rms=sqrt(abs(W_rms.^2-dw2dz*(Z-ML.P) ));
    
    % Buoyancy param (only if we need it)
    Arho=0.;
    if ML.Aentfact~=0,
        [rho T S Tml Sml]=HyP2(Fl.Z,Fl.P,ML,Hy);
        if(~isnan(Tml+Sml))
            x=Tml+Hy.atg*Z; %ML temp from potemp
            rhoml=sw_dens(Sml,x,Z);   % density at this P, ML S & Theta
            Arho= -9.81*(rho-rhoml)/rho;  % Accel is positive down
            %         else
            %             Arho=0.;  % Not in Hyp2's entrainment zone
            %             % Not that this is inconsistent with ML.Zent above...
        end
    end
else
    W_rms=0.;  % if we get here code bombs!
    Arho=0.;
    error('mlf2sim Wturb error');
end

% Old way
% Tau=ML.P/W_rms;  % Tau_L
% W= W0-W0/Tau*dt + sqrt(2.*W_rms^2*dt/Tau)*randn(1)...
%        + dw2dz*dt + Arho*ML.Aentfact*dt;
%         %+0.5*(1+(W0/W_rms)^2)*dw2dz*dt;  % term in Rodean

% modify to allow W_rms=0
ITau=W_rms/ML.P;  % 1./Tau_L
W= W0-W0*ITau*dt + sqrt(2.*W_rms^2*dt*ITau)*randn(1)...
    + Arho*ML.Aentfact*dt ...   % Reduce buoyancy return force (normally 0.)
    +( 1-ML.Rodean+ML.Rodean*(W0/W_rms)^2)*dw2dz*dt;
% ML.Rodean is (0-0.5) 0 for standard, 0.5 for Rodean correction
A=(W-W0)/dt;

%*************************************
function b=bottom(day,BT,IW);
if (BT.upwell~=0)   % lock bottom onto upwelling
    b= BT.mean-Zup(day,IW)*BT.upwell;
else
    ss=sin(2.*pi*day/BT.period);
    b=BT.mean+BT.amp.*abs(ss).^BT.N.*sign(ss);  % independent bottom
end

%*************************************
% Heat mixed layer by  solar radiation and mix to relieve instability
function Hy=Flux(Hy,ML,day,time_step_sec);
H=mld(day,ML);
ml=find(Hy.P<ML.Z & Hy.P>(ML.Z-H) );  % find mixed layer bins
Hy.Th(ml)=Hy.Th(ml)+ML.Q/ML.C/H*time_step_sec; % heat

Hy.Sigma0(ml)=sw_dens(Hy.S(ml),Hy.Th(ml),Hy.P(ml)*0.)-1000;  % pot dens

nbad=1;
while nbad>0
    drho=diff(Hy.Sigma0);
    bad=find(drho< -ML.drhomin);  % inversions
    nbad=length(bad);
    plot(Hy.Sigma0,-Hy.P)
    hold on
    if nbad>0
        mix=(Hy.Th(bad)+Hy.Th(bad+1))/2;  % mix neighbors
        Hy.Th(bad)=mix;
        Hy.Th(bad+1)=mix;
        mix=(Hy.S(bad)+Hy.S(bad+1))/2;  % mix neighbors
        Hy.S(bad)=mix;
        Hy.S(bad+1)=mix;
        Hy.Sigma0(bad)=sw_dens(Hy.S(bad),Hy.Th(bad),Hy.P(bad)*0.)-1000;  % pot dens
        Hy.Sigma0(bad+1)=...
            sw_dens(Hy.S(bad+1),Hy.Th(bad+1),Hy.P(bad+1)*0.)-1000;  % pot dens
    end
end
Hy.T=sw_temp(Hy.S,Hy.Th,Hy.P,0.);  % get T from Th
Hy.Sigma=sw_dens(Hy.S,Hy.Th,Hy.P)-1000;  %  dens

%disp([day0  Z  ML.eps W W0 W_rms ]);
%*************************************

% FAKE ROUTINES FOR TESTING
% function  x=set_ballast(day,P,ballast)
% 	x=15e-6;
%
% function b=get_ballast(x)
% 	b=15e-6;
%
% Time to end of mission */
%double days_remaining()
%{
%return(end_time-day_time);
%}
