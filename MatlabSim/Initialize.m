% This script initializes variables 

% Misc Setup
f=1.42e-4*sin(latitude/57.29);
homing=0;  % 1 means its homing mode

% Mixed Layer growth rate
ML.growth=(ML.z1-ML.z0)/(ML.time1-ML.time0);

% GRID Hydrographic profile
Hy.Pbin=Pbin;
Hy.P=[0:Pbin:FM.Pmax*1.2]';
Hy.S=interp1(Pi',Si',Hy.P,'linear');
Hy.Th=interp1(Pi',Thi',Hy.P,'linear');
Hy.T=sw_temp(Hy.S,Hy.Th,Hy.P,0.);
[Hy.N2,vort,Hy.Pn2] = sw_bfrq(Hy.S,Hy.T,Hy.P); % N^2 and Pgrid ( no Fine Str)
Hy.N2 = max(Hy.N2,0);


% PREMIX profiles to SD.ML0
if SD.flag==1 
    [Hy,SD]=SubductHy(0.,Hy,SD);
end

% Add FINESTRUCTURE between IW.FS_top and IW.FS_bott   (PROBABLY BROKEN)
load Grads.mat  % load Grads = rhoz/rhozBar

Nz=length(Hy.P);
Ngrads=length(Grads);

FSgood=[IW.FS_top/Pbin:IW.FS_bott/Pbin];   % Depth range of finestructure
index=ceil(rand(1,Nz).*Ngrads);  % index of random numbers

% Temperature
Tzbar=conv2(Hy.Th,[-1 0 1]'/2,'same');  % Smoothed T gradient
Tz=Tzbar.*Grads(index);
Tz=Tz-mean(Tz(FSgood))+mean(Tzbar(FSgood)); % correct mean
Th=Hy.Th;
Th(FSgood)=Hy.Th(FSgood(1))-cumtrapz(Hy.P(FSgood),Tz(FSgood));
Hy.Th=Th;
clear Th Tz Tzbar

% Salinity
Szbar=conv2(Hy.S,[-1 0 1]'/2,'same');  % Smoothed S gradient
Sz=Szbar.*Grads(index);
Sz=Sz-mean(Sz(FSgood))+mean(Szbar(FSgood)); % correct mean
S=Hy.S;
S(FSgood)=Hy.S(FSgood(1))-cumtrapz(Hy.P(FSgood),Sz(FSgood));
Hy.S=S;
clear S Sz Szbar index Grads

Hy.T=sw_temp(Hy.S,Hy.Th,Hy.P,0.);
Hy.Sigma=sw_dens(Hy.S,Hy.T,Hy.P)-1000.;   % in situ density
Hy.Sigma0=sw_dens(Hy.S,Hy.Th,0.)-1000.;   % potential density

%plot(Hy.Sigma0,-Hy.P,'k');
% END FINESTRUCTURE

% DATA FOR ENTRAINMENT Mixed layer T/S given ML depth of Hy.P
Smix=Hy.P*NaN;Tmix=Smix;
for i=2:length(Hy.P)
    [Zmix,Smix0]=Entrain(Hy.P,Hy.S,Hy.P(i),ML.Zmlb); 
    [Zmix,Tmix0]=Entrain(Hy.P,Hy.T,Hy.P(i),ML.Zmlb); 
    Smix(i)=Smix0(1);
    Tmix(i)=Tmix0(1);
end
Smix(1)=Smix(2);Tmix(1)=Tmix(2);
Hy.Sml=Smix;
Hy.Tml=Tmix;

% Compare new and old
% plot(Smix,-Hy.P,'k.');
% grid on
% error('stop');

Hy.atg = sw_adtg(Hy.Sml,Hy.Tml,0.);% ATG to get Ptemp from temp
ggg=find(~isnan(Hy.atg));
Hy.atg=Hy.atg(ggg(1));

% Get quantity to infer PWP ML depth 
Hy.Drho=sw_dens(Hy.Sml,Hy.Tml,Hy.P);
Hy.Drho=sw_dens(Hy.S,Hy.T,Hy.P)-Hy.Drho;   % ML density step vrs Zent

% g Drho/rho0*H^3
ML.gDrhoH3=Hy.Drho.*(Hy.P- ML.Zmlb/2.).^3.*9.8/1023;  % ???????
ML.Zent=Hy.P;
%solve ML.gDrhoH3(Zent)=(storm Impulse)^2*Rb   to get PWP MLD

% Internal Wave and Eddy phases
IW.Peddy=rand(1)*2.*pi;      %phase of eddy field
IW.Ptide=rand(1)*2.*pi;           % phase of internal tide
IW.N1=sqrt(abs(interp1q(Hy.Pn2,Hy.N2,IW.ZN)));  % use this buoyancy frequency

if isnan(IW.N1)
    IW.N1=0;
end

if IW.N1<f  % this prevents divide by zero, negative freq steps and NIT errors
    IW.dF=0;  % with dF=0 and finite N1, there is no IW energy.
    IW.N1=1e-5;
else
    IW.dF=(IW.N1-f)/(IW.N-1); % IW.N is number of freq bins
end

IW.F=zeros(1,IW.N); IW.P=IW.F;IW.Z=IW.F;
for i=1:IW.N
    IW.F(i)=f+IW.dF*(i-1);
    IW.P(i)=rand(1)*2.*pi;
    IW.Z(i)=sqrt(abs(IW.r_GM*IW.dF/IW.N1))/IW.F(i);
end
IW.Z(IW.N)=IW.Z(IW.N)*IW.Npeak;   % Peak at N


% IW.Z is amplitude of IW displacment at different frequencies.
% Zdisp adds the phase.

% Float position
Fl.Z=1;           
Fl.P=Fl.Z + Zdisp(0.,IW,Fl.Z,SD); 
while Fl.Z<=0  |  Fl.P<=0  % Make sure we're in water
	Fl.Z=Fl.Z+10;
	Fl.P=Fl.P+10;
end

% Float basic properties

if Mset==1
    rho0=sw_dens(FM.S0,FM.T0,FM.P0);  % reference ballast point
    calc_mass=(FM.vol0+FM.B0...          % FM.vol0 is vol at P=0,T=FM.Tref
        - FM.chi*FM.vol0*FM.P0...
        + FM.air*10./(10.+max(FM.P0,0))...
        + FM.vol0*FM.alpha*(FM.T0-FM.Tref)...
        )*rho0;  % Float mass
    

	FM.mass0=calc_mass;
	fprintf(1,'Mass calculated %f\n',FM.mass0);
end

icommand=1;  % index of simulated commands
ierror=1;  % index of simulated errors
FM.bugmass=0.;
igs=0;

Fl.Wt=0.;             % turbulent velocity at float
Fl.At=0;               % turbulent acceleration
Fl.Wt0=0;            % saves last good turb velocity
Fl.ballast_point=0.;   % computed ballast at target depth
Fl.area=mean(FM.closed_areas);    % float drogue area
Fl.mass_offset=0.;     %  Float mass offset due to "creep"
Fl.g=g;
Fl.Wb=0;		% Float velocity due to buoyancy

FM.bugson=0; % start with no bugs
