%----Entrainment function
% function [Zmix,Smix]=Entrain(Z,S,H,Ent)
% For a profile of conserved quantity S as function of depth Z
% Returns Smix(Zmix): i) value mixed to depth H 
%                    ii) then linearly decaying over layer Ent thick back to Se=S(H+Ent)
% Zmix adds point at H+Ent to Z 
%
% See "ConservingML.jpg" for sketch of the derivation
% 

function [Zmix,Smix]=Entrain(Z,S,H,Ent)
diag=0;% diagnostic

if H<0 || Ent<0
    error('Entrain: H  & Ent must be positive');
end

ic=iscolumn(Z);
if iscolumn(S)~=ic
    error('Entrain: Z & S need same dimensions');
end
if ic==0
    Z=Z';S=S';
end
if diag==1
    figure
    plot(S,Z)
    hold on
    IS0=trapz(Z,S)/max(Z);  % mean salinity
end

Ze=H+Ent; % bottom of mixing zone
Se=interp1(Z,S,Ze); % value of S at Ze
gmix=find(Z<=Ze); % points in mixing zone
ge=min(gmix(end)+1,length(Z)); % entrainment point
Zmix=[Z(gmix); Ze; Z(ge+1:end)];  % New point at Ze
Sml=Se+trapz(Z(1:ge),S(1:ge)-Se)/(H+Ent/2); % Mixed layer salinity

% Evaluate new profile
Smix=zeros(size(Zmix));
Smix(ge:end)=interp1(Z,S,Zmix(ge:end));  % below entrainment
Smix(1:ge)=interp1([0 H Ze],[Sml Sml Se],Zmix(1:ge)); % in Entrainment

% remove duplicate points
[~,iu]=unique(Zmix);
Zmix=Zmix(iu);
Smix=Smix(iu);

%Back to original shape
if ic==0
    Zmix=Zmix';Smix=Smix';
end

if diag==1
    plot([Sml Sml Se],[0 H Ze],'k-+');
    plot(Smix,Zmix,'r.')
    
    IS1=trapz(Zmix,Smix)/max(Z);
    legend(sprintf('Original %f',IS0),sprintf('Mixed %f diff %f Fraction %6.3e',IS1,IS1-IS0,(IS1-IS0)/IS1) );
    title(sprintf('H= %f Ent=%f ',H,Ent));
    drawnow
    
end


return
% ----------------------------


