% Bocha Energy estimator
% Current = 0.1 A @ 0psi, 1.1 A @ 300 psi
% Voltage = 14V
% Energy per battery = 30AH x 3.5V
% 
% Run after simulation
% Uses P (pressure) and B(Bocha position)
% Outputs Energy used during each time step and cumulative sum
% Plots sum and cumulative average vrs time

% Ball - bocha position in m^3


clear all
load 'Data/Env1.mat';
time1=1.669; time2=1.759;time3=2.669;  % Prof Settle boundaries

time=yd;
g=find(time>time1 & time<time3);
time=time(g);P=P(g);Ball=B(g)*1e-6;
sec=time*86400;

Speed=0.91e-6;  % m^3/s
Area=20.1/1e4; % area m^2
V=14.;  % Voltage

Zball=Ball/Area;  % bocha position in m

dt=diff(sec);  % time of interval
dBall=diff(Ball);  % change in bocha position/m^3
dZ=diff(Zball); %change in bocha position/m^3
dtON=abs(dBall)./Speed; % Time bocha is moving in this step
Pmean=conv2(P',[ 1 1]/2,'valid')';

out=find(dZ>0);
in=find(dZ<=0);
Current=ones(size(dZ))*0.1;
Current(out)=0.1+1.0*Pmean(out)*1.45/300; % Current/ A
Current(out)=1.0*Pmean(out)*1.45/300; % Current/ A    - Deviation from no pressure

DWork=dtON.*V.*Current;  % Energy used J

Work=cumsum(DWork);  % Cumulative Energy used;

dPV=zeros(size(dtON));
PmeanPa=Pmean/100*1e6;
dPV(out)=PmeanPa(out).*dBall(out);
PV=cumsum(dPV);

clear h
subplot(2,1,1)
h=plotyy(time,-Ball*1e6,time,-P);
hold on;
ylabel(h(1),'BALL');
ylabel(h(2),'P');
% subplot(3,1,3)
% h=plotyy(time(2:end),Current,time(2:end),DWork);
% legend(h,'Current/A','dWork/J');
% grid on;
subplot(2,1,2)
h=plot(time(2:end),Work,time(2:end),PV);
grid on
text(time(end),Work(end),sprintf('%6.1f J',Work(end) ),'horizontalalignment','right','FontSize',14);
legend(h,'Power Used','Work Done','location','best');
ylabel('Joules');




