%% plot starts here
Zmax=200;
orange=[1 0.75 0]; brown=[0.8 0.4 0.4];
darkred=[0.7 0 0];purple=[0.8 0 0.6];darkblue=[0 0 0.7];
darkgreen=[0 0.8 0];bluegreen=[0 0.9 0.5];

adt=axes('position',[0.20 0.55 0.78 0.4]);  % Depth time
awt=axes('position',[0.20 0.05 0.78 0.4]);  % Weight time
asd=axes('position',[0.01 0.55 0.13 0.4]); % Sigma depth
awd=axes('position',[0.01 0.05 0.13 0.4]); % Weight depth - change to Temp
linkaxes([adt awt], 'x');

%%
axes(adt)   % Depth plot
dl=ceil(1e5/length(H));
l=[1:dl:length(H)];

hh=plot(time,-H,'b-',time,-H-ML.Turb_ent,'b--');  %turbulent MLB
hh=hh(1);
hold on
plot(time,-H- ML.Zmlb,'-.','Color',darkblue);  % Scalar MLB
hz=plot(time,-Zeta-80,'k--');

hz=plot(time,-Z,'g','LineWidth',0.5);
hp=plot(time,-P,'r','LineWidth',2);

dcl=find(Drogue==0);  % Drogue
tl=time(dcl);pl=P(dcl);
le=find(diff(dcl)>1);
tl(le)=NaN;
plot(tl,-pl,'Color',orange,'LineWidth',3);
hB=plot(time,-ZB,'Color',brown,'LineWidth',2);

dcld=find(Drogue==0 & (Mode==FM.ML | Mode==FM.SEEK | Mode==FM.ISO));
plot(time(dcld),-P(dcld),'o','Color',orange,'LineWidth',3,'MarkerSize',4);

mm=find(diff(Mode)~=0 & ~isnan(diff(Mode)) );
Pb=P; Wb=Weightav;
Pb(mm+1)=NaN;Wb(mm+1)=NaN;
gm=find(Mode==FM.ML);
plot(time(gm),-Pb(gm),'k','LineWidth',2,'Color',darkred);
gs=find(Mode==FM.SEEK);
plot(time(gs),-Pb(gs),'k','LineWidth',2,'Color',purple);
gh=find(Home==1);
plot(time(gh),-P(gh),'k.','LineWidth',2,'Color',bluegreen);

if modelabel==1
    for i=1:length(mm)
        k=mm(i);
        s=smode(Mode(k+1)+1);
        ht=text(time(k),-P(k)+(Mode(k)-3),s);
        set(ht,'FontSize',10);
        hold on
    end
    hh=find(diff(Home)>0);
    for i=1:length(hh)
        k=hh(i);
        ht=text(time(k),-P(k)+1,'H');
        set(ht,'FontSize',8);
    end
end

if length(FM.Command)>0   % Plot commands
    for ic=1:length(FM.Command)
        plot(FM.Command_Time(ic)*[1 1],[-Zmax 0],'Color',[1 1 1]*0.4);
        text(FM.Command_Time(ic),-Zmax,sprintf('%1.0f',FM.Command(ic)),...
            'FontSize',12,'Color',[1 1 1]*0.4,'HorizontalAlignment','center',...
            'VerticalAlignment','bottom');
    end
end

a=axis;
axis([a(1) a(2) -Zmax 0]);

U10=ones(size(sec))*ML.U0;
st=find(sec/86400.>ML.storm_start &...
    sec/86400.<ML.storm_start+ML.storm_duration );
U10(st)=ML.Ustorm;
%
% 		hw=plot(Day,U10-120,'LineWidth',2,'Color',[0.5 0.5 0.5]);
% 		text(ML.storm_start*86400,-115,'U10','FontWeight','Bold',...
% 		'HorizontalAlignment','Right');

if nolegend~=1
    %legend([hh hp hz hB ],'Mixed Layer','Pfloat','Zfloat','Bottom',4);
    legend([hh hp hz],'Mixed Layer','Pfloat','Zfloat',4);
end
title(['Float Simulation ' num2str(real) '   '  date])
grid on
set(gcf,'Color','w');
a=axis;

axes(asd)    % DENSITY VRS DEPTH
if (FM.LABSIMTEST~=1)
    plot(Hy.Sigma0,-Hy.P,'k-');
    hold on
    b=axis;
    b(1)=b(1)-(b(2)-b(1))/10;
    plot(SIGMA0,-P,'r');
    axis([b(1) b(2) -Zmax 0.]);
    Sm=nanmedian(SIGMA0);
    gmed=find(abs(SIGMA0-Sm)<0.01);
    %         plot(SIGMA0(gmed),-P(gmed),'g.');
    set(gca,'YtickLabel','');
    xlabel('Sigma');
    grid on;axis manual;
    plot(b(1)+sqrt(Hy.N2)*(b(2)-b(1))/Nmax,-Hy.Pn2,'k','LineWidth',2);
    for it=0:1./Ntick:1
        xx=b(1)+it*(b(2)-b(1));
        plot([xx xx],[-5 0],'k');
    end
    title(['\sigma 0 & N [0 ' num2str(Nmax,2) ']']);
else
    gmed=[];
    plot(SIGMA0,-P,'r');
    axis tight
    b=axis;
    axis([b(1) b(2) -Zmax 0.]);
end

%
axes(awt)   % Ballast plot
if (FM.LABSIMTEST~=1)
    gnn=find(~isnan(SIGMA0));
    hs=plot(time,(SIGMA0-median(SIGMA0(gnn)) )*1000,'m.');
    hold on
    hw=plot(time,Weightav*1e3,'r');
    plot(time(gm),Wb(gm)*1e3,'k','LineWidth',2,'Color',darkred);
    hww=plot(time,Weightav*1e5,'g.','MarkerSize',6);
    plot(time(gm),Wb(gm)*1e5,'g.','MarkerSize',6,'Color',darkgreen);
    plot(time(gs),Wb(gs)*1e5,'g.','MarkerSize',6,'Color',bluegreen);
    %plot(time,Weightmed*1e5,'g-');
    
    %plot(time,Weightav*1e5,'.','Color',[0. 0.4 0],'MarkerSize',6);  % Dot weight
end
hb=plot(time,Ball*1.e6,'k','LineWidth',2);hold on;
bl=Ball(dcl)*1.e6;
plot(tl,bl,'Color',[0  0 1],'LineWidth',2);
plot(time(dcld),Ball(dcld)*1e6,'bo','LineWidth',3,'MarkerSize',3);
plot(time(gh),Ball(gh)*1e6,'k.','LineWidth',2,'Color','r');

if modelabel==1
    mm=find(diff(Mode)~=0 & ~isnan(diff(Mode)) );
    for i=1:length(mm)
        k=mm(i);
        s=smode(Mode(k+1)+1);
        zzl=Ball(k)*1e6+(Mode(k)-3);
        zzl(zzl>400)=400;
        ht=text(time(k),zzl,s);
        set(ht,'FontSize',7);
        hold on
        ht=text(time(k),(Mode(k)-3),s);
        set(ht,'FontSize',7);
    end
    hh=find(diff(Home)>0);
    for i=1:length(hh)
        k=hh(i);
        zzl=Ball(k)*1e6+1;
        zzl(zzl>400)=395;
        ht=text(time(k),zzl,'H');
        set(ht,'FontSize',8);
        ht=text(time(k),1,'H');
        set(ht,'FontSize',8);
    end
end

axis([a(1) a(2) -200 400]);
a=axis;
grid on

if nolegend~=1
    legend([hb hw hww hs],...
        'Ballast','Weight','Weight*100','(\sigma_0-Mean)*1000');
end
set(gcf,'Color','w');
zoom on
if (FM.LABSIMTEST~=1)
    title(sprintf('CTD # %d %d/%d/%d   %6.3f N  %8.3f W'...
        ,cast,sta.mo(cast),floor(sta.day(cast)),sta.yr(cast),...
        sta.lat(cast),abs(sta.lon(cast)) )  );
else
    title('LABTESTSIM');
end

axes(awd)   % TEMPERATURE - formerly weight
plot(THETA,P,'r-');
axis([18 30 0 Zmax]);
hold on
plot(THETA(gmed),P(gmed),'g.');
plot(Thc,Pc,'k-');
grid on;
set(gca,'Yaxislocation','right','Ydir','reverse')
xlabel('Temperature/C');
title('Temperature vrs Depth');
