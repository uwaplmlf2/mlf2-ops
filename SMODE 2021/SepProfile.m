% Get a CTD profile for July data
clear all

Sin =[  31.7175 -305.5531
   30.8517 -164.0568
   30.7729 -152.2556
   30.7439 -135.2094
   30.6941 -118.6003
   30.6587 -104.6137
   30.6169  -91.9384
   30.5606  -74.8922
   30.4947  -63.9652
   30.4497  -47.7931
   30.3709  -39.4886
   30.2953  -28.1245
   30.2246  -18.9458
   30.1635  -11.0783
   30.0735    0 ];
Tin = [ 10.2954 -248.2238
   11.9641 -164.5573
   12.1296 -131.0362
   12.2124 -107.6597
   12.3182  -82.0778
   12.4470  -64.8762
   12.6171  -47.2335
   12.8287  -37.5300
   13.0127  -26.9444
   13.3024  -19.0052
   13.5830   -8.4196
   13.8129   -4.0089
   13.9233    0.];
PG=[0:2:10 15:5:200];

TG=interp1(Tin(:,2),Tin(:,1),-PG);
SG=interp1(Sin(:,2),Sin(:,1),-PG);

SigG=sw_pden(SG,TG,PG,0)-1000;

a1=subplot(1,3,1);
plot(Tin(:,1),Tin(:,2),'.',TG,-PG,'Linewidth',2)
a2=subplot(1,3,2);
plot(Sin(:,1),Sin(:,2),'.',SG,-PG,'Linewidth',2)
a3=subplot(1,3,3);
plot(SigG,-PG,'Linewidth',2)
hold on

linkaxes([a1 a2 a3],'y');
ylim([-150 0])
% 
%   PG         1x44              352  double                                 
%   SG         1x44              352  double                                 
%   SigG       1x44              352  double                                 
%   Sin       19x2               304  double                                 
%   TG         1x44              352  double                                 
%   Tin       12x2               192  double                            


% Add to PS database
root='/Users/eric/Documents/GIT/mlf2-ops/Hydrography/PSsim/';

ifile=[root 'newclean7.mat'];
load(ifile);
ofile=[root 'newclean8.mat'];

%  N            1x1                 8  double
%   P           50x17             6800  double
%   S           50x17             6800  double
%   SIG         50x17             6800  double
%   SIG_0       50x17             6800  double
%   T           50x17             6800  double
%   TH          50x17             6800  double

N=size(P,2)+1;
g=[1:size(PG,2)];
b=[size(PG,2)+1:size(P,1)];
P(g,N)=PG(g); P(b,N)=NaN;
T(g,N)=TG(g); T(b,N)=NaN;
S(g,N)=SG(g); S(b,N)=NaN;
SIG=sw_dens(S,T,P)-1000;
SIG_0=sw_pden(S,T,P,0)-1000;
TH=sw_ptmp(S,T,P,0);
sta.lat(N)=NaN;
sta.lon(N)=NaN;
sta.yr(N)=2020;
sta.mo(N)=7;
sta.day(N)=25;

plot(SIG_0(:,N),-P(:,N),'m+')

clear ifile root PG SG TG SigG Tin Sin a1 a2 a3
save(ofile)

