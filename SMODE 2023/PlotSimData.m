disp('PlotSimData')
if ifig,
    figure('position',[ 100 400 1000  500]);
end
if plotclear==1
    clf
end

time=Day;
%time=(Day+TimeOffset)*24;

orange=[1 0.75 0]; brown=[0.8 0.4 0.4];
darkred=[0.5 0 0];purple=[0.8 0 0.6];darkblue=[0 0 0.7];
darkgreen=[0 0.8 0];bluegreen=[0 0.9 0.5];

adt=axes('position',[0.20 0.55 0.78 0.4]);  % Depth time
awt=axes('position',[0.20 0.05 0.78 0.4]);  % Weight time
asd=axes('position',[0.01 0.55 0.13 0.4]); % Sigma depth
awd=axes('position',[0.01 0.05 0.13 0.4]); % Weight depth - change to Temp
linkaxes([adt,awt],'x');
linkprop([adt,asd],'ylim');

% ----------------------------------
axes(adt)   % Depth time plot
dl=ceil(1e5/length(H));
l=[1:dl:length(H)];
if isfield(FM,'SGsave')
    [~,hh]=contour(FM.Tsave,FM.Psave,FM.SGsave,SigC(1):SigC(2):SigC(3),'Color',[0.7 0.7 0.9]);
    h(14)=hh(1);
    hold on
    leg{14}=sprintf('pdensity ci %4.2f',SigC(2));
end

h(1)=plot(time,H,'b-');leg{1}='MLD'; hold on;
h(2)=plot(time,H+ML.Turb_ent,'b--');leg{2}='TL turb depth'; %turbulent MLB

h(3)=plot(time,H+ML.Zmlb,'-.','Color',darkblue);leg{3}='TL density depth';  % Scalar MLB
plot(time,redline*ones(size(time)),'r-','LineWidth',2);
h(4)=plot(time,Zeta+80,'k--'); leg{4}='Ref isopycnal'; % waves
h(5)=plot(time,FM.Pmin*ones(size(time)),'-','Color',[ 1 1 1]*0.6); leg{5}='Float min P'; % Pmin - bounce

h(6)=plot(time,Z,'g','LineWidth',0.5);leg{6}='Z Depth (T/S)'; % real float
h(7)=plot(time,P,'r','LineWidth',2); leg{7}='P Depth (real)'; % ignoring adiabatic

dcl=find(Drogue==0);  % Drogue closed- mark in orange
tl=time(dcl);pl=P(dcl);
le=find(diff(dcl)>1);
tl(le)=NaN;
if length(tl)>0
    h(8)=plot(tl,pl,'Color',orange,'LineWidth',3);leg{8}='Drogue Closed';
else
    h(8)=plot(NaN,NaN,'Color',orange,'LineWidth',3);leg{8}='Drogue Closed';
end

% Drift modes with drogue closed
dcld=find(Drogue==0 & (Mode==FM.ML | Mode==FM.SEEK | Mode==FM.ISO));
if length(dcld)==0;dcld=1;end
h(9)=plot(time(dcld),P(dcld),'o','Color',orange,'LineWidth',3,'MarkerSize',5);leg{9}='Drift Drogue closed';

h(10)=plot(time,ZB,'Color',brown,'LineWidth',2); leg{10}='Bottom'; % Bottom


% Diagnose various drift modes
mm=find(diff(Mode)~=0 & ~isnan(diff(Mode)) );
Pb=P; Wb=Weightav;
Pb(mm+1)=NaN;Wb(mm+1)=NaN;

gm=find(Mode==FM.ML);
if length(gm)>0
    h(11)=plot(time(gm),Pb(gm),'k','LineWidth',2,'Color',darkred); leg{11}='Lagrangian Mode'; % ML (Lagrangian) drift
else
    h(11)=0;leg{11}='';
end
gs=find(Mode==FM.SEEK);
if length(gs)>0
    h(12)=plot(time(gs),Pb(gs),'k','LineWidth',2,'Color',purple);leg{12}='Seeking Isopycnal';   % Seek drift
else
    h(12)=0;leg{12}='';
end
gh=find(Home==1);
if length(h)>0 & length(gh)>0
    h(13)=plot(time(gh),P(gh),'k.','LineWidth',2,'Color',bluegreen,'MarkerSize',15);leg{13}='Homing';  % Homing
else
    h(13)=0;leg{13}='';
end

% Show state changes
statechange=find(diff(State)~=0);
plot(time(statechange),P(statechange),'k.','MarkerSize',2);


if showballast==1
    gb=find(abs(Weightav*1e3)<0.3);
    h(14)=plot(time(gb),Z(gb),'.','Color',rgb('darkgreen') );
    leg{14}='Good Ballast';
else
    h(14)=0;leg{14}='';
end

if modelabel==1
    for i=1:length(mm)
        k=mm(i);
        s=smode(Mode(k+1)+1);
        ht=text(time(k),P(k)-(Mode(k)-3),s);
        set(ht,'FontSize',10,'HorizontalAlignment','left');
        hold on
    end
    hm=find(diff(Home)>0);
    for i=1:length(hm)
        k=hm(i);
        ht=text(time(k),P(k)-1,'H','color','b','fontweight','b','FontSize',8);
    end
end

if length(FM.Command)>0   % Plot commands
    for ic=1:length(FM.Command)
        plot(FM.Command_Time(ic)*[1 1],[Zmax Zmax*0.8],'Color',[1 1 1]*0.4,'LineWidth',0.5);
        plot(FM.Command_Time(ic)*[1 1],[0 topc],'Color',[1 1 1]*0.4,'LineWidth',0.5); % top
        text(FM.Command_Time(ic)*[1 1],Zmax*[1 0.9],sprintf('%1.0f',FM.Command(ic)),...
            'FontSize',12,'Color',[1 1 1]*0.4,'HorizontalAlignment','center',...
            'VerticalAlignment','bottom');
    end
end

if isfield(FM,'Photo_Time') % plot photos taken
    iok = ~isnan(P);
    plot(FM.Photo_Time, interp1(time(iok),P(iok),FM.Photo_Time),'m*');
    if length(FM.Photo_Time)>0
        plot(FM.Photo_Time, FM.MaxPhotoP,'m.','MarkerSize',4);
    end
    if ~exist('phototimes')
        phototimes=[];photoPs=[];
    end
    phototimes=[phototimes FM.Photo_Time];
    photoPs=[photoPs interp1(time(iok),P(iok),FM.Photo_Time)];
end

a=axis;
axis([min(time) max(time) 0 Zmax]);

U10=ones(size(sec))*ML.U0;
st=find(sec/86400.>ML.storm_start &...
    sec/86400.<ML.storm_start+ML.storm_duration );
U10(st)=ML.Ustorm;
%
% 		hw=plot(Day,U10-120,'LineWidth',2,'Color',[0.5 0.5 0.5]);
% 		text(ML.storm_start*86400,-115,'U10','FontWeight','Bold',...
% 		'HorizontalAlignment','Right');

if nolegend==1 | nolegend==3
    gg=find(h~=0);
    legend(h(gg),leg{gg},'location','southeast');
end

title(['Float Simulation ' num2str(real) '   '  date '   Timebase is GMT + ' num2str(TimeOffset*24,'%3.0f') ' Hours'])
grid on
set(gcf,'Color','w');
set(gca,'ydir','reverse');
if dolog;
    set(gca,'yscale','log');
    ylim([1 Zmax]);
end
a=axis;

if Datelabel
    datetick('x','dd/HHZ','keeplimits')
end

% --------------------------------------------------------
axes(asd)    % DENSITY VRS DEPTH
if (FM.LABSIMTEST~=1)
    plot(Hy.Sigma0,Hy.P,'k-'); % Original values
    hold on
    %         b(1)=SIGMA0(1)-0.01;
    %         b(2)=max(SIGMA0(P<Zmax))+0.01;
    b(1)=min(Hy.Sigma0)-0.01;
    b(2)=max(Hy.Sigma0(Hy.P<Zmax))+0.01;
    plot(SIGMA0,P,'r');
    color_line(SIGMA0,P,time);
    axis([b(1) b(2) 0. Zmax]);
    colormap(gca,'jet');
    Sm=nanmedian(SIGMA0);
    gmed=find(abs(SIGMA0-Sm)<0.01);
    %         plot(SIGMA0(gmed),-P(gmed),'g.');
    set(gca,'YtickLabel','');
    xlabel('Sigma');
    grid on;axis manual;
    plot(b(1)+sqrt(Hy.N2)*(b(2)-b(1))/Nmax,Hy.Pn2,'k','LineWidth',2);
    for it=0:1./Ntick:1
        xx=b(1)+it*(b(2)-b(1));
        plot([xx xx],[Zmax/30 0],'k');
    end
    title(['\sigma_0 & N [0 ' num2str(Nmax,2) ']']);
else
    gmed=[];
    plot(SIGMA0,P,'r');
    axis tight
    b=axis;
    axis([b(1) b(2) 0 Zmax]);
end
set(gca,'ydir','reverse');
if dolog;
    set(gca,'yscale','log');
    ylim([1 Zmax]);
end
linkprop([adt,asd],'ylim');


% -------------------------  Ballast v time -----------------
axes(awt)   % Ballast plot
if (FM.LABSIMTEST~=1)
    gnn=find(~isnan(SIGMA0));
    SIGMED=median(SIGMA0(gnn));
    hs=plot(time,(SIGMA0-SIGMED)*1000,'m.');
    hold on
    hw=plot(time,Weightav*1e3,'r');
    plot(time(gm),Wb(gm)*1e3,'k','LineWidth',2,'Color',darkred);
    hww=plot(time,Weightav*1e5,'g-'); %'g.','MarkerSize',6);
    plot(time(gm),Wb(gm)*1e5,'g.','MarkerSize',6,'Color',darkgreen);
    plot(time(gs),Wb(gs)*1e5,'g.','MarkerSize',6,'Color',bluegreen);
    %plot(time,Weightmed*1e5,'g-');
    
    %plot(time,Weightav*1e5,'.','Color',[0. 0.4 0],'MarkerSize',6);  % Dot weight
end
hb=plot(time,Ball*1.e6,'k','LineWidth',2);hold on;
bl=Ball(dcl)*1.e6;
plot(tl,bl,'Color',[0  0 1],'LineWidth',2);
plot(time(dcld),Ball(dcld)*1e6,'bo','LineWidth',3,'MarkerSize',3);
plot(time(gh),Ball(gh)*1e6,'k.','LineWidth',2,'Color','r');

if showballast==1
    plot(time(gb),Weightav(gb)*1e5,'.','Color',rgb('darkgreen') );
end

if modelabel==1
    mm=find(diff(Mode)~=0 & ~isnan(diff(Mode)) );
    for i=1:length(mm)
        k=mm(i);
        s=smode(Mode(k+1)+1);
        zzl=Ball(k)*1e6+(Mode(k)-3);
        zzl(zzl>400)=400;
        ht=text(time(k),zzl,s);
        set(ht,'FontSize',9);
        hold on
        ht=text(time(k),(Mode(k)-3),s);
        set(ht,'FontSize',7);
    end
    hh=find(diff(Home)>0);
    for i=1:length(hh)
        k=hh(i);
        zzl=Ball(k)*1e6+1;
        zzl(zzl>400)=395;
        ht=text(time(k),zzl,'H','color','k','fontweight','b','FontSize',8);
        
        ht=text(time(k),1,'H','color','k','fontweight','b','FontSize',8);
    end
end

axis([a(1) a(2) -200 670]);
a=axis;
grid on

if (nolegend==2 || nolegend==3 ) && FM.LABSIMTEST~=1
    legend([hb hw hww hs],'Ballast','Weight (g)','Weight*100 (g)',['(\sigma_0-' num2str(SIGMED,'%6.3f') ')*1000']);
end
set(gcf,'Color','w');
zoom on
if (FM.LABSIMTEST~=1)
    title(sprintf('CTD # %d %d/%d/%d   %6.3f N  %8.3f W'...
        ,cast,sta.mo(cast),floor(sta.day(cast)),sta.yr(cast),...
        sta.lat(cast),abs(sta.lon(cast)) )  );
else
    title('LABTESTSIM');
end

if Datelabel
    datetick('x','dd/HHZ','keeplimits')
end


% ------------------- Temperature ------------------

axes(awd)   % TEMPERATURE - formerly weight
plot(THETA,P,'r-');
ylim([0 Zmax]);
hold on
plot(THETA(gmed),P(gmed),'g.');
plot(Thc,Pc,'k-');
grid on;
set(gca,'Yaxislocation','right','Ydir','reverse')
xlabel('Temperature/C');
title('Temperature vrs Depth');

if iprint==1
    dbclear all
    orient landscape
    
    
    if ~ispc
        inbox='/Users/eric/Documents/Acrobat/IN/In';
        print('-depsc2', [inbox '/Fig' sprintf('%s_%03d',ID,real)]);
        pause(pauseS);  % wait for printer
        dbstop if error
    else
        print('-dpdf', ['Data/Fig' num2str(real)]);
    end
end

%% plot the states
if StatePlot
    % the "State" consists of 4 bytes (most-to-least significant): stage, phase(icall), mode, and variation
    lbl = {'Stage','Phase(icall)','Mode','Variation'};
    
    figure;
    
    ha = subplot(4,1,1);
    plot(time,P);
    hold on;grid on
    title(['Float Simulation ' num2str(real) ' (states)  '  date])
    ylabel('Depth (m)');
    set(gca,'ydir','reverse');
    
    ha(2) = subplot(4,1,2:4);
    for k=1:length(lbl)
        
        m  = floor(mod(State,256^(5-k))/(256^(4-k)));
        plot(time,m+.1*(k-2),'linewidth',2);
        hold on;grid on
    end
    
    % mode labels?
    ml = [];
    for k=1:length(FM.smode)
        ml{k} = sprintf('[%c] %2d',FM.smode(k),k-1);
    end
    set(ha(2),'ytick',0:length(ml)-1,'yticklabel',ml,'TickLabelInterpreter','none')
    
    xlabel('Time');
    legend(lbl);
    linkaxes(ha,'x')
else
    disp('Skip StatePlot')
end

