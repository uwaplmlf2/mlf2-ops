% Get bocha power in excess of that at zero pressure
% add this to measured current in bench tests
% Reads directly from simulator output
%load '../Data-2007-12-21/Env.mat'

B=Ball*1e6;
yd=sec/86400;

% Bocha current = 1.2A at 300psi
% 13.5V
I=(P*1.45)*1.2/300;  % Extra Current used when bocha is on and against pressure
I0=0.1;  % surface current (not used here since already measured in test)
W=I*13.5;  % Extra Power used when bocha is on
W0=I0*13.5; % Surface Power
Speed=1;  % cc/sec
dB=diff(B);  % in cc
dB=[0 dB];
dt=dB./Speed.*(dB>0);  % seconds on in each interval working against pressure
dJ=W.*dt;
J=cumsum(dJ);  % cumulative energy  extra
KJ=J/1000;
dJ0=W0.*dt;
J0=cumsum(dJ0);  % cumulative energy background
KJ0=J0/1000;

h=plot(yd,P/100,yd,B/100,yd,KJ,yd,KJ0+KJ,'k');
legend(h,'P/100','B/100','Xtra KJ','Total KJ','location','best');
set(h(end),'LineWidth',2);
grid on
set(gcf,'Color','w');