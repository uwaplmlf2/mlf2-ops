function result = surfcheck_fft(r,p)
% surfcheck (FFT) algorithm in Matlab (for testing)
% new (experimental) version

MIN_SAMPLES = 100; % min number of good samples to attempt detection
P_ATM = 9.9; % atmospheric pressure (ideally, need to be able to change this, but we cannot!)
NOMINAL_SOUND_SPEED = 1500; % m/s, sound speed used by the altimeter
SOUND_SPEED = 1435; % m/s, best guess of the actual sound speed (MIZ: 1440, PS: 1483)

RANGE_FACTOR = 0.9962; %this is to account for altimeter tilt
TRANSDUCER_OFFSET = 0.04; % need to add to measured range
R_MIN = 0.25; % we shouldn't be able to get that close to the ice because of the antenna.

P_MAX = 100.+10.; %If the altimeter is deeper than its cutoff range (100m default), return is likely garbage. Note that we need to add P_ATMH_MAX = 10; % max. ice thickness for crude filter
H_MIN = -10; % min ice thickness (need to allow for waves and wrong Patm)
H_MAX = 10; % max. ice thickness for crude filter

H_SPIKE = 2; % spike threshold
BAD_H = NaN;
ICE_THRESHOLD = 0.2; %% ice/water threshold, discriminator  greater than this is classified as "water" (or smooth flat ice)
LF_CUTOFF = 20; % s, LF cutoff
HF_CUTOFF = 10; % s, HF cutoff


ICE_QUANTILE = 0.01; % return this quantile of draft

EMPIRICAL_RANGE_FACTOR = 0; % do empirical range factor?

n = length(r);
dt = 1; % s, sampling interval
Ny = 0.5/dt; % Hz, Nyquist
%% start the algorithm
% first pass (prior RANGE_FACTOR)
H = p-P_ATM - TRANSDUCER_OFFSET - r*RANGE_FACTOR*SOUND_SPEED/NOMINAL_SOUND_SPEED; %% note, that this is not real ice thickness, since P_ATM can be wrong


% fake <<<<<<<<<<<<<<<<<<<<<<<<<<<<
% H = sign(rand(size(H))-0.5);
% H = .5*cos(pi*(1:n)'+.01*randn(n,1));

H0 = H;
Hf = medfilt1(H,5);
% H = medfilt_repl(H,5,H_SPIKE);
% Hf = medfilt_repl(H,5);

% automatic spike threshold?
lU = sqrt(2*log(n)); % "Universal threshold", eq. 1 from  Wahl, T. (2003) Discussion of 'Despiking Acoustic Doppler Velocimeter Data', Journal of Hydraulic Engineering, p. 485-
MADstd = 1.483*nanmedian(abs(H-median(H)));
H_SPIKE_A = lU*MADstd;
H_SPIKE = H_SPIKE_A; %% use auto-threshold

ibad = (r<R_MIN | p<0 | p>P_MAX | H<H_MIN | H>H_MAX | abs(H-Hf)>H_SPIKE);
iok = ~ibad;
H(ibad) = BAD_H;
n_good = sum(iok);
% empirical range factor correction, including soundspeed correction (determied by range vs. pressure regression)
[ab,stat] = robustfit(r(iok),p(iok));
RANGE_FACTOR1 = ab(2);
    fprintf('Prior range factor: %.3f\n',RANGE_FACTOR*SOUND_SPEED/NOMINAL_SOUND_SPEED);

if EMPIRICAL_RANGE_FACTOR,
    fprintf('Using empirical range factor: %.3f\n',RANGE_FACTOR1);
    % second pass, updated range factor
    H = p-P_ATM - TRANSDUCER_OFFSET - r*RANGE_FACTOR1;
    H(ibad) = BAD_H;
else
    fprintf('Empirical range factor: %.3f, NOT USED\n',RANGE_FACTOR1);
end
% iok = ':';

% fill the gaps?
%  H = intnan(H);

% window?
% H = H.* hann(512);
% H = detrend(H);

qx = 0:.05:1;
Hq = quantile(H(iok),qx);


%% low pass?
f=(0:(n/2))'./(n*dt); % this is the FFT frequency scale
df = 1/(n*dt); % frequency quantum


ilo = find(f<=1/LF_CUTOFF & f>0);
ihi = find(f>1/HF_CUTOFF & f<f(end)); % exclude the last point (often shows strange max)

nLF = sum(ilo);
nHF = sum(ihi);


Hi = demean(H);
Hi(isnan(Hi)) = 0;

% % window would be here
% Hi = bsxfun(@times,Hi, hanning(n));

fH = fft(Hi);
fH = fH(1:n/2+1,:);
pH = 2*fH.*conj(fH)*(dt/n); % spectral density

% do it for pressure (error propagation)
fP = fft(p);
fP = fP(1:n/2+1,:);
pP = (0.01)^2 * 2*(fP.*conj(fP))*(dt/n); % Pressure spectral density * 1% (est. soundspeed error)


% mean LF/HF spectral levels

vLF = mean(pH(ilo,:));
vHF = mean(pH(ihi,:));

lvLF = log10(vLF);
lvHF = log10(vHF);

% error?
p = 0.95; % confidence

p1LF=(2*nLF)./chi2inv((1-p)/2,2*nLF);
p2LF=(2*nLF)./chi2inv((1+p)/2,2*nLF);
p1HF=(2*nHF)./chi2inv((1-p)/2,2*nHF);
p2HF=(2*nHF)./chi2inv((1+p)/2,2*nHF);


% classificator:

Z = @(LF,HF) log10(HF./LF);  c_title = 'log_{10}(pHF/pLF)';

%% plot
%
yl = 'auto';%[-2 2];
figure(1);
clf
paper(10,7,'landscape')
SP = @(n) my_subplot(2,3,n);
a = SP(1:2);
% plot(P,'k');
hold on
plot(H0,'k-','color',[1 1 1]*.75);
plot(find(ibad),H0(ibad),'rx','markersize',4);
plot(H,'b.-');
plot(Hf,'m-');
% plot(LPH,'m','linewidth',1.5);
ylim(yl)
yl = ylim;
xlim([1 n]);
plot([1 n],quantile(H(iok),ICE_QUANTILE)*[1 1],'r--');
text(n+10,quantile(H(iok),ICE_QUANTILE),sprintf('%.0f%%',ICE_QUANTILE*100),'color','r');
plot([1 n],quantile(H(iok),.50)*[1 1],'r--');
text(n+10,quantile(H(iok),.5),'50%','color','r');
plot([1 n],quantile(H(iok),1-ICE_QUANTILE)*[1 1],'r--');
text(n+10,quantile(H(iok),1-ICE_QUANTILE),sprintf('%.0f%%',100-ICE_QUANTILE*100),'color','r');

% xlim([1 length(p)]);
flipy
ylabel('Draft (m)');
xlabel('Sample');
legend('H_{ice}','H_{ice} filt','5-pt median',4);
% title(fn,'interpreter','none')
set(gca,'tag','main_plot'); % for future overplotting

a(2) = SP(3);
plot((0:(n-1))/(n-1),sort(H));
hold on
plot([0 1],quantile(H(iok),ICE_QUANTILE)*[1 1],'r--');
plot([0 1],quantile(H(iok),.50)*[1 1],'r--');
plot([0 1],quantile(H(iok),1-ICE_QUANTILE)*[1 1],'r--');

xlim([0 1]);
ylim(yl)
flipy
xlabel('CDF');
linkaxes(a,'y');

SP(4)
loglog(f, pH,'k')
hold on
loglog(f, pP,'c-')
plot(f(ilo),ilo*0+nanmean(pP(ilo)),'c--','linewidth',1);
plot(f(ihi),ihi*0+nanmean(pP(ihi)),'c--','linewidth',1);

plot(1/LF_CUTOFF*[1 1],ylim,'b--');
plot(1/HF_CUTOFF*[1 1],ylim,'r--');
plot(f(ilo),ilo*0+vLF,'b','linewidth',2);
plot(f(ihi),ihi*0+vHF,'r','linewidth',2);
text(mean(f(ilo))/2,vLF*2,'vLF','color','b','horizontalalignment','center');
text(mean(f(ihi))/2,vHF*2,'vHF','color','r','horizontalalignment','center');
xlim(f([2 end]));
ylim(10.^[-7 0]);
xlabel('f (Hz)');
ylabel('pH (m^2/Hz)');
legend('H', '1% of P','location','sw');
SP(5)

hold on
plot(lvLF, lvHF,'ko','markerfacecolor','r','markersize',8);

plot(lvLF*[1 1], lvHF.*[p1HF p2HF],'k','linewidth',1);
plot( lvLF.*[p1LF p2LF],lvHF*[1 1],'k','linewidth',1);

[x,y] = meshgrid(logspace(-4,2,10));
cl = -2:.5:2;
contour(log10(x),log10(y),Z(x,y),cl(cl>0),'color',[1 1 1]*.5);
hold on
contour(log10(x),log10(y),Z(x,y),[0 0],'-.','color',[1 1 1]*.5);
contour(log10(x),log10(y),Z(x,y),cl(cl<0),'-','color',[1 1 1]*.5);
contour(log10(x),log10(y),Z(x,y),[1 1]*ICE_THRESHOLD,'r-','linewidth',2)

xl  = xlim;
ylim(xl);
% labels?
for k=1:length(cl)
if cl(k)>0,
    xy0 = xl(2)-cl(k)+i*xl(2);
else
    xy0 = xl(2)+i*(xl(2)+cl(k));
end
texti(xy0+(1+1i)*.1,sprintf('%.1f',cl(k)),'rotation',45,'verticalalignment','middle','color',[1 1 1]*.5);
end


axis equal
text(mean(xlim)+.25*diff(xlim),mean(ylim)-.25*diff(ylim),'ICE','rotation',45,'horizontalalignment','center');
text(mean(xlim)-.25*diff(xlim),mean(ylim)+.25*diff(ylim),'WATER','rotation',45,'horizontalalignment','center');
xlabel('vLF (m^2/Hz)')
ylabel('vHF (m^2/Hz)')

D = Z(vLF, vHF);
if D>ICE_THRESHOLD
    s = sprintf('%d samples (%d good)\nlog_{10}(vHF/vLF)=%.2f>%.2f\n\\bf~~~ WATER ~~~\\rm',n,n_good,D,ICE_THRESHOLD);
    result = 'WATER';
else
    s = sprintf('%d samples (%d good)\nlog_{10}(vHF/vLF)=%.2f<%.2f\n\\bf*** ICE ***\\rm',n,n_good,D,ICE_THRESHOLD);
    result = 'ICE';
end
SP(6)
text(0,1,s,'fontsize',12);
ylim([0 1.1])
seta visible off