% PDFS of float depth
clear all

dopdf=0;

D=dir('Data/Data*.mat');
cm=ODVmap2();
N=length(D);
for i=1:N
    file=['Data/' D(i).name];
    kk=[5:strfind(D(i).name,'.')-1];
    sid=D(i).name(kk);id=str2num(sid);
    load(file);
    [n,z]=hist(P,[0:0.5:110]);
    cdf=cumsum(n)/sum(n);
    icol=round(id/N*size(cm,1));
    
    if ~dopdf
        subplot(2,1,1)
        loglog(z,cdf+1e-5,'-o','Color',cm(icol,:),'LineWidth',2);
        hold on
        text(130,10^(-(i-1)/4),sid,'Color',cm(icol,:),'FontSize',15,'FontWeight','Bold');
        
        subplot(2,1,2);
        plot(z,cdf*100,'-o','Color',cm(icol,:),'LineWidth',2);
        hold on
        k=mod(i,4)+24;
        text(z(k),cdf(k)*100,sid,'FontSize',14,'FontWeight','Bold')
        text(z(k-8),cdf(k-8)*100,[' ' sid],'FontSize',14,'FontWeight','Bold')
    end
    
    zp=z+0.5;
    pdf=diff(cdf);
    [peak,ipeak]=max(pdf);
    zpeak(id)=zp(ipeak);
    
    if dopdf
        hh=plot(z(2:end)-0.25,diff(cdf),'Color',cm(icol,:),'LineWidth',1+mod(i,3));hold on;xlim([5 35]);
        set(gcf,'Color','w');xlabel('Depth');ylabel('PDF');
        text(zpeak(id)-0.25,peak,sid,'Horizontalalignment','center');
    end
    
    if i==1
        cdfm=cdf;
    else
        cdfm=cdfm+cdf;
    end
end
if dopdf
    return
end
cdfm=cdfm/N;

subplot(2,1,1);
plot(z,cdfm+1e-5,'k-','LineWidth',4);
for lab=2:8
    text(lab,1e-5,num2str(lab),'VerticalAlignment','top')
end
set(gca,'xticklabel',['  1';' 10';'100'],'FontSize',12);
grid on
xlabel('Depth (m)');
xlim([0.5 110]);
ylabel('Cumulative Probability');
set(gcf,'Color','w');

subplot(2,1,2);
plot(z,cdfm*100,'k-','LineWidth',4);
grid on
xlabel('Depth (m)');
xlim([4 15]);
ylabel('Cumulative Probability %');
set(gcf,'Color','w');





