function PlotIce(fn)
% plot altimeter record
SAMPLE_FREQ = 1; % sampling frequency, Hz
P_ATM = 10; % atm. pressure
ALT_DELAY = 6; %seconds, delay before the altimeter sampling starts
% CORRECT_SVEL = 1; % do sound speed correction?


if nargin<1,
    [fn,pth] = uigetfile('*.dat','Select altimeter file (DAT or NMEA)');
    if ~ischar(fn)
        fn = [];
    else
        fn = fullfile(pth,fn);
        fprintf('Next time run\n PlotIce ''%s''\n',fn);
    end
    
end
pth = fileparts(fn);

f = fopen(fn,'r');%,'b');
if (f<0)
    error(sprintf('Cannot open file %s'),fn);
    return
end
s = fgets(f);
if s(1)=='$'
    %raw NMEA altimeter output
    fseek(f,0,'bof');
    A = [];
    while(1)
        a = textscan(f,'$PRVAT,%f,M,%f,dBar*%*c%*c','CollectOutput',1);
        A = [A;a{1}];
        fgets(f);
        if feof(f),
            break;
        end
    end
    
    r = A(:,1);
    p = A(:,2);
    Mtime0 = 0;
    sz = 32;
else
    %binary surfcheck output
    Mtime0 = datenum(s);
    n = str2num(  fgets(f));
    % check if we have uint16 or uint32 version
    p0 = ftell(f);
    fseek(f,0,'eof');
    p1 = ftell(f);
    fseek(f,p0,'bof');
    if (p1-p0==n*2*2)
%         old style (int16)
        a = fread(f,n*2,'uint16');
        sz = 16;
        fprintf('Warning, 16-bit altimeter file, will try to unwrap');
    else
%         new style
        a = fread(f,n*2,'uint32');
        sz = 32;
    end
    r = double(a(1:2:end))/1e3;
    p = double(a(2:2:end))/1e3; % this is pressure in dbar over Patm (10.0dbar)
end

fclose(f);

Mtime = Mtime0+(0:length(p)-1)/SAMPLE_FREQ/86400 + ALT_DELAY/86400;

xl = Mtime([1 end]);

% % raw file?
% fnr = fullfile(pth,['../altimeter_raw/alt_',datestr(Mtime0,'yyyymmdd_HHMMSS'),'.csv']);
%
% if exist(fnr,'file'),
%     fr = fopen(fnr);
%     A = textscan(fr,'%*s%f%*s%f%*s','delimiter',',');
%     r_r = A{1};
%     p_r = A{2};
%     fclose(fr);
% else
%     p_r = p*NaN;
%     r_r = p*NaN;
% end

% env file?
fne = fullfile(pth,'../env.mat')
if exist(fne,'file'),
    E = load(fne);
    ie = find(within(E.Mtime,xl));
    ie = ie(1)-1:ie(end)+1;
    ep = interp1(E.Mtime(ie),E.P(ie),Mtime).';
end

if sz==16,
    % unwrap
    wrap = find(ep>50 & p<50);
    p(wrap) = p(wrap)+ 65.535;
    wrap = find(ep>50 & r<50);
    r(wrap) = r(wrap)+ 65.535;
end

% if CORRECT_SVEL,
%     gz = 0:100;
%     E.Svel = sw_svel(E.S,E.T,E.P);
%     gSvel = bindata(E.P,E.Svel,gz);
%     gSvel = intnan(gSvel );
%     iz = find(gz<=nanmean(p));
%     c = nanmean(gSvel(iz));
%     fprintf('Soundspeed correction factor: %.3f\n',c/SVEL0);
%     r0 = r;
%     r = r0*c/SVEL0;
%     figure(15);
%     clf;
%     plot(Mtime,p-r0);
%     hold on
%     plot(Mtime,p-r,'r');
%     datetick x keeplimits
%     ylabel('Ice draft (m)');
%
% end

% fast pressure?
fnf = fullfile(pth,'../fpr_0001.nc');
if exist(fnf,'file'),
    FP = ncload(fnf);
    FP.Mtime = FP.time/86400+datenum(1970,1,1);
    ifp = find(within(FP.Mtime,xl));
    ifp = ifp(1)-1:ifp(end)+1;
else
    FP = [];
end

figure(10)
paper(10,4);
clf
plot(Mtime,p-P_ATM,'k.-')
hold on
text(xl(2)+diff(xl)*.01,p(end)-P_ATM,'P-P_0','color','k');

% plot(Mtime,p_r,'k--')
% text(xl(2)+diff(xl)*.01,p_r(end),'P_r','color','k');

plot(Mtime,r,'r');
text(xl(2)+diff(xl)*.01,r(end),'R','color','r');

% if ~isempty(E),
%     plot(E.Mtime(ie),E.P(ie),'g.-');
%     text(xl(2)+diff(xl)*.01,E.P(ie(end)),'P_{fl}','color','g');
% end
if ~isempty(FP),
    plot(FP.Mtime(ifp),FP.pressure0(ifp),'b-');
    text(xl(2)+diff(xl)*.01,FP.pressure0(ifp(end)),'FP_{fl}','color','b');
end

xlim(xl);
flipy
ylabel ('Range/Pressure (m)');
suptitle(fn,'interpreter','none')
datetick keeplimits



surfcheck_fft(r,p);
suptitle(fn,'interpreter','none')
