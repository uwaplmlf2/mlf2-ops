% Test the real surfcheck.c using a synthetic data, made up from AWAC ice
% record and siulator pressure
clear
% these parameters match those in Surfcheck.c (but can also check what
% happens if they don't)
P_ATM = 9.9; % "true" atmospheric pressure
NOMINAL_SOUND_SPEED = 1500; % m/s, sound speed used by the altimeter
SOUND_SPEED = 1425;1435; % m/s, best guess of the actual sound speed (MIZ: 1440, PS: 1483)
RANGE_FACTOR = 0.9962; %this is to account for altimeter tilt
TRANSDUCER_OFFSET = 0.04; % need to add to measured range

%
fn_out = 'awac_altimeter_test';
surfcheck_cmd = 'H:\MLF\CODE\AUX_c\Debug\surfcheck.exe'; 

%  select one AWAC burst 
% good ones to try: 
% for SAMPLE: 5, 12 (waves), 30 (big waves), 70 (waves+noise), 205
% (smooth), 318 (slight undulation), 360 (mixed), 393 (flat floes) 
% for SAMPLE_SPRING: 8, 32 (open), 79 (smooth), 255(stteps)
ai = 12;255; 
ft = 0.547; % starting moment from the simulation

% load AWAC_sample
A = load('H:\IceFloat\AWAC\AWAC_sample');
A.P(abs(A.P-65.535)<eps) = NaN; % apparentely, this is the "bad pressure" flag (?)
A.R = A.R1; % use the first range
A.D= A.P-A.R; % draft
% D = load ('AWAC_sen'); % for T/S

% load float simulator
F = load('H:\MLF\CODE\MIZ\Data\Data1.mat'); 
iok = ~isnan(F.sec);
for_each_field F '*=*(iok);'
fi = find_nearest(F.sec/86400,ft);



%% get the "float" pressure time series
N = 500; % 500-second sample
D = A.D(1:N,ai); % draft
iok = diff(F.sec)>0;% skip repetitions
P = interp1(F.sec(iok)-F.sec(fi),F.P(iok),(1:N)','pchip');

% calculate the range, convert to nominal 1500m/s soundspeed
R = (P-D-TRANSDUCER_OFFSET)*(NOMINAL_SOUND_SPEED/SOUND_SPEED)/RANGE_FACTOR;

f = fopen([fn_out,'.csv'],'wt');
for k=1:N;
    s = sprintf('PRVAT,%06.3f,M,%08.3f,dBar',R(k),P(k)+P_ATM);
    checksum = cumxor(s);
%     if rand(1)<bad_prob, 
%         s = sprintf('PRVAT,%06.3f,M,%08.3f,dBar',0,0); % also, the checksum wouldn't match
%     end
    fprintf(f,'$%s*%x\n',s,checksum);
end
fclose(f);


%% run the real surfcheck
c = sprintf('%s %s',surfcheck_cmd ,[fn_out,'.csv'])
r=system(c);
fprintf('Surfcheck returns %d ',r);
if bitand(r,1)
    fprintf('(ICE)\n');
else
    fprintf('(WATER)\n');
end
    
if (r>0),
    fprintf('Draft=%.1fm\n', bitand(r,127*2)/2/10);
end
    

% run PlotIce
PlotIce([fn_out,'.dat']);
a = findobj(gcf,'tag','main_plot');
axes(a);
hold on
plot(D,'g-');
