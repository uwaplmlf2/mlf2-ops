% Diagnose sampling
% Run after simulation

[x,x,x,x,Ptable] = ballast(0,0,0,0,0,0,0,0,0,0,0); % note that calling it with T=S=0 skips the actual ballasting cycle - just what I need here!
Down.speed=get_param(Ptable,'down.speed');
Down.Brate=get_param(Ptable,'down.brate');
Up.speed=get_param(Ptable,'up.speed');
Up.Brate=get_param(Ptable,'up.brate');

figure;
dP=diff(P);%./diff(time*86400);
g1=find(Mode==2,1,'first');
g2=find(time>time(g1) & Mode==0,1,'last');
g=g1:g2;
color_mark(dP(g),P(g),time(g));
grid on;
set(gca,'ydir','reverse');
set(gcf,'Color','w');

xlabel('Pressure step between samples');
ylabel('Pressure');
title('ORCA float Simulation - No wire drag');
xl=xlim;
text(0,30,sprintf('Down.Speed %4.2f\nDown.Brate %6.2e\nUp.Speed %4.2f\nUp.Brate %6.2e\n\nCycle time %4.2f hr\n%d Points',...
   Down.speed,Down.Brate,Up.speed,Up.Brate,(time(g2)-time(g1))*24,g2-g1),'FontSize',12,'FontWeight','bold');

print('-dpng','Diagnosis');



