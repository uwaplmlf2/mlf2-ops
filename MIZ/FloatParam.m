% floatparam.m for MIZ float #80
floatid = '80';
FM.smode= 'DSUIKM^RPB-GC!>0ZX'
   
FM.DOWN = 0;
FM.SETTLE= 1;
FM.UP= 2;
FM.ISO= 3;
FM.SEEK= 4;
FM.ML= 5;
FM.SURF= 6;
FM.SERVO_P = 7;
FM.SETTLE_P= 8;
FM.BOCHA= 9;
FM.NMODE= 10;
FM.GPS= 11;
FM.COMM= 12;
FM.ERR= 13;
FM.DONE= 14;
FM.START= 15;
FM.SLEEP= 16;
FM.XFER= 17;

FM.mass0= 54.297;
FM.vol0= 52822.e-6;
FM.air= 1.3500e-005;
FM.chi= 3.15000e-006;
FM.alpha= 7.3600e-005;
FM.creep = 0;
FM.Tref = 8;

FM.Soffset1=0;
FM.Soffset2=0;