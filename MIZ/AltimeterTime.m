% PDFS of float depth
clear all

D=dir('Data/Data*.mat');
cm=ODVmap2();
N=length(D);
for i=1:N
    file=['Data/' D(i).name];
    kk=[5:strfind(D(i).name,'.')-1];
    sid=D(i).name(kk);id=str2num(sid);
    load(file);
    icol=round(id/N*size(cm,1));
    
    k0=find(abs(sec-75700)<50,1);
    plot(sec-75700,-P,'color',cm(icol,:),'LineWidth',2);
    hold on
    k=k0-i*2;
    text(sec(k)-75700,-P(k),sid,'FontSize',14);
end
xlim([-4000 500]);
ylim([-32 -5]);
xlabel('Seconds before dive');
ylabel('-Pressure dbar');
    

set(gcf,'Color','w');





