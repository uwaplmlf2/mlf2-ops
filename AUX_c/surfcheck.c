/**
 surfcheck: surface ice detection 

 CALL: 
  surfcheck alt.csv
 
 alt.csv contains Valeport altimeter output in NMEA format, e.g., 
 $PRVAT,09.273,M,0014.325,dBar*37

 OUTPUT:
 bit  0   - type flag (0=smooth, likely open water, 1=rough, likely ice)
 bits 1:6 - ice draft in dm (0-63), 63 corresponds to ice thicker than 6.2 m. 
 bit  7   - error flag:
  -1  	if ice thickness cannot be estimated (possible reasons: not enough good data, altimeter malfunction, etc.)
 -127  	if there was some other error (I/O, etc.)
 
 Ice (surface) draft is measured relative to nominal atmospheric pressure (set by P_ATM constant). If the calling 
 program has a better estimate of atmospheric pressure, it will be able to determine the ice draft more accurately. 

 REQUIRES:
 kiss_fft (http://sourceforge.net/projects/kissfft/)
 kiss_fft130/kiss_fft.c
 kiss_fft130/TOOLS/kiss_fftr.c
**/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>	
#include <time.h>	
#include <ctype.h>
#include <sys/stat.h>
#include "kiss_fft130/tools/kiss_fftr.h"

// return values
#define THICK_ICE (127)
#define NO_DECISION (-1)
#define FATAL_ERROR (-127)

// surface type flag (last bit of the result)
// note that "WATER" may mean smooth ice as well
#define WATER (0)
#define ICE (1)

// some ice detection parameters here
const float SAMPLE_INTERVAL = 1; //s, nominal altimeter sampling interval
const int MIN_SAMPLES = 100; // min number of good samples to attempt detection
const float P_ATM = 9.90; // atmospheric pressure (ideally, need to be able to change this, but we cannot!)
const float NOMINAL_SOUND_SPEED = 1500; // m/s, sound speed used by the altimeter
const float SOUND_SPEED = 1435; // m/s, best guess of the actual sound speed
const float RANGE_FACTOR = 0.9962; // this is to account for 5-degree altimeter tilt
const float TRANSDUCER_OFFSET = 0.04; // need to add to measured range
const float R_MIN = 0.25; // we shouldn't be able to get that close to the ice because of the antenna.
const float P_MAX = 100.+10.; //If the altimeter is deeper than its cutoff range (100m default), return is likely garbage. Note that we need to add P_ATM
const float H_MAX = 10; // max. ice thickness for crude filter
const float H_MIN = -10; // min. ice thickness
const float H_SPIKE = 2; // spike threshold
const float BAD = -100; // bad number flag (can't use 0, since the inferred ice thickness can be negative)
const float LF_CUTOFF = 20; //s, cutoff period for low-frequency portion
const float HF_CUTOFF = 10; //s, cutoff period for high-frequency portion

const float ICE_QUANTILE = 0.99; // return this quantile of draft (0 - min, 0.5 - median, 1.0 - max draft)
const float ICE_THRESHOLD = 0.2; // ice/water threshold, discriminator greater than this is classified as "water" (or smooth flat ice)
const float ICE_RETURN_SCALE = 10; // scaling of the returned ice thickness 


const char MSG[]="$PRVAT"; // altimeter message header
#define MSGLEN (32) // message length is fixed
#define FNLEN (200) // max. length of a file name

const float QL_SCALE = 1000;
const int QL_DECIMATE = 1; // decimates the raw data by this factor

FILE *flog = NULL; // logfile 


// function declarations 
int ice_detection(float *R, float *P, int count);
int pack_return(float H, int type);
float atof_fast(const char *p);
int check_message(char* s);
void get_file_timestamp(const char *fn, struct tm *tm);
void filename_extension(char *fn, const char*ext, char* s);
void write_log(const char *fmt, ...);
void write_QL(const char *fn, float *r, float *p, int count, struct tm *tm);

int comp_descending (const void * elem1, const void * elem2);
float Bfilt(struct butter * , float , float, short , struct butter );
void filt(float *x, int count, struct butter P);
void minmax(float *x, int count, float *mi, float *ma);
float mean(float *x, int count);
float std(float *x, int count);
void median_filter(float* X, int count, float* Y);

time_t timegm(struct tm * a_tm);

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define CLIP(x, mi, ma) MIN(MAX(x,mi),ma)

int ice_detection(float *R, float *P, int count){
	float *H, *Hsort, *Hfilt; // , *LPH, *HPH;
	float Rmin, Rmax, Rmean, Pmin, Pmax, Pmean, Pstd,
			Hmin, Hmax, Hmean, Hstd,
			vLF, vHF;
	int k,n, p, nLF, nHF;
	#define NPC (20) // 20(21) percentile bins 0% 5%, 10%, ..., 95%, 100%
	float Hpc[NPC+1]; //percentiles  
	float Hest; // final estimate of the draft
	float D; // discriminator = log10(vHP/vLP)
	int nfft;
	kiss_fftr_cfg cfg =0 ;
	kiss_fft_scalar *X = 0;
	kiss_fft_cpx *sout =0;

	int type; // surface type
	int result = FATAL_ERROR;

	if (count<MIN_SAMPLES)
	{
		write_log("Not enough samples (%d).", count);
		return(NO_DECISION);
	}


	H = (float*)malloc(count*sizeof(float));
	Hfilt = (float*)malloc(count*sizeof(float));
	Hsort = (float*)malloc(count*sizeof(float));
	
	if (!H || !Hsort || !Hfilt) // || !LPH || !HPH)
	{
		write_log("Error: cannot allocate memory (ice_detection/H).");
		goto cleanup;
	}


	/*************************/
	/* Compute ice dtaft (H) */
	/*************************/

	for (k=0;k<count;k++)
        H[k] = P[k] - P_ATM - TRANSDUCER_OFFSET - R[k] * RANGE_FACTOR * (SOUND_SPEED / NOMINAL_SOUND_SPEED); // note, that this is not real ice thickness, since P_ATM can be wrong
	// median filter(for despiking)
	median_filter(H, count, Hfilt);

		// validation pass: apply limits and de-spike
	n = 0;
	for (k = 0; k < count; k++)
	{
		// crude limits test
		if (R[k] < R_MIN || P[k]<0 || P[k]>P_MAX || H[k] < H_MIN || H[k]>H_MAX || fabs(H[k] - Hfilt[k])>H_SPIKE) 
			H[k] = BAD; // flag as bad data
		else
			n++; // good data counter
	}

	/*****************************************/
	/*  compute some stats, write to the log */
	/*****************************************/
	minmax(P, count, &Pmin, &Pmax);
	minmax(R, count, &Rmin, &Rmax);
	minmax(H, count, &Hmin, &Hmax);
	Pmean = mean(P,count);
    Pstd = std(P, count);
    Rmean = mean(R, count);
	Hmean = mean(H, count);
	Hstd = std(H, count);

	write_log("%d samples (%d good)",count,n);
	write_log("P: %.3f <%.3f> %.3f",Pmin,Pmean,Pmax);
	write_log("R: %.3f <%.3f> %.3f",Rmin,Rmean,Rmax);
	write_log("H: %.3f <%.3f> %.3f",Hmin,Hmean,Hmax);
	
	if (n<MIN_SAMPLES)
	{
		write_log("Not enough samples after filter (%d).", n);
		result = NO_DECISION;
		goto cleanup;
	}
	/*******************/
	/* Compute spectra */
	/*******************/

	nfft = 2*(int)(count/2+0.75); // make sure it is even and nfft>=count

	cfg = kiss_fftr_alloc(nfft, 0, 0, 0);
	X = (kiss_fft_scalar*)malloc((nfft)*sizeof(kiss_fft_scalar)); 
	sout = (kiss_fft_cpx*)malloc((nfft/2+1)*sizeof(kiss_fft_cpx));

	if (!cfg || !X || !sout)
	{
		write_log("Error: cannot allocate memory (ice_detectopn/fft).");
		goto cleanup;
	}

	for (k = 0; k < count; k++) 
	{
		X[k] = (H[k]==BAD)? 0:(H[k] - Hmean); // copy H while removing mean and filling missing data with 0
	}
	if (nfft>count)
		X[nfft - 1] = 0;// if count was odd, set extra element to 0.
  // FFT:
	kiss_fftr(cfg, X, sout);

	// compute spectral density 
	// will put it back into X
	for (k = 0; k < nfft/2+1; k++)
		X[k] = 2 * (sout[k].r*sout[k].r + sout[k].i*sout[k].i)*SAMPLE_INTERVAL / nfft;
	
	// Frequencies for this will be (0:(nfft/2))./(nfft*dt)

	// Get average HP/LP spectral levels
	vHF = 0; vLF = 0;
	nHF = 0; nLF = 0;
	for (k = 1; k < nfft/2 + 1 -1; k++) // note that I skip the last element (@Nyquist), since it often shows a spike; first (f=0) as well
	{
		float f = k / (nfft*SAMPLE_INTERVAL); // frequency band
		if (f <= 1 / LF_CUTOFF)
			{
				vLF += X[k];
				nLF++;
			}
        else if (f > 1 / HF_CUTOFF)
		{
			vHF += X[k];
			nHF++;
		}
	}
	vHF /= nHF;
	vLF /= nLF;
	D = log10f(vHF / vLF);
	type = (D>ICE_THRESHOLD) ? WATER : ICE;

	

	write_log("HF/LF lvl: %.3f / %.3f", vHF, vLF);
	write_log("D: %.3f (%s)", D, type? "ice":"water");

	// sort max to min 
	memcpy(Hsort, H, count*sizeof(float));
	qsort(Hsort,count,sizeof(Hsort[0]), comp_descending); // note that bad numbers will be sorted to the end

	// figure out the percentile indices (just for the information at this point)
    // Note that since the ordering is max-to-min, percentiles need to be reversed!
	write_log("%% H");
	for (k = 0; k <= NPC; k++)
	{
		float pc = (float)k / NPC;
		p = (int)CLIP(n*(1 - pc) - 0.5, 0, n - 1);
		Hpc[k] = Hsort[p];
		write_log("%.0f\t%.3f", pc * 100, Hpc[k]);
	}

	// for now, just return the 95-99th percentile (thickest 1-5%), as set by ICE_QUANTILE
	p = (int)CLIP(n*(1 - ICE_QUANTILE) - 0.5, 0, n - 1);
	Hest = Hsort[p];
	result = pack_return(Hest, type);
    write_log("Est. draft: %.1f (%.0f%%)", Hest, ICE_QUANTILE*100.);

cleanup:
	free(H);
	free(Hsort);
	free(Hfilt);
	free(cfg);
	free(X);
	free(sout);
	return result;
}

int pack_return(float H, int type)
{
	// pack ice draft and surface type in 7-bit output value:
	// bit 0 - type flag (0=smooth, likely open water, 1=rough, likely ice)
	// bits 1:6 - thickness in dm (0-63)
	//  

	int result;
	result = (int)(H*ICE_RETURN_SCALE) + 1; // round up, don't return 0
    result = MIN(MAX(result,1), 63); // clip to 1:63 range
	return ((result << 1) + type);
}

int main (int argc, char *argv[])
{
	FILE *af;
	int count;
	char buf[MSGLEN+5];
	char fn_log[FNLEN];
    char fn_ql[FNLEN];
	float *r, *p;
	struct tm tstamp;
	int result;

	if (argc!=2)
	{
		return FATAL_ERROR; // file name was not supplied
	}

	// obtain next file name in sequence for the log & QL
	// file_number = get_next_file_number(log_fmt);

    // get timestamp
    get_file_timestamp(argv[1], &tstamp);
    // make log and QL filenames (in current directory!)
    filename_extension(argv[1], ".log", fn_log);
    filename_extension(argv[1], ".dat", fn_ql);

    
	flog = fopen(fn_log, "w");
	write_log("%s", argv[1]);


	af = fopen(argv[1],"rb");
	if (af==NULL)
	{
		write_log("Error: cannot open file");
		return FATAL_ERROR; // can't open the file
	}

	// read the file once, counting the lines that look ok 
	count=0;
	while(fgets(buf, sizeof(buf)-1, af) != NULL)
	{
		if(buf[0] == '$') count++;
	}

	// allocate memory for the data
	r = (float*) malloc(count*sizeof(float));
	p = (float*) malloc(count*sizeof(float));

	if (r==NULL || p==NULL)
	{
		// can't allocate memory
		fclose(af);
		write_log("Error: cannot allocate memory.");
		result= FATAL_ERROR;
		goto cleanup;
	}
	// read it
	fseek(af,0,SEEK_SET);
	count=0;
	while(fgets(buf, sizeof(buf)-1, af) != NULL)
	{
		if (strlen(buf)>=MSGLEN && strncmp(buf, MSG, sizeof(MSG)-1)==0 )
		{
			// seems like the right message 
			if (check_message(buf))
			{
				// parse it...
				// Note that Valeport outputs the fixed length messags, so instead of NMEA parsing I can just take the known chunks:
				//	$PRVAT,09.273,M,0014.325,dBar*37
				//         **r***   ***p****
				r[count] = atof_fast(buf+7);
				p[count] = atof_fast(buf+16);
			}
			else
			{
				//message is bad, but we need to include it				
				r[count] = 0;
				p[count] = 0;
			}
			count++;
		}
	}
	fclose(af);


	// save QL file
	// sprintf(fn, QL_fmt, file_number);

	write_QL(fn_ql, r, p, count, &tstamp);

	// run ice detection algorithm
	result = ice_detection(r,p, count);

cleanup:
    free(r);
	free(p);
	if (flog != NULL) fclose(flog);

	return result;
}



//////////////////////
int comp_descending(const void * elem1, const void * elem2) 
{
	// element comparison for qsort
	float f = *((float*)elem1);
	float s = *((float*)elem2);
	return -((f > s) - (f < s)); // return 1 if f<s
}

void get_file_timestamp(const char *fn, struct tm *tm)
{
	// get the timestamp from the altimeter file (has format "alt_Ymd_HMS.csv")
	const char *p, *pd;
	// find beginning of the file name
    p = fn;
    pd = fn;
	while (*p)
	{
		if (*p == '\\' || *p == '/') pd = p+1;
		p++;
	}
	// move to the date
	pd += 4;
	memset(tm, 0, sizeof(struct tm));
	if (sscanf(pd, "%4d%2d%2d_%2d%2d%2d", &tm->tm_year, &tm->tm_mon, &tm->tm_mday, &tm->tm_hour, &tm->tm_min, &tm->tm_sec) == 6)
	{
		// fix some...
		tm->tm_year -= 1900;
		tm->tm_mon--;
	}
}

void filename_extension(char *fn, const char*ext, char* s){
    // returns the file name with out the path and the extension changed
    // extension should include the dot (".dat")
    char *p, *pd = NULL;

    if (strlen(fn) >= FNLEN-3){
        // input too long, return empty string
        *s = 0;
        return;
    }
    // find beginning of the file name
    p = fn;
    pd = fn;
    while (*p)
    {
        if (*p == '\\' || *p == '/') pd = p + 1;
        p++;
    }
	strcpy(s,pd);
	// find extension (no more floders!)
    p = s;
    pd = NULL;
	while (*p){
		if (*p=='.') pd = p; // position of the "."
		p++;
	}
    
    if (pd == NULL)
        pd = p; // will append

	// change extension 
	strcpy(pd,ext);
}


void write_log(const char *fmt, ...)
{
	// writes a log entry (no need to include \n)
    va_list	args;
    va_start(args, fmt);
    if (flog!=NULL) 
	{
		vfprintf(flog, fmt, args);
		fprintf(flog,"\n");
	}
    va_end(args);
}

void write_QL(const char *fn, float *r, float *p, int count, struct tm * tm)
{
// save altimeter data in compact form
// First line is date/time, second - number of readings (count), then pairs of r & p as uint32


	FILE * ql;
	int k;
	unsigned long d[2];
	

	ql = fopen(fn, "wb");
    if (!ql){
        write_log("Error: cannot open QL file (%s).",fn);
        return;
    }

	fprintf(ql, "%d/%d/%d %02d:%02d:%02d\n%d\n", tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec, (int)(count / QL_DECIMATE));
	for (k = 0; k < count; k+=QL_DECIMATE)
	{
		d[0] = (unsigned long)(r[k] * QL_SCALE + 0.5);
		d[1] = (unsigned long)(p[k] * QL_SCALE + 0.5);
		fwrite(d, sizeof(d[1]), 2, ql);
	}

	fclose(ql);
}



static int hex(int c)
{
    return isdigit(c) ? (c - '0') : ((tolower(c) - 'a') + 10);
}

int check_message(char* s)
{
	// check if the message checksum (cumulative XOR of everything between $ and *) matches
   char *p=s+1;
   unsigned sum=0,csum;
   while (*p!=0 && *p!='*')
   {
	   sum^=*p;
	   p++;
   }
   if (*p==0 || *(p+1)==0 || *(p+2)==0) return 0;// line ended too soon
   // checksum
   csum = hex(*(p+1))*16+hex(*(p+2));
   return (sum==csum);
}



/** simple and fast atof for fixed point numbers (no exponent), not necessarily bit-accurate.
 Returns 0 as an error flag.
 Modified from: http://tinodidriksen.com/2011/05/28/cpp-convert-string-to-float-speed/
**/

#define white_space(c) ((c) == ' ' || (c) == '\t')
#define valid_digit(c) ((c) >= '0' && (c) <= '9')
 
float atof_fast(const char *p) 
{
	float r = 0.0;
    int neg = 0;


    // Skip leading white space, if any.
    while (white_space(*p) ) 
	{
        p++;
    }
 
    // Get the sign!
    if (*p == '-') 
	{
        neg = 1;
        p++;
    }
	else if(*p == '+')
	{
        neg = 0;
        p++;
    }
 
    // Get the digits before decimal point
    while (valid_digit(*p)) 
	{
        r = (r*10) + (*p - '0');
        p++; 
    }
 
    // Get the digits after decimal point
    if (*p == '.') 
	{
        float f = 0.0;
        float scale = 1.0;
        p++;
        while (*p >= '0' && *p <= '9') 
		{
            f = (f*10) + (*p - '0');
            p++;
            scale*=10;
        }
        r += f / scale;
    }
 
    // no need to get exponent
    if(neg){ r = -r;}
 
    return r;
}

	void minmax(float* x, int count, float *mi, float *ma)
	{
		// get minimum & maximum of an array, ignoring bad numbers
		int k;
		*mi = 1e10;
		*ma = -1e10;
		for (k = 0; k<count; k++)
			if (x[k] != BAD)
			{
				*mi = MIN(x[k], *mi);
				*ma = MAX(x[k], *ma);
			}
	}
	float mean(float *x, int count)
	{
		// array mean, ignoring bad numbers
		float r=0;
		int k,n=0;
		for (k = 0; k<count; k++)
			if (x[k] != BAD)
			{
				r += x[k];
				n++;
			}
			r /= n;
			return r;
	}
	float std(float *x, int count)
	{
		// array standard deviation, ignoring bad numbers
		float r = 0, xm;
		int k,n=0;
		xm = mean(x, count);
		for (k = 0; k<count; k++)
			if (x[k] != BAD)
			{
				r += (x[k] - xm)*(x[k] - xm);
				n++;
			}
		if (n == 1)
				return 0;
		r /= n-1;
		r = sqrtf(r);
		return r;
	}


#define PIX_SORT(a,b) { if ((a)<(b)) PIX_SWAP((a),(b)); } // was ">". for me it is easier to sort descending
#define PIX_SWAP(a,b) { float temp=(a);(a)=(b);(b)=temp; }
	/*----------------------------------------------------------------------------
	Function :   opt_med5()
	In       :   pointer to array of 5 pixel values
	Out      :   a pixelvalue
	Job      :   optimized search of the median of 5 pixel values
	Notice   :   found on sci.image.processing
	cannot go faster unless assumptions are made
	on the nature of the input signal.
	---------------------------------------------------------------------------*/
	float opt_med5(float * p)
	{
		PIX_SORT(p[0], p[1]); PIX_SORT(p[3], p[4]); PIX_SORT(p[0], p[3]);
		PIX_SORT(p[1], p[4]); PIX_SORT(p[1], p[2]); PIX_SORT(p[2], p[3]);
		PIX_SORT(p[1], p[2]); 
		
		// modification to allow for bad data:
		// since BAD is large negative, bad numbers will be near the end. Check how many
		if (p[4] != BAD)
			return(p[2]); // no bad data
		else if (p[1] == BAD)
			return p[0]; // one or no good data. Return first, anyway
		else if (p[2] == BAD)
			return((p[0] + p[1]) / 2); // two good points
		else if (p[3] == BAD)
			return(p[1]); // 3 good points
		else 
			return((p[1] + p[2]) / 2); // 4 good points
		
	}

	void median_filter(float* X, int count, float* Y)
	{
		// 5-point median filter
		// skips over bad numbers
		// output is in Y (must be allocated with the same size as X)
		float xi[5]; // buffer
		int k;
		for (k = 0; k < count; k++)
		{
			if (k<2 || k>count - 3)
				Y[k] = X[k]; // keep the ends unchanged (?)
			else
			{
				// fill the buffer
				memcpy(xi, X + k - 2, 5 * sizeof(float));
				Y[k] = opt_med5(xi);
			}
		}
	}

#ifdef WIN32
	// timegm should exist on linux
	time_t timegm(struct tm * a_tm)
	{
		time_t ltime = mktime(a_tm);
		struct tm tm_val;
		gmtime_s(&tm_val, &ltime);
		int offset = (tm_val.tm_hour - a_tm->tm_hour);
		if (offset > 12)
		{
			offset = 24 - offset;
		}
		time_t utc = mktime(a_tm) - offset * 3600;
		return utc;
	}
#endif