% test algorithm
clear
% fn = 'awac_test_005_waves.csv';
fn = 'awac_test_030_big_waves.csv';
% fn = 'awac_test_205_flat.csv';
% fn = 'awac_test_070_waves_noise.csv';
% fn = 'awac_test_070_waves_clean.csv';
 fn = 'awac_test_300_partial.csv';
% fn = 'awac_test_393_floes.csv';
fn = 'awac_test_255_steps.csv';
% fn = 'alt_20150218_123456.csv';

MIN_SAMPLES = 100; % min number of good samples to attempt detection
P_ATM = 10; % atmospheric pressure (ideally, need to be able to change this, but we cannot!)
RANGE_FACTOR = 1; %this is to account for altimeter tilt and soundspeed correction (how?)
TRANSDUCER_OFFSET = 0.04; % need to add to measured range
R_MIN = 0.25; % we shouldn't be able to get that close to the ice because of the antenna.
H_MAX = 10; % max. ice thickness for crude filter
H_MIN = -10; % min ice thickness (need to allow for waves and wrong Patm)
H_SPIKE = 2; % spike threshold (will be replaced by 5-pt median)
BAD_H = NaN;
WATER_pHP = 0.8; %% water threshold, pHP greater than this is classified as "water" (or smooth flat ice)

% $PRVAT,00.695,M,0010.990,dBar*32
A = [];
f = fopen(fn);
while(1)
    a = textscan(f,'$PRVAT,%f,M,%f,dBar*%*c%*c','CollectOutput',1);
	A = [A;a{1}];
    fgets(f);
    if feof(f),
        break;
    end
end
fclose(f);

% truncate?
% A = A(1:250,:);


r = A(:,1);
p = A(:,2);
n = length(r);
dt = 1; % s, sampling interval
Ny = 0.5/dt; % Hz, Nyquist
%% start the algorithm

H = p-P_ATM - (r+TRANSDUCER_OFFSET)*RANGE_FACTOR; %% note, that this is not real ice thickness, since P_ATM can be wrong
H0 = H;
Hf = medfilt1(H,5);


iok = (r>R_MIN & p>0 & H<H_MAX & H>H_MIN & abs(H-Hf)<H_SPIKE);
H(~iok) = BAD_H;

% fill the gaps?
% h = intnan(h);

% window?
% H = H.* hann(512);
% H = detrend(H);

qx = 0:.05:1;
Hq = quantile(H(iok),qx);



%% low pass?
T0 = 20; % s, cutoff
F0 = (1/T0)/Ny; % cutoff frequency, normalized by Ny 
[fb,fa] = butter(2,F0); 
LPH = H;
ic = filtic(fb,fa,H(1)*[1 1]',H(1)*[1 1]');
LPH(iok) = filtfilt(fb,fa,H(iok));
HPH = H - LPH;

%% bimodality
[mu,si,p,pmode] = GMM(H,2);
MD = abs(diff(mu))./sum(si); % separation of the modes vs. their STD

%%
%
figure(1);
clf
plot(p,'k');
hold on
plot(H0,'r');
plot(H,'b');
plot(LPH,'m','linewidth',1.5);
ylim([-.5 .5])
% xlim([1 length(p)]);
flipy
ylabel('Draft (m)');
xlabel('Sample');
legend('P','H_{ice}','H_{ice} filt','LP',4);
%%
figure(2)
clf
bigspec(H,1,8,2,'k','noerr')
hold on
bigspec(LPH,1,8,2,'b','noerr')
bigspec(HPH,1,8,2,'r','noerr')
plot(1/T0*[1 1],ylim,'k--');

%%
figure(3)
clf
plot(sort(H),(0:(n-1))/(n-1));
hold on
plot(quantile(H(iok),.05)*[1 1],[0 1],'r');
plot(quantile(H(iok),.50)*[1 1],[0 1],'r');
plot(quantile(H(iok),.95)*[1 1],[0 1],'r');

%%
figure(4);
clf

loglog(nanstd(LPH),nanstd(HPH),'*');
hold on
xx = logspace(-3,1,2);
loglog(xx, xx/sqrt(1-WATER_pHP^2),'r');
xlim([1e-3 10])
ylim([10^-2.5 10]);