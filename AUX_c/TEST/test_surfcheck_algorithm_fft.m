% test FFT-based algorithm
% Runs on Valeport altimeter output files - simulated or real

clear
% fn = 'awac_test_005_waves.csv';
% fn = 'awac_test_030_big_waves.csv';
% fn = 'awac_test_205_flat.csv';
% fn = 'awac_test_070_waves_noise.csv';
% fn = 'awac_test_070_waves_clean.csv';
fn = 'awac_test_300_partial.csv';
% fn = 'awac_test_393_floes.csv';
% fn = 'awac_test_255_steps.csv';
%fn = 'alt_20150218_123456.csv';

% $PRVAT,00.695,M,0010.990,dBar*32
A = [];
f = fopen(fn);
while(1)
    a = textscan(f,'$PRVAT,%f,M,%f,dBar*%*c%*c','CollectOutput',1);
	A = [A;a{1}];
    fgets(f);
    if feof(f),
        break;
    end
end
fclose(f);

% truncate?
% A = A(1:250,:);


r = A(:,1);
p = A(:,2);

surfcheck_fft(r,p);
