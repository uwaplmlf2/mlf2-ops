function result = surfcheck_fft(r,p)
% surfcheck (FFT) algorithm in Matlab (for testing)

MIN_SAMPLES = 100; % min number of good samples to attempt detection
P_ATM = 10; % atmospheric pressure (ideally, need to be able to change this, but we cannot!)
RANGE_FACTOR = 1; %this is to account for altimeter tilt and soundspeed correction (how?)
TRANSDUCER_OFFSET = 0.04; % need to add to measured range
R_MIN = 0.25; % we shouldn't be able to get that close to the ice because of the antenna.
H_MAX = 10; % max. ice thickness for crude filter
H_MIN = -10; % min ice thickness (need to allow for waves and wrong Patm)
H_SPIKE = 2; % spike threshold
BAD_H = NaN;
ICE_THRESHOLD = 0.2; %% ice/water threshold, discriminator  greater than this is classified as "water" (or smooth flat ice)
T_CUTOFF = 20; % s

ICE_QUANTILE = 0.01; % return this quantile of draft

n = length(r);
dt = 1; % s, sampling interval
Ny = 0.5/dt; % Hz, Nyquist
%% start the algorithm

H = p-P_ATM - (r+TRANSDUCER_OFFSET)*RANGE_FACTOR; %% note, that this is not real ice thickness, since P_ATM can be wrong


% fake <<<<<<<<<<<<<<<<<<<<<<<<<<<<
% H = sign(rand(size(H))-0.5);
% H = .5*cos(pi*(1:n)'+.01*randn(n,1));

H0 = H;
Hf = medfilt1(H,5);
% H = medfilt_repl(H,5,H_SPIKE);
% Hf = medfilt_repl(H,5);


 iok = (r>R_MIN & p>0 & H<H_MAX & H>H_MIN & abs(H-Hf)<H_SPIKE);
 H(~iok) = BAD_H;
% iok = ':';

% fill the gaps?
%  H = intnan(H);

% window?
% H = H.* hann(512);
% H = detrend(H);

qx = 0:.05:1;
Hq = quantile(H(iok),qx);


%% low pass?
f=(0:(n/2))'./(n*dt); % this is the FFT frequency scale
df = 1/(n*dt); % frequency quantum

F0 = (1/T_CUTOFF);
ilo = find(f<=F0);
ihi = find(f>F0 & f<f(end)); % exclude the last point (often shows strange max)


Hi = demean(H);
Hi(isnan(Hi)) = 0;

% % window would be here
% Hi = bsxfun(@times,Hi, hanning(n));

fH = fft(Hi);
fH = fH(1:n/2+1,:);
pH = 2*fH.*conj(fH)*(dt/n); % spectral density


% mean LF/HF spectral levels

vLF = mean(pH(ilo,:)); 
vHF = mean(pH(ihi,:)); 

lvLF = log10(vLF);
lvHF = log10(vHF);


% classificator:

Z = @(LF,HF) log10(HF./LF);  c_title = 'log_{10}(pHF/pLF)';

%% plot
%
yl = 'auto';%[-2 2];
figure(1);
clf
paper(10,7,'landscape')
SP = @(n) my_subplot(2,3,n);
a = SP(1:2);
% plot(P,'k');
hold on
plot(H0,'r.');
plot(H,'b.-');
plot(Hf,'m-');
% plot(LPH,'m','linewidth',1.5);
ylim(yl)
yl = ylim;
xlim([1 n]);
plot([1 n],quantile(H(iok),ICE_QUANTILE)*[1 1],'r--');
text(n+10,quantile(H(iok),ICE_QUANTILE),sprintf('%.0f%%',ICE_QUANTILE*100),'color','r');
plot([1 n],quantile(H(iok),.50)*[1 1],'r--');
text(n+10,quantile(H(iok),.5),'50%','color','r');
plot([1 n],quantile(H(iok),1-ICE_QUANTILE)*[1 1],'r--');
text(n+10,quantile(H(iok),1-ICE_QUANTILE),sprintf('%.0f%%',100-ICE_QUANTILE*100),'color','r');

% xlim([1 length(p)]);
flipy
ylabel('Draft (m)');
xlabel('Sample');
legend('H_{ice}','H_{ice} filt','5-pt median',4);
% title(fn,'interpreter','none')


a(2) = SP(3);
plot((0:(n-1))/(n-1),sort(H));
hold on
plot([0 1],quantile(H(iok),ICE_QUANTILE)*[1 1],'r--'); 
plot([0 1],quantile(H(iok),.50)*[1 1],'r--');
plot([0 1],quantile(H(iok),1-ICE_QUANTILE)*[1 1],'r--');

xlim([0 1]);
ylim(yl)
flipy
xlabel('CDF');
linkaxes(a,'y');

SP(4)
loglog(f, pH,'k')
hold on
plot(F0*[1 1],ylim,'k--');
plot(f(ilo),ilo*0+vLF,'b','linewidth',2);
plot(f(ihi),ihi*0+vHF,'r','linewidth',2);
text(mean(f(ilo))/2,vLF*2,'vLF','color','b','horizontalalignment','center');
text(mean(f(ihi))/2,vHF*2,'vHF','color','r','horizontalalignment','center');
xlabel('f (Hz)');
ylabel('pH (m^2/Hz)');

SP(5)
plot(lvLF, lvHF,'ko','markerfacecolor','r','markersize',8);
hold on
[x,y] = meshgrid(logspace(-4,2,10));
cl = -2:.5:2;
contour(log10(x),log10(y),Z(x,y),cl,'-','showtext','on')
contour(log10(x),log10(y),Z(x,y),[1 1]*ICE_THRESHOLD,'-','linewidth',2)
axis equal
text(mean(xlim)+.25*diff(xlim),mean(ylim)-.25*diff(ylim),'ICE','rotation',45,'horizontalalignment','center');
text(mean(xlim)-.25*diff(xlim),mean(ylim)+.25*diff(ylim),'WATER','rotation',45,'horizontalalignment','center');
xlabel('vLF (m^2/Hz)')
ylabel('vHF (m^2/Hz)')

D = Z(vLF, vHF);
if D>ICE_THRESHOLD
    s = sprintf('log_{10}(vHF/vLF)=%.2f>%.2f (\\bfWATER\\rm)',D,ICE_THRESHOLD);
    result = 'WATER';
else
    s = sprintf('log_{10}(vHF/vLF)=%.2f<%.2f (\\bfICE\\rm)',D,ICE_THRESHOLD);
    result = 'ICE';
end
SP(6)
text(0,1,s,'fontsize',12);
ylim([0 1.1])
seta visible off