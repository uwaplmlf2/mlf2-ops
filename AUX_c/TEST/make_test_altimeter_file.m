% generate test altimeter files...
fn = 'alt_test.csv';
N = 500;
P0 = 11; %atm pressure
D = 5;
dD = .5; %ascent
H = 2; % ice thickness
Hs = .5; % ice variance;
HL = 50; %low-pass
ice_prob = .85; % ice probability;
bad_prob = .005; % probability of malformed message
noise_prob = .02; %probability of noise

hi = black_filt(randn(N,1)*Hs+H,HL,'nopadding');
% insert open water?
fi = black_filt(rand(N,1),55,'nopadding');
fi = (fi-min(fi))/diff(minmax(fi));
hi(fi>ice_prob)=0;


p = P0+D-(1:N)'/N*dD;
r = p-P0-hi;

% noise
ni = rand(N,1)<noise_prob;
r(ni) = r(ni)*2;


f = fopen(fn,'wt');
for k=1:N
    s = sprintf('PRVAT,%06.3f,M,%08.3f,dBar',r(k),p(k));
    checksum = cumxor(s);
    if rand(1)<bad_prob, 
        s = sprintf('PRVAT,%06.3f,M,%08.3f,dBar',0,0); % also, the checksum wouldn't match
    end
    fprintf(f,'$%s*%x\n',s,checksum);
end

fclose(f);
%%,%
clf
plot(p-P0,'k');
hold on;
plot(hi,'b');
plot(p-P0-r,'r:');
flipy

