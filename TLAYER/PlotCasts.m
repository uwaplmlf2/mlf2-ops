% Plot CTD

clear all
close all

load '../Hydrography/PAPAII/cleanPAPAII.mat';
%   P           200x27            43200  double              
%   S           200x27            43200  double              
%   SIG         200x27            43200  double              
%   SIG_0       200x27            43200  double              
%   T           200x27            43200  double              
%   TH          200x27            43200  double              
%   readme        1x12               24  char                
%   sta           1x1              1960  struct 


for i=[1:5:50] %[2 5 6 8 20 27]
    [~,iz]=min(abs(P(:,i)-20));
    plot(SIG_0(:,i),-P(:,i),'.-');
    hold on
    ylim([-100 0]);
    text(SIG_0(iz,i),-P(iz,i),num2str(i),'FontSize',14);   
end
xlabel('\sigma_0');
