function W=Weight2Wnew(Cd,N,Area,L,Weight)
% W=Weight2W(Cd,N,Area,L,Weight)
% Uses float drag law to get vertical velocity from buoyancy
% Finds roots of the equation
%  Cd 0.5 rho Area W^2 + rho Area L N W - g Weight=0    
%  Units are Newtons = kg m/s^2  Weight in kg
% 
        rho=1024;g=9.8;
        PC(1)=0.5;   % W^2
        PC(2)=N*L/Cd;   % N L * W
        PC(3)= -g*abs(Weight)/Cd/rho/Area;
        if sum(isnan(PC))==0
            Wroot=roots(PC);
            pos=find(imag(Wroot)==0 & Wroot>=0);
     
            if length(pos)==0
                error('No drag law root');
            end
            
            W=Wroot(pos)*sign(Weight);
        else
             W=0;  % keep going - better than NaN
             disp(['No roots of ' num2str(PC) ' W=0; Keep going! ']);
        end
        % end 
