% Convert EQSim output to float output format

load 'Output/Data1.mat' P SAL TEMP Mode Day Drogue Ball
yd=Day'+299.4;
B=Ball';
S=SAL';
S(:,2)=SAL';
T=TEMP';
T(:,2)=TEMP';
P=P';
Th=sw_ptmp(S,T,P,0.);
PP=P-0.7;
PP(:,2)=P+0.7;
Sig0=sw_pden(S,T,P,0.);
drogue=Drogue';
mode=Mode';
settle=Mode*0.;
save 'EnvSim.mat' yd S T P B Th Sig0 drogue mode PP settle

