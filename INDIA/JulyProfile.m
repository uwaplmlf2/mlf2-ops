% Get a CTD profile for July data
clear all

Sin =[ 30.2 -300
    30.1123 -142.9593
    30.1021 -132.5386
    30.0857 -114.3023
    30.0775 -102.1448
    30.0632  -88.6847
    30.0326  -79.5666
    29.9937  -74.7904
    29.9528  -70.4485
    29.8711  -61.3303
    29.7995  -51.7780
    29.7198  -45.6992
    29.6933  -36.1469
    29.6646  -26.1604
    29.6360  -17.0422
   29.6012   -9.8946
   29.5822   -5.4231
   29.5023   -3.4130
   29.3185    0 ];
Tin = [ 11.2 -300
    11.2821 -149.5085
    11.3867 -110.0777
    11.5749  -78.9712
    11.8155  -59.2558
    12.0142  -43.4834
    12.2339  -28.5873
    12.4536  -19.8249
   12.6736   -9.8488
   13.2036   -3.8798
   13.6139   -2.6860
   14.6567     0 ];
PG=[0:2:10 15:5:200];

TG=interp1(Tin(:,2),Tin(:,1),-PG);
SG=interp1(Sin(:,2),Sin(:,1),-PG);

SigG=sw_pden(SG,TG,PG,0)-1000;

a1=subplot(1,3,1);
plot(Tin(:,1),Tin(:,2),'.',TG,-PG,'Linewidth',2)
a2=subplot(1,3,2);
plot(Sin(:,1),Sin(:,2),'.',SG,-PG,'Linewidth',2)
a3=subplot(1,3,3);
plot(SigG,-PG,'Linewidth',2)
hold on

linkaxes([a1 a2 a3],'y');
ylim([-150 0])
% 
%   PG         1x44              352  double                                 
%   SG         1x44              352  double                                 
%   SigG       1x44              352  double                                 
%   Sin       19x2               304  double                                 
%   TG         1x44              352  double                                 
%   Tin       12x2               192  double                            


% Add to PS database
root='/Users/eric/Documents/GIT/mlf2-ops/Hydrography/PSsim/';

ifile=[root 'newclean2.mat'];
ofile=[root 'newclean3.mat'];
load(ifile);
%  N            1x1                 8  double
%   P           50x17             6800  double
%   S           50x17             6800  double
%   SIG         50x17             6800  double
%   SIG_0       50x17             6800  double
%   T           50x17             6800  double
%   TH          50x17             6800  double

N=size(P,2)+1;
g=[1:size(PG,2)];
b=[size(PG,2)+1:size(P,1)];
P(g,N)=PG(g); P(b,N)=NaN;
T(g,N)=TG(g); T(b,N)=NaN;
S(g,N)=SG(g); S(b,N)=NaN;
SIG=sw_dens(S,T,P)-1000;
SIG_0=sw_pden(S,T,P,0)-1000;
TH=sw_ptmp(S,T,P,0);
sta.lat(N)=NaN;
sta.lon(N)=NaN;
sta.yr(N)=2020;
sta.mo(N)=7;
sta.day(N)=25;

plot(SIG_0(:,N),-P(:,N),'m+')

clear ifile root PG SG TG SigG Tin Sin a1 a2 a3
save(ofile)

