% Get a CTD profile for June 2021 data
% Run EnvDigitize.m to get values- then enter them here
clear all

Tin =[    14.1700   -0.0626
   12.9985   -4.3945
   11.5194   -7.4523
   11.2558  -11.2746
   11.1093  -16.6257
   10.8018  -19.6835
   10.6700  -29.3665
   10.5821  -40.0688
   10.4064  -49.7519
   10.1721  -54.8482
   10.0989  -87.2099
    9.5 -300];

Sin = [
    26.8059   -0.0167
   27.6529   -3.8997
   28.5750   -5.8413
   29.4220   -8.0255
   29.5078  -15.5490
   29.6472  -18.2186
   29.7758  -44.6722
   30.0117  -54.1372
   30.0868  -71.3685
   30.1189  -85.2020
   30.3 -300];

PG=[0:2:10 15:5:200];

TG=interp1(Tin(:,2),Tin(:,1),-PG);
SG=interp1(Sin(:,2),Sin(:,1),-PG);

SigG=sw_pden(SG,TG,PG,0)-1000;

a1=subplot(1,3,1);
plot(Tin(:,1),Tin(:,2),'.',TG,-PG,'Linewidth',2)
a2=subplot(1,3,2);
plot(Sin(:,1),Sin(:,2),'.',SG,-PG,'Linewidth',2)
a3=subplot(1,3,3);
plot(SigG,-PG,'Linewidth',2)
hold on

linkaxes([a1 a2 a3],'y');
ylim([-200 0])
% 
%   PG         1x44              352  double                                 
%   SG         1x44              352  double                                 
%   SigG       1x44              352  double                                 
%   Sin       19x2               304  double                                 
%   TG         1x44              352  double                                 
%   Tin       12x2               192  double                            


% Add to PS database
root='/Users/eric/Documents/GIT/mlf2-ops/Hydrography/PSsim/';

ifile=[root 'newclean5.mat'];
load(ifile);
%  N            1x1                 8  double
%   P           50x17             6800  double
%   S           50x17             6800  double
%   SIG         50x17             6800  double
%   SIG_0       50x17             6800  double
%   T           50x17             6800  double
%   TH          50x17             6800  double

ofile=[root 'newclean6.mat'];

N=size(P,2)+1;
g=[1:size(PG,2)];
b=[size(PG,2)+1:size(P,1)];
P(g,N)=PG(g); P(b,N)=NaN;
T(g,N)=TG(g); T(b,N)=NaN;
S(g,N)=SG(g); S(b,N)=NaN;
SIG=sw_dens(S,T,P)-1000;
SIG_0=sw_pden(S,T,P,0)-1000;
TH=sw_ptmp(S,T,P,0);
sta.lat(N)=NaN;
sta.lon(N)=NaN;
sta.yr(N)=2020;
sta.mo(N)=7;
sta.day(N)=25;

plot(SIG_0(:,N),-P(:,N),'m+')

clear ifile root PG SG TG SigG Tin Sin a1 a2 a3 XX b g i N
save(ofile)

