% build the simulator...
clear  mex
od = pwd;
cd ../C
try
%     mex -v ballast.c sw.c
    mex  -g ballast.c  sw.c ptable_mex.c
    mex -g labsimtest.c sw.c
    cd(od);
    if ispc
        movefile ../C/*.mexw64 .
    else
        movefile ../C/*.mexmaci64 .
    end
catch ME
    cd(od)
    ME.throw;
end

