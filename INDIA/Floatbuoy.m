% Float weight vr Depth


PS=[10 50; 10 30; 10 20; 50 100; 0 15 ; 15 100 ];

PS=[5 100; 5 20 ; 15 100];

for i=1:size(PS,1)
P=PS(i,1):PS(i,2);

VolP= -FM.chi*FM.vol0*P;
VolA= FM.air*10./(10.+max(P,0));
Vol=VolA+VolP;

L=polyfit(P,Vol,1);


plot( 1e6*(Vol-polyval(L,P)),-P,'LineWidth',2);
% PL=[min(P) max(P)];
% plot( 1e6*(Vol-polyval(L,PL)),-PL,'LineWidth',2)

grid on
xlabel('Vol/cc');
ylabel('-P')
hold on

end
xlim([-3 5]);
set(gca,'xtick',[-12:12]);
title('Deviation of Pressure from Linear over limited Pressure range');




