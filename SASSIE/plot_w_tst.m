% plot velocity control diagnostics (from the w_tst.dat file created during
% the simulation)
clear;

e = load('Data/Env1.mat');
e.EOS = load('EOS');

x = load('w_tst.dat');
% day_time,Pressure, PotDensity1, PotDensity2, N, Wp, Wr, Vo,Va,Voff
t = x(:,1)*24; % hours
P = x(:,2);
r1 = x(:,3); % PotDensity1
r2 = x(:,4); % PotDensity2
N = x(:,5);
Wp = x(:,6); % vertical velocity from pressure 
B = x(:,7); % actual Bocha
Vt = x(:,8); % Volume anomaly necessary for target w (from the drag law)
Vw = x(:,9); % Volume anomaly necessary for current w (from the drag law)
Vc = x(:,10); % Current Volume anomaly (from Bocha/EOS)

Voff = x(:,11); % offset relative to best-guess neutral ballast
Va = x(:,12); % requested volume anomaly (typically, Va=Vt-Voff unless limited by deltaB)

mode = x(:,13);

%% ballast file check
L = Read_ballast_file('bal00001.txt'); % this will also get EOS from mission.xml
bad = (isnan(L.RealTimeClock) |  L.B_Neutral<0);
n = length(bad);
for_each_field L 'if size(*,1) ==n, *(bad)=[]; end'
%%
figure(101)
clf
plot(Vw*1e6,Wp,'.');
hg
plot((Vc-Vw)*1e6,Wp,'.');


xlabel('\deltaV (cc)');
%%
figure(102)
clf
for k=1:4;a(k) = subplot(4,1,k);end
axes(a(1));
plot((e.yd-1)*24,e.P,'k');
hg
plot(t,P,'.');

imode = find(diff(e.mode)~=0)+1;
plot((e.yd(imode)-1)*24,e.P(imode),'k+');

flipy
legend('Post','Real-time')
ylabel('Pressure (dbar)')

axes(a(2));
e.B0 = float_neutral(mean(e.S,2),mean(e.T,2),mean(e.PP,2),e.yd-1,e.EOS);
plot((e.yd-1)*24,e.B0*1e6,'r--');
hg
plot(t+median(diff(t)),B*1e6,'.-');
plot((e.yd-1)*24,e.B,'k');

ylabel ('B (cc)');
legend('Neutral B (B0)','Request (delyed)','Actual B');

axes(a(3));
e.w = -diff(e.P)./diff(e.yd)/86400*1e3;
plot(mpoint(e.yd-1)*24,e.w,'k-');
hg
plot(t-0.5*median(diff(t)),Wp*1e3,'.'); % W is calculated (and recorded) since the previous data cycle
legend('Post','Real-time')
ylabel('w (mm/s)');

axes(a(4));
plot(t-0.5*median(diff(t)),Vt*1e6);
hg
plot(t-0.5*median(diff(t)),Vw*1e6);
plot(t-0.5*median(diff(t)),Vc*1e6);

plot(t-0.5*median(diff(t)),(Vt- (Vw-Vc))*1e6);
plot(t-0.5*median(diff(t)),Va*1e6,'.');

plot((e.yd-1)*24,(e.B-e.B0*1e6),'r--');



legend('Drag law for target W (Vt)','Drag law for current W (Vw)','Current (Vc = B-B0)','Vt-(Vw-Vc)','Va','B-B0');
xlabel('Hours');
ylabel('Volume anomaly (cc)')

linkaxes(a,'x');
axis tight