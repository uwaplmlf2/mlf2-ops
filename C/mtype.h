/*
** arch-tag: MLF2 mission-type macros
** Time-stamp: <2006-11-27 11:10:07 mike>
** 
*/
#ifndef _MTYPE_H_
#define _MTYPE_H_

/*------ ONE OF THESE MISSIONS NEEDS TO BE DEFINED --------  */
#define ARABIAN24 /* Arabian24   EKAMSAT*/
#undef INDIA /* INDIA 2022 */
#undef CALYPSO /* CALYPSO 2022 */
#undef SMODE /* SMODE 2020 */
#undef FLOATECO  /* FloatECO (aka Garbage Patch/Camera Float)2019*/
#undef TLAYER  /* Transition Layer 2018 */
#undef EXPORTS18 /* EXPORTS 2018*/
#undef LCDRI /* LCDRI */
#undef COLVOS16 /* COLVOS16 */
#undef SPURS2 /* SPURS 2 */
#undef LASER /* LASER CARTHE */
#undef MIZ  /* Marginal Ice Zone Float */
#undef ORCA  /* ORCA MOORING FLOAT */
#undef OMZ  /* OMZ - oxygen minimum zone float */
#undef BENGAL  /* Bay of Bengal mission */
#undef SPURS /* SPURS mission */
#undef LAKE2 /* Lake Washington year 2 */
#undef LATMIX /* LATMIX mission */
#undef LAKE /* Lake Washington mission */
#undef PUGETSOUNDBALLASTEOS /* define if a Puget Sound Ballast mission w EOS*/
#undef TYPHOON10 /* 2010 TYPOON float missions */
#undef HURR09 /* 2008 Hurricane Gas float mission */
#undef HURR08 /* 2008 Hurricane Gas float mission */
#undef NAB    /* North Atlantic Bloom Experiment Mission */
#undef HOODEOS /* Hood Canal Equation of State Mission */
#undef INTRUSION /* Intrusion floats with acoustic command */
#undef GASBALLAST /* define if a Puget Sound Ballast mission */
#undef DOGEE   /* Define if DOGEE gasfloat mission */
#undef PUGETSOUNDBALLAST /* define if a Puget Sound Ballast mission */
#undef MLB /* define if Mixed Layer Base mission - i.,e equatorial */
#undef AESOP /* AESOP subduction Mission */
#undef AESOPROOF /* AESOP roof test mission */
#undef AESOPEOS   /* AESOP EOS mission - only use for testing - real mission change parameters */

/* older defunct missions */
#undef PUGETSOUNDTEST   /* define if Puget Sound Test mission */
#undef SCS /* define if South China Sea mission */
#undef TURBULENCE /* define if ADV mission */
#undef PYCNOCLINE /* define if pycnocline mission */
#undef HURRICANE /* define if hurricane mission */
#undef HURRICANEGAS /* define if hurricane gasfloat mission */
#undef GASTEST /* defined if Gasfloat test mission */
#undef LABTEST /* define if labtest mission */
#undef LABTESTSIM /* define if a labtest simulation */

/* -------- NOTHING TO DO BELOW HERE ------*/

#ifdef LABTESTSIM
# define LABTEST
#endif

#if defined(LABTEST)
# define MISSIONTYPE	"Lab Test"
# define MFUNC		mission_labtest()
# if defined(LABTESTSIM)
# define MISSIONTYPE	"Lab Test Simulation"
# define MFUNC		mission_labtestsim()
# endif
#elif defined(PUGETSOUNDBALLAST)
# define MISSIONTYPE	"Puget Sound Ballast"
# define MFUNC		mission_psb()
#elif defined(PUGETSOUNDBALLASTEOS)
# define MISSIONTYPE	"Puget Sound Ballast with EOS"
# define MFUNC		mission_psbeos()
#elif defined(HURRICANE)
# define MISSIONTYPE	"Hurricane"
# define MFUNC		mission_hurricane()
#elif defined(HURRICANEGAS)
# define MISSIONTYPE	"HurricaneGas"
# define MFUNC		mission_hurricanegas()
#elif defined(TURBULENCE)
# define MISSIONTYPE	"Turbulence"
# define MFUNC		mission_turbulence()
#elif defined(PYCNOCLINE)
# define MISSIONTYPE	"Pycnocline"
# define MFUNC		mission_pycnocline()
#elif defined(MLB)
# define MISSIONTYPE	"MLB"
# define MFUNC		mission_mlb()
#elif defined(SCS)
# define MISSIONTYPE	"SCS"
# define MFUNC		mission_scs()
#elif defined(PUGETSOUNDTEST)
# define MISSIONTYPE	"PUGETSOUNDTEST"
# define MFUNC		mission_pugetsoundtest()
#elif defined(AESOP)
# define MISSIONTYPE	"AESOP"
# define MFUNC		mission_aesop()
#elif defined(INTRUSION)
# define MISSIONTYPE	"INTRUSION"
# define MFUNC		mission_intrusion()
# elif defined(AESOPROOF)
# define MISSIONTYPE	"AESOPROOF"
# define MFUNC		mission_aesoproof()
#elif defined(AESOPEOS)
# define MISSIONTYPE	"AESOPEOS"
# define MFUNC		mission_aesopeos()
#elif defined(DOGEE)
# define MISSIONTYPE	"DOGEE"
# define MFUNC		mission_dogee()
#elif defined(NAB)
# define MISSIONTYPE	"NAB"
# define MFUNC		mission_nab()
#elif defined(HOODEOS)
# define MISSIONTYPE	"HOODEOS"
# define MFUNC		mission_hoodeos()
#elif defined(GASBALLAST)
# define MISSIONTYPE	"GASBALLAST"
# define MFUNC		mission_gasballast()
#elif defined(HURR08)
# define MISSIONTYPE	"HURR08"
# define MFUNC		mission_hurr08()
#elif defined(HURR09)
# define MISSIONTYPE	"HURR09"
# define MFUNC		mission_hurr09()
#elif defined(TYPHOON10)
# define MISSIONTYPE	"TYPHOON10"
# define MFUNC		mission_typhoon10()
#elif defined(LAKE)
# define MISSIONTYPE	"LAKE"
# define MFUNC		mission_lake()
#elif defined(LATMIX)
# define MISSIONTYPE	"LATMIX"
# define MFUNC		mission_latmix()
#elif defined(LAKE2)
# define MISSIONTYPE	"LAKE2"
# define MFUNC		mission_lake2()
#elif defined(SPURS)
# define MISSIONTYPE	"SPURS"
# define MFUNC		mission_spurs()
#elif defined(BENGAL)
# define MISSIONTYPE	"BENGAL"
# define MFUNC		mission_bengal()
#elif defined(OMZ)
# define MISSIONTYPE	"OMZ"
# define MFUNC		mission_OMZ()
#elif defined(ORCA)
# define MISSIONTYPE	"ORCA"
# define MFUNC		mission_ORCA()
#elif defined(MIZ)
# define MISSIONTYPE	"MIZ"
# define MFUNC		mission_MIZ()
#elif defined(LASER)
# define MISSIONTYPE	"LASER"
# define MFUNC		mission_LASER()
#elif defined(SPURS2)
# define MISSIONTYPE	"SPURS2"
# define MFUNC		mission_spurs2()
#elif defined(COLVOS16)
# define MISSIONTYPE	"COLVOS16"
# define MFUNC		mission_colvos16()
#elif defined(LCDRI)
# define MISSIONTYPE	"LCDRI"
# define MFUNC		mission_lcdri()
#elif defined(EXPORTS18)
# define MISSIONTYPE	"EXPORTS18"
# define MFUNC		mission_exports18()
#elif defined(TLAYER)
# define MISSIONTYPE	"TLAYER"
# define MFUNC		mission_TLayer()
#elif defined(FLOATECO)
# define MISSIONTYPE	"FloatECO"
# define MFUNC		mission_FloatECO()
#elif defined(SMODE)
# define MISSIONTYPE	"SMODE"
# define MFUNC		mission_SMODE()
#elif defined(CALYPSO)
# define MISSIONTYPE    "CALYPSO"
# define MFUNC        mission_CALYPSO()
#elif defined(INDIA)
# define MISSIONTYPE    "INDIA"
# define MFUNC        mission_INDIA()
#elif defined(ARABIAN24)
# define MISSIONTYPE    "ARABIAN24"
# define MFUNC        mission_ARABIAN24()
#else
# error "No mission type defined!"
#endif

extern void MFUNC;

#endif /* _MTYPE_H_ */


