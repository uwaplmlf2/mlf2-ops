THIS IS NOT PROPERLY CHECKED INTO GIT
/* FROM MIZ */
/* APR, 2022  */

# include <time.h>


/* Available stages within this mission: */
#define STAGE_PSBALLAST             0
#define STAGE_ICE_PROFILE         1
#define STAGE_EMERGENCY_UP         2


/* include subordinate files */
#include "InitializeSASSIE.c"
#include "IceSASSIE.c"
#include "SampleSASSIE.c"
#include "ErrorsSASSIE.c"



int next_mode(int nmode, double day_time)
{
static int icall=0; // this should be renamed "phase"
static int next_icall = 0;

static int oldstage=0;
static int savemode; /* saved value of mode - except error */
static int savetimer; /* saved value of Timer.enable */
static int commskipcount=0;  /* Counts skipped comm modes  (not implemented in LASER) */
static int goodcomm=1;  /* flag to set if this program set COMM mode (as opposed to the emergency COMM after ERROR)*/
static int icycle; /* cycle counter for Profile mode */
static double hometime=-1; /* time of last home */
static double looptime=-1; /* time of start of steploop */

// backup Timer.Enable
static short save_Timer_enable=-1;

int oldmode;
time_t now;
struct tm  *tm;
double yday;


oldmode=nmode;

newmode = 1; // by default, we expect the mode to change. We'll set newmode=0 explicitly when this isn't the case (rare!)


/* Check if this is error recovery.
 * Several things can happen:
 * 1. Error triggered COMM, which just returnd. In this case, we'll see (nmode==MODE_COMM && goodcomm!=1)
 * 2. Error handling routine decided to ignore the error and called next_mode(MODE_RESTORE)
 * 3. Error handling routine switch stage/mode, which will look just like a normal mode change.
 * Need to check for the first two cases and restore the old mode:
 */
if ( (nmode == MODE_COMM && goodcomm != 1) || (nmode == MODE_RESTORE) )
{
    /* just restore the previous mode */
    nmode=savemode;
    log_event("next_mode: ERROR recovery, mode stays at %d\n",nmode);
    newmode = 0; // explicitly state that this is continuation of an old mode
    return(nmode);
}

/* Check if this is the start of a stage (do it centrally now!)
 * Note that a new stage should start at the beginning. No need to check for new stage below, just check for MODE_START.
 */
if (nmode == MODE_START || stage != oldstage) {
    log_event("*** STAGE %d ***\n", stage);
    oldstage = stage;
    nmode = MODE_START;
    icall = 1;
    ice_reset(); // do we need that?
}
// Update the state - just the stage and phase(icall) for now
SET_STATE(stage,icall,0,0);

ADVANCE_PHASE:
next_icall = icall + 1; // by default, plan to proceed to the next phase; in some cases this can be changed to skip/rewind phases

goodcomm=0; /* reset COMM flag */

CTDprof.Run = 0; // Reset CTD profiling flag (to be enabled in certain modes)

Snapshot.Run = 0; // Reset photo-taking flags (to be enabled in certain modes)
Snapshot.Force = 0;

IceAvoid.Run = 0; // Reset ice avoidnance flag (to be enabled in certain modes)

Ice.Run = 0; // Reset SURFCHECK flag (to be enabled in certain modes)

switch (stage){
case STAGE_PSBALLAST:
    /* --------------------- PS BALLAST STAGE ---------------   */
    if (nmode == MODE_START) {
        // Disable timers for PS ballast:
        if (save_Timer_enable == -1){ // need to check this, because there may be multiple calls with MODE_START
            save_Timer_enable = Timer.enable;
        Timer.enable = 0;
        }
    }

    switch(icall){
    case 1: nmode = MODE_PROFILE_DOWN;  // Initial captive dive  - short
            log_event("next_mode:PSBall down\n");
            Prof = PSBallast.Down[0];
            set_param_int("down_home", 1); // always home on the first run
            break;
        case 2: nmode=MODE_SETTLE; // First (captive) settle
            log_event("next_mode:PSBall Settle\n");
            Settle = PSBallast.Settle[0];
            hometime = day_time; // Home must have happened just prior to this. Record this home
            break;
        case 3: nmode=MODE_DRIFT_SEEK; // First (captive) drift
            log_event("next_mode:PSBall Seek\n");
            Drift = PSBallast.Drift[0];
            break;
        case 4: nmode = MODE_PROFILE_UP;
            log_event("next_mode:PSBall Up\n");
            Prof = PSBallast.Up;
            CTDprof.Run = 1; // enable the CTD profile
            Snapshot.Run = 1;
            Snapshot.Force = 1;
            IceAvoid.Run = 1; // test ice avoidance

            break;
        case 5: nmode=MODE_COMM;
            goodcomm=1;
            log_event("next_mode:PSBall COMM after test dive\n");
            // Force COMM WAIT
            set_param_int("comm:waiting", 1);
            ctd_stop_profile(); // explicitly stop the CTD profile before COMM
            break;
            
            /**** Actual ballast - longer settle */
        case 6: nmode = MODE_PROFILE_DOWN;
            log_event("next_mode:PSBall Down\n");
            Prof = PSBallast.Down[1];
            set_param_int("down_home", -1); // do not home on this one
            break;
        case 7: nmode=MODE_SETTLE;
            log_event("next_mode:PSBall Settle\n");
            Settle = PSBallast.Settle[1];
            break;
        case 8: nmode=MODE_DRIFT_SEEK;
            log_event("next_mode:PSBall Seek\n");
            Drift = PSBallast.Drift[1];
            break;
        case 9: nmode = MODE_PROFILE_UP;
            log_event("next_mode:PSBall Up\n");
            Prof = PSBallast.Up;
            CTDprof.Run = 1; // enable the CTD profile
            Snapshot.Run = 1;
            Snapshot.Force = 1;
            IceAvoid.Run = 1; // test ice avoidance
            break;
        case 10: nmode = MODE_COMM;
            goodcomm = 1;
            log_event("next_mode:PSBall COMM after settle\n");
            // enable timers:
            Timer.enable = save_Timer_enable;
            stage = STAGE_ICE_PROFILE;  /* continue as STAGE_ICE_PROFILE */
            log_event("PS ballast will end after this COMM. Continue as stage %d\n",stage);
            ctd_stop_profile(); // explicitly stop the CTD profile before COMM
            break;
        default:
            goto Bad_icall; // will throw ERR_INVALID_ICALL
    }
    break; /* End STAGE_PSBALLAST loop */
case STAGE_ICE_PROFILE:
    /* --------------------- ICE PROFILE STAGE ---------------   */
    switch (icall){
    case 1:    nmode = MODE_PROFILE_DOWN; // start with going DOWN (may be a deeper down))
        Prof = Mission.Down;
        if (hometime < 0 || day_time - hometime > Home_days) {   // Home on this down
            set_param_int("down_home", 1);
            hometime = day_time; // remember the time
        }
        else {
            set_param_int("down_home", -1); // do not home - preferred
        }
        icycle = 0; // count the number of "UP" cycles
        break;
    case 2:
        nmode=MODE_PROFILE_UP;
        Prof = Mission.CycleUp;
        CTDprof.Run = 1; // enable the CTD profile
        Snapshot.Run = 1;
        IceAvoid.Run = 1; // enable ice avoidance
        ++icycle;
        break;
    case 3:
        log_event("Cycle# %d of %d done \n", icycle,Mission.ncycles);
        if (icycle<Mission.ncycles){
            //go back down
            nmode=MODE_PROFILE_DOWN;
            Prof = Mission.CycleDown;
            set_param_int("down_home", 0);

            next_icall = 2;
        }
        else {
            // insertion DOWN run
            nmode=MODE_PROFILE_DOWN;
            Prof = Mission.CycleDown;
            Prof.Ptarget = Mission.Drift_P.Ptarget;
            set_param_int("down_home", 0);
        }
        break;
    case 4:
            // check ice, comm?
            nmode = MODE_DRIFT_P;          //  ice-profiling drift
            Drift_P = Mission.Drift_P;

            ice_reset(); // Reset the surfacing rules (I think they were already reset, but ok)
            Ice.Run = 1; // run ice sampling (in sample.c) -- or request altimeter start

            Snapshot.Run = 1;
        break;
    case 5:
        //decision! (will be either UP->COMM or rewind to icall=1)
        // Get real time (double check "always-surface" mode)
        time(&now);
        tm = gmtime(&now);
        yday = (double)tm->tm_yday + (double)tm->tm_hour / 24.0 + (double)tm->tm_min / 24.0 / 60.0 + (double)tm->tm_sec / 24.0 / 60.0 / 60.0;

        if (Ice.okToSurface || yday > Ice.EpochYearDay[N_ICE_EPOCH - 1])
        {
            log_event("\nSurfacing!\n");
            nmode = MODE_PROFILE_UP;
            Prof = Mission.Up;
            ice_reset();
            CTDprof.Run = 1;
            IceAvoid.Run = 0; // We already committed to surfacing, so no ice avoidance here
        }
        else
        {
            log_event("\nNot surfacing!\n");
            ice_reset();
            icall = 1;
            goto ADVANCE_PHASE;
        }
        break;

    case 6: nmode = MODE_COMM; // < can we check if we timed out (i.e. stuck in ice?)
        ctd_stop_profile(); // explicitly stop the CTD profile before COMM
        goodcomm = 1;
        next_icall = 1; // after COMM, restart with icall=1
        break;
    
     default:
                goto Bad_icall; // will throw ERR_INVALID_ICALL
    }
    break; /* End STAGE_ICE_PROFILE loop */
    case STAGE_EMERGENCY_UP:
    /* --------------------- EMERGENCY UP STAGE ---------------   */
    // This is error-response stage, and it is structured differently:
    // It can be called many times with MODE_START, but we must carry on
    if (nmode == MODE_START){ // first call or still in error
        // Disable timers
            if (save_Timer_enable == -1){ // need to check this, because there may be multiple calls with MODE_START
                save_Timer_enable = Timer.enable;
            Timer.enable = 0;
            }
        ice_reset();     // Reset the surfacing rules
        nmode = MODE_PROFILE_UP; // emergency UP
        Prof = Mission.EmergencyUp;
    }
    else { // call at the end of MODE_UP
        Timer.enable = save_Timer_enable; // enable timers
        stage = STAGE_ICE_PROFILE;
        icall = 1;
        log_event("End of EMERGENCY_UP. Continue as stage %d\n",stage);
        goto ADVANCE_PHASE;
    }
    break;// end STAGE_EMERGENCY_UP
    
    
}
 if (newmode)
    log_event("Next_mode: %d->%d\n",oldmode,nmode);
else
    log_event("Next_mode: continue with %d\n", nmode);

SET_STATE_MODE(nmode);
savemode=nmode;
icall = next_icall; // set-up the phase of the next run
return(nmode);

/* Handle the bad icall exception here: */
Bad_icall:
nmode = MODE_ERROR;
log_event("ERROR in next_mode(): impossible combination: stage %d, icall %d\n", stage, icall);
set_param_int("error_code", ERR_INVALID_ICALL);
// Update the state
SET_STATE_MODE(nmode); // Update the state
return(nmode);
}




/************************************************************************************************************/
/*       Timer/Command/Error Exception handling functions                                                    */
/*    Each function returns                                                                                    */
/*    0 if Ballast.c can proceed normally                                                                        */
/*    -1 if immediate return is needed                                                                        */
/*    1 if the current mode needs to terminate gracefully                                                        */
/*                        (mode is set to new_mode, and next_mode is called prior to return)                    */
/************************************************************************************************************/

/* This function will handle timers */
int handle_timer(int mode, double day_time, int timer_command, int *mode_out)
{ // We can switch modes & stages based on the timer_command.
// Old functionality (stage switching) can be replicated by setting stage = timer_command and returning 1 to end the mode.
// The only problem for now is signalling stage reset (but mode_out should help with that)
int result = 0;
if (timer_command == 1)
{ // time to surface!
    Ice.act_now = 1; // will make the decision based on the next altimeter run!
    Ice.day_time_last = -1; // Force the altimeter run
    log_event("Setting 'act now' flag.\n");
}
else
{
    log_event("Bad timer command (%d)\n", timer_command);
}
return result;
}

/* This function will handle commands */
int handle_command(int mode, double day_time, int command, int *mode_out)
{
int surfcheck_result;
// SURFCHECK command, process the new result
#ifndef SIMULATION
surfcheck_result = get_surfcheck_result();
#else
surfcheck_result = command; // in simulation, keep passing via command (so that the simulator could trigger it)
#endif
return check_surfacing_rules(day_time, surfcheck_result); // will return 1 if need to terminate current mode
}

