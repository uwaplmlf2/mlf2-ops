/* ***************  Arabian 2024 ***************
From SASSIE & SMODE & India
Set float sampling scheme for this mission
Call on every data loop
*/


#ifdef SIMULATION
    /* GPS control dummy plugs */
    void start_surf_gps (void){
        return;
    }

    void  stop_surf_gps(void){
        return ;
    }

#endif

#ifdef SIMULATION
    //ctd_prof dummy functions for simulator 
    int CTD_PROFILE_FLAG = 0;

    void ctd_start_profile(int which) { CTD_PROFILE_FLAG = 1; log_event("Starting CTD(%d) profile\n", which); };
    int  ctd_stop_profile(void) { CTD_PROFILE_FLAG = 0;  log_event("Stopping CTD profile\n"); return 1; };
    int ctd_profile_active(void) { return CTD_PROFILE_FLAG; };
#endif


#ifdef SIMULATION
#define BOTTOM_CTD_INDEX        0
#define TOP_CTD_INDEX           1
#endif


int sampling(int nmode, double day_time, double day_sec, double mode_time)
{
    if (PressureG <= GPS.Pmax && !GPS.Running){
        // turn on GPS
        start_surf_gps();
        GPS.Running = 1;
        log_event("GPS ON (P=%.1f)\n",PressureG);

        }
     else if (PressureG > GPS.Pmax && GPS.Running) {
        stop_surf_gps();
        GPS.Running = 0;
        log_event("GPS OFF (P=%.1f)\n",PressureG);
     }

     SET_STATE_VAR(GPS.Running); // set state variation to indicate the GPS cycle

    // CTD profiling control
    // log_event("%d %f\n",ctd_profile_active(),CTDprof.offDepth);
    if (CTDprof.Run) { // CTD profiling is potentially active in this mode!
        if (!ctd_profile_active() && PressureG < CTDprof.onDepth) { //not yet running, but the onDepth has been reached - start up
            log_event("%5.3f P=%.1f CTD prof start (Sample)\n", day_time, PressureG);
            if (CTDprof.topbott){
                ctd_start_profile(TOP_CTD_INDEX);
            }
            else{
                ctd_start_profile(BOTTOM_CTD_INDEX);
            }
        }
    }
    else if (ctd_profile_active() && PressureG > CTDprof.offDepth) { // CTD profiling should not be active, but it is still running and we've reached the offDepth  -- shut it down
        ctd_stop_profile();
        log_event("%5.3f P=%.1f CTD prof end (Sample)\n", day_time, PressureG);
    }
  
    SET_STATE_VAR(ctd_profile_active()?1:0); // CTD profile mode variation

return(0);
}
