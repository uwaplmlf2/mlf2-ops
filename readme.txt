This is the root of MLF mission-level code development.

./C 		- Home of the ballast.c and other core code. This code is universal and (theoretically) should run with any mission. 
./C/Missions	- Mission-specific C code, typically 3 files per mission:
			Initialize[Mission].c	- setting mission parameters
			[Mission].c		- control of mode changes (next_mode function)
			Sample[Mission].c	- control of sampling scheme, enabling/disabling the sensors, etc.
		  Old missions remain here for the reference.
./AUX_C		- Code for the auxiliary board (TS-4200/8160), home of surfcheck.c
./MatlabSim	- Matlab simulator code (used to be BallastLib)
./Hydrography	- Collection of CTD profiles used for mission simulation. (Logically, this belongs in ./[Mission]/Hydrography...)

The following directories contain documentation, illustrations, specific simulation code and simulation results, etc. for each "incarnation" 
(incarnation may include several different missions, e.g. SPURS had two slightly different deployments):
./SPURS
./BENGAL
./ORCA
./MIZ

Within these, 
./[Incarnation]/Doc 	- all documentation, notes, code change log, etc.
./[Incarnation]/Data 	- Simulator output 
./[Incarnation]/Output 	- (same)


