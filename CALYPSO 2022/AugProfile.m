% Get a CTD profile for August 2020 data
% Run EnvDigitize.m to get values- then enter them here
clear all

Tin =[  14.5756   -0.0542
   13.3880   -9.1582
   13.0966  -24.1149
   12.9174  -31.7016
   12.7885  -36.4704
   12.5420  -46.2247
   12.4524  -58.7970
   12.2843  -75.2710
   12.2339  -89.1438
   12.0994 -115.3721
   12.0154 -148.7536];

Sin = [   29.8273   -0.4877
   29.8702  -10.8923
   29.9356  -25.8490
   29.9972  -43.4068
   30.0420  -50.5600
   30.0700  -57.7132
   30.1391  -65.7334
   30.2848  -85.4588
   30.3203  -98.8981
   30.3800 -109.9530
   30.4939 -148.3201 ];

PG=[0:2:10 15:5:200];

TG=interp1(Tin(:,2),Tin(:,1),-PG);
SG=interp1(Sin(:,2),Sin(:,1),-PG);

SigG=sw_pden(SG,TG,PG,0)-1000;

a1=subplot(1,3,1);
plot(Tin(:,1),Tin(:,2),'.',TG,-PG,'Linewidth',2)
a2=subplot(1,3,2);
plot(Sin(:,1),Sin(:,2),'.',SG,-PG,'Linewidth',2)
a3=subplot(1,3,3);
plot(SigG,-PG,'Linewidth',2)
hold on

linkaxes([a1 a2 a3],'y');
ylim([-150 0])
% 
%   PG         1x44              352  double                                 
%   SG         1x44              352  double                                 
%   SigG       1x44              352  double                                 
%   Sin       19x2               304  double                                 
%   TG         1x44              352  double                                 
%   Tin       12x2               192  double                            


% Add to PS database
root='/Users/eric/Documents/GIT/mlf2-ops/Hydrography/PSsim/';

ifile=[root 'newclean4.mat'];
load(ifile);
%  N            1x1                 8  double
%   P           50x17             6800  double
%   S           50x17             6800  double
%   SIG         50x17             6800  double
%   SIG_0       50x17             6800  double
%   T           50x17             6800  double
%   TH          50x17             6800  double

ofile=[root 'newclean5.mat'];

N=size(P,2)+1;
g=[1:size(PG,2)];
b=[size(PG,2)+1:size(P,1)];
P(g,N)=PG(g); P(b,N)=NaN;
T(g,N)=TG(g); T(b,N)=NaN;
S(g,N)=SG(g); S(b,N)=NaN;
SIG=sw_dens(S,T,P)-1000;
SIG_0=sw_pden(S,T,P,0)-1000;
TH=sw_ptmp(S,T,P,0);
sta.lat(N)=NaN;
sta.lon(N)=NaN;
sta.yr(N)=2020;
sta.mo(N)=7;
sta.day(N)=25;

plot(SIG_0(:,N),-P(:,N),'m+')

clear ifile root PG SG TG SigG Tin Sin a1 a2 a3 XX b g i N
save(ofile)

